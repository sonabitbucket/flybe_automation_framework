package PageBase;

import efly.business_logics.online.*;


public class EFLYBasePage {

    public EFLYCheckinFindBooking findBooking = new EFLYCheckinFindBooking();
    public EFLYCheckinPassengerDetails passengerDetails = new EFLYCheckinPassengerDetails();
    public EFLYCheckinJourneyDetails journeyDetails = new EFLYCheckinJourneyDetails();
    public EFLYBoardingPass boardingPass = new EFLYBoardingPass();
    public EFLYCheckinComplete checkicomplete = new EFLYCheckinComplete();
    public CMSLeapCheapFlight home = new CMSLeapCheapFlight();
    public LiveHome liveHome = new LiveHome();
    public LiveFareSelect liveFareSelect = new LiveFareSelect();
    public EFLYRetrieval retrieval = new EFLYRetrieval();
    public CMSCheckInRetrievePNR checkin = new CMSCheckInRetrievePNR();
}
