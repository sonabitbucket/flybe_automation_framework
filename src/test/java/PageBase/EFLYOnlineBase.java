package PageBase;

import efly.business_logics.online.*;

/**
 * Created by Sona on 3/20/2019.
 */
public class EFLYOnlineBase extends EFLYBasePage{

    public EFLYFareSelect fareselect = new EFLYFareSelect();
    public EFLYTravellerDetails paxDetails = new EFLYTravellerDetails();
    public EFLYSeats seat = new EFLYSeats();
    public EFLYExtras extras = new EFLYExtras();
    public EFLYPayment payment = new EFLYPayment();
    public EFLYConfirmation confirmation = new EFLYConfirmation();
    public CMSOffers offers = new CMSOffers();
    public CMSCheapFlightsTo cheapFlightsTo = new CMSCheapFlightsTo();
    public CMSCheapFlightsFrom cheapFlightsFrom = new CMSCheapFlightsFrom();
    public CMSLiveArrivalAndDeparture lad = new CMSLiveArrivalAndDeparture();
    public EFLYTimetable timetable = new EFLYTimetable();
    public DigitalProfile digitalProfile = new DigitalProfile();
    public EFLYManageBookingTravellerDetails mb_paxDetails = new EFLYManageBookingTravellerDetails();
    public EBank ebank = new EBank();
    public EFLYManageBookingFlightDetails mb_flightDetails = new EFLYManageBookingFlightDetails();
    public EFLYManageBookingFlightSelect mb_flightSelect = new EFLYManageBookingFlightSelect();
    public EFLYManageBookingFareSelect mb_fareSelect = new EFLYManageBookingFareSelect();
    public EFLYManageBookingTravelExtras mb_extras = new EFLYManageBookingTravelExtras();
    public EFLYManageBookingPayment mb_payment = new EFLYManageBookingPayment();
    public EFLYManageBookingConfirmation mb_confirmation = new EFLYManageBookingConfirmation();
    public CMSManageBookingRetrievePNR mb_retrieve = new CMSManageBookingRetrievePNR();
    public CMSADS ads = new CMSADS();
    public EFLYLanguageVariantFR frenchSite= new EFLYLanguageVariantFR();
    public EFLYLanguageVariantDE germanSite = new EFLYLanguageVariantDE();
    public EFLYLanguageVariantES spanishSite = new EFLYLanguageVariantES();
}
