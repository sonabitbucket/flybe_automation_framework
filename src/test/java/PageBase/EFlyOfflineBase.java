package PageBase;

import efly.business_logics.offline.*;


public class EFlyOfflineBase extends EFLYBasePage {

    public ARDWEBLogin login = new ARDWEBLogin();
    public ARDWEBHOME home = new ARDWEBHOME();
    public ARDFlightSearch flightSearch = new ARDFlightSearch();
    public ARDPassengerAndContactInformation passengerContactInfo = new ARDPassengerAndContactInformation();
    public ARDNewPNR newPNR = new ARDNewPNR();
    public ARDTicketingArrangement tktArrangement = new ARDTicketingArrangement();
    public ARDPricing pricing = new ARDPricing();
    public ARDManagePayment payment = new ARDManagePayment();
    public ARDModifyFlight modifyFlight = new ARDModifyFlight();
    public ARDRevalidateETKT revalidate = new ARDRevalidateETKT();
    public ARDETicket eticket = new ARDETicket();
    public ARDAddService service = new ARDAddService();
    public ARDRepriceAutomatically reprice = new ARDRepriceAutomatically();
}
