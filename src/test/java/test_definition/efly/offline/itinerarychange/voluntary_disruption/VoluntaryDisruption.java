/*
package test_definition.efly.offline.itinerarychange.voluntary_disruption;

import PageBase.EFlyOfflineBase;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.HashMap;

public class VoluntaryDisruption {

    @Test
    public void ARD9_TST_14711_verifyUserIsAbleToChangeTheDateToNextDayForTicketUsed(){
        HashMap testData = new DataUtility().testData("9");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        //eflyOffline.flightSearch.verifyItinerarySection(selectedFlightDetails,testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        HashMap<String,String>passengerNames = eflyOffline.passengerContactInfo.retrievePassengerNames(testData);
        eflyOffline.flightSearch.verifyPassengerDetailsAfterAddingPassengers(noOfPassengers,passengerNames);
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        //eflyOffline.newPNR.verifyTicketingInformationMessageDisplayed();
        String pnr = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.verifyItineraryDetails("not ticketed",selectedFlightDetails,testData);
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        eflyOffline.newPNR.verifyItineraryDetails("ticketed",selectedFlightDetails,testData);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToModifyFlights();
        HashMap<String,String> newFlightDetails = eflyOffline.modifyFlight.changeFlightNumberORclassORdate(testData,selectedFlightDetails);
        System.out.println(newFlightDetails);
        eflyOffline.newPNR.verifyItineraryDetails("unknown",newFlightDetails,testData);
        eflyOffline.newPNR.navigateToRevalidateETKT();
        eflyOffline.revalidate.revalidate(noOfPassengers);
        eflyOffline.newPNR.verifyItineraryDetails("ticketed",newFlightDetails,testData);
        eflyOffline.eticket.verifyCouponStatusForEachETKT(noOfPassengers,newFlightDetails,etkt,testData);
    }

//    @Test //not priority now
    public void ARD10_TST_14730_verifyUserIsAbleToDoTwoWayBookingForMultiSectorFlightsWhereTheUserFliesFromOneCityToAnotherButReturnsToTheOriginalCityFromDifferentPlace(){
        HashMap testData = new DataUtility().testData("10");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        eflyOffline.flightSearch.enterJourneyDetails(testData);
        HashMap<String,String> selectedFlightDetails = eflyOffline.flightSearch.selectAndBookFlightAndRetrieveBookedFlightDetails(testData);
        eflyOffline.flightSearch.verifyItinerarySection(selectedFlightDetails,testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        HashMap<String,String>passengerNames = eflyOffline.passengerContactInfo.retrievePassengerNames(testData);
        eflyOffline.flightSearch.verifyPassengerDetailsAfterAddingPassengers(noOfPassengers,passengerNames);
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.navigateToAddORmodify();
        eflyOffline.flightSearch.selectSearchTypeAndAddDetails(testData,"Ghost, Passive & Information Sector Sell","Information (ARNK)");

        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData,"forceEOT");
        //eflyOffline.newPNR.verifyTicketingInformationMessageDisplayed();
        String pnr = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.verifyItineraryDetails("not ticketed",selectedFlightDetails,testData);
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        eflyOffline.newPNR.verifyItineraryDetails("ticketed",selectedFlightDetails,testData);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.eticket.verifyCouponStatusForEachETKT(noOfPassengers,selectedFlightDetails,etkt,testData);
    }
}
*/
