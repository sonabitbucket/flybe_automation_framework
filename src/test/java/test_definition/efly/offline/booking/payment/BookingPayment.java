package test_definition.efly.offline.booking.payment;

import PageBase.EFlyOfflineBase;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.HashMap;

public class BookingPayment {

    @Test
    public void ARD1_TST_14618_14665_completeBookingUsingCurrencyGBPWithCashAndSSCI(){
        HashMap testData = new DataUtility().testData("1");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight/*AndRetrieveBookedFlightDetails*/(testData);
        ////eflyOffline.flightSearch.verifyItinerarySection(selectedFlightDetails,testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        HashMap<String,String>passengerNames = eflyOffline.passengerContactInfo.retrievePassengerNames(testData);
        eflyOffline.flightSearch.verifyPassengerDetailsAfterAddingPassengers(noOfPassengers,passengerNames);
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        //eflyOffline.newPNR.verifyTicketingInformationMessageDisplayed();
        String pnr = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.findBooking.navigateToIdentificationPage();
        eflyOffline.findBooking.checkinThroughIdentificationPage(passengerNames, pnr);
        eflyOffline.findBooking.continueToJourneyDetailsPage(1,testData);
        eflyOffline.journeyDetails.navigateToPassengerDetailsPage();
        //eflyOffline.boardingPass.printBoardingPass(testData, pnr); - fixme
        eflyOffline.boardingPass.continueToCompleteCheckin();
        eflyOffline.checkicomplete.completeCheckin();
    }

    @Test
    public void ARD2_TST_14625_14663_completeBookingUsingCurrencyEURWithVisa() {
        HashMap testData = new DataUtility().testData("2");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        ////eflyOffline.flightSearch.verifyItinerarySection(selectedFlightDetails,testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        HashMap<String,String>passengerNames = eflyOffline.passengerContactInfo.retrievePassengerNames(testData);
        eflyOffline.flightSearch.verifyPassengerDetailsAfterAddingPassengers(noOfPassengers,passengerNames);
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        //eflyOffline.newPNR.verifyTicketingInformationMessageDisplayed();
        String pnr = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
    }

    @Test
    public void ARD3_TST_14626_14654_completeBookingUsingCurrencyCHFWithDiner() {
        HashMap testData = new DataUtility().testData("3");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        //eflyOffline.flightSearch.verifyItinerarySection(selectedFlightDetails,testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        HashMap<String,String>passengerNames = eflyOffline.passengerContactInfo.retrievePassengerNames(testData);
        eflyOffline.flightSearch.verifyPassengerDetailsAfterAddingPassengers(noOfPassengers,passengerNames);
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        //eflyOffline.newPNR.verifyTicketingInformationMessageDisplayed();
        String pnr = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
    }

    @Test
    public void ARD4_TST_14652_14627_completeBookingUsingCurrencyAUDWithAmericanExpress() {
        HashMap testData = new DataUtility().testData("4");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        //eflyOffline.flightSearch.verifyItinerarySection(selectedFlightDetails,testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        HashMap<String,String>passengerNames = eflyOffline.passengerContactInfo.retrievePassengerNames(testData);
        eflyOffline.flightSearch.verifyPassengerDetailsAfterAddingPassengers(noOfPassengers,passengerNames);
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        //eflyOffline.newPNR.verifyTicketingInformationMessageDisplayed();
        String pnr = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
    }

    @Test
    public void ARD5_TST_14655_14619_completeBookingUsingCurrencyUSDWithUATP() {
        HashMap testData = new DataUtility().testData("5");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        //eflyOffline.flightSearch.verifyItinerarySection(selectedFlightDetails,testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        HashMap<String,String>passengerNames = eflyOffline.passengerContactInfo.retrievePassengerNames(testData);
        eflyOffline.flightSearch.verifyPassengerDetailsAfterAddingPassengers(noOfPassengers,passengerNames);
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        //eflyOffline.newPNR.verifyTicketingInformationMessageDisplayed();
        String pnr = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
    }

    @Test
    public void ARD6_TST_14664_14628_completeBookingUsingCurrencyCADWithMasterCard() {
        HashMap testData = new DataUtility().testData("6");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        //eflyOffline.flightSearch.verifyItinerarySection(selectedFlightDetails,testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        HashMap<String,String>passengerNames = eflyOffline.passengerContactInfo.retrievePassengerNames(testData);
        eflyOffline.flightSearch.verifyPassengerDetailsAfterAddingPassengers(noOfPassengers,passengerNames);
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        //eflyOffline.newPNR.verifyTicketingInformationMessageDisplayed();
        String pnr = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
    }

    @Test
    public void ARD7_TST_14630_completeBookingUsingCurrencyJPYWithInvoice(){
        HashMap testData = new DataUtility().testData("7");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        //eflyOffline.flightSearch.verifyItinerarySection(selectedFlightDetails,testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        HashMap<String,String>passengerNames = eflyOffline.passengerContactInfo.retrievePassengerNames(testData);
        eflyOffline.flightSearch.verifyPassengerDetailsAfterAddingPassengers(noOfPassengers,passengerNames);
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        //eflyOffline.newPNR.verifyTicketingInformationMessageDisplayed();
        String pnr = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.findBooking.navigateToIdentificationPage();
        eflyOffline.findBooking.checkinThroughIdentificationPage(passengerNames, pnr);
        eflyOffline.findBooking.continueToJourneyDetailsPage(1,testData);
        eflyOffline.journeyDetails.navigateToPassengerDetailsPage();
        //eflyOffline.boardingPass.printBoardingPass(testData, pnr, noOfPassengers); fixme
        eflyOffline.boardingPass.continueToCompleteCheckin();
        eflyOffline.checkicomplete.completeCheckin();
    }

    @Test
    public void ARD8_TST_14629_14663_completeBookingUsingCurrencyHKDWithVisa() {
        HashMap testData = new DataUtility().testData("8");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        //eflyOffline.flightSearch.verifyItinerarySection(selectedFlightDetails,testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        HashMap<String,String>passengerNames = eflyOffline.passengerContactInfo.retrievePassengerNames(testData);
        eflyOffline.flightSearch.verifyPassengerDetailsAfterAddingPassengers(noOfPassengers,passengerNames);
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        //eflyOffline.newPNR.verifyTicketingInformationMessageDisplayed();
        String pnr = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
    }

}
