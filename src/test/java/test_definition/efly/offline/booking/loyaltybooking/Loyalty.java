package test_definition.efly.offline.booking.loyaltybooking;

import PageBase.EFlyOfflineBase;
import org.testng.annotations.Test;
import utilities.DataUtility;
import utilities.ReportUtility;

import java.util.HashMap;

public class Loyalty {


    @Test
    public void web_Web__Flex_UKtoUK_T_S_ADT_Teen_CHD_INF_Oneway_Direct_GBP_noAncillaries_MB_Visa(){
        HashMap<String,String> testData = new DataUtility().testData("106");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        eflyOffline.newPNR.closePNRTab();
        eflyOffline.newPNR.searchPNR(PNR);
        eflyOffline.newPNR.navigateToModifyFlights();
        eflyOffline.modifyFlight.modifyRoute(testData);
        eflyOffline.newPNR.navigateToRepriceAutomatically();
        eflyOffline.reprice.reprice(testData);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_Web__Flex_UK to UK_T_S_ADT_Teen_CHD_INF_Oneway_Direct_GBP_ no ancillaries_MB_Visa",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2")+","+etkt.get("passenger3")+","+etkt.get("passenger4"));
    }


    @Test
    public void web_Staffbooking_Getmore_nonUKtoUK_M_Q_ADT_Teen_CHD_oneway_Direct_Euro_ancillaries_MB_Visa(){
        HashMap<String,String> testData = new DataUtility().testData("105");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        eflyOffline.newPNR.closePNRTab();
        eflyOffline.newPNR.searchPNR(PNR);
        eflyOffline.newPNR.navigateToModifyFlights();
        eflyOffline.modifyFlight.modifyRoute(testData);
        eflyOffline.newPNR.navigateToRepriceAutomatically();
        eflyOffline.reprice.reprice(testData);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_Staffbooking__Getmore_ non UK to UK_M_Q_ADT_Teen_CHD_oneway_Direct_Euro_ancillaries_MB_paypal",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2")+","+etkt.get("passenger3"));
    }


    @Test
    public void web_Web_Getmore_AF_Y_WN_Teen_Return_1sector_GBP_Noancillaries_MB_Visa(){
        HashMap<String,String> testData = new DataUtility().testData("104");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData,"code share");
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        eflyOffline.newPNR.closePNRTab();
        eflyOffline.newPNR.searchPNR(PNR);
        eflyOffline.newPNR.navigateToModifyFlights();
        travel_date = eflyOffline.modifyFlight.modifyDate(testData,travel_date);
        eflyOffline.newPNR.navigateToRepriceAutomatically();
        eflyOffline.reprice.reprice(testData);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_Web__Get more_AF_Y_WN_Teen_Return_1 sector_GBP_ancillaries_MB_Visa",
                PNR,testData.get("teenName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1"));
    }


    @Test
    public void web_Web_Getmore_nonUKtoUK_U_Q_ADT_INF_CHD_oneway_Direct_CHF_ancillaries_MB_Diner(){
        HashMap<String,String> testData = new DataUtility().testData("103");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        eflyOffline.newPNR.closePNRTab();
        eflyOffline.newPNR.searchPNR(PNR);
        eflyOffline.newPNR.navigateToModifyFlights();
        eflyOffline.modifyFlight.modifyRoute(testData);
        eflyOffline.newPNR.navigateToRepriceAutomatically();
        eflyOffline.reprice.reprice(testData);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_Web__Getmore_ non UK to UK_U_Q_ADT_INF_CHD_oneway_Direct_CHF_ancillaries_MB_Diner",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2")+","+etkt.get("passenger3"));
    }


    @Test
    public void web_Web__Justfly_UKtoUK_U_P_2ADT_oneway_2sector_Euro_ancillaries_MB_Mastercard(){
        HashMap<String,String> testData = new DataUtility().testData("102");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        eflyOffline.newPNR.closePNRTab();
        eflyOffline.newPNR.searchPNR(PNR);
        eflyOffline.newPNR.navigateToModifyFlights();
        eflyOffline.modifyFlight.modifyDate(testData,travel_date);
        eflyOffline.newPNR.navigateToRepriceAutomatically();
        eflyOffline.reprice.reprice(testData);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_Web__Justfly_ UK to UK_U_P_2ADT_oneway_2sector_Euro_ancillaries_MB_Mastercard",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2"));
    }


    @Test
    public void web_ARD_Getmore_EA_O_N_ADT_Specialassistance_oneway_4sector_Euro_ancillaries_MB_Visa(){ //need route
        HashMap<String,String> testData = new DataUtility().testData("100");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Passenger Assistance");
        eflyOffline.service.selectService("Wheelchair");
        eflyOffline.service.addSpecialAssistance("WCHS");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        eflyOffline.newPNR.closePNRTab();
        eflyOffline.newPNR.searchPNR(PNR);
        eflyOffline.newPNR.navigateToModifyFlights();
        eflyOffline.modifyFlight.modifyRoute(testData);
        eflyOffline.newPNR.navigateToRepriceAutomatically();
        eflyOffline.reprice.reprice(testData);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_ARD__Getmore_EA_O_N_ADT_Special ass_oneway_4sector_Euro_ ancillaries_MB_Paypal",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1"));
    }


    @Test
    public void web_ARD_justfly_nonUKtoUK_H_J_ADT_Specialassistance_return_2sector_CHF_ancillaries_MB_Visa(){
        HashMap<String,String> testData = new DataUtility().testData("99");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Passenger Assistance");
        eflyOffline.service.selectService("Wheelchair");
        eflyOffline.service.addSpecialAssistance("WCHC");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        eflyOffline.newPNR.closePNRTab();
        eflyOffline.newPNR.searchPNR(PNR);
        eflyOffline.newPNR.navigateToModifyFlights();
        eflyOffline.modifyFlight.modifyDate(testData,travel_date);
        eflyOffline.newPNR.navigateToRepriceAutomatically();
        eflyOffline.reprice.reprice(testData);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_ARD__justfly_non UK to UK_H_J_ADT_Special ass_return_2sector_CHF_ ancillaries_MB_Paypal",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1"));
    }


    @Test
    public void web_Web_justfly_EA_K_J_ADT_Teen_Oneway_Direct_Euro_ancillaries_MB_Visa(){ //need route
        HashMap<String,String> testData = new DataUtility().testData("98");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        eflyOffline.newPNR.closePNRTab();
        eflyOffline.newPNR.searchPNR(PNR);
        eflyOffline.newPNR.navigateToModifyFlights();
        eflyOffline.modifyFlight.modifyRoute(testData);
        eflyOffline.newPNR.navigateToRepriceAutomatically();
        eflyOffline.reprice.reprice(testData);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_Web__justfly_EA_K_J_ADT_Teen_Oneway_Direct_Euro_ ancillaries_MB_Visa",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1"));
    }


    @Test
    public void web_MyIDtravel_Justfly_UktoUK_O_J_ADT_Return_4sector_GBP_Ancillaries_MB_Visa(){ //need route
        HashMap<String,String> testData = new DataUtility().testData("97");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        eflyOffline.newPNR.closePNRTab();
        eflyOffline.newPNR.searchPNR(PNR);
        eflyOffline.newPNR.navigateToModifyFlights();
        eflyOffline.modifyFlight.modifyDate(testData,travel_date);
        eflyOffline.newPNR.navigateToRepriceAutomatically();
        eflyOffline.reprice.reprice(testData);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEb_My ID travel_Justfly_Uk to UK_O_J_ADT_Return_4 sector_GBP_Ancillaries_MB_Diners",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1"));
    }


    @Test
    public void gds_Interline_Justfly_UKtoUK_U_GJ_ADT_CHD_Oneway_3sector_GBP_NoAncillaries_MB_Mastercard(){
        HashMap<String,String> testData = new DataUtility().testData("96");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        eflyOffline.newPNR.closePNRTab();
        eflyOffline.newPNR.searchPNR(PNR);
        eflyOffline.newPNR.navigateToModifyFlights();
        eflyOffline.modifyFlight.modifyDate(testData,travel_date);
        eflyOffline.newPNR.navigateToRepriceAutomatically();
        eflyOffline.reprice.reprice(testData);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "GDS_Interline_Justfly_UK to UK_U_GJ_ADT_CHD_Oneway_3 sector_GBP_No Ancillaries_MB_Mastercard",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2"));
    }


    @Test
    public void gds_Interline_Justfly_OtherAirline_V_GJ_ADT_Oneway_3sector_GBP_Ancillaries_MB_Visa(){
        HashMap<String,String> testData = new DataUtility().testData("95");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        eflyOffline.newPNR.closePNRTab();
        eflyOffline.newPNR.searchPNR(PNR);
        eflyOffline.newPNR.navigateToModifyFlights();
        travel_date = eflyOffline.modifyFlight.modifyDate(testData,travel_date);
        eflyOffline.newPNR.navigateToRepriceAutomatically();
        eflyOffline.reprice.reprice(testData);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "GDS_Interline_Justfly_Other Airline_V_GJ_ADT_Oneway_3 sector_GBP_Ancillaries_MB_UATP",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1"));
    }


    @Test
    public void gds_ARDCryptic_Justfly_UKtoUK_Y_GJ_2ADT_Return_Direct_GBP_NoAncillaries_MB_Visa(){
        HashMap<String,String> testData = new DataUtility().testData("94");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        eflyOffline.newPNR.closePNRTab();
        eflyOffline.newPNR.searchPNR(PNR);
        eflyOffline.newPNR.navigateToModifyFlights();
        eflyOffline.modifyFlight.modifyRoute(testData);
        eflyOffline.newPNR.navigateToRepriceAutomatically();
        eflyOffline.reprice.reprice(testData);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "GDS_ARD Cryptic_Justfly_UK to UK_Y_GJ_2 ADT_Return_Direct_GBP_No Ancillaries_MB_Diners",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2"));
    }


    @Test
    public void gds_ARD_Getmore_Codeshare_L_GB_ADT_CHD_Oneway_2sector_EUR_NoAncillaries_MB_Visa(){
        HashMap<String,String> testData = new DataUtility().testData("93");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData,"code share");
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        eflyOffline.newPNR.closePNRTab();
        eflyOffline.newPNR.searchPNR(PNR);
        eflyOffline.newPNR.navigateToModifyFlights();
        eflyOffline.modifyFlight.modifyDate(testData,travel_date);
        eflyOffline.newPNR.navigateToRepriceAutomatically();
        eflyOffline.reprice.reprice(testData);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "GDS_ARD_Getmore_Codeshare_L_GB_ADT_CHD_Oneway_2 sector_EUR_No Ancillaries_MB_Diners",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2"));
    }


    @Test
    public void gds_ARD_Getmore_NonUKtoUK_B_GB_ADT_CHD_Oneway_2Sector_CHF_Ancillaries_MB_Visa(){
        HashMap<String,String> testData = new DataUtility().testData("92");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        eflyOffline.newPNR.closePNRTab();
        eflyOffline.newPNR.searchPNR(PNR);
        eflyOffline.newPNR.navigateToModifyFlights();
        eflyOffline.modifyFlight.modifyDate(testData,travel_date);
        eflyOffline.newPNR.navigateToRepriceAutomatically();
        eflyOffline.reprice.reprice(testData);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "GDS_ARD_Getmore_Non UK to UK_B_GB_ADT_CHD_Oneway_2 Sector_CHF_Ancillaries_MB_Diners",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2"));
    }


    @Test
    public void gds_ARD_Getmore_UKtoUK_Y_GB_ADT_Return_2sector_GBP_Ancillaries_MB_Visa(){
        HashMap<String,String> testData = new DataUtility().testData("91");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        eflyOffline.newPNR.closePNRTab();
        eflyOffline.newPNR.searchPNR(PNR);
        eflyOffline.newPNR.navigateToModifyFlights();
        travel_date = eflyOffline.modifyFlight.modifyDate(testData,travel_date);
        eflyOffline.newPNR.navigateToRepriceAutomatically();
        eflyOffline.reprice.reprice(testData);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "GDS_ARD_Getmore_UK to UK_Y_GB_ADT_Return_2 sector_GBP_ ancillaries_MB_Visa",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1"));
    }


    @Test
    public void web_ARD__Flex_UktoNonUK_B_FX_ADT_return_2sector_GBP_ancillaries_MB_Visa(){
        HashMap<String,String> testData = new DataUtility().testData("90");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        eflyOffline.newPNR.closePNRTab();
        eflyOffline.newPNR.searchPNR(PNR);
        eflyOffline.newPNR.navigateToModifyFlights();
        travel_date = eflyOffline.modifyFlight.modifyDate(testData,travel_date);
        eflyOffline.newPNR.navigateToRepriceAutomatically();
        eflyOffline.reprice.reprice(testData);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_ARD__Flex_Uk to non UK_B_FX_ADT_retrun_2sector_GBP_ancillaries_MB_Visa",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1"));
    }

    @Test
    public void rule109_Web_Web_Getmore_NonUKtoUK_U_Q_ADT_CHD_INF_Oneway_1Sector_CHF_Ancillaries_MB(){
        HashMap<String,String> testData = new DataUtility().testData("127");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        eflyOffline.newPNR.closePNRTab();
        eflyOffline.newPNR.searchPNR(PNR);
        eflyOffline.newPNR.navigateToModifyFlights();
        travel_date = eflyOffline.modifyFlight.modifyDate(testData,travel_date);
        eflyOffline.newPNR.navigateToRepriceAutomatically();
        eflyOffline.reprice.reprice(testData);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "rule109_Web_Web_Getmore_NonUKtoUK_U_Q_ADT_CHD_INF_Oneway_1Sector_CHF_Ancillaries_MB",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber1")+","+testData.get("frequentFlyerNumber2"),
                travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2")+","+etkt.get("passenger3"));
    }

    @Test
    public void rule136_Web_Web_Getmore_AF_Y_WN_TEEN_Return_1Sector_GBP_noAncillaries_MB(){
        HashMap<String,String> testData = new DataUtility().testData("128");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData,"code share");
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        eflyOffline.newPNR.closePNRTab();
        eflyOffline.newPNR.searchPNR(PNR);
        eflyOffline.newPNR.navigateToModifyFlights();
        travel_date = eflyOffline.modifyFlight.modifyDate(testData,travel_date);
        eflyOffline.newPNR.navigateToRepriceAutomatically();
        eflyOffline.reprice.reprice(testData);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "rule136_Web_Web_Getmore_AF_Y_WN_TEEN_Return_1Sector_GBP_Ancillaries_MB",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber1"),
                travel_date.get("date1"), travel_date.get("date2"),etkt.get("passenger1"));
    }


    //------------------------------Booking-----------------------------------------------------------------------------

    @Test
    public void web_WEb_Flex_UktoUK_V_FX_ADT_Return_Direct_GBP_ancillaries_Prime_Visa(){
        HashMap<String,String> testData = new DataUtility().testData("132");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_WEb_Flex_UktoUK_V_FX_ADT_Return_Direct_GBP_ancillaries_Prime_Visa",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber1")+","+testData.get("frequentFlyerNumber2"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2"));
    }

    @Test
    public void WEB_web_Flex_UKtoUK_Y_FX_ADT_CHD_oneway_direct_GBP_noAnciallries_Prime_mastercard(){
        HashMap<String,String> testData = new DataUtility().testData("133");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_web_Flex_UK to UK_Y_FX_ADT_CHD_oneway_direct_GBP_no anciallries_Prime_mastercard",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber1")+","+testData.get("frequentFlyerNumber2"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2"));
    }



    @Test
    public void rule106_Web_ARD_Getmore_BE_O_Q_ADT_INF_Oneway_Direct_EUR_NoAncillaries_PrimeBooking(){
        HashMap<String,String> testData = new DataUtility().testData("126");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "rule106_Web_ARD_Getmore_BE_O_Q_ADT_INF_Oneway_Direct_EUR_NoAncillaries_PrimeBooking",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber1")+","+testData.get("frequentFlyerNumber2"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2"));
    }


    @Test
    public void rule104_Web_Web_Getmore_BE_B_Q_ADT_CHD_Oneway_Direct_GBP_NoAncillaries_PrimeBooking(){
        HashMap<String,String> testData = new DataUtility().testData("125");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "rule104_Web_Web_Getmore_BE_B_Q_ADT_CHD_Oneway_Direct_GBP_NoAncillaries_PrimeBooking",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber1")+","+testData.get("frequentFlyerNumber2"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2"));
    }


    @Test
    public void rule102_Web_Web_BE_Y_Q_ADT_Return_Direct_GBP_NoAncillaries_PrimeBooking(){
        HashMap<String,String> testData = new DataUtility().testData("124");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "rule102_Web_Web_BE_Y_Q_ADT_Return_Direct_GBP_NoAncillaries_PrimeBooking",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber1"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1"));
    }


    @Test
    public void rule89_web_ARD_Getmore_BE_M_N_ADT_Oneway_Direct_EUR_NoAncillaries_PrimeBooking(){
        HashMap<String,String> testData = new DataUtility().testData("123");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "web_ARD_Getmore_BE_M_N_ADT_Oneway_Direct_EUR_NoAncillaries_PrimeBooking",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber1"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1"));
    }


    @Test
    public void rule82_web_web_Getmore_UKtoNonUK_B_N_ADT_Return_Direct_EUR_Ancillaries_PrimeBooking(){
        HashMap<String,String> testData = new DataUtility().testData("122");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "web_web_Getmore_UKtoNonUK_B_N_ADT_Return_Direct_EUR_Ancillaries_PrimeBooking_Mastercard",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber1"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1"));
    }

    @Test
    public void rule80_web_web_Getmore_BE_Y_N_ADT_Oneway_Direct_GBP_Ancillaries_PrimeBooking(){
        HashMap<String,String> testData = new DataUtility().testData("121");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "web_web_Getmore_BE_ADT_Oneway_Direct_GBP_Ancillaries_PrimeBooking_Visa",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber1"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1"));
    }


    @Test
    public void web_Web_Flex_UKtoUK_O_X_ADT_Teen_CHD_INF_Oneway_Direct_GBP_noAncillaries_Prime_Visa(){
        HashMap<String,String> testData = new DataUtility().testData("119");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_Web__Flex_UK to UK_O_X_ADT_Teen_CHD_INF_Oneway_Direct_GBP_ no ancillaries_Prime_Visa",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2"));
    }


    @Test
    public void WEB_Web_Justfly_UKtoUK_U_WW_ADT_CHD_Oneway_Direct_GBP_noAncillaries_Prime_Visa(){
        HashMap<String,String> testData = new DataUtility().testData("118");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_Web__Justfly_UK to UK_U_WW_ADT_CHD_Oneway_Direct_GBP_ no ancillaries_Prime_Visa",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2"));
    }


    @Test
    public void web_Web_Justfly_UKtoUK_O_WW_ADT_TEEN_CHD_INF_Oneway_Direct_GBP_noAncillaries_Prime_VOucher(){
        HashMap<String,String> testData = new DataUtility().testData("117");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_Web__Justfly_UK to UK_O_WW_ADT_TEEN_CHD_INF_Oneway_Direct_GBP_ no ancillaries_Prime_VOucher",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2")+","+etkt.get("passenger3")+","+etkt.get("passenger4"));
    }


    @Test
    public void web_web_Justfly_UKtoUK_L_P_ADT_TEEN_CHD_oneway_direct_GBP_noAnciallries_Prime_Visa(){
        HashMap<String,String> testData = new DataUtility().testData("116");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_web_Justfly_UK to UK_L_P_ADT_TEEN_CHD_oneway_direct_GBP_no anciallries_Prime_Visa",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2")+","+etkt.get("passenger3"));
    }


    @Test
    public void web_web_Justfly_UKtoUK_B_P_ADT_INF_oneway_direct_GBP_anciallries_Prime_Visa(){
        HashMap<String,String> testData = new DataUtility().testData("115");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_web_Justfly_UK to UK_B_P_ADT_INF_oneway_direct_GBP_anciallries_Prime_Visa",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2"));
    }

    @Test
    public void web_web_Justfly_UKtoUK_T_P_ADT_CHD_oneway_direct_GBP_anciallries_Prime_Diner(){
        HashMap<String,String> testData = new DataUtility().testData("114");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_web_Justfly_UK to UK_T_P_ADT_ CHD_oneway_direct_GBP_anciallries_Prime_Diner",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2"));
    }


    @Test
    public void web_web_Justfly_UKtoUK_M_J_ADT_oneway_direct_GBP_anciallries_Prime_Mastercard(){
        HashMap<String,String> testData = new DataUtility().testData("113");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_web_Justfly_UK to UK_M_J_ADT_ oneway_direct_GBP_anciallries_Prime_Mastercard",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1"));
    }


    @Test
    public void web_Web_Flex_UKtoUK_L_FX_ADT_TEEN_Oneway_Direct_GBP_ancillaries_Prime_Visa(){
        HashMap<String,String> testData = new DataUtility().testData("112");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_Web__Flex_UK to UK_L_FX_ADT_TEEN_Oneway_Direct_GBP_ancillaries_Prime_Visa",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2"));
    }

    @Test
    public void web_web_justfly_BE_U_FX_ADT_CHD_Return_Direct_GBP_Ancillaries_PrimeBooking_Visa(){
        HashMap<String,String> testData = new DataUtility().testData("111");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "web_web_justfly_BE_U_FX_ADT_CHD_Return_Direct_GBP_Ancillaries_PrimeBooking_Mastercard",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2"));
    }

    @Test
    public void web_Web_Flex_nonUKtoUK_M_FX_ADT_INF_Oneway_Direct_Euro_ancillaries_Prime_AMEX(){
        HashMap<String,String> testData = new DataUtility().testData("110");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_Web__Flex_non UK to UK_M_FX_ADT_INF_Oneway_Direct_Euro_ancillaries_Prime_AMEX",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2"));
    }

    @Test
    public void web_web_flex_BE_V_FX_ADT_Return_Direct_GBP_Ancillaries_PrimeBooking_UATP(){
        HashMap<String,String> testData = new DataUtility().testData("109");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "web_web_flex_BE_V_FX_ADT_Return_Direct_GBP_Ancillaries_PrimeBooking_UATP",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1"));
    }

    @Test
    public void web_web_flex_BE_S_FX_ADT_Return_Direct_GBP_Ancillaries_Primebooking_UATP(){
        HashMap<String,String> testData = new DataUtility().testData("108");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "web_web_flex_BE_S_FX_ADT_Return_Direct_GBP_Ancillaries_Primebooking_UATP",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1"));
    }

    @Test
    public void web_web_flex_BE_T_FX_ADT_OneWay_DIRECT_GBP_NoAncillaries_PrimeBooking_Diner(){
        HashMap<String,String> testData = new DataUtility().testData("107");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "web_web_flex_BE_T_FX_ADT_OneWay_DIRECT_GBP_NoAncillaries_PrimeBooking_Diner",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1"));
    }

    @Test
    public void web_ARD_Flex_EA_S_S_ADT_Speicalassis_oneway_2sector_GBP_Ancillaries_Prime_Diner(){
        HashMap<String,String> testData = new DataUtility().testData("132");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "web_ARD_Flex_EA_S_S_ADT_Speicalassis_oneway_2sector_GBP_Ancillaries_Prime_Diner",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1"));
    }


    @Test
    public void web_Staff_booking_Justfly_UktononUK_B_J_ADT_Return_Direct_GBP_Ancillaries_Prime_Visa(){
        HashMap<String,String> testData = new DataUtility().testData("89");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEb_Staff_booking_Justfly_Uk to non UK_B_J_ADT_Return_Direct_GBP_Ancillaries_Prime_Visa",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1"));
    }

    @Test
    public void gds_ARDCryptic_Justfly_UKtoUK_L_GJ_Teen_oneway_Direct_GBP_NoAncillaries_Prime_Mastercard(){
        HashMap<String,String> testData = new DataUtility().testData("51");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "GDS_ARD Cryptic_Justfly_UK to UK_L_GJ_Teen_oneway_Direct_GBP_ No ancillaries_Prime_Mastercard",
                PNR,testData.get("teenName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1"));
    }


    @Test
    public void gds_ARD_Justfly_UKtoNonUK_H_GJ_ADT_INF_Return_2Sector_CHF_Ancillaries_Prime_Amex(){
        HashMap<String,String> testData = new DataUtility().testData("64");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "GDS_ARD_Justfly_UK to Non UK_H_GJ_ADT_INF_Return_2 Sector_CHF_Ancillaries_Prime_Amex",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2"));
    }


    @Test
    public void gds_ARD_Justfly_UKtoUK_K_GJ_ADT_Teen_Return_Direct_EUR_Ancillaries_Prime_Visa(){
        HashMap<String,String> testData = new DataUtility().testData("63");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData,"gds");
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "GDS_ARD_Justfly_UK to UK_K_GJ_ADT_Teen_Return_Direct_EUR_Ancillaries_Prime_Visa",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2"));
    }



    @Test
    public void gds_ARD_Justfly_UKtoNonUK_T_GJ_ADT_Teen_CHD_INF_Oneway_2Sector_GBP_NoAncillaries_Prime_Paypal(){
        HashMap<String,String> testData = new DataUtility().testData("52");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData,"gds");
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "GDS_ARD_Justfly_UK to Non UK_T_GJ_ADT_Teen_CHD_INF_Oneway_2 Sector_GBP_No Ancillaries_Prime_Paypal",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2")+etkt.get("passenger3")+","+etkt.get("passenger4"));
    }

    @Test
    public void gds_ARD_Justfly_UKtoUK_B_GJ_ADT_Teen_CHD_Oneway_Direct_GBP_NoAncillaries_Prime_Visa(){
        HashMap<String,String> testData = new DataUtility().testData("129");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData,"gds");
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "GDS_ARD_Justfly_UK to UK_B_GJ_ADT_Teen_CHD_Oneway_Direct_GBP_No Ancillaries_Prime_Visa",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber1")+","
                        +testData.get("frequentFlyerNumber2")+","+testData.get("frequentFlyerNumber3"),
                travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2")+etkt.get("passenger3"));
    }

    @Test
    public void gds_ARDCryptic_Getmore_UKtoUK_H_GB_2ADT_Return_Direct_MCP_Ancillaries_Prime_Visa(){
        HashMap<String,String> testData = new DataUtility().testData("60");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "GDS_ARD Cryptic_Getmore_UK to UK_H_GB_2 ADT_Return_Direct_MCP_Ancillaries_Prime_Visa",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2"));
    }

    @Test
    public void gds_otherGDS_Getmore_NonUKtoUK_B_GB_ADT_CHD_Oneway_2Sector_CHF_Ancillaries_Prime_Diners(){
        HashMap<String,String> testData = new DataUtility().testData("131");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.addPTCcode(testData,"gds");
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "GDS_other GDS_Getmore_Non UK to UK_B_GB_ADT_CHD_Oneway_2 Sector_CHF_Ancillaries_Prime_Diners",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber1")
                +","+testData.get("frequentFlyerNumber2"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2"));
    }


    @Test
    public void gds_ARD_Getmore_UKtoUK_K_GB_ADT_Oneway_3Sector_GBP_Ancillaries_Prime_Mastercard(){
        HashMap<String,String> testData = new DataUtility().testData("58");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "GDS_ARD_Getmore_UK to UK_K_GB_ADT_Oneway_3 Sector_GBP_Ancillaries_Prime_Mastercard",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1"));
    }

    @Test
    public void gds_ARDCryptic_Getmore_NonUKtoUK_T_GB_ADT_Teen_Oneway_Direct_EUR_NoAncillaries_Prime_Mastercard(){
        HashMap<String,String> testData = new DataUtility().testData("53");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "GDS_ARDCryptic_Getmore_Non UK to UK_T_GB_ADT_Teen_Oneway_Direct_EUR_No Ancillaries_Prime_Mastercard",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2"));
    }

    @Test
    public void gds_ARD_Allin_AF_W_GA_ADT_Teen_CHD_INF_Oneway_Direct_GBP_noAncillaries_Prime_Visa(){
        HashMap<String,String> testData = new DataUtility().testData("55");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.addPTCcode(testData,"gds");
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "GDS_ARD_Allin_Hop/AF_W_GA_ADT_Teen_CHD_INF_Oneway_Direct_GBP_ no ancillaries_Prime_Visa",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2")+","+etkt.get("passenger3")+","+etkt.get("passenger4"));
    }

    @Test
    public void web_pribas_Flex_UKtoUK_Y_FX_ADT_Teen_oneway_direct_GBP_noAnciallries_Prime_mastercard(){
        HashMap<String,String> testData = new DataUtility().testData("66");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_pribas_Flex_UK to UK_Y_FX_ADT_Teen_oneway_direct_GBP_no anciallries_Prime_mastercard",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2"));
    }

    @Test
    public void web_web_Flex_UKtoUK_Y_FX_ADT_Teen_oneway_direct_GBP_noAnciallries_Prime_mastercard(){
        HashMap<String,String> testData = new DataUtility().testData("88");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_web_Flex_UK to UK_Y_FX_ADT_Teen_oneway_direct_GBP_no anciallries_Prime_mastercard",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2"));
    }

    @Test
    public void web_ARDCryptic__Flex_NonUktoUK_O_FX_ADT_CHD_oneway_3sector_Euro_ancillaries_Prime_Diner(){
        HashMap<String,String> testData = new DataUtility().testData("87");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_ARDCryptic__Flex_Non Uk to UK_O_FX_ADT_CHD_oneway_3sector_Euro_ancillaries_Prime_Diner",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2"));
    }


    @Test //unable to find route
    public void gds_Interline_Getmore_OtherAirline_S_GB_ADT_INF_Return_Direct_GBP_NoAncillaries_Prime_Amex(){
        HashMap<String,String> testData = new DataUtility().testData("86");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "GDS_Interline_Getmore_Other Airline_S_GB_ADT_INF_Return_Direct_GBP_No Ancillaries_Prime_Amex",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2"));
    }


    @Test
    public void gds_ARD_Getmore_UKtoNonUK_U_GB_ADT_Teen_CHD_INF_Return_Direct_GBP_Ancillaries_Prime_Visa(){
        HashMap<String,String> testData = new DataUtility().testData("130");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData,"gds");
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "GDS_ARD_Getmore_UKtoNonUK_U_GB_ADT_Teen_CHD_INF_Return_Direct_GBP_Ancillaries_Prime_Visa",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2")+","+etkt.get("passenger3")+","+etkt.get("passenger4"));
    }


    @Test
    public void gds_OtherGDS_Justfly_NonUKtoUK_S_GJ_ADT_Return_Direct_EUR_Ancillaries_Prime_Amex(){
        HashMap<String,String> testData = new DataUtility().testData("85");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "GDS_OtherGDS_Justfly_Non UK to UK_S_GJ_ADT_Return_Direct_EUR_Ancillaries_Prime_Amex",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1"));
    }


    @Test
    public void web_ARDcryptic_allin_UKtoUK_W_L_2ADT_2CHD_INF_return_1sector_GBP_ancillaries_Prime_Mastercard(){
        HashMap<String,String> testData = new DataUtility().testData("84");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_ARDcryptic__all in_ UK to UK_W_L_2ADT_2CHD_INF_return_1sector_GBP_ ancillaries_Prime_Mastercard",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2")+","+etkt.get("passenger3")
                        +","+etkt.get("passenger4")+","+etkt.get("passenger5"));
    }


    @Test
    public void web_ARDcryptic_Getmore_nonUKtoUK_V_N_ADT_return_2sector_CHF_NoAncillaries_Prime_Voucher(){
        HashMap<String,String> testData = new DataUtility().testData("83");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_ARDcryptic__Getmore_ non UK to UK_V_N_ADT_return_2sector_CHF_ No ancillaries_Prime_Voucher",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1"));
    }


    @Test
    public void web_Staffbooking_Getmore_nonUKtoUK_U_N_2ADT_return_2sector_Euro_noAncillaries_Prime_Mastercard(){
        HashMap<String,String> testData = new DataUtility().testData("82");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_Staff booking__Getmore_ non UK to UK_U_N_2ADT_return_2 sector_Euro_no ancillaries_Prime_Mastercard",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2"));
    }


    @Test
    public void web_MyTravelID_Justfly_nonUKtoUK_Y_P_ADT_oneway_Direct_GBP_ancillaries_Prime_Amex(){
        HashMap<String,String> testData = new DataUtility().testData("81");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_My Travel ID__Justfly_ non UK to UK_Y_P_ADT_oneway_Direct_GBP_ancillaries_Prime_Amex",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1"));
    }

    @Test
    public void web_Web_Justfly_UKtoUK_U_P_2ADT_oneway_3sector_Jpy_NOAncillaries_Prime_Visa(){
        HashMap<String,String> testData = new DataUtility().testData("80");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_Web_Justfly_UKtoUK_U_P_2ADT_oneway_3sector_Jpy_NOAncillaries_Prime_Visa",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1"));
    }


    @Test
    public void web_Pribas_Getmore_Vrigin_H_Q_ADT_Teen_INF_return_3sector_GBP_noAncillaries_Prime_Amex(){
        HashMap<String,String> testData = new DataUtility().testData("79");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_Pribas__Getmore_ Vrigin_H_Q_ADT_Teen_INF_return_3 sector_GBP_no ancillaries_Prime_Amex",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2")+","+etkt.get("passenger3"));
    }


    @Test
    public void web_MyIDtravel_Getmore_nonUKtoUK_L_Q_2ADT_2CHD_return_3sector_CHF_noAncillaries_Prime_Visa(){
        HashMap<String,String> testData = new DataUtility().testData("78");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_My ID travel__Getmore_ non UK to UK_L_Q_2ADT_2CHD_return_3 sector_CHF_no ancillaries_Prime_Visa",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2")+","+etkt.get("passenger3")+","+etkt.get("passenger4"));
    }

    @Test
    public void web_ARD_Flex_EA_S_S_ADT_SpeicalAssistance_oneway_2sector_GBP_Ancillaries_Prime_Diner(){
        HashMap<String,String> testData = new DataUtility().testData("77");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Passenger Assistance");
        eflyOffline.service.selectService("Wheelchair");
        eflyOffline.service.addSpecialAssistance("WCHR");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "Web_ARD_Flex_EA_S_S_ADT_Speical assis_return_2 sector_GBP_Ancillaries_Prime_Diner",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1"));
    }

    @Test
    public void web_Pribas_Getmore_nonUKtoUK_S_WN_ADT_Oneway_2sector_Euro_noAncillaries_Prime_Visa(){
        HashMap<String,String> testData = new DataUtility().testData("76");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_Pribas_Get more_non UK to UK_S_WN_ADT_Oneway_2 sector_Euro_no ancillaries_Prime_Visa",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1"));
    }


    @Test
    public void WEB_MyIDtravel_Getmore_nonUKtoUK_L_WN_ADT_CHD_Oneway_Direct_MCP_noAncillaries_Prime_Mastercard(){
        HashMap<String,String> testData = new DataUtility().testData("75");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_My ID travel__Getmore_ non UK to UK_L_WN_ADT_CHD_Oneway_Direct_MCP_no ancillaries_Prime_Mastercard",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2"));
    }


    @Test
    public void web_Web__Justfly_StobartAir_S_WW_ADT_SpecialAssistance_Oneway_Direct_GBP_noAncillaries_Prime_Amex(){
        HashMap<String,String> testData = new DataUtility().testData("74");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Passenger Assistance");
        eflyOffline.service.selectService("Wheelchair");
        eflyOffline.service.addSpecialAssistance("WCHR");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_Web__Justfly_Stobardt Air_S_WW_ADT_Special assistance_Oneway_Direct_GBP_ no ancillaries_Prime_Amex",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1"));
    }


    @Test
    public void web_Pribas_Flex_UKtoNonUK_V_X_2ADT_Return_1sector_MCP_noAncillaries_Prime_Diner(){
        HashMap<String,String> testData = new DataUtility().testData("72");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_Pribas__Flex_UK to non UK_V_X_2 ADT_Return_1 sector_MCP_no ancillaries_Prime_Diner",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2"));
    }

    @Test
    public void web_ARD_Justfly_nonUKtoUK_K_WW_ADT_Teen_INF_Return_3sector_GBP_ancillaries_Prime_Mastercard(){
        HashMap<String,String> testData = new DataUtility().testData("73");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_ARD_Justfly_non uk to UK_K_WW_ADT_Teen_INF_Return_3sector_GBP_ ancillaries_Prime_Mastercard",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2")+","+etkt.get("passenger3"));
    }

    @Test
    public void WEB_ARD_Justfly_nonUKtoUK_K_WW_ADT_Teen_INF_Return_3sector_GBP_ancillaries_Prime_Mastercard(){
        HashMap<String,String> testData = new DataUtility().testData("120");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData,noOfPassengers);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",
                "WEB_ARD_Justfly_non uk to UK_K_WW_ADT_Teen_INF_Return_3sector_GBP_ ancillaries_Prime_Mastercard",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),etkt.get("passenger1")+","+etkt.get("passenger2")+","+etkt.get("passenger2"));
    }

}
