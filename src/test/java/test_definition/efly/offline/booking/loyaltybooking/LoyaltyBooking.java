/*
package test_definition.efly.offline.booking.loyaltybooking;

import PageBase.EFlyOfflineBase;
import org.testng.annotations.Test;
import utilities.DataUtility;
import utilities.ReportUtility;

import java.util.HashMap;

public class LoyaltyBooking {

    ////------------------------WEB TCs----------------------------------------------------------------------------------------



    @Test
    public void web_ard_OneWay_Direct_2ADT_MasterCard_ManageBooking_NonUKtoUK(){
        HashMap<String,String> testData = new DataUtility().testData("71");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(noOfPassengers,"WEB",testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(9,"\\flybe_automation_report_PNRGeneration_","Sheet1","WEB_ARD_OneWay_Direct_2ADT_MasterCard_ManageBooking_NonUKtoUK",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),"-",testData.get("class_1")
                ,currency,"Bag",etkt.get("passenger1")+","+etkt.get("passenger2"));
    }


    @Test
    public void web_ard_OneWay_Direct_ADT_TEEN_CHD_MasterCard_ManageBooking_NonUKtoUK(){
        HashMap<String,String> testData = new DataUtility().testData("70");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(noOfPassengers,"WEB",testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");

        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(9,"\\flybe_automation_report_PNRGeneration_",
                "Sheet1","WEB_ARD_OneWay_Direct_ADT_TEEN_CHD_MasterCard_ManageBooking_NonUKtoUK",PNR,
                testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),
                travel_date.get("date1"),"-",testData.get("class_1"),currency,"Bag",etkt.get("passenger1")+","+etkt.get("passenger2")+","+etkt.get("passenger3"));
    }


    @Test
    public void web_ard_Return_2Sector_ADT_CHD_MasterCard_ManageBooking_NonUKtoUK(){
        HashMap<String,String> testData = new DataUtility().testData("69");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(noOfPassengers,"WEB",testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");

        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(9,"\\flybe_automation_report_PNRGeneration_","Sheet1","WEB_ARD_Return_2Sector_ADT_CHD_MasterCard_ManageBooking_NonUKtoUK",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),travel_date.get("date2"),
                testData.get("class_1"),currency,"Bag",etkt.get("passenger1")+","+etkt.get("passenger2"));
    }


    @Test
    public void web_ard_OneWay_Direct_ADT_TEEN_CHD_INF_VISA_ManageBooking_UKtoUK(){
        HashMap<String,String> testData = new DataUtility().testData("68");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(noOfPassengers,"WEB",testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");

        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(9,"\\flybe_automation_report_PNRGeneration_","Sheet1","WEB_ARD_OneWay_Direct_ADT_TEEN_CHD_INF_VISA_ManageBooking_UKtoUK",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),"-",
                testData.get("class_1"),currency,"Bag",etkt.get("passenger1")+","+etkt.get("passenger2")+etkt.get("passenger3")+","+etkt.get("passenger4"));
    }


    @Test
    public void web_ard_Return_2Sector_ADT_DinersClub_ManageBooking_UKtoUK(){
        HashMap<String,String> testData = new DataUtility().testData("67");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(noOfPassengers,"WEB",testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");

        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(9,"\\flybe_automation_report_PNRGeneration_","Sheet1","WEB_ARD_Return_2Sector_ADT_DinersClub_ManageBooking_UKtoUK",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),travel_date.get("date2"),
                testData.get("class_1"),currency,"Bag",etkt.get("passenger1"));
    }

    @Test
    public void web_ard_Return_Direct_ADT_INF_Visa_ManageBooking_BlueIsland(){
        HashMap<String,String> testData = new DataUtility().testData("66");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        //eflyOffline.flightSearch.verifyItinerarySection(selectedFlightDetails, testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        //      HashMap<String, String> passengerNames = eflyOffline.passengerContactInfo.retrievePassengerNames(testData);
//        eflyOffline.flightSearch.verifyPassengerDetailsAfterAddingPassengers(noOfPassengers, passengerNames);
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(noOfPassengers,"WEB",testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        */
/*eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        *//*

        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(9,"\\flybe_automation_report_PNRGeneration_","Sheet1","WEB_ARD_Return_Direct_ADT_INF_Visa_ManageBooking_BlueIsland",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),travel_date.get("date2"),
                testData.get("class_1"),currency,"-",etkt.get("passenger1")+","+etkt.get("passenger2"));
    }


    @Test
    public void web_ard_Return_2Sector_ADT_Visa_ManageBooking_UKtoNonUK(){
        HashMap<String,String> testData = new DataUtility().testData("65");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        //eflyOffline.flightSearch.verifyItinerarySection(selectedFlightDetails, testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        //      HashMap<String, String> passengerNames = eflyOffline.passengerContactInfo.retrievePassengerNames(testData);
//        eflyOffline.flightSearch.verifyPassengerDetailsAfterAddingPassengers(noOfPassengers, passengerNames);
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.addPTCcode(noOfPassengers,"WEB",testData);
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");

        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(9,"\\flybe_automation_report_PNRGeneration_","Sheet1","WEB_ARD_Return_2Sector_ADT_Visa_ManageBooking_UKtoNonUK",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),travel_date.get("date2"),
                testData.get("class_1"),currency,"Bag",etkt.get("passenger1"));
    }

    //////////---------------------------------------GDS TCs--------------------------------------------------------------------
    @Test
    public void gds_ard_Return_2Sector_ADT_Infant_Visa_PrimeBooking_UKtoNonUK(){
        HashMap<String,String> testData = new DataUtility().testData("64");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        //eflyOffline.flightSearch.verifyItinerarySection(selectedFlightDetails, testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        //      HashMap<String, String> passengerNames = eflyOffline.passengerContactInfo.retrievePassengerNames(testData);
//        eflyOffline.flightSearch.verifyPassengerDetailsAfterAddingPassengers(noOfPassengers, passengerNames);
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(9,"\\flybe_automation_report_PNRGeneration_","Sheet1","GDS_ARD_Return_Direct_ADT_Infant_Visa_PrimeBooking_UKtoNonUK",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),travel_date.get("date2"),
                testData.get("class_1"),currency,"Bag",etkt.get("passenger1")+","+etkt.get("passenger2"));
    }


    @Test
    public void gds_ard_Return_Direct_ADT_TEEN_Visa_PrimeBooking_UKtoUK(){
        HashMap<String,String> testData = new DataUtility().testData("63");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        //eflyOffline.flightSearch.verifyItinerarySection(selectedFlightDetails, testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        //      HashMap<String, String> passengerNames = eflyOffline.passengerContactInfo.retrievePassengerNames(testData);
//        eflyOffline.flightSearch.verifyPassengerDetailsAfterAddingPassengers(noOfPassengers, passengerNames);
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(9,"\\flybe_automation_report_PNRGeneration_","Sheet1","gds_ard_Return_Direct_ADT_TEEN_Visa_PrimeBooking_UKtoUK",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                travel_date.get("date2"),testData.get("class_1"),currency,"Bag",etkt.get("passenger1")+","+etkt.get("passenger2"));
    }

    @Test
    public void gds_ard_Return_Direct_ADT_AMEX_PrimeBooking_NonUKtoUK(){
        HashMap<String,String> testData = new DataUtility().testData("62");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        //eflyOffline.flightSearch.verifyItinerarySection(selectedFlightDetails, testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        //      HashMap<String, String> passengerNames = eflyOffline.passengerContactInfo.retrievePassengerNames(testData);
//        eflyOffline.flightSearch.verifyPassengerDetailsAfterAddingPassengers(noOfPassengers, passengerNames);
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(9,"\\flybe_automation_report_PNRGeneration_","Sheet1","GDS_ARD_Return_Direct_ADT_AMEX_PrimeBooking_NonUKtoUK",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),travel_date.get("date2"),
                testData.get("class_1"),currency,"Bag",etkt.get("passenger1"));
    }


    @Test
    public void gds_ard_Return_Direct_2ADT_Visa_ManageBooking_UKtoUK(){
        HashMap<String,String> testData = new DataUtility().testData("61");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        //eflyOffline.flightSearch.verifyItinerarySection(selectedFlightDetails, testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        //      HashMap<String, String> passengerNames = eflyOffline.passengerContactInfo.retrievePassengerNames(testData);
//        eflyOffline.flightSearch.verifyPassengerDetailsAfterAddingPassengers(noOfPassengers, passengerNames);
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);

        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(9,"\\flybe_automation_report_PNRGeneration_","Sheet1","GDS_ARD_Return_Direct_2ADT_Visa_ManageBooking_UKtoUK",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),travel_date.get("date2"),
                testData.get("class_1"),currency,"-",etkt.get("passenger1")+","+etkt.get("passenger2"));
    }

    @Test
        public void gds_ard_Return_Direct_2ADT_Visa_PrimeBooking_UKtoUK(){
        HashMap<String,String> testData = new DataUtility().testData("60");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        //eflyOffline.flightSearch.verifyItinerarySection(selectedFlightDetails, testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        //      HashMap<String, String> passengerNames = eflyOffline.passengerContactInfo.retrievePassengerNames(testData);
//        eflyOffline.flightSearch.verifyPassengerDetailsAfterAddingPassengers(noOfPassengers, passengerNames);
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(9,"\\flybe_automation_report_PNRGeneration_","Sheet1","GDS_ARD_Return_Direct_2ADT_Visa_PrimeBooking_UKtoUK",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),travel_date.get("date2"),
                testData.get("class_1"),currency,"Bag",etkt.get("passenger1")+","+etkt.get("passenger2"));
    }

    @Test
    public void gds_ard_Return_Direct_ADT_TEEN_CHD_INF_Visa_PrimeBooking_UKtoNonUK(){
        HashMap<String,String> testData = new DataUtility().testData("59");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        //eflyOffline.flightSearch.verifyItinerarySection(selectedFlightDetails, testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        //      HashMap<String, String> passengerNames = eflyOffline.passengerContactInfo.retrievePassengerNames(testData);
//        eflyOffline.flightSearch.verifyPassengerDetailsAfterAddingPassengers(noOfPassengers, passengerNames);
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(9,"\\flybe_automation_report_PNRGeneration_","Sheet1","GDS_ARD_Return_Direct_ADT_TEEN_CHD_INF_Visa_PrimeBooking_UKtoNonUK",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),travel_date.get("date2"),
                testData.get("class_1"),currency,"Bag",etkt.get("passenger1")+","+etkt.get("passenger2")+","+etkt.get("passenger3")+","+etkt.get("passenger4"));
    }


    @Test
    public void gds_ard_Oneway_2Sectors_ADT_MasterCard_PrimeBooking_UKtoUK() {
        HashMap<String,String> testData = new DataUtility().testData("58");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        */
/*HashMap<String, String> passengerNames = eflyOffline.passengerContactInfo.retrievePassengerNames(testData);
        eflyOffline.flightSearch.verifyPassengerDetailsAfterAddingPassengers(noOfPassengers, passengerNames);
        *//*
eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(9,"\\flybe_automation_report_PNRGeneration_","Sheet1","GDS_ARD_Oneway_2Sectors_ADT_MasterCard_PrimeBooking_UKtoUK",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),"-",
                testData.get("class_1"),currency,"Bag",etkt.get("passenger1"));
    }


    public void gds_ard_Return_Direct_ADT_INF_AMEX_PrimeBooking_Loganair() {
        HashMap<String,String> testData = new DataUtility().testData("57");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        */
/*HashMap<String, String> passengerNames = eflyOffline.passengerContactInfo.retrievePassengerNames(testData);
        eflyOffline.flightSearch.verifyPassengerDetailsAfterAddingPassengers(noOfPassengers, passengerNames);
        *//*
eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(9,"\\flybe_automation_report_PNRGeneration_","Sheet1","GDS_ARD_Return_Direct_ADT_INF_AMEX_PrimeBooking_Loganair",PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),travel_date.get("date2"),testData.get("class_1"),currency,"-");
    }


    @Test
    public void gds_ard_Oneway_2Sector_ADT_CHD_VISA_ManageBooking_NonUKtoUK() {
        HashMap<String,String> testData = new DataUtility().testData("56");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        */
/*HashMap<String, String> passengerNames = eflyOffline.passengerContactInfo.retrievePassengerNames(testData);
        eflyOffline.flightSearch.verifyPassengerDetailsAfterAddingPassengers(noOfPassengers, passengerNames);
        *//*
eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");

        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(9,"\\flybe_automation_report_PNRGeneration_","Sheet1","GDS_ARD_Oneway_2Sector_ADT_CHD_VISA_ManageBooking_NonUKtoUK",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                "-",testData.get("class_1"),currency,"Bag",etkt.get("passenger1")+","+etkt.get("passenger2"));
    }


    @Test
    public void gds_ard_oneWay_Direct_TEEN_MasterCard_PrimeBooking_UKtoUK(){
        HashMap<String,String> testData = new DataUtility().testData("51");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        //eflyOffline.flightSearch.verifyItinerarySection(selectedFlightDetails, testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        HashMap<String, String> passengerNames = eflyOffline.passengerContactInfo.retrievePassengerNames(testData);
        eflyOffline.flightSearch.verifyPassengerDetailsAfterAddingPassengers(noOfPassengers, passengerNames);
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(9,"\\flybe_automation_report_PNRGeneration_","Sheet1","GDS_ARD_OneWay_Direct_TEEN_MasterCard_PrimeBooking_UKtoUK",
                PNR,testData.get("teenName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),
                "-",testData.get("class_1"),currency,"-",etkt.get("passenger1"));

    }

    @Test
    public void gds_ard_oneWay_2Sector_ADT_TEEN_CHD_INF_Visa_PrimeBooking_UKtoNonUK(){
        HashMap<String,String> testData = new DataUtility().testData("52");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        //eflyOffline.flightSearch.verifyItinerarySection(selectedFlightDetails, testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
  //      HashMap<String, String> passengerNames = eflyOffline.passengerContactInfo.retrievePassengerNames(testData);
//        eflyOffline.flightSearch.verifyPassengerDetailsAfterAddingPassengers(noOfPassengers, passengerNames);
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        */
/*HashMap<String,String> flightDetails = eflyOffline.newPNR.retrieveFlightDetails(testData);
        eflyOffline.home.changeOfficeID("EXTBE08AA");
        eflyOffline.newPNR.searchPNR(PNR);
        eflyOffline.eticket.changeCouponStatusForEachETKT("Flown",noOfPassengers,flightDetails,etkt,testData);
        *//*
ReportUtility.writeExcel(9,"\\flybe_automation_report_PNRGeneration_","Sheet1","GDS_ARD_OneWay_2Sector_ADT_TEEN_CHD_INF_PayPal_PrimeBooking_UKtoNonUK",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),"-",
                testData.get("class_1"),currency,"-",etkt.get("passenger1")+","+etkt.get("passenger2")+","+etkt.get("passenger3")+","+etkt.get("passenger4"));
    }

    @Test
    public void gds_ard_oneWay_Direct_ADT_TEEN_MasterCard_PrimeBooking_NonUKtoUK(){
        HashMap<String,String> testData = new DataUtility().testData("53");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        HashMap<String, String> passengerNames = eflyOffline.passengerContactInfo.retrievePassengerNames(testData);
        eflyOffline.flightSearch.verifyPassengerDetailsAfterAddingPassengers(noOfPassengers, passengerNames);
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(9,"\\flybe_automation_report_PNRGeneration_","Sheet1","GDS_ARD_OneWay_Direct_ADT_TEEN_MasterCard_PrimeBooking_NonUKtoUK",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),"-",
                testData.get("class_1"),currency,"-",etkt.get("passenger1")+","+etkt.get("passenger2"));
    }

    @Test
    public void gds_ard_Return_2Sector_ADT_Visa_ManageBooking_UKtoUK(){
        HashMap<String,String> testData = new DataUtility().testData("54");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        */
/*HashMap<String, String> passengerNames = eflyOffline.passengerContactInfo.retrievePassengerNames(testData);
        eflyOffline.flightSearch.verifyPassengerDetailsAfterAddingPassengers(noOfPassengers, passengerNames);
        *//*
eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Baggage");
        eflyOffline.service.selectService("Pre-Paid Bags (CKIN)");
        eflyOffline.service.addServiceAndClose("Bag");
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Seats");
        eflyOffline.service.selectService("Seat Preference");
        eflyOffline.service.addServiceAndClose("Seat","1A");
        eflyOffline.newPNR.navigateToManagePayment("yes");
        eflyOffline.payment.managePayment(testData,"ssr");

        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData);
        eflyOffline.newPNR.setEndOfTransaction(testData);
        ReportUtility.writeExcel(9,"\\flybe_automation_report_PNRGeneration_","Sheet1","GDS_ARD_Return_2Sector_ADT_Visa_ManageBooking_UKtoUK",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),travel_date.get("date1"),"-",
                testData.get("class_1"),currency,"Bag and Seat",etkt.get("passenger1"));
    }

    @Test
    public void gds_ard_OneWay_Direct_ADT_TEEN_CHD_INF_Visa_PrimeBooking_AirFrance(){
        HashMap<String,String> testData = new DataUtility().testData("55");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        HashMap<String,String> travel_date = eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.addPassengerContactInfoAndAssoc(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        String currency = eflyOffline.pricing.change_Retrieve_Currency(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.selectFromAvailableFares(testData);
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        String PNR = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        eflyOffline.newPNR.navigateToAddService();
        eflyOffline.service.selectService("Frequent Flyer Number");
        eflyOffline.service.selectService("FQTV Accrual");
        eflyOffline.service.addFrequentFlyerNumber(testData);
        eflyOffline.newPNR.setEndOfTransaction(testData,"yes");
        ReportUtility.writeExcel(9,"\\flybe_automation_report_PNRGeneration_","Sheet1","GDS_ARD_OneWay_Direct_ADT_TEEN_CHD_INF_Visa_PrimeBooking_AirFrance",
                PNR,testData.get("adultName1").split(" ")[2],testData.get("frequentFlyerNumber"),
                travel_date.get("date1"),"-",testData.get("class_1"),currency,"-",etkt.get("passenger1")+","+etkt.get("passenger2")+","+etkt.get("passenger3")+","+etkt.get("passenger4"));
    }
}
*/
