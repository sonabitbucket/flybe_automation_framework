package test_definition.efly.offline.pnr_generation;

import PageBase.EFlyOfflineBase;
import org.testng.annotations.Test;
import utilities.DataUtility;
import utilities.ReportUtility;

import java.util.HashMap;

public class ARDWebPNR {

    @Test//(invocationCount = 25)
    public void pnrGeneratorSingleSector(){
        HashMap<String,String> testData = new DataUtility().testData("10");
        EFlyOfflineBase eflyOffline = new EFlyOfflineBase();
        eflyOffline.login.login(testData);
        eflyOffline.home.changeOfficeID();
        eflyOffline.home.createPNR();
        eflyOffline.flightSearch.enterJourneyDetails(testData);
        eflyOffline.flightSearch.selectAndBookFlight(testData);
        ////eflyOffline.flightSearch.verifyItinerarySection(selectedFlightDetails,testData);
        eflyOffline.flightSearch.navigateToAddPassenger();
        int noOfPassengers = eflyOffline.passengerContactInfo.addPassengerAndRetrieveNoOfPassengersAdded(testData);
        eflyOffline.passengerContactInfo.addMainContactInfo(testData);
        eflyOffline.passengerContactInfo.applyAndClosePassengersAndContactInfo();
        HashMap<String,String>passengerNames = eflyOffline.passengerContactInfo.retrievePassengerNames(testData);
        eflyOffline.flightSearch.verifyPassengerDetailsAfterAddingPassengers(noOfPassengers,passengerNames);
        eflyOffline.newPNR.clickOnNewPNR();
        eflyOffline.newPNR.setSetTicketingArrangement();
        eflyOffline.tktArrangement.addTicketingArrangemnt();
        eflyOffline.newPNR.setEndOfTransaction(testData);
        //eflyOffline.newPNR.verifyTicketingInformationMessageDisplayed();
        String pnr = eflyOffline.newPNR.retrievePNRgeneratedOnEOT();
        eflyOffline.newPNR.navigateToPricing();
        eflyOffline.pricing.selectPricingOption(testData);
        eflyOffline.pricing.selectFareType(testData);
        eflyOffline.pricing.updateFares();
        eflyOffline.pricing.addToPriceSummary();
        String totalFares = eflyOffline.pricing.verifyPriceSummaryAndConfirmPrice();
        eflyOffline.newPNR.navigateToManagePayment();
        eflyOffline.payment.managePayment(testData,totalFares);
        HashMap<String,String> etkt = eflyOffline.newPNR.retrieveETKT(noOfPassengers);
        ReportUtility.setXLFilepathAndCreateXL(new int[]{6,0},"\\flybe_automation_report_PNRGeneration_","FLIGHT_NUMBER","DEPARTURE_AIRPORT","DEPARTURE_DATE","PNR","FIRSTNAME","LASTNAME","DEPARTURE_TIME");
        //ReportUtility.writeExcel(6,"\\flybe_automation_report_PNRGeneration_","Sheet1",selectedFlightDetails.get("flightNo1"),selectedFlightDetails.get("departure1"),selectedFlightDetails.get("journey1Date"),pnr,passengerNames.get("adult1").split(" ")[1],passengerNames.get("adult1").split(" ")[2],selectedFlightDetails.get("departureTime1"));
    }

}
