package test_definition.efly.online.primebooking.seat;

import PageBase.EFLYOnlineBase;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.HashMap;

public class Seat {

    @Test(groups = {"regression","p2"})
    public void R45_verifySeatForCombinationOfJustFlyAndGetMore(){
        HashMap testData = new DataUtility().testData("68");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        HashMap<String,Double> basket = new HashMap<>();
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basket = basePage.seat.selectSeat(testData,basket);
        HashMap<String,String> seatDetails = basePage.seat.retrieveSeatSelectedForEachPax(testData);
        basket = basePage.seat.verifyBasket(basket);
        basePage.seat.navigateToExtrasAfterSelectingSeat();
        basket = basePage.extras.verifyBasket(basket);
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.verifyBasket(basket);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifySeatAllocationAndRetrieveEMD(testData,seatDetails,basket);
    }

    @Test(groups = {"regression","p1"})
    public void R50_verifyStandardFOCAndExtraLegRoomSeatChargesForGetMore(){
        HashMap testData = new DataUtility().testData("69");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        HashMap<String,Double> basket = new HashMap<>();
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basket = basePage.paxDetails.verifyBasket(basket);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basket = basePage.seat.selectSeat(testData,basket);
        HashMap<String,String> seatDetails = basePage.seat.retrieveSeatSelectedForEachPax(testData);
        basket = basePage.seat.verifyBasket(basket);
        basePage.seat.navigateToExtrasAfterSelectingSeat();
        basket = basePage.extras.verifyBasket(basket);
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.verifyBasket(basket);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifySeatAllocationAndRetrieveEMD(testData,seatDetails,basket);
    }

    @Test(groups = {"regression","p1"})
    public void R51_verifyWhetherSelectedSeatIsDisplayedOnExtrasAndNewSeatSelectedOnExtrasIsDisplayedOnConfirmation(){
        HashMap testData = new DataUtility().testData("70");
        HashMap<String,Double> basket = new HashMap<>();
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basket = basePage.seat.selectSeat(testData,basket);
        HashMap<String,String> seatDetails = basePage.seat.retrieveSeatSelectedForEachPax(testData);
        basePage.seat.navigateToExtrasAfterSelectingSeat();
        basePage.extras.verifyBasket(basket);
        basePage.extras.selectService("Edit seats");
        basePage.extras.verifySeatSelectedOnSeatPage(seatDetails,testData);
        basket = basePage.extras.editSeatFromExtras(testData,basket);
        seatDetails = basePage.extras.retrieveSeatSelectedForEachPax(testData);
        basePage.extras.continueAfterSeatSelection();
        basePage.extras.verifyBasket(basket,true);
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        HashMap<String,String> seatEMD = basePage.confirmation.verifySeatAllocationAndRetrieveEMD(testData,seatDetails,basket);
    }

    @Test(groups = {"regression","p2"})
    public void R60_verifyRestrictedSeatMapForInfantAndChild(){
        HashMap testData = new DataUtility().testData("79");
        HashMap<String,Double> basket = new HashMap<>();
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.verifyRestrictedSeatForInfant(testData);
        basePage.seat.verifyRestrictedSeatForChild_Teen(testData,"Child");
        basket = basePage.seat.selectSeat(testData,basket);
        HashMap<String,String> seatDetails = basePage.seat.retrieveSeatSelectedForEachPax(testData);
        basket = basePage.seat.verifyBasket(basket);
        basePage.seat.navigateToExtrasAfterSelectingSeat();
        basket = basePage.extras.verifyBasket(basket);
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.verifyBasket(basket);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifySeatAllocationAndRetrieveEMD(testData,seatDetails,basket);
    }

    @Test(groups = {"regression","p2"})
    public void R68_verifyXLSeatIsRestrictedForTeen(){
        HashMap testData = new DataUtility().testData("80");
        HashMap<String,Double> basket = new HashMap<>();
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.verifyRestrictedSeatForChild_Teen(testData,"Teen");
        basket = basePage.seat.selectSeat(testData,basket);
        HashMap<String,String> seatDetails = basePage.seat.retrieveSeatSelectedForEachPax(testData);
        basket = basePage.seat.verifyBasket(basket);
        basePage.seat.navigateToExtrasAfterSelectingSeat();
        basket = basePage.extras.verifyBasket(basket);
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.verifyBasket(basket);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifySeatAllocationAndRetrieveEMD(testData,seatDetails,basket);
    }

    @Test(groups = {"regression","p2"})
    public void R72_verifyRestrictedSeatForSpecialAssistance(){
        HashMap testData = new DataUtility().testData("81");
        HashMap<String,Double> basket = new HashMap<>();
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basket = basePage.paxDetails.selectSpecialAssistance(testData,basket);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.verifyRestrictedForSpecialAssistance(testData);
        basket = basePage.seat.selectSeat(testData,basket);
        HashMap<String,String> seatDetails = basePage.seat.retrieveSeatSelectedForEachPax(testData);
        basket = basePage.seat.verifyBasket(basket);
        basePage.seat.navigateToExtrasAfterSelectingSeat();
        basket = basePage.extras.verifyBasket(basket);
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.verifyBasket(basket);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifySpecialAssistance(testData,basket);
        basePage.confirmation.verifySeatAllocationAndRetrieveEMD(testData,seatDetails,basket);
    }

    @Test(groups = {"regression","p2"})
    public void R73_verifyJustFlySeatsFreeForCWL_VLY(){
        HashMap testData = new DataUtility().testData("82");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        HashMap<String,Double> basket = basePage.fareselect.getFareDetails(testData);
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basket = basePage.seat.selectSeat(testData,basket);
        HashMap<String,String> seatDetails = basePage.seat.retrieveSeatSelectedForEachPax(testData);
        basket = basePage.seat.verifyBasket(basket);
        basePage.seat.navigateToExtrasAfterSelectingSeat();
        basket = basePage.extras.verifyBasket(basket);
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.verifyBasket(basket);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifySeatAllocationAndRetrieveEMD(testData,seatDetails,basket);
    }


}
