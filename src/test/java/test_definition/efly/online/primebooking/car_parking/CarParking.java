package test_definition.efly.online.primebooking.car_parking;

import PageBase.EFLYOnlineBase;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.HashMap;

public class CarParking {

    @Test(groups = {"regression","p2"})
    public void R76_BookingWithCarParkingForTwoWayBooking(){
        HashMap testData = new DataUtility().testData("112");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        HashMap<String,Double> basket = basePage.fareselect.getFareDetails(testData);
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basket = basePage.paxDetails.selectBags(testData,basket);
        basket = basePage.paxDetails.verifyBasket(basket);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basket = basePage.seat.verifyBasket(basket);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.selectService("Book parking");
        basket= basePage.extras.selectCarParking(testData,basket);
        basket = basePage.extras.verifyBasket(basket);
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        String currency = basePage.payment.retrieve_verifyPaymentCurrency();
        basePage.payment.verifyBasket(basket);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifyPriceSection(basket,currency,testData);
        HashMap<String,String> bagEMD = basePage.confirmation.verifyBagsAndRetrieveEMD(testData,basket);
        HashMap<String,String> ParkingEMD = basePage.confirmation.verifyOtherServicesAndRetrieveEMD("airport parking",basket);

    }


    @Test(groups = {"regression","p2"})
    public void R77_BookingWithCarParkingForOneWayBooking() {
        HashMap testData = new DataUtility().testData("113");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        HashMap<String, Double> basket = basePage.fareselect.getFareDetails(testData);
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData, basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basket = basePage.paxDetails.verifyBasket(basket);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basket = basePage.seat.verifyBasket(basket);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.selectService("Book parking");
        basket = basePage.extras.selectCarParking(testData, basket);
        basket = basePage.extras.verifyBasket(basket);
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        String currency = basePage.payment.retrieve_verifyPaymentCurrency();
        basePage.payment.verifyBasket(basket);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifyPriceSection(basket, currency, testData);
        HashMap<String, String> ParkingEMD = basePage.confirmation.verifyOtherServicesAndRetrieveEMD("airport parking", basket);
    }
}
