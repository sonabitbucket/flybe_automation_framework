package test_definition.efly.online.primebooking.mcp;

import PageBase.EFLYOnlineBase;
import org.testng.annotations.Test;
import utilities.CommonUtility;
import utilities.DataUtility;

import java.util.HashMap;

/**
 * Created by Indhu on 5/16/2019.
 */
public class MCP {

    @Test(groups = {"regression","p1"})
    public void R41_UKtoUKhasEUROcurrencyinVM(){
        //CommonUtility.getCountryFromIP(); //todo - this method is not working
        HashMap testData = new DataUtility().testData("15");
        HashMap<String,Double> basket = new HashMap<>();
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basket = basePage.fareselect.getFareDetails(testData);
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        String currency = basePage.payment.retrieve_verifyPaymentCurrency("Euro (EUR)");
        basePage.payment.verifyAndRetrieveCurrencySymbolInBasket(currency);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifyPriceSection(basket,currency,testData,"mcp payment");
    }

    @Test(groups = {"regression","p1"})
    public void R30_verifyBookingWithAUDCurrencyWithAncillaries(){
        HashMap testData = new DataUtility().testData("14");
        HashMap<String,Double> basket = new HashMap<>();
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basket = basePage.fareselect.getFareDetails(testData);
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basket = basePage.paxDetails.selectBags(testData,basket);
        basePage.paxDetails.addBookerDetails(testData);
        basket = basePage.paxDetails.verifyBasket(basket);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basket = basePage.seat.selectSeat(testData,basket);
        HashMap<String,String> seatDetails = basePage.seat.retrieveSeatSelectedForEachPax(testData);
        basket = basePage.seat.verifyBasket(basket);
        basePage.seat.navigateToExtrasAfterSelectingSeat();
        basePage.extras.selectService("Add golf clubs");
        basket = basePage.extras.addGolf(testData,basket);
        basket = basePage.extras.verifyBasket(basket);
        basePage.extras.continueToPayment();
        basePage.payment.changeCurrency("Australian Dollar (AUD)");
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.verifyAndRetrieveCurrencySymbolInBasket("Australian Dollar (AUD)");
        basket = basePage.payment.verifyBasket(basket);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifyPriceSection(basket,"Australian Dollar (AUD)",testData,"mcp payment");
        HashMap bagEMD = basePage.confirmation.verifyBagsAndRetrieveEMD(testData,basket);
        HashMap seatEMD = basePage.confirmation.verifySeatAllocationAndRetrieveEMD(testData,seatDetails,basket);
        HashMap golfEMD = basePage.confirmation.verifyGolf_SkiAndRetrieveEMD(testData,basket,"Golf");
    }

    @Test(groups = {"regression","p1"})
    public void R31_verifyBookingWithCADCurrencyWithAncillaries(){
        HashMap testData = new DataUtility().testData("157");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        HashMap<String,Double> basket = new HashMap<>();
        basket = basePage.fareselect.getFareDetails(testData);
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basket = basePage.paxDetails.selectBags(testData,basket);
        basePage.paxDetails.addBookerDetails(testData);
        basket = basePage.paxDetails.verifyBasket(basket);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basket = basePage.seat.selectSeat(testData,basket);
        HashMap<String,String> seatDetails = basePage.seat.retrieveSeatSelectedForEachPax(testData);
        basePage.seat.navigateToExtrasAfterSelectingSeat();
        basePage.extras.selectService("Add skis");
        basket = basePage.extras.addSkisSnowboard(testData,basket);
        basket = basePage.extras.verifyBasket(basket);
        basePage.extras.continueToPayment();
        basePage.payment.changeCurrency("Canadian Dollar (CAD)");
        basePage.payment.enterPaymentDetails(testData);
        //String currency = basePage.payment.retrieve_verifyPaymentCurrency();
        basePage.payment.verifyAndRetrieveCurrencySymbolInBasket("Canadian Dollar (CAD)");
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifyPriceSection(basket,"Canadian Dollar (CAD)",testData,"mcp payment");
        basePage.confirmation.verifySeatAllocationAndRetrieveEMD(testData,seatDetails,basket);
        basePage.confirmation.verifyBagsAndRetrieveEMD(testData,basket);
        basePage.confirmation.verifyGolf_SkiAndRetrieveEMD(testData,basket,"Ski");
    }

    @Test(groups = {"regression","p1"})
    public void R32_verifyBookingWithHKDCurrency(){
        HashMap testData = new DataUtility().testData("15");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        HashMap<String,Double> basket = new HashMap<>();
        basket = basePage.fareselect.getFareDetails(testData);
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.changeCurrency("Hong Kong Dollar (HKD)");
        basePage.payment.enterPaymentDetails(testData);
        //String currency = basePage.payment.retrieve_verifyPaymentCurrency();
        basePage.payment.verifyAndRetrieveCurrencySymbolInBasket("Hong Kong Dollar (HKD)");
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifyPriceSection(basket,"Hong Kong Dollar (HKD)",testData,"mcp payment");

    }

    @Test(groups = {"regression","p1"})
    public void R33_verifyBookingWithJPYCurrency(){
        HashMap testData = new DataUtility().testData("16");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        HashMap<String,Double> basket = new HashMap<>();;
        basket = basePage.fareselect.getFareDetails(testData);
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.changeCurrency("Japanese Yen (JPY)");
        basePage.payment.enterPaymentDetails(testData);
        //String currency = basePage.payment.retrieve_verifyPaymentCurrency();
        basePage.payment.verifyAndRetrieveCurrencySymbolInBasket("Japanese Yen (JPY)");
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifyPriceSection(basket,"Japanese Yen (JPY)",testData,"mcp payment");
    }

    @Test(groups = {"regression","p1"})
    public void R34_verifyBookingWithNOKCurrency(){
        HashMap testData = new DataUtility().testData("17");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        HashMap<String,Double> basket = new HashMap<>();
        basket = basePage.fareselect.getFareDetails(testData);
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.changeCurrency("Norwegian Krone (NOK)");
        basePage.payment.enterPaymentDetails(testData);
        //String currency = basePage.payment.retrieve_verifyPaymentCurrency();
        basePage.payment.verifyAndRetrieveCurrencySymbolInBasket("Norwegian Krone (NOK)");
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.complete3DSecureIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifyPriceSection(basket,"Norwegian Krone (NOK)",testData,"mcp payment");
    }

    @Test(groups = {"regression","p1"})
    public void R35_verifyBookingWithUSDCurrency(){
        HashMap testData = new DataUtility().testData("18");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        HashMap<String,Double> basket = basePage.fareselect.getFareDetails(testData);
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.changeCurrency("US Dollar (USD)");
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.verifyAndRetrieveCurrencySymbolInBasket("US Dollar (USD)");
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifyPriceSection(basket,"US Dollar (USD)",testData,"mcp payment");
    }

    @Test(groups = {"regression","p1"})
    public void R36_verifyBookingWithGBPCurrency(){//Base Currency
        HashMap testData = new DataUtility().testData("13");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        HashMap<String,Double> basket = basePage.fareselect.getFareDetails(testData);
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.retrieve_verifyPaymentCurrency("Pound Sterling (GBP)");
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifyPriceSection(basket,"Pound Sterling (GBP)",testData);
    }

    @Test(groups = {"regression","p1"})
    public void R37_verifyBookingWithEURCurrency(){//Base Currency
        HashMap testData = new DataUtility().testData("19");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        HashMap<String,Double> basket = basePage.fareselect.getFareDetails(testData);
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        //Double totalBagAmount = basePage.paxDetails.selectBags(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        String currency = basePage.payment.retrieve_verifyPaymentCurrency("Euro (EUR)");
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.complete3DSecureIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifyPriceSection(basket,currency,testData);
    }

    @Test(groups = {"regression","p1"})
    public void R38_verifyBookingWithCHFCurrency(){//Base Currency
        HashMap testData = new DataUtility().testData("20");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        HashMap<String,Double> basket = new HashMap<>();
        basket = basePage.fareselect.getFareDetails(testData);
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras(testData);
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        String currency = basePage.payment.retrieve_verifyPaymentCurrency("Swiss Franc (CHF)");
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.complete3DSecureIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifyPriceSection(basket,currency,testData);
    }

    @Test(groups = {"regression","p2"})
    public void R39_verifyMessageDisplayedWhenMCPAndOtherPaymentCardsAreSelected(){
        HashMap testData = new DataUtility().testData("21");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.changeCurrency("Australian Dollar (AUD)");
        basePage.payment.enterPaymentDetails(testData,"mcp");
    }

    @Test(groups = {"regression","p2"})
    public void R40_verifyMessageDisplayedWhenMCPAndPaypalIsSelected(){
        HashMap testData = new DataUtility().testData("22");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.changeCurrency("US Dollar (USD)");
        basePage.payment.enterPaymentDetails(testData,"mcp");
    }

    @Test(groups = {"regression","p2"})
    public void R67_verifyMessageForVoucherAndMCPBooking(){
        HashMap<String,String> testData = new DataUtility().testData("108");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.ebank.navigateToEbankAndLogin();
        String voucherNo = basePage.ebank.createVoucherAndGetVoucherNo(testData);
        basePage.home.navigateToHomepage();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.changeCurrency("US Dollar (USD)");
        basePage.payment.applyVoucherCode(voucherNo,"mcp");
    }
}
