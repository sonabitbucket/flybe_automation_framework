package test_definition.efly.online.primebooking.insurance;

import PageBase.EFLYOnlineBase;
import org.testng.annotations.Test;
import utilities.CommonUtility;
import utilities.DataUtility;

import java.util.HashMap;

public class Insurance {

    @Test(groups = {"regression","p1"})
    public void R29_BookingWithInsuranceForAdultTeenAndInfant(){
        HashMap testData = new DataUtility().testData("101");
        testData = CommonUtility.getRandomName(testData);
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        HashMap<String,Double> basket = new HashMap<>();
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basket = basePage.paxDetails.verifyBasket(basket);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basket = basePage.extras.verifyBasket(basket);
        basePage.extras.continueToPayment();
        basket = basePage.payment.addInsurance(testData,basket);
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.verifyBasket(basket);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.complete3DSecureIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifyInsuranceSection(testData);

    }

    @Test(groups = {"regression","p1"})
    public void R42_BookingWithInsuranceForAdultAndChild(){
        HashMap testData = new DataUtility().testData("24");
        testData = CommonUtility.getRandomName(testData);
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        HashMap<String,Double> basket = basePage.fareselect.getFareDetails(testData);
        basePage.fareselect.verifyBasket(basket);
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basket = basePage.paxDetails.verifyBasket(basket);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basket = basePage.extras.verifyBasket(basket);
        basePage.extras.continueToPayment();
        basket = basePage.payment.addInsurance(testData,basket);
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.verifyBasket(basket);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.complete3DSecureIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifyInsuranceSection(testData);

    }

    @Test(groups = {"regression","p2"})
    public void R43_BookingWithInsuranceForUKToUK(){
        HashMap testData = new DataUtility().testData("25");
        testData = CommonUtility.getRandomName(testData);
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        HashMap<String,Double> basket = basePage.fareselect.getFareDetails(testData);
        basePage.fareselect.verifyBasket(basket);
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basket = basePage.paxDetails.verifyBasket(basket);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basket = basePage.extras.verifyBasket(basket);
        basePage.extras.continueToPayment();
        basket = basePage.payment.addInsurance(testData,basket);
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.verifyBasket(basket);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.complete3DSecureIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifyInsuranceSection(testData);
    }

    @Test(groups = {"regression","p2"})
    public void R44_VerifyInsuranceSectionIsNotDisplayedForNonUKToUkRoute(){
        HashMap testData = new DataUtility().testData("26");
        testData = CommonUtility.getRandomName(testData);
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.verifyInsuranceSection();

    }
}
