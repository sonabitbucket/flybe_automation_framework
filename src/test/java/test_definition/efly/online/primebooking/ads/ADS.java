package test_definition.efly.online.primebooking.ads;

import PageBase.EFLYOnlineBase;
import org.testng.annotations.Test;
import utilities.DataUtility;
import java.util.HashMap;

public class ADS {

    @Test(groups = {"regression","p2"})
    public void R74_verifyADSDiscount(){
        HashMap testData = new DataUtility().testData("33");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.handleHomeScreenPopUp(testData);
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        HashMap<String,String> fares = basePage.fareselect.selectFlight(testData,"getFare");
        basePage.home.navigateToHomepage();
        basePage.home.selectHeaderMenu("Flights");
        basePage.home.selectHeaderSubMenu("Air Discount Scheme");
        basePage.ads.ads_login(testData);
        basePage.ads.enterSourceAndDestination(testData);
        basePage.ads.enterTravelDates(testData);
        basePage.ads.selectNoOfPassengers(testData);
        basePage.ads.findFlights();
        basePage.ads.verifyADS_price("Outbound",fares);
        basePage.ads.selectFlight(testData);
        basePage.ads.verifyADS_price("Inbound",fares);
        basePage.ads.navigateToPaxDetails();
        basePage.paxDetails.enterPaxPersonalDetails(testData,"ads");
        basePage.paxDetails.selectBags(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
    }
}
