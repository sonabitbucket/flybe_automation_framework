package test_definition.efly.online.primebooking.voucher;

import PageBase.EFLYOnlineBase;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.HashMap;

public class Voucher {


    @Test(groups = {"regression"})
    public void R52_verifyPartPayWithVoucherBookingUsingVisa(){
        HashMap<String,String> testData = new DataUtility().testData("71");
        HashMap<String,Double> basket = new HashMap<>();
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.ebank.navigateToEbankAndLogin();
        String voucherNo = basePage.ebank.createVoucherAndGetVoucherNo(testData);
        basePage.home.navigateToHomepage();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basket = basePage.fareselect.getFareDetails(testData);
        basePage.fareselect.verifyBasket(basket);
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basket = basePage.paxDetails.verifyBasket(basket);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basket = basePage.extras.verifyBasket(basket);
        basePage.extras.continueToPayment();
        basePage.payment.applyVoucherCode(voucherNo);
        String amountPaidByCard = basePage.payment.verifyVoucherAmountAppliedAndCompletePayment(testData);
        basePage.payment.verifyBasket(basket);
        /*String currency = basePage.payment.retrieve_verifyPaymentCurrency();
        String currencySymbol = basePage.payment.verifyAndRetrieveCurrencySymbolInBasket(currency,"yes");
        */basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR= basePage.confirmation.retrievePNR();
        basePage.confirmation.verifyPriceSection(basket,"Pound Sterling (GBP)",testData,"voucher verification");
        basePage.confirmation.verifyPriceSectionForVoucherPayment(basket,testData,voucherNo,amountPaidByCard,"£","Pound Sterling (GBP)");

    }

    @Test(groups = {"regression"})
    public void R61_verifyPartPayWithVoucherBookingUsingAmex(){
        HashMap<String,String> testData = new DataUtility().testData("102");
        HashMap<String,Double> basket = new HashMap<>();
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.ebank.navigateToEbankAndLogin();
        String voucherNo = basePage.ebank.createVoucherAndGetVoucherNo(testData);
        basePage.home.navigateToHomepage();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basket = basePage.fareselect.getFareDetails(testData);
        basePage.fareselect.verifyBasket(basket);
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basket = basePage.paxDetails.verifyBasket(basket);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basket = basePage.extras.verifyBasket(basket);
        basePage.extras.continueToPayment();
        basePage.payment.applyVoucherCode(voucherNo);
        String amountPaidByCard =  basePage.payment.verifyVoucherAmountAppliedAndCompletePayment(testData);
        basePage.payment.verifyBasket(basket);
        /*String currency = basePage.payment.retrieve_verifyPaymentCurrency();
        String currencySymbol = basePage.payment.verifyAndRetrieveCurrencySymbolInBasket(currency,"yes");
        */basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR= basePage.confirmation.retrievePNR();
        basePage.confirmation.verifyPriceSection(basket,"Pound Sterling (GBP)",testData,"voucher verification");
        basePage.confirmation.verifyPriceSectionForVoucherPayment(basket,testData,voucherNo,amountPaidByCard,"£","Pound Sterling (GBP)");
    }

    @Test(groups = {"regression"})
    public void R62_verifyPartPayWithVoucherBookingUsingMasterCard(){
        HashMap<String,String> testData = new DataUtility().testData("103");
        HashMap<String,Double> basket = new HashMap<>();
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.ebank.navigateToEbankAndLogin();
        String voucherNo = basePage.ebank.createVoucherAndGetVoucherNo(testData);
        basePage.home.navigateToHomepage();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basket = basePage.fareselect.getFareDetails(testData);
        basePage.fareselect.verifyBasket(basket);
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basket = basePage.paxDetails.selectBags(testData,basket);
        basket = basePage.paxDetails.verifyBasket(basket);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basket = basePage.seat.selectSeat(testData,basket);
        basePage.seat.retrieveSeatSelectedForEachPax(testData);
        basket = basePage.seat.verifyBasket(basket);
        basePage.seat.navigateToExtrasAfterSelectingSeat();
        basket = basePage.extras.verifyBasket(basket);
        basePage.extras.continueToPayment();
        basePage.payment.applyVoucherCode(voucherNo);
        String amountPaidByCard =  basePage.payment.verifyVoucherAmountAppliedAndCompletePayment(testData);
        basePage.payment.verifyBasket(basket);
/*
        String currency = basePage.payment.retrieve_verifyPaymentCurrency();
        String currencySymbol = basePage.payment.verifyAndRetrieveCurrencySymbolInBasket(currency,"yes");
*/
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR= basePage.confirmation.retrievePNR();
        basePage.confirmation.verifyPriceSection(basket,"Pound Sterling (GBP)",testData,"voucher verification");
        basePage.confirmation.verifyPriceSectionForVoucherPayment(basket,testData,voucherNo,amountPaidByCard,"£","Pound Sterling (GBP)");
    }

    @Test(groups = {"regression"})
    public void R63_verifyPartPayWithVoucherBookingUsingDinersCard(){
        HashMap<String,String> testData = new DataUtility().testData("104");
        HashMap<String,Double> basket = new HashMap<>();
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.ebank.navigateToEbankAndLogin();
        String voucherNo = basePage.ebank.createVoucherAndGetVoucherNo(testData);
        basePage.home.navigateToHomepage();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basket = basePage.fareselect.getFareDetails(testData);
        basePage.fareselect.verifyBasket(basket);
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basket = basePage.paxDetails.verifyBasket(basket);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basket = basePage.seat.selectSeat(testData,basket);
        basePage.seat.retrieveSeatSelectedForEachPax(testData);
        basket = basePage.seat.verifyBasket(basket);
        basePage.seat.navigateToExtrasAfterSelectingSeat();
        basket = basePage.extras.verifyBasket(basket);
        basePage.extras.continueToPayment();
        basePage.payment.applyVoucherCode(voucherNo);
        String amountPaidByCard =  basePage.payment.verifyVoucherAmountAppliedAndCompletePayment(testData);
        basePage.payment.verifyBasket(basket);
        /*String currency = basePage.payment.retrieve_verifyPaymentCurrency();
        String currencySymbol = basePage.payment.verifyAndRetrieveCurrencySymbolInBasket(currency,"yes");
        */basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR= basePage.confirmation.retrievePNR();
        basePage.confirmation.verifyPriceSection(basket,"Pound Sterling (GBP)",testData,"voucher verification");
        basePage.confirmation.verifyPriceSectionForVoucherPayment(basket,testData,voucherNo,amountPaidByCard,"£","Pound Sterling (GBP)");
    }

    //@Test(groups = {"regression"})
    public void R64_verifyPartPayWithVoucherBookingUsingUATP(){
        HashMap<String,String> testData = new DataUtility().testData("105");
        HashMap<String,Double> basket = new HashMap<>();
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.ebank.navigateToEbankAndLogin();
        String voucherNo = basePage.ebank.createVoucherAndGetVoucherNo(testData);
        basePage.home.navigateToHomepage();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basket = basePage.fareselect.getFareDetails(testData);
        basePage.fareselect.verifyBasket(basket);
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basket = basePage.paxDetails.selectBags(testData,basket);
        basket = basePage.paxDetails.verifyBasket(basket);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basket = basePage.extras.verifyBasket(basket);
        basePage.extras.continueToPayment();
        basePage.payment.applyVoucherCode(voucherNo);
        String amountPaidByCard =  basePage.payment.verifyVoucherAmountAppliedAndCompletePayment(testData);
        basePage.payment.verifyBasket(basket);
        String currency = basePage.payment.retrieve_verifyPaymentCurrency();
        String currencySymbol = basePage.payment.verifyAndRetrieveCurrencySymbolInBasket(currency,"yes");
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR= basePage.confirmation.retrievePNR();
        basePage.confirmation.verifyPriceSection(basket,currency,testData,"voucher verification");
        basePage.confirmation.verifyPriceSectionForVoucherPayment(basket,testData,voucherNo,amountPaidByCard,currencySymbol,currency);
    }

    @Test(groups = {"regression"})
    public void R65_verifyFullPaymentWithVoucher(){
        HashMap<String,String> testData = new DataUtility().testData("106");
        HashMap<String,Double> basket = new HashMap<>();
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.ebank.navigateToEbankAndLogin();
        String voucherNo = basePage.ebank.createVoucherAndGetVoucherNo(testData);
        basePage.home.navigateToHomepage();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basket = basePage.fareselect.getFareDetails(testData);
        basePage.fareselect.verifyBasket(basket);
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basket = basePage.paxDetails.verifyBasket(basket);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basket = basePage.extras.verifyBasket(basket);
        basePage.extras.continueToPayment();
        basePage.payment.applyVoucherCode(voucherNo);
        String amountPaidByCard =  basePage.payment.verifyVoucherAmountAppliedAndCompletePayment(testData);
        basePage.payment.verifyBasket(basket);
/*
        String currency = basePage.payment.retrieve_verifyPaymentCurrency();
        String currencySymbol = basePage.payment.verifyAndRetrieveCurrencySymbolInBasket(currency,"yes");
*/
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData,"no");
        String PNR= basePage.confirmation.retrievePNR();
        basePage.confirmation.verifyPriceSection(basket,"Pound Sterling (GBP)",testData,"voucher verification");
        basePage.confirmation.verifyPriceSectionForVoucherPayment(basket,testData,voucherNo,amountPaidByCard,"£","Pound Sterling (GBP)");
    }

    @Test(groups = {"regression"})
    public void R66_verifyVoucherBookingCannotBeDoneWithPaypal(){
        HashMap<String,String> testData = new DataUtility().testData("107");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.ebank.navigateToEbankAndLogin();
        String voucherNo = basePage.ebank.createVoucherAndGetVoucherNo(testData);
        basePage.home.navigateToHomepage();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.applyVoucherCode(voucherNo);
        basePage.payment.verifyVoucherAmountAppliedAndCompletePayment(testData,"voucher");

    }


}
