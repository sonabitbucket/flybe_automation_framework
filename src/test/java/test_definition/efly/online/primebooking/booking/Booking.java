package test_definition.efly.online.primebooking.booking;

import PageBase.EFLYOnlineBase;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.HashMap;

/**
 * Created by Sona on 3/26/2019.
 */
public class Booking {

    @Test(groups = {"regression","p1"})
    public void R1_TST_14182_verifyUserIsAbleToBookWithVISA(){
        HashMap testData = new DataUtility().testData("7");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
    }

    @Test(groups = {"regression","p1"})
    public void R23_TST_14179_verifyUserIsAbleToBookJustFlyTicketWithAMEX() {
        HashMap testData = new DataUtility().testData("8");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
    }

    @Test(groups = {"regression","p1"})
    public void R24_TST_14179_verifyUserIsAbleToBookJustFlyFlexTicketWithAMEX() {
        HashMap testData = new DataUtility().testData("9");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);

        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
    }

    @Test(groups = {"regression","p1"})
    public void R25_TST_14179_verifyUserIsAbleToBookGetMoreTicketWithAMEX() {
        HashMap testData = new DataUtility().testData("10");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
    }

    @Test(groups = {"regression","p1"})
    public void R26_TST_14179_verifyUserIsAbleToBookAllInTicketWithAMEX() {
        HashMap testData = new DataUtility().testData("11");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        //basePage.fareselect.verifyBasketAncillaries();
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
    }

    @Test(groups = {"regression","p1"})
    public void R27_TST_14181_verifyUKToNonUKWithDiners() {
        HashMap testData = new DataUtility().testData("12");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        //basePage.fareselect.verifyBasketAncillaries();
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
    }

    @Test(groups = {"regression","p1"})
    public void R28_twoWayBookingWithAncillariesAndVerifyThatAPDisNotTakenVIAmasterCard(){
        HashMap testData = new DataUtility().testData("67");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        HashMap<String,Double> basket = basePage.fareselect.getFareDetails(testData,true);
        HashMap<String,String> flightDetails = basePage.fareselect.retrieveFlightDetails(testData);
        basePage.fareselect.verifyBasket(basket);
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basket = basePage.paxDetails.selectBags(testData,basket);
        basket = basePage.paxDetails.verifyBasket(basket);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basket = basePage.seat.selectSeat(testData,basket);
        HashMap<String,String> seatDetails = basePage.seat.retrieveSeatSelectedForEachPax(testData);
        basket = basePage.seat.verifyBasket(basket);
        basePage.seat.navigateToExtrasAfterSelectingSeat();
        basePage.extras.selectService("Add golf clubs");
        basket = basePage.extras.addGolf(testData,basket);
        basePage.extras.selectService("Add skis");
        basket = basePage.extras.addSkisSnowboard(testData,basket);
        basket = basePage.extras.verifyBasket(basket);
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        String currency = basePage.payment.retrieve_verifyPaymentCurrency();
        basePage.payment.verifyBasket(basket);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifyPriceSection(basket,currency,testData,"tax verification per pax");
        basePage.confirmation.verifyItinerarySection(flightDetails);
        HashMap<String,String> e_tkt = basePage.confirmation.verifyTravellerDetailsAndRetrieveETKT(testData);
        System.out.println(e_tkt);
        HashMap<String,String> bagEMD = basePage.confirmation.verifyBagsAndRetrieveEMD(testData,basket);
        System.out.println(bagEMD);
        HashMap<String,String> seatEMD = basePage.confirmation.verifySeatAllocationAndRetrieveEMD(testData,seatDetails,basket);
        System.out.println(seatEMD);
        HashMap<String,String> golfEMD = basePage.confirmation.verifyGolf_SkiAndRetrieveEMD(testData,basket,"Golf");
        System.out.println(golfEMD);
        HashMap<String,String> skiEMD = basePage.confirmation.verifyGolf_SkiAndRetrieveEMD(testData,basket,"Ski");
        System.out.println(skiEMD);
    }

    @Test(groups = {"regression","p1"})
    public void R48_BookingWithAncillariesAsLoggedInUserWithMasterCard(){
        HashMap testData = new DataUtility().testData("23");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.openLoginPage(testData);
        basePage.digitalProfile.login(testData,false);
        basePage.home.navigateToMyAccount();
        testData = basePage.digitalProfile.capturePassengerInformation(testData);
        testData = basePage.digitalProfile.captureContactInformation(testData);
        basePage.digitalProfile.navigateBackToHomeFromLogin();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        HashMap<String,Double> basket = basePage.fareselect.getFareDetails(testData);
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        testData = basePage.paxDetails.enterPaxPersonalDetails(testData,"loggedIn");
        basket = basePage.paxDetails.selectBags(testData,basket);
        basket = basePage.paxDetails.verifyBasket(basket);
        basePage.paxDetails.addBookerDetails(testData,true);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basket = basePage.seat.selectSeat(testData,basket);
        HashMap<String,String> seatDetails = basePage.seat.retrieveSeatSelectedForEachPax(testData);
        basket = basePage.seat.verifyBasket(basket);
        basePage.seat.navigateToExtrasAfterSelectingSeat();
        basePage.extras.selectService("Add golf clubs");
        basket = basePage.extras.addGolf(testData,basket);
        basePage.extras.selectService("Add skis");
        basket = basePage.extras.addSkisSnowboard(testData,basket);
        basket = basePage.extras.verifyBasket(basket);
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        String currency = basePage.payment.retrieve_verifyPaymentCurrency();
        basePage.payment.verifyBasket(basket);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifyPriceSection(basket,currency,testData);
        HashMap<String,String> bagEMD = basePage.confirmation.verifyBagsAndRetrieveEMD(testData,basket);
        System.out.println(bagEMD);
        HashMap<String,String> seatEMD = basePage.confirmation.verifySeatAllocationAndRetrieveEMD(testData,seatDetails,basket);
        System.out.println(seatEMD);
        HashMap<String,String> golfEMD = basePage.confirmation.verifyGolf_SkiAndRetrieveEMD(testData,basket,"Golf");
        System.out.println(golfEMD);
        HashMap<String,String> skiEMD = basePage.confirmation.verifyGolf_SkiAndRetrieveEMD(testData,basket,"Ski");
        System.out.println(skiEMD);
    }

    @Test(groups = {"regression","p1"})
    public void R53_BookingWithSpecialAssistanceWCHRViaPaypal(){
        HashMap testData = new DataUtility().testData("30");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        HashMap<String,Double> basket = new HashMap<>();
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basket = basePage.paxDetails.selectSpecialAssistance(testData,basket);
        basePage.paxDetails.addBookerDetails(testData);
        basket = basePage.paxDetails.verifyBasket(basket);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basket = basePage.seat.selectSeat(testData,basket);
        HashMap<String,String> seatDetails = basePage.seat.retrieveSeatSelectedForEachPax(testData);
        basket = basePage.seat.verifyBasket(basket);
        basePage.seat.navigateToExtrasAfterSelectingSeat();
        basket = basePage.extras.verifyBasket(basket);
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.verifyBasket(basket);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        basePage.payment.completePayPalCardPayment();
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifySpecialAssistance(testData,basket);
        basePage.confirmation.verifySeatAllocationAndRetrieveEMD(testData,seatDetails,basket);
    }

    @Test(groups = {"regression","p1"})
    public void R54_BookingWithSpecialAssistanceWCHSViaMasterCard() {
        HashMap testData = new DataUtility().testData("31");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
/*        HashMap<String,Double> basket = basePage.fareselect.getFareDetails(testData,false);*/
        HashMap<String,Double> basket = new HashMap<>();
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basket = basePage.paxDetails.selectSpecialAssistance(testData,basket);
        basePage.paxDetails.addBookerDetails(testData);
        basket = basePage.paxDetails.verifyBasket(basket);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basket = basePage.seat.selectSeat(testData,basket);
        HashMap<String,String> seatDetails = basePage.seat.retrieveSeatSelectedForEachPax(testData);
        basket = basePage.seat.verifyBasket(basket);
        basePage.seat.navigateToExtrasAfterSelectingSeat();
        basket = basePage.extras.verifyBasket(basket);
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.verifyBasket(basket);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifySpecialAssistance(testData,basket);
        basePage.confirmation.verifySeatAllocationAndRetrieveEMD(testData,seatDetails,basket);
    }

    @Test(groups = {"regression","p1"})
    public void R55_BookingWithSpecialAssistanceWCHCViaVisa() {
        HashMap testData = new DataUtility().testData("32");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        HashMap<String,Double> basket = basePage.fareselect.getFareDetails(testData,false);
        HashMap<String,String> flightDetails = basePage.fareselect.retrieveFlightDetails(testData);
        basePage.fareselect.verifyBasket(basket);
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basket = basePage.paxDetails.selectSpecialAssistance(testData,basket);
        basePage.paxDetails.addBookerDetails(testData);
        basket = basePage.paxDetails.verifyBasket(basket);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basket = basePage.seat.selectSeat(testData,basket);
        HashMap<String,String> seatDetails = basePage.seat.retrieveSeatSelectedForEachPax(testData);
        basket = basePage.seat.verifyBasket(basket);
        basePage.seat.navigateToExtrasAfterSelectingSeat();
        basket = basePage.extras.verifyBasket(basket);
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.verifyBasket(basket);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifySpecialAssistance(testData,basket);
    }
}
