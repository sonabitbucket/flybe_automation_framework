package test_definition.efly.online.primebooking.modifysearch;

import PageBase.EFLYOnlineBase;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.HashMap;

/**
 * Created by Indhu on 5/28/2019.
 */
public class ModifySearch {

    @Test(groups = {"regression","p2"})
    public void R46_verifyUserIsAbleToChangeSourceAndDestinationInModifySearch() {
        HashMap testData = new DataUtility().testData("27");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        testData = basePage.fareselect.changeSourceInModifySearch(testData,"yes");
        testData = basePage.fareselect.changeDestinationInModifySearch(testData,"no");
        basePage.fareselect.searchFlights();
        basePage.fareselect.verifyRoute(testData);
        basePage.fareselect.selectFlight(testData);
        HashMap<String,String> flightDetails = basePage.fareselect.retrieveFlightDetails(testData);
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifyItinerarySection(flightDetails);
    }

    @Test(groups = {"regression"})
    public void R47_changeDatesAndPassengerInModifySearchAndVerifyThePassengerOrder() {
        HashMap testData = new DataUtility().testData("29");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.changeTravelDatesInModifySearch(testData,"yes");
        testData = basePage.fareselect.changeNoOfPassenger(testData,"no");
        basePage.fareselect.searchFlights();
        basePage.fareselect.selectFlight(testData);
        basePage.fareselect.verifyThePassengerOrder(testData);
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        /*basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifyItinerarySection(flightDetails);*/
    }
}
