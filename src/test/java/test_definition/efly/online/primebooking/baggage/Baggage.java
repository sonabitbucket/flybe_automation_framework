package test_definition.efly.online.primebooking.baggage;

import PageBase.EFLYOnlineBase;
import org.testng.annotations.Test;
import utilities.DataUtility;
import java.util.HashMap;

/**
 * Created by Sona on 3/26/2019.
 */
public class Baggage {

    @Test(groups = {"regression","p2"})
    public void R18_verifyJustFlyBookingWith23KgBag2WayForAllPax(){
        HashMap testData = new DataUtility().testData("61");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        HashMap<String,Double> basket = new HashMap<>();
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basket = basePage.paxDetails.selectBags(testData,basket);
        basket = basePage.paxDetails.verifyBasket(basket);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basket = basePage.seat.verifyBasket(basket);
        basePage.seat.skipSeatsAndContinueToExtras();
        basket = basePage.extras.verifyBasket(basket);
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifyBagsAndRetrieveEMD(testData,basket);
    }

    @Test(groups = {"regression","p2"})
    public void R19_verifyJustFlyBookingWith46KgBag2WayForAllPaxEURO(){
        HashMap testData = new DataUtility().testData("63");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        HashMap<String,Double> basket = new HashMap<>();
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basket = basePage.paxDetails.selectBags(testData,basket);
        basket = basePage.paxDetails.verifyBasket(basket);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basket = basePage.seat.verifyBasket(basket);
        basePage.seat.skipSeatsAndContinueToExtras();
        basket = basePage.extras.verifyBasket(basket);
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.verifyBasket(basket);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.complete3DSecureIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifyBagsAndRetrieveEMD(testData,basket);
    }

    @Test(groups = {"regression","p2"})
    public void R20_verifyBaggageAllowanceForGetMore2WayForAllPax(){
        HashMap testData = new DataUtility().testData("62");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        HashMap<String,Double> basket = new HashMap<>();
        basket = basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData,basket);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.verifyBagOptionsDisplayedForGetMore(testData);
        basket = basePage.paxDetails.selectBags(testData,basket);
        basePage.paxDetails.verifyBasket(basket);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basket = basePage.seat.verifyBasket(basket);
        basePage.seat.skipSeatsAndContinueToExtras();
        basket = basePage.extras.verifyBasket(basket);
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.verifyBasket(basket);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        basePage.confirmation.verifyBagsAndRetrieveEMD(testData,basket);
    }

    @Test(groups = {"regression","p2"})
    public void R21_verifyBaggageAllowanceForAllIn2WayForAllPax(){
        HashMap testData = new DataUtility().testData("64");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.verifyBagOptionsDisplayedForAllIn(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
    }

    @Test(groups = {"regression","p2"})
    public void R22_verifyBaggageAllowanceForAirfrance(){
        //direct flights
        HashMap testData = new DataUtility().testData("65");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.verifyBagIsNotAvailableforAirFrance(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        //multisector flight
        testData = new DataUtility().testData("66");
        basePage.home.navigateToHomepage();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.verifyBagIsNotAvailableforAirFrance(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
    }
}
