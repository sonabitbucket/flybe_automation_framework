package test_definition.efly.online.primebooking.loyalty;

import PageBase.EFLYOnlineBase;
import org.testng.annotations.Test;
import utilities.DataUtility;
import utilities.DynamicDataProvider;
import utilities.ReportUtility;

import java.util.HashMap;

public class LoyaltyBooking {


    public void twoWay_Direct_Teen_Visa_ManageBookingTravellerDetails_AirFrance(){
        HashMap<String,String> testData = new DataUtility().testData("45");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        String[] travel_date = basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addFrequentFlyerNumber(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.changeCurrency("Pound Sterling (GBP)");
        basePage.payment.enterPaymentDetails(testData);
        String currency = basePage.payment.retrieve_verifyPaymentCurrency();
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        String lastName = basePage.confirmation.verifyAndGetReservationName(testData);
        basePage.confirmation.navigateToModifyTravellerDetails();
        basePage.mb_paxDetails.enterFrequentFlyerNumber(testData);
        basePage.mb_confirmation.verifyTravellerDetailsAndRetrieveETKT(testData);
        ReportUtility.writeExcel(8,"\\flybe_automation_report_PNRGeneration_","Sheet1","TwoWay_Direct_Teen_Visa_ManageBookingTravellerDetails_AirFrance",PNR,lastName,testData.get("frequentFlyerNumber"),travel_date[0],"-",testData.get("departTicketType"),currency,"-");
    }

    @Test
    public void oneWay_Direct_ADT_SpecialAssistance_AmericanExpress_PrimeBooking_StobartAir(){
        HashMap<String,String> testData = new DataUtility().testData("44");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        String[] travel_date = basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addFrequentFlyerNumber(testData);
        basePage.paxDetails.selectSpecialAssistance(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.changeCurrency("Pound Sterling (GBP)");
        basePage.payment.enterPaymentDetails(testData);
        String currency = basePage.payment.retrieve_verifyPaymentCurrency();
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        String lastName = basePage.confirmation.verifyAndGetReservationName(testData);
        basePage.confirmation.navigateToModifyTravellerDetails();
        basePage.mb_paxDetails.enterFrequentFlyerNumber(testData);
        basePage.mb_confirmation.verifyTravellerDetailsAndRetrieveETKT(testData);
        ReportUtility.writeExcel(8,"\\flybe_automation_report_PNRGeneration_","Sheet1","OneWay_Direct_ADT_SpecialAssitance_AmericanExpress_PrimeBooking_StobartAir",PNR,lastName,testData.get("frequentFlyerNumber"),travel_date[0],"-",testData.get("departTicketType"),currency,"-");
    }

    public void oneWay_2Sectors_ADT_INF_DinersClub_ManageBooking_TravellerDetails_NonUKtoUK(){
        HashMap<String,String> testData = new DataUtility().testData("43");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        String[] travel_date = basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addFrequentFlyerNumber(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.changeCurrency("Euro (EUR)");
        basePage.payment.enterPaymentDetails(testData);
        String currency = basePage.payment.retrieve_verifyPaymentCurrency();
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        String lastName = basePage.confirmation.verifyAndGetReservationName(testData);
        basePage.confirmation.navigateToModifyTravellerDetails();
        basePage.mb_paxDetails.enterFrequentFlyerNumber(testData);
        basePage.mb_confirmation.verifyTravellerDetailsAndRetrieveETKT(testData);
        ReportUtility.writeExcel(8,"\\flybe_automation_report_PNRGeneration_","Sheet1","OneWay_2Sectors_ADT_INF_DinersClub_ManageBooking_TravellerDetails_NonUKtoUK",PNR,lastName,testData.get("frequentFlyerNumber"),travel_date[0],"-",testData.get("departTicketType"),currency,"-");
    }

    @Test
    public void oneWay_Direct_ADT_TEEN_CHD_INF_Insurance_Visa_ManageBooking_TravellerDetails_UKtoUK(){
        HashMap<String,String> testData = new DataUtility().testData("42");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        String[] travel_date = basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addFrequentFlyerNumber(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.changeCurrency("Pound Sterling (GBP)");
        basePage.payment.addInsurance(testData);
        basePage.payment.enterPaymentDetails(testData);
        String currency = basePage.payment.retrieve_verifyPaymentCurrency();
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.complete3DSecureIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        String lastName = basePage.confirmation.verifyAndGetReservationName(testData);
        basePage.confirmation.navigateToModifyTravellerDetails();
        basePage.mb_paxDetails.enterFrequentFlyerNumber(testData);
        basePage.mb_confirmation.verifyTravellerDetailsAndRetrieveETKT(testData);
        ReportUtility.writeExcel(8,"\\flybe_automation_report_PNRGeneration_","Sheet1","OneWay_Direct_ADT_TEEN_CHD_INF_Visa_ManageBooking_TravellerDetails_UKtoUK",PNR,lastName,testData.get("frequentFlyerNumber"),travel_date[0],"-",testData.get("departTicketType"),currency,"-");
    }

    public void oneWay_Direct_ADT_INF_CHD_Insurance_Diners_ManageBooking_TravellerDetails_NonUKtoUK(){
        HashMap<String,String> testData = new DataUtility().testData("41");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        String[] travel_date = basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addFrequentFlyerNumber(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.changeCurrency("Swiss Franc (CHF)");
        basePage.payment.addInsurance(testData);
        basePage.payment.enterPaymentDetails(testData);
        String currency = basePage.payment.retrieve_verifyPaymentCurrency();
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        String lastName = basePage.confirmation.verifyAndGetReservationName(testData);
        basePage.confirmation.navigateToModifyTravellerDetails();
        basePage.mb_paxDetails.enterFrequentFlyerNumber(testData);
        basePage.mb_confirmation.verifyTravellerDetailsAndRetrieveETKT(testData);
        ReportUtility.writeExcel(8,"\\flybe_automation_report_PNRGeneration_","Sheet1","OneWay_Direct_ADT_INF_CHD_Diners_ManageBooking_TravellerDetails_NonUKtoUK",PNR,lastName,testData.get("frequentFlyerNumber"),travel_date[0],"-",testData.get("departTicketType"),currency,"Bag,Seat,Golf and Ski");
    }



    @Test
    public void oneWay_3Sectors_2ADT_Visa_PrimeBooking_UKtoUK(){
        HashMap<String,String> testData = new DataUtility().testData("40");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        String[] travel_date = basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addFrequentFlyerNumber(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.changeCurrency("Japanese Yen (JPY)");
        basePage.payment.enterPaymentDetails(testData);
        String currency = basePage.payment.retrieve_verifyPaymentCurrency();
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        String lastName = basePage.confirmation.verifyAndGetReservationName(testData);
        basePage.confirmation.verifyTravellerDetailsAndRetrieveETKT(testData);
        ReportUtility.writeExcel(8,"\\flybe_automation_report_PNRGeneration_","Sheet1","OneWay_3Sectors_2ADT_Visa_PrimeBooking_UKtoUK",PNR,lastName,testData.get("frequentFlyerNumber"),travel_date[0],"-",testData.get("departTicketType"),currency,"-");
    }


    @Test/*(dataProvider = "testData", dataProviderClass = DynamicDataProvider.class,groups = {"regression","p1"})*/
    public void oneWay_DIRECT_ADT_TEEN_MasterCard_PrimeBooking_UKtoUK(/*String frequent_flyer_number*/){
        HashMap<String,String> testData = new DataUtility().testData("38");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        String[] travel_date = basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addFrequentFlyerNumber(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        String currency = basePage.payment.retrieve_verifyPaymentCurrency();
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR();
        String lastName = basePage.confirmation.verifyAndGetReservationName(testData);
        basePage.confirmation.verifyTravellerDetailsAndRetrieveETKT(testData);
        ReportUtility.writeExcel(8,"\\flybe_automation_report_PNRGeneration_","Sheet1","OneWay_DIRECT_ADT_TEEN_MasterCard_PrimeBooking_UKtoUK",PNR,lastName,testData.get("frequentFlyerNumber"),travel_date[0],"-",testData.get("departTicketType"),currency,"-");
    }


    @Test
    public void oneWay_DIRECT_ADT_TEEN_Visa_ManageBookingTravellerDetails_EasternAirways(){
        HashMap<String,String> testData = new DataUtility().testData("39");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        String[] travel_date = basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.selectBags(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.selectSeat(testData);
        basePage.seat.navigateToExtrasAfterSelectingSeat();
        basePage.extras.continueToPayment();
        basePage.payment.changeCurrency("Euro (EUR)");
        basePage.payment.enterPaymentDetails(testData);
        String currency = basePage.payment.retrieve_verifyPaymentCurrency();
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData,"no");
        String PNR = basePage.confirmation.retrievePNR();
        String lastName = basePage.confirmation.verifyAndGetReservationName(testData);
        basePage.confirmation.navigateToModifyTravellerDetails();
        basePage.mb_paxDetails.enterFrequentFlyerNumber(testData);
        basePage.mb_confirmation.verifyTravellerDetailsAndRetrieveETKT(testData);
        ReportUtility.writeExcel(8,"\\flybe_automation_report_PNRGeneration_","Sheet1","OneWay_DIRECT_ADT_TEEN_Voucher_ManageBookingTravellerDetails_EasternAirways",PNR,lastName,testData.get("frequentFlyerNumber"),travel_date[0],"-",testData.get("departTicketType"),currency,"Bag & Seat");
    }
}
