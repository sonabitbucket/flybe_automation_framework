package test_definition.efly.online.cms;

import PageBase.EFLYOnlineBase;
import org.testng.annotations.Test;
import utilities.DataUtility;
import utilities.DynamicDataProvider;

import java.util.HashMap;

/**
 * Created by Sona on 4/12/2019.
 */
public class CMS {

    @Test(groups = {"regression","p1"})
    public void R2_TST_verifyOffersLink(){
        HashMap testData = new DataUtility().testData("51");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.handleHomeScreenPopUp(testData);
        basePage.home.clickOnTile("Offers");
        basePage.offers.verifyOffersPageNavigation();

        /*basePage.cheapFlightsTo.verifyThatDestinationIsSameAsFlightSelectedInEachTile(destination);
        HashMap details = basePage.cheapFlightsTo.selectRouteWithCurrencySpecifiedAndgetDetails(currency,destination);
        basePage.fareselect.verifyRoute(details);
        HashMap detailsWithDate = basePage.fareselect.getDateDisplayed(details);
        basePage.liveHome.navigateToLive();
        basePage.liveHome.enterSourceAndDestination(testData,detailsWithDate);
        basePage.liveHome.enterTravelDates(detailsWithDate);
        basePage.liveHome.findFlights();
        basePage.liveFareSelect.verifyOfferSelectedAndFareSelectAndSelectFlight(detailsWithDate);
        basePage.liveFareSelect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);*/
    }

    @Test(dataProvider = "testData", dataProviderClass = DynamicDataProvider.class,groups = {"regression","p1"})
    public void R3_TST_verifyPlacesWeFlyFrom(String departure,String currency){
        HashMap testData = new DataUtility().testData("51");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.handleHomeScreenPopUp(testData);
        basePage.home.selectPlaceWeFlyFrom(departure);
        basePage.cheapFlightsFrom.verifyThatDepartureIsSameAsFlightSelectedInEachTile(departure);
        basePage.cheapFlightsFrom.verifyCurrencyInEachTile(currency,departure);
        HashMap<String,String> details = basePage.cheapFlightsFrom.selectRouteAndgetDetails(departure);
        basePage.fareselect.verifyRoute(details);
        HashMap detailsWithDate = basePage.fareselect.getDateDisplayed(details);
        basePage.liveHome.navigateToLive();
        basePage.liveHome.enterSourceAndDestination(testData,detailsWithDate);
        basePage.liveHome.enterTravelDates(detailsWithDate);
        basePage.liveHome.findFlights();
        basePage.liveFareSelect.verifyOfferSelectedAndFareSelectAndSelectFlight(detailsWithDate);
        basePage.liveFareSelect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
    }

    @Test(dataProvider = "testData", dataProviderClass = DynamicDataProvider.class,groups = {"regression","p1"})
    public void R4_TST_verifyPopularDestination(String destination,String currency){
        HashMap testData = new DataUtility().testData("51");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.handleHomeScreenPopUp(testData);
        basePage.home.selectPopularDestination(destination);
        basePage.cheapFlightsTo.verifyThatDestinationIsSameAsFlightSelectedInEachTile(destination);
        HashMap details = basePage.cheapFlightsTo.selectRouteWithCurrencySpecifiedAndgetDetails(currency,destination);
        basePage.fareselect.verifyRoute(details);
        HashMap detailsWithDate = basePage.fareselect.getDateDisplayed(details);
        basePage.liveHome.navigateToLive();
        basePage.liveHome.enterSourceAndDestination(testData,detailsWithDate);
        basePage.liveHome.enterTravelDates(detailsWithDate);
        basePage.liveHome.findFlights();
        basePage.liveFareSelect.verifyOfferSelectedAndFareSelectAndSelectFlight(detailsWithDate);
        basePage.liveFareSelect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
    }

    @Test(dataProvider = "testData", dataProviderClass = DynamicDataProvider.class,groups = {"regression","p1"})
    public void R5_TST_verifyFeaturedRoutes(String tile){
        HashMap testData = new DataUtility().testData("51");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.handleHomeScreenPopUp(testData);
        basePage.home.verifyFeaturedRoutesSection();
        HashMap<String,String> details = basePage.home.selectFeaturedRouteAndGetDetails(new Integer(tile));
        basePage.fareselect.verifyRoute(details);
        HashMap detailsWithDate = basePage.fareselect.getDateDisplayed(details);
        basePage.liveHome.navigateToLive();
        basePage.liveHome.enterSourceAndDestination(testData,detailsWithDate);
        basePage.liveHome.enterTravelDates(detailsWithDate);
        basePage.liveHome.findFlights();
        basePage.liveFareSelect.verifyOfferSelectedAndFareSelectAndSelectFlight(detailsWithDate);
        basePage.liveFareSelect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
    }

    @Test(groups = {"regression","p1"})
    public void R6_TST_verifyIfFeaturedRoutes_RecentSearchPanelArePopulatedBasedOnRecentSearch(){
        HashMap testData = new DataUtility().testData("52");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        HashMap<String,String> details1 = basePage.fareselect.getRouteAndPriceDetails();
        basePage.fareselect.navigateBackToHome();
        basePage.home.verifyRecentSearchedFlightsDisplayedIsCorrect(details1,testData,1);
        basePage.home.verifyIfSearchedRouteIsFeatured(details1);
        basePage.home.navigateToLanguageVariant("FR");
        basePage.home.verifyIfSearchedRouteIsFeatured(details1);
        basePage.fareselect.navigateBackToHome();
        //second route
        testData = new DataUtility().testData("53");
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        HashMap<String,String> details2 = basePage.fareselect.getRouteAndPriceDetails();
        basePage.fareselect.navigateBackToHome();
        basePage.home.verifyRecentSearchedFlightsDisplayedIsCorrect(details2,testData,2);
        basePage.home.verifyIfSearchedRouteIsFeatured(details2);
        basePage.home.navigateToLanguageVariant("ES");
        basePage.home.verifyIfSearchedRouteIsFeatured(details2);
        basePage.fareselect.navigateBackToHome();
        //third route
        testData = new DataUtility().testData("54");
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        HashMap<String,String> details3 = basePage.fareselect.getRouteAndPriceDetails();
        basePage.fareselect.navigateBackToHome();
        basePage.home.verifyRecentSearchedFlightsDisplayedIsCorrect(details3,testData,3);
        basePage.home.verifyIfSearchedRouteIsFeatured(details3);
        basePage.home.navigateToLanguageVariant("DE");
        basePage.home.verifyIfSearchedRouteIsFeatured(details3);
        basePage.fareselect.navigateBackToHome();
        //fourth route
        testData = new DataUtility().testData("55");
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        HashMap<String,String> details4 = basePage.fareselect.getRouteAndPriceDetails();
        basePage.fareselect.navigateBackToHome();
        basePage.home.verifyRecentSearchedFlightsDisplayedIsCorrect(details4,testData,4);
        basePage.home.verifyIfSearchedRouteIsFeatured(details4);
        basePage.home.navigateToLanguageVariant("FR");
        basePage.home.verifyIfSearchedRouteIsFeatured(details4);
        basePage.fareselect.navigateBackToHome();
        //fifth route
        testData = new DataUtility().testData("56");
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        HashMap<String,String> details5 = basePage.fareselect.getRouteAndPriceDetails();
        basePage.fareselect.navigateBackToHome();
        basePage.home.verifyRecentSearchedFlightsDisplayedIsCorrect(details5,testData,5);
        basePage.home.verifyIfSearchedRouteIsFeatured(details5);
        basePage.home.navigateToLanguageVariant("ES");
        basePage.home.verifyIfSearchedRouteIsFeatured(details5);
        basePage.fareselect.navigateBackToHome();
        //sixth route
        testData = new DataUtility().testData("57");
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        HashMap<String,String> details6 = basePage.fareselect.getRouteAndPriceDetails();
        basePage.fareselect.navigateBackToHome();
        basePage.home.verifyRecentSearchedFlightsDisplayedIsCorrect(details6,testData,5);
        basePage.home.verifyIfSearchedRouteIsFeatured(details6);
        basePage.home.navigateToLanguageVariant("DE");
        basePage.home.verifyIfSearchedRouteIsFeatured(details6);
        basePage.fareselect.navigateBackToHome();
    }

    @Test(groups = {"regression","p1"})
    public void R7_TST_verifyHideAlertLinkHidesTheAlertMessage(){
        HashMap testData = new DataUtility().testData("51");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.handleHomeScreenPopUp(testData);
        basePage.home.verifyAlert();
    }

    @Test(groups = {"regression","p1"})
    public void R8_TST_verifySearchWidgetAndFeaturedRoutesOnFR(){
        HashMap testData = new DataUtility().testData("51");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.handleHomeScreenPopUp(testData);
        basePage.home.navigateToLanguageVariant("FR");
        basePage.home.verifyIfSearchWidgetIsDisplayed();
        basePage.home.verifyFeaturedRoutesSection("FR");
    }

    @Test(groups = {"regression","p1"})
    public void R9_TST_verifySearchWidgetAndFeaturedRoutesOnDE(){
        HashMap testData = new DataUtility().testData("51");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.handleHomeScreenPopUp(testData);
        basePage.home.navigateToLanguageVariant("DE");
        basePage.home.verifyIfSearchWidgetIsDisplayed();
        basePage.home.verifyFeaturedRoutesSection("DE");
    }

    @Test(groups = {"regression","p1"})
    public void R10_TST_verifySearchWidgetAndFeaturedRoutesOnES(){
        HashMap testData = new DataUtility().testData("51");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.handleHomeScreenPopUp(testData);
        basePage.home.navigateToLanguageVariant("ES");
        basePage.home.verifyIfSearchWidgetIsDisplayed();
        basePage.home.verifyFeaturedRoutesSection("ES");
    }

    @Test(groups = {"regression","p1"})
    public void R11_TST_verifyHotelCarHireAndParkingMenu() {
        HashMap testData = new DataUtility().testData("51");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.handleHomeScreenPopUp(testData);
        basePage.home.verifyClickingOnHotelsCarsMenuDisplaysAppropriateSite();
    }

    @Test(groups = {"regression","p1"})
    public void R12_TST_verifyHotelCarHireAndParkingLinks() {
        HashMap testData = new DataUtility().testData("51");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.handleHomeScreenPopUp(testData);
        basePage.home.verifyHotelCarLinks();
    }

    @Test(groups = {"regression","p1"})
    public void R13_TST_verifyFooterLink() {
        HashMap testData = new DataUtility().testData("51");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.handleHomeScreenPopUp(testData);
        basePage.home.verifyHomePageFooter(testData);
    }

    @Test(groups = {"regression","p1"})
    public void R14_TST_verifyLiveArrivalAndDepartureForOperatedByFlybe(){
        HashMap testData = new DataUtility().testData("58");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.handleHomeScreenPopUp(testData);
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        HashMap details = basePage.fareselect.getDirectFlightDetails(testData);
        basePage.fareselect.navigateBackToHome();
        basePage.home.selectHeaderMenu("Flights");
        basePage.home.selectHeaderSubMenu("Arrivals & Departures","yes");
        basePage.lad.selectdate(testData);
        basePage.lad.enterSourceAndDestinationAndSubmit(testData);
        basePage.lad.verifyLAD(details);
    }

    @Test(groups = {"regression","p1"})
    public void R15_TST_verifyLiveArrivalAndDepartureForOtherOperatorByFlightNumber() {
        HashMap testData = new DataUtility().testData("60");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.handleHomeScreenPopUp(testData);
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        HashMap details = basePage.fareselect.getDirectFlightDetails(testData);
        basePage.fareselect.navigateBackToHome();
        basePage.home.selectHeaderMenu("Flights");
        basePage.home.selectHeaderSubMenu("Arrivals & Departures","yes");
        basePage.lad.searchByFlightNumber();
        basePage.lad.selectdate(testData);
        basePage.lad.enterFlightNumberAndVerifyLAD(details);
    }

    @Test(groups = {"regression","p1"})
    public void R16_verifyTimetable(){
        HashMap testData = new DataUtility().testData("59");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.handleHomeScreenPopUp(testData);
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        int noOfFlightsAvailable = basePage.fareselect.getNoOfFlightsAvailable();
        basePage.fareselect.navigateBackToHome();
        basePage.home.selectHeaderMenu("Flights");
        //basePage.home.selectHeaderSubMenu("Flights");
        basePage.home.selectHeaderSubMenu("Flight Timetable");
        basePage.timetable.selectdate(testData);
        basePage.timetable.enterSourceAndDestinationAndSubmit(testData);
        basePage.timetable.verifyNoOfAvailableFlights(noOfFlightsAvailable);
    }

    @Test(groups = {"regression","p1"})
    public void R17_TST_verifyLinksInSubHeaderSection() {
        HashMap testData = new DataUtility().testData("51");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.handleHomeScreenPopUp(testData);
        basePage.home.selectHeaderMenu("Flights");
        basePage.home.verifyRedirectionOfSubHeaders("Flight Destinations");
        basePage.home.navigateToHomepage();
        basePage.home.selectHeaderMenu("Flights");
        basePage.home.verifyRedirectionOfSubHeaders("Route Map");
        basePage.home.navigateToHomepage();
        basePage.home.selectHeaderMenu("Flights");
        basePage.home.verifyRedirectionOfSubHeaders("Baggage");
        basePage.home.navigateToHomepage();
        basePage.home.selectHeaderMenu("Flights");
        basePage.home.verifyRedirectionOfSubHeaders("Fast Track Security");
        basePage.home.navigateToHomepage();
        basePage.home.selectHeaderMenu("Flights");
        basePage.home.verifyRedirectionOfSubHeaders("Flights to Isle of Man");
        basePage.home.navigateToHomepage();
        basePage.home.selectHeaderMenu("Help");
        basePage.home.verifyRedirectionOfSubHeaders("Special assistance");
        basePage.home.navigateToHomepage();
        basePage.home.selectHeaderMenu("Help");
        basePage.home.verifyRedirectionOfSubHeaders("Flight timetable");
        basePage.home.navigateToHomepage();
        basePage.home.selectHeaderMenu("Help");
        basePage.home.verifyRedirectionOfSubHeaders("Baggage policy");
        basePage.home.navigateToHomepage();
        basePage.home.selectHeaderMenu("Help");
        basePage.home.verifyRedirectionOfSubHeaders("Flight delays and disruptions");
        basePage.home.navigateToHomepage();
        basePage.home.selectHeaderMenu("Manage Booking");
        basePage.home.verifyRedirectionOfSubHeaders("Change your flight");
        basePage.home.navigateToHomepage();
        basePage.home.selectHeaderMenu("Manage Booking");
        basePage.home.verifyRedirectionOfSubHeaders("Add API information");
        basePage.home.navigateToHomepage();
        basePage.home.selectHeaderMenu("Manage Booking");
        basePage.home.verifyRedirectionOfSubHeaders("Ticket changes");
        basePage.home.navigateToHomepage();
        basePage.home.selectHeaderMenu("Manage Booking");
        basePage.home.verifyRedirectionOfSubHeaders("Refunds");
        basePage.home.navigateToHomepage();
        basePage.home.selectHeaderMenu("Manage Booking");
        basePage.home.verifyRedirectionOfSubHeaders("Amend car hire booking");
    }
}

