package test_definition.efly.online.digital_profile;

import PageBase.EFLYOnlineBase;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utilities.DataUtility;

import java.util.HashMap;

public class DigitalProfile {

    @Parameters("browserName")
    @Test(groups = {"regression", "p1"})
    public void R76_verify_DPCreation_ForgotPassword_Deletion(String browser) {
        HashMap testData = new DataUtility().testData("158");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.handleHomeScreenPopUp(testData);
        basePage.home.openLoginPage(testData);
        basePage.digitalProfile.create_activate_account(browser,testData);
        basePage.digitalProfile.logout();
        basePage.home.navigateToHomepage();
        basePage.home.openLoginPage(testData);
        basePage.digitalProfile.login(testData,false,browser);//to test if activation is successful
        basePage.home.navigateToMyAccount();
        basePage.digitalProfile.logout();
        basePage.home.navigateToHomepage();
        basePage.home.openLoginPage(testData);
        basePage.digitalProfile.verify_forgotPassword_activateAccount(testData,browser);
        basePage.digitalProfile.logout();
        basePage.home.navigateToHomepage();
        basePage.home.openLoginPage(testData);
        basePage.digitalProfile.login(testData,true,browser);//to test new password reset using forgot password is successful
        basePage.digitalProfile.verify_deleteProfile(testData,true,browser);
    }
}
