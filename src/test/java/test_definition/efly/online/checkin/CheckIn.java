package test_definition.efly.online.checkin;

import PageBase.EFLYOnlineBase;
import org.testng.annotations.Test;
import utilities.DataUtility;
import java.util.HashMap;

public class CheckIn {

    @Test(groups = {"regression","p1"})
    public void R59_completeCheckInForDomesticAndVerifyBoardingPass() {
        HashMap testData = new DataUtility().testData("152");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        HashMap<String,String> flightDetails = basePage.fareselect.retrieveFlightDetails(testData);
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        //basePage.payment.changeCurrency("Pound Sterling (GBP)");
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR(testData);
        String reservationName = basePage.confirmation.verifyAndGetReservationName(testData);
        HashMap<String,String> paxDetails = basePage.confirmation.verifyTravellerDetailsAndRetrieveETKT(testData);
        basePage.home.enterBookingDetailsForCheckIn();
        basePage.checkin.retrievePNR(PNR,reservationName);
        basePage.checkin.verifyTripModal(flightDetails,"outbound");
        basePage.findBooking.continueToJourneyDetailsPage(2,testData);
        basePage.journeyDetails.navigateToPassengerDetailsPage();
        basePage.boardingPass.printBoardingPass(testData,PNR,flightDetails,paxDetails,"outbound");
        basePage.boardingPass.continueToCompleteCheckin();
        basePage.checkicomplete.verifyJourneySummery(flightDetails,"outbound");
        basePage.checkicomplete.completeCheckin();
    }

    @Test(groups = {"regression","p1"})
    public void R75_completeTheCheckInForInternationalFlightAndVerifyBoardingPass() {
        HashMap testData = new DataUtility().testData("153");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        HashMap<String,String> flightDetails = basePage.fareselect.retrieveFlightDetails(testData);
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.complete3DSecureIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR(testData);
        String reservationName = basePage.confirmation.verifyAndGetReservationName(testData);
        HashMap<String,String> paxDetails = basePage.confirmation.verifyTravellerDetailsAndRetrieveETKT(testData);
        basePage.home.enterBookingDetailsForCheckIn();
        basePage.checkin.retrievePNR(PNR,reservationName);
        basePage.findBooking.continueToJourneyDetailsPage(1,testData);
        basePage.journeyDetails.navigateToPassengerDetailsPage();
        basePage.passengerDetails.populateAPIDetails(testData);
        basePage.boardingPass.printBoardingPass(testData,PNR,flightDetails,paxDetails,"outbound");
        basePage.checkicomplete.verifyJourneySummery(flightDetails,"outbound");
        basePage.boardingPass.continueToCompleteCheckin();
    }

    @Test(groups = {"regression","p1"})
    public void R69_verifyMessageForNonOnlineCheckInAirport() {
        HashMap testData = new DataUtility().testData("154");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras();
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR(testData);
        String reservationName = basePage.confirmation.verifyAndGetReservationName(testData);
        basePage.home.enterBookingDetailsForCheckIn();
        basePage.checkin.retrievePNR(PNR,reservationName);
        basePage.checkin.verifyNonOnlineCheckInMessage();
    }

    @Test(groups = {"regression","p1"})
    public void R70_verifyCheckinForAirFranceBooking() {
        HashMap testData = new DataUtility().testData("155");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras(testData);
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR(testData);
        String reservationName = basePage.confirmation.verifyAndGetReservationName(testData);
        basePage.home.enterBookingDetailsForCheckIn();
        basePage.checkin.retrievePNR(PNR,reservationName);
        basePage.checkin.verifyUserIsRedirectedToAirFranceSite();
    }

    @Test(groups = {"regression","p1"})
    public void R71_verifyCheckinForCDGDepartureBooking() {
        HashMap testData = new DataUtility().testData("156");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.home.enterSourceAndDestination(testData);
        basePage.home.enterTravelDates(testData);
        basePage.home.selectNoOfPassengers(testData);
        basePage.home.findFlights();
        basePage.fareselect.selectFlight(testData);
        basePage.fareselect.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.paxDetails.enterPaxPersonalDetails(testData);
        basePage.paxDetails.addBookerDetails(testData);
        basePage.paxDetails.selectMarketingPrefAndContinue(testData);
        basePage.seat.skipSeatsAndContinueToExtras(testData);
        basePage.extras.continueToPayment();
        basePage.payment.enterPaymentDetails(testData);
        basePage.payment.acceptTandCandCompleteBooking();
        basePage.payment.complete3DSecureIdentification(testData);
        String PNR = basePage.confirmation.retrievePNR(testData);
        String reservationName = basePage.confirmation.verifyAndGetReservationName(testData);
        basePage.home.enterBookingDetailsForCheckIn();
        basePage.checkin.retrievePNR(PNR,reservationName);
        basePage.checkin.verifyUserIsRedirectedToAirFranceSite();
    }
}
