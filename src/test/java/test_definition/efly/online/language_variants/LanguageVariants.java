package test_definition.efly.online.language_variants;

import PageBase.EFLYOnlineBase;
import org.testng.annotations.Test;
import utilities.DataUtility;
import java.util.HashMap;

public class LanguageVariants {

    @Test(groups = {"regression","p1"})
    public void R1_TST_19782_completeTheBookingAndCheckinInFrenchSite(){
        HashMap testData = new DataUtility().testData("109");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.frenchSite.handleHomeScreenPopUp(testData);
        basePage.frenchSite.navigateToLanguageVariant("FR");
        basePage.frenchSite.enterSourceAndDestination(testData);
        basePage.frenchSite.enterTravelDates(testData);
        basePage.frenchSite.selectNoOfPassengers(testData);
        basePage.frenchSite.findFlights();
        basePage.frenchSite.selectFlight(testData);
        HashMap<String,String> flightDetails = basePage.frenchSite.retrieveFlightDetails(testData);
        basePage.frenchSite.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.frenchSite.enterPaxPersonalDetails(testData);
        basePage.frenchSite.selectBags(testData);
        basePage.frenchSite.addBookerDetails(testData);
        basePage.frenchSite.selectMarketingPrefAndContinue(testData);
        basePage.frenchSite.selectSeat(testData);
        basePage.frenchSite.navigateToExtrasAfterSelectingSeat();
        basePage.frenchSite.continueToPayment();
        basePage.frenchSite.enterPaymentDetails(testData);
        basePage.frenchSite.acceptTandCandCompleteBooking();
        basePage.frenchSite.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.frenchSite.retrievePNR();
        String reservationName = basePage.frenchSite.verifyAndGetReservationName(testData);
        HashMap<String,String> paxDetails = basePage.frenchSite.verifyTravellerDetailsAndRetrieveETKT(testData);
        basePage.frenchSite.enterBookingDetailsForCheckIn();
        basePage.frenchSite.retrievePNR(PNR,reservationName);
        basePage.frenchSite.continueToJourneyDetailsPage(1,testData);
        basePage.frenchSite.navigateToPassengerDetailsPage();
        basePage.frenchSite.printBoardingPass(testData,PNR,flightDetails,paxDetails,"outbound");
        basePage.frenchSite.continueToCompleteCheckin();
        basePage.frenchSite.verifyJourneySummery(flightDetails,"outbound");
        basePage.frenchSite.completeCheckin();
    }

    @Test(groups = {"regression","p1"})
    public void R2_TST_19783_completeTheBookingAndCheckinInSpanishSite(){
        HashMap testData = new DataUtility().testData("110");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.spanishSite.handleHomeScreenPopUp(testData);
        basePage.spanishSite.navigateToLanguageVariant("ES");
        basePage.spanishSite.enterSourceAndDestination(testData);
        basePage.spanishSite.enterTravelDates(testData);
        basePage.spanishSite.selectNoOfPassengers(testData);
        basePage.spanishSite.findFlights();
        basePage.spanishSite.selectFlight(testData);
        HashMap<String,String> flightDetails = basePage.spanishSite.retrieveFlightDetails(testData);
        basePage.spanishSite.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.spanishSite.enterPaxPersonalDetails(testData);
        basePage.spanishSite.selectBags(testData);
        basePage.spanishSite.addBookerDetails(testData);
        basePage.spanishSite.selectMarketingPrefAndContinue(testData);
        basePage.spanishSite.selectSeat(testData);
        basePage.spanishSite.navigateToExtrasAfterSelectingSeat();
        basePage.spanishSite.continueToPayment();
        basePage.spanishSite.enterPaymentDetails(testData);
        basePage.spanishSite.acceptTandCandCompleteBooking();
        basePage.spanishSite.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.spanishSite.retrievePNR();
        String reservationName = basePage.spanishSite.verifyAndGetReservationName(testData);
        HashMap<String,String> paxDetails = basePage.spanishSite.verifyTravellerDetailsAndRetrieveETKT(testData);
        basePage.spanishSite.enterBookingDetailsForCheckIn();
        basePage.spanishSite.retrievePNR(PNR,reservationName);
        basePage.spanishSite.continueToJourneyDetailsPage(1,testData);
        basePage.spanishSite.navigateToPassengerDetailsPage();
        basePage.spanishSite.printBoardingPass(testData,PNR,flightDetails,paxDetails,"outbound");
        basePage.spanishSite.continueToCompleteCheckin();
        basePage.spanishSite.verifyJourneySummery(flightDetails,"outbound");
        basePage.spanishSite.completeCheckin();
    }

    @Test(groups = {"regression","p1"})
    public void R3_TST_19784_completeTheBookingAndCheckinInGermanSite(){
        HashMap testData = new DataUtility().testData("111");
        EFLYOnlineBase basePage = new EFLYOnlineBase();
        basePage.germanSite.handleHomeScreenPopUp(testData);
        basePage.germanSite.navigateToLanguageVariant("DE");
        basePage.germanSite.enterSourceAndDestination(testData);
        basePage.germanSite.enterTravelDates(testData);
        basePage.germanSite.selectNoOfPassengers(testData);
        basePage.germanSite.findFlights();
        basePage.germanSite.selectFlight(testData);
        HashMap<String,String> flightDetails = basePage.germanSite.retrieveFlightDetails(testData);
        basePage.germanSite.navigateToTravellerDetailsAfterHandlingFlexPopup(testData);
        basePage.germanSite.enterPaxPersonalDetails(testData);
        basePage.germanSite.selectBags(testData);
        basePage.germanSite.addBookerDetails(testData);
        basePage.germanSite.selectMarketingPrefAndContinue(testData);
        basePage.germanSite.selectSeat(testData);
        basePage.germanSite.navigateToExtrasAfterSelectingSeat();
        basePage.germanSite.continueToPayment();
        basePage.germanSite.enterPaymentDetails(testData);
        basePage.germanSite.acceptTandCandCompleteBooking();
        basePage.germanSite.handleInsurancePopup_Complete3DSIdentification(testData);
        String PNR = basePage.germanSite.retrievePNR();
        String reservationName = basePage.germanSite.verifyAndGetReservationName(testData);
        HashMap<String,String> paxDetails = basePage.germanSite.verifyTravellerDetailsAndRetrieveETKT(testData);
        basePage.germanSite.enterBookingDetailsForCheckIn();
        basePage.germanSite.retrievePNR(PNR,reservationName);
        basePage.germanSite.continueToJourneyDetailsPage(1,testData);
        basePage.germanSite.navigateToPassengerDetailsPage();
        basePage.germanSite.printBoardingPass(testData,PNR,flightDetails,paxDetails,"outbound");
        basePage.germanSite.continueToCompleteCheckin();
        basePage.germanSite.verifyJourneySummery(flightDetails,"outbound");
        basePage.germanSite.completeCheckin();
    }
}
