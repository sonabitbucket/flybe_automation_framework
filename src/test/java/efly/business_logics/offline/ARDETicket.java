package efly.business_logics.offline;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.TestManager;

import java.util.HashMap;
import java.util.List;

public class ARDETicket {
    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public ARDETicket() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(css = ".link.tkNbrLink.viewAction>span")
    private List<WebElement> eTktNo;

    @FindBy(css = ".ticketId")
    private WebElement ETKT;

    @FindBy(css = "span[class='pnrTitleContainer']>div")
    private WebElement newPNR;

    @FindBy(css = "table[summary='Itinerary'] tbody tr")
    private List<WebElement> flightDetails;

    @FindBy(linkText = "Change Coupon Status")
    private WebElement changeCouponStatusLink;

    //coupon status

    @FindBy(css = "input[id*='changeCouponStatus_couponNo_id_input']")
    private WebElement couponNo;

    @FindBy(css = "select[id*='changeCouponStatus_couponStatus_id_input']")
    private WebElement couponStatusfield;

    @FindBy(css = "input[id*='changeCouponStatus_reasonforChng_id_input']")
    private WebElement reasonForCouponChange;

    @FindBy(css = "input[id*='changeCouponStatus_airlineCode_id_input']")
    private WebElement airlineCode;

    @FindBy(css = "input[id*='changeCouponStatus_flightNo_id_input']")
    private WebElement flightNo;

    @FindBy(css = "input[id*='changeCouponStatus_dateOfFlight_id_input']")
    private WebElement flightDate;

    @FindBy(css = "input[id*='changeCouponStatus_airClass_id_input']")
    private WebElement flightClass;

    @FindBy(css = "input[id*='changeCouponStatus_depCity_id_input']")
    private WebElement departure;

    @FindBy(css = "input[id*='changeCouponStatus_arrCity_id_input']")
    private WebElement arrival;

    @FindBy(css = "button[id*='changeCouponButtons_okBtn_id']")
    private WebElement okButton;

    private void changeCouponStatus(HashMap<String,String> flightDetails,String couponStatus){
        for(int i=1;i<=Integer.parseInt(flightDetails.get("noOfCoupons"));i++){
            navigateToChangeCouponStatus();
            actionUtility.sendKeys(couponNo,i+"");
            actionUtility.dropdownSelect(couponStatusfield, ActionUtility.SelectionType.SELECTBYTEXT,couponStatus);
            actionUtility.sendKeys(reasonForCouponChange,"Test");
            actionUtility.sendKeys(airlineCode,flightDetails.get("flightNo"+i).split(" ")[0]);
            actionUtility.sendKeys(flightNo,flightDetails.get("flightNo"+i).split(" ")[1]);
            actionUtility.sendKeys(flightDate,flightDetails.get("flight"+i+"Date"));
            actionUtility.sendKeys(flightClass,flightDetails.get("flight"+i+"Class"));
            actionUtility.sendKeys(departure,flightDetails.get("departure"+i).split(" ")[0]);
            actionUtility.sendKeys(arrival,flightDetails.get("arrival"+i).split(" ")[0]);
            arrival.sendKeys(Keys.ENTER);
            actionUtility.hardClick(okButton);
        }

    }

    private void navigateToChangeCouponStatus(){
        try{
            actionUtility.click(changeCouponStatusLink);
            reportLogger.log(LogStatus.INFO,"successfully navigated to change coupon status page");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to change coupon status page");
            throw e;
        }
    }



    public void changeCouponStatusForEachETKT(String couponStatus,int noOfPassengers, HashMap<String,String> flightDetails, HashMap<String,String> etkt,HashMap<String,String> testData){
        try{
            for(int i=0;i<noOfPassengers;i++){
                actionUtility.click(eTktNo.get(i));
                changeCouponStatus(flightDetails,couponStatus);
                navigateToNewPNR();
            }
            reportLogger.log(LogStatus.INFO,"successfully changed coupon status for E Ticket");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to change coupon status for E Ticket");
            throw e;
        }
    }

    private void validateFlightDetails(HashMap<String,String> etkt,HashMap<String,String> selectedFlightDetails,int passengerNo,HashMap<String,String> testData){
        try{
            int noOfSectors =Integer.parseInt(testData.get("noOfSectorsDepart"));
            if(testData.get("noOfSectorsReturn")!=null) {
                noOfSectors+= Integer.parseInt(testData.get("noOfSectorsReturn"));
            }
            actionUtility.waitForElementVisible(ETKT,20);
            Assert.assertTrue(etkt.get("passenger"+passengerNo).equals(ETKT.getText()));
            for(int i=1;i<=noOfSectors;i++){
                Assert.assertTrue(flightDetails.get(i-1).findElement(By.className("depCity")).getText().equals(selectedFlightDetails.get("departure"+i).split(" ")[0]));
                Assert.assertTrue(flightDetails.get(i-1).findElement(By.className("arrCity")).getText().equals(selectedFlightDetails.get("arrival"+i)));
                Assert.assertTrue(flightDetails.get(i-1).findElement(By.className("flightNo")).getText().equals(selectedFlightDetails.get("flightNo"+i)));
                Assert.assertTrue(flightDetails.get(i-1).findElement(By.className("class")).getText().equals(selectedFlightDetails.get("classFlight"+i)));
                Assert.assertTrue(flightDetails.get(i-1).findElement(By.className("flightDate")).getText().equalsIgnoreCase(selectedFlightDetails.get("journey"+i+"Date").split(" ")[0]+selectedFlightDetails.get("journey"+i+"Date").split(" ")[1]));
                Assert.assertTrue(flightDetails.get(i-1).findElement(By.className("flightTime")).getText().equals(selectedFlightDetails.get("departureTime"+i)));
                reportLogger.log(LogStatus.INFO,"successfully verified journey details for E Ticket "+ETKT.getText());
            }
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"journey details for E Ticket "+ETKT.getText()+" doesnt match");
            throw e;
        }

    }

    private void navigateToNewPNR(){
        actionUtility.hardClick(newPNR);
    }

}
