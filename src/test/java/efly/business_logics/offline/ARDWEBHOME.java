package efly.business_logics.offline;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.DataUtility;
import utilities.TestManager;

import static utilities.ActionUtility.SelectionType.SELECTBYTEXT;

public class ARDWEBHOME {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public ARDWEBHOME() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    String loader = "//div[@class='uicLoaderOverlay uicLo-loading']";

    //office id

    @FindBy(id = "eusermanagement_logout_logo_changeOffice_id")
    private WebElement officeID;

    @FindBy(id = "eusermanagement_logout_seof_LOGIN_OFFICE_ID_id_input")
    private WebElement selectOfficeID;

    @FindBy(id = "eusermanagement_logout_seof_save_id")
    private WebElement officeIDok;

    @FindBy(id  = "uicAlertBox_ok")
    private WebElement officeIDchangeOK;

    //create PNR
    @FindBy(linkText = "Create New")
    private WebElement createPNR;

    @FindBy(linkText = "PNR")
    private WebElement PNR;

    public void changeOfficeID(String... id){
        try{
            actionUtility.waitForElementNotPresent(loader,30);
            actionUtility.waitForElementVisible(officeID,30);
            actionUtility.hardClick(officeID);
            String officeID;
            if(id.length==0) {
                officeID = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).getAsJsonObject("officeID").get("officeID1").getAsString();
            }
            else{
                officeID = id[0];
            }
            actionUtility.dropdownSelect(selectOfficeID,SELECTBYTEXT,officeID);
            actionUtility.click(officeIDok);
            actionUtility.click(officeIDchangeOK);
            reportLogger.log(LogStatus.INFO,"successfully changed Office ID to "+officeID);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to change Office ID");
            throw e;
        }
    }

    public void createPNR(){
        //clicks on create PNR and then PNR
        try{
            actionUtility.hardClick(createPNR);
            actionUtility.click(PNR);

            reportLogger.log(LogStatus.INFO,"successfully clicked on create PNR and then PNR button to create a new booking");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to create PNR");
            throw e;
        }
    }
}
