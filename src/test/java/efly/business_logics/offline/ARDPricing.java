package efly.business_logics.offline;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.TestManager;

import java.util.HashMap;
import java.util.List;

public class ARDPricing {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public ARDPricing() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(css = "input[id*='_generalOptionsSection_otherCurr_id_input']")
    private WebElement convertFareIn;

    @FindBy(css = "button[title='Click to update fares']")
    private WebElement updateFares;

    @FindBy(css = "button[title='Add to Price Summary']")
    private WebElement addToPriceSummary;

    @FindBy(css= "input[id*='PRICE_AS_BOOKED']")
    private WebElement priceAsBooked;

    @FindBy(css= "input[id*='BEST_PRICER']")
    private WebElement bestPricer;

    @FindBy(css= "input[id*='LOWEST_FARE']")
    private WebElement lowestFare;

    @FindBy(css= "input[id*='fareType_3']")
    private WebElement corporate;

    @FindBy(css= "input[id*='fareType_2']")
    private WebElement negoPriv;

    @FindBy(css= "input[id*='fareType_1']")
    private WebElement published;

    @FindBy(css = "th[class='thTotalPtc selectPtc uicUnSelectable'] span[id*='totalPtc']")
    private WebElement totalFareInAvailableFares;

    @FindBy(css = "div[id*='_total'] span[class='right']")
    private WebElement totalFareInSummary;

    @FindBy(css = "button[id*='pricingSummary_confirmPrice_id']")
    private WebElement confirmPrice;

    //PTC
    @FindBy(css = "td[class='ptc'] a")
    private List<WebElement> ptc;

    /*@FindBy(css = "input[id*='_passengersOptions_discount_1']")
    private WebElement ptcField;*/

    private String ptc_selectPax = "//a[text()='%s']";

    @FindBy(xpath = "//tr[td[a[text()='ADT']]]//following-sibling::tr[@class='discountRow']//input[contains(@id,'discount') and not (contains(@id,'INF'))]")
    private List<WebElement> adt_ptcCode;

    @FindBy(xpath = "//tr[td[a[text()='ADT']]]//following-sibling::tr[@class='discountRow']//input[contains(@id,'discount') and (contains(@id,'INF'))] ")
    private List<WebElement> inf_ptcCode;

    String ptc_code = "//tr[td[a[text()='%s']]]//following-sibling::tr[@class='discountRow']//input[contains(@id,'discount')]";

    private String fareType = "//span[label[text()='%s']]/input";

    private String pricingMode = "//li[label[contains(text(),'%s')]]/input";

    private String fareToBeSelected = "//tr[td[a[contains(text(),'%s')][not (contains(@title,'%s'))]]]/td[contains(@class,'selectPtc')]//input";

    public void changePricingMode(String pricing_mode){
        //Price as booked,Best Pricer,lowest fare
        try{
            TestManager.getDriver().findElement(By.xpath(String.format(pricingMode,pricing_mode)));
            reportLogger.log(LogStatus.INFO,"successfully changed pricing mode to "+pricing_mode);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to change pricing mode to "+pricing_mode);
            throw e;
        }
    }

    public void changeFareType(String fare){
        //"Nego/Priv.",Corporate
        try{
            TestManager.getDriver().findElement(By.xpath(String.format(fareType,"Published")));
            TestManager.getDriver().findElement(By.xpath(String.format(fareType,fare)));
            reportLogger.log(LogStatus.INFO,"successfully changed faretype to "+fare);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to change faretype to "+fare);
            throw e;
        }
    }

    public void selectFromAvailableFares(HashMap<String,String> testData){
        try{
            int noOfPax = 0;
            List<WebElement> availableFares = TestManager.getDriver().findElements(By.xpath(String.format(fareToBeSelected,testData.get("fareToBeSelected"),testData.get("fareToBeSelected"))));
            if(testData.get("noOfAdults")!=null && Integer.parseInt(testData.get("noOfAdults"))>0){
                noOfPax++;
            }
            if(testData.get("noOfTeens")!=null){
                noOfPax++;
            }
            if(testData.get("noOfChild")!=null){
                noOfPax++;
            }
            if(testData.get("noOfInfants")!=null){
                noOfPax++;
            }
            /*int noOfFlights = Integer.parseInt(testData.get("noOfSectorsDepart"));
            if(testData.get("noOfSectorsReturn")!=null) {
                noOfFlights += Integer.parseInt(testData.get("noOfSectorsReturn"));
            }*/

            System.out.println("no. of pax "+noOfPax+"     no. of checkbox "+availableFares.size());
            for(int i=0;i<noOfPax;i++){
                /*for(int j=0;j<noOfFlights;j++)*/
                System.out.println("checkbox "+i+" -- "+availableFares.get(i));
                if(!actionUtility.verifyIfCheckboxIsSelected(availableFares.get(i))) {
                    actionUtility.selectOption(availableFares.get(i));
                }
            }
            reportLogger.log(LogStatus.INFO,"successfully selected "+testData.get("fareToBeSelected")+" from available fares in pricing");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select "+testData.get("fareToBeSelected")+" from available fares in pricing");
            throw e;
        }
    }

    private void addPTCPerPax(String pax,String code,int noOfPax){
        //pax - ADT,INF,YTH,CHD
        List<WebElement> ptcPax = TestManager.getDriver().findElements(By.xpath(String.format(ptc_selectPax,pax)));
        List<WebElement> ptc = null;

        for(int i=0;i<noOfPax;i++){
            actionUtility.click(ptcPax.get(i));

            if(pax.equals("YTH") || pax.equals("CHD")) {
                ptc = TestManager.getDriver().findElements(By.xpath(String.format(ptc_code, pax)));
            }
            else if(pax.equals("ADT")){
                ptc = adt_ptcCode;
            }
            else{
                ptc = inf_ptcCode;
            }
            actionUtility.sendKeys(ptc.get(i),code);
        }
    }

    public void addPTCcode(HashMap<String,String> testData,String... flow){
        try{
            if(flow.length>0 && flow[0].equalsIgnoreCase("gds")){
                if(testData.get("noOfTeens")!= null){
                    addPTCPerPax("YTH","BNN",Integer.parseInt(testData.get("noOfTeens")));
                }
                if (testData.get("noOfChild") != null) {

                    addPTCPerPax("CHD", "WBC", Integer.parseInt(testData.get("noOfChild")));
                }
            }
            else {
                String code = "WEB";
                if (testData.get("noOfAdults") != null) {
                    addPTCPerPax("ADT", code, Integer.parseInt(testData.get("noOfAdults")));
                }
                if (testData.get("noOfInfants") != null) {
                    addPTCPerPax("INF", code, Integer.parseInt(testData.get("noOfInfants")));
                }
                if (testData.get("noOfTeens") != null) {
                    code = "BNN";
                    addPTCPerPax("YTH", code, Integer.parseInt(testData.get("noOfTeens")));
                }
                if (testData.get("noOfChild") != null) {
                    code = "WBC";
                    addPTCPerPax("CHD", code, Integer.parseInt(testData.get("noOfChild")));
                }
            }
            reportLogger.log(LogStatus.INFO,"successfully added PTC for passengers");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to add PTC for passengers");
            throw e;
        }
    }

    public String change_Retrieve_Currency(HashMap<String,String> testData){
        try{
            String currency = "GBP";
            if(testData.get("currency")!=null) {
                currency = testData.get("currency");
                actionUtility.sendKeys(convertFareIn, currency);
            }
            //actionUtility.click(updateFares);

            reportLogger.log(LogStatus.INFO,"successfully set currency as "+testData.get("currency"));
            return currency;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to set currency to "+testData.get("currency"));
            throw e;
        }
    }

    public void selectPricingOption(HashMap<String,String> testData){
        //selects options Price As Booked or Best Pricer or Lowest Fare
        String pricingOption = testData.get("pricingOption");
        try {
            if (pricingOption.equalsIgnoreCase("Price as booked")) {
                actionUtility.selectOption(priceAsBooked);
            } else if (pricingOption.equalsIgnoreCase("Best Pricer")) {
                actionUtility.selectOption(bestPricer);
            } else if (pricingOption.equalsIgnoreCase("Lowest Fare")) {
                actionUtility.selectOption(lowestFare);
            }
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select pricing option "+pricingOption);
            throw e;
        }
    }

    public void selectFareType(HashMap<String,String> testData){

        String fareType = testData.get("fareType");
        try {

            if (fareType.equalsIgnoreCase("Published")) {
                //actionUtility.unSelectOption(negoPriv);
                actionUtility.selectOption(published);
            } else if (fareType.equalsIgnoreCase("Nego/Priv.")) {
                actionUtility.unSelectOption(published);
                actionUtility.selectOption(negoPriv);
            } else if (fareType.equalsIgnoreCase("Corporate")) {
                actionUtility.unSelectOption(published);
                actionUtility.selectOption(corporate);
            }
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to fare type "+fareType);
            throw e;
        }
    }

    public void updateFares(){
        try{
            actionUtility.click(updateFares);
            reportLogger.log(LogStatus.INFO,"successfully updated fares");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to update fares");
            throw e;
        }
    }

    public void addToPriceSummary(){
        try{
            actionUtility.waitForElementVisible(addToPriceSummary,30);
            actionUtility.click(addToPriceSummary);
            reportLogger.log(LogStatus.INFO,"successfully added to price summary");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to add to price summary");
            throw e;
        }
    }

    public String verifyPriceSummaryAndConfirmPrice()
    {
        try{
            Assert.assertTrue(totalFareInAvailableFares.getText().equals(totalFareInSummary.getText()));
            actionUtility.click(confirmPrice);
            return totalFareInSummary.getText();
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"total fare in price summary section doesn't match with the total fares in available fares section");
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify price summary");
            throw e;
        }
    }

}
