package efly.business_logics.offline;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.CommonUtility;
import utilities.DataUtility;
import utilities.TestManager;

import java.util.HashMap;

public class ARDManagePayment {
    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public ARDManagePayment() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(css = "td[class='paymentTypeColumn'] select")
    private WebElement paymentType;

    @FindBy(css = "span[id*='commonCurrency'] span input")
    private WebElement totalAmount;

    @FindBy(xpath = "//span[contains(text(),'Calculate Payment')]")
    private WebElement calculatePayment;

    @FindBy(xpath = "//table[@class='paymentSolution table-view']/tfoot//td[span]")
    private WebElement appliedAmount;

    @FindBy(css = "button[title='Apply the payment to PNR']")
    private WebElement applyPayment;

    @FindBy(css = "input[id*='callerDetails_id_input']")
    private WebElement callerDetails;

    @FindBy(css = "div[class='flowButtonBar'] button[title='Issue Document(s)']")
    private WebElement issueDoc;

    @FindBy(xpath = "//td[@class='paymentDetailColumn']//span[label[text()='Credit Card Type']]/select")
    private WebElement creditcardType;

    @FindBy(xpath = "//span[label[text()='Credit Card Number']]//input")
    private WebElement creditCardNumber;

    @FindBy(xpath = "//span[label[text()='Expiry Date']]//input")
    private WebElement expiryDate;

    @FindBy(xpath = "//span[label[contains(text(),'Cardholder')]]//input")
    private WebElement cardHoldersName;

    @FindBy(xpath = "//span[label[text()='CVV']]//input")
    private WebElement cvv;

    @FindBy(xpath = "//span[label[text()='Name']]//input")
    private WebElement name_billingAddress;

    @FindBy(xpath = "//span[label[text()='Address 1']]//input")
    private WebElement address1_BillingAddress;

    @FindBy(xpath = "//span[label[text()='City']]//input")
    private WebElement city_BillingAddress;

    @FindBy(xpath = "//span[label[text()='Country']]//input")
    private WebElement country_BillingAddress;

    @FindBy(xpath = "//span[label[text()='Details']]//input")
    private WebElement invoiceDetails;

    @FindBy(xpath = "//span[label[text()='PayPal Reference ID']]//input")
    private WebElement paypalRefID;

    @FindBy(css = "button[title='Close the popup and redisplay the PNR Details.']")
    private WebElement closePaypal;

    public void managePayment(HashMap<String,String> testData,String... totalFare){
        try{
            String paymentMethod = testData.get("paymentType");
            actionUtility.dropdownSelect(paymentType, ActionUtility.SelectionType.SELECTBYTEXT,paymentMethod);
            if(paymentMethod.equalsIgnoreCase("Credit Card")){
                paymentByCreditcard(testData);
            }
            else if(paymentMethod.equalsIgnoreCase("Invoice")){
                actionUtility.sendKeys(invoiceDetails,testData.get("invoiceDetails"));
            }
            else if(paymentMethod.equalsIgnoreCase("Paypal")){
                actionUtility.sendKeys(paypalRefID,testData.get("paypalRefID"));
            }
            if(totalFare.length>0 && !totalFare[0].equals("ssr")) {
                try {
                    Assert.assertTrue(totalAmount.getAttribute("value").equals(totalFare[0]));
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.INFO, "total value displayed doesn't match with the total fare displayed on Pricing page for booking method " + paymentMethod);
                    throw e;
                }
            }
            actionUtility.click(calculatePayment);
            if(totalFare.length>0 && !totalFare[0].equals("ssr")) {
                try {
                    Assert.assertTrue(appliedAmount.getText().equals(CommonUtility.removeCommaFromCurrency(totalFare[0])));
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.INFO, "total value displayed in Applied(GBP) column doesn't match with the total fare displayed on Pricing page");
                    throw e;
                }
            }
            actionUtility.waitForElementVisible(applyPayment,900);
            actionUtility.scrollToViewElement(applyPayment);
            actionUtility.click(applyPayment);
            if(!paymentMethod.equalsIgnoreCase("Paypal")) {
                actionUtility.sendKeys(callerDetails, testData.get("callerDetails"));
                actionUtility.scrollToViewElement(issueDoc);
                actionUtility.click(issueDoc);
                actionUtility.hardSleep(3000);
            }
            else{
                actionUtility.click(closePaypal);
            }
            if(totalFare.length>0 && totalFare[0].equals("ssr")){
                actionUtility.click(issueDoc);
                actionUtility.hardSleep(5000);
            }
            reportLogger.log(LogStatus.INFO,"successfully updated payment details");
            //actionUtility.hardSleep(9000);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to do update payment details");
            throw e;
        }
    }

    private void paymentByCreditcard(HashMap<String,String> testData){
        try{
            String creditCardType = testData.get("creditCardType");
            if(!creditCardType.equals("American Express")){
                actionUtility.dropdownSelect(creditcardType,ActionUtility.SelectionType.SELECTBYTEXT,creditCardType);
            }
            String creditCardNo = DataUtility.getJsonData(DataUtility.readConfig("card.details.name")).getAsJsonObject("ard").getAsJsonObject(creditCardType.toLowerCase()).get("cardNumber").getAsString();
            actionUtility.sendKeys(creditCardNumber,creditCardNo);

            String expiryDt = DataUtility.getJsonData(DataUtility.readConfig("card.details.name")).getAsJsonObject("ard").getAsJsonObject(creditCardType.toLowerCase()).get("expiryDate").getAsString();
            actionUtility.sendKeys(expiryDate,expiryDt);

            String cardHolderName = DataUtility.getJsonData(DataUtility.readConfig("card.details.name")).getAsJsonObject("ard").getAsJsonObject(creditCardType.toLowerCase()).get("nameOnCard").getAsString();
            actionUtility.sendKeys(cardHoldersName,cardHolderName);

            if(!creditCardType.equalsIgnoreCase("UATP")) {
                String cVV = DataUtility.getJsonData(DataUtility.readConfig("card.details.name")).getAsJsonObject("ard").getAsJsonObject(creditCardType.toLowerCase()).get("securityNumber").getAsString();
                actionUtility.sendKeys(cvv, cVV);
            }
            actionUtility.sendKeys(name_billingAddress,testData.get("billingAddressName"));
            actionUtility.sendKeys(address1_BillingAddress,testData.get("billingAddress1"));
            actionUtility.sendKeys(city_BillingAddress,testData.get("billingAddressCity"));
            actionUtility.sendKeys(country_BillingAddress,testData.get("billingAddressCountry"));
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to pay by credit card");
            throw e;
        }
    }
}
