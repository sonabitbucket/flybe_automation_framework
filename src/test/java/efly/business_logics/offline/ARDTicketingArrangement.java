package efly.business_logics.offline;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.TestManager;

public class ARDTicketingArrangement {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public ARDTicketingArrangement() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(css = "button[title='Add Ticketing Arrangement']")
    private WebElement addTicketingArrangemnt;

    @FindBy(xpath = "//div[div[div[div[div[form[div[button[@title='Add Ticketing Arrangement']]]]]]]]/div[@class='popupButtonsContainer uicBorder']/button")
    private WebElement ticketArrangementOK;

    public void addTicketingArrangemnt(){
        try{
            actionUtility.click(addTicketingArrangemnt);
            actionUtility.waitForElementVisible(ticketArrangementOK,30);
            actionUtility.hardClick(ticketArrangementOK);
            reportLogger.log(LogStatus.INFO,"successfully added ticketing arrangement");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to add ticketing arrangement");
            throw e;
        }
    }


}
