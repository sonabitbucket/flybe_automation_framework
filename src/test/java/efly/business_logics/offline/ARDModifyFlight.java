package efly.business_logics.offline;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.TestManager;

import java.util.HashMap;
import java.util.List;

public class ARDModifyFlight {
    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public ARDModifyFlight() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(css = "table[class='editItineraryTable'] tr[id*='segmentRow'] td[class*='airlineFlightNumber'] input")
    private WebElement flightNo;

    @FindBy(css = ".default.departureTime")
    private WebElement departureTime;

    @FindBy(css = ".default.arrivalTime")
    private WebElement arrivalTime;

    @FindBy(css = "table[class='editItineraryTable'] tr[id*='segmentRow'] td[class='beginDate'] input")
    private List<WebElement> changeDate;

    @FindBy(css = "td[class='actions'] button[id*='editItineraryDetails_applyButton']")
    private WebElement apply;

    @FindBy(xpath = "//div[h2[text()='The following information messages were returned']]/ul")
    private WebElement revalidationMessage;

    @FindBy(xpath = "//div[h2[text()='The following warning(s) occured']]/ul")
    private WebElement pnrUpdatedMessage;

    @FindBy(css = "button[id*='editItinCloseButton_id']")
    private WebElement close;

    @FindBy(xpath = "//div[div[@class='text']]//div[contains(@class,'alternativesData')]")
    private List<WebElement> alternatives;

    @FindBy(xpath = "//td[@class='bkgClass'][span[@class='selected']]/span")
    private List<WebElement> classToBeSelected;

    @FindBy(css = "tr[id*='segmentRow'] td[class='class'] input")
    private WebElement classBooked;

    @FindBy(xpath = "//div[div[@class='text']]//div[@class='apply']/button")
    private WebElement alternativeApply;

    //change route
    @FindBy(xpath = "//em[text()='Search alternative flight']")
    private WebElement searchAlternative;

    @FindBy(xpath = "//span[input[contains(@id,'editItineraryDetails_locationDC')]]")
    private WebElement edit_Departure;

    @FindBy(xpath = "//span[input[contains(@id,'editItineraryDetails_locationAC')]]")
    private WebElement edit_Destination;

    String flightToBeSelected = "//tr[td[1][@rowspan='%s']]/td[@class='bkgClass']/span";


    public HashMap<String,String> modifyDate(HashMap<String,String> testData,HashMap<String,String> travel_date){
        try{

            int noOfDepartSectors = 0;
            if(testData.get("changeDepartureDate")!=null) {
                noOfDepartSectors = Integer.parseInt(testData.get("noOfSectorsDepart"));
                String date[] = actionUtility.getDate(Integer.parseInt(testData.get("changeDepartureDate")));
                for (int i=0;i<noOfDepartSectors;i++) {
                    actionUtility.sendKeys(changeDate.get(i), date[0] + date[1] + date[2]);
                    changeDate.get(i).sendKeys(Keys.RETURN);
                    actionUtility.hardSleep(5000);
                    if (actionUtility.verifyIfElementIsDisplayed(alternatives.get(i))) {
                        String classSelected = classBooked.getAttribute("value");
                        for (int j = 0; j < classToBeSelected.size(); j++) {
                            if (classToBeSelected.get(i).getText().split("")[0].equals(classSelected)) {
                                actionUtility.click(classToBeSelected.get(j));
                                actionUtility.click(alternativeApply);
                                break;
                            }
                        }
                    }
                    actionUtility.hardClick(apply);
                    actionUtility.hardSleep(2000);

                }
                travel_date.put("date1",date[0] + date[1] + date[2]);
            }

            if(testData.get("changeReturnDate")!=null) {
                int noOfReturnSectors = Integer.parseInt(testData.get("noOfSectorsReturn"));
                String date[] = actionUtility.getDate(Integer.parseInt(testData.get("changeReturnDate")));
                for (int i = noOfDepartSectors; i < noOfReturnSectors+noOfDepartSectors; i++) {
                    actionUtility.sendKeys(changeDate.get(i), date[0] + date[1] + date[2]);
                    changeDate.get(i).sendKeys(Keys.RETURN);
                    actionUtility.hardSleep(5000);
                    if (actionUtility.verifyIfElementIsDisplayed(alternatives.get(i))) {
                        String classSelected = classBooked.getAttribute("value");
                        for (int j = 0; j < classToBeSelected.size(); j++) {
                            if (classToBeSelected.get(i).getText().split("")[0].equals(classSelected)) {
                                actionUtility.click(classToBeSelected.get(j));
                                actionUtility.click(alternativeApply);
                                break;
                            }
                        }
                    }
                    actionUtility.hardClick(apply);
                    actionUtility.hardSleep(2000);
                }
                travel_date.put("date2",date[0] + date[1] + date[2]);
            }
            actionUtility.hardSleep(1000);
            actionUtility.waitForElementVisible(revalidationMessage,10);
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(revalidationMessage));
            actionUtility.hardClick(close);
            actionUtility.waitForPageLoad(30);
            reportLogger.log(LogStatus.INFO,"successfully did itinerary change through modify flight link");
            return travel_date;
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"ticket revalidation/reissue recommendation and/or verify pnr content message is not displayed");
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to do itinerary change");
            throw e;
        }
    }

    public void modifyRoute(HashMap<String,String> testData){
        try{
            actionUtility.click(searchAlternative);
            if(testData.get("changeDeparture")!=null) {
                actionUtility.sendKeys(edit_Departure, testData.get("changeDeparture"));
            }
            if(testData.get("changeDestination")!=null) {
                actionUtility.sendKeys(edit_Destination, testData.get("changeDestination"));
            }
            actionUtility.click(TestManager.getDriver().findElement(By.xpath(String.format(flightToBeSelected,testData.get("noOfSectorsDepart")))));
            reportLogger.log(LogStatus.INFO,"successfully modified route");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to modify route");
            throw e;
        }
    }


    private HashMap<String,String> updateNewFlightDetails(String date,HashMap<String,String> selectedFlightDetails,String... flightNo){ // todo: multisector/return
        try{

            selectedFlightDetails.put("journey1Date",date);
            if(flightNo.length>0){
                selectedFlightDetails.put("flightNo1",flightNo[0]);
                selectedFlightDetails.put("departureTime1",flightNo[1]);
                selectedFlightDetails.put("arrivalTime1",flightNo[2]);
            }
            return selectedFlightDetails;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to capture new flight details after itinerary change");
            throw e;
        }
    }

}
