package efly.business_logics.offline;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.TestManager;

import java.util.HashMap;

public class ARDRevalidateETKT {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public ARDRevalidateETKT() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(css = "table[Summary='Select the Sectors for which you want to revalidate coupons'] div[class*='sectorChkbox uicCheckBoxBR'] input")
    private WebElement selectSectors;

    @FindBy(xpath = "//ul[li[span[contains(text(),'All E-tickets')]]]//input")
    private WebElement selectAllPassengers;

    @FindBy(css = "button[id*='revalidatetktPanelBtn_nextBtn_id']")
    private WebElement nextStep;

    @FindBy(xpath = "//div[ul[li[contains(text(),'Revalidation request successfully processed')]]]/h2[text()='Success Message']")
    private WebElement successMsg;

    @FindBy(css = "button[id*='revalidatetktPanelBtn_revalidateBtn_id']")
    private WebElement revalidate;

    @FindBy(css = "input[id*='etktRevalidatePopup_sectorCheck_id_input']")
    private WebElement sectorCheck;

    @FindBy(css = "input[id*='etktRevalidatePopup_selectCoupons_id_input")
    private WebElement couponCheck;

    @FindBy(css = "input[name='callerDetails']")
    private WebElement callerDetails;


    public void revalidate(int noOfPassengers, HashMap<String,String> testData){
        try {
            if (noOfPassengers > 1) {
                actionUtility.selectOption(selectAllPassengers);
                actionUtility.hardClick(nextStep);
            } else {
                actionUtility.selectOption(sectorCheck);
                actionUtility.selectOption(couponCheck);
            }
            actionUtility.sendKeys(callerDetails,testData.get("callerDetails"));
            actionUtility.hardClick(revalidate);
            actionUtility.hardSleep(1000);
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(successMsg));
            reportLogger.log(LogStatus.INFO,"successfully revalidated");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to revalidate");
            throw e;
        }
    }

}
