package efly.business_logics.offline;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.TestManager;

import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;

public class ARDFlightSearch {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public ARDFlightSearch() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(id = "_yuiResizeMonitor")
    private WebElement frameID;

    //Search flights

    @FindBy(xpath = "//div[@class='searchBounds']/table/tbody")
    private WebElement searchFlights;

    @FindBy(css = ".uicButton.search")
    private WebElement searchFlightsButton;

    @FindBy(xpath = "//div[@class='bookingButtonBar']//button[span[strong[text()='Book']]]")
    private List<WebElement> bookFlight;

    @FindBy(css = "input[id*='_id_seats']")
    private WebElement seatsRequired;

    @FindBy(name = "searchType")
    private WebElement selectSearchType;

    String sectorType = "//li[label[text()='%s']]/input";

    @FindBy(xpath = "//form//table[contains(@id,'airSearch_boundsTable_id')]/tbody//td[contains(@id,'_airSearch_boundsTable_id_')]//input")
    private WebElement dateARNK;

    @FindBy(css = "button[title='Add sector(s) to the itinerary']")
    private WebElement addARNK;

    //Book Flights

    @FindBy(css = "button[id*='buttonMoreFlights']")
            private List<WebElement> moreFlightsOption;

    String outboundFlightToBeSelected = "//div[contains(@id,'_availContainer_0')]//tr[td[1][@rowspan='%s']]/td[@class='bkgClass']/span";

    String inboundFlightToBeSelected = "//div[contains(@id,'_availContainer_1')]//tr[td[1][@rowspan='%s']]/td[@class='bkgClass']/span";

    String outboundCodeShare = "//div[contains(@id,'_availContainer_0')]//tr[td[a[span[@class='infoIcon']]]][td[1][@rowspan='%s']]/td[@class='bkgClass']/span";

    String inboundCodeShare = "//div[contains(@id,'_availContainer_1')]//tr[td[a[span[@class='infoIcon']]]][td[1][@rowspan='%s']]/td[@class='bkgClass']/span";

    @FindBy(xpath = "//div[contains(@id,'_availContainer_0')]//tr[td[span[@class='selected']]]/td[@class='bkgClass']/span[@class='selected']")
    private List<WebElement> outBoundSelectedClass;

    @FindBy(xpath = "//div[contains(@id,'_availContainer_0')]//tr[td[span[@class='selected']]]/td[@class='bkgClass']/span")
    private List<WebElement> outBoundAvailableClasses;

    @FindBy(xpath = "//div[contains(@id,'_availContainer_1')]//tr[td[span[@class='selected']]]/td[@class='bkgClass']/span[@class='selected']")
    private List<WebElement> inBoundSelectedClass;

    @FindBy(xpath = "//div[contains(@id,'_availContainer_1')]//tr[td[span[@class='selected']]]/td[@class='bkgClass']/span")
    private List<WebElement> inBoundAvailableClasses;

    @FindBy(xpath = "//tr[td[span[@class='selected']]]")
    private List<WebElement> selectedFlightRow; //select flight row in flight select and book section

    String classtoBeBooked = "input[id*='id_%d_bkgClass']";

    //Basket

    @FindBy(css = ".itineraryList")
    private WebElement itinerarySection;

    @FindBy(css = "div[class='segmentInfo']")
    private List<WebElement> itinerary;

    @FindBy(css = "a[title='Click to Add/Update Passengers']")
    private WebElement addUpdatePassengers;

    @FindBy(xpath = "//div[text()='Passenger and Contact Information']")
    private WebElement passengerAndContactInformationSection;

    //Add/Update Passengers section
    @FindBy(css = ".paxTitle")
    private WebElement passengers;

    private String passengerName = "div[class*='paxId-%d']";

    public HashMap<String,String> enterJourneyDetails(HashMap<String,String> testData){
        try{
            HashMap<String,String> travel_date = new HashMap<>();
            int noOfSegments = 1;

            if(testData.get("return")!=null && testData.get("return").equalsIgnoreCase("yes")){
                noOfSegments = 2;
            }
            actionUtility.waitForElementVisible(searchFlights,30);
            List<WebElement> dataRows = searchFlights.findElements(By.tagName("tr"));
            for(int i=1;i<=noOfSegments;i++) {
                List<WebElement> dataField = dataRows.get(i).findElements(By.tagName("td"));
                try {
                    actionUtility.hardSleep(10);
                    WebElement source = dataField.get(0).findElement(By.tagName("input"));
                    if(i>1){
                        actionUtility.click(source);
                        actionUtility.hardSendKeys(source, testData.get("source_" + i));
                    }
                    else {
                        actionUtility.sendKeys(source, testData.get("source_" + i));
                    }
                    reportLogger.log(LogStatus.INFO, "successfully entered destination" + testData.get("source_" + i));
                    actionUtility.hardSleep(3000);
                } catch (StaleElementReferenceException e) {
                    WebElement source = dataField.get(0).findElement(By.tagName("input"));
                    actionUtility.hardSendKeys(source, testData.get("source_" + i));
                    reportLogger.log(LogStatus.INFO, "successfully entered destination" + testData.get("source_" + i));
                }
                try {
                    actionUtility.hardSleep(10);
                    WebElement destination = dataField.get(1).findElement(By.tagName("input"));
                    actionUtility.click(destination);
                    actionUtility.hardSendKeys(destination, testData.get("destination_" + i));
                    reportLogger.log(LogStatus.INFO, "successfully entered destination" + testData.get("destination_" + i));
                } catch (StaleElementReferenceException e) {
                    WebElement destination = dataField.get(1).findElement(By.tagName("input"));
                    actionUtility.sendKeys(destination, testData.get("destination_" + i));
                    reportLogger.log(LogStatus.INFO, "successfully entered destination" + testData.get("destination_" + i));
                }
                try {
                    WebElement bookingClass = dataField.get(4).findElement(By.tagName("input"));
                    actionUtility.click(bookingClass);
                    actionUtility.sendKeysByAction(bookingClass, testData.get("class_" + i));
                    reportLogger.log(LogStatus.INFO, "successfully entered class" + testData.get("class_" + i));
                } catch (TimeoutException e) {
                    WebElement bookingClass = dataField.get(4).findElement(By.tagName("input"));
                    actionUtility.click(bookingClass);
                    actionUtility.sendKeysByAction(bookingClass, testData.get("class_" + i));
                    reportLogger.log(LogStatus.INFO, "successfully entered class" + testData.get("class_" + i));
                }

                try {
                    WebElement calender = dataField.get(2).findElement(By.tagName("input"));
                    calender.clear();
                    String date[] = actionUtility.getDate(Integer.parseInt(testData.get("departureOffset_" + i)),"d/MMM/yy");
                    actionUtility.hardSendKeys(calender, date[0] + date[1] + date[2]);
                    reportLogger.log(LogStatus.INFO, "successfully entered date of journey " + date[0] + date[1] + date[2] + "for flight" + i);
                    travel_date.put("date"+i,date[0] + date[1] + date[2]);

                } catch (StaleElementReferenceException e) {
                    WebElement calender = dataField.get(2).findElement(By.tagName("input"));
                    String date[] = actionUtility.getDate(Integer.parseInt(testData.get("departureOffset_" + i)));
                    System.out.println("Date Entered" + date[0] + date[1] + date[2]);
                    actionUtility.sendKeys(calender, date[0] + date[1] + date[2]);
                    reportLogger.log(LogStatus.INFO, "successfully entered date of journey " + date[0] + date[1] + date[2] + "for flight" + i);
                    travel_date.put("date"+i,date[0] + date[1] + date[2]);
                }
            }
                enterNoOfSeats(testData);
                actionUtility.click(searchFlightsButton);
                reportLogger.log(LogStatus.INFO,"able to search flights for the journey details provided");
                return travel_date;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to enter journey details and search for flight options in flight search section");
            throw e;
        }
    }

    private void enterNoOfSeats(HashMap<String,String> testData){
        try{
            String noOfSeatsRequired = "1";
            if(testData.get("noOfSeatsToBeBooked")!=null){
                noOfSeatsRequired = testData.get("noOfSeatsToBeBooked");
            }
            actionUtility.sendKeys(seatsRequired,noOfSeatsRequired);
            reportLogger.log(LogStatus.INFO,"successfully added number of seats needed");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to add number of seats required");
            throw e;
        }
    }

    public /*HashMap<String,String>*/ void selectAndBookFlight/*AndRetrieveBookedFlightDetails*/(HashMap<String,String> testData,String... codeShare){
        try{
            WebElement flight = null;
            try {
                if(codeShare.length>0){
                    flight = TestManager.getDriver().findElements(By.xpath(String.format(outboundCodeShare, testData.get("noOfSectorsDepart")))).get(0);
                }
                else {
                    flight = TestManager.getDriver().findElements(By.xpath(String.format(outboundFlightToBeSelected, testData.get("noOfSectorsDepart")))).get(0);
                }
            }catch(IndexOutOfBoundsException e){
                moreFlightsOption.get(0).click();
                if(codeShare.length>0){
                    flight = TestManager.getDriver().findElements(By.xpath(String.format(outboundCodeShare, testData.get("noOfSectorsDepart")))).get(0);
                }
                else {
                    flight = TestManager.getDriver().findElements(By.xpath(String.format(outboundFlightToBeSelected, testData.get("noOfSectorsDepart")))).get(0);
                }
            }
            actionUtility.hardClick(flight);
            actionUtility.click(bookFlight.get(0));

            actionUtility.waitForElementVisible(itinerarySection,5);
            if(testData.get("return")!=null && testData.get("return").equalsIgnoreCase("yes")){
                actionUtility.hardSleep(3000);
                try {
                    if(codeShare.length>0) {
                        flight = TestManager.getDriver().findElements(By.xpath(String.format(inboundCodeShare, testData.get("noOfSectorsReturn")))).get(0);
                    }
                    else{
                        flight = TestManager.getDriver().findElements(By.xpath(String.format(inboundFlightToBeSelected, testData.get("noOfSectorsReturn")))).get(0);
                    }
                }catch(IndexOutOfBoundsException e){
                    moreFlightsOption.get(1).click();
                    if(codeShare.length>0) {
                        flight = TestManager.getDriver().findElements(By.xpath(String.format(inboundCodeShare, testData.get("noOfSectorsReturn")))).get(0);
                    }
                    else{
                        flight = TestManager.getDriver().findElements(By.xpath(String.format(inboundFlightToBeSelected, testData.get("noOfSectorsReturn")))).get(0);
                    }
                }
                actionUtility.hardClick(flight);
                actionUtility.click(bookFlight.get(1));
            }
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select and book flight");
            throw e;
        }
    }

    private HashMap<String,String> captureBookedFlightDetails(HashMap<String,String> testData){ //todo multisector and return

        HashMap<String,String> selectedFlightDetails = new HashMap<String,String>();
        String[] journeyDate = actionUtility.getDate(Integer.parseInt(testData.get("departureOffset_1")));
        int noOfSectorsDepart = Integer.parseInt(testData.get("noOfSectorsDepart"));
        int noOfSectorsReturn = Integer.parseInt(testData.get("noOfSectorsReturn"));

        for (int i = 1; i <= noOfSectorsDepart; i++) {//retrieves journey date for each sector in departure
            selectedFlightDetails.put("journey" + i + "Date", journeyDate[0] + " " + journeyDate[1] + " " + journeyDate[2]);
        }
        if(testData.get("return")!=null && testData.get("return").equalsIgnoreCase("yes")) {
            journeyDate = actionUtility.getDate(Integer.parseInt(testData.get("departureOffset_2")));
            for (int i = Integer.parseInt(testData.get("noOfSectorsDepart")) + 1; i <= noOfSectorsDepart + noOfSectorsReturn; i++) {
                // retrieves journey date for each sector in return
                selectedFlightDetails.put("journey" + i + "Date", journeyDate[0] + " " + journeyDate[1] + " " + journeyDate[2]);
            }

        }

        for(int i=0;i<selectedFlightRow.size();i++) {
            //retrives overall details
            List<WebElement> selectedFlightData = selectedFlightRow.get(i).findElements(By.tagName("td"));
            if(testData.get("return")!=null && testData.get("return").equalsIgnoreCase("yes")) {
                if ((Integer.parseInt(testData.get("noOfSectorsDepart")) > 1 || Integer.parseInt(testData.get("noOfSectorsReturn")) > 1) && i % 2 != 0) {
                    //retrieves flight details for second sector for both departure and return
                    selectedFlightDetails.put("flightNo" + (i + 1), selectedFlightData.get(0).getText());
                    selectedFlightDetails.put("classFlight" + (i + 1), selectedFlightData.get(1).getText().split("")[0]);
                    selectedFlightDetails.put("departure" + (i + 1), selectedFlightData.get(2).getText());
                    selectedFlightDetails.put("arrival" + (i + 1), selectedFlightData.get(3).getText().split(" ")[0]);
                    selectedFlightDetails.put("departureTime" + (i + 1), selectedFlightData.get(4).getText());
                    selectedFlightDetails.put("arrivalTime" + (i + 1), selectedFlightData.get(5).getText());
                    selectedFlightDetails.put("aircraft" + (i + 1), selectedFlightData.get(7).findElement(By.tagName("span")).getText());
                }
                else{
                    //retrieves flight details for first sector for both departure and return
                    selectedFlightDetails.put("flightNo" + (i + 1), selectedFlightData.get(1).getText());
                    selectedFlightDetails.put("classFlight" + (i + 1), selectedFlightData.get(2).getText().split("")[0]);
                    selectedFlightDetails.put("departure" + (i + 1), selectedFlightData.get(3).getText());
                    selectedFlightDetails.put("arrival" + (i + 1), selectedFlightData.get(4).getText().split(" ")[0]);
                    selectedFlightDetails.put("departureTime" + (i + 1), selectedFlightData.get(5).getText());
                    selectedFlightDetails.put("arrivalTime" + (i + 1), selectedFlightData.get(6).getText());
                    //selectedFlightDetails.put("flightDuration" + (i + 1), selectedFlightData.get(8).getText());
                    selectedFlightDetails.put("aircraft" + (i + 1), selectedFlightData.get(9).findElement(By.tagName("span")).getText());
                }
            }else {
                if ((Integer.parseInt(testData.get("noOfSectorsDepart")) > 1) && i % 2 != 0) {
                    //retrieves flight details for second sector for departure if no return
                    selectedFlightDetails.put("flightNo" + (i + 1), selectedFlightData.get(0).getText());
                    selectedFlightDetails.put("classFlight" + (i + 1), selectedFlightData.get(1).getText().split("")[0]);
                    selectedFlightDetails.put("departure" + (i + 1), selectedFlightData.get(2).getText());
                    selectedFlightDetails.put("arrival" + (i + 1), selectedFlightData.get(3).getText().split(" ")[0]);
                    selectedFlightDetails.put("departureTime" + (i + 1), selectedFlightData.get(4).getText());
                    selectedFlightDetails.put("arrivalTime" + (i + 1), selectedFlightData.get(5).getText());
                    selectedFlightDetails.put("aircraft" + (i + 1), selectedFlightData.get(7).findElement(By.tagName("span")).getText());
                }
                else{
                    //retrieves flight details for first sector for departure if return
                    selectedFlightDetails.put("flightNo" + (i + 1), selectedFlightData.get(1).getText());
                    selectedFlightDetails.put("classFlight" + (i + 1), selectedFlightData.get(2).getText().split("")[0]);
                    selectedFlightDetails.put("departure" + (i + 1), selectedFlightData.get(3).getText());
                    selectedFlightDetails.put("arrival" + (i + 1), selectedFlightData.get(4).getText().split(" ")[0]);
                    selectedFlightDetails.put("departureTime" + (i + 1), selectedFlightData.get(5).getText());
                    selectedFlightDetails.put("arrivalTime" + (i + 1), selectedFlightData.get(6).getText());
                    //selectedFlightDetails.put("flightDuration" + (i + 1), selectedFlightData.get(8).getText());
                    selectedFlightDetails.put("aircraft" + (i + 1), selectedFlightData.get(9).findElement(By.tagName("span")).getText());
                }
            }
        }
        System.out.println(selectedFlightDetails);
        return selectedFlightDetails;
    }

    public void verifyItinerarySection(HashMap<String,String> selectedFlightDetails,HashMap<String,String> testData){ //todo multisector and return
        try{
            int totalSectors = Integer.parseInt(testData.get("noOfSectorsDepart"));
            if(testData.get("return")!=null && testData.get("return").equalsIgnoreCase("yes")){
                totalSectors+= Integer.parseInt(testData.get("noOfSectorsReturn"));
            }
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(itinerarySection));
            System.out.println("itinerary before "+itinerary.size());
            if(itinerary.size()!= totalSectors){

            }
            for(int i=0;i<totalSectors;i++) {
                System.out.println("sector no "+i);
                List<WebElement> itineraryDetails = itinerary.get(i).findElements(By.tagName("div"));
                System.out.println("itineraryDetails "+itineraryDetails.size());
                Assert.assertTrue(itineraryDetails.get(0).getText().contains(selectedFlightDetails.get("flightNo"+(i+1)).split(" ")[0] + selectedFlightDetails.get("flightNo"+(i+1)).split(" ")[1]));
                Assert.assertTrue(itineraryDetails.get(0).getText().contains(selectedFlightDetails.get("departure"+(i+1)).split(" ")[0]));
                Assert.assertTrue(itineraryDetails.get(0).getText().contains(selectedFlightDetails.get("arrival"+(i+1))));
                Assert.assertTrue(itineraryDetails.get(1).getText().toLowerCase().contains((selectedFlightDetails.get("journey"+(i+1)+"Date").split(" ")[0] + selectedFlightDetails.get("journey"+(i+1)+"Date").split(" ")[1]).toLowerCase()));
                Assert.assertTrue(itineraryDetails.get(2).getText().contains(selectedFlightDetails.get("departureTime"+(i+1)) + " - " + selectedFlightDetails.get("arrivalTime"+(i+1))));
                Assert.assertTrue(itineraryDetails.get(3).getText().contains(selectedFlightDetails.get("classFlight"+(i+1))));
            }
            reportLogger.log(LogStatus.INFO,"successfully verified the itinerary section for booked flight details");
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"itinerary section for booked flight is not displayed or the itinerary information displayed is incorrect");
            throw e;
        }
        catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify itinerary section for booked flight");
            throw e;
        }
    }

    public void navigateToAddPassenger(){
        try{
            actionUtility.hardSleep(5000);
            actionUtility.click(addUpdatePassengers);
            actionUtility.waitForElementVisible(passengerAndContactInformationSection,5);
            reportLogger.log(LogStatus.INFO,"successfully navigated to passenger and contact information");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to passenger and contact information");
            throw e;
        }
    }

    public void verifyPassengerDetailsAfterAddingPassengers(int noOfPassenger,HashMap<String,String> passengerNames){
        try{
            actionUtility.waitForElementVisible(passengers,30);
            for(int i=1;i<=noOfPassenger;i++){
                WebElement passengerNameElement = TestManager.getDriver().findElement(By.cssSelector(String.format(passengerName,i)));
                System.out.println(passengerNameElement.getText());
                if(passengerNameElement.getText().contains("ADT"))
                Assert.assertTrue(passengerNameElement.getText().toLowerCase().contains(i+" "+passengerNames.get("adult"+i).split(" ")[2].toLowerCase()+" / "+passengerNames.get("adult"+i).split(" ")[1].toLowerCase()+" "+passengerNames.get("adult"+i).split(" ")[0].toLowerCase()));
            }
        }
        catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"passenger names in add/update passenger section doesnt match with names added in Add Passenger and Contact Information");
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify passenger names in add/update passenger section");
            throw e;
        }
    }

    public void selectSearchTypeAndAddDetails(HashMap<String,String> testData,String... searchType){
        try{
            actionUtility.dropdownSelect(selectSearchType, ActionUtility.SelectionType.SELECTBYTEXT,searchType[0]);
            reportLogger.log(LogStatus.INFO,"successfully selected search type "+searchType+" in \"Your Search Option\"");
            if(searchType[0].equals("Ghost, Passive & Information Sector Sell")){
                actionUtility.selectOption(TestManager.getDriver().findElement(By.xpath(String.format(sectorType,searchType[1]))));
                if(searchType[1].equals("Information (ARNK)")){
                    enterSearchOptions(testData,searchType[1]);
                    actionUtility.click(addARNK);
                }

            }
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select search type "+searchType+" in \"Your Search Option\"");
            throw e;
        }
    }

    private void enterSearchOptions(HashMap<String,String> testData,String sectorType){
        try{
            if(sectorType.equals("Information (ARNK)")){
                String date[] = actionUtility.getDate(Integer.parseInt(testData.get("departureOffset_2")));
                actionUtility.waitForElementVisible(dateARNK,10);
                actionUtility.sendKeys(dateARNK,date[0]+date[1]+date[2]);
            }
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to enter search options");
            throw e;
        }

    }

}
