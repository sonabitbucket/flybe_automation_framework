package efly.business_logics.offline;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.DataUtility;
import utilities.TestManager;

import java.util.HashMap;

public class ARDWEBLogin {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public ARDWEBLogin() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(id = "eusermanagement_login_logi_USER_ALIAS_id_input")
    private WebElement userName;

    @FindBy(id = "eusermanagement_login_logi_PASSWORD_id_input")
    private WebElement password;

    @FindBy(id = "eusermanagement_login_logi_signin_id")
    private WebElement login;

    @FindBy(id = "uicAlertBox_ok")
    private WebElement gdpr;

    public void login(HashMap<String,String> testData){
        // digitalProfile with valid credentials
        String userId = null;
        try {
            if(testData.get("loginUserName")!=null && testData.get("loginPassword")!=null){
                userName.clear();
                userName.sendKeys(testData.get("loginUserName"));
                password.clear();
                password.sendKeys(testData.get("loginPassword"));
                userId = testData.get("loginUserName");
            }
            else {

                userName.clear();
                String userNameValue = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).get("userName").getAsString();
                String passwordValue = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).get("password").getAsString();
                userName.sendKeys(userNameValue);
                password.clear();
                password.sendKeys(passwordValue);
                userId = userNameValue;
            }
            actionUtility.click(login);
            actionUtility.click(gdpr);
            actionUtility.hardSleep(10);
            reportLogger.log(LogStatus.INFO,"successfully logged in with user id - "+userId);
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to digitalProfile");
            throw e;
        }
    }
}
