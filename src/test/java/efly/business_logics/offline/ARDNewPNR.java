package efly.business_logics.offline;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.Logs;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import sun.rmi.runtime.Log;
import utilities.ActionUtility;
import utilities.TestManager;

import java.util.HashMap;
import java.util.List;

public class ARDNewPNR {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public ARDNewPNR() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(css = "span[class='pnrTitleContainer']>div")
    private WebElement newPNR;

    @FindBy(linkText = "Set Ticketing Arrangement")
    private WebElement setTicketingArrangement;

    @FindBy(css = "button[title='End of Transaction']")
    private WebElement endOfTransaction;

    @FindBy(css = "input[name='RECEIVED_FROM']")
    private WebElement callerDetails;

    @FindBy(css = "input[name='callerDetails']")
    private WebElement revalidate_callerDetails;

    @FindBy(css = "button[title='End transaction']")
    private WebElement eotOK;

    @FindBy(css = "button[id*='eotWarningSctn_forceBtn_id']")
    private WebElement eotForce;

    //@FindBy(xpath = "//div[div[div[div[text()='End of Transaction']]]]")
    private String eotPopup = "//div[div[div[div[text()='End of Transaction']]]]";

    @FindBy(linkText = "Pricing")
    private WebElement pricing;

    @FindBy(linkText = "Manage Payment")
    private WebElement managePayment;

    @FindBy(linkText = "Add Service")
    private WebElement addService;

    @FindBy(css = "button[title='Acknowledge']")
    private WebElement hazardousMaterialAcknowledgement;

    @FindBy(css = ".link.tkNbrLink.viewAction>span")
    private List<WebElement> eTktNo;

    @FindBy(xpath = "//div[h2[text()='Information Message']]/ul")
    private WebElement ticketingInfoMessage;

    @FindBy(linkText = "Modify Flights")
    private WebElement modifyFlights;

    @FindBy(linkText = "Add/Modify")
    private WebElement addModifyLink;

    @FindBy(css = "div[id*='itineraryDetails_itineraryTable_id_sw'] tbody tr")
    private List<WebElement> ItineraryDetails;

    @FindBy(linkText = "Revalidate E-Tickets")
    private WebElement reValidateETKT;

    @FindBy(css = "a[title='Close']")
    private WebElement closePNRTab;

    @FindBy(id = "esearch_dataquicksearchflat_complex_id_input")
    private WebElement searchPNR;

    @FindBy(xpath = "//button[span[strong[text()='Retrieve']]]")
    private WebElement retrievePNRButton;

    @FindBy(linkText = "Re-Price/Change E-tickets Automatically")
    private WebElement repriceAutomatically;

    //itinerary

    @FindBy(css = "a[class*='flightInfoLink']")
    private List<WebElement> flightNos;

    @FindBy(css = "td[class='departureCol']")
    private List<WebElement> departure;

    @FindBy(css = "td[class='arrivalCol']")
    private List<WebElement> arrival;

    @FindBy(css = "td[class='dateCol']")
    private List<WebElement> flight_date;

    @FindBy(css = "td[class='classCol']")
    private List<WebElement> flight_class;

    public void navigateToRepriceAutomatically(){
        try{
            actionUtility.click(repriceAutomatically);
            reportLogger.log(LogStatus.INFO,"successfully navigated to reprice");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to reprice");
            throw e;
        }
    }

    public HashMap<String,String> retrieveFlightDetails(HashMap<String,String> testData){
        try{
            HashMap<String,String> flightDetails = new HashMap<>();
            int noOfCoupons = Integer.parseInt(testData.get("noOfSectorsDepart"));
            if(testData.get("return")!=null && testData.get("return").equals("yes")){
                noOfCoupons += Integer.parseInt(testData.get("noOfSectorsReturn"));
            }
            flightDetails.put("noOfCoupons",noOfCoupons+"");
            for(int i=1;i<=noOfCoupons;i++){
                flightDetails.put("flightNo"+i,flightNos.get(i-1).getText());
                flightDetails.put("departure"+i,departure.get(i-1).getText().trim());
                flightDetails.put("arrival"+i,arrival.get(i-1).getText().trim());
                flightDetails.put("flight"+i+"Date",flight_date.get(i-1).getText().trim());
                flightDetails.put("flight"+i+"Class",flight_class.get(i-1).getText().trim());
            }
            reportLogger.log(LogStatus.INFO,"successfully captured itinerary details");
            return flightDetails;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to capture itinerary details");
            throw e;
        }
    }

    public void searchPNR(String pnr){
        try{
            actionUtility.hardSleep(3000);
            actionUtility.sendKeys(searchPNR,pnr);
            actionUtility.hardClick(retrievePNRButton);
            reportLogger.log(LogStatus.INFO,"successfully searched and retrieved PNR "+pnr);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable search and retrieve PNR "+pnr);
            throw e;
        }
    }

    public void closePNRTab(){
        try{
            actionUtility.hardClick(closePNRTab);
            reportLogger.log(LogStatus.INFO,"successfully closed PNR tab");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to close PNR tab");
            throw e;
        }
    }

    public void clickOnNewPNR(){
        try{
            actionUtility.hardClick(newPNR);
            reportLogger.log(LogStatus.INFO,"successfully clicked new PNR tab");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to click on new PNR");
            throw e;
        }
    }

    public void setSetTicketingArrangement(){
        try{
            actionUtility.click(setTicketingArrangement);
            reportLogger.log(LogStatus.INFO,"successfully clicked on set ticketing arrangement");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to click on set ticketing arrangement");
            throw e;
        }
    }

    public void setEndOfTransaction(HashMap<String,String> testData,String... forceEOT){
        try{
            actionUtility.hardClick(endOfTransaction);
            actionUtility.sendKeys(callerDetails,testData.get("callerDetails"));
            actionUtility.click(eotOK);
            if(forceEOT.length>0 && actionUtility.verifyIfElementIsDisplayed(eotForce)){
                actionUtility.click(eotForce);
            }
            reportLogger.log(LogStatus.INFO,"successfully clicked on end of transaction");
            reportLogger.log(LogStatus.INFO,"successfully entered contact details for end of transaction");
            actionUtility.waitForElementNotPresent(eotPopup,10);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to do end of transaction");
            throw e;
        }
    }

    public void verifyTicketingInformationMessageDisplayed(){
        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(ticketingInfoMessage));
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"ticketing information message is not displayed");
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable verify if ticketing information message is displayed");
            throw e;
        }
    }

    public String retrievePNRgeneratedOnEOT(){
        try{
            actionUtility.hardSleep(10000);
            String pnr = newPNR.getText().split(" ")[1];
            reportLogger.log(LogStatus.INFO,"PNR generated on end of transaction "+pnr);
            if(pnr.length()!=6){
                throw new RuntimeException("unable to generate PNR");
            }
            System.out.println(pnr);
            return pnr;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to retrieve PNR at end of transaction");
            throw e;
        }
    }

    public void navigateToPricing(){
        try{
            actionUtility.click(pricing);
            reportLogger.log(LogStatus.INFO,"successfully navigated to pricing page");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to pricing page");
            throw e;
        }
    }

    public void navigateToAddService(){
        try{
            actionUtility.click(addService);
            reportLogger.log(LogStatus.INFO,"successfully navigated to add service");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to add service");
            throw e;
        }
    }

    public void navigateToManagePayment(String... acknowledged){
        try{
            actionUtility.click(managePayment);
            if(!(acknowledged.length>0)) {
                actionUtility.click(hazardousMaterialAcknowledgement);
            }
            reportLogger.log(LogStatus.INFO,"successfully navigated to manage booking after acknowledging hazardous material warning");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to manage booking");
            throw e;
        }
    }

    public HashMap<String, String> retrieveETKT(int noOfPassengers){
        try{
            actionUtility.hardSleep(10000);
            actionUtility.waitForElementVisible(eTktNo,500);
            HashMap<String,String> etkt = new HashMap<>();
            for(int i=0;i<noOfPassengers;i++){
                etkt.put("passenger"+(i+1),eTktNo.get(i).getText());
            }
            reportLogger.log(LogStatus.INFO,"successfully generated E-TKT for all "+noOfPassengers+" passengers "+etkt);
            return etkt;
        }catch(Exception e){
            throw new RuntimeException("unable to generate E-TKT No.");
        }
    }

    public void navigateToModifyFlights(){
        try{
            actionUtility.click(modifyFlights);
            reportLogger.log(LogStatus.INFO,"successfully navigated to modify flights");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to modify flights");
            throw e;
        }
    }

    public void verifyItineraryDetails(String ticketingStatus,HashMap<String,String> selectedFlight,HashMap<String,String> testData){ // todo: verifying DAY
        String itinerary = "";
        try{
            actionUtility.hardSleep(5000);
            int noOfSectors =Integer.parseInt(testData.get("noOfSectorsDepart"));
            if(testData.get("noOfSectorsReturn")!=null) {
                noOfSectors+= Integer.parseInt(testData.get("noOfSectorsReturn"));
            }
            for(int i=1;i<=noOfSectors;i++) {
                itinerary = "Flight No. for segment "+i;
                Assert.assertTrue(ItineraryDetails.get(i-1).findElement(By.className("sectorCol")).getText().contains(selectedFlight.get("flightNo"+i)));
                itinerary = "Class for segment "+i;
                Assert.assertTrue(ItineraryDetails.get(i-1).findElement(By.className("classCol")).getText().equals(selectedFlight.get("classFlight"+i)));
                itinerary = "Journey Date for segment "+i;
                Assert.assertTrue(ItineraryDetails.get(i-1).findElement(By.className("dateCol")).getText().equalsIgnoreCase(selectedFlight.get("journey"+i+"Date").split(" ")[0]+selectedFlight.get("journey"+i+"Date").split(" ")[1]));
                itinerary = "Departure for segment "+i;
                Assert.assertTrue(ItineraryDetails.get(i-1).findElement(By.className("departureCol")).getText().equals(selectedFlight.get("departure"+i)));
                itinerary = "Arrival for segment "+i;
                Assert.assertTrue(ItineraryDetails.get(i-1).findElement(By.className("arrivalCol")).getText().contains(selectedFlight.get("arrival"+i)));
                itinerary = "Departure Time for segment "+i;
                Assert.assertTrue(ItineraryDetails.get(i-1).findElement(By.className("depTimeCol")).getText().equals(selectedFlight.get("departureTime"+i)));
                itinerary = "Arrival Time for segment "+i;
                Assert.assertTrue(ItineraryDetails.get(i-1).findElement(By.className("arrTimeCol")).getText().equals(selectedFlight.get("arrivalTime"+i)));
                itinerary = "Ticketing status for segment "+i;
                Assert.assertTrue(ItineraryDetails.get(i-1).findElement(By.className("ticketStatusCol")).getText().equalsIgnoreCase(ticketingStatus));
                /*itinerary = "Flight Duration for segment "+i;
                Assert.assertTrue(ItineraryDetails.get(i-1).findElement(By.className("fligthDurationCol")).getText().contains(selectedFlight.get("flightDuration"+i)));*/
                itinerary = "Aircraft for segment "+i;
                Assert.assertTrue(ItineraryDetails.get(i-1).findElement(By.className("aircraftCol")).getText().equals(selectedFlight.get("aircraft"+i)));
                reportLogger.log(LogStatus.INFO,"successfully verified itinerary details for segment "+i);
            }
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"itinerary detail: "+itinerary+" displayed is not correct");
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify itinerary details");
            throw e;
        }
    }

    public void navigateToRevalidateETKT(){
        try{
            actionUtility.click(reValidateETKT);
            reportLogger.log(LogStatus.INFO,"successfully navigated to revalidation");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to modify flights");
            throw e;
        }
    }

    public void navigateToAddORmodify(){
        try{
            actionUtility.click(addModifyLink);
            reportLogger.log(LogStatus.INFO,"successfully navigated to search flights by clicking on Add/Modify link");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate search flights by clicking on Add/Modify link");
            throw e;
        }
    }

}
