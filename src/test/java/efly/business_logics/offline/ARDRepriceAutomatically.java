package efly.business_logics.offline;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.DataUtility;
import utilities.TestManager;

import java.util.HashMap;
import java.util.List;

public class ARDRepriceAutomatically {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public ARDRepriceAutomatically() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(css = "button[id*='nextBtn'][title='Go to next step']")
    private WebElement nextStep;

    @FindBy(css = "input[name*='masterCheckbox'][id*='emanualreissueflows']")
    private WebElement pax_checkbox;

    @FindBy(css = "td[class='fareType'] input[type='checkbox']")
    private List<WebElement> fareType;

    @FindBy(xpath = "//button[span[strong[text()='Reprice']]]")
    private WebElement reprice;

    @FindBy(css = "button[title='Apply changes and go next step']")
    private WebElement confirm_reprice;

    //payment
    @FindBy(css = "td[class='paymentTypeColumn'] select")
    private WebElement paymentType;

    @FindBy(css = "span[id*='commonCurrency'] span input")
    private WebElement totalAmount;

    @FindBy(xpath = "//span[contains(text(),'Calculate Payment')]")
    private WebElement calculatePayment;

    @FindBy(xpath = "//span[contains(text(),'Arrange FOP')]")
    private WebElement arrangeFOP;

    @FindBy(css = "button[title='Apply the payment to PNR']")
    private WebElement applyPayment;

    @FindBy(css = "input[id*='callerDetails_id_input']")
    private WebElement callerDetails;

    @FindBy(css = "button[title='Issue the selected document(s)']")
    private WebElement issueDoc;

    @FindBy(xpath = "//td[@class='paymentDetailColumn']//span[label[text()='Credit Card Type']]/select")
    private WebElement creditcardType;

    @FindBy(xpath = "//span[label[text()='Credit Card Number']]//input")
    private WebElement creditCardNumber;

    @FindBy(xpath = "//span[label[text()='Expiry Date']]//input")
    private WebElement expiryDate;

    @FindBy(xpath = "//span[label[contains(text(),'Cardholder')]]//input")
    private WebElement cardHoldersName;

    @FindBy(xpath = "//span[label[text()='CVV']]//input")
    private WebElement cvv;

    @FindBy(xpath = "//span[label[text()='Name']]//input")
    private WebElement name_billingAddress;

    @FindBy(xpath = "//span[label[text()='Address 1']]//input")
    private WebElement address1_BillingAddress;

    @FindBy(xpath = "//span[label[text()='City']]//input")
    private WebElement city_BillingAddress;

    @FindBy(xpath = "//span[label[text()='Country']]//input")
    private WebElement country_BillingAddress;

    @FindBy(xpath = "//span[label[text()='Details']]//input")
    private WebElement invoiceDetails;

    @FindBy(xpath = "//span[label[text()='Payment Type 1']]/select")
    private WebElement previousPaymentType;

    @FindBy(xpath = "//span[label[text()='Card Type']]/select")
    private WebElement previousCreditCard;

    @FindBy(xpath = "//div[ul[li[contains(text(),'Issue e-Doc request succeeded for the given passenger')]]]/h2[text()='Success Message']")
    private WebElement successMsg;

    public void reprice(HashMap<String,String> testData){
        try{
            selectEtkt();
            repriceItinerary(testData);
            managePayment(testData);
            issueDoc(testData);
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(successMsg));
            reportLogger.log(LogStatus.INFO,"successfully able to re-price itinerary");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to re-price itinerary");
            throw e;
        }
    }

    private void selectEtkt(){
        //select etkt
        //actionUtility.click(pax_checkbox);
        actionUtility.click(nextStep);
    }

    private void repriceItinerary(HashMap<String,String> testData){
        //reprice itinerary
        String fare = testData.get("fareType");
        if (fare.equalsIgnoreCase("Nego/Priv.")) {
            actionUtility.unSelectOption(fareType.get(0));
            actionUtility.selectOption(fareType.get(1));
        } else if (fare.equalsIgnoreCase("Corporate")) {
            actionUtility.unSelectOption(fareType.get(0));
            actionUtility.selectOption(fareType.get(2));
        }
        actionUtility.click(reprice);
        actionUtility.hardSleep(5000);
        actionUtility.click(confirm_reprice);
    }

    private void managePayment(HashMap<String,String> testData){
        String paymentMethod = testData.get("paymentType");
        actionUtility.dropdownSelect(previousPaymentType, ActionUtility.SelectionType.SELECTBYTEXT,paymentMethod);
        actionUtility.dropdownSelect(paymentType, ActionUtility.SelectionType.SELECTBYTEXT,paymentMethod);
        if(paymentMethod.equalsIgnoreCase("Credit Card")){
            paymentByCreditcard(testData);
        }
        actionUtility.click(calculatePayment);
        actionUtility.click(arrangeFOP);
        actionUtility.hardSleep(5000);
        actionUtility.hardClick(nextStep);
    }

    private void paymentByCreditcard(HashMap<String,String> testData){
        try{
            String creditCardType = testData.get("creditCardType");
            if(!creditCardType.equals("American Express")){
                actionUtility.dropdownSelect(previousCreditCard,ActionUtility.SelectionType.SELECTBYTEXT,creditCardType);
                actionUtility.dropdownSelect(creditcardType,ActionUtility.SelectionType.SELECTBYTEXT,creditCardType);
            }
            String creditCardNo = DataUtility.getJsonData(DataUtility.readConfig("card.details.name")).getAsJsonObject("ard").getAsJsonObject(creditCardType.toLowerCase()).get("cardNumber").getAsString();
            actionUtility.sendKeys(creditCardNumber,creditCardNo);

            String expiryDt = DataUtility.getJsonData(DataUtility.readConfig("card.details.name")).getAsJsonObject("ard").getAsJsonObject(creditCardType.toLowerCase()).get("expiryDate").getAsString();
            actionUtility.sendKeys(expiryDate,expiryDt);

            String cardHolderName = DataUtility.getJsonData(DataUtility.readConfig("card.details.name")).getAsJsonObject("ard").getAsJsonObject(creditCardType.toLowerCase()).get("nameOnCard").getAsString();
            actionUtility.sendKeys(cardHoldersName,cardHolderName);

            if(!creditCardType.equalsIgnoreCase("UATP")) {
                String cVV = DataUtility.getJsonData(DataUtility.readConfig("card.details.name")).getAsJsonObject("ard").getAsJsonObject(creditCardType.toLowerCase()).get("securityNumber").getAsString();
                actionUtility.sendKeys(cvv, cVV);
            }
            actionUtility.sendKeys(name_billingAddress,testData.get("billingAddressName"));
            actionUtility.sendKeys(address1_BillingAddress,testData.get("billingAddress1"));
            actionUtility.sendKeys(city_BillingAddress,testData.get("billingAddressCity"));
            actionUtility.sendKeys(country_BillingAddress,testData.get("billingAddressCountry"));
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to pay by credit card");
            throw e;
        }
    }

    private void issueDoc(HashMap<String,String> testData){
        actionUtility.sendKeys(callerDetails, testData.get("callerDetails"));
        actionUtility.click(issueDoc);
        actionUtility.hardSleep(2000);
        actionUtility.click(issueDoc);
    }
}
