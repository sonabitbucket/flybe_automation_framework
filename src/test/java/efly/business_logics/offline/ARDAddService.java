package efly.business_logics.offline;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.TestManager;

import java.util.HashMap;

public class ARDAddService {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public ARDAddService() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(xpath = "//select[contains(@id,'servicesAssociations_paxSelect_id_input')]")
    private WebElement paxAssociation;

    @FindBy(xpath = "//span[label[contains(text(),'Frequent Flyer Number')]]/input")
    private WebElement ff_number;

    @FindBy(css = "select[id*='_servicesAssociations_paxSelect_id_input']")
    private WebElement selectPaxAssociation;

    @FindBy(css = "input[id*='seatNumber']")
    private WebElement seatNumber;

    @FindBy(xpath = "//button[span[strong[text()='Book services']]]")
    private WebElement bookServiceButton;

    @FindBy(css = "button[id*='servicesPopupActions_servicesPricingBtn_id']")
    private WebElement priceServiceButton;

    @FindBy(css = "span[class='ConfirmPriceButton'] button")
    private WebElement confirmPriceButton;

    @FindBy(xpath = "//button[contains(@id,'services')][span[strong[text()='Close']]]")
    private WebElement closeAfterAddingService;

    String wheelChair = "input[id*='%s']";

    public void selectService(String service){
        //service - Frequent Flyer Number,FQTV Accrual
        try{
            TestManager.getDriver().findElement(By.xpath("//a[text()='"+service+"']")).click();
            actionUtility.hardSleep(2000);
            reportLogger.log(LogStatus.INFO,"successfully selected service - "+service);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select service - "+service);
            throw e;
        }
    }

    /*private void selectPassengerAssociation(HashMap<String,String> testData){
        actionUtility.dropdownSelect(paxAssociation,);
    }
*/
    public void addSpecialAssistance(String splAssistance){
        try{
            actionUtility.selectOption(TestManager.getDriver().findElement(By.cssSelector(String.format(wheelChair,splAssistance))));
            reportLogger.log(LogStatus.INFO,"successfully selected "+splAssistance);
            addServiceAndClose();
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select "+splAssistance);
        }
    }

    public void addFrequentFlyerNumber(HashMap<String,String> testData,int noOfPassengers){
        try{
            if(testData.get("noOfInfants")!=null){
                noOfPassengers -= Integer.parseInt(testData.get("noOfInfants"));
            }
            for(int i=1;i<=noOfPassengers;i++){
                actionUtility.dropdownSelect(selectPaxAssociation, ActionUtility.SelectionType.SELECTBYVALUE, i+"");
                String frequentFlyerNumber = testData.get("frequentFlyerNumber"+i);
                actionUtility.sendKeys(ff_number, frequentFlyerNumber);
                actionUtility.click(bookServiceButton);
            }
            actionUtility.hardClick(closeAfterAddingService);
            reportLogger.log(LogStatus.INFO,"successfully added frequent flyer number");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to add frequent flyer number");
            throw e;
        }
    }

    private void selectPassengerAssociation_AddFF(HashMap<String,String> testData,String paxType,int pax_number,int noOfPax){
        //selects pax and enters FF for each pax
        for(int i=pax_number;i<=noOfPax;i++) {
            actionUtility.dropdownSelect(selectPaxAssociation, ActionUtility.SelectionType.SELECTBYVALUE, i+"");
            String frequentFlyerNumber = testData.get("frequentFlyerNumber"+paxType+i);
            actionUtility.sendKeys(ff_number, frequentFlyerNumber);
            actionUtility.click(bookServiceButton);
        }
    }

    public void addServiceAndClose(String... service){
        try{

            if(service.length>0 && service[0].equals("Seat")){
                seatNumber.sendKeys(service[1]);
            }
            actionUtility.hardSleep(3000);
            actionUtility.click(bookServiceButton);
            actionUtility.hardSleep(6000);
            if(service.length>0){
                try {
                    actionUtility.click(priceServiceButton);
                    actionUtility.hardSleep(3000);
                    actionUtility.scrollToViewElement(confirmPriceButton);
                    actionUtility.click(confirmPriceButton);
                    actionUtility.hardSleep(3000);
                }catch(StaleElementReferenceException ex){
                    actionUtility.click(priceServiceButton);
                    actionUtility.hardSleep(3000);
                    actionUtility.scrollToViewElement(confirmPriceButton);
                    actionUtility.click(confirmPriceButton);
                    actionUtility.hardSleep(3000);
                }
            }
            actionUtility.hardClick(closeAfterAddingService);
            reportLogger.log(LogStatus.INFO,"successfully added service");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to add service");
            throw e;
        }
    }
}
