package efly.business_logics.offline;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.DataUtility;
import utilities.TestManager;

import java.util.HashMap;
import java.util.List;

public class ARDPassengerAndContactInformation {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public ARDPassengerAndContactInformation() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(linkText = "Add Passenger")
    private WebElement addPassengerLink;

    private String passengerInfo = "tr[id*='paxCheckboxRow_%s'] td";

    private String infantDetail = "//tr[td[span[text()='INF']]]['%d']/td//input";

    @FindBy(css = "span[id*='mainContactTable'] table span[id*='contactPhysicalType']>select")
    private WebElement mainContactType;

    @FindBy(css= "span[id*='mainContactTable'] table td[class='contactDetails'] input")
    private WebElement contactDetails;

    @FindBy(css = "span[id*='paxContactTable'] table span[id*='contactPhysicalType']>select")
    private WebElement paxContactType;

    @FindBy(css = "span[id*='paxContactTable'] table td[class='contactDetails']>span input")
    private List<WebElement> paxContactDetails;

    @FindBy(css = "span[id*='paxContactTable'] span[id*='contactPaxAssocSelect'] span[class='xICNdropdown']")
    private WebElement paxAssocDropDown;

    @FindBy(css = "span[id*='_Items'] span[class='xWidget xICNcheckBoxes']")
    private List<WebElement> paxAssoc;

    @FindBy(css = "button[title='Apply and Close']")
    private WebElement applyAndCloseButton;

    public int addPassengerAndRetrieveNoOfPassengersAdded(HashMap<String,String> testData){
        try{
            int noOfAdults = Integer.parseInt(testData.get("noOfAdults"));
            int noOfTeens = 0;
            int noOfChild = 0;
            int noOfInfants = 0;
            int noOfInfants_detailsEntered = 0;
            for(int i=0;i<noOfAdults;i++){
                List<WebElement> paxInfo = TestManager.getDriver().findElements(By.cssSelector(String.format(passengerInfo,i)+" input"));
                actionUtility.sendKeys(paxInfo.get(0),"ADT");
                String name[] = testData.get("adultName"+(i+1)).split(" ");
                actionUtility.sendKeys(paxInfo.get(1),name[2]);
                paxInfo.get(1).sendKeys(Keys.RETURN);
                actionUtility.sendKeys(paxInfo.get(2),name[1]+" "+name[0]);
                paxInfo.get(2).sendKeys(Keys.RETURN);
                if((testData.get("noOfInfants")!= null && new Integer(testData.get("noOfInfants"))>0)){
                    noOfInfants = Integer.parseInt(testData.get("noOfInfants"));
                    if(noOfInfants_detailsEntered<noOfInfants) {
                        actionUtility.hardClick(paxInfo.get(3));
                        name = testData.get("infantName" + (i + 1)).split(" ");

                        List<WebElement> infantData = TestManager.getDriver().findElements(By.xpath(String.format(infantDetail, noOfInfants_detailsEntered++)));
                        infantData.get(0).clear();
                        actionUtility.sendKeys(infantData.get(0), name[1]);
                        actionUtility.sendKeys(infantData.get(1), name[0]);
                        actionUtility.sendKeys(infantData.get(6), testData.get("infantDOB" + (i + 1)));
                    }
                }
            }
            if(testData.get("noOfTeens")!=null) {
                noOfTeens = Integer.parseInt(testData.get("noOfTeens"));
                for (int k=0,i = noOfAdults; k < noOfTeens ; i++,k++) {
                    List<WebElement> paxInfo = TestManager.getDriver().findElements(By.cssSelector(String.format(passengerInfo, i) + " input"));
                    actionUtility.sendKeys(paxInfo.get(0), "YTH");
                    String name[] = testData.get("teenName" + (k + 1)).split(" ");
                    actionUtility.sendKeys(paxInfo.get(1), name[2]);
                    paxInfo.get(1).sendKeys(Keys.RETURN);
                    actionUtility.sendKeys(paxInfo.get(2), name[1] + " " + name[0]);
                    paxInfo.get(2).sendKeys(Keys.RETURN);

                    //System.out.println(noOfPassengers);
                }
            }
            if(testData.get("noOfChild")!=null) {
                noOfChild = Integer.parseInt(testData.get("noOfChild"));
                for (int i = noOfAdults+noOfTeens,k=0; k < noOfChild ; k++,i++) {
                    List<WebElement> paxInfo = TestManager.getDriver().findElements(By.cssSelector(String.format(passengerInfo, i) + " input"));
                    actionUtility.sendKeys(paxInfo.get(0), "CHD");
                    String name[] = testData.get("childName" + (k + 1)).split(" ");
                    actionUtility.sendKeys(paxInfo.get(1), name[2]);
                    paxInfo.get(1).sendKeys(Keys.RETURN);
                    actionUtility.sendKeys(paxInfo.get(2), name[1] + " " + name[0]);
                    paxInfo.get(2).sendKeys(Keys.RETURN);
                    actionUtility.sendKeys(paxInfo.get(7), testData.get("childDOB"+(k+1)));
                    paxInfo.get(2).sendKeys(Keys.RETURN);

                    //System.out.println(noOfPassengers);
                }
            }

            reportLogger.log(LogStatus.INFO,"successfully added passengers");
            return noOfAdults+noOfChild+noOfTeens+noOfInfants;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to add passengers");
            throw e;
        }
    }

    public void addMainContactInfo(HashMap<String,String> testData){
        try{
            actionUtility.dropdownSelect(mainContactType, ActionUtility.SelectionType.SELECTBYTEXT,testData.get("mainContactType"));
            String mainContactinfo = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).getAsJsonObject("contact").get(testData.get("mainContactType").toLowerCase()).getAsString();
            actionUtility.sendKeys(contactDetails,mainContactinfo);
            contactDetails.sendKeys(Keys.RETURN);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to add main contact information");
            throw e;
        }

    }

    public void addPassengerContactInfoAndAssoc(HashMap<String,String> testData){
        try{
            actionUtility.dropdownSelect(paxContactType, ActionUtility.SelectionType.SELECTBYTEXT,testData.get("paxContactType"));

            if(testData.get("paxContactType").equalsIgnoreCase("email") || testData.get("paxContactType").equalsIgnoreCase("other")){
                actionUtility.sendKeys(paxContactDetails.get(0),testData.get("paxContactDetail"));
                paxContactDetails.get(0).sendKeys(Keys.RETURN);
            }
            else if(testData.get("paxContactType").equalsIgnoreCase("fax") || testData.get("paxContactType").equalsIgnoreCase("mobile") || testData.get("paxContactType").equalsIgnoreCase("phone")){
                actionUtility.sendKeys(paxContactDetails.get(0),testData.get("paxContactDetail").split("-")[0].trim());
                paxContactDetails.get(0).sendKeys(Keys.RETURN);
                actionUtility.sendKeys(paxContactDetails.get(2),testData.get("paxContactDetail").split("-")[1].trim());
                //paxContactDetails.get(0).sendKeys(Keys.RETURN);
            }
            actionUtility.click(paxAssocDropDown);
            actionUtility.click(paxAssoc.get(0));
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to add passenger contact information");
            throw e;
        }

    }

    public HashMap<String,String> retrievePassengerNames(HashMap<String,String> testData){
        HashMap<String,String> passengerNames = new HashMap<String,String>(); //Title FirstName LastName format
        try{
            //List<WebElement> paxInfo = TestManager.getDriver().findElements(By.cssSelector(String.format(passengerInfo,i)+" input"));
            //passengerNames.put("passenger"+i,paxInfo.get(2).getText().split(" ")[1]+" "+paxInfo.get(2).getText().split(" ")[0]+" "+paxInfo.get(1).getText()); //getText() return null
            int noOfAdults = new Integer(testData.get("noOfAdults"));
            for(int i=1;i<=noOfAdults;i++){
                passengerNames.put("adult"+i,testData.get("adultName"+i));
            }
            System.out.println(passengerNames);
            return passengerNames;
        }catch(Exception e) {
            reportLogger.log(LogStatus.INFO, "unable to retrieve passenger names");
            throw e;
        }

    }

    public void applyAndClosePassengersAndContactInfo(){
        try{
            applyAndCloseButton.click();
        }catch(Exception e) {
            reportLogger.log(LogStatus.INFO, "unable to apply and close passenger and contact information");
            throw e;
        }
    }

}
