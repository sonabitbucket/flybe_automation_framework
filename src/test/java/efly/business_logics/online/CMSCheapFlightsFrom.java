package efly.business_logics.online;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.relevantcodes.extentreports.model.Log;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.TestManager;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Sona on 4/19/2019.
 */
public class CMSCheapFlightsFrom {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public CMSCheapFlightsFrom() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(id = "flights-from")
    private WebElement flightFrom;

    @FindBy(xpath = "//div[@class='flight-info']/p[2]")
    private List<WebElement> departure;

    @FindBy(id = "price")
    private List<WebElement> fare;

    @FindBy(css = "div[class='flight-info'] p:nth-child(4)")
    private List<WebElement> destinations;

    public void verifyThatDepartureIsSameAsFlightSelectedInEachTile(String selectedDeparture){
        //verifies that all flights from tile has departure selected on homepage
        try {
            try{
                Assert.assertTrue(flightFrom.getText().toLowerCase().contains(selectedDeparture.toLowerCase()));
            }catch(AssertionError e){
                reportLogger.log(LogStatus.INFO,"'flights from' header does not mention "+selectedDeparture);
                throw e;
            }
            for (WebElement tileDeparture : departure) {
                Assert.assertTrue(tileDeparture.getText().toLowerCase().contains(selectedDeparture.toLowerCase()));
            }
            reportLogger.log(LogStatus.INFO,"departure in tile on 'flights from' matches with departure selected on offers page"+selectedDeparture);
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"departure in tile on 'flights frpm' doesn't match with departure selected on offers page"+selectedDeparture);
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify if departure in each tile in 'flights from' is "+selectedDeparture);
            throw e;
        }
    }

    public void verifyCurrencyInEachTile(String currency,String departure){
        //verifies whether currency in each is based on the departure
        String price = null;
        try {
            for (WebElement eachFare : fare) {
                price = eachFare.getText();
                Assert.assertTrue(price.contains(currency));
            }
            reportLogger.log(LogStatus.INFO,"currency in each tile is "+currency+" for departure "+departure);
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"currency in a tile is "+price.split("")[0]+" and not "+currency+" for departure "+departure);
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify currency in each tile under flights from "+departure);
            throw e;
        }
    }

    public HashMap<String, String> selectRouteAndgetDetails(String departure){
        //selects any random route specified and returns route details
        HashMap<String,String> selectedDetails = new HashMap<>();
        try{
            int index = destinations.size()-1;
            selectedDetails.put("departure",departure);
            selectedDetails.put("destination",destinations.get(index).getText());
            selectedDetails.put("price",fare.get(index).getText().substring(0,fare.get(index).getText().length()-1));
            System.out.println("Selected details "+selectedDetails);
            actionUtility.hardClick(destinations.get(index));
            reportLogger.log(LogStatus.INFO,"successfully selected a route from "+departure);
            return selectedDetails;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select a route under flights from "+departure);
            throw e;
        }
    }




}
