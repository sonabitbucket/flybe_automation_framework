package efly.business_logics.online;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.CommonUtility;
import utilities.TestManager;

import java.util.HashMap;
import java.util.List;

public class EFLYManageBookingTravelExtras {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public EFLYManageBookingTravelExtras(){
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    String extraService = "//div[@class='footer']//button[contains(text(),'%s')]";

    @FindBy(xpath = "//button[span[text()='continue']]")
    private WebElement continueButton;

    //Bag
    String bagButton = "//div[div[div[contains(text(),'%s')]]]//button[span[span[contains(text(),'%s')]]]";

    String bagCost = "//div[div[div[contains(text(),'%s')]]]//button[span[span[contains(text(),'%s')]]]//span[@class='price-value']";

    @FindBy(xpath = "//button[span[text()='Add to booking']]")
    private WebElement addbagButton;

    //seat

    @FindBy(className = "seatNumber")
    private List<WebElement> seatNo;

    @FindBy(id= "seatmap")
    private WebElement seatMap;

    @FindBy(css = "div[class='seatMapfooter'] button")
    private WebElement continueWithSelectedSeat;

    @FindBy(xpath = "//li[a[span[@class='tab-header']]]")
    private List<WebElement> flightTabHeader;

    @FindBy(xpath = "//button[contains(text(),'NEXT FLIGHT')]")
    private WebElement nextFlight;

    @FindBy(xpath = "//button[contains(text(),'PREVIOUS FLIGHT')]")
    private WebElement previousFlight;

    @FindBy(xpath = "//td[section[@class='extra-leg']]/div[not(contains(@class,'unavailable'))][not(@data-seat-selected='true')]")
    private List<WebElement> extraLegSeat;

    @FindBy(xpath = "//td[not(section)]/div[contains(@class,'icon-seat')][not(contains(@class,'unavailable'))][not(@data-seat-selected='true')]")
    private List<WebElement> standardSeat;

    @FindBy(xpath = "//div[@class='seatMapfooter']//span[@class='plnext-widget-btn-text one-icon']")
    private WebElement continueWithSeats;

    String paxButton = "//div[div[div[div[label[div[strong[text()='%s']]]]]]]";

    String seatPrice = "span[class='primaryCurrency'] div";

    String seatNumber = "//span[@class='seatNumber']";

    //basket
    String serviceCost = "//div[h3[contains(text(),'%s')]]//span[@class='tripsummary-price-amount-text']";

    @FindBy(xpath = "//div[span[contains(text(),'Total new services')]]//span[@class='tripsummary-price-amount-text']")
    private WebElement totalNewServices;

    //golf/ski
    String addGolf = "//div[div[div[text()='%s']]]//button";

    String addSki = "//div[div[div[text()='%s']]]//button[span[span[text()='%s']]]";

    String servicePrice = "//span[@class='total-price-container']/span[@class='price'][contains(text(),'%s')]";

    @FindBy(xpath = "//button/span[text()='Add to booking']")
    private WebElement addService;

    String openService = "//div[contains(@class,'dialog-show')]";

    private Double selectSkiSnowForEachPax(String paxName,String skiSnow){
        if(skiSnow.equalsIgnoreCase("ski")) {
            actionUtility.click(TestManager.getDriver().findElement(By.xpath(String.format(addSki, paxName.split(" ")[1] + " " + paxName.split(" ")[2], "Skis"))));
        }else{
            actionUtility.click(TestManager.getDriver().findElement(By.xpath(String.format(addSki, paxName.split(" ")[1] + " " + paxName.split(" ")[2], "Snowboard"))));
        }
        actionUtility.hardSleep(5000);
        return 30.00;
    }

    public HashMap<String, Double> addSkisSnowboard(HashMap<String,String> testData,HashMap<String,Double>... basket){
        //adds golf to each pax
        //calculates cost for ski
        Double amount = 0.00;
        Double eachCost = 0.00;
        HashMap<String,Double> cost = new HashMap<>();
        try{
            int noOfAdult = Integer.parseInt(testData.get("noOfAdult"));
            int noOfTeen = 0;
            int noOfChild = 0;
            for(int i=1;i<=noOfAdult;i++){
                if(testData.get("mb_Adult"+i+"SkiSnow")!=null) {
                    eachCost = selectSkiSnowForEachPax(testData.get("adult"+i+"Name"),testData.get("mb_Adult"+i+"SkiSnow"));
                    if(testData.get("returnDateOffset")!=null){
                        eachCost *= 2;
                    }
                    System.out.println("added "+testData.get("mb_Adult"+i+"SkiSnow")+" for passenger - "+testData.get("adult"+i+"Name")+" for a cost of "+eachCost);
                    reportLogger.log(LogStatus.INFO,"added "+testData.get("mb_Adult"+i+"SkiSnow")+" for passenger - "+testData.get("adult"+i+"Name")+" for a cost of "+eachCost);
                    amount += eachCost;
//                    actionUtility.waitForElementVisible(TestManager.getDriver().findElement(By.xpath(String.format(servicePrice,amount.toString()))),30);
                }
            }
            if(testData.get("noOfTeen")!=null){
                noOfTeen = new Integer(testData.get("noOfTeen"));
                for(int i=1;i<=noOfTeen;i++){
                    if(testData.get("mb_Teen"+i+"SkiSnow")!=null) {
                        eachCost = selectSkiSnowForEachPax(testData.get("teen"+i+"Name"),testData.get("mb_Teen"+i+"SkiSnow"));
                        if(testData.get("returnDateOffset")!=null){
                            eachCost *= 2;
                        }
                        System.out.println("added "+testData.get("mb_Teen"+i+"SkiSnow")+" for passenger - "+testData.get("teen"+i+"Name")+" for a cost of "+eachCost);
                        reportLogger.log(LogStatus.INFO,"added "+testData.get("mb_Teen"+i+"SkiSnow")+" for passenger - "+testData.get("teen"+i+"Name")+" for a cost of "+eachCost);
                        amount += eachCost;
                        actionUtility.waitForElementVisible(TestManager.getDriver().findElement(By.xpath(String.format(servicePrice,amount.toString()))),30);
                    }
                }
            }
            if(testData.get("noOfChild")!=null){
                noOfChild = new Integer(testData.get("noOfChild"));
                for(int i=1;i<=noOfChild;i++){
                    if(testData.get("mb_Child"+i+"SkiSnow")!=null) {
                        eachCost = selectSkiSnowForEachPax(testData.get("child"+i+"Name"),testData.get("mb_Child"+i+"SkiSnow"));
                        if(testData.get("returnDateOffset")!=null){
                            eachCost *= 2;
                        }
                        System.out.println("added "+testData.get("mb_Child"+i+"SkiSnow")+" for passenger - "+testData.get("child"+i+"Name")+"for a cost of "+eachCost);
                        reportLogger.log(LogStatus.INFO,"added "+testData.get("mb_Child"+i+"SkiSnow")+" for passenger - "+testData.get("child"+i+"Name")+" for a cost of "+eachCost);
                        amount += eachCost;
                        actionUtility.waitForElementVisible(TestManager.getDriver().findElement(By.xpath(String.format(servicePrice,amount.toString()))),30);
                    }
                }
            }
            actionUtility.hardClick(addService);
            if(basket.length>0){
                if(basket[0].get("total new services")!=null){
                    basket[0].put("total new services",basket[0].get("total new services")+amount);
                }
                else{
                    basket[0].put("total new services",amount);
                }
                if(basket[0].get("skis")!=null){
                    amount += basket[0].get("skis");
                }
                basket[0].put("skis",amount);
                cost = basket[0];
                System.out.println("SKI BASKET "+basket[0]);
            }
            actionUtility.waitForElementNotPresent(openService,30);
            return cost;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to add ski/snowboard for passengers");
            throw e;
        }
    }


    private Double selectGolfForEachPax(String paxName){
        actionUtility.click(TestManager.getDriver().findElement(By.xpath(String.format(addGolf,paxName.split(" ")[1]+" "+paxName.split(" ")[2]))));
        actionUtility.hardSleep(5000);
        return 30.00;
    }

    public HashMap<String,Double> addGolf(HashMap<String,String> testData,HashMap<String,Double>... basket){
        //adds golf to each pax
        //calculates cost for golf
        Double amount = 0.00;
        Double eachCost = 0.00;
        HashMap<String,Double> cost = new HashMap<>();
        try{
            int noOfAdult = Integer.parseInt(testData.get("noOfAdult"));
            int noOfTeen = 0;
            int noOfChild = 0;
            for(int i=1;i<=noOfAdult;i++){
                if(testData.get("mb_Adult"+i+"Golf")!=null && testData.get("mb_Adult"+i+"Golf").equalsIgnoreCase("yes")) {
                    eachCost = selectGolfForEachPax(testData.get("adult"+i+"Name"));
                    if(testData.get("returnDateOffset")!=null){
                        eachCost *= 2;
                    }
                    System.out.println("successfully added golf for passenger - "+testData.get("adult"+i+"Name")+" for a cost of "+eachCost);
                    reportLogger.log(LogStatus.INFO,"successfully added golf for passenger - "+testData.get("adult"+i+"Name")+" for a cost of "+eachCost);
                    amount += eachCost;
//                    actionUtility.waitForElementVisible(TestManager.getDriver().findElement(By.xpath(String.format(servicePrice,amount.toString()))),30);
                }

            }
            if(testData.get("noOfTeen")!=null){
                noOfTeen = new Integer(testData.get("noOfTeen"));
                for(int i=1;i<=noOfTeen;i++){
                    if(testData.get("mb_Teen"+i+"Golf")!=null && testData.get("mb_Teen"+i+"Golf").equalsIgnoreCase("yes")) {
                        eachCost = selectGolfForEachPax(testData.get("teen"+i+"Name"));
                        if(testData.get("returnDateOffset")!=null){
                            eachCost *= 2;
                        }
                        System.out.println("successfully added golf for passenger - "+testData.get("teen"+i+"Name")+" for a cost of "+eachCost);
                        reportLogger.log(LogStatus.INFO,"successfully added golf for passenger - "+testData.get("teen"+i+"Name")+" for a cost of "+eachCost);
                        amount += eachCost;
                        //actionUtility.waitForElementVisible(TestManager.getDriver().findElement(By.xpath(String.format(servicePrice,amount.toString()))),30);
                    }
                }
            }
            if(testData.get("noOfChild")!=null){
                noOfChild = new Integer(testData.get("noOfChild"));
                for(int i=1;i<=noOfChild;i++){
                    if(testData.get("mb_Child"+i+"Golf")!=null && testData.get("mb_Child"+i+"Golf").equalsIgnoreCase("yes")) {
                        eachCost = selectGolfForEachPax(testData.get("child" + i + "Name"));
                        if (testData.get("returnDateOffset") != null) {
                            eachCost *= 2;
                        }
                        System.out.println("successfully added golf for passenger - " + testData.get("child" + i + "Name") + " for a cost of " + eachCost);
                        reportLogger.log(LogStatus.INFO, "successfully added golf for passenger - " + testData.get("child" + i + "Name") + " for a cost of " + eachCost);
                        amount += eachCost;
                        //actionUtility.waitForElementVisible(TestManager.getDriver().findElement(By.xpath(String.format(servicePrice, amount.toString()))), 30);
                    }
                }
            }
            actionUtility.hardClick(addService);
            if(basket.length>0){
                if(basket[0].get("total new services")!=null){
                    basket[0].put("total new services",basket[0].get("total new services")+amount);
                }
                else{
                    basket[0].put("total new services",amount);
                }
                if(basket[0].get("golf clubs")!=null){
                    amount += basket[0].get("golf clubs");
                }
                basket[0].put("golf clubs",amount);
                cost = basket[0];
                System.out.println("GOLF BASKET  "+basket[0]);
            }
            actionUtility.waitForElementNotPresent(openService,30);
            return cost;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to add golf for passengers");
            throw e;
        }
    }

    private Double calculateBagCost(String paxName,String bag,int counter,Boolean returnJourney,Boolean returnDifferentTicketType) {
        Double cost = 0.00;
        cost = CommonUtility.convertStringToDouble(TestManager.getDriver().findElements(By.xpath(String.format(bagCost,paxName,bag))).get(counter).getText());

        return cost;
    }

    public void selectService(String service){
        //selects Reserve seats,Book baggage,Add golf clubs,Add skis,Edit seat,Edit baggage, Edit golf clubs, Edit skis
        try{
            actionUtility.waitForElementVisible(continueButton,120);
            actionUtility.waitForElementNotPresent(openService,30);
            actionUtility.hardClick(TestManager.getDriver().findElement(By.xpath(String.format(extraService,service))));
            reportLogger.log(LogStatus.INFO,"selected service - "+service);
            System.out.println(service.toUpperCase());
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select service - "+service);
            throw e;
        }
    }

    public void continueToPayment(){
        //navigates to payment page
        try{
            actionUtility.scrollToViewElement(continueButton);
            try {
                actionUtility.hardClick(continueButton);
            }catch(NoSuchElementException e){
                System.out.println("HERE I AM ");
                actionUtility.scrollToViewElement(continueButton);
                actionUtility.hardClick(continueButton);
            }
            reportLogger.log(LogStatus.INFO,"successfully navigated to payment page");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to payment page");
            throw e;
        }
    }

    public HashMap<String,Double> addBag(HashMap<String,String> testData,HashMap<String,Double>... basket){
        //adds bag
        try {
            Double totalBagAmount = 0.00;
            Boolean returnJourney = false;
            Boolean returnDifferentTickeType = true;
            if(testData.get("returnDateOffset")!=null)
            {
                returnJourney = true;
                if(testData.get("mb_DepartTicketType")!=null && testData.get("mb_ReturnTicketType")!=null) {
                    if (testData.get("mb_DepartTicketType").equalsIgnoreCase(testData.get("mb_ReturnTicketType"))) {
                        returnDifferentTickeType = false;
                    }
                }
                else if(testData.get("mb_DepartTicketType")!=null && testData.get("mb_ReturnTicketType")==null){
                    if (testData.get("mb_DepartTicketType").equalsIgnoreCase(testData.get("returnTicketType"))) {
                        returnDifferentTickeType = false;
                    }
                }
                else if(testData.get("mb_DepartTicketType")==null && testData.get("mb_ReturnTicketType")!=null){
                    if (testData.get("departTicketType").equalsIgnoreCase(testData.get("mb_ReturnTicketType"))) {
                        returnDifferentTickeType = false;
                    }
                }
                else if(testData.get("departTicketType").equalsIgnoreCase(testData.get("returnTicketType"))){
                    returnDifferentTickeType = false;
                }
            }
            int noOfAdults = Integer.valueOf(testData.get("noOfAdult"));
            int bagCountDepart = 0;
            int bagCountReturn = 1;
            Double cost = 0.00;
            WebElement bag = null;
            for(int i=0;i<noOfAdults;i++){
                if(testData.get("mb_Adult"+(i+1)+"BagDepart") != null) {
                    bag = TestManager.getDriver().findElements(By.xpath(String.format(bagButton, testData.get("adult" + (i + 1) + "Name").split(" ")[1]+" "+testData.get("adult" + (i + 1) + "Name").split(" ")[2],testData.get("mb_Adult"+(i+1)+"BagDepart")))).get(bagCountDepart);
                    actionUtility.hardClick(bag);
                    actionUtility.hardSleep(2000);
                    cost = calculateBagCost(testData.get("adult" + (i + 1) + "Name").split(" ")[1]+" "+testData.get("adult" + (i + 1) + "Name").split(" ")[2],testData.get("mb_Adult" + (i + 1) + "BagDepart"),bagCountDepart,returnJourney,returnDifferentTickeType);

                    if(basket.length>0){
                        basket[0].put("mb_Adult"+(i+1)+"DepartBagCost",cost);
                    }
                    if(returnJourney && !returnDifferentTickeType){
                        cost *= 2;
                    }
                    totalBagAmount += cost;
                    reportLogger.log(LogStatus.INFO,"selected bag for departure "+testData.get("mb_Adult" + (i + 1) + "BagDepart")+" for "+testData.get("adult" + (i + 1)+"Name"));
                    System.out.println("selected adult bag for departure "+testData.get("mb_Adult" + (i + 1) + "BagDepart")+"for "+testData.get("adult" + (i + 1)+"Name"));
                    System.out.println("total bagCost "+totalBagAmount+" individual cost "+cost);
                }
                if(returnJourney && returnDifferentTickeType){
                    bagCountDepart += 2;
                }
                else{
                    bagCountDepart += 1;
                }
                if(returnJourney && returnDifferentTickeType){
                    if(testData.get("mb_Adult"+(i+1)+"BagReturn")!=null) {
                        bag = TestManager.getDriver().findElements(By.xpath(String.format(bagButton,testData.get("adult" + (i + 1) + "Name"), testData.get("mb_Adult" + (i + 1) + "BagReturn")))).get(bagCountReturn);
                        actionUtility.hardClick(bag);
                        actionUtility.hardSleep(2000);
                        cost = calculateBagCost(testData.get("adult"+(i+1)+"Name").split(" ")[1]+" "+testData.get("adult" + (i + 1) + "Name").split(" ")[2],testData.get("adult" + (i + 1) + "BagReturn"),bagCountReturn, returnJourney, returnDifferentTickeType);
                        if(basket.length>0){
                            basket[0].put("mb_Adult"+(i+1)+"ReturnBagCost",cost);
                        }
                        if(returnJourney && !returnDifferentTickeType){
                            cost *= 2;
                        }
                        totalBagAmount += cost;
                        reportLogger.log(LogStatus.INFO,"selected bag for return "+testData.get("mb_Adult" + (i + 1) + "BagReturn")+" for "+testData.get("adult" + (i + 1)+"Name"));
                        System.out.println("selected adult bag for return "+testData.get("mb_Adult" + (i + 1) + "BagReturn")+"for "+testData.get("adult" + (i + 1)+"Name"));
                        System.out.println("total bagCost "+totalBagAmount+" individual cost"+cost);
                    }
                    bagCountReturn += 2;
                }
            }
            if(testData.get("noOfTeen")!=null) {
                int noOfTeens = Integer.valueOf(testData.get("noOfTeen"));
                bagCountDepart = 0;
                bagCountReturn = 1;
                for (int i = 0; i < noOfTeens; i++) {
                    if (testData.get("mb_Teen" + (i + 1) + "BagDepart") != null) {
                        bag = TestManager.getDriver().findElements(By.xpath(String.format(bagButton, testData.get("teen" + (i + 1)+"Name").split(" ")[1]+" "+testData.get("teen" + (i + 1) + "Name").split(" ")[2],testData.get("mb_Teen" + (i + 1) + "BagDepart")))).get(bagCountDepart);
                        actionUtility.hardClick(bag);
                        actionUtility.hardSleep(2000);
                        cost = calculateBagCost(testData.get("teen" + (i + 1)+"Name").split(" ")[1]+" "+testData.get("adult" + (i + 1) + "Name").split(" ")[2],testData.get("mb_Teen" + (i + 1) + "BagDepart"),bagCountDepart,returnJourney,returnDifferentTickeType);
                        if(basket.length>0){
                            basket[0].put("mb_Teen"+(i+1)+"DepartBagCost",cost);
                        }
                        if(returnJourney && !returnDifferentTickeType){
                            cost *= 2;
                        }
                        totalBagAmount += cost;
                        reportLogger.log(LogStatus.INFO,"selected bag for departure "+testData.get("mb_Teen" + (i + 1) + "BagDepart")+" for "+testData.get("teen" + (i + 1)+"Name"));
                        System.out.println("selected teen bag for departure " + testData.get("mb_Teen" + (i + 1) + "BagDepart")+testData.get("teen" + (i + 1)+"Name"));
                        System.out.println("bagCost "+totalBagAmount);
                    }
                    if(returnJourney && returnDifferentTickeType){
                        bagCountDepart += 2;
                    }
                    else{
                        bagCountDepart += 1;
                    }
                    if (returnJourney && returnDifferentTickeType ) {
                        if(testData.get("mb_Teen" + (i + 1) + "BagReturn") != null) {
                            bag = TestManager.getDriver().findElements(By.xpath(String.format(bagButton , testData.get("teen" + (i + 1)+"Name").split(" ")[1]+" "+testData.get("teen" + (i + 1) + "Name").split(" ")[2],testData.get("mb_Teen" + (i + 1) + "BagReturn")))).get(bagCountReturn);
                            actionUtility.hardClick(bag);
                            actionUtility.hardSleep(2000);
                            cost = calculateBagCost(testData.get("teen" + (i + 1)+"Name").split(" ")[1]+" "+testData.get("teen" + (i + 1) + "Name").split(" ")[2],testData.get("mb_Teen" + (i + 1) + "BagReturn"),bagCountReturn, returnJourney, returnDifferentTickeType);

                            if(basket.length>0){
                                basket[0].put("mb_Teen"+(i+1)+"ReturnBagCost",cost);
                            }
                            if(returnJourney && !returnDifferentTickeType){
                                cost *= 2;
                            }
                            totalBagAmount += cost;
                            reportLogger.log(LogStatus.INFO,"selected bag for return "+testData.get("mb_Teen" + (i + 1) + "BagReturn")+"for "+testData.get("teen" + (i + 1)+"Name"));
                            System.out.println("selected teen bag for return " + testData.get("mb_Teen" + (i + 1) + "BagReturn")+"for "+testData.get("teen" + (i + 1)+"Name"));
                            System.out.println("bagCost "+totalBagAmount);
                        }
                        bagCountReturn += 2;
                    }
                }

            }
            if(testData.get("noOfChild")!=null) {
                int noOfChild = Integer.valueOf(testData.get("noOfChild"));
                bagCountDepart = 0;
                bagCountReturn = 1;
                for (int i = 0; i < noOfChild; i++) {
                    if (testData.get("mb_Child" +(i+1)+ "BagDepart") != null) {
                        bag = TestManager.getDriver().findElements(By.xpath(String.format(bagButton,testData.get("child" + (i + 1)+"Name").split(" ")[1]+" "+testData.get("child" + (i + 1) + "Name").split(" ")[2], testData.get("mb_Child" + (i + 1) + "BagDepart")))).get(bagCountDepart);
                        actionUtility.hardClick(bag);
                        actionUtility.hardSleep(2000);
                        cost = calculateBagCost(testData.get("child" + (i + 1)+"Name").split(" ")[1]+" "+testData.get("child" + (i + 1) + "Name").split(" ")[2],testData.get("mb_Child" + (i + 1) + "BagDepart"),bagCountDepart,returnJourney,returnDifferentTickeType);
                        if(basket.length>0){
                            basket[0].put("mb_Child"+(i+1)+"DepartBagCost",cost);
                        }
                        if(returnJourney && !returnDifferentTickeType){
                            cost *= 2;
                        }
                        totalBagAmount += cost;
                        reportLogger.log(LogStatus.INFO,"selected bag for departure "+testData.get("mb_Child" + (i + 1) + "BagDepart")+" for "+testData.get("child" + (i + 1)+"Name"));
                        System.out.println("selected child bag for departure " + testData.get("mb_Child" + (i + 1) + "BagDepart")+"for "+testData.get("child" + (i + 1)+"Name"));
                        System.out.println("bagCost "+totalBagAmount);
                    }
                    if(returnJourney && returnDifferentTickeType){
                        bagCountDepart += 2;
                    }
                    else{
                        bagCountDepart += 1;
                    }
                    if (returnJourney && returnDifferentTickeType) {
                        if(testData.get("mb_Child" + (i + 1) + "BagReturn") != null) {
                            bag = TestManager.getDriver().findElements(By.xpath(String.format(bagButton, testData.get("child" + (i + 1)+"Name"),testData.get("mb_Child" + (i + 1) + "BagReturn")))).get(bagCountReturn);
                            actionUtility.hardClick(bag);
                            actionUtility.hardSleep(2000);
                            cost = calculateBagCost(testData.get("child" + (i + 1)+"Name").split(" ")[1]+" "+testData.get("child" + (i + 1) + "Name").split(" ")[2],testData.get("mb_Child" + (i + 1) + "BagReturn"),bagCountReturn, returnJourney, returnDifferentTickeType);

                            if(basket.length>0){
                                basket[0].put("mb_Child"+(i+1)+"ReturnBagCost",cost);
                            }
                            if(returnJourney && !returnDifferentTickeType){
                                cost *= 2;
                            }
                            totalBagAmount += cost;
                            reportLogger.log(LogStatus.INFO,"selected bag for departure "+testData.get("mb_Child" + (i + 1) + "BagReturn")+" for "+testData.get("child" + (i + 1)+"Name"));
                            System.out.println("selected child bag for return " + testData.get("mb_Child" + (i + 1) + "BagReturn")+"for "+testData.get("child" + (i + 1)+"Name"));
                            System.out.println("bagCost "+totalBagAmount);
                        }
                        bagCountReturn += 2;
                    }
                    actionUtility.hardSleep(3000);
                }
            }
            reportLogger.log(LogStatus.INFO,"successfully added bags for passenger for a total cost of - "+totalBagAmount);
            actionUtility.click(addbagButton);
            if (basket.length > 0) {
                if(basket[0].get("total new services")!=null){
                    basket[0].put("total new services",basket[0].get("total new services")+totalBagAmount);
                }
                else{
                    basket[0].put("total new services",totalBagAmount);
                }
                if(basket[0].get("hold baggage")!=null){
                    totalBagAmount += basket[0].get("hold baggage");
                }
                basket[0].put("hold baggage",totalBagAmount);
                System.out.println("BAG BASKET  "+basket[0]);
                return basket[0];
            }
            return null;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to add bag in manage booking - travel extras");
            throw e;
        }
    }

    public HashMap<String,Double> verifyThatSeatsAreNotCharged(HashMap<String,Double> basket,HashMap<String,String> testData){
        //verifies that seat is not charged when added after modify flight
        String service = null;
        Double actualPrice = 1.00;
        Double expected = 0.00;
        try{
            service = "your seat";
            actualPrice = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(serviceCost,"Your seat"))).getText());
            Assert.assertEquals(actualPrice,expected);
            basket.put("your seat",actualPrice);

            service = "total new services";
            actualPrice = CommonUtility.convertStringToDouble(totalNewServices.getText());
            Assert.assertEquals(actualPrice,expected);
            basket.put("total new services",actualPrice);

            putSeatPriceForEachPaxInBasket(basket,testData);

            return basket;
        }catch(AssertionError e){
            System.out.println(service+" == "+expected+" -- "+actualPrice);
            reportLogger.log(LogStatus.INFO,"seats are charged when added after modify flight");
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify if seats are charged when added after modify flight");
            throw e;
        }
    }

    private HashMap<String,Double> putSeatPriceForEachPaxInBasket(HashMap<String,Double> basket,HashMap<String,String> testData){

        return basket;
    }

    public void verifyBasket(HashMap<String,Double> basket){
        String service = null;
        Double expectedPrice = 0.00;
        Double actualPrice = 0.00;
        boolean expectedTotalServiceCharges = false;
        try{
            if(basket.get("your seat")!=null){
                service = "seat";
                expectedPrice = basket.get("your seat");
                actualPrice = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(serviceCost,"Your seat"))).getText());
                Assert.assertEquals(actualPrice,expectedPrice);
                expectedTotalServiceCharges = true;
                reportLogger.log(LogStatus.INFO,"successfully verified your seat cost in basket - "+actualPrice);
            }
            if(basket.get("hold baggage")!=null){
                service = "baggage";
                expectedPrice = basket.get("hold baggage");
                actualPrice = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(serviceCost,"Hold baggage"))).getText());
                Assert.assertEquals(actualPrice,expectedPrice);
                expectedTotalServiceCharges = true;
                reportLogger.log(LogStatus.INFO,"successfully verified hold baggage cost in basket - "+actualPrice);
            }
            if(basket.get("golf clubs")!=null){
                service = "golf";
                expectedPrice = basket.get("golf clubs");
                actualPrice = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(serviceCost,"Golf clubs"))).getText());
                Assert.assertEquals(actualPrice,expectedPrice);
                expectedTotalServiceCharges = true;
                reportLogger.log(LogStatus.INFO,"successfully verified golf clubs cost in basket - "+actualPrice);
            }
            if(basket.get("skis")!=null){
                service = "ski/snowboard";
                expectedPrice = basket.get("skis");
                actualPrice = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(serviceCost,"Skis"))).getText());
                Assert.assertEquals(actualPrice,expectedPrice);
                expectedTotalServiceCharges = true;
                reportLogger.log(LogStatus.INFO,"successfully verified skis cost in basket - "+actualPrice);
            }
            if(expectedTotalServiceCharges) {
                service = "total new services";
                expectedPrice = basket.get("total new services");
                actualPrice = CommonUtility.convertStringToDouble(totalNewServices.getText());
                Assert.assertEquals(actualPrice,expectedPrice);
                reportLogger.log(LogStatus.INFO,"successfully verified services cost in basket - "+actualPrice);
            }
        }catch(AssertionError ae){
            System.out.println(service+" >> "+expectedPrice+" ---------- "+actualPrice);
            reportLogger.log(LogStatus.INFO,service+" charges expected - "+expectedPrice+" does not match with the displayed charges - "+actualPrice+" in the basket");
            throw ae;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify total service charges in the basket");
            throw e;
        }
    }

    private void viewFirstSectorSeatMap(){
        if(actionUtility.verifyIfElementIsDisplayed(previousFlight))
            do{
                actionUtility.hardClick(previousFlight);
            }while(actionUtility.verfiyIfAnElementISEnabled(previousFlight));
    }

    private HashMap<String,Double> selectSeatPerSectorPerPax(HashMap<String,String> testData,int noOfPax,String paxType,int sectorNo,int noOfDepartureFlights,int noOfReturnFlights,HashMap<String,Double>... basket){
        String selectedSeat = null;
        Double totalPrice = 0.00;
        Double eachPrice = 0.00;
        Double priceToBeSubtractedFromYourSeat = 0.00;
        Double priceToBeSubtractedFromTotalNewServices = 0.00;
        WebElement pax = null;
        for(int i=1;i<=noOfPax;i++){
            if(testData.get("mb_"+paxType+i+"SeatSector"+sectorNo)!=null) {
                pax = TestManager.getDriver().findElement(By.xpath(String.format(paxButton,testData.get(paxType.toLowerCase()+i+"Name"))));
                actionUtility.click(pax);
                actionUtility.hardSleep(2000);
                if (testData.get("mb_"+paxType + i + "SeatSector"+sectorNo).equalsIgnoreCase("XL")) {
                    selectedSeat = extraLegSeat.get(0).getAttribute("id").split("seat")[1];
                    actionUtility.click(extraLegSeat.get(0));
                    actionUtility.hardSleep(2000);
                }
                else
                {
                    selectedSeat = standardSeat.get(1).getAttribute("id").split("seat")[1];
                    actionUtility.click(standardSeat.get(1));
                    actionUtility.hardSleep(2000);
                }
                reportLogger.log(LogStatus.INFO,"successfully selected seat - "+selectedSeat+" for passenger "+paxType+i+" - "+testData.get(paxType.toLowerCase()+i+"Name")+" for sector "+sectorNo);
                eachPrice = calculateSeatCost(testData,testData.get("mb_"+paxType+i+"SeatSector"+sectorNo),sectorNo,noOfDepartureFlights,noOfReturnFlights,pax,testData.get(paxType.toLowerCase()+i+"Name"),testData.get(paxType.toLowerCase()+i+"SpecialAssistance"));
                if(basket.length>0){
                    basket[0].put("mb_"+paxType+i+"SeatSector"+sectorNo+"Price",eachPrice);
                    if(basket[0].get(paxType.toLowerCase()+i+"SeatSector"+sectorNo+"Price")!=null){
                        priceToBeSubtractedFromYourSeat += basket[0].get(paxType.toLowerCase()+i+"SeatSector"+sectorNo+"Price");
                    }
                }
                totalPrice += eachPrice;

                System.out.println("total seat price for  - "+paxType+" is "+totalPrice);
            }
            if(basket[0].get(paxType.toLowerCase()+i+"SeatSector"+sectorNo+"Price")!=null){
                priceToBeSubtractedFromTotalNewServices += basket[0].get(paxType.toLowerCase()+i+"SeatSector"+sectorNo+"Price");
            }
        }
        if(basket.length>0){
            if(basket[0].get("your seat")!=null) {
                basket[0].put("your seat", totalPrice+(basket[0].get("your seat")-priceToBeSubtractedFromYourSeat));
            }
            else {
                basket[0].put("your seat", totalPrice);
            }
            if(basket[0].get("total new services")!=null){
                totalPrice += basket[0].get("total new services");
                basket[0].put("total new services",totalPrice);
            }
            else{
                basket[0].put("total new services",totalPrice);
            }
            return  basket[0];
        }
        return null;
    }

    private Double calculateSeatCost(HashMap<String,String> testData,String seatType,int sectorNo,int noOfDepartureFlights,int noOfReturnFlights,WebElement pax,String paxname,String splAssistance){
        //verifies seat price for  pax according to ticket type
        // returns price of seat selected for pax
        String departureTicketType = testData.get("departTicketType");
        if(testData.get("mb_DepartTicketType")!=null){
            departureTicketType = testData.get("mb_DepartTicketType");
        }

        Double price = 0.00;
        if(seatType.equalsIgnoreCase("XL")) {
            if (sectorNo <= noOfDepartureFlights) {
                if (departureTicketType.equalsIgnoreCase("just fly")) {
                    price = CommonUtility.convertStringToDouble(pax.findElement(By.cssSelector(seatPrice)).getText());
                    try{
                        Assert.assertTrue(price>0);
                    }catch (AssertionError ae) {
                        reportLogger.log(LogStatus.INFO, "displayed XL seat price for ticket type - just fly for sector - " + sectorNo+" is "+price);
                        throw ae;
                    }
                    if(testData.get("sector"+sectorNo+"XLSeatPrice")!=null){
                        try{
                            Assert.assertEquals(price,Double.parseDouble(testData.get("sector"+sectorNo+"XLSeatPrice")));
                            reportLogger.log(LogStatus.INFO,"XL seat price is as expected - "+price+" for sector "+sectorNo);
                        }catch (AssertionError ae) {
                            reportLogger.log(LogStatus.INFO, "displayed XL seat price for ticket type - just fly for sector - " + sectorNo+" is "+price+" and not "+testData.get("sector"+sectorNo+"XLSeatPrice")+" as expected");
                            throw ae;
                        }
                    }
                } else if (departureTicketType.equalsIgnoreCase("get more")) {
                    price = CommonUtility.convertStringToDouble(pax.findElement(By.cssSelector(seatPrice)).getText());
                    try{
                        Assert.assertTrue(price>0);
                    }catch (AssertionError ae) {
                        reportLogger.log(LogStatus.INFO, "displayed XL seat price for ticket type - get more for sector - " + sectorNo+" is "+price);
                        throw ae;
                    }
                    if(testData.get("sector"+sectorNo+"XLSeatPrice")!=null){
                        try{
                            Assert.assertEquals(price,Double.parseDouble(testData.get("sector"+sectorNo+"XLSeatPrice")));
                            reportLogger.log(LogStatus.INFO,"XL seat price is as expected - "+price+" for sector "+sectorNo);
                        }catch (AssertionError ae) {
                            reportLogger.log(LogStatus.INFO, "displayed XL seat price for ticket type - get more for sector - " + sectorNo+" is "+price+" and not "+testData.get("sector"+sectorNo+"XLSeatPrice")+" as expected");
                            throw ae;
                        }
                    }
                } else if (departureTicketType.equalsIgnoreCase("all in")) {
                    try {
                        Assert.assertTrue(pax.findElement(By.cssSelector(seatPrice)).getText().contains("Free of charge"));
                    } catch (AssertionError ae) {
                        reportLogger.log(LogStatus.INFO, "XL seat is not free of charge for ticket type - all in for sector - " + sectorNo);
                        throw ae;
                    }
                }
            }
            else if(noOfReturnFlights > 0 && sectorNo > noOfDepartureFlights){
                String returnTicketType = testData.get("returnTicketType");
                if(testData.get("mb_ReturnTicketType")!=null){
                    returnTicketType = testData.get("mb_ReturnTicketType");
                }
                if (returnTicketType.equalsIgnoreCase("just fly")) {
                    price = CommonUtility.convertStringToDouble(pax.findElement(By.cssSelector(seatPrice)).getText());
                    try{
                        Assert.assertTrue(price>0);
                    }catch (AssertionError ae) {
                        reportLogger.log(LogStatus.INFO, "displayed XL seat price for ticket type - just fly for sector - " + sectorNo+" is "+price);
                        throw ae;
                    }
                    if(testData.get("sector"+sectorNo+"XLSeatPrice")!=null){
                        try{
                            Assert.assertEquals(price,Double.parseDouble(testData.get("sector"+sectorNo+"XLSeatPrice")));
                            reportLogger.log(LogStatus.INFO,"XL seat price is as expected - "+price+" for sector "+sectorNo);
                        }catch (AssertionError ae) {
                            reportLogger.log(LogStatus.INFO, "displayed XL seat price for ticket type - just fly for sector - " + sectorNo+" is "+price+" and not "+testData.get("sector"+sectorNo+"XLSeatPrice")+" as expected");
                            throw ae;
                        }
                    }
                } else if (returnTicketType.equalsIgnoreCase("get more")) {
                    price = CommonUtility.convertStringToDouble(pax.findElement(By.cssSelector(seatPrice)).getText());
                    try{
                        Assert.assertTrue(price>0);
                    }catch (AssertionError ae) {
                        reportLogger.log(LogStatus.INFO, "displayed XL seat price for ticket type - get more for sector - " + sectorNo+" is "+price);
                        throw ae;
                    }
                    if(testData.get("sector"+sectorNo+"XLSeatPrice")!=null){
                        try{
                            Assert.assertEquals(price,Double.parseDouble(testData.get("sector"+sectorNo+"XLSeatPrice")));
                            reportLogger.log(LogStatus.INFO,"XL seat price is as expected - "+price+" for sector "+sectorNo);
                        }catch (AssertionError ae) {
                            reportLogger.log(LogStatus.INFO, "displayed XL seat price for ticket type - get more for sector - " + sectorNo+" is "+price+" and not "+testData.get("sector"+sectorNo+"XLSeatPrice")+" as expected");
                            throw ae;
                        }
                    }
                } else if (returnTicketType.equalsIgnoreCase("all in")) {
                    try {
                        Assert.assertTrue(pax.findElement(By.cssSelector(seatPrice)).getText().contains("Free of charge"));
                    } catch (AssertionError ae) {
                        reportLogger.log(LogStatus.INFO, "XL seat is not free of charge for ticket type - all in for sector - " + sectorNo);
                        throw ae;
                    }
                }
            }
        }
        else if(seatType.equalsIgnoreCase("standard")){
            if (sectorNo <= noOfDepartureFlights) {
                if (departureTicketType.equalsIgnoreCase("just fly")) {
                    price = CommonUtility.convertStringToDouble(pax.findElement(By.cssSelector(seatPrice)).getText());
                    if(splAssistance==null) {
                        try{
                        Assert.assertTrue(price>0);
                        }catch (AssertionError ae) {
                            reportLogger.log(LogStatus.INFO, "displayed standard seat price for ticket type - just fly for sector - " + sectorNo+" is "+price);
                            throw ae;
                        }
                    }
                    else{
                        Assert.assertTrue(pax.findElement(By.cssSelector(seatPrice)).getText().contains("Free of charge"));
                    }

                    if(testData.get("sector"+sectorNo+"StandardSeatPrice")!=null){
                        try{
                            Assert.assertEquals(price,Double.parseDouble(testData.get("sector"+sectorNo+"StandardSeatPrice")));
                            reportLogger.log(LogStatus.INFO,"standard seat price is as expected - "+price+" for sector "+sectorNo);
                        }catch (AssertionError ae) {
                            reportLogger.log(LogStatus.INFO, "displayed standard seat price for ticket type - just fly for sector - " + sectorNo+" is "+price+" and not "+testData.get("sector"+sectorNo+"XLSeatPrice")+" as expected");
                            throw ae;
                        }
                    }
                } else if (departureTicketType.equalsIgnoreCase("get more")) {
                    try {
                        String seat_number = pax.findElement(By.xpath(seatNumber)).getText();
                        System.out.println("seat selected - "+seat_number+" row no = "+seat_number.substring(0,seat_number.length()-1));
                        int rowNo = Integer.parseInt(seat_number.substring(0,seat_number.length()-1));
                        if(rowNo>8) {
                            Assert.assertTrue(pax.findElement(By.cssSelector(seatPrice)).getText().contains("Free of charge"));
                        }
                        else{
                            price = CommonUtility.convertStringToDouble(pax.findElement(By.cssSelector(seatPrice)).getText());
                            try{
                                Assert.assertTrue(price>0);
                            }catch (AssertionError ae) {
                                reportLogger.log(LogStatus.INFO, "displayed FOC seat price for ticket type - get more for sector - " + sectorNo+" is "+price);
                                throw ae;
                            }
                            if(testData.get("sector"+sectorNo+"FOCSeatPrice")!=null){
                                try{
                                    Assert.assertEquals(price,Double.parseDouble(testData.get("sector"+sectorNo+"StandardSeatPrice")));
                                    reportLogger.log(LogStatus.INFO,"standard seat price is as expected - "+price+" for sector "+sectorNo);
                                }catch (AssertionError ae) {
                                    reportLogger.log(LogStatus.INFO, "displayed FOC seat price for ticket type - get more for sector - " + sectorNo+" is "+price+" and not "+testData.get("sector"+sectorNo+"XLSeatPrice")+" as expected");
                                    throw ae;
                                }
                            }
                        }
                    } catch (AssertionError ae) {
                        reportLogger.log(LogStatus.INFO, "standard seat is not free of charge for ticket type - get more for sector - " + sectorNo);
                        throw ae;
                    }
                } else if (departureTicketType.equalsIgnoreCase("all in")) {
                    try {
                        Assert.assertTrue(pax.findElement(By.cssSelector(seatPrice)).getText().contains("Free of charge"));
                    } catch (AssertionError ae) {
                        reportLogger.log(LogStatus.INFO, "standard seat is not free of charge for ticket type - all in for sector - " + sectorNo);
                        throw ae;
                    }
                }
            }
            else if(noOfReturnFlights > 0 && sectorNo > noOfDepartureFlights){
                String returnTicketType = testData.get("returnTicketType");
                if(testData.get("mb_ReturnTicketType")!=null){
                    returnTicketType = testData.get("mb_ReturnTicketType");
                }
                if (returnTicketType.equalsIgnoreCase("just fly")) {
                    price = CommonUtility.convertStringToDouble(pax.findElement(By.cssSelector(seatPrice)).getText());
                    if(splAssistance==null) {
                        try{
                            Assert.assertTrue(price>0);
                        }catch (AssertionError ae) {
                            reportLogger.log(LogStatus.INFO, "displayed standard seat price for ticket type - just fly for sector - " + sectorNo+" is "+price);
                            throw ae;
                        }
                    }
                    else{
                        Assert.assertTrue(pax.findElement(By.cssSelector(seatPrice)).getText().contains("Free of charge"));
                    }
                    if(testData.get("sector"+sectorNo+"StandardSeatPrice")!=null){
                        try{
                            Assert.assertEquals(price,Double.parseDouble(testData.get("sector"+sectorNo+"StandardSeatPrice")));
                            reportLogger.log(LogStatus.INFO,"standard seat price is as expected - "+price+" for sector "+sectorNo);
                        }catch (AssertionError ae) {
                            reportLogger.log(LogStatus.INFO, "displayed standard seat price for ticket type - just fly for sector - " + sectorNo+" is "+price+" and not "+testData.get("sector"+sectorNo+"XLSeatPrice")+" as expected");
                            throw ae;
                        }
                    }
                } else if (returnTicketType.equalsIgnoreCase("get more")) {
                    try {
                        String seat_number = pax.findElement(By.xpath(seatNumber)).getText();
                        int rowNo = Integer.parseInt(seat_number.substring(0,seat_number.length()-1));
                        if(rowNo>8) {
                            Assert.assertTrue(pax.findElement(By.cssSelector(seatPrice)).getText().contains("Free of charge"));
                        }
                        else{
                            price = CommonUtility.convertStringToDouble(pax.findElement(By.cssSelector(seatPrice)).getText());
                            try{
                                Assert.assertTrue(price>0);
                            }catch (AssertionError ae) {
                                reportLogger.log(LogStatus.INFO, "displayed FOC seat price for ticket type - get more for sector - " + sectorNo+" is "+price);
                                throw ae;
                            }
                            if(testData.get("sector"+sectorNo+"FOCSeatPrice")!=null){
                                try{
                                    Assert.assertEquals(price,Double.parseDouble(testData.get("sector"+sectorNo+"StandardSeatPrice")));
                                    reportLogger.log(LogStatus.INFO,"standard seat price is as expected - "+price+" for sector "+sectorNo);
                                }catch (AssertionError ae) {
                                    reportLogger.log(LogStatus.INFO, "displayed FOC seat price for ticket type - get more for sector - " + sectorNo+" is "+price+" and not "+testData.get("sector"+sectorNo+"XLSeatPrice")+" as expected");
                                    throw ae;
                                }
                            }
                        }
                    } catch (AssertionError ae) {
                        reportLogger.log(LogStatus.INFO, "standard seat is not free of charge for ticket type - get more for sector - " + sectorNo);
                        throw ae;
                    }
                } else if (returnTicketType.equalsIgnoreCase("all in")) {
                    try {
                        Assert.assertTrue(pax.findElement(By.cssSelector(seatPrice)).getText().contains("Free of charge"));
                    } catch (AssertionError ae) {
                        reportLogger.log(LogStatus.INFO, "standard seat is not free of charge for ticket type - all in for sector - " + sectorNo);
                        throw ae;
                    }
                }
            }
        }
        reportLogger.log(LogStatus.INFO,"seat price for passenger - "+paxname+" is "+price+" for sector "+sectorNo);
        return price;
    }

    public HashMap<String, Double> addSeat(HashMap<String,String> testData,HashMap<String,Double>... basket){
        //selects seat for each pax
        //verifies seat charges for just fly,get more, all in - chargeable or free of charge
        //returns basket with seat cost for each pax, total seat price.
        try{
            actionUtility.waitForElementVisible(seatMap,120);
            actionUtility.hardSleep(5000);
//            actionUtility.click(flightTabHeader.get(0));
            viewFirstSectorSeatMap();
            actionUtility.hardSleep(4000);
            int totalSectors = new Integer(testData.get("departNoOfStops"))+1;
            if(testData.get("mb_DepartNoOfStops")!=null) {
                totalSectors = new Integer(testData.get("mb_DepartNoOfStops")) + 1;
            }
            int noOfDepartureFlights = totalSectors;
            int noOfReturnFlights = 0;
            if(testData.get("mb_ReturnDateOffset")!=null){
                totalSectors += new Integer(testData.get("mb_ReturnNoOfStops"))+1;
                noOfReturnFlights = totalSectors - noOfDepartureFlights;
            }
            else if(testData.get("returnDateOffset")!=null){
                totalSectors += new Integer(testData.get("returnNoOfStops"))+1;
                noOfReturnFlights = totalSectors - noOfDepartureFlights;
            }
            int noOfAdult =0,noOfTeen = 0,noOfChild = 0;
            for (int i = 1; i <= totalSectors; i++) {
                if(testData.get("noOfAdult")!=null && Integer.parseInt(testData.get("noOfAdult"))>0) {
                    noOfAdult = Integer.parseInt(testData.get("noOfAdult"));
                    if(basket.length>0) {
                        basket[0] = selectSeatPerSectorPerPax(testData, noOfAdult, "Adult", i, noOfDepartureFlights, noOfReturnFlights,basket[0]);
                    }
                    else{
                        selectSeatPerSectorPerPax(testData, noOfAdult, "Adult", i, noOfDepartureFlights, noOfReturnFlights);
                    }
                }
                if(testData.get("noOfTeen")!=null) {
                    noOfTeen = Integer.parseInt(testData.get("noOfTeen"));
                    if(basket.length>0) {
                        basket[0] = selectSeatPerSectorPerPax(testData, noOfTeen, "Teen", i, noOfDepartureFlights, noOfReturnFlights, basket[0]);
                    }
                    else{
                        selectSeatPerSectorPerPax(testData, noOfTeen, "Teen", i, noOfDepartureFlights, noOfReturnFlights);
                    }
                }
                if(testData.get("noOfChild")!=null) {
                    noOfChild = Integer.parseInt(testData.get("noOfChild"));
                    if(basket.length>0){
                        basket[0] = selectSeatPerSectorPerPax(testData, noOfTeen, "Child", i, noOfDepartureFlights, noOfReturnFlights, basket[0]);
                    }
                    else{
                        selectSeatPerSectorPerPax(testData, noOfChild, "Child", i, noOfDepartureFlights, noOfReturnFlights);
                    }
                }
                if (totalSectors > 1 && i != totalSectors) {
                    actionUtility.hardClick(nextFlight);
                    actionUtility.waitForElementVisible(seatMap, 30);
                }
            }
            if(basket.length>0){
                System.out.println("SEAT BASKET in MB -  "+basket[0]);
                return basket[0];
            }
            return null;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select seat");
            throw e;
        }
    }

    public HashMap<String, String> retrieveSeatSelectedForEachPax(HashMap<String,String> testData,HashMap<String,String>... previouslyAdded){
        HashMap<String,String> seatDetails = new HashMap<>();
        try {
            viewFirstSectorSeatMap();
            if(previouslyAdded.length>0) {
                seatDetails = previouslyAdded[0];
            }
            int totalSectors = new Integer(testData.get("departNoOfStops"))+1;
            if(testData.get("returnDateOffset")!=null){
                totalSectors += new Integer(testData.get("returnNoOfStops"))+1;
            }
            int noOfPax;
            for(int i=1;i<=totalSectors;i++) {
                seatDetails.put("counter","0");
                if(testData.get("noOfAdult")!=null){
                    noOfPax = Integer.parseInt(testData.get("noOfAdult"));
                    seatDetails = captureSeatNumber(testData, "Adult", noOfPax, i, seatDetails);
                }
                if(testData.get("noOfChild")!=null){
                    noOfPax = Integer.parseInt(testData.get("noOfChild"));
                    seatDetails = captureSeatNumber(testData, "Child", noOfPax, i, seatDetails);
                }
                if(testData.get("noOfTeen")!=null){
                    noOfPax = Integer.parseInt(testData.get("noOfTeen"));
                    seatDetails = captureSeatNumber(testData, "Teen", noOfPax, i, seatDetails);
                }
                if(totalSectors>1 && i!=totalSectors) {
                    actionUtility.hardClick(nextFlight);
                    actionUtility.waitForElementVisible(seatMap,30);
                }
            }
            reportLogger.log(LogStatus.INFO,"successfully retrieved seat selected for passengers - "+seatDetails);
            System.out.println(seatDetails);
            return seatDetails;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to retrieve seat selected for passengers");
            throw e;
        }
    }

    private HashMap<String,String> captureSeatNumber(HashMap<String,String> testData,String pax,int noOfPax,int sector,HashMap<String,String> seatDetails){
        String seatSelected = null;
        int counter = Integer.parseInt(seatDetails.get("counter"));
        int noOfPaxTypeWithSeat = 0;
        System.out.println("counter "+counter);
        System.out.println("sector "+sector);
        System.out.println("pax "+pax);

        for(int j=1;j<=noOfPax;j++) {
            if(testData.get("mb_"+pax+j+"SeatSector"+sector)!=null){
                seatSelected = seatNo.get(counter++).getText();
                seatDetails.put("counter",counter+"");
            }
            if(seatSelected!=null){
                noOfPaxTypeWithSeat++;
                seatDetails.put("mb_"+pax+j+"SeatNoForSector"+sector,seatSelected);
                System.out.println("MB SEAT CAPTURED -- "+seatDetails);
            }
            seatSelected = null;
        }
        //int totalNoOfPaxTypeWithSeat = Integer.valueOf(seatDetails.get("noOf"+pax.toLowerCase()+"withSeatForSector"+sector))+noOfPaxTypeWithSeat;
        seatDetails.put("noOf"+pax.toLowerCase()+"withSeatInMBForSector"+sector,noOfPaxTypeWithSeat+"");
        return seatDetails;
    }

    public void continueAfterSeatSelection(){
        try{
            actionUtility.click(continueWithSelectedSeat);
            actionUtility.waitForElementNotPresent(openService,90);
            reportLogger.log(LogStatus.INFO,"successfully added or edited seat on extras");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to continue with selected seat");
            throw e;
        }
    }
}
