package efly.business_logics.online;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.TestManager;

/**
 * Created by Indhu on 6/17/2019.
 */
public class EFLYRetrieval {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public EFLYRetrieval(){
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy (xpath = "//span[contains(text(),'Ticket Information')]")
    private WebElement ticketInformation;

    @FindBy (css = "li[class='reservation-name'] span:nth-child(2)")
    private WebElement reservationName;

    @FindBy(css = "span[class='number']")
    private WebElement reservationNo;

    //modify
    @FindBy(xpath = "//span[contains(text(),'Manage booking')]")
    private WebElement manageBookingButton;

    @FindBy(css = "li[class='modify-passengers-button']")
    private WebElement modifyTravellerButton;

    @FindBy(css = "li[class='modify-boundsATC-button']")
    private WebElement modifyFlightButton;

    @FindBy(css = "li[class='modify-services-button']")
    private WebElement modifyTravelExtrasButton;

    //checkin

    @FindBy(xpath = "//a[contains(text(),'now open ')]")
    private WebElement checkInLink;

    public void navigateToModifyTravellerDetails(){
        try{
            actionUtility.hardClick(manageBookingButton);
            actionUtility.waitForElementVisible(modifyTravellerButton,20);
            actionUtility.click(modifyTravellerButton);
            reportLogger.log(LogStatus.INFO,"successfully navigated to modify traveler section from manage booking page");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to modify traveler section from manage booking page");
            throw e;
        }
    }

    public void navigateToModifyFlights(){
        try{
            actionUtility.hardClick(manageBookingButton);
            actionUtility.waitForElementVisible(modifyFlightButton,20);
            actionUtility.click(modifyFlightButton);
            reportLogger.log(LogStatus.INFO,"successfully navigated to modify flights page from manage booking page");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to modify flights page from manage booking page");
            throw e;
        }
    }

    public void navigateToTravelExtras(){
        try{
            actionUtility.hardClick(manageBookingButton);
            actionUtility.waitForElementVisible(modifyTravelExtrasButton,20);
            actionUtility.click(modifyTravelExtrasButton);
            reportLogger.log(LogStatus.INFO,"successfully navigated to modify travel Extras from manage booking page");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to modify travel Extras from manage booking page");
            throw e;
        }
    }

    public void navigateToCheckInFlow(){
        try{
            actionUtility.waitForElementVisible(checkInLink,40);
            actionUtility.hardClick(checkInLink);
            actionUtility.waitForPageLoad(90);
            reportLogger.log(LogStatus.INFO,"successfully navigated to check-in page from retrieval page");
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to check-in page from retrieval page");
            throw e;
        }
    }


    public void verifyPNRAndReservationName(String pnr, String reservationName){
        String actual=null,expected = null,item=null;
        try{
            item = "PNR";
            actual= retrievePNR();
            expected = pnr;
            Assert.assertTrue(actual.equals(expected));

            item = "reservation name";
            expected = reservationName;
            actual = getReservationName();
            Assert.assertTrue(actual.equals(expected));
            reportLogger.log(LogStatus.INFO,"PNR displayed- "+actual+" is matching with the PNR generated in booking flow");
            reportLogger.log(LogStatus.INFO,"reservation name displayed is matching with the expected-" +reservationName);
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,item+" displayed - "+actual+" doesn't match with "+item+" expected - "+expected);
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify the PNR and reservation name in retrieval page");
            throw e;

        }
    }

    public String retrievePNR(){
        //retrieves PNR
        String pnr = null;
        try{
            actionUtility.waitForElementVisible(reservationNo,90);
            pnr = reservationNo.getText();
            System.out.println("PNR - "+pnr);
            reportLogger.log(LogStatus.INFO,"Reservation Number is "+pnr);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to retrieve PNR");
            throw e;
        }
        return pnr;
    }

    private String getReservationName(){
        //gets the passenger name from MB conf page
        String passengerName= null;
        actionUtility.waitForElementVisible(ticketInformation,90);
        return reservationName.getText();
    }

}
