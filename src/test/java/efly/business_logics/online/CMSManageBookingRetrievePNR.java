package efly.business_logics.online;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.TestManager;

public class CMSManageBookingRetrievePNR {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public CMSManageBookingRetrievePNR(){
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(id = "booking-reference")
    private WebElement bookingReference;

    @FindBy(id = "surname")
    private WebElement lastName;

    @FindBy(id = "flybe_manage-booking_btn")
    private WebElement viewBookingButton;

    @FindBy(xpath = "//span[contains(text(),'Manage booking')]")
    private WebElement manageBookingButton;

    @FindBy (xpath = "//span[contains(text(),'Ticket Information')]")
    private WebElement ticketInformation;

    public void retrievePNR(String PNR, String reservationName){
        try {
            bookingReference.sendKeys(PNR);
            lastName.sendKeys(reservationName);
            actionUtility.hardClick(viewBookingButton);
            actionUtility.waitForPageLoad(50);
            actionUtility.waitForElementVisible(manageBookingButton, 30);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to retrieve PNR for manage booking");
            throw e;
        }
    }


}
