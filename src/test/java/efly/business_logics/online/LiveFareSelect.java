package efly.business_logics.online;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.CommonUtility;
import utilities.TestManager;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Sona on 4/30/2019.
 */
public class LiveFareSelect {
    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public LiveFareSelect() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(xpath = "//span[text()='continue']")
    private WebElement continueButton;

    @FindBy(css = "div[class='header-location header-locationfrom availability-header-locationfrom']")
    private List<WebElement> route;

    //price on selected date
    @FindBy(xpath="//a[span[text()='Selected departure date']]//abbr")
    private WebElement dateSliderCurrencyOnSelectedDate;

    @FindBy(xpath = "//a[span[text()='Selected departure date']]//span[@class='price']")
    private WebElement dateSliderPriceonSelectedDate;

    @FindBy(xpath = "//div[span[text()='JUST FLY']]/div")
    private List<WebElement> justFlyPriceOnSelectedDate;

    //basket
    @FindBy(css = "span[class='trip-summary-total-cash-amount']")
    private WebElement totalAmount;

    //Flex popup
    @FindBy(css = "button[class='btn dialog-upgrade-btn flybe-btn']")
    private WebElement addFlex;

    @FindBy(css = "button[class='btn btn-link cancel-button']")
    private WebElement doNotAddFlex;


    public void verifyOfferSelectedAndFareSelectAndSelectFlight(HashMap<String,String> detailsToBeCompared){
        //verifies that price and departure displayed on offers page is same as that on selected date on date slider
        //verifies basket amount
        actionUtility.waitForElementVisible(continueButton,90);
        int i;
        try{
            verifyRoute(detailsToBeCompared);
            verifyDateSliderPrice(detailsToBeCompared);
            verifyJustFlyPriceAndSelectflight(detailsToBeCompared);
            verifyBasket(CommonUtility.convertStringToDouble(detailsToBeCompared.get("price")));
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify the price on selected date on date slider with the price on offers page");
            throw e;
        }
    }

    private void verifyRoute(HashMap<String,String> details){
        try{
            Assert.assertTrue(route.get(0).getText().toLowerCase().contains(details.get("departure").toLowerCase()));
            Assert.assertTrue(route.get(1).getText().toLowerCase().contains(details.get("destination").toLowerCase()));
            reportLogger.log(LogStatus.INFO,"source and destination["+route.get(0).getText()+" and "+route.get(1).getText()+"] on fare select matches with offer selected["+details.get("departure").toUpperCase()+" and "+details.get("destination").toUpperCase()+"]");
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"source and destination["+route.get(0).getText()+" and "+route.get(1).getText()+"] on fare select do not match with offer selected["+details.get("departure").toUpperCase()+" and "+details.get("destination").toUpperCase()+"]");
            throw e;
        }
    }

    private void verifyDateSliderPrice(HashMap<String,String> detailsToBeCompared){
        //verifies if date selected on date slider has same price as that of Offer
        try{
            System.out.println("**"+dateSliderCurrencyOnSelectedDate.getText()+dateSliderPriceonSelectedDate.getText());
            Assert.assertTrue(detailsToBeCompared.get("price").equals(dateSliderCurrencyOnSelectedDate.getText()+dateSliderPriceonSelectedDate.getText()));
            reportLogger.log(LogStatus.INFO,"price displayed on date slider for selected date matches with the offer price");
        }catch(AssertionError e) {
            reportLogger.log(LogStatus.INFO, "mismatch in price on offers page(" +detailsToBeCompared.get("price")+ ") and fare select page date slider(" + (dateSliderCurrencyOnSelectedDate.getText() + dateSliderPriceonSelectedDate.getText()) + ")");
            throw e;
        }
    }

    private void verifyJustFlyPriceAndSelectflight(HashMap<String,String> detailsToBeCompared){
        //verifies that just fly price is same as that on selected date on date slider
        //selects just fly flight with fare same as offer price
        int i;
        try{
            for(i = 0;i<justFlyPriceOnSelectedDate.size();i++){
                System.out.println("***"+justFlyPriceOnSelectedDate.get(i).getText());
                String[] actualFare = justFlyPriceOnSelectedDate.get(i).getText().split("\n");
                if(actualFare.length>1){
                    if((actualFare[0]+actualFare[1]).equals(detailsToBeCompared.get("price"))){
                        break;
                    }
                }
                if(actualFare[0].equals(detailsToBeCompared.get("price"))){
                    break;
                }
            }
            Assert.assertTrue(i<justFlyPriceOnSelectedDate.size());
            actionUtility.hardClick(justFlyPriceOnSelectedDate.get(i));
            reportLogger.log(LogStatus.INFO,"just fly price matches with offer price and selected the flight");
        }catch(AssertionError e) {
            reportLogger.log(LogStatus.INFO, "mismatch in price on offers page(" +detailsToBeCompared.get("price")+ ") and fare select page just fly price("+justFlyPriceOnSelectedDate.get(0).getText()+")");
            throw e;
        }
    }

    public void verifyBasket(Double fare){
        //verifies if fare selected is equal to total fare displayed in the basket
        System.out.println(fare+" fare passed");
        Double basketAmount = new Double(CommonUtility.convertStringToDouble(totalAmount.getText()));
        System.out.println("Basket amount "+basketAmount);
        try {
            Assert.assertEquals(fare,basketAmount);
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"fare selected - "+fare+" doesnt match with total amount displayed in basket "+basketAmount);
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to compare total amount in basket with the fare selected");
            throw e;
        }
    }

    public void navigateToTravellerDetailsAfterHandlingFlexPopup(HashMap<String,String> testData){
        //navigates to traveller details page and also select Just Fly Flex if specified in testdata
        try{
            //actionUtility.hardSleep(2000);
            actionUtility.hardClick(continueButton);
            actionUtility.hardSleep(2000);
            try {
                if(testData.get("departTicketType")!=null && testData.get("departTicketType").equalsIgnoreCase("just fly")) {
                    if (testData.get("returnTicketType") != null) {
                        if(testData.get("returnTicketType").equalsIgnoreCase("just fly")) {
                            if (testData.get("flex") != null && testData.get("flex").equalsIgnoreCase("yes")) {
                                actionUtility.click(addFlex);
                                reportLogger.log(LogStatus.INFO, "successfully selected just fly flex");
                            } else {
                                actionUtility.click(doNotAddFlex);
                                reportLogger.log(LogStatus.INFO, "successfully continued without flex");
                            }
                        }
                    }
                    else{
                        if (testData.get("flex") != null && testData.get("flex").equalsIgnoreCase("yes")) {
                            actionUtility.click(addFlex);
                            reportLogger.log(LogStatus.INFO, "successfully selected just fly flex");
                        } else {
                            actionUtility.click(doNotAddFlex);
                            reportLogger.log(LogStatus.INFO, "successfully continued without flex");
                        }
                    }
                }
            }catch(Exception e){
                reportLogger.log(LogStatus.INFO,"unable to select options on flex popup and proceed");
                throw e;
            }
            reportLogger.log(LogStatus.INFO,"successfully navigated to traveller details page");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to proceed to traveller details page");
            throw e;
        }
    }


}
