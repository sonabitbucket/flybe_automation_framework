package efly.business_logics.online;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.CommonUtility;
import utilities.TestManager;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Indhu on 6/20/2019.
 */
public class EFLYManageBookingFlightDetails {
    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public EFLYManageBookingFlightDetails() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(xpath = "//button[span[text()='continue']]")
    private WebElement continueButton;

    //price section
    String itemsOnLHS = "//div[contains(@class,'price-left')]//div[span[contains(text(),'%s')]]//span[contains(@class,'price-value')]";

    String itemsOnRHS = "//div[contains(@class,'price-right')]//div[span[contains(text(),'%s')]]//span[contains(@class,'price-value')]";

    //itinerary
    @FindBy(css = "div[id='fare-review-bounds'] div[class='segment-details-departure '] div div:nth-child(2)")
    List<WebElement> journeySource;

    @FindBy(css = "div[id='fare-review-bounds'] div[class='segment-details-arrival '] div div:nth-child(2)")
    List<WebElement> journeyDestination;

    @FindBy(css = "div[id='fare-review-bounds'] time[class='date']")
    List<WebElement> journeyDates;

    @FindBy(className = "total-duration-value")
    List<WebElement> journeyDuration;

    @FindBy(xpath = "//div[@id='fare-review-bounds']//div[span[@class='flight-info-airline-name']]")
    List<WebElement> flightNos;

    @FindBy(css = "div[id='fare-review-bounds'] div[class*='flight-info-aircraft'] div:nth-child(2)")
    List<WebElement> aircraft;

    @FindBy(css = "div[id='fare-review-bounds'] time[class*='segment-details-time']")
    List<WebElement> journeyTimings;

    @FindBy(xpath = "//div[span[@class='segment-details-stop-location-name']]")
    List<WebElement> stop;

    //traveller details

    String traveller_name = "div[class*='%s'] div[class*='traveller-name']";

    @FindBy(className = "traveller-contact-email")
    private WebElement emailId;

    @FindBy(className = "phone-number")
    private WebElement phoneNo;

    //basket
    String basketItems = "//div[contains(@class,'tripsummary-price')]//div[span[contains(text(),'%s')]]//span[contains(@class,'price-value')]";

    public void navigateToPaymentPage(){
        try{
            actionUtility.waitForPageLoad(20);
            actionUtility.waitForElementVisible(continueButton, 30);
            actionUtility.click(continueButton);
            reportLogger.log(LogStatus.INFO,"successfully navigated to payment page");
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to payment page");
            throw e;
        }
    }

    public void verifyPriceSection(HashMap<String,Double> basket){
        String item = null;
        Double expected = 0.0,actual = 0.0;
        try{
            verify("TO BE PAID in price section",basket.get("to be paid"),CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(itemsOnLHS,"to be paid"))).getText()));
            verify("Price difference on LHS in price section",basket.get("price difference"),CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(itemsOnLHS,"Price difference"))).getText()));
            verify("Price difference on RHS in price section",basket.get("price difference"),CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(itemsOnRHS,"Price difference"))).getText()));
            verify("Previous total already paid in price section",basket.get("total"),CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(itemsOnRHS,"Previous total already paid"))).getText()));

            if(basket.get("change fee")>0){
                verify("Change fee on LHS in price section",basket.get("change fee"),CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(itemsOnLHS,"Change fee"))).getText()));
                verify("Price difference on RHS in price section",basket.get("change fee"),CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(itemsOnRHS,"Change fee"))).getText()));
            }
            else if(basket.get("change fee")==0.0){
                try {
                    Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(String.format(itemsOnLHS, "Change fee")));
                    Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(String.format(itemsOnRHS, "Change fee")));
                    reportLogger.log(LogStatus.INFO,"change fee is not displayed as its not applicable");
                }catch(AssertionError ae){
                    reportLogger.log(LogStatus.INFO,"change fee is displayed in price section even if change fee is not applicable");
                    throw ae;
                }
            }

            //todo pending tax per pax
            basket.put("new flight",CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(itemsOnRHS,"New flight"))).getText()));
            reportLogger.log(LogStatus.INFO,"successfully verified the price section on manage booking details page");
        }catch(Exception ae){
            reportLogger.log(LogStatus.INFO,"unable to verify price section on flight details page");
            throw ae;
        }

    }

    private void verify(String item,Double expected,Double actual){
        try{
            Assert.assertEquals(actual,expected);
        }catch(AssertionError ae){
            System.out.println(item+" = "+expected+" --- "+actual);
            reportLogger.log(LogStatus.INFO,item+" expected is "+expected+" but displayed is "+actual);
            throw ae;
        }
    }

    public  HashMap<String,String> verifyAndRetrieveItineraryDetails(HashMap<String,String> testData,HashMap<String,String> flightDetails){
        String expected = null,actual = null,item=null;
        boolean changeInOutbound = false, changeInInbound = false;
        int stopCounter = 0,flightCounter = 0,timeCounter = 2;;
        try{
            item = "outbound date";
            if(testData.get("mb_DepartDateOffset")!=null) {
                String[] departureDate = actionUtility.getDate(Integer.parseInt(testData.get("mb_DepartDateOffset")));
                expected = departureDate[0]+" "+actionUtility.getMonth(Integer.parseInt(testData.get("mb_DepartDateOffset")))+" "+departureDate[2];
                changeInOutbound = true;
            }
            else{
                expected = flightDetails.get("outboundDate");
            }
            actual = journeyDates.get(0).getText().trim();
            Assert.assertTrue(actual.contains(expected));
            flightDetails.put("outboundDate",actual);

            if(testData.get("returnDateOffset")!=null){
                item = "inbound date";
                if(testData.get("mb_ReturnDateOffset")!=null) {
                    String[] returnDate = actionUtility.getDate(Integer.parseInt(testData.get("mb_ReturnDateOffset")));
                    expected = returnDate[0]+" "+actionUtility.getMonth(Integer.parseInt(testData.get("mb_ReturnDateOffset")))+" "+returnDate[2];
                    changeInInbound = true;
                }
                else{
                    expected = flightDetails.get("inboundDate");
                }
                actual = journeyDates.get(1).getText().trim();
                Assert.assertTrue(actual.contains(expected));
                flightDetails.put("inboundDate",actual);

                if(testData.get("changeReturnFlightNumber")!=null || testData.get("mb_ReturnTicketType")!=null || (testData.get("returnDateOffset")!=null && (testData.get("mb_FlightFrom")!=null || testData.get("mb_FlightTo")!=null))){
                    changeInInbound = true;
                }
            }

            if(testData.get("changeDepartFlightNumber")!=null || testData.get("mb_DepartTicketType")!=null || testData.get("mb_FlightFrom")!=null || testData.get("mb_FlightTo")!=null){
                changeInOutbound = true;
            }
            if(changeInOutbound){
                int outboundNoOfStops = Integer.parseInt(testData.get("departNoOfStops"));
                if(testData.get("mb_DepartTicketType")!=null){
                    flightDetails.put("outboundTicketType",testData.get("mb_DepartTicketType"));
                }
                if(testData.get("mb_DepartNoOfStops")!=null) {
                    outboundNoOfStops = Integer.parseInt(testData.get("mb_DepartNoOfStops"));
                    flightDetails.put("outboundStops",outboundNoOfStops+"");
                }
                flightDetails.put("outboundDuration",journeyDuration.get(0).getText());
                if(testData.get("changeDepartFlight")!=null){
                    try {
                        item = "outbound flight-1 number";
                        Assert.assertTrue(flightDetails.get("outboundFlightNo1") != flightNos.get(flightCounter).getText());
                        reportLogger.log(LogStatus.INFO,"flight number in booking flow - "+flightDetails.get("outboundFlightNo1")+" modified successfully in MB flow to "+flightNos.get(flightCounter).getText());
                    }catch (AssertionError ae){
                        System.out.println(item+" = "+expected+" -- "+actual);
                        reportLogger.log(LogStatus.INFO,"modification in flight number - "+flightDetails.get("outboundFlightNo1")+" is not displayed correctly in itinerary section on flight details page");
                        throw ae;
                    }
                }
                flightDetails.put("outboundFlightNo1",flightNos.get(flightCounter).getText());
                flightDetails.put("outboundAircraft1",aircraft.get(flightCounter++).getText());
                flightDetails.put("outboundSector1TakeOffTime",journeyTimings.get(0).getText());
                flightDetails.put("outboundSector1LandingTime",journeyTimings.get(1).getText());
                if(outboundNoOfStops>0){
                    flightDetails.put("outboundNoOfFlights",(outboundNoOfStops+1)+"");
                    for(int i=1;i<=outboundNoOfStops;i++) {
                        flightDetails.put("outboundStop"+i,stop.get(stopCounter++).getText());
                        if(testData.get("changeDepartFlightNumber")!=null){
                            try {
                                item = "outbound flight-"+i+" number";
                                Assert.assertTrue(flightDetails.get("outboundFlightNo"+(i+1)) != flightNos.get(flightCounter).getText());
                                reportLogger.log(LogStatus.INFO,"flight number in booking flow - "+flightDetails.get("outboundFlightNo"+(i+1))+" modified successfully in MB flow to "+flightNos.get(flightCounter).getText());
                            }catch (AssertionError ae){
                                reportLogger.log(LogStatus.INFO,"modification in flight number - "+flightDetails.get("outboundFlightNo"+(i+1))+" is not displayed correctly in itinerary section on flight details page");
                                throw ae;
                            }
                        }
                        flightDetails.put("outboundFlightNo"+(i+1),flightNos.get(flightCounter).getText());
                        flightDetails.put("outboundAircraft"+(i+1),aircraft.get(flightCounter).getText());
                        flightDetails.put("outboundSector"+(i+1)+"TakeOffTime",journeyTimings.get(timeCounter++).getText());
                        flightDetails.put("outboundSector"+(i+1)+"LandingTime",journeyTimings.get(flightCounter++).getText());
                    }
                }
                if(testData.get("mb_FlightFrom")!=null){
                    item = "outbound source";
                    actual = journeySource.get(0).getText();
                    expected = testData.get("mb_FlightFrom");
                    Assert.assertTrue(actual.contains(expected));
                    flightDetails.put("outboundSource",journeySource.get(0).getText());
                }
                if(testData.get("mb_FlightTo")!=null){
                    item = "outbound destination";
                    actual = journeyDestination.get(flightCounter-1).getText();
                    expected = testData.get("mb_FlightTo");
                    Assert.assertTrue(actual.contains(expected));
                    flightDetails.put("outboundDestination",journeyDestination.get(flightCounter-1).getText());
                }
            }
            if(changeInInbound){
                if(testData.get("mb_ReturnTicketType")!=null){
                    flightDetails.put("inboundTicketType",testData.get("mb_ReturnTicketType"));
                }

                if(testData.get("mb_FlightFrom")!=null){
                    item = "inbound source";
                    actual = journeySource.get(flightCounter).getText();
                    expected = testData.get("mb_FlightFrom");
                    Assert.assertTrue(actual.contains(expected));
                    flightDetails.put("inboundSource",journeySource.get(flightCounter).getText());
                }
                if(testData.get("mb_FlightTo")!=null){
                    item = "inbound destination";
                    actual = journeyDestination.get(journeyDestination.size()-1).getText();
                    expected = testData.get("mb_FlightTo");
                    Assert.assertTrue(actual.contains(expected));
                    flightDetails.put("inboundDestination",journeyDestination.get(journeyDestination.size()-1).getText());
                }
                flightDetails.put("inboundDuration",journeyDuration.get(1).getText());
                if(testData.get("changeReturnFlightNumber")!=null){
                    try {
                        item = "inbound flight-1 number";
                        Assert.assertTrue(flightDetails.get("inboundFlightNo1") != flightNos.get(flightCounter).getText());
                        reportLogger.log(LogStatus.INFO,"flight number in booking flow - "+flightDetails.get("inboundFlightNo1")+" modified successfully in MB flow to "+flightNos.get(flightCounter).getText());
                    }catch (AssertionError ae){
                        reportLogger.log(LogStatus.INFO,"modification in flight number - "+flightDetails.get("inboundFlightNo1")+" is not displayed correctly in itinerary section on flight details page");
                        throw ae;
                    }
                }
                flightDetails.put("inboundFlightNo1",flightNos.get(flightCounter).getText());
                flightDetails.put("inboundAircraft1",aircraft.get(flightCounter++).getText());
                flightDetails.put("inboundSector1TakeOffTime",journeyTimings.get(timeCounter++).getText());
                flightDetails.put("inboundSector1LandingTime",journeyTimings.get(timeCounter++).getText());
                int inboundNoOfStops = Integer.parseInt(testData.get("returnNoOfStops"));
                if(testData.get("mb_ReturnNoOfStops")!=null) {
                    inboundNoOfStops = Integer.parseInt(testData.get("mb_ReturnNoOfStops"));
                }
                if(inboundNoOfStops>0){
                    flightDetails.put("inboundNoOfFlights",(inboundNoOfStops+1)+"");
                    for(int i=1;i<=inboundNoOfStops;i++) {
                        flightDetails.put("inboundStop"+i,stop.get(stopCounter++).getText());
                        if(testData.get("changeReturnFlight")!=null){
                            try {
                                item = "inbound flight-"+(i+1)+" number";
                                Assert.assertTrue(flightDetails.get("inboundFlightNo"+(i+1)) != flightNos.get(flightCounter).getText());
                                reportLogger.log(LogStatus.INFO,"flight number in booking flow - "+flightDetails.get("inboundFlightNo"+(i+1))+" modified successfully in MB flow to "+flightNos.get(flightCounter).getText());
                            }catch (AssertionError ae){
                                reportLogger.log(LogStatus.INFO,"modification in flight number - "+flightDetails.get("inboundFlightNo"+(i+1))+" is not displayed correctly in itinerary section on flight details page");
                                throw ae;
                            }
                        }
                        flightDetails.put("inboundFlightNo"+(i+1),flightNos.get(flightCounter).getText());
                        flightDetails.put("inboundAircraft"+(i+1),aircraft.get(flightCounter++).getText());
                        flightDetails.put("inboundSector"+(i+1)+"TakeOffTime",journeyTimings.get(timeCounter++).getText());
                        flightDetails.put("inboundSector"+(i+1)+"LandingTime",journeyTimings.get(timeCounter++).getText());
                    }
                }
            }
            reportLogger.log(LogStatus.INFO,"successfully verified changes in source/destination/journey date details in itinerary section");
        }catch (AssertionError ae){
            System.out.println(item+" = "+expected+" -- "+actual);
            reportLogger.log(LogStatus.INFO,item+" expected - "+expected+" but displayed is "+actual);
            throw ae;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify and retrieve flight details from booking details section on MB fare select page");
            throw e;
        }
        System.out.println("FLIGHT DETAILS AFTER MB "+flightDetails);
        return flightDetails;
    }

    public void verifyTravellerSection(HashMap<String,String> testData){
        //verifies traveller section
        String expected=null,actual=null,item=null;
        try{
            if (Integer.parseInt(testData.get("noOfAdult")) > 0) {
                int noOfAdult = Integer.parseInt(testData.get("noOfAdult"));
                for (int i=0;i<noOfAdult; i++) {
                    item = "adult"+(i+1)+" name";
                    expected = testData.get("adult" + (i + 1) + "Name").split(" ")[1]+" "+ testData.get("adult" + (i + 1) + "Name").split(" ")[2];
                    actual = TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_name, "ADT"))).get(i).getText();;
                    Assert.assertEquals(actual,expected);
                }
            }
            if (testData.get("noOfTeen")!=null) {
                int noOfTeen = Integer.parseInt(testData.get("noOfTeen"));
                for (int i=0;i<noOfTeen; i++) {
                    item = "teen"+(i+1)+" name";
                    expected = testData.get("teen" + (i + 1) + "Name").split(" ")[1]+" "+ testData.get("teen" + (i + 1) + "Name").split(" ")[2];
                    actual = TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_name, "YTH"))).get(i).getText();;
                    Assert.assertEquals(actual,expected);
                }
            }
            if (testData.get("noOfChild")!=null) {
                int noOfChild = Integer.parseInt(testData.get("noOfChild"));
                for (int i=0;i<noOfChild; i++) {
                    item = "child"+(i+1)+" name";
                    expected = testData.get("child" + (i + 1) + "Name").split(" ")[1]+" "+testData.get("child" + (i + 1) + "Name").split(" ")[2];
                    actual = TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_name, "CHD"))).get(i).getText();;
                    Assert.assertEquals(actual,expected);
                }
            }
            if (testData.get("noOfInfant")!=null) {
                int noOfInfant = Integer.parseInt(testData.get("noOfInfant"));
                for (int i=0;i<noOfInfant; i++) {
                    item = "infant"+(i+1)+" name";
                    expected = testData.get("infant" + (i + 1) + "Name").split(" ")[1] + testData.get("infant" + (i + 1) + "Name").split(" ")[2];
                    actual = TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_name, "INF"))).get(i).getText();;
                    Assert.assertEquals(actual,expected);
                }
            }
            //Assert.assertEquals(emailId.getText().trim(),testData.get("1").get); //todo contact info
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,item+" - "+expected+" is displayed as "+actual);
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify traveller section on MB flight details page");
            throw e;
        }
    }

    public void verifyBasket(HashMap<String,Double> basket){
        //verify basket
        try{
            verify("to be paid",basket.get("to be paid"),CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(basketItems,"to be paid"))).getText()));
            if(basket.get("change fee")>0){
                verify("change fee",basket.get("change fee"),CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(basketItems,"Change fee"))).getText()));
            }
            else if(basket.get("change fee")==0.0){
                try{
                    Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(String.format(basketItems,"Change fee")));
                }catch(AssertionError e){
                    reportLogger.log(LogStatus.INFO,"change fee is added in basket even though change fee is not applicable for this booking");
                    throw e;
                }
            }
            verify("price difference",basket.get("price difference"),CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(basketItems,"Price difference"))).getText()));
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify basket on flight details page in MB");
            throw e;
        }
    }
}
