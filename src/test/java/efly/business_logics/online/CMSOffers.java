package efly.business_logics.online;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.relevantcodes.extentreports.model.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.TestManager;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Sona on 4/12/2019.
 */
public class CMSOffers {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public CMSOffers() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(css = "div[id*='otherDestinations'] a")
    private List<WebElement> cheapFlightsTo;

    @FindBy(id = "flight-search-button" )
    private WebElement findFlights;

    @FindBy(css = "img[alt='Special Offers Banner']")
    private WebElement specialOfferImage;

    String tileToBeSelected = "div[id*='otherDestinations'] a[href*='%s']";

    public void selectFeaturedFlight(String destination){
        //selects a featured flight and returns the destination selected
        try{
            actionUtility.hardClick(TestManager.getDriver().findElement(By.cssSelector(String.format(tileToBeSelected,destination.toLowerCase()))));
            actionUtility.waitForPageLoad(30);
            reportLogger.log(LogStatus.INFO,"successfully selected flights for destination "+destination);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select featured flight on offers page");
            throw e;
        }
    }

    public void verifyOffersPageNavigation(){
        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(findFlights));
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(specialOfferImage));
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"offers page is displayed with error");
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify if offers page is displayed");
            throw e;
        }
    }

}
