package efly.business_logics.online;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.CommonUtility;
import utilities.TestManager;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Sona on 4/3/2019.
 */
public class EFLYSeats {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public EFLYSeats(){
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    //timeout
    @FindBy(xpath = "//span[text()='Add more time']")
    private WebElement addMoreTime;

    @FindBy(id= "seatmap")
    private WebElement seatMap;

    @FindBy(xpath = "//span[contains(text(),'The seat map is not available for this flight.')]")
    private WebElement unavailableSeatMap;

    @FindBy(xpath = "//span[text()='Skip seats']")
    private WebElement skipSeat;

    @FindBy(xpath = "//button[contains(text(),'CONTINUE WITHOUT SEATS')]")
    private WebElement continueWithoutSeat;

    @FindBy(id = "dialog-empty-seat-dialog")
    private WebElement withoutSeatPopup;

    @FindBy(xpath = "//td[section[@class='extra-leg']]/div[not(contains(@class,'unavailable'))][not(@data-seat-selected='true')]")
    private List<WebElement> extraLegSeat;

    @FindBy(xpath = "//td[not(section)]/div[contains(@class,'icon-seat')][not(contains(@class,'unavailable'))][not(@data-seat-selected='true')][not(contains(@class,'FC'))]")
    private List<WebElement> standardSeat;

    @FindBy(xpath = "//td[not(section)]/div[contains(@class,'icon-seat')][not(contains(@class,'unavailable'))][not(@data-seat-selected='true')][contains(@class,'FC')]")
    private List<WebElement> focSeat;

    String restrictedSeat = "//div[@data-seat='%s'][contains(@class,'icon-ban-circle')]";

    String unavailableSeat = "//div[@data-seat='%s'][contains(@class,'unavailable')]";

    String availableSeat = "//div[@data-seat='%s'][contains(@class,'chargeable')]";

    @FindBy(xpath = "//button[contains(text(),'NEXT FLIGHT')]")
    private WebElement nextFlight;

    @FindBy(xpath = "//button[contains(text(),'PREVIOUS FLIGHT')]")
    private WebElement previousFlight;

    @FindBy(className = "seatNumber")
    private List<WebElement> seatNo;

    @FindBy(xpath = "//button/span[text()='CONTINUE WITH SELECTED SEATS']")
    private WebElement continueWithSeat;

    @FindBy(css = "div[id='dialog-incomplete-seat-dialog'] div[class='continue-button-container'] button")
    private WebElement continueWithSelectedSeats;

    String paxButton = "//div[div[div[div[label[div[strong[text()='%s']]]]]]]";

    String adultWithInfant = "//label[div[strong[text()='%s']]]/div[@class='travelWith']";

    String seatPrice = "//div[div[div[div[label[div[strong[text()='%s']]]]]]]//span[@class='primaryCurrency']/div";

    String seatNumber = "//span[@class='seatNumber']";

    //basket
    private String serviceCost = "//div[h3[contains(text(),'%s')]]//span[@class='tripsummary-price-amount-text']";

    @FindBy(className = "price-details-services-amount-value")
    private WebElement services;

    @FindBy(css = "div[class*='tripsummary-price-total'] span[class='tripsummary-price-amount-text']")
    private WebElement totalPrice;


    public void skipSeatsAndContinueToExtras(HashMap<String,String>... testData){
        //continues without adding seat
        try{
            if(testData.length>0 && testData[0].get("departOperatorName")!=null && testData[0].get("departOperatorName").equalsIgnoreCase("Air France")){
                actionUtility.waitForElementVisible(unavailableSeatMap,90);
            }
            else {
                actionUtility.waitForElementVisible(seatMap, 90);
            }
            actionUtility.hardClick(skipSeat);
            actionUtility.waitForElementVisible(withoutSeatPopup,15);
            actionUtility.waitForElementVisible(continueWithoutSeat,15);
            actionUtility.click(continueWithoutSeat);
            reportLogger.log(LogStatus.INFO,"successfully skipped adding seat and navigated to booking page");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to continue by skipping seat");
            throw e;
        }
    }

    public void verifyRestrictedForSpecialAssistance(HashMap<String,String> testData){
        actionUtility.waitForElementVisible(seatMap, 120);
        verifyRestrictedXLSeat(Integer.parseInt(testData.get("noOfXLSeat")),testData.get("XLseatNo"),"pax with special assistance");
        verifyRestrictedFOC_StandardSeat(testData,"pax with special assistance");
        verifyAvailableFOC_StandardSeat(testData,"pax with special assistance");
    }

    public void verifyRestrictedSeatForInfant(HashMap<String,String> testData) {
        try {
            actionUtility.waitForElementVisible(seatMap, 120);
            for(int i=1;i<=Integer.parseInt(testData.get("noOfInfant"));i++) {
                actionUtility.click(TestManager.getDriver().findElement(By.xpath(String.format(paxButton,testData.get("adult"+i+"Name")))));
                verifyRestrictedXLSeat(Integer.parseInt(testData.get("noOfXLSeat")),testData.get("XLseatNo"),"adult with infant");
                verifyRestrictedFOC_StandardSeat(testData,"adult with infant");
                verifyAvailableFOC_StandardSeat(testData,"adult with infant");
            }
            reportLogger.log(LogStatus.INFO,"successfully verified restricted seat map for adult with infant");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify restricted seat map for adult with infant");
            throw e;
        }
    }

    public void verifyRestrictedSeatForChild_Teen(HashMap<String,String> testData,String pax) {
        try {
            actionUtility.waitForElementVisible(seatMap, 120);
            for(int i=1;i<=Integer.parseInt(testData.get("noOf"+pax));i++) {
                actionUtility.click(TestManager.getDriver().findElement(By.xpath(String.format(paxButton,testData.get(pax.toLowerCase()+i+"Name")))));
                verifyRestrictedXLSeat(Integer.parseInt(testData.get("noOfXLSeat")),testData.get("XLseatNo"),pax.toLowerCase());
                verifyAvailableFOC_StandardSeat(testData,pax.toLowerCase());
            }
            reportLogger.log(LogStatus.INFO,"successfully verified restricted seat map for child");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify restricted seat map for child");
            throw e;
        }
    }

    private void verifyAvailableFOC_StandardSeat(HashMap<String,String> testData,String paxType){
        if(paxType.equalsIgnoreCase("child")){
            verifyAvailableSeatRow(Integer.parseInt(testData.get("seatAvailableForChild").split("-")[0]), Integer.parseInt(testData.get("seatAvailableForChild").split("-")[1]));
        }
        else if(paxType.equalsIgnoreCase("adult with infant")){
            //verifying available front seat for infants
            verifyAvailableSeatRow(Integer.parseInt(testData.get("frontSeatForInfant").split("-")[0]), Integer.parseInt(testData.get("frontSeatForInfant").split("-")[1]));
            //verifying available back seat for infants
            verifyAvailableSeatRow(Integer.parseInt(testData.get("backSeatForInfant").split("-")[0]), Integer.parseInt(testData.get("backSeatForInfant").split("-")[1]));
        }
        else if(paxType.equalsIgnoreCase("pax with special assistance")){
            verifyAvailableSeatRow(Integer.parseInt(testData.get("seatForSpecialAssistance").split("-")[0]), Integer.parseInt(testData.get("seatForSpecialAssistance").split("-")[1]));
        }
    }

    private void verifyAvailableSeatRow(int start,int stop){
        String seatBeingVerified =null;
        char[] seatColumn = {};
        for(int i=start;i<=stop;i++){
            if(i!=13) {
                seatColumn = new char[]{'A', 'B', 'C', 'D'};
                if (i == 2) {
                    seatColumn = new char[]{'A', 'B'};
                }
                for (int j = 0; j < seatColumn.length; j++) {
                    try {
                        seatBeingVerified = i + "" + seatColumn[j] + "";
                        Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(String.format(availableSeat, seatBeingVerified)) || actionUtility.verifyIfElementIsDisplayed(String.format(unavailableSeat, seatBeingVerified)));
                    } catch (AssertionError ae) {
                        System.out.println(seatBeingVerified + " - seat is restricted for adult with infant which is not expected");
                        reportLogger.log(LogStatus.INFO, seatBeingVerified + " - seat is restricted for adult with infant which is not expected");
                        throw ae;
                    }
                }
            }
        }
    }

    private void verifyRestrictedFOC_StandardSeat(HashMap<String,String> testData,String paxType){
        if(paxType.equalsIgnoreCase("adult with infant")) {
            //verifying restricted front seat
            verifyRestrictedSeatRow(Integer.parseInt(testData.get("restrictedFrontSeatForInfant").split("-")[0]), Integer.parseInt(testData.get("restrictedFrontSeatForInfant").split("-")[1]));
            //verifying restricted back seats
            verifyRestrictedSeatRow(Integer.parseInt(testData.get("restrictedBackSeatForInfant").split("-")[0]), Integer.parseInt(testData.get("restrictedBackSeatForInfant").split("-")[1]));
            //verifying restricted last seat
            verifyRestrictedSeatRow(Integer.parseInt(testData.get("restrictedLastSeatforInfant")), Integer.parseInt(testData.get("restrictedLastSeatforInfant")));
        }
        else if(paxType.equalsIgnoreCase("pax with special assistance")){
            verifyRestrictedSeatRow(Integer.parseInt(testData.get("restrictedFrontSeatForSpecialAssistance").split("-")[0]), Integer.parseInt(testData.get("restrictedFrontSeatForSpecialAssistance").split("-")[1]));
            verifyRestrictedSeatRow(Integer.parseInt(testData.get("restrictedLastSeatforSpecialAssistance")), Integer.parseInt(testData.get("restrictedLastSeatforSpecialAssistance")));
        }

    }

    private void verifyRestrictedSeatRow(int start,int stop){
        char[] seatColumn = {};
        String seatBeingVerified =null;
        for(int i=start;i<=stop;i++){
            for(int j=0;j<seatColumn.length;j++){
                try{
                    if(i!=13) { //todo
                        seatColumn = new char[]{'A', 'B', 'C', 'D'};
                        if (i == 2) {
                            seatColumn = new char[]{'A', 'B'};
                        }
                        seatBeingVerified = i + "" + seatColumn[j] + "";
                        Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(String.format(restrictedSeat, seatBeingVerified)) || actionUtility.verifyIfElementIsDisplayed(String.format(unavailableSeat, seatBeingVerified)));
                    }
                }catch(AssertionError ae){
                    System.out.println(seatBeingVerified+ " - seat is not restricted or unavailable for adult with infant");
                    reportLogger.log(LogStatus.INFO,seatBeingVerified+ " - seat is not restricted or unavailable for adult with infant");
                    throw ae;
                }
            }
        }
    }

    private void verifyRestrictedXLSeat(int noOfXLSeat,String seatNo,String paxType){
        String seatBeingVerified = null;
        try{
            for(int i=1;i<=noOfXLSeat;i++){
                seatBeingVerified = seatNo.split(",")[i-1];
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(String.format(restrictedSeat,seatBeingVerified)) || actionUtility.verifyIfElementIsDisplayed(String.format(unavailableSeat,seatBeingVerified)));
            }
        }catch(AssertionError ae){
            System.out.println(seatBeingVerified+ " - seat is not restricted or unavailable for "+paxType);
            reportLogger.log(LogStatus.INFO,seatBeingVerified+ " which is XL seat is not restricted or unavailable for "+paxType);
            throw ae;
        }
    }

    public HashMap<String,Double> selectSeat(HashMap<String,String> testData,HashMap<String,Double>... basket){
        //selects seat for each pax
        //verifies seat charges for just fly,get more, all in - chargeable or free of charge
        //returns basket with seat cost for each pax, total seat price. Also the basket will have paxTypeNo. with selected seat number
        try{
            actionUtility.waitForElementVisible(seatMap,120);
            int totalSectors = new Integer(testData.get("departNoOfStops"))+1;
            int noOfDepartureFlights = totalSectors;
            int noOfReturnFlights = 0;
            if(testData.get("returnDateOffset")!=null){
                totalSectors += new Integer(testData.get("returnNoOfStops"))+1;
                noOfReturnFlights = totalSectors - noOfDepartureFlights;
            }
            int noOfAdult =0;
            if(testData.get("noOfAdult")!=null) {
                noOfAdult = Integer.parseInt(testData.get("noOfAdult"));
            }
            for(int i=1;i<=totalSectors;i++) {
                if(basket.length>0) {
                    basket[0] = selectSeatPerSectorPerPax(testData, noOfAdult, "adult", i, noOfDepartureFlights, noOfReturnFlights, basket[0]);
                }
                else{
                    selectSeatPerSectorPerPax(testData, noOfAdult, "adult", i, noOfDepartureFlights, noOfReturnFlights);
                }
                if(totalSectors>1 && i!=totalSectors) {
                    actionUtility.click(nextFlight);
                    actionUtility.waitForElementVisible(seatMap,30);
                }
            }
            int noOfTeen = 0;
            int noOfChild = 0;
            if(testData.get("noOfTeen")!=null){
                viewFirstSectorSeatMap();
                noOfTeen = Integer.parseInt(testData.get("noOfTeen"));
                for(int i=1;i<=totalSectors;i++) {
                    if(basket.length>0){
                        basket[0] = selectSeatPerSectorPerPax(testData, noOfTeen, "teen", i, noOfDepartureFlights, noOfReturnFlights,basket[0]);
                    }
                    else{
                        selectSeatPerSectorPerPax(testData, noOfTeen, "teen", i, noOfDepartureFlights, noOfReturnFlights);
                    }
                    if(totalSectors>1 && i!=totalSectors) {
                        actionUtility.click(nextFlight);
                        actionUtility.waitForElementVisible(seatMap,30);
                    }
                }
            }
            if(testData.get("noOfChild")!=null){
                viewFirstSectorSeatMap();
                noOfChild = Integer.parseInt(testData.get("noOfChild"));
                for(int i=1;i<=totalSectors;i++) {
                    if(basket.length>0){
                        basket[0] = selectSeatPerSectorPerPax(testData, noOfChild, "child", i, noOfDepartureFlights, noOfReturnFlights,basket[0]);
                    }
                    else{
                        selectSeatPerSectorPerPax(testData, noOfChild, "child", i, noOfDepartureFlights, noOfReturnFlights);
                    }
                    if(totalSectors>1 && i!=totalSectors) {
                        actionUtility.click(nextFlight);
                        actionUtility.waitForElementVisible(seatMap,30);
                    }
                }
            }
            if(basket.length>0){
                System.out.println("BASKET AFTER ADDING SEAT - "+basket[0]);
                return basket[0];
            }
            return null;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select seat");
            throw e;
        }
    }

    private void viewFirstSectorSeatMap(){
        if(actionUtility.verifyIfElementIsDisplayed(previousFlight))
        do{
            actionUtility.click(previousFlight);
        }while(actionUtility.verfiyIfAnElementISEnabled(previousFlight));
    }

    private void clickAddMoreTime(){
        if(actionUtility.verifyIfElementIsDisplayed(addMoreTime)){
            actionUtility.click(addMoreTime);
        }
    }

    private HashMap<String, Double> selectSeatPerSectorPerPax(HashMap<String,String> testData,int noOfPax,String paxType,int sectorNo,int noOfDepartureFlights,int noOfReturnFlights,HashMap<String,Double>... basket){
        String selectedSeat = null;
        Double totalPrice = 0.00;
        Double eachPaxPrice = 0.00;
        WebElement pax = null;
        for(int i=1;i<=noOfPax;i++){
            if(testData.get(paxType+i+"SeatSector"+sectorNo)!=null) {
                pax = TestManager.getDriver().findElement(By.xpath(String.format(paxButton,testData.get(paxType+i+"Name"))));
                clickAddMoreTime();
                actionUtility.click(pax);
                clickAddMoreTime();
                if (testData.get(paxType + i + "SeatSector" + sectorNo).equalsIgnoreCase("XL")) {
                    selectedSeat = extraLegSeat.get(0).getAttribute("id").split("seat")[1];
                    actionUtility.click(extraLegSeat.get(0));
                    actionUtility.hardSleep(1000);
                }
                else if (testData.get(paxType + i + "SeatSector" + sectorNo).equalsIgnoreCase("FOC")) {
                    selectedSeat = focSeat.get(0).getAttribute("id").split("seat")[1];
                    actionUtility.click(focSeat.get(0));
                    actionUtility.hardSleep(1000);
                }
                else {
                    selectedSeat = standardSeat.get(1).getAttribute("id").split("seat")[1];
                    actionUtility.click(standardSeat.get(1));
                    actionUtility.hardSleep(1000);
                }
                reportLogger.log(LogStatus.INFO,"successfully selected seat - "+selectedSeat+" for passenger "+paxType+i+" - "+testData.get(paxType+i+"Name")+" for sector "+sectorNo);
                eachPaxPrice = calculateSeatCost(testData,testData.get(paxType+i+"SeatSector"+sectorNo),sectorNo,noOfDepartureFlights,noOfReturnFlights,testData.get(paxType+i+"Name"),testData.get("specialAssistance"));
                if(basket.length>0){
                    basket[0].put(paxType+i+"SeatSector"+sectorNo+"Price",eachPaxPrice);
                }
                totalPrice += eachPaxPrice;
            }
        }
        if(basket.length>0){
            if(basket[0].get("your seat")!=null) {
                totalPrice += basket[0].get("your seat");
                basket[0].put("your seat", totalPrice);
            }
            else
                basket[0].put("your seat",totalPrice);
            return  basket[0];
        }
            return null;
    }

    public void navigateToExtrasAfterSelectingSeat(){
        //navigate to extras page after selecting seat
        try {
            actionUtility.hardClick(continueWithSeat);
            if (actionUtility.verifyIfElementIsDisplayed(continueWithSelectedSeats, 30)) {
                actionUtility.hardClick(continueWithSelectedSeats);
            }
            reportLogger.log(LogStatus.INFO,"successfully navigated to extras page after selecting seat");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to continue to extras page after selecting seat");
            throw e;
        }
    }

    public HashMap<String,Double> verifyBasket(HashMap<String,Double> basket){
        //verifies and retrieves baggage,total and services cost in basket
        String item = null;
        Double itemPrice = 0.00;
        Double expectedPrice = 0.00;
        try{
            actionUtility.waitForElementVisible(totalPrice,90);
            if(basket.get("hold baggage") != null) {
                expectedPrice = basket.get("hold baggage");
                item = "hold baggage";
                itemPrice = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(serviceCost,"Hold baggage"))).getText());
                Assert.assertEquals(itemPrice,expectedPrice);
                reportLogger.log(LogStatus.INFO,"successfully verified hold luggage cost in basket - "+itemPrice);
            }
            if(basket.get("special assistance")!=null){
                expectedPrice = basket.get("special assistance");
                item = "special assistance";
                itemPrice = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(serviceCost,"Special Assistance"))).getText());
                Assert.assertEquals(itemPrice,expectedPrice);
                reportLogger.log(LogStatus.INFO,"successfully verified hold luggage cost in basket - "+itemPrice);
            }
            if(basket.get("service")!=null) {
                item = "services";
                expectedPrice = basket.get("services");
                itemPrice = CommonUtility.convertStringToDouble(services.getText());
                Assert.assertEquals(itemPrice, expectedPrice);
                reportLogger.log(LogStatus.INFO, "successfully verified services cost in basket - " + itemPrice);
            }
            item = "total";
            itemPrice = CommonUtility.convertStringToDouble(totalPrice.getText());
            expectedPrice = CommonUtility.truncateToTWODecimalPlaces(basket.get("total"));
            Assert.assertEquals(itemPrice,expectedPrice);
            reportLogger.log(LogStatus.INFO,"successfully verified total cost in basket - "+itemPrice);
            reportLogger.log(LogStatus.INFO,"successfully verified basket - hold baggage,services and total cost on seat page");
        }catch(Exception e){
            System.out.println(expectedPrice+" --- "+itemPrice);
            reportLogger.log(LogStatus.INFO,item+" cost displayed in basket - "+itemPrice+" doesn't match with expected "+item+" cost - "+expectedPrice+" on seat page");
            throw e;
        }
        return basket;
    }

    private Double calculateSeatCost(HashMap<String,String> testData,String seatType,int sectorNo,int noOfDepartureFlights,int noOfReturnFlights,String paxname,String splAssistance){
        //verifies seat price for  pax according to ticket type
        // returns price of seat selected for pax
        Double price = 0.00;
        if(seatType.equalsIgnoreCase("XL")) {
            if (sectorNo <= noOfDepartureFlights) {
                if (testData.get("departTicketType").equalsIgnoreCase("just fly")) {
                    price = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(seatPrice,paxname))).getText());
                    if(splAssistance!=null) {
                        try {
                            Assert.assertTrue(TestManager.getDriver().findElement(By.xpath(String.format(seatPrice, paxname))).getText().contains("Free of charge"));
                            reportLogger.log(LogStatus.INFO, "displayed XL seat price for ticket type - just fly for sector - " + sectorNo + " is free of charge");
                        } catch (AssertionError ae) {
                            reportLogger.log(LogStatus.INFO, "displayed XL seat price for ticket type - just fly for sector - " + sectorNo + " is not free of charge");
                            throw ae;
                        }
                    }
                    else {
                        if (testData.get("sector" + sectorNo + "XLSeatPrice") != null) {
                            try {
                                Assert.assertEquals(price, Double.parseDouble(testData.get("sector" + sectorNo + "XLSeatPrice")));
                                reportLogger.log(LogStatus.INFO, "XL seat price is as expected - " + price + " for sector " + sectorNo);
                            } catch (AssertionError ae) {
                                reportLogger.log(LogStatus.INFO, "displayed XL seat price for ticket type - just fly for sector - " + sectorNo + " is " + price + " and not " + testData.get("sector" + sectorNo + "XLSeatPrice") + " as expected");
                                throw ae;
                            }
                        } else {
                            try {
                                Assert.assertTrue(price > 0);
                            } catch (AssertionError ae) {
                                reportLogger.log(LogStatus.INFO, "displayed XL seat price for ticket type - just fly for sector - " + sectorNo + " is free of charge");
                                throw ae;
                            }
                        }
                    }
                } else if (testData.get("departTicketType").equalsIgnoreCase("get more")) {
                    price = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(seatPrice,paxname))).getText());
                    if(splAssistance!=null) {
                        try {
                            Assert.assertTrue(TestManager.getDriver().findElement(By.xpath(String.format(seatPrice, paxname))).getText().contains("Free of charge"));
                        } catch (AssertionError ae) {
                            reportLogger.log(LogStatus.INFO, "displayed XL seat price for ticket type - just fly for sector - " + sectorNo + " is free of charge");
                            throw ae;
                        }
                    }
                    else {
                        if (testData.get("sector" + sectorNo + "XLSeatPrice") != null) {
                            try {
                                Assert.assertEquals(price, Double.parseDouble(testData.get("sector" + sectorNo + "XLSeatPrice")));
                                reportLogger.log(LogStatus.INFO, "XL seat price is as expected - " + price + " for sector " + sectorNo);
                            } catch (AssertionError ae) {
                                reportLogger.log(LogStatus.INFO, "displayed XL seat price for ticket type - get more for sector - " + sectorNo + " is " + price + " and not " + testData.get("sector" + sectorNo + "XLSeatPrice") + " as expected");
                                throw ae;
                            }
                        } else {
                            try {
                                Assert.assertTrue(price > 0);
                            } catch (AssertionError ae) {
                                reportLogger.log(LogStatus.INFO, "displayed XL seat price for ticket type - get more for sector - " + sectorNo + " is " + price);
                                throw ae;
                            }
                        }
                    }
                } else if (testData.get("departTicketType").equalsIgnoreCase("all in")) {
                    try {
                        Assert.assertTrue(TestManager.getDriver().findElement(By.xpath(String.format(seatPrice,paxname))).getText().contains("Free of charge"));
                    } catch (AssertionError ae) {
                        reportLogger.log(LogStatus.INFO, "XL seat is not free of charge for ticket type - all in for sector - " + sectorNo);
                        throw ae;
                    }
                }
            }
            else if(noOfReturnFlights > 0 && sectorNo > noOfDepartureFlights){
                if (testData.get("returnTicketType").equalsIgnoreCase("just fly")) {
                    price = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(seatPrice,paxname))).getText());
                    if(splAssistance!=null) {
                        try {
                            Assert.assertTrue(TestManager.getDriver().findElement(By.xpath(String.format(seatPrice, paxname))).getText().contains("Free of charge"));
                            reportLogger.log(LogStatus.INFO, "displayed XL seat price for ticket type - just fly for sector - " + sectorNo + " is free of charge");
                        } catch (AssertionError ae) {
                            reportLogger.log(LogStatus.INFO, "displayed XL seat price for ticket type - just fly for sector - " + sectorNo + " is not free of charge for special assistance");
                            throw ae;
                        }
                    }
                    else {
                        if (testData.get("sector" + sectorNo + "XLSeatPrice") != null) {
                            try {
                                Assert.assertEquals(price, Double.parseDouble(testData.get("sector" + sectorNo + "XLSeatPrice")));
                                reportLogger.log(LogStatus.INFO, "XL seat price is as expected - " + price + " for sector " + sectorNo);
                            } catch (AssertionError ae) {
                                reportLogger.log(LogStatus.INFO, "displayed XL seat price for ticket type - just fly for sector - " + sectorNo + " is " + price + " and not " + testData.get("sector" + sectorNo + "XLSeatPrice") + " as expected");
                                throw ae;
                            }
                        } else {
                            try {
                                Assert.assertTrue(price > 0);
                            } catch (AssertionError ae) {
                                reportLogger.log(LogStatus.INFO, "displayed XL seat price for ticket type - just fly for sector - " + sectorNo + " is " + price);
                                throw ae;
                            }
                        }
                    }
                } else if (testData.get("returnTicketType").equalsIgnoreCase("get more")) {
                    price = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(seatPrice,paxname))).getText());
                    if(splAssistance!=null) {
                        try {
                            Assert.assertTrue(TestManager.getDriver().findElement(By.xpath(String.format(seatPrice,paxname))).getText().contains("Free of charge"));
                            reportLogger.log(LogStatus.INFO, "displayed XL seat price for ticket type - just fly for sector - " + sectorNo + " is free of charge");
                            reportLogger.log(LogStatus.INFO, "displayed XL seat price for ticket type - just fly for sector - " + sectorNo + " is free of charge for special assistance");
                        } catch (AssertionError ae) {
                            reportLogger.log(LogStatus.INFO, "displayed XL seat price for ticket type - just fly for sector - " + sectorNo + " is free of charge for special assistance");
                            throw ae;
                        }
                    }
                    else {
                        if (testData.get("sector" + sectorNo + "XLSeatPrice") != null) {
                            try {
                                Assert.assertEquals(price, Double.parseDouble(testData.get("sector" + sectorNo + "XLSeatPrice")));
                                reportLogger.log(LogStatus.INFO, "XL seat price is as expected - " + price + " for sector " + sectorNo);
                            } catch (AssertionError ae) {
                                reportLogger.log(LogStatus.INFO, "displayed XL seat price for ticket type - get more for sector - " + sectorNo + " is " + price + " and not " + testData.get("sector" + sectorNo + "XLSeatPrice") + " as expected");
                                throw ae;
                            }
                        } else {
                            try {
                                Assert.assertTrue(price > 0);
                            } catch (AssertionError ae) {
                                reportLogger.log(LogStatus.INFO, "displayed XL seat price for ticket type - get more for sector - " + sectorNo + " is " + price);
                                throw ae;
                            }
                        }
                    }
                } else if (testData.get("returnTicketType").equalsIgnoreCase("all in")) {
                    try {
                        Assert.assertTrue(TestManager.getDriver().findElement(By.xpath(String.format(seatPrice,paxname))).getText().contains("Free of charge"));
                    } catch (AssertionError ae) {
                        reportLogger.log(LogStatus.INFO, "XL seat is not free of charge for ticket type - all in for sector - " + sectorNo);
                        throw ae;
                    }
                }
            }
        }
        else if(seatType.equalsIgnoreCase("standard")){
            if (sectorNo <= noOfDepartureFlights) {
                if (testData.get("departTicketType").equalsIgnoreCase("just fly")) {
                    price = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(seatPrice,paxname))).getText());
                    if(testData.get("sector"+sectorNo+"StandardSeatPrice")!=null){
                        try{
                            Assert.assertEquals(price,Double.parseDouble(testData.get("sector"+sectorNo+"StandardSeatPrice")));
                            reportLogger.log(LogStatus.INFO,"standard seat price is as expected - "+price+" for sector "+sectorNo);
                        }catch (AssertionError ae) {
                            reportLogger.log(LogStatus.INFO, "displayed standard seat price for ticket type - just fly for sector - " + sectorNo+" is "+price+" and not "+testData.get("sector"+sectorNo+"XLSeatPrice")+" as expected");
                            throw ae;
                        }
                    }
                    else {
                        if (splAssistance == null) {
                            try {
                                Assert.assertTrue(price > 0);
                            } catch (AssertionError ae) {
                                reportLogger.log(LogStatus.INFO, "displayed standard seat price for ticket type - just fly for sector - " + sectorNo + " is " + price);
                                throw ae;
                            }
                        } else {
                            try {
                                Assert.assertTrue(TestManager.getDriver().findElement(By.xpath(String.format(seatPrice, paxname))).getText().contains("Free of charge"));
                                reportLogger.log(LogStatus.INFO, "displayed standard seat price for ticket type - just fly for sector - " + sectorNo + " is free of charge for passenger with special assistance");
                            } catch (AssertionError ae) {
                                reportLogger.log(LogStatus.INFO, "displayed standard seat price for ticket type - just fly for sector - " + sectorNo + " is " + price + " and not free of charge for passenger with special assistance");
                                throw ae;
                            }
                        }
                    }
                } else if (testData.get("departTicketType").equalsIgnoreCase("get more")) {
                    try {
                        Assert.assertTrue(TestManager.getDriver().findElement(By.xpath(String.format(seatPrice,paxname))).getText().contains("Free of charge"));
                    } catch (AssertionError ae) {
                        reportLogger.log(LogStatus.INFO, "standard seat is not free of charge for ticket type - get more for sector - " + sectorNo);
                        throw ae;
                    }
                } else if (testData.get("departTicketType").equalsIgnoreCase("all in")) {
                    try {
                        Assert.assertTrue(TestManager.getDriver().findElement(By.xpath(String.format(seatPrice,paxname))).getText().contains("Free of charge"));
                    } catch (AssertionError ae) {
                        reportLogger.log(LogStatus.INFO, "standard seat is not free of charge for ticket type - all in for sector - " + sectorNo);
                        throw ae;
                    }
                }
            }
            else if(noOfReturnFlights > 0 && sectorNo > noOfDepartureFlights){
                if (testData.get("returnTicketType").equalsIgnoreCase("just fly")) {
                    price = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(seatPrice,paxname))).getText());
                    if(testData.get("sector"+sectorNo+"StandardSeatPrice")!=null){
                        try{
                            Assert.assertEquals(price,Double.parseDouble(testData.get("sector"+sectorNo+"StandardSeatPrice")));
                            reportLogger.log(LogStatus.INFO,"standard seat price is as expected - "+price+" for sector "+sectorNo);
                        }catch (AssertionError ae) {
                            reportLogger.log(LogStatus.INFO, "displayed standard seat price for ticket type - just fly for sector - " + sectorNo+" is "+price+" and not "+testData.get("sector"+sectorNo+"XLSeatPrice")+" as expected");
                            throw ae;
                        }
                    }
                    else {
                        if (splAssistance == null) {
                            try {
                                Assert.assertTrue(price > 0);
                            } catch (AssertionError ae) {
                                reportLogger.log(LogStatus.INFO, "displayed standard seat price for ticket type - just fly for sector - " + sectorNo + " is " + price);
                                throw ae;
                            }
                        } else {
                            try {
                                Assert.assertTrue(TestManager.getDriver().findElement(By.xpath(String.format(seatPrice, paxname))).getText().contains("Free of charge"));
                                reportLogger.log(LogStatus.INFO, "displayed standard seat price for ticket type - just fly for sector - " + sectorNo + " is free of charge for pax with special assistance");
                            } catch (AssertionError ae) {
                                reportLogger.log(LogStatus.INFO, "displayed standard seat price for ticket type - just fly for sector - " + sectorNo + " is " + price + " for pax with special assistance");
                                throw ae;
                            }
                        }
                    }
                } else if (testData.get("returnTicketType").equalsIgnoreCase("get more")) {
                    try {
                        Assert.assertTrue(TestManager.getDriver().findElement(By.xpath(String.format(seatPrice,paxname))).getText().contains("Free of charge"));
                    } catch (AssertionError ae) {
                        reportLogger.log(LogStatus.INFO, "standard seat is not free of charge for ticket type - get more for sector - " + sectorNo);
                        throw ae;
                    }
                } else if (testData.get("returnTicketType").equalsIgnoreCase("all in")) {
                    try {
                        Assert.assertTrue(TestManager.getDriver().findElement(By.xpath(String.format(seatPrice,paxname))).getText().contains("Free of charge"));
                    } catch (AssertionError ae) {
                        reportLogger.log(LogStatus.INFO, "standard seat is not free of charge for ticket type - all in for sector - " + sectorNo);
                        throw ae;
                    }
                }
            }
        }
        else if(seatType.equalsIgnoreCase("FOC")){
            if (sectorNo <= noOfDepartureFlights) {
                if (testData.get("departTicketType").equalsIgnoreCase("just fly")) {
                    price = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(seatPrice,paxname))).getText());
                    try {
                        Assert.assertTrue(price > 0);
                    } catch (AssertionError ae) {
                        reportLogger.log(LogStatus.INFO, "displayed FOC seat price for ticket type - just fly for sector - " + sectorNo + " is " + price);
                        throw ae;
                    }
                    if(testData.get("sector"+sectorNo+"FOCSeatPrice")!=null){
                        try{
                            Assert.assertEquals(price,Double.parseDouble(testData.get("sector"+sectorNo+"FOCSeatPrice")));
                            reportLogger.log(LogStatus.INFO,"FOC seat price is as expected - "+price+" for sector "+sectorNo);
                        }catch (AssertionError ae) {
                            reportLogger.log(LogStatus.INFO, "displayed FOC seat price for ticket type - just fly for sector - " + sectorNo+" is "+price+" and not "+testData.get("sector"+sectorNo+"XLSeatPrice")+" as expected");
                            throw ae;
                        }
                    }
                } else if (testData.get("departTicketType").equalsIgnoreCase("get more")) {
                    price = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(seatPrice,paxname))).getText());
                    try{
                        Assert.assertTrue(price>0);
                    }catch (AssertionError ae) {
                        reportLogger.log(LogStatus.INFO, "displayed FOC seat price for ticket type - get more for sector - " + sectorNo+" is "+price);
                        throw ae;
                    }
                    if(testData.get("sector"+sectorNo+"FOCSeatPrice")!=null){
                        try{
                            Assert.assertEquals(price,Double.parseDouble(testData.get("sector"+sectorNo+"FOCSeatPrice")));
                            reportLogger.log(LogStatus.INFO,"standard seat price is as expected - "+price+" for sector "+sectorNo);
                        }catch (AssertionError ae) {
                            reportLogger.log(LogStatus.INFO, "displayed FOC seat price for ticket type - get more for sector - " + sectorNo+" is "+price+" and not "+testData.get("sector"+sectorNo+"XLSeatPrice")+" as expected");
                            throw ae;
                        }
                    }
                } else if (testData.get("departTicketType").equalsIgnoreCase("all in")) {
                    try {
                        Assert.assertTrue(TestManager.getDriver().findElement(By.xpath(String.format(seatPrice,paxname))).getText().contains("Free of charge"));
                    } catch (AssertionError ae) {
                        reportLogger.log(LogStatus.INFO, "foc seat is not free of charge for ticket type - all in for sector - " + sectorNo);
                        throw ae;
                    }
                }
            }
            else if(noOfReturnFlights > 0 && sectorNo > noOfDepartureFlights){
                if (testData.get("returnTicketType").equalsIgnoreCase("just fly")) {
                    price = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(seatPrice,paxname))).getText());
                    try {
                        Assert.assertTrue(price > 0);
                    } catch (AssertionError ae) {
                        reportLogger.log(LogStatus.INFO, "displayed FOC seat price for ticket type - just fly for sector - " + sectorNo + " is " + price);
                        throw ae;
                    }
                    if(testData.get("sector"+sectorNo+"FOCSeatPrice")!=null){
                        try{
                            Assert.assertEquals(price,Double.parseDouble(testData.get("sector"+sectorNo+"FOCSeatPrice")));
                            reportLogger.log(LogStatus.INFO,"FOC seat price is as expected - "+price+" for sector "+sectorNo);
                        }catch (AssertionError ae) {
                            reportLogger.log(LogStatus.INFO, "displayed FOC seat price for ticket type - just fly for sector - " + sectorNo+" is "+price+" and not "+testData.get("sector"+sectorNo+"XLSeatPrice")+" as expected");
                            throw ae;
                        }
                    }
                }
                else if (testData.get("returnTicketType").equalsIgnoreCase("get more")) {
                    price = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(seatPrice,paxname))).getText());
                    try{
                        Assert.assertTrue(price>0);
                    }catch (AssertionError ae) {
                        reportLogger.log(LogStatus.INFO, "displayed FOC seat price for ticket type - get more for sector - " + sectorNo+" is "+price);
                        throw ae;
                    }
                    if(testData.get("sector"+sectorNo+"FOCSeatPrice")!=null){
                        try{
                            Assert.assertEquals(price,Double.parseDouble(testData.get("sector"+sectorNo+"FOCSeatPrice")));
                            reportLogger.log(LogStatus.INFO,"standard seat price is as expected - "+price+" for sector "+sectorNo);
                        }catch (AssertionError ae) {
                            reportLogger.log(LogStatus.INFO, "displayed FOC seat price for ticket type - get more for sector - " + sectorNo+" is "+price+" and not "+testData.get("sector"+sectorNo+"XLSeatPrice")+" as expected");
                            throw ae;
                        }
                    }
                } else if (testData.get("returnTicketType").equalsIgnoreCase("all in")) {
                    try {
                        Assert.assertTrue(TestManager.getDriver().findElement(By.xpath(String.format(seatPrice,paxname))).getText().contains("Free of charge"));
                    } catch (AssertionError ae) {
                        reportLogger.log(LogStatus.INFO, "FOC seat is not free of charge for ticket type - all in for sector - " + sectorNo);
                        throw ae;
                    }
                }
            }
        }
        reportLogger.log(LogStatus.INFO,"seat price for passenger - "+paxname+" is "+price+" for sector "+sectorNo);
        return price;
    }

    public HashMap<String,String> retrieveSeatSelectedForEachPax(HashMap<String,String> testData){
        HashMap<String,String> seatDetails = new HashMap<>();
        try {
            viewFirstSectorSeatMap();
            int totalSectors = new Integer(testData.get("departNoOfStops"))+1;
            if(testData.get("returnDateOffset")!=null){
                totalSectors += new Integer(testData.get("returnNoOfStops"))+1;
            }
            int noOfPax;
            for(int i=1;i<=totalSectors;i++) {
                seatDetails.put("counter","0");
                if(testData.get("noOfAdult")!=null){
                    noOfPax = Integer.parseInt(testData.get("noOfAdult"));
                    seatDetails = captureSeatNumber(testData, "adult", noOfPax,i,seatDetails);
                }
                if(testData.get("noOfChild")!=null){
                    noOfPax = Integer.parseInt(testData.get("noOfChild"));
                    seatDetails = captureSeatNumber(testData, "child", noOfPax,i,seatDetails);
                }
                if(testData.get("noOfTeen")!=null){
                    noOfPax = Integer.parseInt(testData.get("noOfTeen"));
                    seatDetails = captureSeatNumber(testData, "teen", noOfPax,i,seatDetails);
                }
                if(totalSectors>1 && i!=totalSectors) {
                    actionUtility.click(nextFlight);
                    actionUtility.waitForElementVisible(seatMap,30);
                }
            }
            reportLogger.log(LogStatus.INFO,"successfully retrieved seat selected for passengers - "+seatDetails);
            System.out.println("SEATs added in booking flow - "+seatDetails);
            return seatDetails;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to retrieve seat selected for passengers");
            throw e;
        }
    }

    private HashMap<String,String> captureSeatNumber(HashMap<String,String> testData,String pax,int noOfPax,int sector,HashMap<String,String> seatDetails){
        String seatSelected = null;
        int counter = Integer.parseInt(seatDetails.get("counter"));
        int noOfPaxTypeWithSeat = 0;
        for(int j=1;j<=noOfPax;j++) {
            if(testData.get(pax+j+"SeatSector"+sector)!=null){
                seatSelected = seatNo.get(counter++).getText();
                seatDetails.put("counter",counter+"");
            }
            if(seatSelected!=null){
                noOfPaxTypeWithSeat++;
                seatDetails.put(pax+j+"SeatNoForSector"+sector,seatSelected);
            }
            seatSelected = null;
        }
        seatDetails.put("noOf"+pax+"withSeatForSector"+sector,noOfPaxTypeWithSeat+"");
        return seatDetails;
    }
}

