package efly.business_logics.online;

import com.google.gson.JsonObject;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utilities.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static utilities.ActionUtility.SelectionType.SELECTBYTEXT;

public class EFLYLanguageVariantDE{

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public EFLYLanguageVariantDE(){
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(id = "search-widget")
    private WebElement searchWidget;

    @FindBy(xpath = "//div[contains(@class,'teenItaly')]")
    private WebElement unaccompaniedMinorMessage;

    @FindBy(xpath = "//div[@id='flight_from_chosen']/a/div/b")
    private WebElement flightFromDropDown;

    @FindBy(xpath = "//div[@id ='flight_from_chosen']//input")
    private WebElement flightFrom;

    @FindBy(xpath = "//div[@id='flight_to_chosen']/a")
    private WebElement flightToDropDown;

    @FindBy(xpath = "//div[@id='flight_to_chosen']//input")
    private WebElement flightTo;

    @FindBy(xpath = "//div[@id='flight_to_chosen']/a[@class='chosen-single']/span")
    private WebElement selectedDestination;

    //@FindBy(id = "ui-datepicker-div")
    private WebElement datePicker;

    @FindBy(className = "ui-datepicker-year")
    private WebElement datePickerYear;

    @FindBy(className = "ui-datepicker-month")
    private WebElement datePickerMonth;

    @FindBy(xpath = "//input[contains(@class,'inputAdults')]")
    private WebElement adultNos;

    @FindBy(xpath = "//input[contains(@class,'inputTeens')]")
    private WebElement teenNos;

    @FindBy(xpath = "//input[contains(@class,'inputChildren')]")
    private WebElement childrenNos;

    @FindBy(xpath = "//input[contains(@class,'inputInfants')]")
    private WebElement infantNos;

    @FindBy(xpath = "//button[contains(@class,'adult-up')]")
    private WebElement adultIncrement;

    @FindBy(xpath = "//button[contains(@class,'adult-down')]")
    private WebElement adultDecrement;

    @FindBy(xpath = "//button[contains(@class,'teen-up')]")
    private WebElement teenIncrement;

    @FindBy(xpath = "//button[contains(@class,'teen-down')]")
    private WebElement teenDecrement;

    @FindBy(xpath = "//button[contains(@class,'child-up')]")
    private WebElement childIncrement;

    @FindBy(xpath = "//button[contains(@class,'child-down')]")
    private WebElement childDecrement;

    @FindBy(xpath = "//button[contains(@class,'infant-up')]")
    private WebElement infantIncrement;

    @FindBy(xpath = "//button[contains(@class,'infant-down')]")
    private WebElement infantDecrement;

    @FindBy(css = "div[class='clearfix'] h3")
    private WebElement selectPassengerMenu;

    @FindBy(id = "pax-display")
    private WebElement openSelectPassengerMenu;

    @FindBy(xpath = "//div[contains(@class,'pax-close-button')]")
    private WebElement closeSelectPassengerMenu;

    @FindBy(id = "flight-search-button" )
    private WebElement findFlights;

    @FindBy(xpath = "//li[a[text()='My account']]")
    private WebElement myAccount;

    @FindBy(xpath = "//a[contains(text(),'Log in')]")
    private WebElement login;

    @FindBy (id = "sign-in-button")
    private WebElement loginOption;

    @FindBy(xpath = "//li/a[text()='Check-in']")
    private WebElement checkInTab;

    @FindBy(id = "label-booking-ref")
    private WebElement bookingRefForCheckIn;

    @FindBy(id = "label-forename")
    private WebElement foreNameForCheckIn;

    @FindBy(id = "label-surname")
    private WebElement surNameForCheckIn;

    @FindBy(xpath = "//div/button[contains(text(),'Check-in')]")
    private WebElement checkInButton;

    @FindBy(className ="loading-spinner")
    private WebElement checkInLoader;

    @FindBy(linkText = "Find flights")
    private WebElement findFlightTab;

    @FindBy(linkText = "View booking")
    private WebElement viewBookingTab;

    @FindBy(id = "ManageBookingRef")
    private WebElement bookingRefForViewBooking;

    @FindBy(id = "ManageBookingForename")
    private WebElement foreNameForViewBooking;

    @FindBy(id = "ManageBookingSurname")
    private WebElement surNameForViewBooking;

    @FindBy(className = "modal-dialog")
    private WebElement popUp;

    @FindBy(className = "modal-header")
    private WebElement popUpHeader;

    @FindBy(id = "accept-cookies")
    private WebElement acceptCookies;

    @FindBy(id = "amend-cookie-prefs")
    private WebElement amendCookies;

    @FindBy(id = "your-cookie-settings-button")
    private WebElement cookiePrefDone;

    @FindBy(css = ".slider.round")
    private List<WebElement> cookiePreference;

    String datePickerDay = "//div[@id='ui-datepicker-div']/div[contains(@class,'first')]/descendant::a[text()='%s']";

    //language
    @FindBy(css = "a[alt='Change language']")
    private WebElement changeLanguage;

    @FindBy(css = "ul[class='dropdown-menu']")
    private WebElement langDropdown;

    String selectLanguage = "a[href='/%s']";

    @FindBy(xpath = "//span[text()='Weiter']")
    private WebElement continueButton;

    //Select Flight
    @FindBy(xpath = "//div[div[div[div[div[h3[div[span[@class='direct']]]]]]]]//div[div[span[text()='JUST FLY']]]")
    List<WebElement> directJustFly;

    @FindBy(xpath = "//div[div[div[div[div[h3[div[span[@class='direct']]]]]]]]//div[div[span[text()='GET MORE']]]")
    List<WebElement> directGetMore;

    @FindBy(xpath = "//div[div[div[div[div[h3[div[span[@class='direct']]]]]]]]//div[div[span[text()='ALL IN']]]")
    List<WebElement> directAllIn;

    @FindBy(xpath = "//div[div[div[div[div[span[text()='1 stop']]]]]]//div[div[span[text()='JUST FLY']]]")
    List<WebElement> oneStopJustFly;

    @FindBy(xpath = "//div[div[div[div[div[span[text()='1 stop']]]]]]//div[div[span[text()='GET MORE']]]")
    List<WebElement> oneStopGetMore;

    @FindBy(xpath = "//div[div[div[div[div[span[text()='1 stop']]]]]]//div[div[span[text()='ALL IN']]]")
    List<WebElement> oneStopAllIn;

    @FindBy(xpath = "//div[div[div[div[div[span[text()='2 stops']]]]]]//div[div[span[text()='JUST FLY']]]")
    List<WebElement> twoStopJustFly;

    @FindBy(xpath = "//div[div[div[div[div[span[text()='2 stops']]]]]]//div[div[span[text()='GET MORE']]]")
    List<WebElement> twoStopGetMore;

    @FindBy(xpath = "//div[div[div[div[div[span[text()='2 stops']]]]]]//div[div[span[text()='ALL IN']]]")
    List<WebElement> twoStopAllIn;

    private String directOtherOperatorJustFly = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='Direct']]]]]//div[div[span[text()='JUST FLY']]]";
    private String directOtherOperatorGetMore = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='Direct']]]]]//div[div[span[text()='GET MORE']]]";
    private String directOtherOperatorAllIn = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='Direct']]]]]//div[div[span[text()='ALL IN']]]";

    private String oneStopOtherOperatorJustFly = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='1 stop']]]]]//div[div[span[text()='JUST FLY']]]";
    private String oneStopOtherOperatorGetMore = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='1 stop']]]]]//div[div[span[text()='GET MORE']]]";
    private String oneStopOtherOperatorAllIn = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='1 stop']]]]]//div[div[span[text()='ALL IN']]]";

    private String twoStopOtherOperatorJustFly = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='2 stops']]]]]//div[div[span[text()='JUST FLY']]]";
    private String twoStopOtherOperatorGetMore = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='2 stops']]]]]//div[div[span[text()='GET MORE']]]";
    private String twoStopOtherOperatorAllIn = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='2 stops']]]]]//div[div[span[text()='ALL IN']]]";


    @FindBy(xpath = "//div[span[text()='JUST FLY']]/div/span")
    private List<WebElement> justFlyFares;

    @FindBy(xpath = "//div[span[text()='GET MORE']]/div/span")
    private List<WebElement> getMoreFares;

    @FindBy(xpath = "//div[span[text()='ALL IN']]/div/span")
    private List<WebElement> allInFares;

    @FindBy(className = "tripsummary-price-amount-text")
    private WebElement totalAmount;

    @FindBy(id = "button-tripsummary-booking")
    private WebElement bookingDetails;

    @FindBy(className = "price-total-amount-value")
    private WebElement fare;

    @FindBy(css = "ul[class*='price-mini-breakdown'] li span[class*='price-details-traveller-value']")
    private List<WebElement> travellerTotal;

    @FindBy(css = "span[class*='price-details-taxes-amount-value']")
    private List<WebElement> taxAndFees;

    @FindBy(css = "i[id*='traveller-taxes']")
    private List<WebElement> travellerTaxBreakdown;

    @FindBy(css = "span[class*='departure']")
    private List<WebElement> departure;

    @FindBy(css = "span[class*='return']")
    private List<WebElement> destination;

    @FindBy(css = "div[class='segment-details-departure '] div div:nth-child(2)")
    List<WebElement> journeySource;

    @FindBy(css = "div[class='segment-details-arrival '] div div:nth-child(2)")
    List<WebElement> journeyDestination;

    @FindBy(xpath = "//div[span[@class='segment-details-stop-location-name']]")
    List<WebElement> stop;

    @FindBy(xpath = "//div[span[@class='flight-info-airline-name']]")
    List<WebElement> flightNos;

    @FindBy(css = "div[class*='flight-info-aircraft'] div:nth-child(2)")
    List<WebElement> aircraft;

    @FindBy(className = "date")
    List<WebElement> journeyDates;

    @FindBy(css = "time[class*='segment-details-time']")
    List<WebElement> journeyTimings;

    @FindBy(className = "total-duration-value")
    List<WebElement> journeyDuration;

    //Flex popup
    @FindBy(css = "button[class='btn dialog-upgrade-btn flybe-btn']")
    private WebElement addFlex;

    @FindBy(css = "button[class='btn btn-link cancel-button']")
    private WebElement doNotAddFlex;

    @FindBy(className = "price-value")
    private WebElement flexPrice;

    //personal details
    @FindBy(css = "input[id*='ADT-ADS_NUMBER']")
    private List<WebElement> adsADT;

    @FindBy(css = "input[id*='ADT_INF-ADS_NUMBER']")
    private List<WebElement> adsINF;

    @FindBy(css = "input[id*='CHD-ADS_NUMBER']")
    private List<WebElement> adsCHD;

    @FindBy(css = "input[id*='YTH-ADS_NUMBER']")
    private List<WebElement> adsYTH;

    @FindBy(css = "select[id*='ADT-IDEN_TitleCode']")
    private List<WebElement> titleADT;

    @FindBy(css = "input[id*='ADT-IDEN_FirstName']")
    private List<WebElement> firstNameADT;

    @FindBy(css = "input[id*='ADT-IDEN_LastName']")
    private List<WebElement> lastNameADT;

    @FindBy(css = "input[id*='ADT_INF-IDEN_FirstName']")
    private List<WebElement> firstNameINF;

    @FindBy(css = "input[id*='ADT_INF-IDEN_LastName']")
    private List<WebElement> lastNameINF;

    @FindBy(css = "input[id*='ADT_INF-IDEN_DateOfBirth-DateDay']")
    private List<WebElement> birthDayINF;

    @FindBy(css = "select[id*='ADT_INF-IDEN_DateOfBirth-DateMonth']")
    private List<WebElement> birthMonthINF;

    @FindBy(css = "input[id*='ADT_INF-IDEN_DateOfBirth-DateYear']")
    private List<WebElement> birthYearINF;

    @FindBy(css = "select[id*='YTH-IDEN_TitleCode']")
    private List<WebElement> titleTeen;

    @FindBy(css = "input[id*='YTH-IDEN_FirstName']")
    private List<WebElement> firstNameTeen;

    @FindBy(css = "input[id*='YTH-IDEN_LastName']")
    private List<WebElement> lastNameTeen;

    @FindBy(css = "select[id*='CHD-IDEN_TitleCode']")
    private List<WebElement> titleChild;

    @FindBy(css = "input[id*='CHD-IDEN_FirstName']")
    private List<WebElement> firstNameChild;

    @FindBy(css = "input[id*='CHD-IDEN_LastName']")
    private List<WebElement> lastNameChild;

    @FindBy(css = "input[id*='CHD-IDEN_DateOfBirth-DateDay']")
    private List<WebElement> birthDayChild;

    @FindBy(css = "select[id*='CHD-IDEN_DateOfBirth-DateMonth']")
    private List<WebElement> birthMonthChild;

    @FindBy(css = "input[id*='CHD-IDEN_DateOfBirth-DateYear']")
    private List<WebElement> birthYearChild;

    //Booker contact information
    @FindBy(css = "div[class='booker-selector'] button")
    private List<WebElement> booker;

    @FindBy(css = "input[id*='apimTravellers-contactInformation-PhoneMobileCode']")
    private WebElement bookerPhoneCode;

    @FindBy(xpath = "//span[span[text()='Vorgeschlagene Werte']]//li[1]")
    private WebElement selectPhoneCode;

    @FindBy(name = "PhoneMobile")
    private WebElement bookerPhoneNumber;

    @FindBy(name = "Email")
    private WebElement bookerEmail;

    @FindBy(name = "EmailConfirm")
    private WebElement bookerEmailConfirmation;

    @FindBy(css = "input[id*='apimTravellers-contactInformation-PhoneSmsNotifCode']")
    private WebElement paxPhoneCode;

    @FindBy(name = "PhoneSmsNotif")
    private WebElement paxPhoneNumber;

    @FindBy(name = "EMailNotif")
    private WebElement paxEmail;

    //marketing preference
    private String mktPref = "//div[contains(@class,'consent-button-container')]/button[span[contains(text(),'%s')]]";

    //Bags
    private String bagCost = "//section[div[contains(@id,'%s')]]//button[span[span[contains(text(),'%s')]]]//span[@class='price-value']";

    private String adultBags= "//section[div[contains(@id,'paneltravellerCardIdADT')]]//button[span[span[contains(text(),'%s')]]]";

    private String teenBags = "//section[div[contains(@id,'paneltravellerCardIdYTH')]]//button[span[span[contains(text(),'%s')]]]";

    private String childBags = "//section[div[contains(@id,'paneltravellerCardIdCHD')]]//button[span[span[contains(text(),'%s')]]]";

    //Seats
    @FindBy(id= "seatmap")
    private WebElement seatMap;

    @FindBy(xpath = "//span[contains(text(),'The seat map is not available for this flight.')]")
    private WebElement unavailableSeatMap;

    @FindBy(xpath = "//span[text()='Sitzauswahl übergehen']")
    private WebElement skipSeat;

    @FindBy(xpath = "//button[contains(text(),'OHNE SITZPLATZWAHL FORTFAHREN')]")
    private WebElement continueWithoutSeat;

    @FindBy(id = "dialog-empty-seat-dialog")
    private WebElement withoutSeatPopup;

    @FindBy(xpath = "//td[section[@class='extra-leg']]/div[not(contains(@class,'unavailable'))][not(@data-seat-selected='true')]")
    private List<WebElement> extraLegSeat;

    @FindBy(xpath = "//td[not(section)]/div[contains(@class,'icon-seat')][not(contains(@class,'unavailable'))][not(@data-seat-selected='true')][not(contains(@class,'FC'))]")
    private List<WebElement> standardSeat;

    @FindBy(xpath = "//td[not(section)]/div[contains(@class,'icon-seat')][not(contains(@class,'unavailable'))][not(@data-seat-selected='true')][contains(@class,'FC')]")
    private List<WebElement> focSeat;

    String restrictedSeat = "//div[@data-seat='%s'][contains(@class,'icon-ban-circle')]";

    String unavailableSeat = "//div[@data-seat='%s'][contains(@class,'unavailable')]";

    String availableSeat = "//div[@data-seat='%s'][contains(@class,'chargeable')]";

    @FindBy(xpath = "//button[contains(text(),'NEXT FLIGHT')]")
    private WebElement nextFlight;

    @FindBy(xpath = "//button[contains(text(),'PREVIOUS FLIGHT')]")
    private WebElement previousFlight;

    @FindBy(className = "seatNumber")
    private List<WebElement> seatNo;

    @FindBy(xpath = "//button/span[text()='MIT DEN AUSGEWÄHLTEN SITZPLÄTZEN FORTFAHREN']")
    private WebElement continueWithSeat;

    @FindBy(css = "div[id='dialog-incomplete-seat-dialog'] div[class='continue-button-container'] button")
    private WebElement continueWithSelectedSeats;

    String paxButton = "//div[div[div[div[label[div[strong[text()='%s']]]]]]]";

    String adultWithInfant = "//label[div[strong[text()='%s']]]/div[@class='travelWith']";

    String seatPrice = "//div[div[div[div[label[div[strong[text()='%s']]]]]]]//span[@class='primaryCurrency']/div";

    String seatNumber = "//span[@class='seatNumber']";

    //payment details
    private String cardToBeSelected = "//div[label[span[contains(text(),'%s')]]]/input";

    private String loader = "//div[@class='loader']";

    @FindBy(css = "input[name*='cardNumber']")
    private WebElement cardNumber;

    @FindBy(name = "securityCode")
    private WebElement securityNumber;

    @FindBy(name = "nameOnCard")
    private WebElement nameOnCard;

    @FindBy(css = "select[id*='ccMonth']")
    private WebElement expiryMonth;

    @FindBy(css = "select[id*='ccYear']")
    private WebElement expiryYear;

    @FindBy(xpath = "//li[label[span[contains(text(),'PayPal')]]]")
    private WebElement paymentByPayPal;

    @FindBy(name = "ccCountry")
    private WebElement country;

    @FindBy(name = "ccPostalCode")
    private WebElement postCode;

    @FindBy(xpath = "//button[text()='Find my address']")
    private WebElement findMyAddress;

    @FindBy(css = "div[class='address-list-container'] input")
    private List<WebElement> address;

    @FindBy(xpath = "//button[contains(text(),'SELECT')]")
    private WebElement selectAddress;

    @FindBy(name = "ccState")
    private WebElement state;

    @FindBy(name = "ccAddressFirstLine")
    private WebElement addressLineOne;

    @FindBy(name = "ccCity")
    private WebElement city;

    //terms and conditions
    @FindBy(css = "div[id*='termsAndConditions'] div[class='checkbox'] input")
    private WebElement termsAndConditions;

    @FindBy(xpath = "//span[text()='Pay Now']")
    private WebElement payNow;

    //insurance
    private String insurancePopup = "//button[span[text()='%s']]";

    //3DS

    @FindBy(className = "vbv-iframe")
    private WebElement frame_3ds;

    @FindBy(css = "input[name='password'][id*='userInput']")
    private WebElement passCode;

    @FindBy(css = "form[id='masterForm'] input[value='Do Authentication']")
    private WebElement authenticate;

    @FindBy(id = "backToMerchant")
    private WebElement backToMerchant;

    //Confirmation page
    @FindBy(css = "span[class='number']")
    private WebElement reservationNo;

    //feedback
    @FindBy(xpath = "//button[text()='No thanks']")
    private WebElement noThanks;

    @FindBy(xpath = "//button[text()='Give feedback']")
    private WebElement giveFeedback;

    String feedback = "//h3[contains(text(),'feedback')]";

    String traveller_name = "div[class*='%s'] div[class*='traveller-name']";

    String traveller_etkt = "div[class*='%s'] div[class='ticket-container'] span";

    @FindBy (xpath = "//span[contains(text(),'Ticketinformationen')]")
    private WebElement ticketInformation;

    @FindBy (css = "li[class='reservation-name'] span:nth-child(2)")
    private WebElement reservationName;

    //Checkin
    @FindBy(xpath = "//a[contains(text(),'Meine Buchung')]")
    private WebElement myBooking;

    @FindBy(css = "button[class='secondary-button my-booking-button js-submit']")
    private WebElement checkinButton;

    @FindBy(id = "booking-reference")
    private WebElement bookingReference;

    @FindBy(id = "surname")
    private WebElement lastName;

    @FindBy(id = "flybe_checkin_btn")
    private WebElement checkin;

    @FindBy(css = "label[for='AcknowledgeDangerousGoodsCheckBox']")
    private WebElement readAndUnderstoodCheckBox;

    @FindBy(xpath = "//button[text()='Bestätigen']")
    private WebElement continueToJourneyDetailsButton;

    @FindBy(className = "pax-table")
    private WebElement passengerList;

    @FindBy(xpath = "//table[@class='pax-table']/tbody/tr/td[2]/span/span[2]/label/span[1]")
    private List<WebElement> selectPassenger;

    @FindBy(css = "button[title='Click to confirm the selection of the passengers.']")
    private WebElement continueToDangerousGoodsFromPassengerSelection;

    @FindBy(xpath = "//span[contains(text(),'Check-in bestätigen')]")
    private WebElement continueToPassengerDetailsButton;

    //Boarding pass
    @FindBy(xpath = "//div[@class='panel-heading-container ng-scope']//span[@class='ng-scope'][contains(text(),'Bordkarten drucken')]")
    private WebElement printBoardingPassSection;

    @FindBy(xpath = "//button[contains(@id,'buttonId_Printer')]")
    private WebElement printBoardingPassButton;

    @FindBy(xpath = "//button[span[text()='Finish check-in']]")
    private WebElement finishCheckIn;

    @FindBy(css = "span[ng-bind='flight.boardpoint.airportName']")
    private WebElement sourceAirport;

    @FindBy(css = "span[ng-bind='flight.offpoint.airportName']")
    private WebElement destinationAirport;

    @FindBy(css = "span[ng-bind*='flight.arrivalTime']")
    private WebElement arrivalTime;

    @FindBy(css = "span[ng-bind*='flight.departureTime']")
    private WebElement departureTime;

    @FindBy(css = "a[class='button continue ng-scope']")
    private WebElement checkinComplete;


    //HOME PAGE
    public Boolean enterSourceAndDestination(HashMap<String,String> testData, Boolean... cookieClosed){
        //enter source and destination
        Boolean popupHandled = false;
        try {
            actionUtility.hardSleep(2000);
            //actionUtility.click(findFlights);
            actionUtility.waitForElementClickable(flightFromDropDown,3);
            actionUtility.click(flightFromDropDown);
            actionUtility.hardSleep(2000);
            actionUtility.sendKeysByAction(flightFrom, testData.get("flightFrom") + Keys.RETURN);
            /*actionUtility.waitForElementClickable(flightToDropDown,3);
            actionUtility.click(flightToDropDown);*/
            actionUtility.hardSleep(2000);
            actionUtility.sendKeysByAction(flightTo, testData.get("flightTo") + Keys.RETURN);
            reportLogger.log(LogStatus.INFO,"successfully entered the source - "+testData.get("flightFrom")+" and destination - "+testData.get("flightTo"));
            return popupHandled;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to enter source and destination of the trip");
            throw e;
        }
    }

    public int selectNoOfPassengers(HashMap<String,String> testData){
        //select the no.of passengers
        try {
            int noOfPassengers=1;
            actionUtility.click(openSelectPassengerMenu);
            int noOfTeen = 0, noOfChild = 0, noOfInfant = 0, noOfAdult = 1;
            if (testData.get("noOfTeen") != null) {
                noOfTeen = Integer.parseInt(testData.get("noOfTeen"));

                noOfPassengers += noOfTeen;
                if(noOfTeen == 8)
                {
                    selectPassengers("teens",noOfTeen,Integer.parseInt(testData.get("noOfAdult")));
                }
                else{
                    selectPassengers("teens",noOfTeen);
                }
            }

            if (testData.get("noOfAdult") != null) {
                noOfAdult = Integer.parseInt(testData.get("noOfAdult"));
                selectPassengers("adults", noOfAdult);
                noOfPassengers += (noOfAdult-1);
            }

            if (testData.get("noOfChild") != null) {
                noOfChild = Integer.parseInt(testData.get("noOfChild"));
                selectPassengers("children", noOfChild);
                noOfPassengers += noOfChild;
            }
            if (testData.get("noOfInfant") != null) {
                noOfInfant = Integer.parseInt(testData.get("noOfInfant"));
                selectPassengers("infants", Integer.parseInt(testData.get("noOfInfant")));
            }
            try{
                actionUtility.click(closeSelectPassengerMenu);
            }catch (Exception e){}
            reportLogger.log(LogStatus.INFO,"successfully selected all types of passengers");
            reportLogger.log(LogStatus.INFO,"Adults - "+noOfAdult+" Teens - "+noOfTeen+" Children - "+noOfChild+" Infant - "+noOfInfant);

            return noOfPassengers;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to select the number of passengers");
            throw e;
        }
    }

    private void selectPassengers(String passengerType, int noOfPassenger, int... adultPassenger){
        // selects the no. of passengers
        String passengerNos ;
        boolean flag = true;
        int attempt =0;
        boolean attemptFlag = false;
        while(attempt<8 && attemptFlag == false) {
            try {
                if (!actionUtility.verifyIfElementIsDisplayed(selectPassengerMenu)) {
                    actionUtility.click(openSelectPassengerMenu);

                }

                switch (passengerType) {
                    case ("adults"):
                        while (flag) {

                            actionUtility.waitForElementVisible(selectPassengerMenu,3);
                            passengerNos = adultNos.getAttribute("value");

                            if (Integer.parseInt(passengerNos) < noOfPassenger) {
                                actionUtility.click(adultIncrement);

                            } else if (Integer.parseInt(passengerNos) > noOfPassenger) {
                                actionUtility.click(adultDecrement);
                            } else {
                                flag = false;
                            }
                        }
                        attemptFlag = true;
                        break;

                    case ("teens"):
                        int counter = 0;
                        while (flag) {
                            actionUtility.waitForElementVisible(selectPassengerMenu,3);
                            passengerNos = teenNos.getAttribute("value");

                            if (Integer.parseInt(passengerNos) < noOfPassenger) {
                                actionUtility.click(teenIncrement);
                                counter++;
                                if (counter == 7)
                                    if (adultPassenger.length > 0)
                                        if (adultPassenger[0] == 0)
                                            actionUtility.click(adultDecrement);//Decrement adult when there are 8 teens
                            } else {
                                flag = false;
                            }

                        }
                        attemptFlag = true;
                        break;

                    case ("children"):
                        while (flag) {
                            actionUtility.waitForElementVisible(selectPassengerMenu,3);
                            passengerNos = childrenNos.getAttribute("value");
                            if (Integer.parseInt(passengerNos) < noOfPassenger) {
                                actionUtility.click(childIncrement);
                            } else {
                                flag = false;
                            }
                        }
                        attemptFlag = true;
                        break;

                    case ("infants"):
                        while (flag) {
                            actionUtility.waitForElementVisible(selectPassengerMenu,3);
                            passengerNos = infantNos.getAttribute("value");
                            if (Integer.parseInt(passengerNos) < noOfPassenger) {
                                actionUtility.click(infantIncrement);
                            } else {
                                flag = false;
                            }
                        }
                        attemptFlag = true;
                        break;
                }
            } catch (Exception e) {

                attempt++;
            }
        }
        if (!attemptFlag){
            throw  new RuntimeException("unable to select the passengers");
        }
    }

    public Boolean handleHomeScreenPopUp(HashMap<String,String> testData){
//     Handles the home screen popup/modal
        Boolean popUpHandled = false;
        String popUpMsg = "no cookie modal";
        try{
            try {
                actionUtility.waitForElementVisible(popUp, 2);
                popUpMsg = popUpHeader.getText();
                System.out.println(popUpHeader.getText() + "---"+popUpMsg);
                reportLogger.log(LogStatus.INFO,"home screen cookie modal is displayed");

            }catch (TimeoutException e){
                reportLogger.log(LogStatus.INFO,"home screen cookie modal is not displayed");

            }
            if(popUpMsg.equals("This website uses cookies")){
                int pref = 0;
                String[] preference = {"strictly necessary","analytical and performance","personalization","targeting and marketing"};
                if(testData.get("amendCookie")!=null && testData.get("amendCookie").equalsIgnoreCase("yes")){
                    actionUtility.hardClick(amendCookies);
                    if(testData.get("cookiePreferenceNotNeeded1")!=null) {
                        pref = Arrays.asList(preference).indexOf(testData.get("cookiePreferenceNotNeeded1").toLowerCase());
                        actionUtility.hardClick(cookiePreference.get(pref));
                    }
                    if(testData.get("cookiePreferenceNotNeeded2")!=null){
                        pref = Arrays.asList(preference).indexOf(testData.get("cookiePreferenceNotNeeded2").toLowerCase());
                        actionUtility.hardClick(cookiePreference.get(pref));
                    }
                    if(testData.get("cookiePreferenceNotNeeded3")!=null){
                        pref = Arrays.asList(preference).indexOf(testData.get("cookiePreferenceNotNeeded3").toLowerCase());
                        actionUtility.hardClick(cookiePreference.get(pref));
                    }
                    actionUtility.hardClick(cookiePrefDone);
                }
                else {
                    actionUtility.waitForElementClickable(acceptCookies, 3);
                    actionUtility.hardClick(acceptCookies);
                }
                popUpHandled = true;
                reportLogger.log(LogStatus.INFO,"successfully handled the cookie modal");
            }

            //reportLogger.log(LogStatus.INFO,"successfully handled the cookie modal");
            //popUpHandled = true;
            if(popUpMsg.toLowerCase().equals("no cookie modal")) {
                throw new RuntimeException("Cookie Modal is not displayed");
            }
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to close the home screen cookie modal or cookie modal is not displayed");
            //throw e;
        }
        return popUpHandled;
    }

    public void enterTravelDates(HashMap<String,String> testData){//fixme- changing the month in respective language is pending
        // enter the travel dates

        try {
            String promoCodeVal = null;
            String date[] = new String[3];
            int departDateOffset = Integer.parseInt(testData.get("departDateOffset"));
            date = actionUtility.getDate(departDateOffset);
            WebElement departure = TestManager.getDriver().findElement(By.id("departureDate"));
            selectDateFromCalendar(date,departure);

            reportLogger.log(LogStatus.INFO, "Entered the depart date - "+date[0]+"-"+date[1]+"-"+date[2]);

            if (testData.get("returnDateOffset") != null) {
                int returnDateOffset = Integer.parseInt(testData.get("returnDateOffset"));
                date = actionUtility.getDate(returnDateOffset);
                WebElement returnDate = TestManager.getDriver().findElement(By.id("returnDate"));
                selectDateFromCalendar(date,returnDate);
                reportLogger.log(LogStatus.INFO, "Entered the return date - "+date[0]+"-"+date[1]+"-"+date[2]);
            }


        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable to enter travel Dates");
            throw e;
        }
    }

    private void selectDateFromCalendar(String date[],WebElement datePicker){
        //selects the Date from calendar control
        actionUtility.waitForElementVisible(datePicker,3);
        actionUtility.hardClick(datePicker);
//        String date[] = actionUtility.getDate(offset);
        actionUtility.dropdownSelect(datePickerYear, ActionUtility.SelectionType.SELECTBYTEXT,date[2]);
        actionUtility.dropdownSelect(datePickerMonth, ActionUtility.SelectionType.SELECTBYTEXT,date[1]);
        actionUtility.click(TestManager.getDriver().findElement(By.xpath(String.format(datePickerDay,date[0]))));

    }

    public void findFlights(){
        // click on  the find flights
        try{
            actionUtility.hardClick(findFlights);
            actionUtility.hardSleep(3000);
            actionUtility.waitForPageLoad(20);
            actionUtility.hardSleep(3000);

            reportLogger.log(LogStatus.INFO, "Flight searched");

        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable to search flights");
            throw e;
        }
    }

    public void navigateToLanguageVariant(String lang){
        try{
            String url = TestManager.getDriver().getCurrentUrl();
            actionUtility.hardClick(changeLanguage);
//            actionUtility.waitForElementVisible(langDropdown,30);
            actionUtility.hardClick(TestManager.getDriver().findElement(By.cssSelector(String.format(selectLanguage,lang.toLowerCase()))));
            actionUtility.waitForPageURL(url+lang.toLowerCase(),30);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to language site "+lang);
            throw e;
        }
    }

    //FARE SELECT PAGE
    public HashMap<String,String> selectFlight(HashMap<String,String> testData,String... getFare){
        //selects both departure and return flight - direct,1 stop and 2 stops
        /*Double inboundFare = 0.00;
        Double outboundFare = 0.00;
        */
        HashMap<String,String> fares = new HashMap<>();
        try{
            actionUtility.waitForElementVisible(continueButton,90);
            if(testData.get("departOperatorName")!=null){
                /*outboundFare = */selectOtherOperatorFlight(testData);
            }
            else {
                if (testData.get("departNoOfStops").equals("0")) {
                    /*outboundFare = */selectDirectFlight(testData);
                } else if (testData.get("departNoOfStops").equals("1")) {
                    /*outboundFare = */selectOneStopFlight(testData);
                } else if (testData.get("departNoOfStops").equals("2")) {
                    /*outboundFare = */selectTwoStopFlight(testData);
                }
            }
            if(getFare.length>0){
                fares.put("noOfFlights","0");
                fares = getPriceForEachFareTypeDisplayed("Outbound",fares);
            }
            if(testData.get("returnDateOffset")!=null){
                actionUtility.hardSleep(2000);
                actionUtility.hardClick(continueButton);
                actionUtility.hardSleep(2000);
                if(testData.get("returnOperatorName")!=null){
                    /*inboundFare = */selectOtherOperatorFlight(testData,"inbound");
                }
                else {
                    if (testData.get("returnNoOfStops").equals("0")) {
                        /*inboundFare = */selectDirectFlight(testData,"inbound");
                    } else if (testData.get("returnNoOfStops").equals("1")) {
                        /*inboundFare = */selectOneStopFlight(testData,"inbound");
                    } else if (testData.get("returnNoOfStops").equals("2")) {
                        /*inboundFare = */selectTwoStopFlight(testData,"inbound");
                    }
                }
                if(getFare.length>0){
                    fares = getPriceForEachFareTypeDisplayed("Inbound",fares);
                }
            }
            return fares;
        }catch(Exception e){
            throw e;
        }
        /*return outboundFare+inboundFare;*/
    }

    public HashMap<String,String> retrieveFlightDetails(HashMap<String,String> testData){
        HashMap<String,String> flightDetails = new HashMap<>();
        String expected = null;
        String actual = null;
        String item = null;
        try{
            flightDetails.put("outboundStops",testData.get("departNoOfStops"));
            flightDetails.put("outboundTicketType",testData.get("departTicketType"));
            int outboundNoOfStops = Integer.parseInt(testData.get("departNoOfStops"));
            String[] departureDate = actionUtility.getDate(Integer.parseInt(testData.get("departDateOffset")));
            String[] returnDate = null;
            if(testData.get("returnDateOffset")!=null){
                flightDetails.put("inboundStops",testData.get("returnNoOfStops"));
                flightDetails.put("inboundTicketType",testData.get("returnTicketType"));
                returnDate = actionUtility.getDate(Integer.parseInt(testData.get("returnDateOffset")));
            }
            int stopCounter = 0;
            int flightCounter = 0;
            actionUtility.hardClick(bookingDetails);
            flightDetails.put("outboundSource",journeySource.get(0).getText());
            flightDetails.put("outboundDate",journeyDates.get(0).getText());
            flightDetails.put("outboundDuration",journeyDuration.get(0).getText());
            flightDetails.put("outboundFlightNo1",flightNos.get(flightCounter).getText());
            flightDetails.put("outboundAircraft1",aircraft.get(flightCounter++).getText());
            flightDetails.put("outboundSector1TakeOffTime",journeyTimings.get(0).getText());
            flightDetails.put("outboundSector1BoardingTime",CommonUtility.SubtractMinutesFromGivenTime(CommonUtility.convertStringToTime(flightDetails.get("outboundSector1TakeOffTime")),30)+"");
            flightDetails.put("outboundSector1LandingTime",journeyTimings.get(1).getText());
            int timeCounter = 2;
            if(outboundNoOfStops>0){
                flightDetails.put("outboundNoOfFlights",(outboundNoOfStops+1)+"");
                for(int i=1;i<=outboundNoOfStops;i++) {
                    flightDetails.put("outboundStop"+i,stop.get(stopCounter++).getText());
                    flightDetails.put("outboundFlightNo"+(i+1),flightNos.get(flightCounter).getText());
                    flightDetails.put("outboundAircraft"+(i+1),aircraft.get(flightCounter).getText());
                    flightDetails.put("outboundSector"+(i+1)+"TakeOffTime",journeyTimings.get(timeCounter++).getText());
                    flightDetails.put("outboundSector"+(i+1)+"BoardingTime",CommonUtility.SubtractMinutesFromGivenTime(CommonUtility.convertStringToTime(flightDetails.get("outboundSector"+(i+1)+"TakeOffTime")),30)+"");
                    flightDetails.put("outboundSector"+(i+1)+"LandingTime",journeyTimings.get(flightCounter++).getText());
                }
            }
            flightDetails.put("outboundDestination",journeyDestination.get(flightCounter-1).getText());
            if(testData.get("returnDateOffset")!=null){
                flightDetails.put("inboundSource",journeySource.get(flightCounter).getText());
                flightDetails.put("inboundDestination",journeyDestination.get(journeyDestination.size()-1).getText());
                flightDetails.put("inboundDate",journeyDates.get(1).getText());
                flightDetails.put("inboundDuration",journeyDuration.get(1).getText());
                flightDetails.put("inboundFlightNo1",flightNos.get(flightCounter).getText());
                flightDetails.put("inboundAircraft1",aircraft.get(flightCounter++).getText());
                flightDetails.put("inboundSector1TakeOffTime",journeyTimings.get(timeCounter++).getText());
                flightDetails.put("inboundSector1BoardingTime",CommonUtility.SubtractMinutesFromGivenTime(CommonUtility.convertStringToTime(flightDetails.get("inboundSector1TakeOffTime")),30)+"");
                flightDetails.put("inboundSector1LandingTime",journeyTimings.get(timeCounter++).getText());
                int inboundNoOfStops = Integer.parseInt(testData.get("returnNoOfStops"));
                if(inboundNoOfStops>0){
                    flightDetails.put("inboundNoOfFlights",(inboundNoOfStops+1)+"");
                    for(int i=1;i<=inboundNoOfStops;i++) {
                        flightDetails.put("inboundStop"+i,stop.get(stopCounter++).getText());
                        flightDetails.put("inboundFlightNo"+(i+1),flightNos.get(flightCounter).getText());
                        flightDetails.put("inboundAircraft"+(i+1),aircraft.get(flightCounter++).getText());
                        flightDetails.put("inboundSector"+(i+1)+"TakeOffTime",journeyTimings.get(timeCounter++).getText());
                        flightDetails.put("inboundSector"+(i+1)+"BoardingTime",CommonUtility.SubtractMinutesFromGivenTime(CommonUtility.convertStringToTime(flightDetails.get("inboundSector"+(i+1)+"TakeOffTime")),30)+"");
                        flightDetails.put("inboundSector"+(i+1)+"LandingTime",journeyTimings.get(timeCounter++).getText());
                    }
                }
            }
            try{
                item = "outbound source";
                expected = testData.get("flightFrom");
                actual = flightDetails.get("outboundSource").split("\\(")[1];
                actual = actual.substring(0,actual.length()-1);
                Assert.assertTrue(expected.contains(actual));

                item = "outbound destination";
                expected = testData.get("flightTo");
                actual = flightDetails.get("outboundDestination");
                Assert.assertTrue(actual.contains(expected));

                item = "outbound date";
                expected = departureDate[0]+" "+actionUtility.getMonth(Integer.parseInt(testData.get("departDateOffset")))+" "+departureDate[2];
                actual = flightDetails.get("outboundDate");
                actual = CommonUtility.convertNonEnglishDateToEnglishDate(actual,"EEEE dd MMMM yyyy","dd MMMM yyyy","DE");
                Assert.assertTrue(actual.contains(expected));
                flightDetails.put("outboundDate",actual);


                if(testData.get("returnDateOffset")!=null) {
                    item = "inbound source";
                    expected = testData.get("flightTo");
                    actual = flightDetails.get("inboundSource");
                    Assert.assertTrue(actual.contains(expected));

                    item = "inbound destination";
                    expected = testData.get("flightFrom");
                    actual = flightDetails.get("inboundDestination");
                    Assert.assertTrue(actual.contains(expected));

                    item = "inbound date";
                    expected = returnDate[0]+" "+actionUtility.getMonth(Integer.parseInt(testData.get("returnDateOffset")))+" "+returnDate[2];
                    actual = flightDetails.get("inboundDate");
                    actual = CommonUtility.convertNonEnglishDateToEnglishDate(actual,"EEEE dd MMMM yyyy","dd MMMM yyyy","DE");
                    Assert.assertTrue(actual.contains(expected));
                    flightDetails.put("outboundDate",actual);
                }
                reportLogger.log(LogStatus.INFO,"successfully retrieved itinerary details from booking details section - "+flightDetails);
            }catch(AssertionError ae){
                System.out.println(expected+" ---- "+actual);
                reportLogger.log(LogStatus.INFO,"there is mismatch in "+item+": displayed "+item+" - "+actual+" and expected "+item+" - "+expected);
                throw ae;
            }
            actionUtility.hardClick(bookingDetails);
        }catch(Exception e){
            throw e;
        }
        System.out.println(flightDetails);
        return flightDetails;
    }

    private void selectOtherOperatorFlight(HashMap<String,String> testData,String... inbound){
        //select other operators flights specified in testData and also calculates and returns total fare
        /*Double fareSelected = 0.00;*/
        try{
            if(inbound.length>0){
                if (testData.get("returnNoOfStops").equals("0")) {
                    if (testData.get("returnTicketType").equalsIgnoreCase("just fly")) {
                        List<WebElement> directJustFlyOperator = TestManager.getDriver().findElements(By.xpath(String.format(directOtherOperatorJustFly, testData.get("returnOperatorName"))));
                        actionUtility.click(directJustFlyOperator.get(directJustFlyOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected Just Fly direct " + testData.get("returnOperatorName") + " outbound flight");
                    } else if (testData.get("returnTicketType").equalsIgnoreCase("get more")) {
                        List<WebElement> directGetMoreOperator = TestManager.getDriver().findElements(By.xpath(String.format(directOtherOperatorGetMore, testData.get("returnOperatorName"))));
                        actionUtility.click(directGetMoreOperator.get(directGetMoreOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected Get More direct " + testData.get("returnOperatorName") + " outbound flight");
                    } else if (testData.get("returnTicketType").equalsIgnoreCase("all in")) {
                        List<WebElement> directAllInOperator = TestManager.getDriver().findElements(By.xpath(String.format(directOtherOperatorAllIn, testData.get("returnOperatorName"))));
                        actionUtility.click(directAllInOperator.get(directAllInOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected All In direct " + testData.get("returnOperatorName") + " outbound flight");
                    }
                }
                if (testData.get("returnNoOfStops").equals("1")) {
                    if (testData.get("returnTicketType").equalsIgnoreCase("just fly")) {
                        List<WebElement> oneStopJustFlyOperator = TestManager.getDriver().findElements(By.xpath(String.format(oneStopOtherOperatorJustFly, testData.get("returnOperatorName"))));
                        actionUtility.click(oneStopJustFlyOperator.get(oneStopJustFlyOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected Just Fly 1 stop " + testData.get("returnOperatorName") + " outbound flight");
                    } else if (testData.get("returnTicketType").equalsIgnoreCase("get more")) {
                        List<WebElement> oneStopGetMoreOperator = TestManager.getDriver().findElements(By.xpath(String.format(oneStopOtherOperatorGetMore, testData.get("returnOperatorName"))));
                        actionUtility.click(oneStopGetMoreOperator.get(oneStopGetMoreOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected Get More 1 stop " + testData.get("returnOperatorName") + " outbound flight");
                    } else if (testData.get("returnTicketType").equalsIgnoreCase("all in")) {
                        List<WebElement> oneStopAllInOperator = TestManager.getDriver().findElements(By.xpath(String.format(oneStopOtherOperatorAllIn, testData.get("returnOperatorName"))));
                        actionUtility.click(oneStopAllInOperator.get(oneStopAllInOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected All In 1 stop " + testData.get("returnOperatorName") + " outbound flight");
                    }

                }
                if (testData.get("returnNoOfStops").equals("2")) {
                    if (testData.get("returnTicketType").equalsIgnoreCase("just fly")) {
                        List<WebElement> twoStopJustFlyOperator = TestManager.getDriver().findElements(By.xpath(String.format(twoStopOtherOperatorJustFly, testData.get("returnOperatorName"))));
                        actionUtility.click(twoStopJustFlyOperator.get(twoStopJustFlyOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected Just Fly 2 stop " + testData.get("returnOperatorName") + " outbound flight");
                    } else if (testData.get("returnTicketType").equalsIgnoreCase("get more")) {
                        List<WebElement> twoStopGetMoreOperator = TestManager.getDriver().findElements(By.xpath(String.format(twoStopOtherOperatorGetMore, testData.get("returnOperatorName"))));
                        actionUtility.click(twoStopGetMoreOperator.get(twoStopGetMoreOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected Get More 2 stop " + testData.get("returnOperatorName") + " outbound flight");
                    } else if (testData.get("returnTicketType").equalsIgnoreCase("all in")) {
                        List<WebElement> twoStopAllInOperator = TestManager.getDriver().findElements(By.xpath(String.format(twoStopOtherOperatorAllIn, testData.get("returnOperatorName"))));
                        actionUtility.click(twoStopAllInOperator.get(twoStopAllInOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected All In 2 stop " + testData.get("returnOperatorName") + " outbound flight");
                    }

                }
                //fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(1).getText());
            }else {
                WebElement flightToBeSelected;
                if (testData.get("departNoOfStops").equals("0")) {
                    if (testData.get("departTicketType").equalsIgnoreCase("just fly")) {
                        flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(directOtherOperatorJustFly, testData.get("departOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected just fly direct " + testData.get("departOperatorName") + " outbound flight");
                    } else if (testData.get("departTicketType").equalsIgnoreCase("get more")) {
                        flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(directOtherOperatorGetMore, testData.get("departOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected get more direct " + testData.get("departOperatorName") + " outbound flight");
                    } else if (testData.get("departTicketType").equalsIgnoreCase("all in")) {
                        flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(directOtherOperatorAllIn, testData.get("departOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected all in direct " + testData.get("departOperatorName") + " outbound flight");
                    }
                }
                if (testData.get("departNoOfStops").equals("1")) {
                    if (testData.get("departTicketType").equalsIgnoreCase("just fly")) {
                        flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(oneStopOtherOperatorJustFly, testData.get("departOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected just fly 1 stop " + testData.get("departOperatorName") + " outbound flight");
                    } else if (testData.get("departTicketType").equalsIgnoreCase("get more")) {
                        flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(oneStopOtherOperatorGetMore, testData.get("departOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected get more 1 stop " + testData.get("departOperatorName") + " outbound flight");
                    } else if (testData.get("departTicketType").equalsIgnoreCase("all in")) {
                        flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(oneStopOtherOperatorAllIn, testData.get("departOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected all in 1 stop " + testData.get("departOperatorName") + " outbound flight");
                    }
                }
                if (testData.get("departNoOfStops").equals("2")) {
                    if (testData.get("departTicketType").equalsIgnoreCase("just fly")) {
                        flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(twoStopOtherOperatorJustFly, testData.get("departOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected just fly 2 stop " + testData.get("departOperatorName") + " outbound flight");
                    } else if (testData.get("departTicketType").equalsIgnoreCase("get more")) {
                        flightToBeSelected =TestManager.getDriver().findElements(By.xpath(String.format(twoStopOtherOperatorGetMore, testData.get("departOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected get more 2 stop " + testData.get("departOperatorName") + " outbound flight");
                    } else if (testData.get("departTicketType").equalsIgnoreCase("all in")) {
                        flightToBeSelected = (TestManager.getDriver().findElements(By.xpath(String.format(twoStopOtherOperatorAllIn, testData.get("departOperatorName")))).get(0));
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected all in 2 stop " + testData.get("departOperatorName") + " outbound flight");
                    }
                }
                //fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(0).getText());
            }
            actionUtility.hardSleep(2000);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select other operator flight");
            throw e;
        }
        /*return fareSelected;*/
    }

    private void selectDirectFlight(HashMap<String,String> testData,String... inbound){
        //selects direct flight for ticket type specified in testdata
        //Double fareSelected = 0.00;
        try{
            if(inbound.length>0){
                if (testData.get("returnTicketType").equalsIgnoreCase("just fly")) {
                    actionUtility.hardClick(directJustFly.get(directJustFly.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully selected inbound Just Fly Direct flight");
                } else if (testData.get("returnTicketType").equalsIgnoreCase("get more")) {
                    actionUtility.hardClick(directGetMore.get(directGetMore.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully selected inbound Get More Direct flight");
                } else {
                    actionUtility.hardClick(directAllIn.get(directAllIn.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully selected inbound All In Direct flight");
                }
                //fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(1).getText());
            }else {
                if (testData.get("departTicketType").equalsIgnoreCase("just fly")) {
                    actionUtility.click(directJustFly.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully selected outbound Just Fly Direct flight");
                } else if (testData.get("departTicketType").equalsIgnoreCase("get more")) {
                    actionUtility.click(directGetMore.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully selected outbound Get More Direct flight");
                } else {
                    actionUtility.click(directAllIn.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully selected outbound All In Direct flight");
                }
                //fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(0).getText());
            }
            actionUtility.hardSleep(2000);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select direct flight");
            throw e;
        }
        //return fareSelected;
    }

    private void selectOneStopFlight(HashMap<String,String> testData, String... inbound){
        //selects 1 Stop flight for ticket type specified in testdata
        /*Double fareSelected = 0.0;*/
        try{
            if(inbound.length>0){
                if (testData.get("returnTicketType").equalsIgnoreCase("just fly")) {
                    actionUtility.click(oneStopJustFly.get(oneStopJustFly.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully selected inbound Just Fly 1 stop flight");
                } else if (testData.get("returnTicketType").equalsIgnoreCase("get more")) {
                    actionUtility.click(oneStopGetMore.get(oneStopGetMore.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully selected inbound Get More 1 stop flight");
                } else {
                    actionUtility.click(oneStopAllIn.get(oneStopAllIn.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully selected inbound All In 1 stop flight");
                }
                //fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(1).getText());
            }else {
                if (testData.get("departTicketType").equalsIgnoreCase("just fly")) {
                    actionUtility.click(oneStopJustFly.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully selected outbound Just Fly 1 stop flight");
                } else if (testData.get("departTicketType").equalsIgnoreCase("get more")) {
                    actionUtility.click(oneStopGetMore.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully selected outbound Get More 1 stop flight");
                } else {
                    actionUtility.click(oneStopAllIn.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully selected outbound All In 1 stop flight");
                }
                //fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(0).getText());
            }
            actionUtility.hardSleep(2000);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select 1 stop flight");
            throw e;
        }
        /*return fareSelected;*/
    }

    private void selectTwoStopFlight(HashMap<String,String> testData, String... inbound){
        //selects 2 stops flight for ticket type specified in testdata
        /*Double fareSelected = 0.00;*/
        try{
            if(inbound.length>0){
                if (testData.get("returnTicketType").equalsIgnoreCase("justfly")) {
                    actionUtility.click(twoStopJustFly.get(twoStopJustFly.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully inbound selected Just Fly 2 stops flight");
                } else if (testData.get("returnTicketType").equalsIgnoreCase("getmore")) {
                    actionUtility.click(twoStopGetMore.get(twoStopGetMore.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully inbound selected Get More 2 Stops flight");
                } else {
                    actionUtility.click(twoStopAllIn.get(twoStopAllIn.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully inbound selected All In 2 stops flight");
                }
                //fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(1).getText());
            }
            else {
                if (testData.get("departTicketType").equalsIgnoreCase("justfly")) {
                    actionUtility.click(twoStopJustFly.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully outbound selected Just Fly 2 stops flight");
                } else if (testData.get("departTicketType").equalsIgnoreCase("getmore")) {
                    actionUtility.click(twoStopGetMore.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully outbound selected Get More 2 Stops flight");
                } else {
                    actionUtility.click(twoStopAllIn.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully outbound selected All In 2 stops flight");
                }
                //fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(1).getText());
            }
            actionUtility.hardSleep(2000);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select 2 stops flight");
            throw e;
        }
        /*return fareSelected;*/
    }

    private HashMap<String,String> getPriceForEachFareTypeDisplayed(String way,HashMap<String,String> fares){
        try{
            int noOfFlights= 1;
            if(way.equals("Inbound")){
                noOfFlights = Integer.parseInt(fares.get("noOfOutboundFlights"))+1;
            }
            for(int flightCount=1, i=noOfFlights;i<=justFlyFares.size();i++,flightCount++){
                fares.put("justFly"+way+flightCount,justFlyFares.get(i-1).getText());
            }
            for(int flightCount=1,i=noOfFlights;i<=getMoreFares.size();i++,flightCount++){
                fares.put("getMore"+way+flightCount,getMoreFares.get(i-1).getText());
            }
            for(int flightCount=1,i=noOfFlights;i<=allInFares.size();i++,flightCount++){
                fares.put("allIn"+way+flightCount,allInFares.get(i-1).getText());
                noOfFlights = i;
            }
            fares.put("noOf"+way+"Flights",noOfFlights+"");
            if(way.equals("Inbound")) {
                fares.put("totalFare", getBasketPrice() + "");
            }
            System.out.println(fares);
            return fares;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to get fare for each flight");
            throw e;
        }
    }

    private Double getBasketPrice(){
        Double fare = 0.00;
        try{
            actionUtility.waitForElementVisible(totalAmount,30);
            fare = CommonUtility.convertStringToDouble(totalAmount.getText());
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to retrieve total fare displayed in basket");
            throw e;
        }
        return fare;
    }

    public HashMap<String,Double> navigateToTravellerDetailsAfterHandlingFlexPopup(HashMap<String,String> testData,HashMap<String,Double>... basket){
        //navigates to traveller details page and also select Just Fly Flex if specified in testdata
        //get fare with or without flex
        Double fare = 0.00;
        try{
            if(basket.length>0 && basket[0].get("fare")==null){
                basket[0].put("fare",getBasketPrice());
            }
            actionUtility.hardClick(continueButton);
            actionUtility.hardSleep(2000);
            boolean flexOption = true;
            if(testData.get("flexOptionAvailable")!=null && testData.get("flexOptionAvailable").equalsIgnoreCase("no")){
                flexOption = false;
            }
            if(flexOption) {
                try {
                    if (testData.get("departTicketType").equalsIgnoreCase("just fly")) {
                        if (testData.get("returnTicketType") != null) {
                            if (testData.get("returnTicketType").equalsIgnoreCase("just fly")) {
                                fare = handleFlexAndCalculateFare(testData);
                            }
                        } else {
                            fare = handleFlexAndCalculateFare(testData);
                        }
                    }
                } catch (Exception e) {
                    reportLogger.log(LogStatus.INFO, "unable to select options on flex popup and proceed");
                    throw e;
                }
            }
            reportLogger.log(LogStatus.INFO,"successfully navigated to traveller details page");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to proceed to traveller details page");
            throw e;
        }
        if(basket.length>0) {

            fare += basket[0].get("fare");
            basket[0].put("fare", fare);
            basket[0].put("total",fare);
            return basket[0];
        }
        else{
            return null;
        }
    }

    private Double handleFlexAndCalculateFare(HashMap<String,String> testData){
        Double fare = 0.00;
        if (testData.get("flex") != null && testData.get("flex").equalsIgnoreCase("yes")) {
            fare = CommonUtility.convertStringToDouble(flexPrice.getText());
            actionUtility.click(addFlex);
            reportLogger.log(LogStatus.INFO, "successfully selected just fly flex");
        } else {
            actionUtility.hardSleep(2000);
            actionUtility.click(doNotAddFlex);
            reportLogger.log(LogStatus.INFO, "successfully continued without flex");
        }
        return fare;
    }

    //PASSENGER DETAILS PAGE
    public HashMap<String,String> enterPaxPersonalDetails(HashMap<String,String> testData, String... loggedInUser){
        // to enter passenger name,age
        try {
            actionUtility.waitForElementVisible(continueButton,90);
            if(testData.get("noOfAdult")!=null) {
                int noOfAdults = Integer.valueOf(testData.get("noOfAdult"));
                for (int i = 0; i < noOfAdults; i++) {
                    if(i==0 && loggedInUser.length>0 && loggedInUser[0].equals("loggedIn")){
                        verifyLoggedInUserName(testData,"adult");
                        testData.put("adult1Name",testData.get("title")+" "+testData.get("firstName")+" "+testData.get("lastName"));
                    }
                    else {
                        actionUtility.dropdownSelect(titleADT.get(i), SELECTBYTEXT, testData.get("adult" + (i + 1) + "Name").split(" ")[0]);
                        actionUtility.sendKeys(firstNameADT.get(i), testData.get("adult" + (i + 1) + "Name").split(" ")[1]);
                        actionUtility.sendKeys(lastNameADT.get(i), testData.get("adult" + (i + 1) + "Name").split(" ")[2]);
                    }
                    if(loggedInUser.equals("ads")){
                        if(i==0){
                            verifyADSNumber(testData);
                        }
                        else{
                            actionUtility.sendKeys(adsADT.get(i),testData.get("adult") + (i + 1) + "ADS");
                        }
                    }
                }
            }
            if(testData.get("noOfInfant")!=null) {
                int noOfInfant = Integer.valueOf(testData.get("noOfInfant"));
                for (int i = 0; i < noOfInfant; i++) {
                    actionUtility.sendKeys(firstNameINF.get(i), testData.get("infant" + (i + 1) + "Name").split(" ")[0]);
                    actionUtility.sendKeys(lastNameINF.get(i), testData.get("infant" + (i + 1) + "Name").split(" ")[1]);
                    actionUtility.sendKeys(birthDayINF.get(i), testData.get("infant" + (i + 1) + "DOB").split(" ")[0]);
                    actionUtility.dropdownSelect(birthMonthINF.get(i), SELECTBYTEXT, testData.get("infant" + (i + 1) + "DOB").split(" ")[1]);
                    actionUtility.sendKeys(birthYearINF.get(i), testData.get("infant" + (i + 1) + "DOB").split(" ")[2]);
                    if(loggedInUser.equals("ads")){
                        actionUtility.sendKeys(adsADT.get(i),testData.get("infant") + (i + 1) + "ADS");
                    }
                }
            }
            if(testData.get("noOfChild")!=null) {
                int noOfChild = Integer.valueOf(testData.get("noOfChild"));
                for (int i = 0; i < noOfChild; i++) {
                    actionUtility.dropdownSelect(titleChild.get(i), SELECTBYTEXT, testData.get("child" + (i + 1) + "Name").split(" ")[0]);
                    actionUtility.sendKeys(firstNameChild.get(i), testData.get("child" + (i + 1) + "Name").split(" ")[1]);
                    actionUtility.sendKeys(lastNameChild.get(i), testData.get("child" + (i + 1) + "Name").split(" ")[2]);
                    actionUtility.sendKeys(birthDayChild.get(i), testData.get("child" + (i + 1) + "DOB").split(" ")[0]);
                    actionUtility.dropdownSelect(birthMonthChild.get(i), SELECTBYTEXT, testData.get("child" + (i + 1) + "DOB").split(" ")[1]);
                    actionUtility.sendKeys(birthYearChild.get(i), testData.get("child" + (i + 1) + "DOB").split(" ")[2]);
                    if(loggedInUser.equals("ads")){
                        actionUtility.sendKeys(adsADT.get(i),testData.get("child") + (i + 1) + "ADS");
                    }
                }
            }
            if(testData.get("noOfTeen")!=null) {
                int noOfTeens = Integer.valueOf(testData.get("noOfTeen"));
                for (int i = 0; i < noOfTeens; i++) {
                    if(i==0 && Integer.parseInt(testData.get("noOfAdult"))==0 && loggedInUser.length>0 && loggedInUser[0].equals("loggedIn")){
                        verifyLoggedInUserName(testData,"teen");
                        testData.put("teen1Name",testData.get("title")+" "+testData.get("firstName")+" "+testData.get("lastName"));
                    }
                    actionUtility.dropdownSelect(titleTeen.get(i), SELECTBYTEXT, testData.get("teen" + (i + 1) + "Name").split(" ")[0]);
                    actionUtility.sendKeys(firstNameTeen.get(i), testData.get("teen" + (i + 1) + "Name").split(" ")[1]);
                    actionUtility.sendKeys(lastNameTeen.get(i), testData.get("teen" + (i + 1) + "Name").split(" ")[2]);
                    if(loggedInUser.equals("ads")){
                        actionUtility.sendKeys(adsADT.get(i),testData.get("teen") + (i + 1) + "ADS");
                    }
                }
            }
            reportLogger.log(LogStatus.INFO,"successfully entered traveller details");
            return testData;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to enter traveller details");
            throw e;
        }
    }

    private void verifyLoggedInUserName(HashMap<String,String> testData,String pax){
        String item = null, expected = null, actual = null;
        try {
            if (pax.equals("adult")) {
                item = "title";
                actual = actionUtility.getValueSelectedInDropdown(titleADT.get(0));
                expected = testData.get("title");
                Assert.assertEquals(actual,expected);
                item = "first name";
                actual = firstNameADT.get(0).getAttribute("value");
                expected = testData.get("firstName");
                Assert.assertTrue(actual.equalsIgnoreCase(expected));
                item = "last name";
                actual = lastNameADT.get(0).getAttribute("value");
                expected = testData.get("lastName");
                Assert.assertTrue(actual.equalsIgnoreCase(expected));
            }
            else{
                item = "title";
                actual = actionUtility.getValueSelectedInDropdown(titleTeen.get(0));
                expected = testData.get("title");
                Assert.assertEquals(actual,expected);
                item = "first name";
                actual = firstNameTeen.get(0).getAttribute("value");;
                expected = testData.get("firstName");
                Assert.assertTrue(actual.equalsIgnoreCase(expected));
                item = "last name";
                actual = lastNameTeen.get(0).getAttribute("value");;
                expected = testData.get("lastName");
                Assert.assertTrue(actual.equalsIgnoreCase(expected));
            }
        }catch(AssertionError ae){
            System.out.println(item+" == "+expected+" ---- "+actual);
            reportLogger.log(LogStatus.INFO,"passenger "+item+" on traveller details doesn't match with "+item+" in My Account");
            throw ae;
        }
    }

    private void verifyADSNumber(HashMap<String,String> testData){
        try{
            Assert.assertTrue(adsADT.get(0).getAttribute("value").equals(testData.get("ads_number")));
            reportLogger.log(LogStatus.INFO,"ADS number displayed for adult 1 is as expected - "+testData.get("ads_number"));
        }catch(AssertionError ae){
            reportLogger.log(LogStatus.INFO,"expected ADS number displayed for adult 1 is "+testData.get("ads_number")+" but displayed is "+adsADT.get(0).getAttribute("value"));
            throw ae;
        }
    }

    public void addBookerDetails(HashMap<String,String> testData,Boolean... loggedInUser){
        //selects booker,enter details. Also enters details if the booker is someone else
        //verifies booker contact details in case of logged in user
        HashMap<String,String> contactDetails = null;
        try{
            if(loggedInUser.length>0 && loggedInUser[0]){
                verifyContactInformationIsCorrect(testData);
            }
            else {
                if (testData.get("booker") != null && testData.get("booker").equals("Someone else")) {
                    actionUtility.click(booker.get(booker.size() - 1));
                } else {
                    actionUtility.hardClick(booker.get(0));
                }
                contactDetails = new DataUtility().testData("1");
                populateBookerContactDetails(contactDetails);
                if (testData.get("booker") != null && testData.get("booker").equals("Someone else")) {
                    populatePaxContactDetails(contactDetails);
                }
                reportLogger.log(LogStatus.INFO, "successfully selected booker and entered booker contact details");
            }
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select booker");
            throw e;
        }
    }

    private void verifyContactInformationIsCorrect(HashMap<String,String> testData) {
        String item=null, expected = null,actual = null;
        try {
            actionUtility.click(booker.get(0));
            item = "phone number country code";
            expected = testData.get("countryCode");
            actual = bookerPhoneCode.getAttribute("value");
            Assert.assertTrue(expected.equals(actual));

            item = "phone number";
            expected = testData.get("phoneNumber");
            actual = bookerPhoneNumber.getAttribute("value");
            Assert.assertTrue(expected.equals(actual), "Phone Number in booker section is not matching");

            item = "booker email id";
            HashMap<String,String> contact = new DataUtility().testData("1");
            expected = contact.get("bookerEmail");
            actual = bookerEmail.getAttribute("value");
            Assert.assertTrue(expected.equals(actual));
            //incase of logged in teen is travelling alone
            if(Integer.parseInt(testData.get("noOfAdult"))==0){
                populatePaxContactDetails(contact);
            }
        } catch (AssertionError e) {
            System.out.println(item+" == "+expected+" ---- "+actual);
            reportLogger.log(LogStatus.INFO, item+" in booker contact details section not matching for logged in user, Expected- " + expected + " Displayed - " + actual);
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.INFO, "unable to verify contact details for logged in user");
            throw e;
        }
    }

    private void populateBookerContactDetails(HashMap<String,String> contactDetails){
        actionUtility.sendKeys(bookerPhoneNumber, contactDetails.get("bookerPhoneNumber"));
        actionUtility.click(bookerPhoneCode);
        try {
            actionUtility.click(selectPhoneCode);
        }catch (TimeoutException te){
            actionUtility.scrollUP(selectPhoneCode);
            actionUtility.click(selectPhoneCode);
        }
        actionUtility.sendKeys(bookerEmail, contactDetails.get("bookerEmail"));
        actionUtility.sendKeys(bookerEmailConfirmation, contactDetails.get("bookerEmail"));
    }

    private void populatePaxContactDetails(HashMap<String,String> contactDetails){
        actionUtility.click(paxPhoneCode);
        try {
            actionUtility.click(selectPhoneCode);
        }catch (TimeoutException te){
            actionUtility.scrollUP(selectPhoneCode);
            actionUtility.click(selectPhoneCode);

        }
        actionUtility.sendKeys(paxPhoneNumber, contactDetails.get("paxPhoneNumber"));
        actionUtility.sendKeys(paxEmail, contactDetails.get("paxEmail"));
    }

    public void selectMarketingPrefAndContinue(HashMap<String,String> testData){
        //select marketing preference and continue to seat page
        try{
            actionUtility.click(TestManager.getDriver().findElement(By.xpath(String.format(mktPref,testData.get("marketingPreference")))));
            actionUtility.hardSleep(2000);
            reportLogger.log(LogStatus.INFO,"successfully selected marketing preference and navigated to seat page");
            actionUtility.hardClick(continueButton);
            actionUtility.hardSleep(2000);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to add marketing preference and continue to seat page");
            throw e;
        }
    }

    public HashMap<String,Double> selectBags(HashMap<String,String> testData,HashMap<String,Double>... basket){
        //adds bags for each passenger and calculate cost incurred
        try{
            Double totalBagAmount = 0.00;
            Boolean returnJourney = false;
            Boolean returnDifferentTickeType = true;
            if(testData.get("returnDateOffset")!=null)
            {
                returnJourney = true;
                if(testData.get("departTicketType").equalsIgnoreCase(testData.get("returnTicketType"))){
                    returnDifferentTickeType = false;
                }
            }
            int noOfAdults = Integer.valueOf(testData.get("noOfAdult"));
            int bagCountDepart = 0;
            int bagCountReturn = 1;
            Double cost = 0.00;
            WebElement bag = null;
            for(int i=0;i<noOfAdults;i++){
                if(testData.get("adult"+(i+1)+"BagDepart") != null) {
                    bag = TestManager.getDriver().findElements(By.xpath(String.format(adultBags, testData.get("adult" + (i + 1) + "BagDepart")))).get(bagCountDepart);
                    actionUtility.hardClick(bag);
                    actionUtility.hardSleep(2000);
                    //cost = calculateBagCost("ADT",testData.get("adult" + (i + 1) + "BagDepart"),bagCountDepart,returnJourney,returnDifferentTickeType);

                    if(basket.length>0){
                        basket[0].put("adult"+(i+1)+"DepartBagCost",cost);
                    }
                    if(returnJourney && !returnDifferentTickeType){
                        cost *= 2;
                    }
                    totalBagAmount += cost;
                    reportLogger.log(LogStatus.INFO,"selected bag for departure "+testData.get("adult" + (i + 1) + "BagDepart")+" for "+testData.get("adult" + (i + 1)+"Name"));
                    System.out.println("selected adult bag for departure "+testData.get("adult" + (i + 1) + "BagDepart")+"for "+testData.get("adult" + (i + 1)+"Name")+" for cost "+cost);
                }
                if(returnJourney && returnDifferentTickeType){
                    bagCountDepart += 2;
                }
                else{
                    bagCountDepart += 1;
                }
                if(returnJourney && returnDifferentTickeType){
                    if(testData.get("adult"+(i+1)+"BagReturn")!=null) {
                        bag = TestManager.getDriver().findElements(By.xpath(String.format(adultBags, testData.get("adult" + (i + 1) + "BagReturn")))).get(bagCountReturn);
                        actionUtility.hardClick(bag);
                        actionUtility.hardSleep(2000);
                        //cost = calculateBagCost("ADT",testData.get("adult" + (i + 1) + "BagReturn"),bagCountReturn, returnJourney, returnDifferentTickeType);
                        if(basket.length>0){
                            basket[0].put("adult"+(i+1)+"ReturnBagCost",cost);
                        }
                        if(returnJourney && !returnDifferentTickeType){
                            cost *= 2;
                        }
                        totalBagAmount += cost;
                        reportLogger.log(LogStatus.INFO,"selected bag for return "+testData.get("adult" + (i + 1) + "BagReturn")+" for "+testData.get("adult" + (i + 1)+"Name"));
                        System.out.println("selected adult bag for return "+testData.get("adult" + (i + 1) + "BagReturn")+"for "+testData.get("adult" + (i + 1)+"Name")+" for a cost of "+cost);
                    }
                    bagCountReturn += 2;
                }
            }
            if(testData.get("noOfTeen")!=null) {
                int noOfTeens = Integer.valueOf(testData.get("noOfTeen"));
                bagCountDepart = 0;
                bagCountReturn = 1;
                for (int i = 0; i < noOfTeens; i++) {
                    if (testData.get("teen" + (i + 1) + "BagDepart") != null) {
                        bag = TestManager.getDriver().findElements(By.xpath(String.format(teenBags, testData.get("teen" + (i + 1) + "BagDepart")))).get(bagCountDepart);
                        actionUtility.hardClick(bag);
                        actionUtility.hardSleep(2000);
                        //cost = calculateBagCost("YTH",testData.get("teen" + (i + 1) + "BagDepart"),bagCountDepart,returnJourney,returnDifferentTickeType);
                        if(basket.length>0){
                            basket[0].put("teen"+(i+1)+"DepartBagCost",cost);
                        }
                        if(returnJourney && !returnDifferentTickeType){
                            cost *= 2;
                        }
                        totalBagAmount += cost;
                        reportLogger.log(LogStatus.INFO,"selected bag for departure "+testData.get("teen" + (i + 1) + "BagDepart")+" for "+testData.get("teen" + (i + 1)+"Name"));
                        System.out.println("selected teen bag for departure " + testData.get("teen" + (i + 1) + "BagDepart")+testData.get("teen" + (i + 1)+"Name")+"for a cost "+cost);
                    }
                    if(returnJourney && returnDifferentTickeType){
                        bagCountDepart += 2;
                    }
                    else{
                        bagCountDepart += 1;
                    }
                    if (returnJourney && returnDifferentTickeType ) {
                        if(testData.get("teen" + (i + 1) + "BagReturn") != null) {
                            bag = TestManager.getDriver().findElements(By.xpath(String.format(teenBags, testData.get("teen" + (i + 1) + "BagReturn")))).get(bagCountReturn);
                            actionUtility.hardClick(bag);
                            actionUtility.hardSleep(2000);
                            //cost = calculateBagCost("YTH",testData.get("teen" + (i + 1) + "BagReturn"),bagCountReturn, returnJourney, returnDifferentTickeType);

                            if(basket.length>0){
                                basket[0].put("teen"+(i+1)+"ReturnBagCost",cost);
                            }
                            if(returnJourney && !returnDifferentTickeType){
                                cost *= 2;
                            }
                            totalBagAmount += cost;
                            reportLogger.log(LogStatus.INFO,"selected bag for return "+testData.get("teen" + (i + 1) + "BagReturn")+"for "+testData.get("teen" + (i + 1)+"Name"));
                            System.out.println("selected teen bag for return " + testData.get("teen" + (i + 1) + "BagReturn")+"for "+testData.get("teen" + (i + 1)+"Name")+"for a cost of "+cost);
                        }
                        bagCountReturn += 2;
                    }
                }

            }
            if(testData.get("noOfChild")!=null) {
                int noOfChild = Integer.valueOf(testData.get("noOfChild"));
                bagCountDepart = 0;
                bagCountReturn = 1;
                for (int i = 0; i < noOfChild; i++) {
                    if (testData.get("child" +(i+1)+ "BagDepart") != null) {
                        bag = TestManager.getDriver().findElements(By.xpath(String.format(childBags, testData.get("child" + (i + 1) + "BagDepart")))).get(bagCountDepart);
                        actionUtility.hardClick(bag);
                        actionUtility.hardSleep(2000);
                        //cost = calculateBagCost("CHD",testData.get("child" + (i + 1) + "BagDepart"),bagCountDepart,returnJourney,returnDifferentTickeType);

                        if(basket.length>0){
                            basket[0].put("child"+(i+1)+"DepartBagCost",cost);
                        }
                        if(returnJourney && !returnDifferentTickeType){
                            cost *= 2;
                        }
                        totalBagAmount += cost;
                        reportLogger.log(LogStatus.INFO,"selected bag for departure "+testData.get("child" + (i + 1) + "BagDepart")+" for "+testData.get("child" + (i + 1)+"Name"));
                        System.out.println("selected child bag for departure " + testData.get("child" + (i + 1) + "BagDepart")+"for "+testData.get("child" + (i + 1)+"Name")+"for a cost of "+cost);
                    }
                    if(returnJourney && returnDifferentTickeType){
                        bagCountDepart += 2;
                    }
                    else{
                        bagCountDepart += 1;
                    }
                    if (returnJourney && returnDifferentTickeType) {
                        if(testData.get("child" + (i + 1) + "BagReturn") != null) {
                            bag = TestManager.getDriver().findElements(By.xpath(String.format(childBags, testData.get("child" + (i + 1) + "BagReturn")))).get(bagCountReturn);
                            actionUtility.hardClick(bag);
                            actionUtility.hardSleep(2000);
                            //cost = calculateBagCost("CHD",testData.get("child" + (i + 1) + "BagReturn"),bagCountReturn, returnJourney, returnDifferentTickeType);

                            if(basket.length>0){
                                basket[0].put("child"+(i+1)+"ReturnBagCost",cost);
                            }
                            if(returnJourney && !returnDifferentTickeType){
                                cost *= 2;
                            }
                            totalBagAmount += cost;
                            reportLogger.log(LogStatus.INFO,"selected bag for departure "+testData.get("child" + (i + 1) + "BagReturn")+" for "+testData.get("child" + (i + 1)+"Name"));
                            System.out.println("selected child bag for return " + testData.get("child" + (i + 1) + "BagReturn")+"for "+testData.get("child" + (i + 1)+"Name")+" for a cost of "+cost);
                        }
                        bagCountReturn += 2;
                    }
                    actionUtility.hardSleep(3000);
                }
            }
            reportLogger.log(LogStatus.INFO,"successfully added bags for passenger for a total cost of - "+totalBagAmount);
            if(basket.length>0) {
                basket[0].put("hold baggage",totalBagAmount);
                System.out.println("BASKET AFTER ADDING BAG - "+basket[0]);
                return basket[0];
            }
            return null;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to add bags");
            throw e;
        }
    }

    //SEAT MAP
    public void skipSeatsAndContinueToExtras(HashMap<String,String>... testData){
        //continues without adding seat
        try{
            if(testData.length>0 && testData[0].get("departOperatorName").equalsIgnoreCase("Air France")){
                actionUtility.waitForElementVisible(unavailableSeatMap,90);
            }
            else {
                actionUtility.waitForElementVisible(seatMap, 90);
            }
            actionUtility.hardClick(skipSeat);
            actionUtility.waitForElementVisible(withoutSeatPopup,5);
            actionUtility.waitForElementVisible(continueWithoutSeat,5);
            actionUtility.click(continueWithoutSeat);
            reportLogger.log(LogStatus.INFO,"successfully skipped adding seat and navigated to booking page");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to continue by skipping seat");
            throw e;
        }
    }

    public HashMap<String,Double> selectSeat(HashMap<String,String> testData,HashMap<String,Double>... basket){
        //selects seat for each pax
        //verifies seat charges for just fly,get more, all in - chargeable or free of charge
        //returns basket with seat cost for each pax, total seat price. Also the basket will have paxTypeNo. with selected seat number
        try{
            actionUtility.waitForElementVisible(seatMap,120);
            int totalSectors = new Integer(testData.get("departNoOfStops"))+1;
            int noOfDepartureFlights = totalSectors;
            int noOfReturnFlights = 0;
            if(testData.get("returnDateOffset")!=null){
                totalSectors += new Integer(testData.get("returnNoOfStops"))+1;
                noOfReturnFlights = totalSectors - noOfDepartureFlights;
            }
            int noOfAdult =0;
            if(testData.get("noOfAdult")!=null) {
                noOfAdult = Integer.parseInt(testData.get("noOfAdult"));
            }
            for(int i=1;i<=totalSectors;i++) {
                if(basket.length>0) {
                    basket[0] = selectSeatPerSectorPerPax(testData, noOfAdult, "adult", i, noOfDepartureFlights, noOfReturnFlights, basket[0]);
                }
                else{
                    selectSeatPerSectorPerPax(testData, noOfAdult, "adult", i, noOfDepartureFlights, noOfReturnFlights);
                }
                if(totalSectors>1 && i!=totalSectors) {
                    actionUtility.click(nextFlight);
                    actionUtility.waitForElementVisible(seatMap,30);
                }
            }
            int noOfTeen = 0;
            int noOfChild = 0;
            if(testData.get("noOfTeen")!=null){
                viewFirstSectorSeatMap();
                noOfTeen = Integer.parseInt(testData.get("noOfTeen"));
                for(int i=1;i<=totalSectors;i++) {
                    if(basket.length>0){
                        basket[0] = selectSeatPerSectorPerPax(testData, noOfTeen, "teen", i, noOfDepartureFlights, noOfReturnFlights,basket[0]);
                    }
                    else{
                        selectSeatPerSectorPerPax(testData, noOfTeen, "teen", i, noOfDepartureFlights, noOfReturnFlights);
                    }
                    if(totalSectors>1 && i!=totalSectors) {
                        actionUtility.click(nextFlight);
                        actionUtility.waitForElementVisible(seatMap,30);
                    }
                }
            }
            if(testData.get("noOfChild")!=null){
                viewFirstSectorSeatMap();
                noOfChild = Integer.parseInt(testData.get("noOfChild"));
                for(int i=1;i<=totalSectors;i++) {
                    if(basket.length>0){
                        basket[0] = selectSeatPerSectorPerPax(testData, noOfChild, "child", i, noOfDepartureFlights, noOfReturnFlights,basket[0]);
                    }
                    else{
                        selectSeatPerSectorPerPax(testData, noOfChild, "child", i, noOfDepartureFlights, noOfReturnFlights);
                    }
                    if(totalSectors>1 && i!=totalSectors) {
                        actionUtility.click(nextFlight);
                        actionUtility.waitForElementVisible(seatMap,30);
                    }
                }
            }
            if(basket.length>0){
                System.out.println("BASKET AFTER ADDING SEAT - "+basket[0]);
                return basket[0];
            }
            return null;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select seat");
            throw e;
        }
    }

    private HashMap<String, Double> selectSeatPerSectorPerPax(HashMap<String,String> testData,int noOfPax,String paxType,int sectorNo,int noOfDepartureFlights,int noOfReturnFlights,HashMap<String,Double>... basket){
        String selectedSeat = null;
        Double totalPrice = 0.00;
        Double eachPaxPrice = 0.00;
        WebElement pax = null;
        for(int i=1;i<=noOfPax;i++){
            if(testData.get(paxType+i+"SeatSector"+sectorNo)!=null) {
                pax = TestManager.getDriver().findElement(By.xpath(String.format(paxButton,testData.get(paxType+i+"Name"))));
                actionUtility.click(pax);
                if (testData.get(paxType + i + "SeatSector" + sectorNo).equalsIgnoreCase("XL")) {
                    selectedSeat = extraLegSeat.get(0).getAttribute("id").split("seat")[1];
                    actionUtility.click(extraLegSeat.get(0));
                    actionUtility.hardSleep(1000);
                }
                else if (testData.get(paxType + i + "SeatSector" + sectorNo).equalsIgnoreCase("FOC")) {
                    selectedSeat = focSeat.get(0).getAttribute("id").split("seat")[1];
                    actionUtility.click(focSeat.get(0));
                    actionUtility.hardSleep(1000);
                }
                else {
                    selectedSeat = standardSeat.get(1).getAttribute("id").split("seat")[1];
                    actionUtility.click(standardSeat.get(1));
                    actionUtility.hardSleep(1000);
                }
                reportLogger.log(LogStatus.INFO,"successfully selected seat - "+selectedSeat+" for passenger "+paxType+i+" - "+testData.get(paxType+i+"Name")+" for sector "+sectorNo);
                //eachPaxPrice = calculateSeatCost(testData,testData.get(paxType+i+"SeatSector"+sectorNo),sectorNo,noOfDepartureFlights,noOfReturnFlights,testData.get(paxType+i+"Name"),testData.get("specialAssistance"));
                if(basket.length>0){
                    basket[0].put(paxType+i+"SeatSector"+sectorNo+"Price",eachPaxPrice);
                }
                totalPrice += eachPaxPrice;
            }
        }
        if(basket.length>0){
            if(basket[0].get("your seat")!=null) {
                totalPrice += basket[0].get("your seat");
                basket[0].put("your seat", totalPrice);
            }
            else
                basket[0].put("your seat",totalPrice);
            return  basket[0];
        }
        return null;
    }

    private void viewFirstSectorSeatMap(){
        if(actionUtility.verifyIfElementIsDisplayed(previousFlight))
            do{
                actionUtility.click(previousFlight);
            }while(actionUtility.verfiyIfAnElementISEnabled(previousFlight));
    }

    public void navigateToExtrasAfterSelectingSeat(){
        //navigate to extras page after selecting seat
        try {
            actionUtility.hardClick(continueWithSeat);
            if (actionUtility.verifyIfElementIsDisplayed(continueWithSelectedSeats, 30)) {
                actionUtility.hardClick(continueWithSelectedSeats);
            }
            reportLogger.log(LogStatus.INFO,"successfully navigated to extras page after selecting seat");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to continue to extras page after selecting seat");
            throw e;
        }
    }

    //EXTRAS
    public void continueToPayment(){
        //navigates to payment page
        try{
            actionUtility.scrollToViewElement(continueButton);
            actionUtility.hardClick(continueButton);
            actionUtility.waitForElementVisible(paymentByPayPal,120);
            reportLogger.log(LogStatus.INFO,"successfully navigated to payment page");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to payment page");
            throw e;
        }
    }

    //PAYMENT
    public void enterPaymentDetails(HashMap<String, String> testData,String... blocker) {
        // selects the booking type and enters the card details
        try {
            actionUtility.waitForElementNotPresent(loader,30);
            if (testData.get("paymentType") == null || testData.get("paymentType").toLowerCase().contains("card")) {
                actionUtility.hardClick(TestManager.getDriver().findElement(By.xpath(String.format(cardToBeSelected,testData.get("cardName")))));
                populateCardDetails(testData.get("cardName"));
                populateAddress(testData);
            }
            else if (testData.get("paymentType").toLowerCase().contains("paypal")) {
                actionUtility.click(paymentByPayPal);
                reportLogger.log(LogStatus.INFO, "successfully selected paypal booking option");
            }

        }catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select payment option");
            throw e;
        }
    }

    public void acceptTandCandCompleteBooking(){
        try{
            actionUtility.hardClick(termsAndConditions);
            actionUtility.hardClick(continueButton);
            reportLogger.log(LogStatus.INFO,"successfully checked terms and conditions and navigated to confirmation page");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to check terms and conditions and navigate to confirmation page");
            throw e;
        }
    }

    public void handleInsurancePopup_Complete3DSIdentification(HashMap<String, String> testData,String... otpRequired){
        //selects insurance option based testData provided
        try{
            if (testData.get("insurancePopUp") != null && testData.get("insurancePopUp").equalsIgnoreCase("yes")) {
                actionUtility.click(TestManager.getDriver().findElement(By.xpath(String.format(insurancePopup, "Ja, ich möchte eine Versicherung abschließen"))));
                reportLogger.log(LogStatus.INFO, "successfully selected insurance option - Yes, I want to purchase insurance");
            } else {
                actionUtility.click(TestManager.getDriver().findElement(By.xpath(String.format(insurancePopup, "Nein, ohne Versicherung fortfahren"))));
                reportLogger.log(LogStatus.INFO, "successfully selected insurance option - No, continue without insurance");
            }
            if(otpRequired.length>0 && otpRequired[0].equals("no")){
                return;
            }
            complete3DSecureIdentification(testData);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select insurance option on the popup");
            throw e;
        }
    }

    private void populateCardDetails(String cardName) {
        //depending on the card type fetches data from JSON and populate in the respective field
        JsonObject cardDetails = DataUtility.getJsonData("cardDetails.json");
        actionUtility.sendKeys(cardNumber,cardDetails.getAsJsonObject(cardName.toLowerCase()).get("cardNumber").getAsString());
        String[] expiryDates = cardDetails.getAsJsonObject(cardName.toLowerCase()).get("expiryDate").getAsString().split("/");
        actionUtility.dropdownSelect(expiryMonth, ActionUtility.SelectionType.SELECTBYTEXT, expiryDates[0]);
        actionUtility.dropdownSelect(expiryYear, ActionUtility.SelectionType.SELECTBYTEXT, expiryDates[1]);
        actionUtility.sendKeys(nameOnCard,cardDetails.getAsJsonObject(cardName.toLowerCase()).get("nameOnCard").getAsString());
        if(!cardName.equalsIgnoreCase("UATP/Airplus")) {
            actionUtility.sendKeys(securityNumber, cardDetails.getAsJsonObject(cardName.toLowerCase()).get("securityNumber").getAsString());
        }
        reportLogger.log(LogStatus.INFO, "successfully selected " + cardName + " card type and the card number is " + cardDetails.getAsJsonObject(cardName.toLowerCase()).get("cardNumber").getAsString());
    }

    private void populateAddress(HashMap<String,String> testData){
        //populates address

        actionUtility.dropdownSelect(country, ActionUtility.SelectionType.SELECTBYTEXT, testData.get("country"));
        HashMap<String,String> addressTestData = null;
        addressTestData = new DataUtility().testData("1");
        if(testData.get("country").equalsIgnoreCase("Großbritannien")) {
            postCode.clear();
            actionUtility.sendKeys(postCode,addressTestData.get("postCode").trim());
            /*actionUtility.hardClick(findMyAddress);
            actionUtility.hardClick(address.get(0));
            actionUtility.hardClick(selectAddress);*/
        }
        actionUtility.sendKeys(addressLineOne,addressTestData.get("addressLineOne"));
        actionUtility.sendKeys(city,addressTestData.get("city"));
        if(actionUtility.verifyIfElementIsDisplayed(state)) {
            actionUtility.dropdownSelect(state, ActionUtility.SelectionType.SELECTBYINDEX, "1");
        }
    }

    public void complete3DSecureIdentification(HashMap<String,String> testData){
        try{
            String[] applicableCards = {"Visa","Diners Club","MasterCard","American Express"};
            if(Arrays.asList(applicableCards).contains(testData.get("cardName"))) {
                try {
                    Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(frame_3ds));
                }catch(AssertionError ae){
                    reportLogger.log(LogStatus.INFO,"3DS page is not displayed");
                    throw ae;
                }
                actionUtility.waitAndSwitchToFrame(ActionUtility.FrameLocator.WEBELEMENT, frame_3ds);
                actionUtility.sendKeys(passCode, "123");
                actionUtility.click(authenticate);
                actionUtility.click(backToMerchant);
                actionUtility.switchBackToWindowFromFrame();
                reportLogger.log(LogStatus.INFO,"3D secure identification was successful and navigated to confirmation page");
            }
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to complete 3D secure identification and proceed to confirmation page");
            throw e;
        }
    }

    //CONFIRMATION PAGE
    public String retrievePNR(HashMap<String,String>... testData){
        //retrieves PNR
        String pnr = null;
        try{
            actionUtility.waitForElementVisible(reservationNo,90);
            handleFeedback();
            pnr = reservationNo.getText();
            System.out.println("PNR - "+pnr);
            reportLogger.log(LogStatus.INFO,"Reservation Number generated is "+pnr);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to retrieve PNR or booking is unsuccessful");
            throw e;
        }
        return pnr;
    }

    private void handleFeedback(HashMap<String,String>... testData){
        actionUtility.waitForElementVisible(noThanks,30);
        if(testData.length>0 && testData[0].get("giveFeedback")!=null && testData[0].get("giveFeedback").equalsIgnoreCase("yes")){
            actionUtility.hardClick(giveFeedback);
        }
        else{
            actionUtility.hardClick(noThanks);
        }
        actionUtility.waitForElementNotPresent(feedback,60);
    }

    public String verifyAndGetReservationName(HashMap<String,String> testData){
        //gets the passenger details from conf page
        String passengerName= null,expected=null;
        try{
            actionUtility.waitForElementVisible(ticketInformation,90);
            passengerName = reservationName.getText();
            if(Integer.parseInt(testData.get("noOfAdult"))>0) {
                expected = testData.get("adult1Name").split(" ")[2];
            }
            else{
                expected = testData.get("teen1Name").split(" ")[2];
            }
            Assert.assertTrue(passengerName.equalsIgnoreCase(expected));
            reportLogger.log(LogStatus.INFO,"reservation name is correctly displayed-" +passengerName);
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"expected reservation name is "+expected+" but reservation name displayed is "+passengerName);
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify and capture the reservation name");
            throw e;
        }
        return passengerName;
    }

    public HashMap<String,String> verifyTravellerDetailsAndRetrieveETKT(HashMap<String,String> testData){
        HashMap<String,String> e_tkt = new HashMap<>();
        String expected=null,actual=null,item=null;
        try {
            if (Integer.parseInt(testData.get("noOfAdult")) > 0) {
                int noOfAdult = Integer.parseInt(testData.get("noOfAdult"));
                for (int i=0;i<noOfAdult; i++) {
                    item = "adult"+(i+1)+" name";
                    expected = testData.get("adult" + (i + 1) + "Name").split(" ")[1]+" "+ testData.get("adult" + (i + 1) + "Name").split(" ")[2];
                    actual = TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_name, "ADT"))).get(i).getText();;
                    Assert.assertEquals(actual,expected);
                    e_tkt.put("adult"+(i+1)+"etkt",TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_etkt, "ADT"))).get(i).getText().replace("-",""));
                }
            }
            if (testData.get("noOfTeen")!=null) {
                int noOfTeen = Integer.parseInt(testData.get("noOfTeen"));
                for (int i=0;i<noOfTeen; i++) {
                    item = "teen"+(i+1)+" name";
                    expected = testData.get("teen" + (i + 1) + "Name").split(" ")[1]+" "+ testData.get("teen" + (i + 1) + "Name").split(" ")[2];
                    actual = TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_name, "YTH"))).get(i).getText();;
                    Assert.assertEquals(actual,expected);
                    e_tkt.put("teen"+(i+1)+"etkt",TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_etkt, "YTH"))).get(i).getText().replace("-",""));
                }
            }
            if (testData.get("noOfChild")!=null) {
                int noOfChild = Integer.parseInt(testData.get("noOfChild"));
                for (int i=0;i<noOfChild; i++) {
                    item = "child"+(i+1)+" name";
                    expected = testData.get("child" + (i + 1) + "Name").split(" ")[1]+" "+testData.get("child" + (i + 1) + "Name").split(" ")[2];
                    actual = TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_name, "CHD"))).get(i).getText();;
                    Assert.assertEquals(actual,expected);
                    e_tkt.put("child"+(i+1)+"etkt",TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_etkt, "CHD"))).get(i).getText());
                }
            }
            if (testData.get("noOfInfant")!=null) {
                int noOfInfant = Integer.parseInt(testData.get("noOfInfant"));
                for (int i=0;i<noOfInfant; i++) {
                    item = "infant"+(i+1)+" name";
                    expected = testData.get("infant" + (i + 1) + "Name").split(" ")[0] +" "+ testData.get("infant" + (i + 1) + "Name").split(" ")[1];
                    actual = TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_name, "INF"))).get(i).getText();;
                    Assert.assertEquals(actual,expected);
                    e_tkt.put("infant"+(i+1)+"etkt",TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_etkt, "INF"))).get(i).getText().replace("-",""));
                }
            }
            return e_tkt;
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,item+" - "+expected+" is displayed as "+actual);
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify traveller details section");
            throw e;
        }
    }

    public void enterBookingDetailsForCheckIn(){
        // enter the PNR and passenger name in check-in tab
        try {
            navigateToHomepage();
            navigateToLanguageVariant("DE");
            actionUtility.waitForElementVisible(myBooking,30);
            actionUtility.hardClick(myBooking);
            actionUtility.click(checkinButton);
            reportLogger.log(LogStatus.INFO, "successfully entered the booking details and continued to check-in");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to enter the booking details and continue to check-in");
            throw e;
        }
    }

    public void navigateToHomepage() {
        //navigate to home page
        try {

            String appUrl = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).get("appUrl").getAsString();

            try {
                TestManager.getDriver().navigate().to(appUrl);
            } catch (TimeoutException e) {
            }

            actionUtility.waitForPageLoad(25);
            reportLogger.log(LogStatus.INFO, "successfully navigated to home page");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to navigate to the home page");
            throw e;
        }
    }

    public void retrievePNR(String PNR, String reservationName){
        try{
            bookingReference.sendKeys(PNR);
            lastName.sendKeys(reservationName);
            actionUtility.hardClick(checkin);
            actionUtility.waitForPageLoad(50);
            reportLogger.log(LogStatus.INFO,"Successfully navigated to checkin page");
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"Unable to navigate to checkin page");
            throw e;

        }
    }

    public void continueToJourneyDetailsPage(int noOfPassengers,HashMap<String,String> testData){
        //clicks continue button after selecting passengers in case of multiple passengers and then clicks on read and understood checkbox and clicks on continue button to navigate to Journey Details Page
        try{
            if(actionUtility.verifyIfElementIsDisplayed(passengerList)){
                selectPassengerForCheckin(noOfPassengers,testData);
                actionUtility.click(continueToDangerousGoodsFromPassengerSelection);
                reportLogger.log(LogStatus.INFO,"navigated to dangerous goods page");
            }
            actionUtility.hardClick(readAndUnderstoodCheckBox);
            actionUtility.click(continueToJourneyDetailsButton);
            reportLogger.log(LogStatus.INFO,"able to navigate to Journey Details Page");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"navigated to Journey Details Page");
            throw e;
        }

    }

    public void selectPassengerForCheckin(int noOfPassengers,HashMap<String,String> testData){
        //in case of multiple passengers in a booking, this method selects all the passenger for checkin
        try{
            int totalPassengersToBeSelected = noOfPassengers;
            if(testData.get("noOfInfants")!=null && Integer.parseInt(testData.get("noOfInfants"))>0){
                totalPassengersToBeSelected = noOfPassengers-Integer.parseInt(testData.get("noOfInfants"));
            }

            for(int i=0;i<totalPassengersToBeSelected; i++){
                actionUtility.click(selectPassenger.get(i));
            }
            reportLogger.log(LogStatus.INFO,"selected all passengers for checkin");

        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select passengers for checkin");
            throw e;
        }
    }

    public void navigateToPassengerDetailsPage(){
        try{
            actionUtility.click(continueToPassengerDetailsButton);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to Passenger Details page during checkin");
            throw e;
        }
    }

    public void printBoardingPass(HashMap<String,String> testData,String PNR,HashMap<String,String> flightDetails,HashMap<String,String> paxDetails,String journeyForCheckin,Boolean... vouchers)
    {
        try{
            Boolean voucherTest = false;
            if(vouchers.length>0 && vouchers[0]){
                voucherTest = true;
            }
            actionUtility.click(printBoardingPassSection);
            actionUtility.click(printBoardingPassButton);
            switchToBoardingPass();
            verifyBP(voucherTest,PNR,testData,flightDetails,paxDetails,journeyForCheckin);
            actionUtility.switchBackToMainWindow("Amadeus 6X check-in,Boarding Pass");

        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to print boarding pass");
            throw e;
        }
    }

    private void verifyBP(boolean voucherTest,String PNR,HashMap<String,String> testData,HashMap<String,String>flightDetails,HashMap<String,String>paxDetails,String journey){
        int noOfTeen=0,noOfChild=0,noOfInfant=0;

        int noOfAdult = Integer.parseInt(testData.get("noOfAdult"));
        if(testData.get("noOfTeen")!=null){
            noOfTeen = Integer.parseInt(testData.get("noOfTeen"));
        }
        if(testData.get("noOfChild")!=null){
            noOfChild = Integer.parseInt(testData.get("noOfChild"));
        }
        if(testData.get("noOfInfant")!=null){
            noOfInfant = Integer.parseInt(testData.get("noOfInfant"));
        }

        String paxName = null;
        for (int i = 1; i <= noOfInfant; i++) {
            paxName = testData.get("infant"+i+"Name").split(" ")[1]+" "+testData.get("infant"+i+"Name").split(" ")[0];
            verifyBoardingPassContent(testData,i, PNR, voucherTest,i,flightDetails,paxDetails.get("infant"+i+"etkt"),paxName,journey);
        }

        if(voucherTest){
            for (int i = 1; i < (noOfAdult*2);i=i+2) {//fixme
                paxName = testData.get("adult"+i+"Name").split(" ")[0]+". "+testData.get("adult"+i+"Name").split(" ")[2]+" "+testData.get("adult"+i+"Name").split(" ")[1];
                verifyBoardingPassContent(testData,i, PNR, voucherTest,i+1,flightDetails,paxDetails.get("adult"+i+"etkt"),paxName,journey);
            }
            for (int i = 1; i < (noOfChild*2);i=i+2) {//fixme
                paxName = testData.get("child"+i+"Name").split(" ")[0];
                if(paxName.equalsIgnoreCase("Master")){
                    paxName = "Mstr";
                }
                paxName = paxName+". "+testData.get("child"+i+"Name").split(" ")[2]+" "+testData.get("child"+i+"Name").split(" ")[1];
                verifyBoardingPassContent(testData,i, PNR, voucherTest,i+1,flightDetails,paxDetails.get("child"+i+"etkt"),paxName,journey);
            }
            for (int i = 1; i < (noOfTeen*2);i=i+2) {//fixme
                paxName = testData.get("teen"+i+"Name").split(" ")[0];
                if(paxName.equalsIgnoreCase("Master")){
                    paxName = "Mstr";
                }
                paxName = paxName+". "+testData.get("teen"+i+"Name").split(" ")[2]+" "+testData.get("teen"+i+"Name").split(" ")[1];
                verifyBoardingPassContent(testData,i, PNR, voucherTest,i+1,flightDetails,paxDetails.get("teen"+i+"etkt"),paxName,journey);
            }
        }
        else {
            for (int i = 1,page=noOfInfant+1; i <= noOfAdult; i++,page++) {
                System.out.println("start page - "+page+" for adult - "+i);
                paxName = testData.get("adult"+i+"Name").split(" ")[0]+". "+testData.get("adult"+i+"Name").split(" ")[2]+" "+testData.get("adult"+i+"Name").split(" ")[1];
                verifyBoardingPassContent(testData,page, PNR, voucherTest,page,flightDetails,paxDetails.get("adult"+i+"etkt"),paxName,journey);
            }
            for (int i = 1,page=noOfInfant+noOfAdult+1; i <= noOfChild; i++,page++) {
                paxName = testData.get("child"+i+"Name").split(" ")[0];
                if(paxName.equalsIgnoreCase("Master")){
                    paxName = "Mstr";
                }
                paxName = paxName+". "+testData.get("child"+i+"Name").split(" ")[2]+" "+testData.get("child"+i+"Name").split(" ")[1];
                verifyBoardingPassContent(testData,page, PNR, voucherTest,page,flightDetails,paxDetails.get("child"+i+"etkt"),paxName,journey);
            }
            for (int i = 1,page=noOfInfant+noOfAdult+noOfChild+1; i <= noOfTeen; i++,page++) {
                paxName = testData.get("teen"+i+"Name").split(" ")[0];
                if(paxName.equalsIgnoreCase("Master")){
                    paxName = "Mstr";
                }
                paxName = paxName+". "+testData.get("teen"+i+"Name").split(" ")[2]+" "+testData.get("teen"+i+"Name").split(" ")[1];
                verifyBoardingPassContent(testData,page, PNR, voucherTest,page,flightDetails,paxDetails.get("teen"+i+"etkt"),paxName,journey);
            }
        }
    }

    private void verifyBoardingPassContent(HashMap<String,String> testData,int startPage,String PNR,Boolean voucherTest,int endPage,HashMap<String,String> flightDetails,String etkt,String paxName,String journey) {
        //BP verification
        String assertval = null;
        String boardingPassContent = null;
        try {
            boardingPassContent = PDFReader.extractPDFContent(TestManager.getDriver().getCurrentUrl(), startPage,endPage);
            System.out.println("*************boardingPassContent*****************"+boardingPassContent);
            assertval = "PNR "+PNR;
            Assert.assertTrue(boardingPassContent.contains(PNR), "PNR is not present in boarding pass");

            assertval = "E-Ticket "+etkt;
            Assert.assertTrue(boardingPassContent.contains(etkt), "etkt is not present in boarding pass");

            assertval = "Passenger name "+paxName;
            Assert.assertTrue(boardingPassContent.contains(paxName), "passenger name is not present in boarding pass");

            assertval = journey+" flight number - "+flightDetails.get(journey+"FlightNo1");
            String flightNo =  flightDetails.get(journey+"FlightNo1").split("\\(")[1];
            flightNo = flightNo.split("\\)")[0];
            flightNo = flightNo.split(" ")[0]+flightNo.split(" ")[1];
            Assert.assertTrue(boardingPassContent.contains(flightNo));

            assertval = journey+" source - "+flightDetails.get(journey+"Source");
            Assert.assertTrue((boardingPassContent.contains("Depart "+flightDetails.get(journey+"Source").split("\\(")[0].trim())));

            assertval = journey+" destination - "+flightDetails.get(journey+"Destination");
            Assert.assertTrue((boardingPassContent.contains("Arrive "+flightDetails.get(journey+"Destination").split("\\(")[0].trim())));

            assertval = journey+" boarding time - "+flightDetails.get(journey+"1"+"BoardingTime");
//            Assert.assertTrue((boardingPassContent.contains("Boarding time\n"+flightDetails.get(journey+"1"+"BoardingTime"))));

            assertval = journey+" departure time - "+flightDetails.get(journey+"Sector1TakeOffTime");
            Assert.assertTrue((boardingPassContent.contains("Departs\n"+flightDetails.get(journey+"Sector1TakeOffTime"))));

            assertval = journey+" date - "+flightDetails.get(journey+"Date");//13 Aug 2019
            Assert.assertTrue((boardingPassContent.contains("Date\n"+ CommonUtility.convertDateFormat("E d MMMM yyyy","d MMM yyyy",flightDetails.get(journey+"Date")))));

            if(voucherTest){
                assertval = "Drink voucher";
                Assert.assertTrue(boardingPassContent.contains("Drink"), "drink voucher is not present in boarding pass");
                Assert.assertTrue(boardingPassContent.contains("onboard the aircraft for your drink."), "drink voucher is not present in boarding pass");
                reportLogger.log(LogStatus.INFO,assertval + "is present in boarding pass");


                assertval = "Snack voucher";
                Assert.assertTrue(boardingPassContent.contains("Snack"), "snack voucher is not present in boarding pass");
                Assert.assertTrue(boardingPassContent.contains("onboard the aircraft for your snack."), "snack voucher is not present in boarding pass");
                reportLogger.log(LogStatus.INFO,assertval + "is present in boarding pass");

                if(testData.get("AirportHasLounge").equalsIgnoreCase("yes")) {
                    assertval = "Lounge voucher";
                    Assert.assertTrue(boardingPassContent.contains("Lounge"), "lounge voucher is not present in boarding pass");
                    Assert.assertTrue(boardingPassContent.contains("Visit flybe.com/lounges for more details"), "lounge voucher is not present in boarding pass");
                    reportLogger.log(LogStatus.INFO, assertval + "is present in boarding pass");
                }
                if(testData.get("AirportHasFastTrackSecurity").equalsIgnoreCase("yes")){
                    assertval = "Fast Track Security voucher";
                    Assert.assertTrue(boardingPassContent.contains("Fast track"), "lounge voucher is not present in boarding pass");
                    Assert.assertTrue(boardingPassContent.contains("Please visit fast track security."), "fast track security voucher is not present in boarding pass");
                    reportLogger.log(LogStatus.INFO,assertval + "is present in boarding pass");
                }
            }
            reportLogger.log(LogStatus.INFO,"successfully verified boarding pass for passenger - "+paxName);
        } catch (AssertionError ae) {
            System.out.println(assertval + " is not present in boarding pass");
            reportLogger.log(LogStatus.INFO, assertval + "is not present in boarding pass");
            throw ae;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify boarding pass");
            throw e;
        }
    }

    private void switchToBoardingPass() {
        //verifies the contents of boarding pass
        boolean isSwitchSuccess = false;
        try {
            actionUtility.hardSleep(10);
            isSwitchSuccess = actionUtility.switchWindow("");
            System.out.println(isSwitchSuccess);
            System.out.println("title of BP - -- "+TestManager.getDriver().getTitle());
            if (!isSwitchSuccess) {
                throw new RuntimeException("unable to switch control to boarding pass");
            }
            reportLogger.log(LogStatus.INFO,"successfully switched to boarding pass window");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to if verify boarding pass is displayed");
            throw e;
        }
    }

    public void continueToCompleteCheckin(){
        //navigates to Complete Checkin Page
        try{
            actionUtility.click(finishCheckIn);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to continue to complete checkin page");
            throw e;
        }
    }

    public void verifyJourneySummery(HashMap<String,String> flightDetails,String journey){
        String item=null,expected=null,actual=null;
        try{
            handleFeedback();
            int outboundSectors = Integer.parseInt(flightDetails.get(journey+"Stops"))+1;
            for(int i=1;i<=outboundSectors;i++){
                String takeOffTime = CommonUtility.convert24HourTo12HourFormat(flightDetails.get(journey+"Sector"+i+"TakeOffTime"));
                String landingTime = CommonUtility.convert24HourTo12HourFormat(flightDetails.get(journey+"Sector"+i+"LandingTime"));

                item = journey+" sector "+i+" departure time";
                expected = takeOffTime.substring(1);
                actual = departureTime.getText();
                Assert.assertEquals(actual,expected);

                item = journey+" sector "+i+" arrival time";
                expected = landingTime.substring(1);
                actual = arrivalTime.getText();
                Assert.assertEquals(actual,expected);
            }
            item = journey+" departure airport";
            expected = flightDetails.get(journey+"Source").split(" ")[0];
            actual = sourceAirport.getText();
            Assert.assertTrue(actual.contains(expected.toUpperCase()));

            item = journey+" destination airport";
            expected = flightDetails.get(journey+"Destination").split(" ")[0];
            actual = destinationAirport.getText();
            Assert.assertTrue(actual.contains(expected.toUpperCase()));
            reportLogger.log(LogStatus.INFO,"successfully verified journey summary displayed on checkin complete page");
        }catch(AssertionError e){
            System.out.println(item+" == "+expected+"  -- "+actual);
            reportLogger.log(LogStatus.INFO,"journey summary - "+item+" displayed is incorrect on checkin complete page - expected - "+expected+" displayed - "+actual);
            throw e;
        }
    }

    public void completeCheckin(){
        //clicks on done button
        try{
            actionUtility.click(checkinComplete);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to complete checkin");
            throw e;
        }
    }



}
