package efly.business_logics.online;

import com.google.gson.JsonObject;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.CommonUtility;
import utilities.DataUtility;
import utilities.TestManager;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Sona on 4/5/2019.
 */
public class EFLYConfirmation {
    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public EFLYConfirmation(){
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(css = "span[class='number']")
    private WebElement reservationNo;

    //insurance
    String contractNoPerPax = "//div[div[span[contains(text(),'%s')]]]//a[@class='contract-link']/span";

    @FindBy(css = "a[class='contract-link'] span")
    private WebElement contractNoPerPNR;

    //mcp
    @FindBy(className = "reservation-payment-info-mcp")
    private WebElement mcpPayment;

    @FindBy (css = "div[id='payment-breakdown-area'] li[class='payment-item-no-bullet'] span[class='bold-without-padding']")
    private WebElement gbpPayment;

    @FindBy (css = "div[id='payment-breakdown-area'] li[class='payment-item-no-bullet'] span[class='bold-without-padding']")
    private WebElement baseCurrencyPayment;

    //feedback
    @FindBy(xpath = "//button[text()='No thanks']")
    private WebElement noThanks;

    @FindBy(xpath = "//button[text()='Give feedback']")
    private WebElement giveFeedback;

    String feedback = "//h3[contains(text(),'feedback')]";

    //price section

    @FindBy(xpath = "//li[@class='payment-item-no-bullet']/span[@class='bold-without-padding']")
    private List<WebElement> paymentWithText;

    @FindBy(xpath = "//li[@class='payment-item-no-bullet']//li[@class='payment-sub-item']")
    private List<WebElement> paymentDetails;

    @FindBy(xpath = "//div[span[contains(text(),'TOTAL PAID')]]//span[contains(@class,'amount')]")
    private WebElement totalPaid;

    @FindBy(xpath = "//div[span[contains(text(),'TOTAL PAID')]]//abbr[contains(@class,'amount-currency')]")
    private WebElement totalPaidCurrency;

    @FindBy(css = "div[class*='price-details-traveller']")
    private List<WebElement> travellerTotal;

    @FindBy(css = "span[class*='price-details-taxes-amount-value']")
    private List<WebElement> taxFees;

    @FindBy(css = "i[id*='traveller-taxes']")
    private List<WebElement> travellerTaxBreakdown;

    String taxText = "//div[a[div[span[contains(text(),'%s')]]]]//div[contains(@id,'traveller-taxes')]//span[contains(@class,'text')]";
    String taxValue = "//div[a[div[span[contains(text(),'%s')]]]]//div[contains(@id,'traveller-taxes')]//span[contains(@class,'price-value')]";

    @FindBy(css = "span[class*='taxesbreakdown-total-value']")
    private WebElement totalTaxFee;

    @FindBy(css = "span[class*='price-details-services-amount-value']")
    private WebElement service;

    @FindBy(css = "span[class*='price-details-insurance-amount-value']")
    private WebElement insurance;

    @FindBy(className = "reservation-payment-info-standard")
    private WebElement paymentInfo;

    //itinerary section
    @FindBy(className = "itinerary")
    private List<WebElement> itinerary;

    @FindBy(className = "date")
    List<WebElement> journeyDates;

    @FindBy(className = "total-duration-value")
    List<WebElement> journeyDuration;

    @FindBy(css = "div[class='segment-details-departure '] div div:nth-child(2)")
    List<WebElement> journeySource;

    @FindBy(css = "div[class='segment-details-arrival '] div div:nth-child(2)")
    List<WebElement> journeyDestination;

    @FindBy(className = "stop-number-text")
    List<WebElement> journeySectors;

    @FindBy(className = "ff-name")
    List<WebElement> ticketType;

    @FindBy(xpath = "//div[span[@class='flight-info-airline-name']]")
    List<WebElement> flightNos;

    @FindBy(css = "div[class*='flight-info-aircraft'] div:nth-child(2)")
    List<WebElement> aircraft;

    @FindBy(css = "time[class*='segment-details-time']")
    List<WebElement> journeyTimings;

    @FindBy(xpath = "//div[span[@class='segment-details-stop-location-name']]")
    List<WebElement> stop;

    //service

    @FindBy(css = "ul[class*='servicesbreakdown-segment-list servicesbreakdown-list']")
    List<WebElement> seatForAllSectors;

    String serviceInfoForEachPaxTag = "li";

    String serviceName = "servicesbreakdown-service-name";

    String serviceEMD = "servicesbreakdown-service-emd";

    String serviceCost = "servicesbreakdown-service-price";

    @FindBy(css = "ul[class*='servicesbreakdown-bound-list servicesbreakdown-list']")
    List<WebElement> serviceForBothJourney;

    //other service

    @FindBy(css = "li[class*='servicesbreakdown-other-elem']")
    WebElement otherServices;

    //ticket information

    @FindBy (xpath = "//span[contains(text(),'Ticket Information')]")
    private WebElement ticketInformation;

    @FindBy (css = "li[class='reservation-name'] span:nth-child(2)")
    private WebElement reservationName;

    //modify
    @FindBy(xpath = "//span[contains(text(),'Manage booking')]")
    private WebElement manageBookingButton;

    @FindBy(css = "li[class='modify-passengers-button']")
    private WebElement modifyTravellerButton;

    @FindBy(css = "li[class='modify-services-button']")
    private WebElement modifyTravelExtrasButton;

    //traveller details

    String traveller_name = "div[class*='%s'] div[class*='traveller-name']";

    String traveller_etkt = "div[class*='%s'] div[class='ticket-container'] span";

    //frequent flyer

    @FindBy(className = "traveller-frequent-flyer")
    private WebElement frequentFlyerNumber;

    public String retrievePNR(HashMap<String,String>... testData){
        //retrieves PNR
        String pnr = null;
        try{
            actionUtility.waitForElementVisible(reservationNo,90);
            handleFeedback();
            pnr = reservationNo.getText();
            System.out.println("PNR - "+pnr);
            reportLogger.log(LogStatus.INFO,"Reservation Number generated is "+pnr);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to retrieve PNR or booking is unsuccessful");
            throw e;
        }
        return pnr;
    }

    private void handleFeedback(HashMap<String,String>... testData){
        actionUtility.waitForElementVisible(noThanks,30);
        if(testData.length>0 && testData[0].get("giveFeedback")!=null && testData[0].get("giveFeedback").equalsIgnoreCase("yes")){
            actionUtility.hardClick(giveFeedback);
        }
        else{
            actionUtility.hardClick(noThanks);
        }
        actionUtility.waitForElementNotPresent(feedback,60);
    }

    public HashMap<String,String> verifyTravellerDetailsAndRetrieveETKT(HashMap<String,String> testData){
        HashMap<String,String> e_tkt = new HashMap<>();
        String expected=null,actual=null,item=null;
        try {
            if (Integer.parseInt(testData.get("noOfAdult")) > 0) {
                int noOfAdult = Integer.parseInt(testData.get("noOfAdult"));
                for (int i=0;i<noOfAdult; i++) {
                    item = "adult"+(i+1)+" name";
                    expected = testData.get("adult" + (i + 1) + "Name").split(" ")[1]+" "+ testData.get("adult" + (i + 1) + "Name").split(" ")[2];
                    actual = TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_name, "ADT"))).get(i).getText();;
                    Assert.assertEquals(actual,expected);
                    e_tkt.put("adult"+(i+1)+"etkt",TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_etkt, "ADT"))).get(i).getText().replace("-",""));
                    if(i==0 && testData.get("frequentFlyerNumber")!=null){
                        Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(frequentFlyerNumber));
                        Assert.assertTrue(frequentFlyerNumber.getText().contains(testData.get("frequentFlyerNumber")));
                    }
                }
            }
            if (testData.get("noOfTeen")!=null) {
                int noOfTeen = Integer.parseInt(testData.get("noOfTeen"));
                for (int i=0;i<noOfTeen; i++) {
                    item = "teen"+(i+1)+" name";
                    expected = testData.get("teen" + (i + 1) + "Name").split(" ")[1]+" "+ testData.get("teen" + (i + 1) + "Name").split(" ")[2];
                    actual = TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_name, "YTH"))).get(i).getText();;
                    Assert.assertEquals(actual,expected);
                    e_tkt.put("teen"+(i+1)+"etkt",TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_etkt, "YTH"))).get(i).getText().replace("-",""));
                }
            }
            if (testData.get("noOfChild")!=null) {
                int noOfChild = Integer.parseInt(testData.get("noOfChild"));
                for (int i=0;i<noOfChild; i++) {
                    item = "child"+(i+1)+" name";
                    expected = testData.get("child" + (i + 1) + "Name").split(" ")[1]+" "+testData.get("child" + (i + 1) + "Name").split(" ")[2];
                    actual = TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_name, "CHD"))).get(i).getText();;
                    Assert.assertEquals(actual,expected);
                    e_tkt.put("child"+(i+1)+"etkt",TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_etkt, "CHD"))).get(i).getText());
                }
            }
            if (testData.get("noOfInfant")!=null) {
                int noOfInfant = Integer.parseInt(testData.get("noOfInfant"));
                for (int i=0;i<noOfInfant; i++) {
                    item = "infant"+(i+1)+" name";
                    expected = testData.get("infant" + (i + 1) + "Name").split(" ")[0] +" "+ testData.get("infant" + (i + 1) + "Name").split(" ")[1];
                    actual = TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_name, "INF"))).get(i).getText();;
                    Assert.assertEquals(actual,expected);
                    e_tkt.put("infant"+(i+1)+"etkt",TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_etkt, "INF"))).get(i).getText().replace("-",""));
                }
            }
            return e_tkt;
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,item+" - "+expected+" is displayed as "+actual);
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify traveller details section");
            throw e;
        }
    }

    public void verifyXLSeatIsNotConfirmedForTeen(){
        try{
            Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(seatForAllSectors));
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify if XL seat is confirmed for teen");
            throw e;
        }
    }

    public void navigateToModifyTravellerDetails(){
        try{
            actionUtility.hardClick(manageBookingButton);
            actionUtility.waitForElementVisible(modifyTravellerButton,20);
            actionUtility.click(modifyTravellerButton);
            reportLogger.log(LogStatus.INFO,"successfully navigated to modify traveler section from manage booking page");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to modify traveler section from manage booking page");
            throw e;
        }
    }

    public void verifyPriceSection(HashMap<String,Double> basket,String currency,HashMap<String,String> testData,String... taxVerificationPerPax_VOUCHERVERIFICATION_MCPPAYMENT){
        String item = null;
        Double expected = 0.00;
        Double actual = 0.00;
        String[] pax = {"Adult","Teen","Child","Infant"};
        String total_paid = null;
        try{
            item = "total paid";
            List<WebElement> price = actionUtility.findChildNode(totalPaid);
            if(taxVerificationPerPax_VOUCHERVERIFICATION_MCPPAYMENT.length>0 && taxVerificationPerPax_VOUCHERVERIFICATION_MCPPAYMENT[0].equalsIgnoreCase("mcp payment")){
                Assert.assertTrue(price.size()>0);
            }
            if(price.size()>0){
                actual = CommonUtility.convertStringToDouble(price.get(1).getText());
                expected = basket.get("total");
                Assert.assertEquals(actual,expected);
                total_paid = price.get(0).getText()+" "+price.get(1).getText();
            }
            else{
                actual = CommonUtility.convertStringToDouble(totalPaid.getText());
                expected = basket.get("total");
                Assert.assertEquals(actual,expected);
                total_paid = totalPaidCurrency.getText()+" "+totalPaid.getText();
            }

            item = "tax and fees on lhs";
            expected = basket.get("tax&feesTotal");
            actual = CommonUtility.convertStringToDouble(taxFees.get(0).getText());
            Assert.assertEquals(actual,expected);

            item = "tax and fees on rhs";
            actual = CommonUtility.convertStringToDouble(totalTaxFee.getText());
            Assert.assertEquals(actual,expected);

            int noOfPax = (basket.get("paxType")).intValue();
            for(int i=0;i<noOfPax;i++) {
                item = pax[i]+"_total";
                expected = basket.get(pax[i] + "Total");
                actual = CommonUtility.convertStringToDouble(travellerTotal.get(i).getText());
                Assert.assertEquals(actual,expected);

                item = pax[i]+"Tax&Fees";
                expected = basket.get(pax[i] + "Tax&Fees");
                actual = CommonUtility.convertStringToDouble(taxFees.get(i+1).getText());
                Assert.assertEquals(actual,expected);

                if(taxVerificationPerPax_VOUCHERVERIFICATION_MCPPAYMENT.length>0 && taxVerificationPerPax_VOUCHERVERIFICATION_MCPPAYMENT[0].equalsIgnoreCase("tax verification per pax")) {
                    actionUtility.hardClick(travellerTaxBreakdown.get(i));
                    taxVerificationPerPax(pax[i]);
                    actionUtility.hardClick(travellerTaxBreakdown.get(i));
                }
            }

            if(basket.get("services") != null){
                item = "services";
                expected = basket.get("services");
                actual = CommonUtility.convertStringToDouble(service.getText());
                Assert.assertEquals(actual,expected);
            }

            if(basket.get("travel insurance") != null){
                item = "travel insurance";
                expected = basket.get("travel insurance");
                actual = CommonUtility.convertStringToDouble(insurance.getText());
                Assert.assertEquals(actual,expected);
            }
            //verifyPaymentCurrency(currency,testData);
            if(taxVerificationPerPax_VOUCHERVERIFICATION_MCPPAYMENT.length>0 && taxVerificationPerPax_VOUCHERVERIFICATION_MCPPAYMENT[0].equals("voucher verification")) {
                return;
            }
            if(taxVerificationPerPax_VOUCHERVERIFICATION_MCPPAYMENT.length>0 && taxVerificationPerPax_VOUCHERVERIFICATION_MCPPAYMENT[0].equals("mcp payment")) {
                verifyPaymentMode(testData, total_paid, currency,"mcp payment");
            }
            else {
                verifyPaymentMode(testData, total_paid, currency);
            }
            reportLogger.log(LogStatus.INFO,"successfully verified price section");
        }catch(AssertionError ae){
            System.out.println(item+" == "+expected+" -- "+actual);
            reportLogger.log(LogStatus.INFO,item+" in price section mismatch: expected - "+expected+" displayed - "+actual);
            throw ae;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify the price section");
            throw e;
        }

    }

    public void verifyPriceSectionForVoucherPayment(HashMap<String,Double> basket,HashMap<String,String> testData,String voucherCode,String totalPaidByCard,String currencySymbol,String currency){
        try{
            Double amountPaidByVoucher = basket.get("total")-CommonUtility.convertStringToDouble(totalPaidByCard);
            String paidByVoucher = String.format( "%.2f", amountPaidByVoucher );
            String voucherCurrency = testData.get("voucherCurrency").split(" ")[0];
            verifyVoucherPayment(voucherCode,paidByVoucher,voucherCurrency,testData);
            verifyPaymentMode(testData,currencySymbol+" "+totalPaidByCard,currency,"voucher verification");
            reportLogger.log(LogStatus.INFO,"successfully verified price section for voucher Booking");
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify the price section");
            throw e;
        }
    }

    private void verifyVoucherPayment(String code,String amountPaidByVoucher,String voucherCurrency,HashMap<String,String> testData){
        String expected = null, actual = null;
        int i = 0;
        try {
            if(testData.get("cardName")!=null && testData.get("cardName").equals("MasterCard")){
                i=1;
            }
            actual = paymentWithText.get(i).getText();
            expected = "Payment " + voucherCurrency +" "+ amountPaidByVoucher + " with Voucher(s)";
            Assert.assertTrue(actual.equals(expected));

            actual = paymentDetails.get(i).getText();
            expected = code + " (" + voucherCurrency +" "+ amountPaidByVoucher + ")";
            Assert.assertTrue(actual.equals(expected));

            reportLogger.log(LogStatus.INFO, "voucher amount -" + amountPaidByVoucher + "and voucher currency -" + voucherCurrency + "is displayed correctly");
        }catch (AssertionError e){
            System.out.println(expected+"\n"+actual);
            reportLogger.log(LogStatus.INFO,"voucher amount and voucher is not displayed correctly on the confirmation page");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify the voucher payment text on confirmation page");
            throw e;
        }
    }

    public void verifyPaymentMode(HashMap<String,String> testData,String total_paid,String currency,String... mcp_voucher){
        String expected = null;
        String actual = null;
        String lastDigits = null;
        String paymentCurrency = currency.substring(currency.indexOf("(")+1,currency.indexOf(")"));
        String amountPaidByCard = total_paid.split(" ")[1];
        String cardNumber = null;
        try{
            if(testData.get("paymentType") != null && testData.get("paymentType").equalsIgnoreCase("paypal")) {
                expected = "Payment has been made with PayPal for an amount of "+total_paid;
                actual = paymentInfo.getText().trim();
                Assert.assertEquals(actual, expected);
            }
            else{
                if(!amountPaidByCard.equals("0.00")) {
                    JsonObject cardDetails = DataUtility.getJsonData("cardDetails.json");
                    cardNumber = cardDetails.getAsJsonObject(testData.get("cardName").toLowerCase()).get("cardNumber").getAsString();
                    String card = cardNumber.substring(0, cardNumber.length() - 4);
                    char[] symbols = new char[card.length()];
                    Arrays.fill(symbols, 'X');
                    card = card.replaceAll(card, new String(symbols));
                    lastDigits = cardNumber.substring((cardNumber.length() - 4), cardNumber.length());
                    cardNumber = card + lastDigits;
                    if(total_paid.contains("KR")){
                        total_paid = total_paid.replace("KR","Kr");
                    }
                    expected = "Payment has been made with "+testData.get("cardName")+" "+cardNumber+" for an amount of "+total_paid;
                    actual = paymentInfo.getText().trim();
                    Assert.assertEquals(actual, expected);


                    if(mcp_voucher.length>0 && mcp_voucher[0].equals("mcp payment")){
                        verifyPaymentCurrency(currency,testData,total_paid,0,mcp_voucher[0]);
                    }
                    else if(mcp_voucher.length>0 && mcp_voucher[0].equals("voucher verification")){
                        if(testData.get("cardName")!=null && !testData.get("cardName").equals("MasterCard")) {
                            verifyPaymentCurrency(currency, testData, total_paid, 1);
                        }
                        else{
                            verifyPaymentCurrency(currency, testData, total_paid, 0);
                        }
                    }

                    expected = testData.get("cardName").toUpperCase() + ": XXXX" + lastDigits;
                    int i=0;
                    if(mcp_voucher.length>0 && mcp_voucher[0].equals("voucher verification") && !testData.get("cardName").equals("MasterCard")){
                        i = 1;
                    }
                    actual = paymentDetails.get(i).getText();
                    Assert.assertTrue(actual.equals(expected));
                }


            }

            reportLogger.log(LogStatus.INFO, "payment information displayed is as expected - "+expected);
        }catch(AssertionError ae) {
            System.out.println("payment mode"+expected+" -- "+actual);
            reportLogger.log(LogStatus.INFO, "payment information displayed is incorrect: expected - "+expected+"\n"+"displayed - "+actual);
            throw ae;
        }
    }

    public void verifyInsuranceSection(HashMap<String,String> testData){
        //verifies contract no. generated for insurance for each pax
        try{
            if(testData.get("noOfChild") == null || Integer.parseInt(testData.get("noOfChild")) == 0) {
                actionUtility.verifyIfElementIsDisplayed(contractNoPerPNR);
                reportLogger.log(LogStatus.INFO,"contract number - "+contractNoPerPNR.getText()+" is generated for insurance added");
            }
            else{
                int noOfAdult = Integer.parseInt(testData.get("noOfAdult"));
                for(int i=1;i<noOfAdult;i++){
                    if(testData.get("adult"+i+"Insurance")!=null && testData.get("adult"+i+"Insurance").equalsIgnoreCase("yes")) {
                        WebElement e = TestManager.getDriver().findElement(By.xpath(String.format(contractNoPerPax,testData.get("adult"+i+"Name"))));
                        try{
                            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(e));
                            reportLogger.log(LogStatus.INFO, "contract number - "+e.getText()+" is generated for insurance added");
                        }catch(AssertionError ae) {
                            reportLogger.log(LogStatus.INFO, "contract number is not generated for passenger - adult"+i+" - "+testData.get("adult"+i+"Name"));
                            throw ae;
                        }
                    }
                }
                if(testData.get("noOfTeen")!=null) {
                    int noOfTeen = Integer.parseInt(testData.get("noOfTeen"));
                    for (int i = 1; i < noOfTeen; i++) {
                        if (testData.get("teen" + i + "Insurance") != null && testData.get("teen" + i + "Insurance").equalsIgnoreCase("yes")) {
                            WebElement e = TestManager.getDriver().findElement(By.xpath(String.format(contractNoPerPax, testData.get("teen" + i + "Name"))));
                            try {
                                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(e));
                                reportLogger.log(LogStatus.INFO, "contract number - " + e.getText() + " is generated for insurance added");
                            } catch (AssertionError ae) {
                                reportLogger.log(LogStatus.INFO, "contract number is not generated for passenger - teen" + i + " - " + testData.get("teen" + i + "Name"));
                                throw ae;
                            }
                        }
                    }
                }
            }
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify if contract number is generated for insurance added");
            throw e;
        }
    }

    public void verifyPaymentCurrency(String currencyToBeVerified, HashMap<String,String> testData,String total_paid,int i,String... mcp){
        // verify the currency of payment
        String currency = currencyToBeVerified.substring(currencyToBeVerified.indexOf("(")+1,currencyToBeVerified.indexOf(")"));
        String expected=null,actual=null;
        String paymentStatement_expected = "You have chosen to pay your booking in ";
        try{
            if (testData.get("baseCurrency") != null) {
                actual = paymentWithText.get(i).getText();
                paymentStatement_expected = "Payment "+testData.get("baseCurrency")+" "+CommonUtility.removeCommaFromCurrency(total_paid.split(" ")[1])+" with ";
                if(testData.get("paymentType")!=null && testData.get("paymentType").equalsIgnoreCase("paypal")){
                    expected = paymentStatement_expected+"PayPal";
                }
                else{
                    expected = paymentStatement_expected+"Credit Card";
                }
            }
            else if(mcp.length>0 && mcp[0].equals("mcp payment")){
                expected = mcpPayment.getText();
                actual = paymentStatement_expected+currencyToBeVerified;
            }
            Assert.assertTrue(actual.equals(expected),expected+" -- "+actual);
            reportLogger.log(LogStatus.INFO,"currency of payment - "+currencyToBeVerified+" is displayed correctly");
        }catch(AssertionError e){
            reportLogger.log(LogStatus.WARNING, "currency of payment displayed on confirmation doesn't match with expected currency of payment");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the currency of payment");
            throw e;
        }
    }

    private void taxVerificationPerPax(String pax){
        //verifies that APD is not present for teen,child and infant
        //verifies tax calculation //fixme - should we do this?
        List<WebElement> tax = TestManager.getDriver().findElements(By.xpath(String.format(taxText,pax)));
        List<WebElement> taxAmount = TestManager.getDriver().findElements(By.xpath(String.format(taxValue,pax)));
        for(int i=0;i<tax.size();i++){
            if (pax.equals("Teen") || pax.equals("Child") || pax.equals("Infant")) {
                try {
                    Assert.assertFalse(tax.get(i).getText().contains("Air Passenger Duty"));
                    reportLogger.log(LogStatus.INFO, "successfully verified that APD charges are not applied for "+ pax);
                } catch (AssertionError e) {
                    reportLogger.log(LogStatus.INFO, "APD charges are applied for " + pax);
                    throw e;
                }
            }
        }


    }

    public void verifyItinerarySection(HashMap<String,String> flightDetails){
        //verifies itinerary section
        String item=null, expected=null,actual=null;
        try{
            item = "outbound journey source - destination";
            expected = flightDetails.get("outboundSource").substring(0,flightDetails.get("outboundSource").indexOf('(')).toUpperCase()+"- "+flightDetails.get("outboundDestination").substring(0,flightDetails.get("outboundDestination").indexOf('(')).toUpperCase();
            actual = itinerary.get(0).getText().trim();
            Assert.assertTrue(expected.contains(actual));

            item = "outbound journey date";
            expected = flightDetails.get("outboundDate");
            actual = journeyDates.get(0).getText();
            Assert.assertEquals(actual,expected);

            item = "outbound journey duration";
            expected = flightDetails.get("outboundDuration");
            actual = journeyDuration.get(0).getText();
            Assert.assertEquals(actual,expected);

            item = "outbound departure airport";
            expected = flightDetails.get("outboundSource");
            actual = journeySource.get(0).getText();
            Assert.assertEquals(actual,expected);

            item = "outbound - direct/indirect";
            if(flightDetails.get("outboundStops").equals("0")) {
                expected = "DIRECT FLIGHT";
            }
            if(flightDetails.get("outboundStops").equals("1")) {
                expected = "1 STOP";
            }
            if(flightDetails.get("outboundStops").equals("2")) {
                expected = "2 STOP";
            }
            actual = journeySectors.get(0).getText();
            Assert.assertTrue(actual.contains(expected));

            item = "outbound ticket type";
            expected = flightDetails.get("outboundTicketType").toUpperCase();
            actual = ticketType.get(0).getText();
            Assert.assertTrue(actual.contains(expected));

            int flightCounter = 0;
            item = "outbound flight no. for sector 1";
            expected = flightDetails.get("outboundFlightNo1");
            actual = flightNos.get(flightCounter).getText();
            Assert.assertEquals(actual,expected);

            item = "outbound aircraft for sector 1";
            expected = flightDetails.get("outboundAircraft1");
            actual = aircraft.get(flightCounter++).getText();
            Assert.assertEquals(actual,expected);

            item = "outbound take off time for sector 1";
            expected = flightDetails.get("outboundSector1TakeOffTime");
            actual = journeyTimings.get(0).getText();
            Assert.assertEquals(actual,expected);

            item = "outbound landing time for sector 1";
            expected = flightDetails.get("outboundSector1LandingTime");
            actual = journeyTimings.get(1).getText();
            Assert.assertEquals(actual,expected);

            int stopCounter = 0;
            int timeCounter = 2;
            if(flightDetails.get("outboundNoOfFlights")!=null){
                int outboundNoOfStops = Integer.parseInt(flightDetails.get("outboundNoOfFlights"))-1;
                for(int i=1;i<=outboundNoOfStops;i++) {
                    item = "outbound stop";
                    expected = flightDetails.get("outboundStop"+i);
                    actual = stop.get(stopCounter++).getText();
                    Assert.assertEquals(actual,expected);

                    item = "outbound flight no. for sector "+(i+1);
                    expected = flightDetails.get("outboundFlightNo"+(i+1));
                    actual = flightNos.get(flightCounter).getText();
                    Assert.assertEquals(actual,expected);

                    item = "outbound aircraft for sector "+(i+1);
                    expected = flightDetails.get("outboundAircraft"+(i+1));
                    actual = aircraft.get(flightCounter).getText();
                    Assert.assertEquals(expected,actual);

                    item = "outbound take off time for sector"+(i+1);
                    expected = flightDetails.get("outboundSector"+(i+1)+"TakeOffTime");
                    actual = journeyTimings.get(timeCounter++).getText();
                    Assert.assertEquals(expected,actual);

                    item = "outbound landing time for sector"+(i+1);
                    expected = flightDetails.get("outboundSector"+(i+1)+"LandingTime");
                    actual = journeyTimings.get(flightCounter++).getText();
                    Assert.assertEquals(expected,actual);
                }
            }
            item = "outbound return airport";
            expected = flightDetails.get("outboundDestination");
            actual = journeyDestination.get(flightCounter-1).getText();
            Assert.assertEquals(actual,expected);
            if(flightDetails.get("inboundDate")!=null){
                item = "inbound source - destination";
                expected = flightDetails.get("inboundSource").split(" ")[0].toUpperCase()+" - "+flightDetails.get("inboundDestination").split(" ")[0].toUpperCase();
                actual = itinerary.get(1).getText().trim();
                Assert.assertEquals(actual,expected);

                item = "inbound journey date";
                expected = flightDetails.get("inboundDate");
                actual = journeyDates.get(1).getText();
                Assert.assertEquals(actual,expected);

                item = "inbound journey duration";
                expected = flightDetails.get("inboundDuration");
                actual = journeyDuration.get(1).getText();
                Assert.assertEquals(actual,expected);

                item = "inbound flight no. for sector 1";
                expected = flightDetails.get("inboundFlightNo1");
                actual = flightNos.get(flightCounter).getText();
                Assert.assertEquals(actual,expected);

                item = "inbound aircraft for sector 1";
                expected = flightDetails.get("inboundAircraft1");
                actual = aircraft.get(flightCounter).getText();
                Assert.assertEquals(expected,actual);

                item = "inbound departure airport";
                expected = flightDetails.get("inboundSource");
                actual = journeySource.get(flightCounter++).getText();
                Assert.assertEquals(actual,expected);

                item = "inbound return airport";
                expected = flightDetails.get("inboundDestination");
                actual = journeyDestination.get(journeyDestination.size()-1).getText();
                Assert.assertEquals(actual,expected);

                item = "inbound - direct/indirect";
                if(flightDetails.get("outboundStops").equals("0")) {
                    expected = "DIRECT FLIGHT";
                }
                if(flightDetails.get("outboundStops").equals("1")) {
                    expected = "1 STOP";
                }
                if(flightDetails.get("outboundStops").equals("2")) {
                    expected = "2 STOP";
                }
                actual = journeySectors.get(1).getText();
                Assert.assertTrue(actual.contains(expected));

                item = "inbound ticket type";
                expected = flightDetails.get("inboundTicketType").toUpperCase();
                actual = ticketType.get(1).getText();
                Assert.assertTrue(actual.contains(expected));

                item = "inbound take off time for sector 1";
                expected = flightDetails.get("inboundSector1TakeOffTime");
                actual = journeyTimings.get(timeCounter++).getText();
                Assert.assertEquals(expected,actual);

                item = "inbound landing time for sector 1";
                expected = flightDetails.get("inboundSector1LandingTime");
                actual = journeyTimings.get(timeCounter++).getText();
                Assert.assertEquals(expected,actual);

                if(flightDetails.get("inboundNoOfFlights")!=null){
                    int inboundNoOfStops = Integer.parseInt(flightDetails.get("inboundNoOfFlights"))-1;
                    for(int i=1;i<=inboundNoOfStops;i++) {
                        item = "inbound stop";
                        expected = flightDetails.get("inboundStop"+i);
                        actual = stop.get(stopCounter++).getText();
                        Assert.assertEquals(actual,expected);

                        item = "inbound flight no. for sector "+(i+1);
                        expected = flightDetails.get("inboundFlightNo"+(i+1));
                        actual = flightNos.get(flightCounter).getText();
                        Assert.assertEquals(actual,expected);

                        item = "inbound aircraft for sector "+(i+1);
                        expected = flightDetails.get("inboundAircraft"+(i+1));
                        actual = aircraft.get(flightCounter++).getText();
                        Assert.assertEquals(expected,actual);

                        item = "inbound take off time for sector"+(i+1);
                        expected = flightDetails.get("inboundSector"+(i+1)+"TakeOffTime");
                        actual = journeyTimings.get(timeCounter++).getText();
                        Assert.assertEquals(expected,actual);

                        item = "inbound landing time for sector"+(i+1);
                        expected = flightDetails.get("inboundSector"+(i+1)+"LandingTime");
                        actual = journeyTimings.get(timeCounter++).getText();
                        Assert.assertEquals(expected,actual);
                    }
                }
            }
            reportLogger.log(LogStatus.INFO, "successfully verified itinerary section");
        }catch (AssertionError e) {
            System.out.println(item+" ==== "+expected+"-------"+actual);
            reportLogger.log(LogStatus.INFO, item+" displayed is incorrect: expected - "+expected+" and displayed - "+actual);
            throw e;
        }catch (Exception e) {
            reportLogger.log(LogStatus.INFO, "unable to verify itinerary section");
            throw e;
        }
    }

    public HashMap<String,String> verifySeatAllocationAndRetrieveEMD(HashMap<String,String> testData,HashMap<String,String> seatDetails,HashMap<String,Double> basket){
        //verifies that seat displayed on confirmation is same as that selected on seat/extras page
        //retrieves EMD generated for seat
        HashMap<String,String> seatEMD = new HashMap<>();
        int emdCount = 0;
        try{
            int totalNoOfSectors = Integer.parseInt(testData.get("departNoOfStops"))+1;
            if(testData.get("returnDateOffset")!=null){
                totalNoOfSectors += Integer.parseInt(testData.get("returnNoOfStops"))+1;
            }
            int noOfAdultsWithSeat=0,noOfChildWithSeat=0,noOfTeenWithSeat=0;
            for(int i=1;i<=totalNoOfSectors;i++){
                int countStart = 0;
                if(Integer.parseInt(testData.get("noOfAdult"))>0){
                    noOfAdultsWithSeat = Integer.parseInt(seatDetails.get("noOfadultwithSeatForSector"+i));
                    if(noOfAdultsWithSeat>0) {
                        seatEMD = verifySeatForEachSectorForEachPax("adult", i, seatDetails, countStart, basket, seatEMD,Integer.parseInt(testData.get("noOfAdult")));
                        countStart = noOfAdultsWithSeat;
                    }
                }
                if(testData.get("noOfTeen")!=null && Integer.parseInt(testData.get("noOfTeen"))>0){
                    noOfTeenWithSeat = Integer.parseInt(seatDetails.get("noOfteenwithSeatForSector"+i));
                    if(noOfTeenWithSeat>0) {
                        seatEMD = verifySeatForEachSectorForEachPax("teen", i, seatDetails, countStart, basket, seatEMD,Integer.parseInt(testData.get("noOfTeen")));
                        countStart += noOfTeenWithSeat;
                    }
                }
                if(testData.get("noOfChild")!=null && Integer.parseInt(testData.get("noOfChild"))>0){
                    noOfChildWithSeat = Integer.parseInt(seatDetails.get("noOfchildwithSeatForSector"+i));
                    if(noOfChildWithSeat>0) {
                        seatEMD = verifySeatForEachSectorForEachPax("child", i, seatDetails, countStart, basket, seatEMD,Integer.parseInt(testData.get("noOfChild")));
                        countStart += noOfChildWithSeat;
                    }
                }
                emdCount += countStart;
            }
            Assert.assertTrue(seatEMD.size()>0);
            Assert.assertTrue(seatEMD.size()==emdCount);
            System.out.println("SEAT EMD "+seatEMD);
            reportLogger.log(LogStatus.INFO,"successfully verified seat details on confirmation page :- seat EMD generated for all passenger - "+seatEMD);
            return seatEMD;
        }catch(AssertionError e){
            System.out.println("SEAT EMD size - "+emdCount+" --- "+seatEMD.size());
            reportLogger.log(LogStatus.INFO,"unable to verify seat EMDs generated");
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify seat allocations on confirmation page");
            throw e;
        }
    }

    private HashMap<String,String> verifySeatForEachSectorForEachPax(String paxType,int sectorNo,HashMap<String,String> seatDetails,int countStart,HashMap<String,Double> basket,HashMap<String,String> seatEMD,int noOfPax){
        WebElement sectorSeatInfo = seatForAllSectors.get(sectorNo-1);
        List<WebElement> seatInfoForEachPax = sectorSeatInfo.findElements(By.tagName(serviceInfoForEachPaxTag));
        Object item = null,expected = null,actual = null;
        for(int paxCount=1,i=countStart;paxCount<=noOfPax;i++,paxCount++){
            if(seatDetails.get(paxType+paxCount+"SeatNoForSector"+sectorNo)!=null){
                try {
                    System.out.println("verifying seat "+seatDetails.get(paxType+paxCount+"SeatNoForSector"+sectorNo)+" for pax "+paxType+paxCount+" for sector "+sectorNo);
                    WebElement seatInfoForThisPax = seatInfoForEachPax.get(i);

                    item = paxType+paxCount+" seat number for sector "+sectorNo;
                    expected = seatDetails.get(paxType + paxCount + "SeatNoForSector" + sectorNo);
                    actual = seatInfoForThisPax.findElement(By.className(serviceName)).getText().trim();
                    System.out.println(expected+" == "+actual);
                    Assert.assertTrue(expected.equals(actual));

                    item = paxType+paxCount+" seat price for sector "+sectorNo;
                    expected = basket.get(paxType + paxCount + "SeatSector" + sectorNo+"Price");
                    if(expected.equals(0.0)){
                        expected = "Free of charge";
                        actual = seatInfoForThisPax.findElement(By.className(serviceCost)).getText().trim();
                    }
                    else {
                        actual = CommonUtility.convertStringToDouble(seatInfoForThisPax.findElement(By.className(serviceCost)).getText().trim());
                    }
                    System.out.println(expected+" == "+actual);
                    Assert.assertTrue(expected.equals(actual));

                    if(expected.equals("Free of charge")) {
                        Assert.assertFalse(actionUtility.verifyIfDependentElementIsDisplayed(seatInfoForThisPax,serviceEMD,"className"));
                        seatEMD.put(paxType + paxCount +"SeatEMDForSector"+ sectorNo,"nil");
                    }else {
                        seatEMD.put(paxType + paxCount+"SeatEMDForSector" + sectorNo, seatInfoForThisPax.findElement(By.className(serviceEMD)).getText().trim());
                        try{
                            Assert.assertTrue(seatEMD.get(paxType + paxCount+"SeatEMDForSector" + sectorNo)!=null);
                            reportLogger.log(LogStatus.INFO,"successfully captured EMD generated - "+seatEMD.get(paxType + paxCount+"SeatEMDForSector"+ sectorNo)+" for passenger "+paxType+paxCount);
                        }catch(AssertionError ae){
                            reportLogger.log(LogStatus.INFO,"EMD is not generated for seat added for "+paxType + paxCount+" for sector "+sectorNo);
                            throw ae;
                        }
                    }
                    reportLogger.log(LogStatus.INFO,"successfully verified "+paxType+paxCount+" seat number displayed - "+seatDetails.get(paxType + paxCount + "SeatNoForSector" + sectorNo));
                }catch(AssertionError ae){
                    System.out.println(item+" == "+expected+" --- "+actual);
                    reportLogger.log(LogStatus.INFO,item+" expected - "+expected+" but displayed seat number is "+actual);
                    throw ae;
                }
            }
        }
        return seatEMD;
    }

    private WebElement findList(HashMap<String,Double> basket,int journey,String service){
        int i=0;
        if(journey==1) {
            if (service.equals("SpecialAssistance"))
                return serviceForBothJourney.get(0);

            if (service.equals("Bag")) {
                if(basket.get("special assistance")!=null)
                    return serviceForBothJourney.get(1);
                else
                    return serviceForBothJourney.get(0);
            }

            if (service.equals("Golf")){
                if(basket.get("special assistance")!=null && basket.get("hold baggage")!=null){
                    return serviceForBothJourney.get(2);
                }
                else if(basket.get("special assistance")==null && basket.get("hold baggage")==null){
                    return serviceForBothJourney.get(0);
                }
                else if(basket.get("special assistance")==null || basket.get("hold baggage")==null){
                    return serviceForBothJourney.get(1);
                }
            }

            if(service.equals("SkiSnow")) {
                if (basket.get("special assistance") == null && basket.get("golf clubs") == null && basket.get("hold baggage") == null) {
                    return serviceForBothJourney.get(0);
                }
                if(basket.get("hold baggage")==null){
                    if(basket.get("special assistance")!=null && basket.get("golf clubs")!=null){
                        return serviceForBothJourney.get(2);
                    }
                    else if(basket.get("special assistance")==null || basket.get("golf clubs")==null){
                        return serviceForBothJourney.get(1);
                    }
                }
                if(basket.get("special assistance")==null){
                    if(basket.get("hold baggage")!=null && basket.get("golf clubs")!=null){
                        return serviceForBothJourney.get(2);
                    }
                    else if(basket.get("hold baggage")==null || basket.get("golf clubs")==null){
                        return serviceForBothJourney.get(1);
                    }
                }

                if(basket.get("golf clubs")==null){
                    if(basket.get("hold baggage")!=null && basket.get("special assistance")!=null){
                        return serviceForBothJourney.get(2);
                    }
                    else if(basket.get("hold baggage")==null || basket.get("special assistance")==null){
                        return serviceForBothJourney.get(1);
                    }
                }
            }
        }
        if(journey==2){
            if(service.equals("SpecialAssistance")){
                if(basket.get("golf clubs")!=null && basket.get("skis")!=null && basket.get("hold baggage")!=null){
                    return serviceForBothJourney.get(4);
                }
                if(basket.get("hold baggage")==null) {
                    if (basket.get("golf clubs") != null && basket.get("skis") != null) {
                        return serviceForBothJourney.get(3);
                    } else if (basket.get("golf clubs") == null && basket.get("skis") == null) {
                        return serviceForBothJourney.get(1);
                    } else if (basket.get("golf clubs") == null || basket.get("skis") == null) {
                        return serviceForBothJourney.get(2);
                    }
                }
            }
            if (service.equals("Golf")){
                if(basket.get("golf clubs")!=null && basket.get("skis")!=null && basket.get("special assistance")!=null){
                    return serviceForBothJourney.get(6);
                }
                if(basket.get("special assistance")==null) {
                    if (basket.get("skis") != null && basket.get("hold baggage") != null) {
                        return serviceForBothJourney.get(4);
                    } else if (basket.get("golf clubs") == null && basket.get("hold baggage") == null) {
                        return serviceForBothJourney.get(1);
                    } else if (basket.get("golf clubs") == null || basket.get("skis") == null) {
                        return serviceForBothJourney.get(2);
                    }
                }
            }
            if (service.equals("SkiSnow")){
                if(basket.get("golf clubs")!=null && basket.get("skis")!=null && basket.get("special assistance")!=null){
                    return serviceForBothJourney.get(7);
                }
                if(basket.get("special assistance")==null) {

                    if (basket.get("golf clubs") != null && basket.get("hold baggage") != null) {
                        return serviceForBothJourney.get(5);
                    } else if (basket.get("golf clubs") == null && basket.get("hold baggage") == null) {
                        return serviceForBothJourney.get(1);
                    } else if (basket.get("golf clubs") == null || basket.get("skis") == null) {
                        return serviceForBothJourney.get(3);
                    }
                }
            }
            if (service.equals("Bag")){
                if(basket.get("golf clubs")!=null && basket.get("skis")!=null && basket.get("special assistance")!=null){
                    return serviceForBothJourney.get(5);
                }
                if(basket.get("special assistance")==null) {
                    if (basket.get("skis") != null && basket.get("golf clubs") != null) {
                        return serviceForBothJourney.get(3);
                    } else if (basket.get("golf clubs") == null && basket.get("skis") == null) {
                        return serviceForBothJourney.get(1);
                    } else if (basket.get("golf clubs") == null || basket.get("skis") == null) {
                        return serviceForBothJourney.get(2);
                    }
                }
                if(basket.get("skis")==null) {
                    if (basket.get("special assistance") != null && basket.get("golf clubs") != null) {
                        return serviceForBothJourney.get(4);
                    } else if (basket.get("golf clubs") == null && basket.get("special assistance") == null) {
                        return serviceForBothJourney.get(1);
                    } else if (basket.get("golf clubs") == null) {
                        return serviceForBothJourney.get(3);
                    }
                }
                if(basket.get("golf clubs")==null) {
                    if (basket.get("special assistance") != null && basket.get("skis") != null)
                        return serviceForBothJourney.get(4);
                }
            }
        }
        return null;
    }


    private HashMap<String,String> verifyBag_Ski_Golf_ForEachJourneyForEachPax(HashMap<String,Double> basket,String paxType,int journeyNo,HashMap<String,String> testData,int countStart,int noOfPax,String service,HashMap<String,String>... emd){
        WebElement perJourneyServiceInfo = null;
        perJourneyServiceInfo = findList(basket,journeyNo,service);
        List<WebElement> serviceInfoForEachPax = perJourneyServiceInfo.findElements(By.tagName(serviceInfoForEachPaxTag));
        Object item = null,expected = null,actual = null;
        String journey = "";
        if(service.equals("Bag")){
            journey = findJourney(journeyNo,testData);
        }
        for(int paxCount=1,i=countStart;paxCount<=noOfPax;i++,paxCount++){
            if(testData.get(paxType+paxCount+service+journey)!=null){
                try {
                    WebElement serviceInfoForThisPax = serviceInfoForEachPax.get(i);
                    item = paxType+paxCount+" "+service.toLowerCase()+" for journey "+journeyNo;
                    if(service.equals("Bag")){
                        expected = getExpectedServiceMessage(testData.get(paxType+paxCount+service+journey),testData.get(journey.toLowerCase()+"TicketType"));
                    }
                    else {
                        expected = getExpectedServiceMessage(testData.get(paxType + paxCount + service + journey));
                    }
                    actual = serviceInfoForThisPax.findElement(By.className(serviceName)).getText().trim();
                    System.out.println(expected+" == "+actual);
                    Assert.assertTrue(expected.equals(actual));

                    item = paxType+paxCount+" "+service.toLowerCase()+" price for journey "+journeyNo;
                    actual = CommonUtility.convertStringToDouble(serviceInfoForThisPax.findElement(By.className(serviceCost)).getText().trim());
                    if(service.equals("Bag")) {
                        expected = basket.get(paxType + paxCount + journey + "BagCost");
                    }
                    else if(service.equals("Golf")|| service.equals("SkiSnow")){
                        expected = 30.00; //price for ski and golf
                    }
                    else if(service.equals("SpecialAssistance")){
                        expected = "Free of charge";
                        actual = serviceInfoForThisPax.findElement(By.className(serviceCost)).getText().trim();
                    }
                    System.out.println(expected+" == "+actual);
                    Assert.assertTrue(expected.equals(actual));

                    if(emd.length>0) {
                        emd[0].put(paxType + paxCount + service + "sector" + journeyNo, serviceInfoForThisPax.findElement(By.className(serviceEMD)).getText().trim());
                        try {
                            Assert.assertTrue(emd[0].get(paxType + paxCount + service + "sector" + journeyNo) != null);
                            if (service.equals("Bag")) {
                                reportLogger.log(LogStatus.INFO, "successfully captured EMD - " + emd[0].get(paxType + paxCount + service + "sector" + journeyNo) + " generated for " + service + " added in  booking flow for pax - " + paxType + paxCount + " for journey " + journey);
                            } else {
                                reportLogger.log(LogStatus.INFO, "successfully captured EMD - " + emd[0].get(paxType + paxCount + service + "sector" + journeyNo) + " generated for " + service + " added in  booking flow for pax - " + paxType + paxCount);
                            }
                        } catch (AssertionError ae) {
                            reportLogger.log(LogStatus.INFO, "EMD is not generated for " + service + " added for " + paxType + paxCount + " for sector " + journeyNo);
                            throw ae;
                        }
                    }
                    reportLogger.log(LogStatus.INFO,"successfully verified "+paxType+paxCount+" "+service+" details displayed - "+testData.get(paxType + paxCount + service + journey)+"for a cost of  - "+serviceInfoForThisPax.findElement(By.className(serviceCost)).getText().trim());
                }catch(AssertionError ae){
                    System.out.println(item+" == "+expected+" --- "+actual);
                    reportLogger.log(LogStatus.INFO,item+" expected - "+expected+" but displayed is "+actual);
                    throw ae;
                }
            }
        }
        if(emd.length>0) {
            return emd[0];
        }
        return null;
    }

    public HashMap<String,String> verifyGolf_SkiAndRetrieveEMD(HashMap<String,String> testData, HashMap<String,Double> basket,String service){
        HashMap<String,String> emd = new HashMap<>();
        int  emdCount =0;
        try{
            if(service.equals("Ski")){
                service = "SkiSnow";
            }
            int noOfJourney = getNoOfJourney(testData);
            int noOfAdultsWithService =0, noOfChildWithService=0,noOfTeenWithService=0;
            if(Integer.parseInt(testData.get("noOfAdult"))>0) {
                noOfAdultsWithService = getNumberOfPaxWithBag_Golf_Ski(testData, "Adult", service);
            }
            if(testData.get("noOfChild")!=null && Integer.parseInt(testData.get("noOfChild"))>0) {
                noOfChildWithService = getNumberOfPaxWithBag_Golf_Ski(testData, "Child", service);
            }
            if(testData.get("noOfTeen")!=null && Integer.parseInt(testData.get("noOfTeen"))>0) {
                noOfTeenWithService = getNumberOfPaxWithBag_Golf_Ski(testData, "Teen", service);
            }
            System.out.println("NO. Of JOURNEYs =  "+noOfJourney);
            for(int i=1;i<=noOfJourney;i++){
                System.out.println("JOURNEYTYPE "+i);
                int countStart = 0;
                if(noOfAdultsWithService>0) {
                    emd = verifyBag_Ski_Golf_ForEachJourneyForEachPax(basket,"adult", i, testData, countStart,Integer.parseInt(testData.get("noOfAdult")),service,emd);
                    countStart = noOfAdultsWithService;
                }
                if(noOfChildWithService>0) {
                    emd = verifyBag_Ski_Golf_ForEachJourneyForEachPax(basket,"child", i, testData, countStart,Integer.parseInt(testData.get("noOfChild")),service,emd);
                    countStart += noOfChildWithService;
                }
                if (noOfTeenWithService > 0) {
                    emd = verifyBag_Ski_Golf_ForEachJourneyForEachPax(basket, "teen", i, testData, countStart, Integer.parseInt(testData.get("noOfTeen")),service,emd);
                    countStart += noOfTeenWithService;
                }
                emdCount += countStart;
            }
            Assert.assertTrue(emd.size()>0);
            Assert.assertTrue(emd.size()==emdCount);
            System.out.println(service.toUpperCase()+" EMD "+emd);
            reportLogger.log(LogStatus.INFO,"successfully verified "+service+" details on confirmation page :- "+service+" EMD generated for all passenger - "+emd);
            return emd;
        }catch(AssertionError e){
            System.out.println(service.toUpperCase()+" EMD size - "+emdCount+" --- "+emd.size());
            reportLogger.log(LogStatus.INFO,"unable to retrieve EMD generated for  service - "+service);
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify service section - "+ service);
            throw e;
        }
    }

    public void verifySpecialAssistance(HashMap<String,String> testData,HashMap<String,Double> basket){
        try{
            int noOfJourney = getNoOfJourney(testData);
            int noOfAdultsWithSplAssistance =0, noOfChildWithSplAssistance =0, noOfTeenWithSplAssistance =0;
            for(int i=1;i<=noOfJourney;i++){
                int countStart = 0;
                if(Integer.parseInt(testData.get("noOfAdult"))>0){
                    noOfAdultsWithSplAssistance = getNumberOfPaxWithBag_Golf_Ski(testData,"Adult","SpecialAssistance");
                    System.out.println("no. of adults with special assistance "+noOfAdultsWithSplAssistance);
                    if(noOfAdultsWithSplAssistance >0) {
                        verifyBag_Ski_Golf_ForEachJourneyForEachPax(basket,"adult", i, testData, countStart,Integer.parseInt(testData.get("noOfAdult")),"SpecialAssistance");
                        countStart = noOfAdultsWithSplAssistance;
                    }
                }
                if(testData.get("noOfChild")!=null && Integer.parseInt(testData.get("noOfChild"))>0){
                    noOfChildWithSplAssistance = getNumberOfPaxWithBag_Golf_Ski(testData,"Child","SpecialAssistance");
                    if(noOfChildWithSplAssistance >0) {
                        verifyBag_Ski_Golf_ForEachJourneyForEachPax(basket,"child", i, testData, countStart,Integer.parseInt(testData.get("noOfChild")),"SpecialAssistance");
                        countStart += noOfChildWithSplAssistance;
                    }
                }
                if(testData.get("noOfTeen")!=null && Integer.parseInt(testData.get("noOfTeen"))>0){
                    noOfTeenWithSplAssistance = getNumberOfPaxWithBag_Golf_Ski(testData,"Teen","SpecialAssistance");
                    if(noOfTeenWithSplAssistance >0) {
                        verifyBag_Ski_Golf_ForEachJourneyForEachPax(basket,"teen", i, testData, countStart,Integer.parseInt(testData.get("noOfTeen")),"SpecialAssistance");
                    }
                }
            }
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify service - bag section");
            throw e;
        }
    }

    public HashMap<String,String> verifyBagsAndRetrieveEMD(HashMap<String,String> testData,HashMap<String,Double> basket){
        HashMap<String,String> bagEMD = new HashMap<>();
        int emdCount=0;
        try{
            int noOfJourney = getNoOfJourney(testData);
            int noOfAdultsWithBag=0,noOfChildWithBag=0,noOfTeenWithBag=0;
            for(int i=1;i<=noOfJourney;i++){
                int countStart = 0;
                if(Integer.parseInt(testData.get("noOfAdult"))>0){
                    noOfAdultsWithBag = getNumberOfPaxWithBag_Golf_Ski(testData,"Adult","Bag",i);
                    if(noOfAdultsWithBag>0) {
                        bagEMD = verifyBag_Ski_Golf_ForEachJourneyForEachPax(basket,"adult", i, testData, countStart,Integer.parseInt(testData.get("noOfAdult")),"Bag", bagEMD);
                        countStart = noOfAdultsWithBag;
                    }
                }
                if(testData.get("noOfTeen")!=null && Integer.parseInt(testData.get("noOfTeen"))>0){
                    noOfTeenWithBag = getNumberOfPaxWithBag_Golf_Ski(testData,"Teen","Bag",i);
                    if(noOfTeenWithBag>0) {
                        bagEMD = verifyBag_Ski_Golf_ForEachJourneyForEachPax(basket,"teen", i, testData, countStart, Integer.parseInt(testData.get("noOfTeen")),"Bag", bagEMD);
                        countStart += noOfTeenWithBag;
                    }
                }
                if(testData.get("noOfChild")!=null && Integer.parseInt(testData.get("noOfChild"))>0){
                    noOfChildWithBag = getNumberOfPaxWithBag_Golf_Ski(testData,"Child","Bag",i);
                    if(noOfChildWithBag>0) {
                        bagEMD = verifyBag_Ski_Golf_ForEachJourneyForEachPax(basket,"child", i, testData, countStart,Integer.parseInt(testData.get("noOfChild")),"Bag", bagEMD);
                        countStart += noOfChildWithBag;
                    }
                }
                System.out.println("journey "+i+" BAg EMD "+bagEMD);
                emdCount += countStart;
                System.out.println("journey "+i+" BAg EMD count "+emdCount);
            }
            Assert.assertTrue(bagEMD.size()>0);
            Assert.assertTrue(bagEMD.size()==emdCount);
            System.out.println("BAG EMD "+bagEMD);
            reportLogger.log(LogStatus.INFO,"successfully verified bag details on confirmation page :- bag EMD generated for all passenger"+bagEMD);
            return bagEMD;
        }catch(AssertionError e){
            System.out.println("BAG EMD size - "+emdCount+" --- "+bagEMD.size());
            reportLogger.log(LogStatus.INFO,"unable to retrieve EMD generated for bag");
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify service - bag section");
            throw e;
        }
    }

    private int getNoOfJourney(HashMap<String,String> testData){
        int noOfJourney = 1;
        if(testData.get("returnDateOffset")!=null){
            noOfJourney = 2;
        }
        return noOfJourney;
    }

    private String findJourney(int journeyNo,HashMap<String,String> testData){
        String journey = "Depart";
        if(journeyNo==2){
            /*if(!testData.get("departTicketType").equals(testData.get("returnTicketType")))*/
            journey = "Return";
        }
        return journey;
    }

    private int getNumberOfPaxWithBag_Golf_Ski(HashMap<String,String> testData,String paxType,String service,int... journeyNo) {
        int paxWithServiceCount = 0;
        String journey = "";
        if(journeyNo.length>0) {
            journey = findJourney(journeyNo[0], testData);
        }
        int noOfPax = Integer.parseInt(testData.get("noOf"+paxType));
        for(int i=1;i<=noOfPax;i++){
            if(testData.get(paxType.toLowerCase()+i+service+journey)!=null){
                paxWithServiceCount++;
            }
        }
        return paxWithServiceCount;
    }

    public HashMap<String, String> verifyOtherServicesAndRetrieveEMD(String service,HashMap<String,Double> basket){
        //verifies car parking and other services applicable
        HashMap<String,String> emd = new HashMap<>();
        try{
            //verifying service name
            System.out.println(getExpectedServiceMessage(service)+" == "+otherServices.findElement(By.className(serviceName)).getText());
            Assert.assertEquals(getExpectedServiceMessage(service),otherServices.findElement(By.className(serviceName)).getText());
            //verifying service cost
            System.out.println(basket.get(service)+" == "+CommonUtility.convertStringToDouble(otherServices.findElement(By.className(serviceCost)).getText()));
            Assert.assertEquals(basket.get(service),CommonUtility.convertStringToDouble(otherServices.findElement(By.className(serviceCost)).getText()));
            //retrieving service emd
            emd.put(service.replace("\\s","")+"EMD",otherServices.findElement(By.className(serviceEMD)).getText());
            System.out.println(emd);
            return emd;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify "+service+" confirmation and retrieve EMD generated");
            throw e;
        }
    }

    private String getExpectedServiceMessage(String serviceType,String... ticketType){

        if (serviceType.equals("23kg")) {
            return "1 × 23kg baggage";
        } else if((serviceType.equals("46kg"))) {
            if(ticketType[0].equals("just fly"))
                return "2 × 23kg baggage";
            else
                return "1 × 23kg baggage";
        }
        else if(serviceType.equals("ski")){
            return "1 × Skis";
        }
        else if(serviceType.equals("snowboard")){
            return "1 × Snowboard";
        }
        else if(serviceType.equals("WCHR")){
            return "1 × Wheelchair assistance from the check-in desk to the bottom of the aircraft steps";
        }
        else if(serviceType.equals("WCHS")){
            return "1 × Wheelchair assistance from the check-in desk to the top of the aircraft steps";
        }
        else if(serviceType.equals("WCHC")){
            return "1 × Wheelchair assistance from the check-in desk to the seat on board the aircraft";
        }
        else if(serviceType.equals("airport parking")){
            return "1 × Parking";
        }
        else{
            return "1 × Golf Equipment";
        }
    }

    public String verifyAndGetReservationName(HashMap<String,String> testData){
        //gets the passenger details from conf page
        String passengerName= null,expected=null;
        try{
            actionUtility.waitForElementVisible(ticketInformation,90);
            passengerName = reservationName.getText();
            if(Integer.parseInt(testData.get("noOfAdult"))>0) {
                expected = testData.get("adult1Name").split(" ")[2];
            }
            else{
                expected = testData.get("teen1Name").split(" ")[2];
            }
            Assert.assertTrue(passengerName.equalsIgnoreCase(expected));
            reportLogger.log(LogStatus.INFO,"reservation name is correctly displayed-" +passengerName);
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"expected reservation name is "+expected+" but reservation name displayed is "+passengerName);
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify and capture the reservation name");
            throw e;
        }
        return passengerName;
    }
}


