package efly.business_logics.online;

import com.google.gson.JsonObject;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.CommonUtility;
import utilities.DataUtility;
import utilities.TestManager;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Indhu on 6/20/2019.
 */
public class EFLYManageBookingPayment{
    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public EFLYManageBookingPayment(){
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    //booking details
    private String cardToBeSelected = "//div[label[span[contains(text(),'%s')]]]/input";

    @FindBy(css = "input[name*='cardNumber']")
    private WebElement cardNumber;

    @FindBy(name = "securityCode")
    private WebElement securityNumber;

    @FindBy(name = "nameOnCard")
    private WebElement nameOnCard;

    @FindBy(css = "select[id*='ccMonth']")
    private WebElement expiryMonth;

    @FindBy(css = "select[id*='ccYear']")
    private WebElement expiryYear;

    @FindBy(css = "label[for*='radio_BE_PAYPAL']")
    private WebElement paymentByPayPal;

    @FindBy(name = "ccCountry")
    private WebElement country;

    @FindBy(name = "ccPostalCode")
    private WebElement postCode;

    @FindBy(xpath = "//button[text()='Find my address']")
    private WebElement findMyAddress;

    @FindBy(css = "div[class='address-list-container'] input")
    private List<WebElement> address;

    @FindBy(xpath = "//button[contains(text(),'SELECT')]")
    private WebElement selectAddress;

    @FindBy(name = "ccState")
    private WebElement state;

    @FindBy(name = "ccAddressFirstLine")
    private WebElement addressLineOne;

    @FindBy(name = "ccCity")
    private WebElement city;

    @FindBy(xpath = "//div[@class='plnext-dropdown form-group']")
    private WebElement addressFrom;

    @FindBy(xpath = "//ul[@class='dropdown-menu ']//a")
    private List<WebElement> saveAddress;

    //terms and conditions
    @FindBy(css = "div[id*='termsAndConditions'] div[class='checkbox'] input")
    private WebElement termsAndConditions;

    @FindBy(xpath = "//span[text()='Pay Now']")
    private WebElement payNow;

    //Paypal site
    @FindBy(id = "email")
    private WebElement payPalEmail;

    @FindBy(id = "password")
    private WebElement payPalPassword;

    @FindBy(id = "btnLogin")
    private WebElement login;

    @FindBy(css = "button[class='btn full confirmButton continueButton']")
    private WebElement continueWithBalance;

    @FindBy(id = "confirmButtonTop")
    private WebElement confirmBalance;

    @FindBy(css = "p[id='spinner-message']")
    private WebElement payPalSpinner;

    String payPalProcessingSpinner = "p[id='spinner-message']";

    @FindBy (xpath = "//button[@id='btnNext']")
    private WebElement nextButton;

    //Currency
    @FindBy(css = "select[id*='widget-input-purchaseForm-mcpForm-currencyOfferId']")
    private WebElement paymentCurrency;

    //Basket - ancillaries
    @FindBy(xpath = "//div[div[span[contains(text(),'Travel Insurance')]]]//span[contains(@class,'tripsummary-price-amount')]")
    private WebElement insuranceTotal;

    String serviceCost = "//div[h3[contains(text(),'%s')]]//span[@class='tripsummary-price-amount-text']";

    @FindBy(xpath = "//div[span[contains(text(),'Total new services')]]//span[@class='tripsummary-price-amount-text']")
    private WebElement services;

    @FindBy (css = "div[class='tripsummary-price-amount text-right'] abbr[class='tripsummary-price-amount-currency']")
    private WebElement currencyInBasket;

    @FindBy(css = "div[class*='tripsummary-price-total'] span[class='tripsummary-price-amount-text']")
    private WebElement totalPrice;

    //Basket - modify flight

    @FindBy(xpath = "//div[div[h3[contains(text(),'flights')]]]//span[contains(@class,'price-amount-text')]")
    private WebElement newFlight;

    String basketItems = "//div[contains(@class,'tripsummary-price')]//div[span[contains(text(),'%s')]]//span[contains(@class,'price-value')]";

    //3DS

    @FindBy(className = "vbv-iframe")
    private WebElement frame_3ds;

    @FindBy(css = "input[name='password'][id*='userInput']")
    private WebElement passCode;

    @FindBy(css = "form[id='masterForm'] input[value='Do Authentication']")
    private WebElement authenticate;

    @FindBy(id = "backToMerchant")
    private WebElement backToMerchant;

    //mcp
    @FindBy(css = "section[id*='mcpBlockerWarningDialogContent'] button:nth-child(2)")
    private WebElement continueMCP;

    @FindBy (css = "div[class='plnext-modal-dialog dialog-show'] div[class='message-title']")
    private WebElement blockerMessage;

    public void verifyCurrencyInBasket(String currencyToBeVerified){
        // verify the currency displayed in basket
        try{
            String[] expectedCurrency = {"£","€","$","¥","Kr","CHF"};
            if(currencyToBeVerified.contains("EUR")){
                org.testng.Assert.assertTrue(currencyInBasket.getText().equals(expectedCurrency[1]));
            }
            else if(currencyToBeVerified.contains("AUD")){
                org.testng.Assert.assertTrue(currencyInBasket.getText().equals(expectedCurrency[2]));
            }
            else if(currencyToBeVerified.contains("CAD")){
                org.testng.Assert.assertTrue(currencyInBasket.getText().equals(expectedCurrency[2]));
            }
            else if(currencyToBeVerified.contains("HKD")){
                org.testng.Assert.assertTrue(currencyInBasket.getText().equals(expectedCurrency[2]));
            }
            else if(currencyToBeVerified.contains("JPY")){
                org.testng.Assert.assertTrue(currencyInBasket.getText().equals(expectedCurrency[3]));
            }
            else if(currencyToBeVerified.contains("NOK")){
                org.testng.Assert.assertTrue(currencyInBasket.getText().equals(expectedCurrency[4]));
            }
            else if(currencyToBeVerified.contains("USD")){
                org.testng.Assert.assertTrue(currencyInBasket.getText().equals(expectedCurrency[2]));
            }
            else if(currencyToBeVerified.contains("CHF")){
                org.testng.Assert.assertTrue(currencyInBasket.getText().equals(expectedCurrency[5]));
            }
            else {
                org.testng.Assert.assertTrue(currencyInBasket.getText().equals(expectedCurrency[0]));
            }
            reportLogger.log(LogStatus.INFO,"currency - "+currencyToBeVerified+" is displayed correctly in basket");
        }catch(AssertionError e){
            reportLogger.log(LogStatus.WARNING, "currency displayed in basket - "+currencyInBasket.getText()+" doesn't match with expected currency -  "+currencyToBeVerified );
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the currency of payment in basket");
            throw e;
        }
    }

    public void verifyBasketAncillaries(HashMap<String,Double> basket){
        //verifies basket
        String service = null;
        Double expectedPrice = 0.00;
        Double actualPrice = 0.00;
        try{
            if(basket.get("your seat")!=null){
                service = "your seat";
                expectedPrice = basket.get("your seat");
                actualPrice = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(serviceCost,"Your seat"))).getText());
                Assert.assertEquals(actualPrice,expectedPrice);
                reportLogger.log(LogStatus.INFO,"successfully verified your seat cost in basket - "+actualPrice);
            }
            if(basket.get("hold baggage")!=null){
                service = "hold baggage";
                expectedPrice = basket.get("hold baggage");
                actualPrice = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(serviceCost,"Hold baggage"))).getText());
                Assert.assertEquals(actualPrice,expectedPrice);
                reportLogger.log(LogStatus.INFO,"successfully verified hold baggage cost in basket - "+actualPrice);
            }
            if(basket.get("golf clubs")!=null){
                service = "golf clubs";
                expectedPrice = basket.get("golf clubs");
                actualPrice = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(serviceCost,"Golf clubs"))).getText());
                Assert.assertEquals(actualPrice,expectedPrice);
                reportLogger.log(LogStatus.INFO,"successfully verified golf clubs cost in basket - "+actualPrice);
            }
            if(basket.get("skis")!=null){
                service = "ski/snowboard";
                expectedPrice = basket.get("skis");
                actualPrice = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(serviceCost,"Skis"))).getText());
                Assert.assertEquals(actualPrice,expectedPrice);
                reportLogger.log(LogStatus.INFO,"successfully verified ski/snowboard cost in basket - "+actualPrice);
            }
            if(basket.get("total new services")!=null)  {
                service = "total new services";
                expectedPrice = basket.get("total new services");
                actualPrice = CommonUtility.convertStringToDouble(services.getText());
                Assert.assertEquals(actualPrice,expectedPrice);
                reportLogger.log(LogStatus.INFO,"successfully verified services cost in basket - "+actualPrice);
            }
        }catch(AssertionError e){
            System.out.println(service+" == "+expectedPrice+" --- "+actualPrice);
            reportLogger.log(LogStatus.INFO,service+" cost displayed in basket - "+actualPrice+" doesn't match with expected insurance cost - "+expectedPrice);
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify basket");
            throw e;
        }
    }

    public boolean enterPaymentDetails(HashMap<String, String> testData,String paymentFor,HashMap<String,Double>... basket) {
        // selects the booking type and enters the card details
        try {
            if(basket.length > 0) {
                if (paymentFor.equals("modifyFlight")) {
                    if (basket[0].get("to be paid") != null && basket[0].get("to be paid") == 0){
                        return false;
                    }
                } else if (paymentFor.equals("modifyTravelExtras")) {
                    if (basket[0].get("total new services") != null && basket[0].get("total new services") == 0) {
                        return false;
                    }
                }
            }
            enterDetails(testData);
            return true;
        }catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to do payment");
            throw e;
        }
    }

    private void enterDetails(HashMap<String,String> testData){
        actionUtility.waitForElementVisible(paymentByPayPal, 90);
        if (testData.get("paymentType") == null || testData.get("paymentType").toLowerCase().contains("card")) {
            actionUtility.hardClick(TestManager.getDriver().findElement(By.xpath(String.format(cardToBeSelected, testData.get("cardName")))));
            populateCardDetails(testData.get("cardName"));
            populateAddress(testData);
            reportLogger.log(LogStatus.INFO, "successfully entered  payment details for " + testData.get("cardName"));
        } else if (testData.get("paymentType").toLowerCase().contains("paypal")) {
            actionUtility.hardClick(paymentByPayPal);
            reportLogger.log(LogStatus.INFO, "successfully selected paypal payment option");
        }
    }

    private void populateCardDetails(String cardName) {
        //depending on the card type fetches data from JSON and populate in the respective field
        JsonObject cardDetails = DataUtility.getJsonData("cardDetails.json");
        actionUtility.sendKeys(cardNumber,cardDetails.getAsJsonObject(cardName.toLowerCase()).get("cardNumber").getAsString());
        String[] expiryDates = cardDetails.getAsJsonObject(cardName.toLowerCase()).get("expiryDate").getAsString().split("/");
        actionUtility.dropdownSelect(expiryMonth, ActionUtility.SelectionType.SELECTBYTEXT, expiryDates[0]);
        actionUtility.dropdownSelect(expiryYear, ActionUtility.SelectionType.SELECTBYTEXT, expiryDates[1]);
        actionUtility.sendKeys(nameOnCard,cardDetails.getAsJsonObject(cardName.toLowerCase()).get("nameOnCard").getAsString());
        if(!cardName.equalsIgnoreCase("UATP")) {
            actionUtility.sendKeys(securityNumber, cardDetails.getAsJsonObject(cardName.toLowerCase()).get("securityNumber").getAsString());
        }
        reportLogger.log(LogStatus.INFO, "successfully selected " + cardName + " card type and the card number is " + cardDetails.getAsJsonObject(cardName.toLowerCase()).get("cardNumber").getAsString());
    }

    private void populateAddress(HashMap<String,String> testData){
        //populates address given during prime booking
        actionUtility.waitForElementVisible(addressFrom,30);
        actionUtility.click(addressFrom);
        actionUtility.click(saveAddress.get(0));
    }

    public void acceptTandCandCompleteBooking(){
        try{
            actionUtility.waitForElementVisible(payNow,90);
            actionUtility.hardClick(termsAndConditions);
            actionUtility.hardClick(payNow);
            reportLogger.log(LogStatus.INFO,"successfully checked terms and conditions");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to check terms and conditions");
            throw e;
        }
    }

    public void completePayPalCardPayment() {
//        enters paypal digitalProfile details  and completes the paypal booking
        try {
            actionUtility.waitForElementVisible(payPalEmail,30);
            JsonObject cardDetails = DataUtility.getJsonData("cardDetails.json");
            payPalEmail.clear();
            payPalEmail.sendKeys(cardDetails.getAsJsonObject("paypal").get("payPalUserName").getAsString());
            actionUtility.click(nextButton);
            payPalPassword.clear();
            payPalPassword.sendKeys(cardDetails.getAsJsonObject("paypal").get("payPalPassword").getAsString());
            actionUtility.click(login);
            //actionUtility.switchBackToWindowFromFrame();
            /*try {
                actionUtility.waitForElementVisible(payPalSpinner, 5);
            }catch (TimeoutException e){}
            actionUtility.waitForElementNotPresent(payPalProcessingSpinner, 30);*/
            actionUtility.waitForElementVisible(continueWithBalance,30);
            actionUtility.hardClick(continueWithBalance);
            /*try {
                actionUtility.waitForElementVisible(payPalSpinner, 5);
            }catch (TimeoutException e){}
            actionUtility.waitForElementNotPresent(payPalProcessingSpinner, 8);*/
            actionUtility.waitForElementVisible(confirmBalance,30);
            actionUtility.hardClick(confirmBalance);
            reportLogger.log(LogStatus.INFO, "successfully paid using paypal");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to enter paypal details");
            throw e;
        }
    }

    private void verify(String item,Double expected,Double actual){
        try{
            Assert.assertEquals(actual,expected);
        }catch(AssertionError ae){
            System.out.println(item+" = "+expected+" --- "+actual);
            reportLogger.log(LogStatus.INFO,item+" expected is "+expected+" but displayed is "+actual);
            throw ae;
        }
    }

    public void complete3DSecureIdentification(HashMap<String,String> testData,boolean... paymentApplicable){
        try{
            if(paymentApplicable.length>0 && !paymentApplicable[0]){
                return;
            }
            String[] applicableCards = {"Visa","Diners Club","MasterCard","American Express"};
            if(Arrays.asList(applicableCards).contains(testData.get("cardName"))) {
                int i=0;
                do {
                    actionUtility.waitAndSwitchToFrame(ActionUtility.FrameLocator.WEBELEMENT, frame_3ds);
                    actionUtility.sendKeys(passCode, "123");
                    actionUtility.click(authenticate);
                    actionUtility.click(backToMerchant);
                    i++;
                }while(i<2);
                actionUtility.switchBackToWindowFromFrame();
            reportLogger.log(LogStatus.INFO,"3D secure identification was successful and navigated to confirmation page");
            }
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to complete 3D secure identification and proceed to confirmation page");
            throw e;
        }
    }


    public void verifyBasketModifyFlight(HashMap<String,Double> basket){
        //verifies basket after modifying flight
        try{
            verify("new flight",basket.get("new flight"),CommonUtility.convertStringToDouble(newFlight.getText()));
            verify("to be paid",basket.get("to be paid"),CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(basketItems,"to be paid"))).getText()));
            if(basket.get("change fee")>0){
                verify("change fee",basket.get("change fee"),CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(basketItems,"Change fee"))).getText()));
            }
            else if(basket.get("change fee")==0.0){
                try{
                    Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(String.format(basketItems,"Change fee")));
                }catch(AssertionError e){
                    reportLogger.log(LogStatus.INFO,"change fee is added in basket even though change fee is not applicable for this booking");
                    throw e;
                }
            }
            verify("price difference",basket.get("price difference"),CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(basketItems,"Price difference"))).getText()));
            reportLogger.log(LogStatus.INFO,"successfully verified basket on payment page after modifying flight");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify basket on payment page after modifying flight");
            throw e;
        }
    }



}
