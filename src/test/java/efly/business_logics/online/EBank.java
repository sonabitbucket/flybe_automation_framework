package efly.business_logics.online;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.DataUtility;
import utilities.TestManager;

import java.util.HashMap;
import java.util.Iterator;


public class EBank {

        ActionUtility actionUtility;
        ExtentTest reportLogger;

    public EBank() {
            PageFactory.initElements(TestManager.getDriver(), this);
            this.actionUtility = new ActionUtility();
            this.reportLogger = TestManager.getReportLogger();
        }

    @FindBy(css = "button[class*='digitalProfile']")
    private WebElement login;

    @FindBy(id = "userAliasInput")
    private WebElement userName;

    @FindBy(id = "organizationInput")
    private WebElement organization;

    @FindBy(id = "passwordInput")
    private WebElement password;

    @FindBy(xpath = "//button[contains(text(),'Clear')]")
    private WebElement clear;

    @FindBy(xpath = "//button[contains(text(),'Login')]")
    private WebElement loginButton;

    @FindBy(xpath = "//button[contains(text(),'Create')]")
    private WebElement createVoucher;

    @FindBy(id = "voucherexternalID")
    private WebElement reference;

    @FindBy(id = "recipientOrganization")
    private WebElement issuedBy;

    @FindBy(id = "voucherType")
    private WebElement campaign;

    @FindBy(id = "voucherCurrency")
    private WebElement currency;

    @FindBy(id = "voucherAmount")
    private WebElement voucherAmount;

    @FindBy(css = "span[class*='voucherId']")
    private WebElement voucherID;

    public void navigateToEbankAndLogin(){
            try{
                String url = new DataUtility().testData("1").get("EbankURL").toString();
                TestManager.getDriver().get(url);
                login();
                actionUtility.waitForElementVisible(createVoucher,90);
                reportLogger.log(LogStatus.INFO,"successfully navigated to ebank site and logged in");
            }catch(Exception e){
                reportLogger.log(LogStatus.INFO,"unable to navigate to ebank site and digitalProfile");
                throw e;
            }
        }

    private void login(){
        actionUtility.click(loginButton);
        String user = new DataUtility().testData("1").get("EbankUserName").toString();
        String org = new DataUtility().testData("1").get("EbankOrganization").toString();
        String pwd = new DataUtility().testData("1").get("EbankPassword").toString();
        //actionUtility.hardSleep(1000);
        String parent = actionUtility.getPresentWindowHandle();
        actionUtility.switchWindow("eBank");
        actionUtility.click(clear);
        actionUtility.sendKeys(userName,user);
        actionUtility.sendKeys(organization,org);
        actionUtility.sendKeys(password,pwd);
        actionUtility.click(loginButton);
        //actionUtility.hardSleep(5000);
        actionUtility.switchToWindowHandle(parent);
    }

    public String createVoucherAndGetVoucherNo(HashMap<String,String> testData){
        try{
            actionUtility.click(createVoucher);
            populateDetails(testData);
            actionUtility.click(createVoucher);
            actionUtility.waitForElementVisible(voucherID,60);
            reportLogger.log(LogStatus.INFO,"successfully created voucher "+voucherID.getText());
            return voucherID.getText();
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to create voucher");
            throw e;
        }
    }

    private void populateDetails(HashMap<String,String> testData){
        actionUtility.sendKeys(reference,testData.get("voucherReference"));
        actionUtility.dropdownSelect(issuedBy, ActionUtility.SelectionType.SELECTBYTEXT,testData.get("voucherIssuedBy"));
        actionUtility.sendKeys(campaign,testData.get("voucherCampaign"));
        actionUtility.dropdownSelect(currency,ActionUtility.SelectionType.SELECTBYTEXT,testData.get("voucherCurrency"));
        actionUtility.sendKeys(voucherAmount,testData.get("voucherAmount"));
    }
}
