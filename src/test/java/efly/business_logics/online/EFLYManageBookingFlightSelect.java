package efly.business_logics.online;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.TestManager;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Created by Indhu on 6/17/2019.
 */
public class EFLYManageBookingFlightSelect {
    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public EFLYManageBookingFlightSelect(){
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(xpath = "//span[contains(text(),'Flights')]")
    private WebElement flightSection;

    @FindBy(css = "div[id*='itineraryReviewForm-checkBound'] div[class='checkbox']")
    private List<WebElement> journeySelection;

    @FindBy(xpath = "//button[span[contains(@class,'square icon-calendar')]]")
    private List<WebElement> datePicker;

    @FindBy(css = "div[class='calendarBox']>span:nth-child(1) div[class*='monthTitle']>span")
    private WebElement firstMonthDisplayed;

    @FindBy(css = "div[class='calendarBox']>span:nth-child(3) div[class*='monthTitle']>span")
    private WebElement lastMonthDisplayed;

    String modifyDatePicker ="//td[contains(@id,'xCalendar_std')][contains(@title,'%s')]";

    @FindBy(css = "span[class='plnext-widget-btn-text one-icon']")
    private WebElement continueButton;

    @FindBy(css = "span[class='icon-arrow-right']")
    private WebElement calendarControlRight;

    @FindBy(css = "span[class='icon-arrow-right']")
    private WebElement calendarControlLeft;

    @FindBy(css = "td[id*='xCalendar_std']")
    private WebElement date;

    @FindBy(css = "input[id*='itineraryReviewForm-origin']")
    private List<WebElement> flightFromModify;

    @FindBy(css = "input[id*='itineraryReviewForm-destination']")
    private List<WebElement> flightToModify;


    /*public void selectTripToModify(HashMap<String,String> testData,boolean... selectBothJourneys){
        try{
            actionUtility.waitForElementVisible(flightSection, 20);
            if(selectBothJourneys.length>0 && selectBothJourneys[0]){
                actionUtility.click(journeySelection.get(0));
                if(testData.get("returnDateOffset")!=null) {
                    actionUtility.click(journeySelection.get(1));
                }
            }
            else{
                if (testData.get("changeOutbound").equalsIgnoreCase("yes")) {
                    actionUtility.click(journeySelection.get(0));
                    actionUtility.hardSleep(200);
                }
                if (testData.get("changeInbound") != null) {
                    if (testData.get("changeInbound").equalsIgnoreCase("yes")) {
                        actionUtility.click(journeySelection.get(1));
                        actionUtility.hardSleep(200);
                    }
                }
            }
            reportLogger.log(LogStatus.INFO, "successfully selected the journey for modification");
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO, "unable to select the journey for modification");
            throw e;
        }

    }*/

    public void selectOutboundTripForModification(){
        try{
            actionUtility.waitForElementVisible(flightSection, 20);
            actionUtility.click(journeySelection.get(0));
            reportLogger.log(LogStatus.INFO, "successfully selected the outbound journey for  modification");
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO, "unable to select the outbound journey for modification");
            throw e;
        }
    }


    public void selectInboundTripForModification(){
        try{
            actionUtility.waitForElementVisible(flightSection, 20);
            actionUtility.click(journeySelection.get(1));
            reportLogger.log(LogStatus.INFO, "successfully selected the inbound journey for  modification");
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO, "unable to select the inbound journey for modification");
            throw e;
        }
    }

    public void changeDate(HashMap<String,String> testData,Boolean... tripSelection){
        try{
            actionUtility.waitForElementVisible(flightSection, 30);
            if(testData.get("mb_DepartDateOffset")!= null){
                if(tripSelection.length>0 && tripSelection[0]) {
                    selectOutboundTripForModification();
                }
                actionUtility.waitForElementVisible(datePicker.get(0),30);
                actionUtility.click(datePicker.get(0));
                selectDateFromCalendar(Integer.parseInt(testData.get("mb_DepartDateOffset")));
                actionUtility.hardSleep(3000);
            }
            if (testData.get("mb_ReturnDateOffset") != null) {
                if(tripSelection.length>0 && tripSelection[0]) {
                    selectInboundTripForModification();
                }
                actionUtility.waitForElementVisible(datePicker.get(1),30);
                actionUtility.click(datePicker.get(1));
                selectDateFromCalendar(Integer.parseInt(testData.get("mb_ReturnDateOffset")));
                actionUtility.hardSleep(3000);
            }
            reportLogger.log(LogStatus.INFO, "successfully changed the journey date");
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO, "unable to change travel dates in manage booking flow");
            throw e;
        }
    }

    private void selectDateFromCalendar(int offset){
        //selects the Date from calendar control
        WebElement calendarControl = null;
        String[] date = actionUtility.getDateWithFullMonthName(offset);
        boolean flag = verifyDatesInCalendar(date);
        if(!flag){
            calendarControl = selectCalendarControl(offset);
        }
        while (!flag){
            actionUtility.hardClick(calendarControl);
            flag = verifyDatesInCalendar(date);
        }
        actionUtility.hardClick(TestManager.getDriver().findElement(By.xpath(String.format(modifyDatePicker, date[0] + " " + date[1]+" "+date[2]))));
        actionUtility.hardSleep(300);
    }

    private WebElement selectCalendarControl(int offset){
        String monthYearToBeSelected = actionUtility.getMonth(offset)+" "+actionUtility.getYear(offset);
        String monthYearDisplayed1 = firstMonthDisplayed.getText().trim();
        String monthYearDisplayed2 = lastMonthDisplayed.getText().trim();
        YearMonth toBeSelected = actionUtility.convertStringinMMMMYYYYformatToDate(monthYearToBeSelected);
        YearMonth displayed1 = actionUtility.convertStringinMMMMYYYYformatToDate(monthYearDisplayed1);
        YearMonth displayed2 = actionUtility.convertStringinMMMMYYYYformatToDate(monthYearDisplayed2);
        if(toBeSelected.isBefore(displayed1)){
            return calendarControlLeft;
        }
        if(toBeSelected.isAfter(displayed2)) {
            return calendarControlLeft;
        }
        return null;
    }

    private boolean verifyDatesInCalendar(String[] date){
        String calDate = String.format(modifyDatePicker,date[0]+" "+date[1]+" "+date[2]);
        return actionUtility.verifyIfElementIsDisplayed(calDate);
    }

    public void navigateToFareSelectManageBooking(){
        try{
            actionUtility.waitForElementVisible(continueButton, 20);
            actionUtility.click(continueButton);
            actionUtility.waitForPageLoad(200);
            reportLogger.log(LogStatus.INFO,"successfully navigated to fare select page in manage booking flow");
        }catch (Exception e){
            reportLogger.log(LogStatus.ERROR,"unable to navigate to fare select page in manage booking flow");
            throw e;
        }
    }

    public void changeSource(HashMap<String,String> testData,Boolean... tripSelection) {
        // changes the source from flight section in manage booking flow
        try {
            if(tripSelection.length>0 && tripSelection[0]) {
                selectOutboundTripForModification();
            }
            actionUtility.hardClick(flightFromModify.get(0));
            flightFromModify.get(0).clear();
            actionUtility.hardSleep(2000);
            actionUtility.sendKeys(flightFromModify.get(0), testData.get("mb_FlightFrom") + Keys.RETURN);
            actionUtility.hardSleep(4000);
            if (testData.get("returnDateOffset") != null) {
                if(tripSelection.length>0 && tripSelection[0]) {
                    selectInboundTripForModification();
                }
                actionUtility.hardSleep(3000);
                actionUtility.click(flightToModify.get(1));
                flightToModify.get(1).clear();
                actionUtility.sendKeys(flightToModify.get(1), testData.get("mb_FlightFrom") + Keys.RETURN);
                actionUtility.hardSleep(4000);
            }
            reportLogger.log(LogStatus.INFO, "successfully changed the source to " + testData.get("mb_FlightFrom") );
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to change source from manage booking search");
            throw e;
        }
    }

    public void changeDestination(HashMap<String,String> testData,Boolean... tripSelection) {
        // changes the destination from select flight section in manage booking flow
        try {
            if(tripSelection.length>0 && tripSelection[0]) {
                selectOutboundTripForModification();
            }
            actionUtility.click(flightToModify.get(0));
            flightToModify.get(0).clear();
            actionUtility.hardSleep(3000);
            actionUtility.sendKeys(flightToModify.get(0), testData.get("mb_FlightTo") + Keys.RETURN);
            actionUtility.hardSleep(4000);
            if(testData.get("returnDateOffset")!=null){
                if(tripSelection.length>0 && tripSelection[0]) {
                    selectInboundTripForModification();
                }
                actionUtility.hardSleep(3000);
                actionUtility.click(flightFromModify.get(1));
                flightFromModify.get(1).clear();
                actionUtility.sendKeys(flightFromModify.get(1),testData.get("mb_FlightTo")+ Keys.RETURN);
                actionUtility.hardSleep(4000);
            }
            reportLogger.log(LogStatus.INFO, "successfully changed the destination to " + testData.get("mb_FlightTo"));
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to change destination from manage booking flight select");
            throw e;
        }
    }

}
