package efly.business_logics.online;

import com.google.gson.JsonObject;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.Assertion;
import utilities.ActionUtility;
import utilities.CommonUtility;
import utilities.DataUtility;
import utilities.TestManager;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static utilities.ActionUtility.SelectionType.SELECTBYTEXT;

/**
 * Created by Sona on 4/3/2019.
 */
public class EFLYPayment {
    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public EFLYPayment(){
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    //payment details
    private String cardToBeSelected = "//div[label[span[contains(text(),'%s')]]]/input";

    private String loader = "//div[@class='loader']";

    @FindBy(css = "input[name*='cardNumber']")
    private WebElement cardNumber;

    @FindBy(name = "securityCode")
    private WebElement securityNumber;

    @FindBy(name = "nameOnCard")
    private WebElement nameOnCard;

    @FindBy(css = "select[id*='ccMonth']")
    private WebElement expiryMonth;

    @FindBy(css = "select[id*='ccYear']")
    private WebElement expiryYear;

    @FindBy(xpath = "//li[label[span[contains(text(),'PayPal')]]]")
    private WebElement paymentByPayPal;

    @FindBy(name = "ccCountry")
    private WebElement country;

    @FindBy(name = "ccPostalCode")
    private WebElement postCode;

    @FindBy(xpath = "//button[text()='Find my address']")
    private WebElement findMyAddress;

    @FindBy(css = "div[class='address-list-container'] input")
    private List<WebElement> address;

    @FindBy(xpath = "//button[contains(text(),'SELECT')]")
    private WebElement selectAddress;

    @FindBy(name = "ccState")
    private WebElement state;

    @FindBy(name = "ccAddressFirstLine")
    private WebElement addressLineOne;

    @FindBy(name = "ccCity")
    private WebElement city;

    //terms and conditions
    @FindBy(css = "div[id*='termsAndConditions'] div[class='checkbox'] input")
    private WebElement termsAndConditions;

    @FindBy(xpath = "//span[text()='Pay Now']")
    private WebElement payNow;

    //Paypal site
    @FindBy(id = "email")
    private WebElement payPalEmail;

    @FindBy(id = "password")
    private WebElement payPalPassword;

    @FindBy (id = "btnNext")
    private WebElement nextButton;

    @FindBy(id = "btnLogin")
    private WebElement login;

    @FindBy(id = "button")
    private WebElement continueWithBalance;

    @FindBy(id = "confirmButtonTop")
    private WebElement confirmBalance;

    @FindBy(css = "p[id='spinner-message']")
    private WebElement payPalSpinner;

    String payPalProcessingSpinner = "p[id='spinner-message']";

    //insurance
    private String insurancePopup = "//button[span[text()='%s']]";

    @FindBy(xpath = "//span[contains(text(),'Yes, I need Travel insurance')]")
    private WebElement yesInsurance;

    @FindBy(xpath = "//span[contains(text(),'travel insurance')]")
    private WebElement yesInsuranceCHD;

    @FindBy(css = "span[class='insurance-amount']")
    private WebElement pnrInsuranceAmount;

    @FindBy(xpath = "//div[@class='radio']//span[contains(text(),'No,')]")
    private WebElement noInsurance;

    @FindBy (css = "section[id*='insurance']")
    private WebElement insuranceSection;

    String perPaxInsuranceYes = "//div[span[text()='%s']]//span[contains(text(),'travel insurance')]";

    String perPaxInsuranceNo = "//div[span[text()='%s']]//span[text()=\"I don't want to add travel insurance\"]";

    String perpaxInsuranceAmount = "//div[span[text()='%s']]//span[@class='insurance-amount']";

    //Currency
    @FindBy(css = "select[id*='widget-input-purchaseForm-mcpForm-currencyOfferId']")
    private WebElement paymentCurrency;

    //Basket
    @FindBy(xpath = "//div[div[span[contains(text(),'Travel Insurance')]]]//span[contains(@class,'tripsummary-price-amount')]")
    private WebElement insuranceTotal;

    String serviceCost = "//div[h3[contains(text(),'%s')]]//span[@class='tripsummary-price-amount-text']";

    @FindBy(className = "price-details-services-amount-value")
    private WebElement services;

    @FindBy (css = "div[class='tripsummary-price-amount text-right'] abbr[class='tripsummary-price-amount-currency']")
    private WebElement toPayCurrency;

    @FindBy (css = "span[class='trip-summary-total-cash-amount'] abbr")
    private WebElement total_Currency;

    @FindBy(css = "div[class*='tripsummary-price-total'] span[class='tripsummary-price-amount-text']")
    private WebElement totalPrice;

    //mcp
    @FindBy(css = "section[id*='mcpBlockerWarningDialogContent'] button:nth-child(2)")
    private WebElement continueMCP;

    @FindBy (css = "div[class='plnext-modal-dialog dialog-show'] div[class='message-title']")
    private WebElement blockerMessage;

    //voucher
    @FindBy(name = "ebank_voucher")
    private WebElement voucherCode;

    @FindBy(id = "eBankAddVoucherButton")
    private WebElement applyVoucher;

    @FindBy(css = "div[class*='total-voucher-currency'] strong")
    private WebElement appliedVoucherAmount;

    @FindBy(className = "payment-price-total")
    private WebElement amountToBePaidByCard;

    @FindBy(xpath = "//div[contains(text(),'No payment to be done. Your booking will be confirmed when you are ready to click continue.')]")
    private WebElement noPaymentText;

    //3DS

    @FindBy(className = "vbv-iframe")
    private WebElement frame_3ds;

    @FindBy(css = "input[name='password'][id*='userInput']")
    private WebElement passCode;

    @FindBy(css = "form[id='masterForm'] input[value='Do Authentication']")
    private WebElement authenticate;

    @FindBy(id = "backToMerchant")
    private WebElement backToMerchant;

    public void complete3DSecureIdentification(HashMap<String,String> testData){
        try{
            String[] applicableCards = {"Visa","Diners Club","MasterCard","American Express"};
            if(testData.get("cardName")!=null && Arrays.asList(applicableCards).contains(testData.get("cardName"))) {
                actionUtility.waitForElementVisible(frame_3ds,90);
                try {
                    Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(frame_3ds));
                }catch(AssertionError ae){
                    reportLogger.log(LogStatus.INFO,"3DS page is not displayed");
                    throw ae;
                }
                actionUtility.waitAndSwitchToFrame(ActionUtility.FrameLocator.WEBELEMENT, frame_3ds);
                actionUtility.sendKeys(passCode, "123");
                actionUtility.click(authenticate);
                actionUtility.click(backToMerchant);
                actionUtility.switchBackToWindowFromFrame();
                reportLogger.log(LogStatus.INFO,"3D secure identification was successful and navigated to confirmation page");
            }
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to complete 3D secure identification and proceed to confirmation page");
            throw e;
        }
    }

    public void applyVoucherCode(String code,String... mcp){
        try{
            actionUtility.waitForElementNotPresent(loader,20);
            voucherCode.clear();
            actionUtility.sendKeys(voucherCode,code);
            actionUtility.waitForElementNotPresent(loader,20);
            //actionUtility.clickByAction(applyVoucher);
            applyVoucher.sendKeys(Keys.ENTER);
            if(mcp.length>0){
                verifyVoucherAndMCPBlockerMessage();
                return;
            }
            reportLogger.log(LogStatus.INFO,"successfully applied voucher - "+code);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to apply voucher");
            throw e;
        }
    }

    private void verifyVoucherAndMCPBlockerMessage(){
        try{
            Assert.assertTrue(blockerMessage.getText().equals("Using Vouchers is only available with Pound Sterling"));
            reportLogger.log(LogStatus.INFO, "successfully verified the message when paying through voucher + MCP");
        }catch(AssertionError e) {
            reportLogger.log(LogStatus.INFO, "pop-up message doesn't match with expected message ");
            throw e;
        }catch (Exception e) {
            reportLogger.log(LogStatus.INFO, "unable to verify the message when paying through voucher + MCP");
            throw e;
        }

    }

    public String verifyVoucherAmountAppliedAndCompletePayment(HashMap<String,String> testData,String... blocker){
        String item = null;
        String amount_toBePaid = "0.00";
        try{
            actionUtility.waitForElementVisible(appliedVoucherAmount,30);
            if(CommonUtility.convertStringToDouble(totalPrice.getText())<Double.parseDouble(testData.get("voucherAmount"))){
                item = "full payment by voucher";
                Assert.assertTrue(CommonUtility.convertStringToDouble(appliedVoucherAmount.getText().trim()) == CommonUtility.convertStringToDouble(totalPrice.getText()));
                Assert.assertEquals(amountToBePaidByCard.getText(),"0.00");
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(noPaymentText));
            }
            else{
                item = "part payment by voucher";
                Assert.assertEquals(CommonUtility.convertStringToDouble(appliedVoucherAmount.getText().trim()),Double.parseDouble(testData.get("voucherAmount")),0.0);
                amount_toBePaid = CommonUtility.truncateToTWODecimalPlaces((CommonUtility.convertStringToDouble(totalPrice.getText())-Double.parseDouble(testData.get("voucherAmount"))))+"";
                Assert.assertTrue(CommonUtility.removeCommaFromCurrency(amountToBePaidByCard.getText()).equals(amount_toBePaid));
                if(blocker.length>0) {
                    enterPaymentDetails(testData, "voucher");
                }
                else{
                    enterPaymentDetails(testData);
                }
            }
            reportLogger.log(LogStatus.INFO,item+" is applied successfully");
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"unable to do "+item+" as the voucher amount is not applied as expected");
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to complete voucher payment");
            throw e;
        }
        return amountToBePaidByCard.getText();
    }

    public HashMap<String,Double> addInsurance(HashMap<String,String> testData,HashMap<String,Double>... basket){
        //adds insurance to the booking - PNR basis and Pax basis
        Double amount = 0.00;
        try{
            actionUtility.waitForElementVisible(paymentByPayPal,90);
            if(testData.get("noOfChild") == null || Integer.parseInt(testData.get("noOfChild")) == 0) {
                actionUtility.hardClick(yesInsurance);
                if (testData.get("noOfAdult") != null) {
                    amount += addInsuranceForEachPax(testData, "adult", Integer.parseInt(testData.get("noOfAdult")));
                }
                if (testData.get("noOfTeen") != null) {
                    amount += addInsuranceForEachPax(testData, "teen", Integer.parseInt(testData.get("noOfTeen")));
                }
            }
            else{
                actionUtility.hardClick(yesInsuranceCHD);
                amount = new Double(pnrInsuranceAmount.getText());
            }
            reportLogger.log(LogStatus.INFO, "successfully added insurance");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO, "unable to select insurance");
            throw e;
        }
        if(basket.length>0){
            basket[0].put("travel insurance",amount);
            return basket[0];
        }
        return null;
    }

    private Double addInsuranceForEachPax(HashMap<String,String> testData,String paxType,int noOfPax){
        Double amount = 0.00;
        for(int i=1;i<=noOfPax;i++){
            if(testData.get(paxType+i+"Insurance")!=null && testData.get(paxType+i+"Insurance").equalsIgnoreCase("yes") ) {
                System.out.println(testData.get(paxType+i+"Name"));
                actionUtility.hardClick(TestManager.getDriver().findElement(By.xpath(String.format(perPaxInsuranceYes,testData.get(paxType+i+"Name")))));
                amount += Double.parseDouble(TestManager.getDriver().findElement(By.xpath(String.format(perpaxInsuranceAmount,testData.get(paxType+i+"Name")))).getText());
            }
            else{
                actionUtility.hardClick(TestManager.getDriver().findElement(By.xpath(String.format(perPaxInsuranceNo,testData.get(paxType+i+"Name")))));
            }
            reportLogger.log(LogStatus.INFO,"successfully selected insurance for passenger "+paxType+i+" - "+testData.get(paxType+i+"Name"));
        }
        return amount;
    }

    public void changeCurrency(String currency){
        //changes currency on payment page
        //Pound Sterling (GBP),Euro (EUR)
        try {
            actionUtility.waitForElementNotPresent(loader,60);
            actionUtility.dropdownSelect(paymentCurrency,SELECTBYTEXT,currency);
            reportLogger.log(LogStatus.INFO,"successfully changed the currency to " +currency);
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to change the currency to " +currency);
            throw e;
        }
    }

    public String retrieve_verifyPaymentCurrency(String... expectedCurrency){
        // retrieves the payment currency
        String currencyDisplayed = null;
        try{
            currencyDisplayed = actionUtility.getValueSelectedInDropdown(paymentCurrency);
            if(expectedCurrency.length>0){
                try{
                    Assert.assertEquals(currencyDisplayed,expectedCurrency[0]);
                }catch(AssertionError e){
                    reportLogger.log(LogStatus.INFO,"expected currency is "+expectedCurrency[0]+" but "+currencyDisplayed+" is displayed on payment page");
                    throw e;
                }
            }
            reportLogger.log(LogStatus.INFO, "payment currency is "+currencyDisplayed);
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to retrieve the payment currency");
            throw e;
        }
        return currencyDisplayed;
    }

    public String verifyAndRetrieveCurrencySymbolInBasket(String currencyToBeVerified,String... baseCurrency){
        // verify the currency displayed in basket
        String expectedSymbol = "£";
        try{
            String[] expectedCurrency = {"£","€","$","¥","Kr","CHF"};
            if(currencyToBeVerified.contains("EUR")){
                expectedSymbol = expectedCurrency[1];
            }
            else if(currencyToBeVerified.contains("AUD")){
                expectedSymbol = expectedCurrency[2];
            }
            else if(currencyToBeVerified.contains("CAD")){
                expectedSymbol = expectedCurrency[2];
            }
            else if(currencyToBeVerified.contains("HKD")){
                expectedSymbol = expectedCurrency[2];
            }
            else if(currencyToBeVerified.contains("JPY")){
                expectedSymbol = expectedCurrency[3];
            }
            else if(currencyToBeVerified.contains("NOK")){
                expectedSymbol = expectedCurrency[4];
            }
            else if(currencyToBeVerified.contains("USD")){
                expectedSymbol = expectedCurrency[2];
            }
            else if(currencyToBeVerified.contains("CHF")){
                expectedSymbol = expectedCurrency[5];
            }
            if(baseCurrency.length>0 && baseCurrency[0].equalsIgnoreCase("yes")){
                Assert.assertTrue(total_Currency.getText().equals(expectedSymbol));
            }
            else {
                Assert.assertTrue(toPayCurrency.getText().equals(expectedSymbol));
            }
            reportLogger.log(LogStatus.INFO,"currency - "+currencyToBeVerified+" is displayed correctly in basket");
            return expectedSymbol;
        }catch(AssertionError e){
            System.out.println(expectedSymbol+" --- "+ toPayCurrency.getText());
            reportLogger.log(LogStatus.WARNING, "currency displayed in basket - "+ toPayCurrency.getText()+" doesn't match with expected currency -  "+currencyToBeVerified );
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify the currency of payment in basket");
            throw e;
        }
    }

    public void enterPaymentDetails(HashMap<String, String> testData,String... blocker) {
        // selects the booking type and enters the card details
        try {
            actionUtility.waitForElementNotPresent(loader,30);
            if (testData.get("paymentType") == null || testData.get("paymentType").toLowerCase().contains("card")) {
                actionUtility.hardClick(TestManager.getDriver().findElement(By.xpath(String.format(cardToBeSelected,testData.get("cardName")))));
                if(blocker.length>0 && blocker[0].equalsIgnoreCase("mcp")){
                    verifyMCPmessage(testData.get("cardName"));
                    return;
                }
                populateCardDetails(testData.get("cardName"));
                populateAddress(testData);
            }
            else if (testData.get("paymentType").toLowerCase().contains("paypal")) {
                actionUtility.hardClick(paymentByPayPal);
                reportLogger.log(LogStatus.INFO, "successfully selected paypal booking option");

                if(blocker.length>0 && blocker[0].equalsIgnoreCase("mcp")){
                    verifyMCPmessage("PayPal");
                    return;
                }else if(blocker.length>0 && blocker[0].equalsIgnoreCase("voucher")){
                    verifyVoucherBlockerMessage();
                    return;
                }
            }

        }catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to select payment option");
            throw e;
        }
    }

    private void verifyVoucherBlockerMessage(){
        try{
            Assert.assertTrue(blockerMessage.getText().equals("Using Vouchers is only available with certain methods of payment"));
            reportLogger.log(LogStatus.INFO, "successfully verified the message when paying through voucher + paypal");
        }catch(AssertionError e) {
            reportLogger.log(LogStatus.INFO, "pop-up message when paying through voucher + paypal doesn't match with expected message ");
            throw e;
        }catch (Exception e) {
            reportLogger.log(LogStatus.INFO, "unable to verify the message when paying through voucher + paypal");
            throw e;
        }
    }

    private void verifyMCPmessage(String paymentOption){
        try{
            Assert.assertTrue(blockerMessage.getText().contains("Using "+paymentOption+" is only available with Pound Sterling"));
            reportLogger.log(LogStatus.INFO,"successfully verified the MCP message");
        }catch(AssertionError e) {
            reportLogger.log(LogStatus.INFO, "MCP pop-up message doesn't match with expected message ");
            throw e;
        }catch (Exception e) {
            reportLogger.log(LogStatus.INFO, "unable to verify mcp message");
            throw e;
        }

    }

    private void populateCardDetails(String cardName) {
        //depending on the card type fetches data from JSON and populate in the respective field
        JsonObject cardDetails = DataUtility.getJsonData("cardDetails.json");
        actionUtility.sendKeys(cardNumber,cardDetails.getAsJsonObject(cardName.toLowerCase()).get("cardNumber").getAsString());
        String[] expiryDates = cardDetails.getAsJsonObject(cardName.toLowerCase()).get("expiryDate").getAsString().split("/");
        actionUtility.dropdownSelect(expiryMonth, ActionUtility.SelectionType.SELECTBYTEXT, expiryDates[0]);
        actionUtility.dropdownSelect(expiryYear, ActionUtility.SelectionType.SELECTBYTEXT, expiryDates[1]);
        actionUtility.sendKeys(nameOnCard,cardDetails.getAsJsonObject(cardName.toLowerCase()).get("nameOnCard").getAsString());
        if(!cardName.equalsIgnoreCase("UATP/Airplus")) {
            actionUtility.sendKeys(securityNumber, cardDetails.getAsJsonObject(cardName.toLowerCase()).get("securityNumber").getAsString());
        }
        reportLogger.log(LogStatus.INFO, "successfully selected " + cardName + " card type and the card number is " + cardDetails.getAsJsonObject(cardName.toLowerCase()).get("cardNumber").getAsString());
    }

    private void populateAddress(HashMap<String,String> testData){
        //populates address

        actionUtility.dropdownSelect(country, ActionUtility.SelectionType.SELECTBYTEXT, testData.get("country"));
        HashMap<String,String> addressTestData = null;
        addressTestData = new DataUtility().testData("1");
        if(testData.get("country").equalsIgnoreCase("united kingdom")) {
            postCode.clear();
            actionUtility.sendKeys(postCode,addressTestData.get("postCode").trim());
            /*actionUtility.hardClick(findMyAddress);
            actionUtility.hardClick(address.get(0));
            actionUtility.hardClick(selectAddress);*/
        }
        actionUtility.sendKeys(addressLineOne,addressTestData.get("addressLineOne"));
        actionUtility.sendKeys(city,addressTestData.get("city"));
        if(actionUtility.verifyIfElementIsDisplayed(state)) {
            actionUtility.dropdownSelect(state, ActionUtility.SelectionType.SELECTBYINDEX, "1");
        }
    }

    public void acceptTandCandCompleteBooking(){
        try{
            actionUtility.hardClick(termsAndConditions);
            actionUtility.hardClick(payNow);
            reportLogger.log(LogStatus.INFO,"successfully checked terms and conditions and navigated to confirmation page");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to check terms and conditions and navigate to confirmation page");
            throw e;
        }
    }

    public void handleInsurancePopup_Complete3DSIdentification(HashMap<String, String> testData,String... otpRequired){
        //selects insurance option based testData provided
        try{
            if (testData.get("insurancePopUp") != null && testData.get("insurancePopUp").equalsIgnoreCase("yes")) {
                actionUtility.click(TestManager.getDriver().findElement(By.xpath(String.format(insurancePopup, "Yes, I want to purchase insurance"))));
                reportLogger.log(LogStatus.INFO, "successfully selected insurance option - Yes, I want to purchase insurance");
            } else {
                actionUtility.click(TestManager.getDriver().findElement(By.xpath(String.format(insurancePopup, "No, continue without insurance"))));
                reportLogger.log(LogStatus.INFO, "successfully selected insurance option - No, continue without insurance");
            }
            if(otpRequired.length>0 && otpRequired[0].equals("no")){
                return;
            }
            complete3DSecureIdentification(testData);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select insurance option on the popup");
            throw e;
        }
    }

    public void completePayPalCardPayment() {
//        enters paypal digitalProfile details  and completes the paypal booking
        try {
            actionUtility.waitForElementVisible(payPalEmail,90);
            JsonObject cardDetails = DataUtility.getJsonData("cardDetails.json");
            payPalEmail.clear();
            payPalEmail.sendKeys(cardDetails.getAsJsonObject("paypal").get("payPalUserName").getAsString());
            actionUtility.click(nextButton);
            actionUtility.waitForElementVisible(payPalPassword,30);
            payPalPassword.clear();
            payPalPassword.sendKeys(cardDetails.getAsJsonObject("paypal").get("payPalPassword").getAsString());
            actionUtility.click(login);
            //actionUtility.switchBackToWindowFromFrame();
            /*try {
                actionUtility.waitForElementVisible(payPalSpinner, 5);
            }catch (TimeoutException e){}
            actionUtility.waitForElementNotPresent(payPalProcessingSpinner, 30);*/
            actionUtility.waitForElementVisible(continueWithBalance,30);
            actionUtility.click(continueWithBalance);
            /*try {
                actionUtility.waitForElementVisible(payPalSpinner, 5);
            }catch (TimeoutException e){}
            actionUtility.waitForElementNotPresent(payPalProcessingSpinner, 8);*/
            actionUtility.waitForElementVisible(confirmBalance,30);
            actionUtility.hardClick(confirmBalance);
            reportLogger.log(LogStatus.INFO, "successfully paid using paypal");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to enter paypal details");
            throw e;
        }
    }
    public HashMap<String,Double> verifyBasket(HashMap<String,Double> basket){
        //verifies basket
        String service = null;
        Double expectedPrice = 0.00;
        Double actualPrice = 0.00;
        Double expectedTotal = 0.00;
        try{
            if(basket.get("total")!=null) {
                expectedTotal = CommonUtility.truncateToTWODecimalPlaces(basket.get("total"));
            }
            if(basket.get("travel insurance")!=null) {
                try{
                    service = "travel insurance";
                    expectedPrice = basket.get("travel insurance");
                    actualPrice = CommonUtility.convertStringToDouble(insuranceTotal.getText());
                    Assert.assertEquals(actualPrice,expectedPrice);
                }catch (StaleElementReferenceException ex) {
                    actualPrice = CommonUtility.convertStringToDouble(insuranceTotal.getText());
                    Assert.assertEquals(actualPrice,expectedPrice);
                }
                //expectedTotalServiceCharges += actualPrice;
                expectedTotal += actualPrice;
                expectedTotal = CommonUtility.truncateToTWODecimalPlaces(expectedTotal);
                reportLogger.log(LogStatus.INFO,"successfully verified travel insurance cost in basket - "+actualPrice);
            }
            if(basket.get("your seat")!=null){
                service = "your seat";
                expectedPrice = basket.get("your seat");
                actualPrice = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(serviceCost,"Your seat"))).getText());
                Assert.assertEquals(actualPrice,expectedPrice);
                reportLogger.log(LogStatus.INFO,"successfully verified your seat cost in basket - "+actualPrice);
            }
            if(basket.get("hold baggage")!=null){
                service = "hold baggage";
                expectedPrice = basket.get("hold baggage");
                actualPrice = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(serviceCost,"Hold baggage"))).getText());
                Assert.assertEquals(actualPrice,expectedPrice);
                reportLogger.log(LogStatus.INFO,"successfully verified hold baggage cost in basket - "+actualPrice);
            }
            if(basket.get("golf clubs")!=null){
                service = "golf clubs";
                expectedPrice = basket.get("golf clubs");
                actualPrice = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(serviceCost,"Golf clubs"))).getText());
                Assert.assertEquals(actualPrice,expectedPrice);
                reportLogger.log(LogStatus.INFO,"successfully verified golf clubs cost in basket - "+actualPrice);
            }
            if(basket.get("skis")!=null){
                service = "ski/snowboard";
                expectedPrice = basket.get("skis");
                actualPrice = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(serviceCost,"Skis"))).getText());
                Assert.assertEquals(actualPrice,expectedPrice);
                reportLogger.log(LogStatus.INFO,"successfully verified ski/snowboard cost in basket - "+actualPrice);
            }
            if(basket.get("airport parking")!=null){
                service = "airport parking";
                expectedPrice = basket.get("airport parking");
                actualPrice = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(serviceCost,"Airport parking"))).getText());
                Assert.assertEquals(actualPrice,expectedPrice);
                reportLogger.log(LogStatus.INFO,"successfully verified airport parking cost in basket - "+actualPrice);
            }
            if(basket.get("services")!=null)  {
                service = "services";
                expectedPrice = basket.get("services");
                actionUtility.scrollToViewElement(services);
                actualPrice = CommonUtility.convertStringToDouble(services.getText());
                Assert.assertEquals(actualPrice,expectedPrice);
                //basket.put("services",actualPrice);
                reportLogger.log(LogStatus.INFO,"successfully verified services cost in basket - "+actualPrice);
            }
            if(expectedTotal>0) {
                service = "total";
                expectedPrice = expectedTotal;
                actualPrice = CommonUtility.convertStringToDouble(totalPrice.getText());
                Assert.assertEquals(actualPrice, CommonUtility.truncateToTWODecimalPlaces(expectedPrice));
                basket.put("total", actualPrice);
                reportLogger.log(LogStatus.INFO,"successfully verified total cost in basket - "+actualPrice);
            }
            else{
                service = "total";
                expectedPrice = basket.get("total");
                actualPrice = CommonUtility.convertStringToDouble(totalPrice.getText());
                Assert.assertEquals(actualPrice, expectedPrice);
                reportLogger.log(LogStatus.INFO,"successfully verified total cost in basket - "+actualPrice);
            }
            return basket;
        }catch(AssertionError e){
            System.out.println(service+" == "+expectedPrice+" --- "+actualPrice);
            reportLogger.log(LogStatus.INFO,service+" cost displayed in basket - "+actualPrice+" doesn't match with expected insurance cost - "+expectedPrice);
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify basket");
            throw e;
        }
    }

    public void verifyInsuranceSection(){
        try{
            Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(insuranceSection));
            reportLogger.log(LogStatus.INFO,"insurance section is not displayed for Non UK to UK route");
        }catch(AssertionError e) {
            reportLogger.log(LogStatus.INFO, "insurance Section is displayed ");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify insurance section");
            throw e;
        }

    }
}


