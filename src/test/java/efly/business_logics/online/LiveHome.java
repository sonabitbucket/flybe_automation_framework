package efly.business_logics.online;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.TestManager;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Sona on 4/30/2019.
 */
public class LiveHome {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public LiveHome() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    //search widget
    @FindBy(id = "flight-search-button" )
    private WebElement findFlights;

    @FindBy(xpath = "//div[@id='flight_from_chosen']/a/div/b")
    private WebElement flightFromDropDown;

    @FindBy(xpath = "//div[@id ='flight_from_chosen']//input")
    private WebElement flightFrom;

    @FindBy(xpath = "//div[@id='flight_to_chosen']/a")
    private WebElement flightToDropDown;

    @FindBy(xpath = "//div[@id='flight_to_chosen']//input")
    private WebElement flightTo;

    @FindBy(className = "ui-datepicker-year")
    private WebElement datePickerYear;

    @FindBy(className = "ui-datepicker-month")
    private WebElement datePickerMonth;

    String datePickerDay = "//div[@id='ui-datepicker-div']/div[contains(@class,'first')]/descendant::a[text()='%s']";

    //cookie popup
    @FindBy(className = "modal-dialog")
    private WebElement popUp;

    @FindBy(className = "modal-header")
    private WebElement popUpHeader;

    @FindBy(id = "accept-cookies")
    private WebElement acceptCookies;

    @FindBy(id = "amend-cookie-prefs")
    private WebElement amendCookies;

    @FindBy(id = "your-cookie-settings-button")
    private WebElement cookiePrefDone;

    @FindBy(css = ".slider.round")
    private List<WebElement> cookiePreference;



    public void navigateToLive(){
        //navigate to live home page
        try{
            String appUrl = "https://www.flybe.com/";
            try{
                TestManager.getDriver().navigate().to(appUrl);
            }catch (TimeoutException e){}
            reportLogger.log(LogStatus.INFO, "successfully navigated to live page");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to navigate to the live page");
            throw e;
        }
    }

    public Boolean enterSourceAndDestination(HashMap<String,String>... testData){
        //enter source and destination
        Boolean popupHandled = false;
        try {
            popupHandled = handleHomeScreenPopUp(testData[0]);
            actionUtility.hardSleep(2000);
            actionUtility.click(findFlights);
            actionUtility.waitForElementClickable(flightFromDropDown,3);
            actionUtility.click(flightFromDropDown);
            actionUtility.hardSleep(2000);
            actionUtility.sendKeysByAction(flightFrom, testData[1].get("departure") + Keys.RETURN);
            actionUtility.hardSleep(2000);
            actionUtility.sendKeysByAction(flightTo, testData[1].get("destination") + Keys.RETURN);
            reportLogger.log(LogStatus.INFO,"successfully entered the source - "+testData[1].get("departure")+" and destination - "+testData[1].get("destination"));
            return popupHandled;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to enter source and destination of the trip");
            throw e;
        }
    }

    public Boolean handleHomeScreenPopUp(HashMap<String,String> testData){
//     Handles the home screen popup/modal
        Boolean popUpHandled = false;
        String popUpMsg = "no cookie modal";
        try{
            try {
                actionUtility.waitForElementVisible(popUp, 2);
                popUpMsg = popUpHeader.getText();
                System.out.println(popUpHeader.getText() + "---"+popUpMsg);
                reportLogger.log(LogStatus.INFO,"home screen cookie modal is displayed");

            }catch (TimeoutException e){
                reportLogger.log(LogStatus.INFO,"home screen cookie modal is not displayed");

            }
            if(popUpMsg.equals("This website uses cookies")){
                int pref = 0;
                String[] preference = {"strictly necessary","analytical and performance","personalization","targeting and marketing"};
                if(testData.get("amendCookie")!=null && testData.get("amendCookie").equalsIgnoreCase("yes")){
                    actionUtility.hardClick(amendCookies);
                    if(testData.get("cookiePreferenceNotNeeded1")!=null) {
                        pref = Arrays.asList(preference).indexOf(testData.get("cookiePreferenceNotNeeded1").toLowerCase());
                        actionUtility.hardClick(cookiePreference.get(pref));
                    }
                    if(testData.get("cookiePreferenceNotNeeded2")!=null){
                        pref = Arrays.asList(preference).indexOf(testData.get("cookiePreferenceNotNeeded2").toLowerCase());
                        actionUtility.hardClick(cookiePreference.get(pref));
                    }
                    if(testData.get("cookiePreferenceNotNeeded3")!=null){
                        pref = Arrays.asList(preference).indexOf(testData.get("cookiePreferenceNotNeeded3").toLowerCase());
                        actionUtility.hardClick(cookiePreference.get(pref));
                    }
                    actionUtility.hardClick(cookiePrefDone);
                }
                else {
                    actionUtility.waitForElementClickable(acceptCookies, 3);
                    actionUtility.hardClick(acceptCookies);
                }
                popUpHandled = true;
                reportLogger.log(LogStatus.INFO,"successfully handled the cookie modal");
            }

            //reportLogger.log(LogStatus.INFO,"successfully handled the cookie modal");
            //popUpHandled = true;
            if(popUpMsg.toLowerCase().equals("no cookie modal")) {
                throw new RuntimeException("Cookie Modal is not displayed");
            }
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to close the home screen cookie modal or cookie modal is not displayed");
            //throw e;
        }
        return popUpHandled;
    }

    public void enterTravelDates(HashMap<String,String> details,String... fromTestData){
        // enter the travel dates

        try {
            String[] date = details.get("dateSelected").split(" ");
            WebElement departure = TestManager.getDriver().findElement(By.id("departureDate"));
            selectDateFromCalendar(date,departure);
            reportLogger.log(LogStatus.INFO, "Entered the depart date - "+date[0]+"-"+date[1]+"-"+date[2]);
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable to enter travel Dates");
            throw e;
        }
    }

    private void selectDateFromCalendar(String date[],WebElement datePicker){
        //selects the Date from calendar control
        actionUtility.waitForElementVisible(datePicker,3);
        actionUtility.hardClick(datePicker);
//        String date[] = actionUtility.getDate(offset);
        actionUtility.dropdownSelect(datePickerYear, ActionUtility.SelectionType.SELECTBYTEXT,date[2]);
        actionUtility.dropdownSelect(datePickerMonth, ActionUtility.SelectionType.SELECTBYTEXT,date[1].substring(0,1)+date[1].substring(1).toLowerCase());
        if(date[0].charAt(0)=='0') {
            actionUtility.click(TestManager.getDriver().findElement(By.xpath(String.format(datePickerDay, date[0].substring(1)))));
        }
        else{
            actionUtility.click(TestManager.getDriver().findElement(By.xpath(String.format(datePickerDay, date[0]))));
        }

    }

    public void findFlights(){
        // click on  the find flights
        try{
            actionUtility.hardClick(findFlights);
            actionUtility.hardSleep(3000);
            actionUtility.waitForPageLoad(20);
            actionUtility.hardSleep(3000);

            reportLogger.log(LogStatus.INFO, "Flight searched");

        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable to search flights");
            throw e;
        }
    }



}
