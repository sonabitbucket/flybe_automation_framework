package efly.business_logics.online;

import com.google.gson.JsonObject;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import sun.rmi.runtime.Log;
import utilities.ActionUtility;
import utilities.CommonUtility;
import utilities.DataUtility;
import utilities.TestManager;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Indhu on 6/25/2019.
 */
public class EFLYManageBookingConfirmation {
    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public EFLYManageBookingConfirmation(){
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(css = "span[class='number']")
    private WebElement reservationNo;

    @FindBy (xpath = "//span[contains(text(),'Ticket Information')]")
    private WebElement ticketInformation;

    @FindBy (css = "li[class='reservation-name'] span:nth-child(2)")
    private WebElement reservationName;

    String button = "//button[span[contains(text(),'%s')]]";

    //service

    @FindBy(css = "ul[class*='servicesbreakdown-segment-list servicesbreakdown-list']")
    List<WebElement> seatForAllSectors;

    String serviceInfoForEachPaxTag = "li";

    String serviceName = "servicesbreakdown-service-name";

    String serviceEMD = "servicesbreakdown-service-emd";

    String serviceCost = "servicesbreakdown-service-price";

    @FindBy(css = "ul[class*='servicesbreakdown-bound-list servicesbreakdown-list']")
    List<WebElement> serviceForBothJourney;

   //checkin

    @FindBy(xpath = "//a[contains(text(),'now open ')]")
    private WebElement checkInLink;

    //price section
    String itemsOnLHS = "//div[contains(@class,'price-left')]//div[span[contains(text(),'%s')]]//span[contains(@class,'price-value')]";

    String itemsOnRHS = "//div[contains(@class,'price-right')]//div[span[contains(text(),'%s')]]//span[contains(@class,'price-value')]";

    @FindBy(css = "div[class*='price-atc-balance-amount']")
    private WebElement totalPaidWithCurrency;

    @FindBy(xpath = "//p[span[@class='card-name']]")
    private WebElement paymentInfo;

    @FindBy(css = "li[class='payment-item-no-bullet'] span[class='bold-without-padding']")
    private WebElement paymentWithText;

    @FindBy(css = "li[class='payment-item-no-bullet'] li[class='payment-sub-item']")
    private WebElement paymentDetails;

    //itinerary section
    @FindBy(className = "itinerary")
    private List<WebElement> itinerary;

    @FindBy(className = "date")
    List<WebElement> journeyDates;

    @FindBy(className = "total-duration-value")
    List<WebElement> journeyDuration;

    @FindBy(css = "div[class='segment-details-departure '] div div:nth-child(2)")
    List<WebElement> journeySource;

    @FindBy(css = "div[class='segment-details-arrival '] div div:nth-child(2)")
    List<WebElement> journeyDestination;

    @FindBy(className = "stop-number-text")
    List<WebElement> journeySectors;

    @FindBy(className = "ff-name")
    List<WebElement> ticketType;

    @FindBy(xpath = "//div[span[@class='flight-info-airline-name']]")
    List<WebElement> flightNos;

    @FindBy(css = "div[class*='flight-info-aircraft'] div:nth-child(2)")
    List<WebElement> aircraft;

    @FindBy(css = "time[class*='segment-details-time']")
    List<WebElement> journeyTimings;

    @FindBy(xpath = "//div[span[@class='segment-details-stop-location-name']]")
    List<WebElement> stop;

    //traveller details

    String traveller_name = "div[class*='%s'] div[class*='traveller-name']";

    String traveller_etkt = "div[class*='%s'] div[class='ticket-container'] span";

    //frequent flyer

    @FindBy(className = "traveller-frequent-flyer")
    private WebElement frequentFlyerNumber;

    public HashMap<String,String> verifyTravellerDetailsAndRetrieveETKT(HashMap<String,String> testData){
        HashMap<String,String> e_tkt = new HashMap<>();
        String expected=null,actual=null,item=null;
        try {
            if (Integer.parseInt(testData.get("noOfAdult")) > 0) {
                int noOfAdult = Integer.parseInt(testData.get("noOfAdult"));
                for (int i=0;i<noOfAdult; i++) {
                    item = "adult"+(i+1)+" name";
                    expected = testData.get("adult" + (i + 1) + "Name").split(" ")[1]+" "+ testData.get("adult" + (i + 1) + "Name").split(" ")[2];
                    actual = TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_name, "ADT"))).get(i).getText();;
                    Assert.assertEquals(actual,expected);
                    e_tkt.put("adult"+(i+1)+"etkt",TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_etkt, "ADT"))).get(i).getText().replace("-",""));
                    if(i==0 && testData.get("frequentFlyerNumber")!=null){
                        Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(frequentFlyerNumber));
                        Assert.assertTrue(frequentFlyerNumber.getText().contains(testData.get("frequentFlyerNumber")));
                    }
                }
            }
            if (testData.get("noOfTeen")!=null) {
                int noOfTeen = Integer.parseInt(testData.get("noOfTeen"));
                for (int i=0;i<noOfTeen; i++) {
                    item = "teen"+(i+1)+" name";
                    expected = testData.get("teen" + (i + 1) + "Name").split(" ")[1]+" "+ testData.get("teen" + (i + 1) + "Name").split(" ")[2];
                    actual = TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_name, "YTH"))).get(i).getText();;
                    Assert.assertEquals(actual,expected);
                    e_tkt.put("teen"+(i+1)+"etkt",TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_etkt, "YTH"))).get(i).getText().replace("-",""));
                }
            }
            if (testData.get("noOfChild")!=null) {
                int noOfChild = Integer.parseInt(testData.get("noOfChild"));
                for (int i=0;i<noOfChild; i++) {
                    item = "child"+(i+1)+" name";
                    expected = testData.get("child" + (i + 1) + "Name").split(" ")[1]+" "+testData.get("child" + (i + 1) + "Name").split(" ")[2];
                    actual = TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_name, "CHD"))).get(i).getText();;
                    Assert.assertEquals(actual,expected);
                    e_tkt.put("child"+(i+1)+"etkt",TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_etkt, "CHD"))).get(i).getText());
                }
            }
            if (testData.get("noOfInfant")!=null) {
                int noOfInfant = Integer.parseInt(testData.get("noOfInfant"));
                for (int i=0;i<noOfInfant; i++) {
                    item = "infant"+(i+1)+" name";
                    expected = testData.get("infant" + (i + 1) + "Name").split(" ")[0] +" "+ testData.get("infant" + (i + 1) + "Name").split(" ")[1];
                    actual = TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_name, "INF"))).get(i).getText();;
                    Assert.assertEquals(actual,expected);
                    e_tkt.put("infant"+(i+1)+"etkt",TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_etkt, "INF"))).get(i).getText().replace("-",""));
                }
            }
            return e_tkt;
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,item+" - "+expected+" is displayed as "+actual);
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify traveller details section");
            throw e;
        }
    }

    public void verifyItinerarySection(HashMap<String,String> flightDetails){
        //verifies itinerary section
        String item=null, expected=null,actual=null;
        try{
            item = "outbound journey source - destination";
            expected = flightDetails.get("outboundSource").substring(0,flightDetails.get("outboundSource").indexOf('(')).toUpperCase()+"- "+flightDetails.get("outboundDestination").substring(0,flightDetails.get("outboundDestination").indexOf('(')).toUpperCase();
            actual = itinerary.get(0).getText().trim();
            Assert.assertTrue(expected.contains(actual));

            item = "outbound journey date";
            expected = flightDetails.get("outboundDate");
            actual = journeyDates.get(0).getText();
            Assert.assertEquals(actual,expected);

            item = "outbound journey duration";
            expected = flightDetails.get("outboundDuration");
            actual = journeyDuration.get(0).getText();
            Assert.assertEquals(actual,expected);

            item = "outbound departure airport";
            expected = flightDetails.get("outboundSource");
            actual = journeySource.get(0).getText();
            Assert.assertEquals(actual,expected);

            item = "outbound - direct/indirect";
            if(flightDetails.get("outboundStops").equals("0")) {
                expected = "DIRECT FLIGHT";
            }
            if(flightDetails.get("outboundStops").equals("1")) {
                expected = "1 STOP";
            }
            if(flightDetails.get("outboundStops").equals("2")) {
                expected = "2 STOP";
            }
            actual = journeySectors.get(0).getText();
            Assert.assertTrue(actual.contains(expected));

            item = "outbound ticket type";
            expected = flightDetails.get("outboundTicketType").toUpperCase();
            actual = ticketType.get(0).getText();
            Assert.assertTrue(actual.contains(expected));

            int flightCounter = 0;
            item = "outbound flight no. for sector 1";
            expected = flightDetails.get("outboundFlightNo1");
            actual = flightNos.get(flightCounter).getText();
            Assert.assertEquals(actual,expected);

            item = "outbound aircraft for sector 1";
            expected = flightDetails.get("outboundAircraft1");
            actual = aircraft.get(flightCounter++).getText();
            Assert.assertEquals(actual,expected);

            item = "outbound take off time for sector 1";
            expected = flightDetails.get("outboundSector1TakeOffTime");
            actual = journeyTimings.get(0).getText();
            Assert.assertEquals(actual,expected);

            item = "outbound landing time for sector 1";
            expected = flightDetails.get("outboundSector1LandingTime");
            actual = journeyTimings.get(1).getText();
            Assert.assertEquals(actual,expected);

            int stopCounter = 0;
            int timeCounter = 2;
            if(flightDetails.get("outboundNoOfFlights")!=null){
                int outboundNoOfStops = Integer.parseInt(flightDetails.get("outboundNoOfFlights"))-1;
                for(int i=1;i<=outboundNoOfStops;i++) {
                    item = "outbound stop";
                    expected = flightDetails.get("outboundStop"+i);
                    actual = stop.get(stopCounter++).getText();
                    Assert.assertEquals(actual,expected);

                    item = "outbound flight no. for sector "+(i+1);
                    expected = flightDetails.get("outboundFlightNo"+(i+1));
                    actual = flightNos.get(flightCounter).getText();
                    Assert.assertEquals(actual,expected);

                    item = "outbound aircraft for sector "+(i+1);
                    expected = flightDetails.get("outboundAircraft"+(i+1));
                    actual = aircraft.get(flightCounter).getText();
                    Assert.assertEquals(expected,actual);

                    item = "outbound take off time for sector"+(i+1);
                    expected = flightDetails.get("outboundSector"+(i+1)+"TakeOffTime");
                    actual = journeyTimings.get(timeCounter++).getText();
                    Assert.assertEquals(expected,actual);

                    item = "outbound landing time for sector"+(i+1);
                    expected = flightDetails.get("outboundSector"+(i+1)+"LandingTime");
                    actual = journeyTimings.get(flightCounter++).getText();
                    Assert.assertEquals(expected,actual);
                }
            }
            item = "outbound return airport";
            expected = flightDetails.get("outboundDestination");
            actual = journeyDestination.get(flightCounter-1).getText();
            Assert.assertEquals(actual,expected);
            if(flightDetails.get("inboundDate")!=null){
                item = "inbound source - destination";
                expected = flightDetails.get("inboundSource").split(" ")[0].toUpperCase()+" - "+flightDetails.get("inboundDestination").split(" ")[0].toUpperCase();
                actual = itinerary.get(1).getText().trim();
                Assert.assertEquals(actual,expected);

                item = "inbound journey date";
                expected = flightDetails.get("inboundDate");
                actual = journeyDates.get(1).getText();
                Assert.assertEquals(actual,expected);

                item = "inbound journey duration";
                expected = flightDetails.get("inboundDuration");
                actual = journeyDuration.get(1).getText();
                Assert.assertEquals(actual,expected);

                item = "inbound flight no. for sector 1";
                expected = flightDetails.get("inboundFlightNo1");
                actual = flightNos.get(flightCounter).getText();
                Assert.assertEquals(actual,expected);

                item = "inbound aircraft for sector 1";
                expected = flightDetails.get("inboundAircraft1");
                actual = aircraft.get(flightCounter).getText();
                Assert.assertEquals(expected,actual);

                item = "inbound departure airport";
                expected = flightDetails.get("inboundSource");
                actual = journeySource.get(flightCounter++).getText();
                Assert.assertEquals(actual,expected);

                item = "inbound return airport";
                expected = flightDetails.get("inboundDestination");
                actual = journeyDestination.get(journeyDestination.size()-1).getText();
                Assert.assertEquals(actual,expected);

                item = "inbound - direct/indirect";
                if(flightDetails.get("outboundStops").equals("0")) {
                    expected = "DIRECT FLIGHT";
                }
                if(flightDetails.get("outboundStops").equals("1")) {
                    expected = "1 STOP";
                }
                if(flightDetails.get("outboundStops").equals("2")) {
                    expected = "2 STOP";
                }
                actual = journeySectors.get(1).getText();
                Assert.assertTrue(actual.contains(expected));

                item = "inbound ticket type";
                expected = flightDetails.get("inboundTicketType").toUpperCase();
                actual = ticketType.get(1).getText();
                Assert.assertTrue(actual.contains(expected));

                item = "inbound take off time for sector 1";
                expected = flightDetails.get("inboundSector1TakeOffTime");
                actual = journeyTimings.get(timeCounter++).getText();
                Assert.assertEquals(expected,actual);

                item = "inbound landing time for sector 1";
                expected = flightDetails.get("inboundSector1LandingTime");
                actual = journeyTimings.get(timeCounter++).getText();
                Assert.assertEquals(expected,actual);

                if(flightDetails.get("inboundNoOfFlights")!=null){
                    int inboundNoOfStops = Integer.parseInt(flightDetails.get("inboundNoOfFlights"))-1;
                    for(int i=1;i<=inboundNoOfStops;i++) {
                        item = "inbound stop";
                        expected = flightDetails.get("inboundStop"+i);
                        actual = stop.get(stopCounter++).getText();
                        Assert.assertEquals(actual,expected);

                        item = "inbound flight no. for sector "+(i+1);
                        expected = flightDetails.get("inboundFlightNo"+(i+1));
                        actual = flightNos.get(flightCounter).getText();
                        Assert.assertEquals(actual,expected);

                        item = "inbound aircraft for sector "+(i+1);
                        expected = flightDetails.get("inboundAircraft"+(i+1));
                        actual = aircraft.get(flightCounter++).getText();
                        Assert.assertEquals(expected,actual);

                        item = "inbound take off time for sector"+(i+1);
                        expected = flightDetails.get("inboundSector"+(i+1)+"TakeOffTime");
                        actual = journeyTimings.get(timeCounter++).getText();
                        Assert.assertEquals(expected,actual);

                        item = "inbound landing time for sector"+(i+1);
                        expected = flightDetails.get("inboundSector"+(i+1)+"LandingTime");
                        actual = journeyTimings.get(timeCounter++).getText();
                        Assert.assertEquals(expected,actual);
                    }
                }
            }
            reportLogger.log(LogStatus.INFO, "successfully verified itinerary section after Manage booking - modify flight");
        }catch (AssertionError e) {
            System.out.println(item+" ==== "+expected+"-------"+actual);
            reportLogger.log(LogStatus.INFO, item+" displayed is incorrect: expected - "+expected+" and displayed - "+actual);
            throw e;
        }catch (Exception e) {
            reportLogger.log(LogStatus.INFO, "unable to verify itinerary section after manage booking - modify flight");
            throw e;
        }
    }

    public void verifySeatIsNotCarriedForward(HashMap<String,String>testData,HashMap<String,String> seatDetails){
        try{
            Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(seatForAllSectors));
            Assert.assertTrue(seatForAllSectors.size()==0);
            reportLogger.log(LogStatus.INFO,"successfully verified that seat is not carried forward after modify flight");
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"seat is getting carried forward after modifying flight in manage booking flow");
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify if seat is carried forward after modifying flight");
            throw e;
        }
    }


    public void verifyBagIsCarriedForward(HashMap<String,String> testData,HashMap<String,Double> basket){
        //verifies if bags booked are displayed in service section or not after modification in flight
        try{
            int noOfJourney = getNoOfJourney(testData);
            int noOfAdultsWithBag=0,noOfChildWithBag=0,noOfTeenWithBag=0;
            for(int i=1;i<=noOfJourney;i++){
                int countStart = 0;
                if(Integer.parseInt(testData.get("noOfAdult"))>0){
                    noOfAdultsWithBag = getNumberOfPaxWithBag_Golf_Ski(testData,"Adult","Bag",i);
                    if(noOfAdultsWithBag>0) {
                        verifyServiceCarriedForwardForEachPax(basket,"adult", i, testData, countStart,Integer.parseInt(testData.get("noOfAdult")),"Bag");
                        countStart = noOfAdultsWithBag;
                    }
                }
                if(testData.get("noOfChild")!=null && Integer.parseInt(testData.get("noOfChild"))>0){
                    noOfChildWithBag = getNumberOfPaxWithBag_Golf_Ski(testData,"Child","Bag",i);
                    if(noOfChildWithBag>0) {
                        verifyServiceCarriedForwardForEachPax(basket,"child", i, testData, countStart,Integer.parseInt(testData.get("noOfChild")),"Bag");
                        countStart += noOfChildWithBag;
                    }
                }
                if(testData.get("noOfTeen")!=null && Integer.parseInt(testData.get("noOfTeen"))>0){
                    noOfTeenWithBag = getNumberOfPaxWithBag_Golf_Ski(testData,"Teen","Bag",i);
                    if(noOfTeenWithBag>0) {
                        verifyServiceCarriedForwardForEachPax(basket,"teen", i, testData, countStart,Integer.parseInt(testData.get("noOfTeen")),"Bag");
                    }
                }
            }
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify service - bag section");
            throw e;
        }
    }

    public HashMap<String,Double> bagIsNotCarriedForward(HashMap<String,Double> basket){
        //makes hold baggage=0.0 in basket
        try{
            /*Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(serviceForBothJourney,10));
            reportLogger.log(LogStatus.INFO,"successfully verified that bag is not carried forward after route change");*/
            basket.put("hold baggage",0.0);
            return basket;
        }catch(Exception e){
            //reportLogger.log(LogStatus.INFO,"unable to verify if service - bag is not carried forward after route change");
            throw e;
        }
    }

    private void verifyServiceCarriedForwardForEachPax(HashMap<String,Double> basket,String paxType,int journeyNo,HashMap<String,String> testData,int countStart,int noOfPax,String service){
        //verifies whether bag/ski/golf is carried forward for each pax
        WebElement perJourneyServiceInfo = null;
        perJourneyServiceInfo = findList(basket,journeyNo,service);
        List<WebElement> serviceInfoForEachPax = perJourneyServiceInfo.findElements(By.tagName(serviceInfoForEachPaxTag));
        Object item = null,expected = null,actual = null;
        String journey = "";
        if(service.equals("Bag")){
            journey = findJourney(journeyNo,testData);
        }
        for(int paxCount=1,i=countStart;paxCount<=noOfPax;i++,paxCount++){
            if(testData.get(paxType+paxCount+service+journey)!=null){
                try {
                    WebElement serviceInfoForThisPax = serviceInfoForEachPax.get(i);
                    item = paxType+paxCount+" "+service.toLowerCase()+" for journey "+journeyNo;
                    if(service.equals("Bag")){
                        expected = getExpectedServiceMessage(testData.get(paxType+paxCount+service+journey),testData.get(journey.toLowerCase()+"TicketType"));
                    }
                    else {
                        expected = getExpectedServiceMessage(testData.get(paxType + paxCount + service + journey));
                    }
                    actual = serviceInfoForThisPax.findElement(By.className(serviceName)).getText().trim();
                    System.out.println(expected+" == "+actual);
                    Assert.assertTrue(expected.equals(actual));
                    reportLogger.log(LogStatus.INFO,"successfully verified that "+paxType+paxCount+" "+service+"  - "+testData.get(paxType + paxCount + service + journey)+" is carried forward");
                }catch(AssertionError ae){
                    System.out.println(item+" == "+expected+" --- "+actual);
                    reportLogger.log(LogStatus.INFO,item+" expected - "+expected+" but displayed is "+actual+" after modification of flight");
                    throw ae;
                }
            }
        }


    }

    private String retrievePNR(){
        //retrieves PNR
        String pnr = null;
        try{
            actionUtility.waitForElementVisible(reservationNo,90);
            pnr = reservationNo.getText();
            System.out.println("PNR - "+pnr);
            reportLogger.log(LogStatus.INFO,"Reservation Number is "+pnr);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to retrieve PNR");
            throw e;
        }
        return pnr;
    }

    private String getReservationName(){
        //gets the passenger name from MB conf page
        String passengerName= null;
        actionUtility.waitForElementVisible(ticketInformation,90);
        return reservationName.getText();
    }

    public void verifyPNRAndReservationName(String pnr, String reservationName){
        String actual=null,expected = null,item=null;
        try{
            item = "PNR";
            actual= retrievePNR();
            expected = pnr;
            Assert.assertTrue(actual.equals(expected));
            reportLogger.log(LogStatus.INFO,"PNR displayed- "+actual+" is matching with the PNR generated in booking flow");

            item = "reservation name";
            expected = reservationName;
            actual = getReservationName();
            Assert.assertTrue(actual.equals(expected));
            reportLogger.log(LogStatus.INFO,"reservation name displayed is matching with the expected - " +reservationName);
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,item+" displayed - "+actual+" doesn't match with expected - "+expected);
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify the PNR and reservation name in MB confirmation page");
            throw e;

        }
    }

    public void navigateToCheckInFlow(){
        try{
            actionUtility.waitForElementVisible(checkInLink,40);
            actionUtility.click(checkInLink);
            actionUtility.waitForPageLoad(90);
            reportLogger.log(LogStatus.INFO,"successfully navigated to checkin page from retrieval page");
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to checkin page from retrieval page");
            throw e;
        }
    }

    private int getNoOfJourney(HashMap<String,String> testData){
        int noOfJourney = 1;
        if(testData.get("returnDateOffset")!=null){
            noOfJourney = 2;
        }
        return noOfJourney;
    }

    private String findJourney(int journeyNo,HashMap<String,String> testData){
        String journey = "Depart";
        if(journeyNo==2){
            if(testData.get("departTicketType")!=testData.get("returnTicketType"))
                journey = "Return";
        }
        return journey;
    }

    private int getNumberOfPaxWithServiceAddedInBookingFlow(HashMap<String,String> testData, String paxType, String service) {
        int paxWithServiceCount = 0;
        String journey = "";
        int noOfPax = Integer.parseInt(testData.get("noOf"+paxType));
        for(int i=1;i<=noOfPax;i++){
            if(testData.get(paxType.toLowerCase()+i+service+journey)!=null){
                paxWithServiceCount++;
            }
        }
        System.out.println("NO OF "+paxType+" WITH "+service+" - "+paxWithServiceCount);
        return paxWithServiceCount;
    }


    private int getNumberOfPaxWithBag_Golf_Ski(HashMap<String,String> testData,String paxType,String service,int... journeyNo) {
        int paxWithServiceCount = 0;
        String journey = "";
        if(journeyNo.length>0) {
            journey = findJourney(journeyNo[0], testData);
        }
        int noOfPax = Integer.parseInt(testData.get("noOf"+paxType));
        for(int i=1;i<=noOfPax;i++){
            if(testData.get(paxType.toLowerCase()+i+service+journey)!=null){
                paxWithServiceCount++;
            }
            else if(testData.get("mb_"+paxType+i+service+journey)!=null){
                paxWithServiceCount++;
            }
        }
        System.out.println("NO OF "+paxType+" WITH "+service+" - "+paxWithServiceCount);
        return paxWithServiceCount;
    }

    private WebElement findList(HashMap<String,Double> basket,int journey,String service){
        int i=0;
        if(journey==1) {
            if (service.equals("SpecialAssistance"))
                return serviceForBothJourney.get(0);

            if (service.equals("Bag")) {
                if(basket.get("special assistance")!=null)
                    return serviceForBothJourney.get(1);
                else
                    return serviceForBothJourney.get(0);
            }

            if (service.equals("Golf")){
                if(basket.get("special assistance")!=null && basket.get("hold baggage")!=null){
                    return serviceForBothJourney.get(2);
                }
                else if(basket.get("special assistance")==null && basket.get("hold baggage")==null){
                    return serviceForBothJourney.get(0);
                }
                else if(basket.get("special assistance")==null || basket.get("hold baggage")==null){
                    return serviceForBothJourney.get(1);
                }
            }

            if(service.equals("SkiSnow")) {
                if (basket.get("special assistance") == null && basket.get("golf clubs") == null && basket.get("hold baggage") == null) {
                    return serviceForBothJourney.get(0);
                }
                if(basket.get("hold baggage")==null){
                    if(basket.get("special assistance")!=null && basket.get("golf clubs")!=null){
                        return serviceForBothJourney.get(2);
                    }
                    else if(basket.get("special assistance")==null || basket.get("golf clubs")==null){
                        return serviceForBothJourney.get(1);
                    }
                }
                if(basket.get("special assistance")==null){
                    if(basket.get("hold baggage")!=null && basket.get("golf clubs")!=null){
                        return serviceForBothJourney.get(2);
                    }
                    else if(basket.get("hold baggage")==null || basket.get("golf clubs")==null){
                        return serviceForBothJourney.get(1);
                    }
                }

                if(basket.get("golf clubs")==null){
                    if(basket.get("hold baggage")!=null && basket.get("special assistance")!=null){
                        return serviceForBothJourney.get(2);
                    }
                    else if(basket.get("hold baggage")==null || basket.get("special assistance")==null){
                        return serviceForBothJourney.get(1);
                    }
                }
            }
        }
        if(journey==2){
            if(service.equals("SpecialAssistance")){
                if(basket.get("golf clubs")!=null && basket.get("skis")!=null && basket.get("hold baggage")!=null){
                    return serviceForBothJourney.get(4);
                }
                if(basket.get("hold baggage")==null) {
                    if (basket.get("golf clubs") != null && basket.get("skis") != null) {
                        return serviceForBothJourney.get(3);
                    } else if (basket.get("golf clubs") == null && basket.get("skis") == null) {
                        return serviceForBothJourney.get(1);
                    } else if (basket.get("golf clubs") == null || basket.get("skis") == null) {
                        return serviceForBothJourney.get(2);
                    }
                }
            }
            if (service.equals("Golf")){
                if(basket.get("golf clubs")!=null && basket.get("skis")!=null && basket.get("special assistance")!=null){
                    return serviceForBothJourney.get(6);
                }
                if(basket.get("special assistance")==null) {
                    if (basket.get("skis") != null && basket.get("hold baggage") != null) {
                        return serviceForBothJourney.get(4);
                    } else if (basket.get("golf clubs") == null && basket.get("hold baggage") == null) {
                        return serviceForBothJourney.get(1);
                    } else if (basket.get("golf clubs") == null || basket.get("skis") == null) {
                        return serviceForBothJourney.get(2);
                    }
                }
            }
            if (service.equals("SkiSnow")){
                if(basket.get("golf clubs")!=null && basket.get("skis")!=null && basket.get("special assistance")!=null){
                    return serviceForBothJourney.get(7);
                }
                if(basket.get("special assistance")==null) {

                    if (basket.get("golf clubs") != null && basket.get("hold baggage") != null) {
                        return serviceForBothJourney.get(5);
                    } else if (basket.get("golf clubs") == null && basket.get("hold baggage") == null) {
                        return serviceForBothJourney.get(1);
                    } else if (basket.get("golf clubs") == null || basket.get("skis") == null) {
                        return serviceForBothJourney.get(3);
                    }
                }
            }
            if (service.equals("Bag")){
                if(basket.get("golf clubs")!=null && basket.get("skis")!=null && basket.get("special assistance")!=null){
                    return serviceForBothJourney.get(5);
                }
                if(basket.get("special assistance")==null) {
                    if (basket.get("skis") != null && basket.get("golf clubs") != null) {
                        return serviceForBothJourney.get(3);
                    } else if (basket.get("golf clubs") == null && basket.get("skis") == null) {
                        return serviceForBothJourney.get(1);
                    } else if (basket.get("golf clubs") == null || basket.get("skis") == null) {
                        return serviceForBothJourney.get(2);
                    }
                }
                if(basket.get("skis")==null) {
                    if (basket.get("special assistance") != null && basket.get("golf clubs") != null) {
                        return serviceForBothJourney.get(4);
                    } else if (basket.get("golf clubs") == null && basket.get("special assistance") == null) {
                        return serviceForBothJourney.get(1);
                    } else if (basket.get("golf clubs") == null) {
                        return serviceForBothJourney.get(3);
                    }
                }
                if(basket.get("golf clubs")==null) {
                    if (basket.get("special assistance") != null && basket.get("skis") != null)
                        return serviceForBothJourney.get(4);
                }
            }
        }
        return null;
    }


    private String getExpectedServiceMessage(String bagType,String... ticketType){
        if (bagType.equals("23kg")) {
            return "1 × 23kg baggage";
        } else if((bagType.equals("46kg"))) {
            if(ticketType[0].equals("just fly"))
                return "2 × 23kg baggage";
            else
                return "1 × 23kg baggage";
        }
        else if(bagType.equals("ski")){
            return "1 × Skis";
        }
        else if(bagType.equals("snowboard")){
            return "1 × Snowboard";
        }
        else{
            return "1 × Golf Equipment";
        }
    }

    private HashMap<String,String> verifySkiForEachPaxAddedInBookingFlow(HashMap<String,Double> basket,String paxType,int journeyNo,HashMap<String,String> testData,int countStart,HashMap<String,String> emd,int noOfPax,String service){
        WebElement perJourneyServiceInfo = null;
        perJourneyServiceInfo = findList(basket,journeyNo,service);
        List<WebElement> serviceInfoForEachPax = perJourneyServiceInfo.findElements(By.tagName(serviceInfoForEachPaxTag));
        WebElement serviceInfoForThisPax = null;
        Object item = null,expected = null,actual = null;
        try{
            for(int paxCount=1,i=countStart;paxCount<=noOfPax;i++,paxCount++) {
                if (testData.get(paxType.toLowerCase() + paxCount + service) != null) {
                    serviceInfoForThisPax = serviceInfoForEachPax.get(i);
                    item = paxType.toLowerCase() + paxCount + " " + service.toLowerCase() + " for journey " + journeyNo;
                    if (service.equals("Bag")) {
                        expected = getExpectedServiceMessage(testData.get(paxType.toLowerCase() + paxCount + service));
                    } else {
                        expected = getExpectedServiceMessage(testData.get(paxType.toLowerCase() + paxCount + service));
                    }
                    actual = serviceInfoForThisPax.findElement(By.className(serviceName)).getText().trim();
                    System.out.println(expected + " == " + actual);
                    Assert.assertTrue(expected.equals(actual));
                    item = paxType.toLowerCase() + paxCount + " " + service.toLowerCase() + " price for journey " + journeyNo;
                    expected = "Already paid";
                    actual = serviceInfoForThisPax.findElement(By.className(serviceCost)).getText().trim();
                    System.out.println(expected + " == " + actual);
                    Assert.assertTrue(expected.equals(actual));
                    item = "EMD generated in booking flow";
                    expected = emd.get(paxType.toLowerCase() + paxCount + service+"sector"+journeyNo);
                    actual = serviceInfoForThisPax.findElement(By.className(serviceEMD)).getText().trim();
                    Assert.assertTrue(expected.equals(actual));
                    reportLogger.log(LogStatus.INFO, "successfully verified " + paxType + paxCount + " " + service + " details displayed - " + testData.get(paxType + paxCount + service));
                }
            }
        }catch(AssertionError ae){
            System.out.println(item+" == "+expected+" --- "+actual);
            reportLogger.log(LogStatus.INFO,item+" expected - "+expected+" but displayed is "+actual);
            throw ae;
        }
        return emd;
    }

    private HashMap<String,String> verifyBag_Ski_Golf_ForEachJourneyForEachPax(HashMap<String,Double> basket,String paxType,int journeyNo,HashMap<String,String> testData,int countStart,HashMap<String,String> emd,int noOfPax,String service){
        WebElement perJourneyServiceInfo = null;
        perJourneyServiceInfo = findList(basket,journeyNo,service);
        List<WebElement> serviceInfoForEachPax = perJourneyServiceInfo.findElements(By.tagName(serviceInfoForEachPaxTag));
        Object item = null,expected = null,actual = null;
        String journey = "";
        WebElement serviceInfoForThisPax = null;
        if(service.equals("Bag")){
            journey = findJourney(journeyNo,testData);
        }
        for(int paxCount=1,i=countStart;paxCount<=noOfPax;i++,paxCount++){
            try {
                if(testData.get("mb_"+paxType+paxCount+service+journey)!=null){
                    serviceInfoForThisPax = serviceInfoForEachPax.get(i);
                    item = paxType+paxCount+" "+service.toLowerCase()+" for journey "+journeyNo;
                    if(service.equals("Bag")){
                        expected = getExpectedServiceMessage(testData.get("mb_"+paxType+paxCount+service+journey),testData.get(journey.toLowerCase()+"TicketType"));
                    }
                    else {
                        expected = getExpectedServiceMessage(testData.get("mb_"+paxType + paxCount + service + journey));
                    }
                    actual = serviceInfoForThisPax.findElement(By.className(serviceName)).getText().trim();
                    System.out.println(expected+" == "+actual);
                    Assert.assertTrue(expected.equals(actual));

                    item = paxType+paxCount+" "+service.toLowerCase()+" price for journey "+journeyNo;
                    if(service.equals("Bag")) {
                        expected = basket.get("mb_"+paxType + paxCount + journey + "BagCost");
                    }
                    else{
                        expected = 30.00; //price for ski and golf
                    }
                    actual = CommonUtility.convertStringToDouble(serviceInfoForThisPax.findElement(By.className(serviceCost)).getText().trim());
                    System.out.println(expected+" == "+actual);
                    Assert.assertTrue(expected.equals(actual));
                    /*if(service!="Bag"){
                        journey = findJourney(journeyNo,testData);
                    }*/
                    emd.put(paxType + paxCount + service +"sector"+journeyNo, serviceInfoForThisPax.findElement(By.className(serviceEMD)).getText().trim());
                    try {
                        Assert.assertTrue(emd.get(paxType + paxCount + service + "sector"+journeyNo) != null);
                        System.out.println("EMD for "+paxType + paxCount + service + journey+" = "+emd.get(paxType + paxCount + service + "sector"+journeyNo));
                    }catch(AssertionError ae){
                        reportLogger.log(LogStatus.INFO,"unable to capture emd generated for "+paxType + paxCount+" for service "+service);
                        throw ae;
                    }
                    reportLogger.log(LogStatus.INFO,"successfully captured EMD - "+emd.get(paxType+paxCount+service+"sector"+journeyNo)+" generated for "+service+" added in manage booking - travel extras for pax - "+paxType + paxCount);

                }else if (testData.get(paxType.toLowerCase() + paxCount + service + journey) != null && service!="SkiSnow") {
                    serviceInfoForThisPax = serviceInfoForEachPax.get(i);
                    item = paxType.toLowerCase() + paxCount + " " + service.toLowerCase() + " for journey " + journeyNo;
                    if (service.equals("Bag")) {
                        expected = getExpectedServiceMessage(testData.get(paxType.toLowerCase() + paxCount + service + journey), testData.get(journey.toLowerCase() + "TicketType"));
                    } else {
                        expected = getExpectedServiceMessage(testData.get(paxType.toLowerCase() + paxCount + service + journey));
                    }
                    actual = serviceInfoForThisPax.findElement(By.className(serviceName)).getText().trim();
                    System.out.println(expected + " == " + actual);
                    Assert.assertTrue(expected.equals(actual));
                    item = paxType.toLowerCase() + paxCount + " " + service.toLowerCase() + " price for journey " + journeyNo;
                    expected = "Already paid";
                    actual = serviceInfoForThisPax.findElement(By.className(serviceCost)).getText().trim();
                    System.out.println(expected + " == " + actual);
                    Assert.assertTrue(expected.equals(actual));
                    item = "EMD generated in booking flow";
                    expected = emd.get(paxType.toLowerCase() + paxCount + service + "sector"+journeyNo);
                    actual = serviceInfoForThisPax.findElement(By.className(serviceEMD)).getText().trim();
                    System.out.println(expected+" == "+actual);
                    Assert.assertTrue(expected.equals(actual));
                    reportLogger.log(LogStatus.INFO, "successfully verified " + paxType + paxCount + " " + service + " details displayed - " + testData.get(paxType.toLowerCase() + paxCount + service + journey));
                }
            }catch(AssertionError ae){
               System.out.println(item+" == "+expected+" --- "+actual);
               reportLogger.log(LogStatus.INFO,item+" expected - "+expected+" but displayed is "+actual);
               throw ae;
            }
        }
        return emd;
    }

    public HashMap<String,String> verifyBagsAndRetrieveEMD(HashMap<String,String> testData,HashMap<String,Double> basket,HashMap<String,String>... emd){
        HashMap<String,String> bagEMD = new HashMap<>();
        if(emd.length>0){
            bagEMD = emd[0];
        }
        int emdCount = 0;
        try{
            int noOfJourney = getNoOfJourney(testData);
            int noOfAdultsWithBag=0,noOfChildWithBag=0,noOfTeenWithBag=0;

            for(int i=1;i<=noOfJourney;i++){
                int countStart = 0;
                if(testData.get("noOfChild")!=null && Integer.parseInt(testData.get("noOfChild"))>0){
                    noOfChildWithBag = getNumberOfPaxWithBag_Golf_Ski(testData,"Child","Bag",i);
                    if(noOfChildWithBag>0) {
                        bagEMD = verifyBag_Ski_Golf_ForEachJourneyForEachPax(basket,"Child", i, testData, countStart, bagEMD,Integer.parseInt(testData.get("noOfChild")),"Bag");
                        countStart = noOfChildWithBag;
                    }
                }
                if(Integer.parseInt(testData.get("noOfAdult"))>0){
                    noOfAdultsWithBag = getNumberOfPaxWithBag_Golf_Ski(testData,"Adult","Bag",i);
                    if(noOfAdultsWithBag>0) {
                        bagEMD = verifyBag_Ski_Golf_ForEachJourneyForEachPax(basket,"Adult", i, testData, countStart, bagEMD,Integer.parseInt(testData.get("noOfAdult")),"Bag");
                        countStart += noOfAdultsWithBag;
                    }
                }
                if(testData.get("noOfTeen")!=null && Integer.parseInt(testData.get("noOfTeen"))>0){
                    noOfTeenWithBag = getNumberOfPaxWithBag_Golf_Ski(testData,"Teen","Bag",i);
                    if(noOfTeenWithBag>0) {
                        bagEMD = verifyBag_Ski_Golf_ForEachJourneyForEachPax(basket,"Teen", i, testData, countStart, bagEMD,Integer.parseInt(testData.get("noOfTeen")),"Bag");
                        countStart += noOfTeenWithBag;
                    }
                }
                emdCount += countStart;
            }
            Assert.assertTrue(bagEMD.size()>0);
            Assert.assertTrue(bagEMD.size()==emdCount);
            reportLogger.log(LogStatus.INFO,"EMD generated for bag - "+bagEMD);
            System.out.println("EMD generated for bag after MB- "+bagEMD);
            return bagEMD;
        }catch(AssertionError e){
            System.out.println("BAG EMD size - "+emdCount+" --- "+bagEMD.size());
            reportLogger.log(LogStatus.INFO,"unable to retrieve EMD generated for bag");
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify service - bag section");
            throw e;
        }
    }

    private HashMap<String,String> verifySeatAdded_ModifiedinMB(String paxType,int sectorNo,HashMap<String,String> seatDetails,int countStart,HashMap<String,Double> basket,HashMap<String,String> seatEMD,int noOfPax){
        WebElement sectorSeatInfo = seatForAllSectors.get(sectorNo-1);
        List<WebElement> seatInfoForEachPax = sectorSeatInfo.findElements(By.tagName(serviceInfoForEachPaxTag));
        Object item = null,expected = null,actual = null;
        try {
            for (int paxCount = 1, i = countStart; paxCount <= noOfPax; i++, paxCount++) {
                if (seatDetails.get("mb_" + paxType + paxCount + "SeatNoForSector" + sectorNo) != null) {
                    WebElement seatInfoForThisPax = seatInfoForEachPax.get(i);
                    item = paxType + paxCount + " seat number for sector " + sectorNo;
                    expected = seatDetails.get("mb_" + paxType + paxCount + "SeatNoForSector" + sectorNo);
                    actual = seatInfoForThisPax.findElement(By.className(serviceName)).getText().trim().split(" ")[0];
                    System.out.println(expected + " == " + actual);
                    Assert.assertTrue(actual.equals(expected));

                    item = paxType + paxCount + " seat price for sector " + sectorNo;
                    expected = basket.get("mb_" + paxType + paxCount + "SeatSector" + sectorNo + "Price");
                    if (expected.equals(0.0)) {
                        expected = "Free of charge";
                        actual = seatInfoForThisPax.findElement(By.className(serviceCost)).getText().trim();
                    } else {
                        actual = CommonUtility.convertStringToDouble(seatInfoForThisPax.findElement(By.className(serviceCost)).getText().trim());
                    }
                    System.out.println(expected + " == " + actual);
                    Assert.assertTrue(expected.equals(actual));


                    /*if (seatEMD.get(paxType.toLowerCase() + paxCount + "SeatEMDForSector" + sectorNo) != "nil" && seatEMD.get(paxType.toLowerCase() + paxCount + "SeatEMDForSector" + sectorNo)!=null) {
                        try {
                            item = "seat emd for ";
                            expected = seatEMD.get(paxType + paxCount + "SeatEMDForSector" + sectorNo);
                            actual = seatEMDForPax.split(",")[0];
                            System.out.println(expected + " == " + actual);
                            Assert.assertTrue(expected.equals(actual));
                        } catch (AssertionError ae) {
                            reportLogger.log(LogStatus.INFO, "seat EMD generated in booking flow for passenger " + paxType + paxCount + " is " + expected + " but the EMD displayed after changing the seat for same passenger is " + actual);
                            throw ae;
                        }
                    }*/
                    if (expected.equals("Free of charge")) {
                        Assert.assertFalse(actionUtility.verifyIfDependentElementIsDisplayed(seatInfoForThisPax,serviceEMD,"className"));
                        seatEMD.put(paxType + paxCount + "SeatEMDForSector" + sectorNo, "nil");
                    } else {
                        try {
                            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(seatInfoForThisPax.findElement(By.className(serviceEMD))));
                            seatEMD.put(paxType + paxCount + "SeatEMDForSector" + sectorNo, seatInfoForThisPax.findElement(By.className(serviceEMD)).getText().trim());
                        } catch (AssertionError ae) {
                            reportLogger.log(LogStatus.INFO, "EMD is not generated for seat added for " + paxType + paxCount + " for sector " + sectorNo);
                            throw ae;
                        }
                    }
                }
            }
        }catch(AssertionError ae){
            System.out.println(item+" == "+expected+" --- "+actual);
            reportLogger.log(LogStatus.INFO,item+" expected - "+expected+" but displayed seat number is "+actual);
            throw ae;
        }
        return seatEMD;
    }

    private HashMap<String,String> verifySeatForEachSectorForEachPax(String paxType,int sectorNo,HashMap<String,String> seatDetails,int countStart,HashMap<String,Double> basket,HashMap<String,String> seatEMD,int noOfPax){
        WebElement sectorSeatInfo = seatForAllSectors.get(sectorNo-1);
        List<WebElement> seatInfoForEachPax = sectorSeatInfo.findElements(By.tagName(serviceInfoForEachPaxTag));
        Object item = null,expected = null,actual = null;
        for(int paxCount=1,i=countStart;i<=noOfPax;i++,paxCount++){
            try{
                 if(seatDetails.get(paxType.toLowerCase()+paxCount+"SeatNoForSector"+sectorNo)!=null && seatDetails.get("mb_"+paxType+paxCount+"SeatNoForSector"+sectorNo)==null) {
                    System.out.println("verifying seat " + seatDetails.get(paxType.toLowerCase() + paxCount + "SeatNoForSector" + sectorNo) + " for pax " + paxType.toLowerCase() + paxCount + " for sector ");
                    WebElement seatInfoForThisPax = seatInfoForEachPax.get(i);

                    item = paxType + paxCount + " seat number for sector " + sectorNo;
                    expected = seatDetails.get(paxType.toLowerCase() + paxCount + "SeatNoForSector" + sectorNo);
                    actual = seatInfoForThisPax.findElement(By.className(serviceName)).getText().trim();
                    System.out.println(expected + " == " + actual);
                    Assert.assertTrue(expected.equals(actual));

                    item = paxType + paxCount + " seat price for sector " + sectorNo;
                    expected = "Already paid";
                    actual = CommonUtility.convertStringToDouble(seatInfoForThisPax.findElement(By.className(serviceCost)).getText().trim());
                    System.out.println(expected + " == " + actual);
                    Assert.assertTrue(expected.equals(actual));

                    if (seatEMD.get(paxType.toLowerCase() + paxCount + "SeatEMDForSector" + sectorNo) != "nil") {
                        item = paxType + paxCount + " emd for sector " + sectorNo;
                        expected = seatEMD.get(paxType.toLowerCase() + paxCount + "SeatEMDForSector" + sectorNo);
                        actual = seatInfoForThisPax.findElement(By.className(serviceEMD)).getText().trim();
                        System.out.println(expected + " == " + actual);
                        Assert.assertTrue(expected.equals(actual));
                    }
                    reportLogger.log(LogStatus.INFO, "successfully verified " + paxType + paxCount + " seat number displayed - " + seatDetails.get(paxType + paxCount + "SeatNoForSector" + sectorNo));
                }
            }catch(AssertionError ae){
                System.out.println(item+" == "+expected+" --- "+actual);
                reportLogger.log(LogStatus.INFO,item+" expected - "+expected+" but displayed seat number is "+actual);
                throw ae;
            }
        }
        return seatEMD;
    }

    public HashMap<String,String> verifySeatAllocationAndRetrieveEMD(HashMap<String,String> testData,HashMap<String,String> seatDetails,HashMap<String,Double> basket,HashMap<String,String>... emd){
        //verifies that seat displayed on confirmation is same as that selected on seat/extras page
        //retrieves EMD generated for seat
        HashMap<String,String> seatEMD = new HashMap<>();
        if(emd.length>0){
            seatEMD = emd[0];
        }
        int emdCount = seatEMD.size();
        try{
            int totalNoOfSectors = Integer.parseInt(testData.get("departNoOfStops"))+1;
            if(testData.get("mb_DepartNoOfStops")!=null){
                totalNoOfSectors = Integer.parseInt(testData.get("mb_DepartNoOfStops"))+1;
            }
            if(testData.get("mb_ReturnDateOffset")!=null){
                totalNoOfSectors += Integer.parseInt(testData.get("mb_ReturnNoOfStops"))+1;
            }
            else if(testData.get("returnDateOffset")!=null){
                totalNoOfSectors += Integer.parseInt(testData.get("returnNoOfStops"))+1;
            }
            int noOfAdultsWithSeat=0,noOfChildWithSeat=0,noOfTeenWithSeat=0;
            for(int i=1;i<=totalNoOfSectors;i++){
                int countStart = 0;
                if(Integer.parseInt(testData.get("noOfAdult"))>0 && seatDetails.get("noOfadultwithSeatForSector")!=null){
                    noOfAdultsWithSeat = Integer.parseInt(seatDetails.get("noOfadultwithSeatForSector"+i));
                    if(noOfAdultsWithSeat>0) {
                        seatEMD = verifySeatForEachSectorForEachPax("Adult", i, seatDetails, countStart, basket, seatEMD,Integer.parseInt(testData.get("noOfAdult")));
                        countStart = noOfAdultsWithSeat;
                    }
                }
                if(testData.get("noOfChild")!=null && Integer.parseInt(testData.get("noOfChild"))>0 && seatDetails.get("noOfchildwithSeatForSector")!=null){
                    noOfChildWithSeat = Integer.parseInt(seatDetails.get("noOfchildwithSeatForSector"+i));
                    if(noOfChildWithSeat>0) {
                        seatEMD = verifySeatForEachSectorForEachPax("Child", i, seatDetails, countStart, basket, seatEMD,Integer.parseInt(testData.get("noOfChild")));
                        countStart += noOfChildWithSeat;
                    }
                }
                if(testData.get("noOfTeen")!=null && Integer.parseInt(testData.get("noOfTeen"))>0 && seatDetails.get("noOfteenwithSeatForSector")!=null){
                    noOfTeenWithSeat = Integer.parseInt(seatDetails.get("noOfteenwithSeatForSector"+i));
                    if(noOfTeenWithSeat>0) {
                        seatEMD = verifySeatForEachSectorForEachPax("Teen", i, seatDetails, countStart, basket, seatEMD,Integer.parseInt(testData.get("noOfTeen")));
                        countStart += noOfTeenWithSeat;
                    }
                }
                //for seats added or modified in MB
                if(Integer.parseInt(testData.get("noOfAdult"))>0 && seatDetails.get("noOfadultwithSeatInMBForSector"+i)!=null){
                    noOfAdultsWithSeat = Integer.parseInt(seatDetails.get("noOfadultwithSeatInMBForSector"+i));
                    System.out.println("NO OF ADULT WITH SEAT "+noOfAdultsWithSeat);
                    if(noOfAdultsWithSeat>0) {
                        seatEMD = verifySeatAdded_ModifiedinMB("Adult", i, seatDetails, countStart, basket, seatEMD,Integer.parseInt(testData.get("noOfAdult")));
                        countStart = noOfAdultsWithSeat;
                    }
                }
                if(testData.get("noOfChild")!=null && Integer.parseInt(testData.get("noOfChild"))>0 && seatDetails.get("noOfchildwithSeatInMBForSector"+i)!=null){
                    noOfChildWithSeat = Integer.parseInt(seatDetails.get("noOfchildwithSeatInMBForSector"+i));
                    System.out.println("NO OF CHILDREN WITH SEAT "+noOfChildWithSeat);
                    if(noOfChildWithSeat>0) {
                        seatEMD = verifySeatAdded_ModifiedinMB("Child", i, seatDetails, countStart, basket, seatEMD,Integer.parseInt(testData.get("noOfChild")));
                        countStart += noOfChildWithSeat;
                    }
                }
                if(testData.get("noOfTeen")!=null && Integer.parseInt(testData.get("noOfTeen"))>0 && seatDetails.get("noOfteenwithSeatInMBForSector"+i)!=null){
                    noOfTeenWithSeat = Integer.parseInt(seatDetails.get("noOfteenwithSeatInMBForSector"+i));
                    System.out.println("NO OF TEEN WITH SEAT "+noOfTeenWithSeat);
                    if(noOfTeenWithSeat>0) {
                        seatEMD = verifySeatAdded_ModifiedinMB("Teen", i, seatDetails, countStart, basket, seatEMD,Integer.parseInt(testData.get("noOfTeen")));
                        countStart += noOfTeenWithSeat;
                    }
                }
                emdCount += countStart;
            }
            Assert.assertTrue(seatEMD.size()>0);
            Assert.assertTrue(seatEMD.size()==emdCount);
            reportLogger.log(LogStatus.INFO,"EMD generated for seat - "+seatEMD);
            System.out.println("EMD generated for seat after MB- "+seatEMD);
            return seatEMD;
        }catch(AssertionError e){
            System.out.println("SEAT EMD size - "+emdCount+" --- "+seatEMD.size());
            reportLogger.log(LogStatus.INFO,"unable to verify seat EMDs generated");
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify seat allocations on confirmation page");
            throw e;
        }
    }


    public HashMap<String,String> verifyGolf_SkiAndRetrieveEMD(HashMap<String,String> testData, HashMap<String,Double> basket,String service,HashMap<String,String>... previousEMD){
        HashMap<String,String> emd = new HashMap<>();
        if(previousEMD.length>0){
            emd = previousEMD[0];
        }
        int emdCount = 0;
        try{
            if(service.equals("Ski")){
                service = "SkiSnow";
            }
            int noOfJourney = getNoOfJourney(testData);
            int noOfAdultsWithService =0, noOfChildWithService=0,noOfTeenWithService=0;
            int noOfAdultsWithServiceInBookingFlow =0, noOfChildWithServiceInBookingFlow=0,noOfTeenWithServiceInBookingFlow=0;
            /*if(service.equals("SkiSnow")) {
                if (Integer.parseInt(testData.get("noOfAdult")) > 0) {
                    noOfAdultsWithServiceInBookingFlow = getNumberOfPaxWithServiceAddedInBookingFlow(testData, "Adult", service);
                }
                if (testData.get("noOfChild") != null && Integer.parseInt(testData.get("noOfChild")) > 0) {
                    noOfChildWithServiceInBookingFlow = getNumberOfPaxWithServiceAddedInBookingFlow(testData, "Child", service);
                }
                if (testData.get("noOfTeen") != null && Integer.parseInt(testData.get("noOfTeen")) > 0) {
                    noOfTeenWithServiceInBookingFlow = getNumberOfPaxWithServiceAddedInBookingFlow(testData, "Teen", service);
                }
            }*/
            if (Integer.parseInt(testData.get("noOfAdult")) > 0) {
                noOfAdultsWithService = getNumberOfPaxWithBag_Golf_Ski(testData, "Adult", service);
            }
            if (testData.get("noOfChild") != null && Integer.parseInt(testData.get("noOfChild")) > 0) {
                noOfChildWithService = getNumberOfPaxWithBag_Golf_Ski(testData, "Child", service);
            }
            if (testData.get("noOfTeen") != null && Integer.parseInt(testData.get("noOfTeen")) > 0) {
                noOfTeenWithService = getNumberOfPaxWithBag_Golf_Ski(testData, "Teen", service);
            }
            for(int i=1;i<=noOfJourney;i++){
                int countStart = 0;
                /*if(service.equals("SkiSnow")) {
                    if (noOfAdultsWithServiceInBookingFlow > 0) {
                        emd = verifySkiForEachPaxAddedInBookingFlow(basket, "Adult", i, testData, countStart, emd, Integer.parseInt(testData.get("noOfAdult")), service);
                        countStart = noOfAdultsWithService;
                    }
                    if (noOfChildWithServiceInBookingFlow > 0) {
                        emd = verifySkiForEachPaxAddedInBookingFlow(basket, "Child", i, testData, countStart, emd, Integer.parseInt(testData.get("noOfChild")), service);
                        countStart += noOfChildWithService;
                    }
                    if (noOfTeenWithServiceInBookingFlow > 0) {
                        emd = verifySkiForEachPaxAddedInBookingFlow(basket, "Teen", i, testData, countStart, emd, Integer.parseInt(testData.get("noOfTeen")), service);
                        countStart += noOfTeenWithService;
                    }
                }*/
                if(noOfAdultsWithService>0) {
                    emd = verifyBag_Ski_Golf_ForEachJourneyForEachPax(basket,"Adult", i, testData, countStart, emd,Integer.parseInt(testData.get("noOfAdult")),service);
                    countStart += noOfAdultsWithService;
                }
                if(noOfChildWithService>0) {
                    emd = verifyBag_Ski_Golf_ForEachJourneyForEachPax(basket,"Child", i, testData, countStart, emd,Integer.parseInt(testData.get("noOfChild")),service);
                    countStart += noOfChildWithService;
                }
                if (noOfTeenWithService > 0) {
                    emd = verifyBag_Ski_Golf_ForEachJourneyForEachPax(basket, "Teen", i, testData, countStart, emd, Integer.parseInt(testData.get("noOfTeen")),service);
                    countStart += noOfTeenWithService;
                }
                emdCount += countStart;
            }
            Assert.assertTrue(emd.size()>0);
            Assert.assertTrue(emd.size() == emdCount);
            reportLogger.log(LogStatus.INFO,"EMD generated for  service - "+service+" -  "+emd);
            System.out.println("EMD generated for  service - "+service+" -  "+emd);
            return emd;
        }catch(AssertionError e){
            System.out.println(service.toUpperCase()+" EMD size - "+emdCount+" --- "+emd.size());
            reportLogger.log(LogStatus.INFO,"unable to retrieve EMD generated for  service - "+service);
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify service section - "+ service);
            throw e;
        }
    }

    private void verify(String item,Double expected,Double actual){
        try{
            Assert.assertEquals(actual,expected);
        }catch(AssertionError ae){
            System.out.println(item+" = "+expected+" --- "+actual);
            reportLogger.log(LogStatus.INFO,item+" expected is "+expected+" but displayed is "+actual);
            throw ae;
        }
    }

    public void verifyPriceSection(HashMap<String,Double> basket,HashMap<String,String> testData){
        String item = null;
        Double expected = 0.0,actual = 0.0;
        System.out.println("BASKET AFTER MODIFY FLIGHT - "+basket);
        try{
            verify("Total in price section",basket.get("to be paid"),CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(itemsOnLHS,"Total"))).getText()));
            verify("Price difference on LHS in price section",basket.get("price difference"),CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(itemsOnLHS,"Price difference"))).getText()));
            verify("Price difference on RHS in price section",basket.get("price difference"),CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(itemsOnRHS,"Price difference"))).getText()));
            verify("Previous total already paid in price section",basket.get("total"),CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(itemsOnRHS,"Previous total already paid"))).getText()));
            verify("New flight in price section",basket.get("new flight"),CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(itemsOnRHS,"New flight"))).getText()));

            if(basket.get("change fee")>0){
                verify("Change fee on LHS in price section",basket.get("change fee"),CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(itemsOnLHS,"Change fee"))).getText()));
                verify("Price difference on RHS in price section",basket.get("change fee"),CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(itemsOnRHS,"Change fee"))).getText()));
            }
            else if(basket.get("change fee")==0.0){
                try {
                    Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(String.format(itemsOnLHS, "Change fee")));
                    Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(String.format(itemsOnRHS, "Change fee")));
                    reportLogger.log(LogStatus.INFO,"change fee is not displayed as its not applicable");
                }catch(AssertionError ae){
                    reportLogger.log(LogStatus.INFO,"change fee is displayed in price section even if change fee is not applicable");
                    throw ae;
                }
            }

            //todo pending tax per pax
            if(basket.get("to be paid")>0) {
                verifyPaymentMode(testData);
            }
            reportLogger.log(LogStatus.INFO,"successfully verified the price section on manage booking details page");
        }catch(Exception ae){
            reportLogger.log(LogStatus.INFO,"unable to verify price section on confirmation page after manage booking ");
            throw ae;
        }

    }

    public void verifyPaymentMode(HashMap<String,String> testData){
        String expected = null;
        String actual = null;
        String lastDigits = null;
        String paymentCurrency = "GBP";
        if(testData.get("baseCurrency")!=null){
            paymentCurrency = testData.get("baseCurrency");
        }

        String total_paid = totalPaidWithCurrency.getText().charAt(0)+" "+totalPaidWithCurrency.getText().substring(1);
        try{
            if(testData.get("paymentType") != null && testData.get("paymentType").equalsIgnoreCase("paypal")) {
                expected = "Payment has been made with PayPal for an amount of "+total_paid;
            }
            else{
                String totalPaidWithoutCurrency = TestManager.getDriver().findElement(By.xpath(String.format(itemsOnLHS,"Total"))).getText().trim();
                JsonObject cardDetails = DataUtility.getJsonData("cardDetails.json");
                String cardNumber = cardDetails.getAsJsonObject(testData.get("cardName").toLowerCase()).get("cardNumber").getAsString();
                String card = cardNumber.substring(0,cardNumber.length()-4);
                char[] symbols = new char[card.length()];
                Arrays.fill(symbols, '*');
                card = card.replaceAll(card, new String(symbols));
                lastDigits = cardNumber.substring((cardNumber.length()-4)+1,cardNumber.length())+cardNumber.charAt(cardNumber.length()-1);
                cardNumber = card+lastDigits;

                expected = "Payment " + paymentCurrency+" "+totalPaidWithoutCurrency+ " with Credit Card";
                actual = paymentWithText.getText();
                Assert.assertTrue(actual.equals(expected));

                expected = testData.get("cardName").toUpperCase()+": XXXX"+lastDigits;//fixme - how's it for cards other than VISA? as in testdata UATP is UATP/Airplus
                actual = paymentDetails.getText();
                Assert.assertTrue(actual.equals(expected));

                expected = "Payment has been made with "+testData.get("cardName")+" "+cardNumber+" for an amount of "+total_paid;
            }
                actual = paymentInfo.getText().trim();
                Assert.assertEquals(actual, expected);
            reportLogger.log(LogStatus.INFO, "payment information displayed is as expected - "+expected);
        }catch(AssertionError ae) {
            System.out.println("expected = "+expected);
            System.out.println("actual = "+actual);
            reportLogger.log(LogStatus.INFO, "payment information displayed is incorrect: expected - "+expected+"\n"+"displayed - "+actual);
            throw ae;
        }
    }

    public void navigateTo(String navigateTo){
        //navigates to page mentioned by clicking on the respective button - Travel extras, Modify traveller details
        try{
            actionUtility.click(TestManager.getDriver().findElement(By.xpath(String.format(button,navigateTo))));
            reportLogger.log(LogStatus.INFO,"successfully clicked on button - "+navigateTo);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to click on button - "+navigateTo);
            throw e;
        }
    }
}
