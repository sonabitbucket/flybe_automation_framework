package efly.business_logics.online;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.CommonUtility;
import utilities.TestManager;

import java.util.HashMap;
import java.util.List;

public class CMSADS {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public CMSADS() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(id = "cardNumber")
    private WebElement cardNumber;

    @FindBy(id = "pin")
    private WebElement pin;

    @FindBy(id = "accept")
    private WebElement tC;

    @FindBy(xpath = "//button[text()='Log in']")
    private WebElement logIn;

    //ads search
    @FindBy(xpath = "//div[@id='flight_from_chosen']/a/div/b")
    private WebElement flightFromDropDown;

    @FindBy(xpath = "//div[@id ='flight_from_chosen']//input")
    private WebElement flightFrom;

    @FindBy(xpath = "//div[@id='flight_to_chosen']/a")
    private WebElement flightToDropDown;

    @FindBy(xpath = "//div[@id='flight_to_chosen']//input")
    private WebElement flightTo;


    @FindBy(xpath = "//input[contains(@class,'inputAdults')]")
    private WebElement adultNos;

    @FindBy(xpath = "//input[contains(@class,'inputTeens')]")
    private WebElement teenNos;

    @FindBy(xpath = "//input[contains(@class,'inputChildren')]")
    private WebElement childrenNos;

    @FindBy(xpath = "//input[contains(@class,'inputInfants')]")
    private WebElement infantNos;

    @FindBy(xpath = "//button[contains(@class,'adult-up')]")
    private WebElement adultIncrement;

    @FindBy(xpath = "//button[contains(@class,'adult-down')]")
    private WebElement adultDecrement;

    @FindBy(xpath = "//button[contains(@class,'teen-up')]")
    private WebElement teenIncrement;

    @FindBy(xpath = "//button[contains(@class,'teen-down')]")
    private WebElement teenDecrement;

    @FindBy(xpath = "//button[contains(@class,'child-up')]")
    private WebElement childIncrement;

    @FindBy(xpath = "//button[contains(@class,'child-down')]")
    private WebElement childDecrement;

    @FindBy(xpath = "//button[contains(@class,'infant-up')]")
    private WebElement infantIncrement;

    @FindBy(xpath = "//button[contains(@class,'infant-down')]")
    private WebElement infantDecrement;

    @FindBy(xpath = "//h3[contains(text(),'Select passengers')]")
    private WebElement selectPassengerMenu;

    @FindBy(id = "pax-display")
    private WebElement openSelectPassengerMenu;

    @FindBy(xpath = "//div[contains(@class,'pax-close-button')]")
    private WebElement closeSelectPassengerMenu;

    @FindBy(className = "ui-datepicker-year")
    private WebElement datePickerYear;

    @FindBy(className = "ui-datepicker-month")
    private WebElement datePickerMonth;

    String datePickerDay = "//div[@id='ui-datepicker-div']/div[contains(@class,'first')]/descendant::a[text()='%s']";

    @FindBy(id = "flight-search-button" )
    private WebElement findFlights;

    //ads fare select

    @FindBy(css = "div[class*='ads-teaser-panel-content'] div")
    private WebElement adsTeaser;

    @FindBy(xpath = "//span[text()='continue']")
    private WebElement continueButton;

    @FindBy(xpath = "//div[span[text()='JUST FLY']]/div/span")
    private List<WebElement> justFlyFares;

    @FindBy(xpath = "//div[span[text()='GET MORE']]/div/span")
    private List<WebElement> getMoreFares;

    @FindBy(xpath = "//div[span[text()='ALL IN']]/div/span")
    private List<WebElement> allInFares;

    //basket
    @FindBy(className = "tripsummary-price-amount-text")
    private WebElement totalAmount;

    //Select Flight
    @FindBy(xpath = "//div[div[div[div[div[h3[div[span[@class='direct']]]]]]]]//div[div[span[text()='JUST FLY']]]")
    List<WebElement> directJustFly;

    @FindBy(xpath = "//div[div[div[div[div[h3[div[span[@class='direct']]]]]]]]//div[div[span[text()='GET MORE']]]")
    List<WebElement> directGetMore;

    @FindBy(xpath = "//div[div[div[div[div[h3[div[span[@class='direct']]]]]]]]//div[div[span[text()='ALL IN']]]")
    List<WebElement> directAllIn;

    @FindBy(xpath = "//div[div[div[div[div[span[text()='1 stop']]]]]]//div[div[span[text()='JUST FLY']]]")
    List<WebElement> oneStopJustFly;

    @FindBy(xpath = "//div[div[div[div[div[span[text()='1 stop']]]]]]//div[div[span[text()='GET MORE']]]")
    List<WebElement> oneStopGetMore;

    @FindBy(xpath = "//div[div[div[div[div[span[text()='1 stop']]]]]]//div[div[span[text()='ALL IN']]]")
    List<WebElement> oneStopAllIn;

    @FindBy(xpath = "//div[div[div[div[div[span[text()='2 stops']]]]]]//div[div[span[text()='JUST FLY']]]")
    List<WebElement> twoStopJustFly;

    @FindBy(xpath = "//div[div[div[div[div[span[text()='2 stops']]]]]]//div[div[span[text()='GET MORE']]]")
    List<WebElement> twoStopGetMore;

    @FindBy(xpath = "//div[div[div[div[div[span[text()='2 stops']]]]]]//div[div[span[text()='ALL IN']]]")
    List<WebElement> twoStopAllIn;

    private String directOtherOperatorJustFly = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='Direct']]]]]//div[div[span[text()='JUST FLY']]]";
    private String directOtherOperatorGetMore = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='Direct']]]]]//div[div[span[text()='GET MORE']]]";
    private String directOtherOperatorAllIn = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='Direct']]]]]//div[div[span[text()='ALL IN']]]";

    private String oneStopOtherOperatorJustFly = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='1 stop']]]]]//div[div[span[text()='JUST FLY']]]";
    private String oneStopOtherOperatorGetMore = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='1 stop']]]]]//div[div[span[text()='GET MORE']]]";
    private String oneStopOtherOperatorAllIn = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='1 stop']]]]]//div[div[span[text()='ALL IN']]]";

    private String twoStopOtherOperatorJustFly = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='2 stops']]]]]//div[div[span[text()='JUST FLY']]]";
    private String twoStopOtherOperatorGetMore = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='2 stops']]]]]//div[div[span[text()='GET MORE']]]";
    private String twoStopOtherOperatorAllIn = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='2 stops']]]]]//div[div[span[text()='ALL IN']]]";


    public void ads_login(HashMap<String,String> testData){
        try{
            actionUtility.sendKeys(cardNumber,testData.get("ads_number"));
            actionUtility.sendKeys(pin,testData.get("ads_pin"));
            actionUtility.hardClick(tC);
            actionUtility.click(logIn);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to do ADS digitalProfile");
            throw e;
        }
    }

    public Boolean enterSourceAndDestination(HashMap<String,String> testData){
        //enter source and destination
        Boolean popupHandled = false;
        try {
            actionUtility.hardSleep(2000);
            //actionUtility.click(findFlights);
            actionUtility.waitForElementClickable(flightFromDropDown,3);
            actionUtility.click(flightFromDropDown);
            actionUtility.hardSleep(2000);
            actionUtility.sendKeysByAction(flightFrom, testData.get("flightFrom") + Keys.RETURN);
            /*actionUtility.waitForElementClickable(flightToDropDown,3);
            actionUtility.click(flightToDropDown);*/
            actionUtility.hardSleep(2000);
            actionUtility.sendKeysByAction(flightTo, testData.get("flightTo") + Keys.RETURN);
            reportLogger.log(LogStatus.INFO,"successfully entered the source - "+testData.get("flightFrom")+" and destination - "+testData.get("flightTo"));
            return popupHandled;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to enter source and destination of the trip");
            throw e;
        }
    }

    public void enterTravelDates(HashMap<String,String> testData){
        // enter the travel dates

        try {
            String promoCodeVal = null;
            String date[] = new String[3];
            int departDateOffset = Integer.parseInt(testData.get("departDateOffset"));
            date = actionUtility.getDate(departDateOffset);
            WebElement departure = TestManager.getDriver().findElement(By.id("departureDate"));
            selectDateFromCalendar(date,departure);

            reportLogger.log(LogStatus.INFO, "Entered the depart date - "+date[0]+"-"+date[1]+"-"+date[2]);

            if (testData.get("returnDateOffset") != null) {
                int returnDateOffset = Integer.parseInt(testData.get("returnDateOffset"));
                date = actionUtility.getDate(returnDateOffset);
                WebElement returnDate = TestManager.getDriver().findElement(By.id("returnDate"));
                selectDateFromCalendar(date,returnDate);
                reportLogger.log(LogStatus.INFO, "Entered the return date - "+date[0]+"-"+date[1]+"-"+date[2]);
            }
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable to enter travel Dates");
            throw e;
        }
    }

    private void selectDateFromCalendar(String date[],WebElement datePicker){
        //selects the Date from calendar control
        actionUtility.waitForElementVisible(datePicker,3);
        actionUtility.hardClick(datePicker);
//        String date[] = actionUtility.getDate(offset);
        actionUtility.dropdownSelect(datePickerYear, ActionUtility.SelectionType.SELECTBYTEXT,date[2]);
        actionUtility.dropdownSelect(datePickerMonth, ActionUtility.SelectionType.SELECTBYTEXT,date[1]);
        actionUtility.click(TestManager.getDriver().findElement(By.xpath(String.format(datePickerDay,date[0]))));

    }

    private void selectPassengers(String passengerType, int noOfPassenger, int... adultPassenger){
        // selects the no. of passengers
        String passengerNos ;
        boolean flag = true;
        int attempt =0;
        boolean attemptFlag = false;
        while(attempt<8 && attemptFlag == false) {
            try {
                if (!actionUtility.verifyIfElementIsDisplayed(selectPassengerMenu)) {
                    actionUtility.click(openSelectPassengerMenu);

                }

                switch (passengerType) {
                    case ("adults"):
                        while (flag) {

                            actionUtility.waitForElementVisible(selectPassengerMenu,3);
                            passengerNos = adultNos.getAttribute("value");

                            if (Integer.parseInt(passengerNos) < noOfPassenger) {
                                actionUtility.click(adultIncrement);

                            } else if (Integer.parseInt(passengerNos) > noOfPassenger) {
                                actionUtility.click(adultDecrement);
                            } else {
                                flag = false;
                            }
                        }
                        attemptFlag = true;
                        break;

                    case ("teens"):
                        int counter = 0;
                        while (flag) {
                            actionUtility.waitForElementVisible(selectPassengerMenu,3);
                            passengerNos = teenNos.getAttribute("value");

                            if (Integer.parseInt(passengerNos) < noOfPassenger) {
                                actionUtility.click(teenIncrement);
                                counter++;
                                if (counter == 7)
                                    if (adultPassenger.length > 0)
                                        if (adultPassenger[0] == 0)
                                            actionUtility.click(adultDecrement);//Decrement adult when there are 8 teens
                            } else {
                                flag = false;
                            }

                        }
                        attemptFlag = true;
                        break;

                    case ("children"):
                        while (flag) {
                            actionUtility.waitForElementVisible(selectPassengerMenu,3);
                            passengerNos = childrenNos.getAttribute("value");
                            if (Integer.parseInt(passengerNos) < noOfPassenger) {
                                actionUtility.click(childIncrement);
                            } else {
                                flag = false;
                            }
                        }
                        attemptFlag = true;
                        break;

                    case ("infants"):
                        while (flag) {
                            actionUtility.waitForElementVisible(selectPassengerMenu,3);
                            passengerNos = infantNos.getAttribute("value");
                            if (Integer.parseInt(passengerNos) < noOfPassenger) {
                                actionUtility.click(infantIncrement);
                            } else {
                                flag = false;
                            }
                        }
                        attemptFlag = true;
                        break;
                }
            } catch (Exception e) {

                attempt++;
            }
        }
        if (!attemptFlag){
            throw  new RuntimeException("unable to select the passengers");
        }
    }

    public int selectNoOfPassengers(HashMap<String,String> testData){
        //select the no.of passengers
        try {
            int noOfPassengers=1;
            actionUtility.click(openSelectPassengerMenu);
            int noOfTeen = 0, noOfChild = 0, noOfInfant = 0, noOfAdult = 1;
            if (testData.get("noOfTeen") != null) {
                noOfTeen = Integer.parseInt(testData.get("noOfTeen"));

                noOfPassengers += noOfTeen;
                if(noOfTeen == 8)
                {
                    selectPassengers("teens",noOfTeen,Integer.parseInt(testData.get("noOfAdult")));
                }
                else{
                    selectPassengers("teens",noOfTeen);
                }
            }

            if (testData.get("noOfAdult") != null) {
                noOfAdult = Integer.parseInt(testData.get("noOfAdult"));
                selectPassengers("adults", noOfAdult);
                noOfPassengers += (noOfAdult-1);
            }

            if (testData.get("noOfChild") != null) {
                noOfChild = Integer.parseInt(testData.get("noOfChild"));
                selectPassengers("children", noOfChild);
                noOfPassengers += noOfChild;
            }
            if (testData.get("noOfInfant") != null) {
                noOfInfant = Integer.parseInt(testData.get("noOfInfant"));
                selectPassengers("infants", Integer.parseInt(testData.get("noOfInfant")));
            }
            try{
                actionUtility.click(closeSelectPassengerMenu);
            }catch (Exception e){}
            reportLogger.log(LogStatus.INFO,"successfully selected all types of passengers");
            reportLogger.log(LogStatus.INFO,"Adults - "+noOfAdult+" Teens - "+noOfTeen+" Children - "+noOfChild+" Infant - "+noOfInfant);

            return noOfPassengers;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to select the number of passengers");
            throw e;
        }
    }

    public void findFlights(){
        // click on  the find flights
        try{
            actionUtility.hardClick(findFlights);
            actionUtility.hardSleep(3000);
            actionUtility.waitForPageLoad(20);
            actionUtility.hardSleep(3000);

            reportLogger.log(LogStatus.INFO, "Flight searched");

        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable to search flights");
            throw e;
        }
    }

    public void verifyADS_price(String way,HashMap<String,String> fares){
        String actual = null;
        try{
            actionUtility.waitForElementVisible(adsTeaser,90);
            if(way.equals("one way")){
                way = "Outbound";
            }
            int noOfFlights = Integer.parseInt(fares.get("noOf"+way+"Flights"));
            for(int flightCount=1, i=noOfFlights;i<=justFlyFares.size();i++,flightCount++){
                actual = justFlyFares.get(i-1).getText();
                Assert.assertTrue(CommonUtility.convertStringToDouble(fares.get("justFly"+way+flightCount))>CommonUtility.convertStringToDouble(justFlyFares.get(i-1).getText()));
            }
            for(int flightCount=1,i=noOfFlights;i<=getMoreFares.size();i++,flightCount++){
                actual = getMoreFares.get(i-1).getText();
                Assert.assertTrue(CommonUtility.convertStringToDouble(fares.get("getMore"+way+flightCount))>CommonUtility.convertStringToDouble(getMoreFares.get(i-1).getText()));
            }
            for(int flightCount=1,i=noOfFlights;i<=allInFares.size();i++,flightCount++){
                actual = allInFares.get(i-1).getText();
                Assert.assertTrue(CommonUtility.convertStringToDouble(fares.get("allIn"+way+flightCount))>CommonUtility.convertStringToDouble(allInFares.get(i-1).getText()));
            }
            if(way.equals("Inbound")) {
                actual = getBasketPrice()+"";
                Assert.assertTrue(CommonUtility.convertStringToDouble(fares.get("totalFare")) > getBasketPrice());
            }
        }catch (AssertionError e){
            System.out.println(actual);
            reportLogger.log(LogStatus.INFO,"ADS discount is not displayed");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify ADS fares displayed");
            throw e;
        }
    }

    private Double getBasketPrice(){
        Double fare = 0.00;
        try{
            actionUtility.waitForElementVisible(totalAmount,30);
            fare = CommonUtility.convertStringToDouble(totalAmount.getText());
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to retrieve total fare displayed in basket");
            throw e;
        }
        return fare;
    }

    public HashMap<String,String> selectFlight(HashMap<String,String> testData,String... getFare){
        //selects both departure and return flight - direct,1 stop and 2 stops
        /*Double inboundFare = 0.00;
        Double outboundFare = 0.00;
        */
        HashMap<String,String> fares = new HashMap<>();
        try{
            actionUtility.waitForElementVisible(continueButton,90);
            if(testData.get("departOperatorName")!=null){
                /*outboundFare = */selectOtherOperatorFlight(testData);
            }
            else {
                if (testData.get("departNoOfStops").equals("0")) {
                    /*outboundFare = */selectDirectFlight(testData);
                } else if (testData.get("departNoOfStops").equals("1")) {
                    /*outboundFare = */selectOneStopFlight(testData);
                } else if (testData.get("departNoOfStops").equals("2")) {
                    /*outboundFare = */selectTwoStopFlight(testData);
                }
            }
            if(testData.get("returnDateOffset")!=null){
                actionUtility.hardClick(continueButton);
                actionUtility.hardSleep(2000);
                if(testData.get("returnOperatorName")!=null){
                    /*inboundFare = */selectOtherOperatorFlight(testData,"inbound");
                }
                else {
                    if (testData.get("returnNoOfStops").equals("0")) {
                        /*inboundFare = */selectDirectFlight(testData,"inbound");
                    } else if (testData.get("returnNoOfStops").equals("1")) {
                        /*inboundFare = */selectOneStopFlight(testData,"inbound");
                    } else if (testData.get("returnNoOfStops").equals("2")) {
                        /*inboundFare = */selectTwoStopFlight(testData,"inbound");
                    }
                }
            }
            return fares;
        }catch(Exception e){
            throw e;
        }
        /*return outboundFare+inboundFare;*/
    }

    private void selectDirectFlight(HashMap<String,String> testData,String... inbound){
        //selects direct flight for ticket type specified in testdata
        //Double fareSelected = 0.00;
        try{
            if(inbound.length>0){
                if (testData.get("returnTicketType").equalsIgnoreCase("just fly")) {
                    actionUtility.hardClick(directJustFly.get(directJustFly.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully selected inbound Just Fly Direct flight");
                } else if (testData.get("returnTicketType").equalsIgnoreCase("get more")) {
                    actionUtility.hardClick(directGetMore.get(directGetMore.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully selected inbound Get More Direct flight");
                } else {
                    actionUtility.hardClick(directAllIn.get(directAllIn.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully selected inbound All In Direct flight");
                }
                //fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(1).getText());
            }else {
                if (testData.get("departTicketType").equalsIgnoreCase("just fly")) {
                    actionUtility.click(directJustFly.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully selected outbound Just Fly Direct flight");
                } else if (testData.get("departTicketType").equalsIgnoreCase("get more")) {
                    actionUtility.click(directGetMore.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully selected outbound Get More Direct flight");
                } else {
                    actionUtility.click(directAllIn.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully selected outbound All In Direct flight");
                }
                //fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(0).getText());
            }
            actionUtility.hardSleep(2000);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select direct flight");
            throw e;
        }
        //return fareSelected;
    }

    private void selectOneStopFlight(HashMap<String,String> testData, String... inbound){
        //selects 1 Stop flight for ticket type specified in testdata
        /*Double fareSelected = 0.0;*/
        try{
            if(inbound.length>0){
                if (testData.get("returnTicketType").equalsIgnoreCase("just fly")) {
                    actionUtility.click(oneStopJustFly.get(oneStopJustFly.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully selected inbound Just Fly 1 stop flight");
                } else if (testData.get("returnTicketType").equalsIgnoreCase("get more")) {
                    actionUtility.click(oneStopGetMore.get(oneStopGetMore.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully selected inbound Get More 1 stop flight");
                } else {
                    actionUtility.click(oneStopAllIn.get(oneStopAllIn.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully selected inbound All In 1 stop flight");
                }
                //fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(1).getText());
            }else {
                if (testData.get("departTicketType").equalsIgnoreCase("just fly")) {
                    actionUtility.click(oneStopJustFly.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully selected outbound Just Fly 1 stop flight");
                } else if (testData.get("departTicketType").equalsIgnoreCase("get more")) {
                    actionUtility.click(oneStopGetMore.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully selected outbound Get More 1 stop flight");
                } else {
                    actionUtility.click(oneStopAllIn.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully selected outbound All In 1 stop flight");
                }
                //fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(0).getText());
            }
            actionUtility.hardSleep(2000);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select 1 stop flight");
            throw e;
        }
        /*return fareSelected;*/
    }

    private void selectTwoStopFlight(HashMap<String,String> testData, String... inbound){
        //selects 2 stops flight for ticket type specified in testdata
        /*Double fareSelected = 0.00;*/
        try{
            if(inbound.length>0){
                if (testData.get("returnTicketType").equalsIgnoreCase("justfly")) {
                    actionUtility.click(twoStopJustFly.get(twoStopJustFly.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully inbound selected Just Fly 2 stops flight");
                } else if (testData.get("returnTicketType").equalsIgnoreCase("getmore")) {
                    actionUtility.click(twoStopGetMore.get(twoStopGetMore.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully inbound selected Get More 2 Stops flight");
                } else {
                    actionUtility.click(twoStopAllIn.get(twoStopAllIn.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully inbound selected All In 2 stops flight");
                }
                //fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(1).getText());
            }
            else {
                if (testData.get("departTicketType").equalsIgnoreCase("justfly")) {
                    actionUtility.click(twoStopJustFly.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully outbound selected Just Fly 2 stops flight");
                } else if (testData.get("departTicketType").equalsIgnoreCase("getmore")) {
                    actionUtility.click(twoStopGetMore.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully outbound selected Get More 2 Stops flight");
                } else {
                    actionUtility.click(twoStopAllIn.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully outbound selected All In 2 stops flight");
                }
                //fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(1).getText());
            }
            actionUtility.hardSleep(2000);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select 2 stops flight");
            throw e;
        }
        /*return fareSelected;*/
    }

    private void selectOtherOperatorFlight(HashMap<String,String> testData,String... inbound){
        //select other operators flights specified in testData and also calculates and returns total fare
        /*Double fareSelected = 0.00;*/
        try{
            if(inbound.length>0){
                if (testData.get("returnNoOfStops").equals("0")) {
                    if (testData.get("returnTicketType").equalsIgnoreCase("just fly")) {
                        List<WebElement> directJustFlyOperator = TestManager.getDriver().findElements(By.xpath(String.format(directOtherOperatorJustFly, testData.get("returnOperatorName"))));
                        actionUtility.click(directJustFlyOperator.get(directJustFlyOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected Just Fly direct " + testData.get("returnOperatorName") + " outbound flight");
                    } else if (testData.get("returnTicketType").equalsIgnoreCase("get more")) {
                        List<WebElement> directGetMoreOperator = TestManager.getDriver().findElements(By.xpath(String.format(directOtherOperatorGetMore, testData.get("returnOperatorName"))));
                        actionUtility.click(directGetMoreOperator.get(directGetMoreOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected Get More direct " + testData.get("returnOperatorName") + " outbound flight");
                    } else if (testData.get("returnTicketType").equalsIgnoreCase("all in")) {
                        List<WebElement> directAllInOperator = TestManager.getDriver().findElements(By.xpath(String.format(directOtherOperatorAllIn, testData.get("returnOperatorName"))));
                        actionUtility.click(directAllInOperator.get(directAllInOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected All In direct " + testData.get("returnOperatorName") + " outbound flight");
                    }
                }
                if (testData.get("returnNoOfStops").equals("1")) {
                    if (testData.get("returnTicketType").equalsIgnoreCase("just fly")) {
                        List<WebElement> oneStopJustFlyOperator = TestManager.getDriver().findElements(By.xpath(String.format(oneStopOtherOperatorJustFly, testData.get("returnOperatorName"))));
                        actionUtility.click(oneStopJustFlyOperator.get(oneStopJustFlyOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected Just Fly 1 stop " + testData.get("returnOperatorName") + " outbound flight");
                    } else if (testData.get("returnTicketType").equalsIgnoreCase("get more")) {
                        List<WebElement> oneStopGetMoreOperator = TestManager.getDriver().findElements(By.xpath(String.format(oneStopOtherOperatorGetMore, testData.get("returnOperatorName"))));
                        actionUtility.click(oneStopGetMoreOperator.get(oneStopGetMoreOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected Get More 1 stop " + testData.get("returnOperatorName") + " outbound flight");
                    } else if (testData.get("returnTicketType").equalsIgnoreCase("all in")) {
                        List<WebElement> oneStopAllInOperator = TestManager.getDriver().findElements(By.xpath(String.format(oneStopOtherOperatorAllIn, testData.get("returnOperatorName"))));
                        actionUtility.click(oneStopAllInOperator.get(oneStopAllInOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected All In 1 stop " + testData.get("returnOperatorName") + " outbound flight");
                    }

                }
                if (testData.get("returnNoOfStops").equals("2")) {
                    if (testData.get("returnTicketType").equalsIgnoreCase("just fly")) {
                        List<WebElement> twoStopJustFlyOperator = TestManager.getDriver().findElements(By.xpath(String.format(twoStopOtherOperatorJustFly, testData.get("returnOperatorName"))));
                        actionUtility.click(twoStopJustFlyOperator.get(twoStopJustFlyOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected Just Fly 2 stop " + testData.get("returnOperatorName") + " outbound flight");
                    } else if (testData.get("returnTicketType").equalsIgnoreCase("get more")) {
                        List<WebElement> twoStopGetMoreOperator = TestManager.getDriver().findElements(By.xpath(String.format(twoStopOtherOperatorGetMore, testData.get("returnOperatorName"))));
                        actionUtility.click(twoStopGetMoreOperator.get(twoStopGetMoreOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected Get More 2 stop " + testData.get("returnOperatorName") + " outbound flight");
                    } else if (testData.get("returnTicketType").equalsIgnoreCase("all in")) {
                        List<WebElement> twoStopAllInOperator = TestManager.getDriver().findElements(By.xpath(String.format(twoStopOtherOperatorAllIn, testData.get("returnOperatorName"))));
                        actionUtility.click(twoStopAllInOperator.get(twoStopAllInOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected All In 2 stop " + testData.get("returnOperatorName") + " outbound flight");
                    }

                }
                //fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(1).getText());
            }else {
                WebElement flightToBeSelected;
                if (testData.get("departNoOfStops").equals("0")) {
                    if (testData.get("departTicketType").equalsIgnoreCase("just fly")) {
                        flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(directOtherOperatorJustFly, testData.get("departOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected just fly direct " + testData.get("departOperatorName") + " outbound flight");
                    } else if (testData.get("departTicketType").equalsIgnoreCase("get more")) {
                        flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(directOtherOperatorGetMore, testData.get("departOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected get more direct " + testData.get("departOperatorName") + " outbound flight");
                    } else if (testData.get("departTicketType").equalsIgnoreCase("all in")) {
                        flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(directOtherOperatorAllIn, testData.get("departOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected all in direct " + testData.get("departOperatorName") + " outbound flight");
                    }
                }
                if (testData.get("departNoOfStops").equals("1")) {
                    if (testData.get("departTicketType").equalsIgnoreCase("just fly")) {
                        flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(oneStopOtherOperatorJustFly, testData.get("departOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected just fly 1 stop " + testData.get("departOperatorName") + " outbound flight");
                    } else if (testData.get("departTicketType").equalsIgnoreCase("get more")) {
                        flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(oneStopOtherOperatorGetMore, testData.get("departOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected get more 1 stop " + testData.get("departOperatorName") + " outbound flight");
                    } else if (testData.get("departTicketType").equalsIgnoreCase("all in")) {
                        flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(oneStopOtherOperatorAllIn, testData.get("departOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected all in 1 stop " + testData.get("departOperatorName") + " outbound flight");
                    }
                }
                if (testData.get("departNoOfStops").equals("2")) {
                    if (testData.get("departTicketType").equalsIgnoreCase("just fly")) {
                        flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(twoStopOtherOperatorJustFly, testData.get("departOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected just fly 2 stop " + testData.get("departOperatorName") + " outbound flight");
                    } else if (testData.get("departTicketType").equalsIgnoreCase("get more")) {
                        flightToBeSelected =TestManager.getDriver().findElements(By.xpath(String.format(twoStopOtherOperatorGetMore, testData.get("departOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected get more 2 stop " + testData.get("departOperatorName") + " outbound flight");
                    } else if (testData.get("departTicketType").equalsIgnoreCase("all in")) {
                        flightToBeSelected = (TestManager.getDriver().findElements(By.xpath(String.format(twoStopOtherOperatorAllIn, testData.get("departOperatorName")))).get(0));
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected all in 2 stop " + testData.get("departOperatorName") + " outbound flight");
                    }
                }
                //fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(0).getText());
            }
            actionUtility.hardSleep(2000);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select other operator flight");
            throw e;
        }
        /*return fareSelected;*/
    }

    public void navigateToPaxDetails(){
        try{
            actionUtility.hardClick(continueButton);
            reportLogger.log(LogStatus.INFO,"successfully clicked on continue button to navigate to passenger details page fm fare select page");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to passenger details page");
            throw e;
        }
    }

}
