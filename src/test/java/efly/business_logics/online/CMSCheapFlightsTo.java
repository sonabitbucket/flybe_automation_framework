package efly.business_logics.online;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.relevantcodes.extentreports.model.Log;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.CommonUtility;
import utilities.TestManager;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Sona on 4/12/2019.
 */
public class CMSCheapFlightsTo {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public CMSCheapFlightsTo() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(xpath = "//div[@class='flight-info']/p[4]")
    private List<WebElement> destination;

    @FindBy(id = "find-cheap-flights")
    private WebElement findCheapFlight;

    @FindBy(id = "price")
    private List<WebElement> fare;

    String flightDetails = "//div[div[p[@id='price'][contains(text(),'%s')]]]/div[@class='flight-info']/p[2]";

    public void verifyThatDestinationIsSameAsFlightSelectedInEachTile(String selectedDestination){
        //verifies that all cheap flights to tile has destination selected in offers page
        try {
            try{
                Assert.assertTrue(findCheapFlight.getText().toLowerCase().contains(selectedDestination.toLowerCase()));
            }catch(AssertionError e){
                reportLogger.log(LogStatus.INFO,"'find cheap flights to' header does not mention "+selectedDestination);
                throw e;
            }
            for (WebElement tileDestination : destination) {
                //System.out.println(tileDestination.getText());
                Assert.assertTrue(tileDestination.getText().toLowerCase().contains(selectedDestination.toLowerCase()));
            }
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"destination in tile on 'cheap flights to' doesn't match with destination selected on offers page"+selectedDestination);
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify if destination in each tile in 'cheap flights to' is "+selectedDestination);
            throw e;
        }
    }

    public HashMap<String, String> selectRouteWithCurrencySpecifiedAndgetDetails(String currency,String destination){
        //selects a route for currency specified and  return selected
        HashMap<String,String> selectedDetails = null;
        try{
            selectedDetails = getFareAndDepartureAirportAndSelectRoute(currency);
            selectedDetails.put("destination",destination);
            System.out.println("Selected details "+selectedDetails);
            return selectedDetails;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select a route with currency "+currency);
            throw e;
        }
    }

    private HashMap<String,String> getFareAndDepartureAirportAndSelectRoute(String currency){
        HashMap<String,String> details = new HashMap<String,String>();
        for(int i=0;i<fare.size();i++){
            String price = fare.get(i).getText();
            if(price.contains(currency)){
                details.put("price",price.substring(0,price.length()-1));
                details.put("departure",TestManager.getDriver().findElements(By.xpath(String.format(flightDetails,currency))).get(0).getText());
                actionUtility.click(fare.get(i));
                break;
            }
        }
        return details;
    }
}