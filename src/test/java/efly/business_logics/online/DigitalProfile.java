package efly.business_logics.online;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.junit.Assert;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.DataUtility;
import utilities.EmailUtility;
import utilities.TestManager;

import java.util.HashMap;

/**
 * Created by Indhu on 5/23/2019.
 */
public class DigitalProfile {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public DigitalProfile() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy (id = "sign-in-button")
    private WebElement loginOption;

    @FindBy (css = "input[placeholder*='email']")
    private WebElement userName;

    @FindBy (css = "input[placeholder*='password']")
    private WebElement password;

    @FindBy (xpath = "//span[text()=' Sign in ']")
    private WebElement signInButton;

    //my account

    @FindBy(css = "h1[class='title']")
    private WebElement profileName;

    //passenger information

    @FindBy (xpath = "//span[text()='Passenger information']")
    private WebElement passengerInformation;

    @FindBy (xpath = "//div[@class='profile-section-page-container-wrapper']//li[1]//div[2]")
    private WebElement title;

    @FindBy (xpath = "//div[@class='profile-section-page-container-wrapper']//li[2]//div[2]")
    private WebElement firstName;

    @FindBy (xpath = "//div[@class='profile-section-page-container-wrapper']//li[3]//div[2]")
    private WebElement lastName;

    //contact information

    @FindBy (xpath = "//span[text()='Contact information']")
    private WebElement contactInformation;

    @FindBy (xpath = "//div[@class='profile-section-page-container-wrapper']//li[2]//div[2]")
    private WebElement mobilePhone;

    @FindBy (xpath = "//div[@class='actions']//span[text()=' Back to profile ']")
    private WebElement backToProfile;

    //forgot password

    @FindBy(linkText = "Forgot password?")
    private WebElement forgotPassword;

    //account information

    @FindBy (xpath = "//span[text()='Account information']")
    private WebElement accountInformation;

    @FindBy(linkText = "Update your password")
    private WebElement updatePasswordLink;

    @FindBy(css = "input[placeholder*='current password']")
    private WebElement currentPassword;

    @FindBy(css = "input[id*='newPassword']")
    private WebElement newPassword;

    @FindBy(css = "input[id*='confirmPassword']")
    private WebElement confirmPassword;

    @FindBy(xpath = "//button[span[text()=' Update profile ']]")
    private WebElement updatePasswordButton;

    @FindBy(linkText = "Delete your password")
    private WebElement deleteProfileLink;

    @FindBy(xpath = "//button[span[text()=' Delete my account ']]")
    private WebElement deleteProfileButton;

    @FindBy(xpath = "//span[contains(text(),' Incorrect email or password, please try again ')]")
    private WebElement DPDoesNotExist;

    //create an account

    @FindBy(className = "create-account-link")
    private WebElement createAccount;

    @FindBy(xpath = "//button[span[contains(text(),'Create profile')]]")
    private WebElement createButton;

    //forgot password

    @FindBy(xpath = "//button[span[contains(text(),'Create new password')]]")
    private WebElement createNewPassword;


    @FindBy(xpath = "//button[span[contains(text(),' Confirm password ')]]")
    private WebElement confirmNewPassword;

    @FindBy(xpath = "//span[text()='Your password has been updated']")
    private WebElement forgotPasswordConfirmation;

    //activate account

    @FindBy(xpath = "//div[label[span[contains(text(),'Title')]]]//select")
    private WebElement chooseTitle;

    @FindBy(css = "input[placeholder*='first name']")
    private WebElement enterFirstname;

    @FindBy(css = "input[placeholder*='last name']")
    private WebElement enterLastname;

    @FindBy(css = "input[placeholder*='residence']")
    private WebElement enterCountryOfResidence;

    @FindBy(css = "i[class*='checkbox']")
    private WebElement tC;

    @FindBy(xpath = "//button[span[text()=' Activate profile ']]")
    private WebElement activateProfile;

    //logout

    @FindBy(xpath = "//a[contains(text(),'Log out')]")
    private WebElement logout;

    public void logout(){
        try{
            actionUtility.scrollToViewElement(logout);
            actionUtility.click(logout);
            actionUtility.hardSleep(5000);
            reportLogger.log(LogStatus.INFO,"successfully logged out of DP");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to logout of DP");
            throw e;
        }
    }

    public void verify_deleteProfile(HashMap<String,String> testData, Boolean passwordUpdated, String... browser){
        String passwd = null;
        try {
            navigateToAccountInformation();
            actionUtility.click(deleteProfileLink);
            if(passwordUpdated) {
                passwd = testData.get("newPassword");
                actionUtility.sendKeys(password, passwd);
            }
            else{
                passwd = testData.get("password");
                actionUtility.sendKeys(password,passwd);
            }
            actionUtility.click(deleteProfileButton);
            if(browser.length>0) {
                verifyProfileDeletion(testData, passwordUpdated,browser[0]);
            }
            else{
                verifyProfileDeletion(testData,passwordUpdated);
            }
            reportLogger.log(LogStatus.INFO,"successfully deleted DP");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to delete DP");
            throw e;
        }
    }

    private void verifyProfileDeletion(HashMap<String,String> testData,Boolean passwordChanged,String... browser){
        if(browser.length>0) {
            login(testData,passwordChanged, browser[0]);
        }
        else{
            login(testData,passwordChanged);
        }
        Assert.assertTrue(DPDoesNotExist.isDisplayed());
    }

    public void updatePassword(HashMap<String,String> testData){
        try {
            navigateToAccountInformation();
            actionUtility.click(updatePasswordLink);
            actionUtility.sendKeys(currentPassword,testData.get("password"));
            actionUtility.sendKeys(newPassword,testData.get("newPassword"));
            actionUtility.sendKeys(confirmPassword,testData.get("confirmPassword"));
            actionUtility.click(updatePasswordButton);
            reportLogger.log(LogStatus.INFO,"successfully updated DP password");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to update DP password");
            throw e;
        }
    }

    private void navigateToAccountInformation(){
        actionUtility.waitForElementVisible(accountInformation,30);
        actionUtility.hardClick(accountInformation);
    }

    public void create_activate_account(String browser, HashMap<String,String>testData){
        //creates and account for the email id provided
        String email = null;
        try{
            actionUtility.waitForElementVisible(createAccount,180);
            email = testData.get(browser.toLowerCase()+"_userName");
            String passwd = testData.get("password");
            actionUtility.click(createAccount);
            actionUtility.sendKeys(userName,email);
            actionUtility.sendKeys(password,passwd);
            actionUtility.click(createButton);
            activateAccount(email,passwd,"Activate your Flybe account","activate your account");
            completeActivation(testData,passwd);
            reportLogger.log(LogStatus.INFO,"successfully created a DP account for email - "+email);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to create an account for email - "+email);
            throw e;
        }
    }

    private void completeActivation(HashMap<String,String> testData,String passwd){
        actionUtility.dropdownSelect(chooseTitle, ActionUtility.SelectionType.SELECTBYVALUE,testData.get("title").toUpperCase());
        actionUtility.sendKeys(enterFirstname,testData.get("firstName"));
        actionUtility.sendKeys(enterLastname,testData.get("lastName"));
        actionUtility.sendKeys(enterCountryOfResidence,testData.get("countryOfResidence"));
        actionUtility.sendKeys(password,passwd);
        actionUtility.click(tC);
        actionUtility.click(activateProfile);
    }

    private void activateAccount(String email, String password,String subject,String linktext){
        actionUtility.hardSleep(10000); // waiting for mail to be delivered to gmail
        String url = EmailUtility.readMail(email,password,subject,linktext);

        try{
            url = url.replace("accounts.flybe.com","wav.eu8.amadeus.com");
        }catch(NullPointerException e){
            url = EmailUtility.readMail(email,password,subject,linktext);
            url = url.replace("accounts.flybe.com","wav.eu8.amadeus.com");
        }
        System.out.println("UAT url = "+url);
        TestManager.getDriver().get(url);


    }

    public void verify_forgotPassword_activateAccount(HashMap<String,String> testData, String browser){
        String email=null,passwd=null;
        try{
            String[] details = getEmailAndPassword(testData,false,browser);
            email = details[0];
            passwd = details[1];
            userName.clear();
            userName.sendKeys(email);
            actionUtility.click(forgotPassword);
            actionUtility.waitForElementVisible(createNewPassword,90);
            userName.sendKeys(email);
            password.sendKeys(passwd);
            actionUtility.click(createNewPassword);
            activateAccount(email,testData.get("password"),"Flybe account password request","activate your password");
            password.sendKeys(passwd);
            actionUtility.click(confirmNewPassword);
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(forgotPasswordConfirmation));
            reportLogger.log(LogStatus.WARNING,"successfully reset password using forgot password link");
        }catch(Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to reset password using forgot password link");
            throw e;
        }
    }

    public void login(HashMap<String,String> testData,Boolean passwordChanged,String... browser){
        String email = null;
        String pwd = null;
        try {
            String[] details = null;
            if(browser.length>0) {
               details = getEmailAndPassword(testData,passwordChanged,browser);
            }
            else{
                details = getEmailAndPassword(testData,passwordChanged);
            }
            email = details[0];
            pwd = details[1];
            userName.clear();
            userName.sendKeys(email);
            password.clear();
            password.sendKeys(pwd);
            actionUtility.hardSleep(3000);
            actionUtility.hardClick(signInButton);
            actionUtility.hardSleep(3000);
            reportLogger.log(LogStatus.INFO,"successfully able to login with email id - "+email);
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to login with email id - "+email);
            throw e;
        }
    }

    private String[] getEmailAndPassword(HashMap<String,String> testData,Boolean passwordChanged,String... browser){
        String email = null;
        String pwd = null;
        if(browser.length>0){
            email = testData.get(browser[0].toLowerCase()+"_userName");
            if(passwordChanged) {
                pwd = testData.get("newPassword");
            }
            else{
                pwd = testData.get("password");
            }
        }
        else {
            email = new DataUtility().testData("1").get("bookerEmail").toString();
            pwd = new DataUtility().testData("1").get("loginPassword").toString();
            if (testData.get("bookerEmail") != null) {
                email = testData.get("bookerEmail");
                pwd = testData.get("loginEmailAddress");
            }
        }
        return new String[]{email,pwd};
    }
    public HashMap capturePassengerInformation(HashMap<String,String> testData){
        //gets the logged in passenger details
        try{
            actionUtility.waitForElementVisible(passengerInformation,120);
            actionUtility.hardClick(passengerInformation);
            testData.put("title",title.getText());
            testData.put("firstName",firstName.getText());
            testData.put("lastName",lastName.getText());
            actionUtility.click(backToProfile);
            reportLogger.log(LogStatus.INFO,"successfully captured passenger information from my account page");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to capture passenger information from my account page");
            throw e;
        }
        return testData;
    }

    public HashMap captureContactInformation(HashMap<String,String> details){
        //gets the contact details of logged in passenger
        try{
            actionUtility.waitForElementVisible(contactInformation,90);
            actionUtility.hardClick(contactInformation);
            String mobilePhoneNumber = mobilePhone.getText();
            String[] phoneNumber = mobilePhone.getText().split(" ");
            String countryCode = mobilePhoneNumber.substring(mobilePhoneNumber.indexOf("(")+1,mobilePhoneNumber.indexOf(")"));
            details.put("countryCode",countryCode);
            details.put("phoneNumber",phoneNumber[1]);
            actionUtility.click(backToProfile);
            reportLogger.log(LogStatus.INFO,"successfully captured route the contact information from my account page");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to capture route the contact information from my account page");
            throw e;
        }
        return details;
    }

    public void navigateBackToHomeFromLogin(){
        //navigate to live home page
        try{
            String appUrl = "https://www.itflybe.com/";
            try{
                TestManager.getDriver().navigate().to(appUrl);
            }catch (TimeoutException e){}
            reportLogger.log(LogStatus.INFO, "successfully navigated to home page");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to navigate to the home page");
            throw e;
        }
    }

}
