package efly.business_logics.online;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.CommonUtility;
import utilities.TestManager;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Sona on 4/3/2019.
 */
public class EFLYExtras {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public EFLYExtras(){
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(xpath = "//button[span[text()='continue']]")
    private WebElement continueButton;
/*
    @FindBy(css = "button[class*='catalogService-button-add']")
    private List<WebElement> extraService;*/

    String extraService = "//div[@class='footer']//button[contains(text(),'%s')]";

    String addGolf = "//div[div[div[text()='%s']]]//button";

    String addSki = "//div[div[div[text()='%s']]]//button[span[span[text()='%s']]]";

    @FindBy(xpath = "//button/span[text()='Add to booking']")
    private WebElement addService;

    String openService = "//div[contains(@class,'dialog-show')]";

    /*@FindBy(css = "span[class='total-price-container'] span[class='price']")
    private WebElement servicePrice;*/

    String servicePrice = "//span[@class='total-price-container']/span[@class='price'][contains(text(),'%s')]";

    //basket
    String serviceCost = "//div[h3[contains(text(),'%s')]]//span[@class='tripsummary-price-amount-text']";

    @FindBy(className = "price-details-services-amount-value")
    private WebElement services;

    @FindBy(css = "div[class*='tripsummary-price-total'] span[class='tripsummary-price-amount-text']")
    private WebElement totalPrice;

    //seat

    @FindBy(xpath = "//span[text()='We are unable to price the services selected. (15233)']")
    private WebElement xlSeatForTeenError;

    @FindBy(className = "seatNumber")
    private List<WebElement> seatNo;

    @FindBy(id= "seatmap")
    private WebElement seatMap;

    @FindBy(css = "div[class='seatMapfooter'] button")
    private WebElement continueWithSelectedSeat;

    @FindBy(xpath = "//li[a[span[@class='tab-header']]]")
    private List<WebElement> flightTabHeader;

    @FindBy(xpath = "//button[contains(text(),'NEXT FLIGHT')]")
    private WebElement nextFlight;

    @FindBy(xpath = "//button[contains(text(),'PREVIOUS FLIGHT')]")
    private WebElement previousFlight;

    @FindBy(xpath = "//td[section[@class='extra-leg']]/div[not(contains(@class,'unavailable'))][not(@data-seat-selected='true')]")
    private List<WebElement> extraLegSeat;

    @FindBy(xpath = "//td[not(section)]/div[contains(@class,'icon-seat')][not(contains(@class,'unavailable'))][not(@data-seat-selected='true')]")
    private List<WebElement> standardSeat;

    @FindBy(xpath = "//div[@class='seatMapfooter']//span[@class='plnext-widget-btn-text one-icon']")
    private WebElement continueWithSeats;

    String paxButton = "//div[div[div[div[label[div[strong[text()='%s']]]]]]]";

    String seatPrice = "span[class='primaryCurrency'] div";

    @FindBy(css = "label[for*='radio_BE_PAYPAL']")
    private WebElement paymentByPayPal;

    //car parking

    @FindBy(xpath = "//span[div[label[contains(text(),'Parking to')]]]//span[contains(@class,'calendar')]")
    private WebElement parkingTo_CalendarIcon;

    private String parkingToDate = "//td[contains(@title,'%s')]";

    @FindBy(className = "icon-arrow-right")
    private  WebElement nextCalendar;

    @FindBy(xpath = "//button[contains(text(),'GET QUOTE')]")
    private WebElement getCarParkingQuotes;

    @FindBy(name = "departureTime")
    private WebElement parkingToTime;

    @FindBy(xpath = "//button[contains(@class,'park-offer-actions-select')][contains(text(),'Select parking')]")
    private List<WebElement> selectCarpark;

    @FindBy(css = "input[id*='-input-registration']")
    private WebElement carRegistrationNo;

    @FindBy(xpath = "//span[contains(text(),'Add to booking')]")
    private WebElement parkingAddingButton;

    @FindBy(xpath = "//h3[contains(text(),'Airport parking')]")
    private WebElement parkingInBasket;

    @FindBy(xpath="//span[@class='total-price-container']/span")
    private List<WebElement> parkingCost;

    public void continueToPayment(){
        //navigates to payment page
        try{
            actionUtility.scrollToViewElement(continueButton);
            actionUtility.hardClick(continueButton);
            actionUtility.waitForElementVisible(paymentByPayPal,120);
            reportLogger.log(LogStatus.INFO,"successfully navigated to payment page");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to payment page");
            throw e;
        }
    }

    public void verifySeatSelectedOnSeatPage(HashMap<String,String> seatDetails,HashMap<String,String> testData){
        try{
            HashMap<String,String> seatDisplayed =  retrieveSeatSelectedForEachPax(testData,true);
            Set<String> expectedPaxWithSeat = seatDetails.keySet();
            Set<String> expectedSeatNo = new HashSet<>(seatDetails.values());
            Set<String> actualPaxWithSeat = seatDisplayed.keySet();
            Set<String> actualSeatNo = new HashSet<>(seatDisplayed.values());

            Assert.assertTrue(actualPaxWithSeat.equals(expectedPaxWithSeat));
            Assert.assertTrue(actualSeatNo.equals(expectedSeatNo));

        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"seat selected on seat page are not displayed in 'edit seat' tile on extras page");
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify seat selection displayed on extras - 'edit seat' tile");
            throw e;
        }
    }

    private HashMap<String,String> captureSeatNumber(HashMap<String,String> testData,String pax,int noOfPax,int sector,HashMap<String,String> seatDetails,boolean... previouslyAdded){
        String seatSelected = null;
        int counter = Integer.parseInt(seatDetails.get("counter"));
        int noOfPaxTypeWithSeat = 0;
        System.out.println("counter "+counter);
        System.out.println("sector "+sector);
        System.out.println("pax "+pax);

        for(int j=1;j<=noOfPax;j++) {
            if(previouslyAdded.length>0 && (testData.get(pax+j+"SeatSector"+sector)!=null || testData.get(pax+j+"SeatSector"+sector+"Replace")!=null)  && previouslyAdded[0]){
                seatSelected = seatNo.get(counter++).getText();
                seatDetails.put("counter",counter+"");
            }
            else if(previouslyAdded.length == 0 && (testData.get(pax+j+"SeatSector"+sector+"Extra")!=null || testData.get(pax+j+"SeatSector"+sector)!=null)){
                seatSelected = seatNo.get(counter++).getText();
                seatDetails.put("counter",counter+"");
            }
            if(seatSelected!=null){
                noOfPaxTypeWithSeat++;
                seatDetails.put(pax+j+"SeatNoForSector"+sector,seatSelected);
                System.out.println("Extra -- "+seatDetails);
            }
            seatSelected = null;
        }
        seatDetails.put("noOf"+pax+"withSeatForSector"+sector,noOfPaxTypeWithSeat+"");
        return seatDetails;
    }

    public HashMap<String, String> retrieveSeatSelectedForEachPax(HashMap<String,String> testData,boolean... previouslyAdded){
        HashMap<String,String> seatDetails = new HashMap<>();
        try {
            if(previouslyAdded.length == 0) {
                viewFirstSectorSeatMap();
            }
            int totalSectors = new Integer(testData.get("departNoOfStops"))+1;
            if(testData.get("returnDateOffset")!=null){
                totalSectors += new Integer(testData.get("returnNoOfStops"))+1;
            }
            int noOfPax;
            for(int i=1;i<=totalSectors;i++) {
                seatDetails.put("counter","0");
                if(testData.get("noOfAdult")!=null){
                    noOfPax = Integer.parseInt(testData.get("noOfAdult"));
                    if(previouslyAdded.length>0){
                        seatDetails = captureSeatNumber(testData, "adult", noOfPax,i,seatDetails,previouslyAdded);
                    }
                    else {
                        seatDetails = captureSeatNumber(testData, "adult", noOfPax, i, seatDetails);
                    }
                }
                if(testData.get("noOfTeen")!=null){
                    noOfPax = Integer.parseInt(testData.get("noOfTeen"));
                    if(previouslyAdded.length>0){
                        seatDetails = captureSeatNumber(testData, "teen", noOfPax,i,seatDetails,previouslyAdded);
                    }
                    else {
                        seatDetails = captureSeatNumber(testData, "teen", noOfPax, i, seatDetails);
                    }
                }
                if(testData.get("noOfChild")!=null){
                    noOfPax = Integer.parseInt(testData.get("noOfChild"));
                    if(previouslyAdded.length>0){
                        seatDetails = captureSeatNumber(testData, "child", noOfPax,i,seatDetails,previouslyAdded);
                    }
                    else {
                        seatDetails = captureSeatNumber(testData, "child", noOfPax, i, seatDetails);
                    }
                }
                if(totalSectors>1 && i!=totalSectors) {
                    actionUtility.hardClick(nextFlight);
                    actionUtility.waitForElementVisible(seatMap,30);
                }
            }
            reportLogger.log(LogStatus.INFO,"successfully retrieved seat selected for passengers");
            System.out.println(seatDetails);
            return seatDetails;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to retrieve seat selected for passengers");
            throw e;
        }
    }

    private void viewFirstSectorSeatMap(){
        if(actionUtility.verifyIfElementIsDisplayed(previousFlight))
            do{
                actionUtility.hardClick(previousFlight);
            }while(actionUtility.verfiyIfAnElementISEnabled(previousFlight));
    }

    private void selectParkingToDate_Time_GetQuote(HashMap<String,String> testData){
        actionUtility.waitForElementVisible(parkingTo_CalendarIcon,120);
        actionUtility.click(parkingTo_CalendarIcon);
        String[] date = actionUtility.getDateWithFullMonthName(new Integer(testData.get("parkingToDateOffset")));
        boolean flag = true;
        do {
            if (actionUtility.verifyIfElementIsDisplayed(String.format(parkingToDate, date[0] + " " + date[1] + " " + date[2]))) {
                actionUtility.click(TestManager.getDriver().findElement(By.xpath(String.format(parkingToDate, date[0] + " " + date[1] + " " + date[2]))));
                flag = false;
            }
            else{
                actionUtility.click(nextCalendar);
            }
        }while(flag);
        actionUtility.dropdownSelect(parkingToTime, ActionUtility.SelectionType.SELECTBYTEXT,testData.get("parkingToTime"));
        actionUtility.click(getCarParkingQuotes);
        reportLogger.log(LogStatus.INFO,"successfully selected date,time to get car parking quotes");
    }

    public HashMap<String,Double> selectCarParking(HashMap<String,String> testData,HashMap<String,Double>... basket){
        try{
            Double parkingAmount = 0.00;
            if(testData.get("returnDateOffset")==null){
                selectParkingToDate_Time_GetQuote(testData);
            }
            actionUtility.waitForElementVisible(selectCarpark,120);
            actionUtility.hardClick(selectCarpark.get(0));
            carRegistrationNo.sendKeys(testData.get("CarRegistrationNo"));
            actionUtility.waitForElementVisible(parkingAddingButton,90);
            if(basket.length>0){
                parkingAmount = CommonUtility.convertStringToDouble(parkingCost.get(1).getText().split(" ")[1]);
                basket[0].put("airport parking",parkingAmount);
            }
            actionUtility.hardClick(parkingAddingButton);
            actionUtility.hardSleep(100);
            actionUtility.waitForElementVisible(parkingInBasket,120);
            reportLogger.log(LogStatus.INFO,"car parking is added successfully to the booking");
            if(basket.length>0) {
                return basket[0];
            }
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to add car parking to the booking");
            throw e;
        }
        return null;

    }


    public void selectService(String service){
        //selects View cars,Book parking,Reserve seats,Book baggage,Add golf clubs,Add skis,Edit seat
        WebElement e = null;
        try {
//            actionUtility.waitForElementVisible(continueButton,120);
            actionUtility.waitForElementNotPresent(openService, 30);
            e = TestManager.getDriver().findElement(By.xpath(String.format(extraService, service)));
            actionUtility.click(e);
            reportLogger.log(LogStatus.INFO, "selected service - " + service);
        }catch(NoSuchElementException ns){
            actionUtility.scrollToViewElement(e);
            actionUtility.click(e);
        }catch(Exception ex){
            reportLogger.log(LogStatus.INFO,"unable to select service - "+service);
            throw ex;
        }
    }

    public HashMap<String,Double> addGolf(HashMap<String,String> testData,HashMap<String,Double>... basket){
        //adds golf to each pax
        //calculates cost for golf
        Double amount = 0.00;
        Double eachCost = 0.00;
        HashMap<String,Double> cost = new HashMap<>();
        try{
            int noOfAdult = Integer.parseInt(testData.get("noOfAdult"));
            int noOfTeen = 0;
            int noOfChild = 0;
            for(int i=1;i<=noOfAdult;i++){
                if(testData.get("adult"+i+"Golf")!=null && testData.get("adult"+i+"Golf").equalsIgnoreCase("yes")) {
                    eachCost = selectGolfForEachPax(testData.get("adult"+i+"Name"));
                    if(testData.get("returnDateOffset")!=null){
                        eachCost *= 2;
                    }
                    System.out.println("successfully added golf for passenger - "+testData.get("adult"+i+"Name")+" for a cost of "+eachCost);
                    reportLogger.log(LogStatus.INFO,"successfully added golf for passenger - "+testData.get("adult"+i+"Name")+" for a cost of "+eachCost);
                    amount += eachCost;
//                    actionUtility.waitForElementVisible(TestManager.getDriver().findElement(By.xpath(String.format(servicePrice,amount.toString()))),30);
                }

            }
            if(testData.get("noOfTeen")!=null){
                noOfTeen = new Integer(testData.get("noOfTeen"));
                for(int i=1;i<=noOfTeen;i++){
                    if(testData.get("teen"+i+"Golf")!=null && testData.get("teen"+i+"Golf").equalsIgnoreCase("yes")) {
                        eachCost = selectGolfForEachPax(testData.get("teen"+i+"Name"));
                        if(testData.get("returnDateOffset")!=null){
                            eachCost *= 2;
                        }
                        System.out.println("successfully added golf for passenger - "+testData.get("teen"+i+"Name")+" for a cost of "+eachCost);
                        reportLogger.log(LogStatus.INFO,"successfully added golf for passenger - "+testData.get("teen"+i+"Name")+" for a cost of "+eachCost);
                        amount += eachCost;
                        //actionUtility.waitForElementVisible(TestManager.getDriver().findElement(By.xpath(String.format(servicePrice,amount.toString()))),30);
                    }
                }
            }
            if(testData.get("noOfChild")!=null){
                noOfChild = new Integer(testData.get("noOfChild"));
                for(int i=1;i<=noOfChild;i++){
                    if(testData.get("child"+i+"Golf")!=null && testData.get("child"+i+"Golf").equalsIgnoreCase("yes")) {
                        eachCost = selectGolfForEachPax(testData.get("child" + i + "Name"));
                        if (testData.get("returnDateOffset") != null) {
                            eachCost *= 2;
                        }
                        System.out.println("successfully added golf for passenger - " + testData.get("child" + i + "Name") + " for a cost of " + eachCost);
                        reportLogger.log(LogStatus.INFO, "successfully added golf for passenger - " + testData.get("child" + i + "Name") + " for a cost of " + eachCost);
                        amount += eachCost;
                        //actionUtility.waitForElementVisible(TestManager.getDriver().findElement(By.xpath(String.format(servicePrice, amount.toString()))), 30);
                    }
                }
            }
            actionUtility.hardClick(addService);
            if(basket.length>0){
                basket[0].put("golf clubs",amount);
                cost = basket[0];
            }
            actionUtility.waitForElementNotPresent(openService,30);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to add golf for passengers");
            throw e;
        }
        return cost;
    }

    private Double selectGolfForEachPax(String paxName){
        actionUtility.click(TestManager.getDriver().findElement(By.xpath(String.format(addGolf,paxName.split(" ")[1]+" "+paxName.split(" ")[2]))));
        actionUtility.hardSleep(5000);
        return 30.00;
    }

    private Double selectSkiSnowForEachPax(String paxName,String skiSnow){
        if(skiSnow.equalsIgnoreCase("ski")) {
            actionUtility.click(TestManager.getDriver().findElement(By.xpath(String.format(addSki, paxName.split(" ")[1] + " " + paxName.split(" ")[2], "Skis"))));
        }else{
            actionUtility.click(TestManager.getDriver().findElement(By.xpath(String.format(addSki, paxName.split(" ")[1] + " " + paxName.split(" ")[2], "Snowboard"))));
        }
        actionUtility.hardSleep(5000);
        return 30.00;
    }

    public HashMap<String, Double> addSkisSnowboard(HashMap<String,String> testData,HashMap<String,Double>... basket){
        //adds golf to each pax
        //calculates cost for ski
        Double amount = 0.00;
        Double eachCost = 0.00;
        HashMap<String,Double> cost = new HashMap<>();
        try{
            int noOfAdult = Integer.parseInt(testData.get("noOfAdult"));
            int noOfTeen = 0;
            int noOfChild = 0;
            for(int i=1;i<=noOfAdult;i++){
                if(testData.get("adult"+i+"SkiSnow")!=null) {
                    eachCost = selectSkiSnowForEachPax(testData.get("adult"+i+"Name"),testData.get("adult"+i+"SkiSnow"));
                    if(testData.get("returnDateOffset")!=null){
                        eachCost *= 2;
                    }
                    System.out.println("successfully added "+testData.get("adult"+i+"SkiSnow")+" for passenger - "+testData.get("adult"+i+"Name")+" for a cost of "+eachCost);
                    reportLogger.log(LogStatus.INFO,"successfully added "+testData.get("adult"+i+"SkiSnow")+" for passenger - "+testData.get("adult"+i+"Name")+" for a cost of "+eachCost);
                    amount += eachCost;
//                    actionUtility.waitForElementVisible(TestManager.getDriver().findElement(By.xpath(String.format(servicePrice,amount.toString()))),30);
                }
            }
            if(testData.get("noOfTeen")!=null){
                noOfTeen = new Integer(testData.get("noOfTeen"));
                for(int i=1;i<=noOfTeen;i++){
                    if(testData.get("teen"+i+"SkiSnow")!=null) {
                        eachCost = selectSkiSnowForEachPax(testData.get("teen"+i+"Name"),testData.get("teen"+i+"SkiSnow"));
                        if(testData.get("returnDateOffset")!=null){
                            eachCost *= 2;
                        }
                        System.out.println("successfully added "+testData.get("teen"+i+"SkiSnow")+" for passenger - "+testData.get("teen"+i+"Name")+" for a cost of "+eachCost);
                        reportLogger.log(LogStatus.INFO,"successfully added "+testData.get("teen"+i+"SkiSnow")+" for passenger - "+testData.get("teen"+i+"Name")+" for a cost of "+eachCost);
                        amount += eachCost;
                        actionUtility.waitForElementVisible(TestManager.getDriver().findElement(By.xpath(String.format(servicePrice,amount.toString()))),30);
                    }
                }
            }
            if(testData.get("noOfChild")!=null){
                noOfChild = new Integer(testData.get("noOfChild"));
                for(int i=1;i<=noOfChild;i++){
                    if(testData.get("child"+i+"SkiSnow")!=null) {
                        eachCost = selectSkiSnowForEachPax(testData.get("child"+i+"Name"),testData.get("child"+i+"SkiSnow"));
                        if(testData.get("returnDateOffset")!=null){
                            eachCost *= 2;
                        }
                        System.out.println("added "+testData.get("child"+i+"SkiSnow")+" for passenger - "+testData.get("child"+i+"Name")+"for a cost of "+eachCost);
                        reportLogger.log(LogStatus.INFO,"successfully added "+testData.get("child"+i+"SkiSnow")+" for passenger - "+testData.get("child"+i+"Name")+" for a cost of "+eachCost);
                        amount += eachCost;
                        actionUtility.waitForElementVisible(TestManager.getDriver().findElement(By.xpath(String.format(servicePrice,amount.toString()))),30);
                    }
                }
            }
            actionUtility.hardClick(addService);
            actionUtility.waitForElementNotPresent(openService,30);
            if(basket.length>0){
                basket[0].put("skis",amount);
                cost = basket[0];
            }
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to add ski/snowboard for passengers");
            throw e;
        }
        return cost;
    }

    public HashMap<String,Double> verifyXLSeatIsNotConfirmedForTeen(HashMap<String,Double> basket) {
        try {
            actionUtility.waitForElementVisible(totalPrice, 120);
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(xlSeatForTeenError));
            basket.put("your seat",0.0);
        }catch(AssertionError ae){
            reportLogger.log(LogStatus.INFO,"the message - 'We are unable to price the services selected.' is not displayed on extras page when XL seat is selected for teen");
            throw ae;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify total service charges in the basket");
            throw e;
        }
        return basket;
    }

    public HashMap<String,Double> verifyBasket(HashMap<String,Double> basket,Boolean... editSeat){
      String service = null;
      Double expectedPrice = 0.00;
      Double actualPrice = 0.00;
      Double expectedTotalServiceCharges = 0.00;
      try{
          actionUtility.waitForElementVisible(totalPrice,120);
          if(basket.get("services")!=null){
              expectedTotalServiceCharges = basket.get("services");
          }
          if(basket.get("your seat")!=null){
              service = "seat";
              expectedPrice = basket.get("your seat");
              actualPrice = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(serviceCost,"Your seat"))).getText());
              Assert.assertEquals(actualPrice,expectedPrice);
              if(editSeat.length==0) {
                  expectedTotalServiceCharges += expectedPrice;
              }
              reportLogger.log(LogStatus.INFO,"successfully verified your seat cost in basket - "+actualPrice);
          }
          if(basket.get("hold baggage")!=null){
              service = "baggage";
              expectedPrice = basket.get("hold baggage");
              actualPrice = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(serviceCost,"Hold baggage"))).getText());
              Assert.assertEquals(actualPrice,expectedPrice);
              reportLogger.log(LogStatus.INFO,"successfully verified hold baggage cost in basket - "+actualPrice);
          }
          if(basket.get("golf clubs")!=null){
              service = "golf";
              expectedPrice = basket.get("golf clubs");
              actualPrice = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(serviceCost,"Golf clubs"))).getText());
              Assert.assertEquals(actualPrice,expectedPrice);
              expectedTotalServiceCharges += expectedPrice;
              reportLogger.log(LogStatus.INFO,"successfully verified golf clubs cost in basket - "+actualPrice);
          }
          if(basket.get("skis")!=null){
              service = "ski/snowboard";
              expectedPrice = basket.get("skis");
              actualPrice = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(serviceCost,"Skis"))).getText());
              Assert.assertEquals(actualPrice,expectedPrice);
              expectedTotalServiceCharges += expectedPrice;
              reportLogger.log(LogStatus.INFO,"successfully verified skis cost in basket - "+actualPrice);
          }
          if(basket.get("airport parking")!=null){
              service = "airport parking";
              expectedPrice = basket.get("airport parking");
              actualPrice = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(serviceCost,"Airport parking"))).getText());
              Assert.assertEquals(actualPrice,expectedPrice);
              expectedTotalServiceCharges += expectedPrice;
              reportLogger.log(LogStatus.INFO,"successfully verified airport parking cost in basket - "+actualPrice);
          }
          if(expectedTotalServiceCharges>0) {
              service = "services";
              expectedPrice = expectedTotalServiceCharges;
              actualPrice = CommonUtility.convertStringToDouble(services.getText());
              Assert.assertEquals(actualPrice,expectedPrice);
              basket.put("services",actualPrice);
              reportLogger.log(LogStatus.INFO,"successfully verified services cost in basket - "+actualPrice);
          }
          if(basket.get("fare")!=null) {
              service = "total";
              actualPrice = CommonUtility.convertStringToDouble(totalPrice.getText());
              if (expectedTotalServiceCharges > 0) {
                  expectedPrice = CommonUtility.truncateToTWODecimalPlaces(basket.get("services") + basket.get("fare"));
              } else {
                  expectedPrice = basket.get("fare");
              }
              Assert.assertEquals(actualPrice, expectedPrice);
              basket.put("total", actualPrice);
              reportLogger.log(LogStatus.INFO, "successfully verified total cost in basket "+ actualPrice);
          }
      }catch(AssertionError ae){
          System.out.println(service+" >> "+expectedPrice+" ---------- "+actualPrice);
          reportLogger.log(LogStatus.INFO,service+" charges expected - "+expectedPrice+" does not match with the displayed charges - "+actualPrice+" in the basket");
          throw ae;
      }catch(Exception e){
          reportLogger.log(LogStatus.INFO,"unable to verify total service charges in the basket");
          throw e;
      }
      return basket;
    }

    private HashMap<String, Double> replaceSeatPerSectorPerPax(HashMap<String,String> testData,int noOfPax,String paxType,int sectorNo,int noOfDepartureFlights,int noOfReturnFlights,HashMap<String,Double>... basket){
        String selectedSeat = null;
        Double totalPrice = 0.00;
        Double newPrice = 0.00;
        Double priceApplicable = 0.00;
        WebElement pax = null;
        for(int i=1;i<=noOfPax;i++){
            if(testData.get(paxType+i+"SeatSector"+sectorNo+"Replace")!=null) {
                pax = TestManager.getDriver().findElement(By.xpath(String.format(paxButton,testData.get(paxType+i+"Name"))));
                actionUtility.click(pax);
                actionUtility.hardSleep(2000);
                if (testData.get(paxType + i + "SeatSector"+sectorNo+"Replace").equalsIgnoreCase("XL")) {
                    selectedSeat = extraLegSeat.get(0).getAttribute("id").split("seat")[1];
                    actionUtility.click(extraLegSeat.get(0));
                    actionUtility.hardSleep(2000);
                }
                else
                {
                    selectedSeat = standardSeat.get(1).getAttribute("id").split("seat")[1];
                    actionUtility.click(standardSeat.get(1));
                    actionUtility.hardSleep(2000);
                }
                reportLogger.log(LogStatus.INFO,"successfully selected seat - "+selectedSeat+" for passenger "+paxType+i+" - "+testData.get(paxType+i+"Name")+" for coupon "+sectorNo);
                newPrice = calculateSeatCost(testData,testData.get(paxType+i+"SeatSector"+sectorNo+"Replace"),sectorNo,noOfDepartureFlights,noOfReturnFlights,pax,testData.get(paxType+i+"Name"));
                totalPrice += newPrice;
                if(basket.length>0){
                    basket[0].put(("your seat"),basket[0].get("your seat") - CommonUtility.truncateToTWODecimalPlaces(basket[0].get(paxType+i+"SeatSector"+sectorNo+"Price")));
                    basket[0].put(("services"),basket[0].get("services") - CommonUtility.truncateToTWODecimalPlaces(basket[0].get(paxType+i+"SeatSector"+sectorNo+"Price")));
                    basket[0].put(paxType+i+"SeatSector"+sectorNo+"Price",newPrice);
                }

                System.out.println("total seat price for  - "+paxType+" is "+totalPrice);
            }
        }
        if(basket.length>0){
            basket[0].put("your seat",basket[0].get("your seat")+totalPrice);
            basket[0].put("services",basket[0].get("services")+totalPrice);
            return basket[0];
        }
        return null;
    }

    public HashMap<String, Double> editSeatFromExtras(HashMap<String,String> testData,HashMap<String,Double>... basket){
        //selects seat for each pax
        //verifies seat charges for just fly,get more, all in - chargeable or free of charge
        //returns basket with seat cost for each pax, total seat price.
        try{
            actionUtility.waitForElementVisible(seatMap,120);
            actionUtility.hardSleep(1000);
//            actionUtility.click(flightTabHeader.get(0));
            viewFirstSectorSeatMap();
            actionUtility.hardSleep(1000);
            int totalSectors = new Integer(testData.get("departNoOfStops"))+1;
            int noOfDepartureFlights = totalSectors;
            int noOfReturnFlights = 0;
            Double seatPrice = 0.00;
            if(testData.get("returnDateOffset")!=null){
                totalSectors += new Integer(testData.get("returnNoOfStops"))+1;
                noOfReturnFlights = totalSectors - noOfDepartureFlights;
            }
            int noOfAdult =0,noOfTeen = 0,noOfChild = 0;
            for (int i = 1; i <= totalSectors; i++) {
                if(testData.get("noOfAdult")!=null && Integer.parseInt(testData.get("noOfAdult"))>0) {
                    noOfAdult = Integer.parseInt(testData.get("noOfAdult"));
                    if(basket.length>0){
                        basket[0] = replaceSeatPerSectorPerPax(testData, noOfAdult, "adult", i, noOfDepartureFlights, noOfReturnFlights,basket[0]);
                    }else {
                        replaceSeatPerSectorPerPax(testData, noOfAdult, "adult", i, noOfDepartureFlights, noOfReturnFlights);
                    }
                }
                if(testData.get("noOfTeen")!=null) {
                    noOfTeen = Integer.parseInt(testData.get("noOfTeen"));
                    if(basket.length>0) {
                        basket[0] = replaceSeatPerSectorPerPax(testData, noOfTeen, "teen", i, noOfDepartureFlights, noOfReturnFlights,basket[0]);
                    }else{
                        replaceSeatPerSectorPerPax(testData, noOfTeen, "teen", i, noOfDepartureFlights, noOfReturnFlights);
                    }
                }
                if(testData.get("noOfChild")!=null) {
                    noOfChild = Integer.parseInt(testData.get("noOfChild"));
                    if(basket.length>0) {
                        basket[0] = replaceSeatPerSectorPerPax(testData, noOfChild, "child", i, noOfDepartureFlights, noOfReturnFlights,basket[0]);
                    }else {
                        replaceSeatPerSectorPerPax(testData, noOfChild, "child", i, noOfDepartureFlights, noOfReturnFlights);
                    }
                }
                if (totalSectors > 1 && i != totalSectors) {
                    actionUtility.hardClick(nextFlight);
                    actionUtility.waitForElementVisible(seatMap, 30);
                }
            }
            if(basket.length>0){
                System.out.println(basket[0]);
                return basket[0];
            }
            return null;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select seat");
            throw e;
        }
    }

    public void continueAfterSeatSelection(){
        try{
            actionUtility.click(continueWithSelectedSeat);
            actionUtility.waitForElementNotPresent(openService,30);
            reportLogger.log(LogStatus.INFO,"successfully added or edited seat on extras");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to continue with selected seat");
            throw e;
        }
    }

    public HashMap<String, Double> addSeatFromExtras(HashMap<String,String> testData,HashMap<String,Double>... basket){
        //selects seat for each pax
        //verifies seat charges for just fly,get more, all in - chargeable or free of charge
        //returns basket with seat cost for each pax, total seat price.
        try{
            actionUtility.waitForElementVisible(seatMap,120);
            actionUtility.hardSleep(5000);
//            actionUtility.click(flightTabHeader.get(0));
            viewFirstSectorSeatMap();
            actionUtility.hardSleep(4000);
            int totalSectors = new Integer(testData.get("departNoOfStops"))+1;
            int noOfDepartureFlights = totalSectors;
            int noOfReturnFlights = 0;
            Double seatPrice = 0.00;
            if(testData.get("returnDateOffset")!=null){
                totalSectors += new Integer(testData.get("returnNoOfStops"))+1;
                noOfReturnFlights = totalSectors - noOfDepartureFlights;
            }
            int noOfAdult =0,noOfTeen = 0,noOfChild = 0;
            for (int i = 1; i <= totalSectors; i++) {
                if(testData.get("noOfAdult")!=null && Integer.parseInt(testData.get("noOfAdult"))>0) {
                    noOfAdult = Integer.parseInt(testData.get("noOfAdult"));
                    seatPrice = selectSeatPerSectorPerPax(testData, noOfAdult, "adult", i, noOfDepartureFlights, noOfReturnFlights);
                }
                if(testData.get("noOfTeen")!=null) {
                    noOfTeen = Integer.parseInt(testData.get("noOfTeen"));
                    seatPrice += selectSeatPerSectorPerPax(testData, noOfTeen, "teen", i, noOfDepartureFlights, noOfReturnFlights);
                }
                if(testData.get("noOfChild")!=null) {
                    noOfChild = Integer.parseInt(testData.get("noOfChild"));
                    seatPrice += selectSeatPerSectorPerPax(testData, noOfChild, "child", i, noOfDepartureFlights, noOfReturnFlights);
                }
                    if (basket.length > 0) {
                        if (basket[0].get("your seat") != null) {
                            seatPrice += basket[0].get("your seat");
                            basket[0].put("your seat", seatPrice);
                        } else
                            basket[0].put("your seat", seatPrice);
                    }
                if (totalSectors > 1 && i != totalSectors) {
                    actionUtility.hardClick(nextFlight);
                    actionUtility.waitForElementVisible(seatMap, 30);
                }
            }
            if(basket.length>0){
                System.out.println(basket[0]);
                return basket[0];
            }
            return null;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select seat");
            throw e;
        }
    }

    private Double selectSeatPerSectorPerPax(HashMap<String,String> testData,int noOfPax,String paxType,int sectorNo,int noOfDepartureFlights,int noOfReturnFlights){
        String selectedSeat = null;
        Double totalPrice = 0.00;
        WebElement pax = null;
        for(int i=1;i<=noOfPax;i++){
            if(testData.get(paxType+i+"SeatSector"+sectorNo+"Extra")!=null) {
                System.out.println(testData.get(paxType+i+"Name")+" and "+testData.get(paxType+i+"SeatSectorExtra"+sectorNo));
                pax = TestManager.getDriver().findElement(By.xpath(String.format(paxButton,testData.get(paxType+i+"Name"))));
                actionUtility.click(pax);
                actionUtility.hardSleep(2000);
                if (testData.get(paxType + i + "SeatSector"+sectorNo+"Extra").equalsIgnoreCase("XL")) {
                    selectedSeat = extraLegSeat.get(0).getAttribute("id").split("seat")[1];
                    actionUtility.click(extraLegSeat.get(0));
                    actionUtility.hardSleep(2000);
                }
                else
                {
                    selectedSeat = standardSeat.get(1).getAttribute("id").split("seat")[1];
                    actionUtility.click(standardSeat.get(1));
                    actionUtility.hardSleep(2000);
                }
                reportLogger.log(LogStatus.INFO,"successfully selected seat - "+selectedSeat+" for passenger "+paxType+i+" - "+testData.get(paxType+i+"Name")+" for coupon "+sectorNo);
                totalPrice += calculateSeatCost(testData,testData.get(paxType+i+"SeatSector"+sectorNo+"Extra"),sectorNo,noOfDepartureFlights,noOfReturnFlights,pax,testData.get(paxType+i+"Name"));
                System.out.println("total seat price for  - "+paxType+" is "+totalPrice);
            }
        }
        return totalPrice;
    }

    private Double calculateSeatCost(HashMap<String,String> testData,String seatType,int sectorNo,int noOfDepartureFlights,int noOfReturnFlights,WebElement pax,String paxname){
        //verifies seat price for  pax according to ticket type
        // returns price of seat selected for pax
        Double price = 0.00;
        if(seatType.equalsIgnoreCase("XL")) {
            if (sectorNo <= noOfDepartureFlights) {
                if (testData.get("departTicketType").equalsIgnoreCase("just fly")) {
                    price = CommonUtility.convertStringToDouble(pax.findElement(By.cssSelector(seatPrice)).getText());
                    if(testData.get("sector"+sectorNo+"XLSeatPrice")!=null){
                        try{
                            Assert.assertEquals(price,Double.parseDouble(testData.get("sector"+sectorNo+"XLSeatPrice")));
                            reportLogger.log(LogStatus.INFO,"XL seat price is as expected - "+price+" for sector "+sectorNo);
                        }catch (AssertionError ae) {
                            reportLogger.log(LogStatus.INFO, "displayed XL seat price for ticket type - just fly for sector - " + sectorNo+" is "+price+" and not "+testData.get("sector"+sectorNo+"XLSeatPrice")+" as expected");
                            throw ae;
                        }
                    }
                } else if (testData.get("departTicketType").equalsIgnoreCase("get more")) {
                    price = CommonUtility.convertStringToDouble(pax.findElement(By.cssSelector(seatPrice)).getText());
                    if(testData.get("sector"+sectorNo+"XLSeatPrice")!=null){
                        try{
                            Assert.assertEquals(price,Double.parseDouble(testData.get("sector"+sectorNo+"XLSeatPrice")));
                            reportLogger.log(LogStatus.INFO,"XL seat price is as expected - "+price+" for sector "+sectorNo);
                        }catch (AssertionError ae) {
                            reportLogger.log(LogStatus.INFO, "displayed XL seat price for ticket type - get more for sector - " + sectorNo+" is "+price+" and not "+testData.get("sector"+sectorNo+"XLSeatPrice")+" as expected");
                            throw ae;
                        }
                    }
                } else if (testData.get("departTicketType").equalsIgnoreCase("all in")) {
                    try {
                        Assert.assertTrue(pax.findElement(By.cssSelector(seatPrice)).getText().contains("Free of charge"));
                    } catch (AssertionError ae) {
                        reportLogger.log(LogStatus.INFO, "XL seat is not free of charge for ticket type - all in for sector - " + sectorNo);
                        throw ae;
                    }
                }
            }
            else if(noOfReturnFlights > 0 && sectorNo > noOfDepartureFlights){
                if (testData.get("returnTicketType").equalsIgnoreCase("just fly")) {
                    price = CommonUtility.convertStringToDouble(pax.findElement(By.cssSelector(seatPrice)).getText());
                    if(testData.get("sector"+sectorNo+"XLSeatPrice")!=null){
                        try{
                            Assert.assertEquals(price,Double.parseDouble(testData.get("sector"+sectorNo+"XLSeatPrice")));
                            reportLogger.log(LogStatus.INFO,"XL seat price is as expected - "+price+" for sector "+sectorNo);
                        }catch (AssertionError ae) {
                            reportLogger.log(LogStatus.INFO, "displayed XL seat price for ticket type - just fly for sector - " + sectorNo+" is "+price+" and not "+testData.get("sector"+sectorNo+"XLSeatPrice")+" as expected");
                            throw ae;
                        }
                    }
                } else if (testData.get("returnTicketType").equalsIgnoreCase("get more")) {
                    price = CommonUtility.convertStringToDouble(pax.findElement(By.cssSelector(seatPrice)).getText());
                    if(testData.get("sector"+sectorNo+"XLSeatPrice")!=null){
                        try{
                            Assert.assertEquals(price,Double.parseDouble(testData.get("sector"+sectorNo+"XLSeatPrice")));
                            reportLogger.log(LogStatus.INFO,"XL seat price is as expected - "+price+" for sector "+sectorNo);
                        }catch (AssertionError ae) {
                            reportLogger.log(LogStatus.INFO, "displayed XL seat price for ticket type - get more for sector - " + sectorNo+" is "+price+" and not "+testData.get("sector"+sectorNo+"XLSeatPrice")+" as expected");
                            throw ae;
                        }
                    }
                } else if (testData.get("returnTicketType").equalsIgnoreCase("all in")) {
                    try {
                        Assert.assertTrue(pax.findElement(By.cssSelector(seatPrice)).getText().contains("Free of charge"));
                    } catch (AssertionError ae) {
                        reportLogger.log(LogStatus.INFO, "XL seat is not free of charge for ticket type - all in for sector - " + sectorNo);
                        throw ae;
                    }
                }
            }
        }
        else if(seatType.equalsIgnoreCase("standard")){
            if (sectorNo <= noOfDepartureFlights) {
                if (testData.get("departTicketType").equalsIgnoreCase("just fly")) {
                    price = CommonUtility.convertStringToDouble(pax.findElement(By.cssSelector(seatPrice)).getText());
                    if(testData.get("sector"+sectorNo+"StandardSeatPrice")!=null){
                        try{
                            Assert.assertEquals(price,Double.parseDouble(testData.get("sector"+sectorNo+"StandardSeatPrice")));
                            reportLogger.log(LogStatus.INFO,"standard seat price is as expected - "+price+" for sector "+sectorNo);
                        }catch (AssertionError ae) {
                            reportLogger.log(LogStatus.INFO, "displayed standard seat price for ticket type - just fly for sector - " + sectorNo+" is "+price+" and not "+testData.get("sector"+sectorNo+"XLSeatPrice")+" as expected");
                            throw ae;
                        }
                    }
                } else if (testData.get("departTicketType").equalsIgnoreCase("get more")) {
                    try {
                        Assert.assertTrue(pax.findElement(By.cssSelector(seatPrice)).getText().contains("Free of charge"));
                    } catch (AssertionError ae) {
                        reportLogger.log(LogStatus.INFO, "standard seat is not free of charge for ticket type - get more for sector - " + sectorNo);
                        throw ae;
                    }
                } else if (testData.get("departTicketType").equalsIgnoreCase("all in")) {
                    try {
                        Assert.assertTrue(pax.findElement(By.cssSelector(seatPrice)).getText().contains("Free of charge"));
                    } catch (AssertionError ae) {
                        reportLogger.log(LogStatus.INFO, "standard seat is not free of charge for ticket type - all in for sector - " + sectorNo);
                        throw ae;
                    }
                }
            }
            else if(noOfReturnFlights > 0 && sectorNo > noOfDepartureFlights){
                if (testData.get("returnTicketType").equalsIgnoreCase("just fly")) {
                    price = CommonUtility.convertStringToDouble(pax.findElement(By.cssSelector(seatPrice)).getText());
                    if(testData.get("sector"+sectorNo+"StandardSeatPrice")!=null){
                        try{
                            Assert.assertEquals(price,Double.parseDouble(testData.get("sector"+sectorNo+"StandardSeatPrice")));
                            reportLogger.log(LogStatus.INFO,"standard seat price is as expected - "+price+" for sector "+sectorNo);
                        }catch (AssertionError ae) {
                            reportLogger.log(LogStatus.INFO, "displayed standard seat price for ticket type - just fly for sector - " + sectorNo+" is "+price+" and not "+testData.get("sector"+sectorNo+"XLSeatPrice")+" as expected");
                            throw ae;
                        }
                    }
                } else if (testData.get("returnTicketType").equalsIgnoreCase("get more")) {
                    try {
                        Assert.assertTrue(pax.findElement(By.cssSelector(seatPrice)).getText().contains("Free of charge"));
                    } catch (AssertionError ae) {
                        reportLogger.log(LogStatus.INFO, "standard seat is not free of charge for ticket type - get more for sector - " + sectorNo);
                        throw ae;
                    }
                } else if (testData.get("returnTicketType").equalsIgnoreCase("all in")) {
                    try {
                        Assert.assertTrue(pax.findElement(By.cssSelector(seatPrice)).getText().contains("Free of charge"));
                    } catch (AssertionError ae) {
                        reportLogger.log(LogStatus.INFO, "standard seat is not free of charge for ticket type - all in for sector - " + sectorNo);
                        throw ae;
                    }
                }
            }
        }
        System.out.println("seat price for passenger - "+paxname+"is "+price);
        reportLogger.log(LogStatus.INFO,"seat price for passenger - "+paxname+" is "+price+" for sector "+sectorNo);
        return price;
    }


}
