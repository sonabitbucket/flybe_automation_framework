package efly.business_logics.online;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import utilities.ActionUtility;
import utilities.TestManager;

import java.time.Month;
import java.util.HashMap;

import static utilities.ActionUtility.SelectionType.SELECTBYTEXT;
import static utilities.ActionUtility.SelectionType.SELECTBYVALUE;

public class EFLYCheckinPassengerDetails {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public EFLYCheckinPassengerDetails(){
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    //API details

    @FindBy(xpath = "//select[contains(@id,'apis_field_Country')]")
    private WebElement nationality;

    @FindBy(xpath = "//button[@title='Save Nationality']")
    private WebElement saveNationality;

    @FindBy(xpath = "//select[contains(@id,'day_apis_field_DateOfBirth')]")
    private WebElement dayDob;

    @FindBy(xpath = "//select[contains(@id,'month_apis_field_DateOfBirth')]")
    private WebElement monthDob;

    @FindBy(xpath = "//select[contains(@id,'year_apis_field_DateOfBirth')]")
    private WebElement yearDob;

    @FindBy(xpath = "//button[@title='Save Personal details']")
    private WebElement saveDob;

    @FindBy(xpath = "//select[contains(@id,'apis_field_IdentityDocument')]")
    private WebElement identityDoc;

    @FindBy(xpath = "//input[contains(@id,'apis_field_Number')]")
    private WebElement number;

    @FindBy(xpath = "//select[contains(@id,'day_apis_field_ExpiryDate')]")
    private WebElement dayExpiry;

    @FindBy(xpath = "//select[contains(@id,'month_apis_field_ExpiryDate')]")
    private WebElement monthExpiry;

    @FindBy(xpath = "//select[contains(@id,'year_apis_field_ExpiryDate')]")
    private WebElement yearExpiry;

    @FindBy(xpath = "//select[contains(@id,'apis_field_CountryOfIssue')]")
    private WebElement countryIssue;

    @FindBy(xpath = "//button[@title='Save Identity Document']")
    private WebElement saveID;

    @FindBy(className = "checkbox-skin")
    private WebElement iConfirmChkBox;

    @FindBy(xpath = "//button[contains(@id,'apis_navigation')]")
    private WebElement confirmAPI;

    @FindBy(xpath = "//label[span[span[text()='Female']]]/span[1]")
    private WebElement gender;

    @FindBy(css = "button[title='Save Personal details']")
    private WebElement saveGender;

    @FindBy(xpath = "//span[text()='Print boarding pass']")
    private WebElement printBoardingPassSection;

    private void navigateToBoardingPass(){
        try{
            actionUtility.click(iConfirmChkBox);
            actionUtility.click(confirmAPI);
            reportLogger.log(LogStatus.INFO,"successfully navigated to boarding pass section from passenger details section");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to boarding pass section from passenger details section");
            throw e;
        }
    }



    public void populateAPIDetails(HashMap<String,String> testData,Boolean... apiDetailsAdded){
        //populates API details
        try{
            if(testData.get("paxToBeSelectedForCheckin")!=null && apiDetailsAdded[0]){
                navigateToBoardingPass();
                actionUtility.verifyIfElementIsDisplayed(printBoardingPassSection,30);
                return;
            }
            int noOfAdults = 0,noOfInfant = 0,noOfChild = 0,noOfTeen = 0;
            if(testData.get("noOfAdult")!=null){
                noOfAdults = Integer.parseInt(testData.get("noOfAdult"));
            }
            if(testData.get("noOfInfant")!=null) {
               noOfInfant = Integer.parseInt(testData.get("noOfInfant"));
            }
            if (testData.get("noOfChild")!=null) {
                noOfChild = Integer.parseInt(testData.get("noOfChild"));
            }
            if (testData.get("noOfTeen")!=null) {
                noOfTeen = Integer.parseInt(testData.get("noOfTeen"));
            }
            int infantCountAPIHandled = 0;
            for (int i = 1; i <= noOfAdults; i++) {
                if(apiDetailsAdded.length==0 || !apiDetailsAdded[0]) {
                    enterNationality("adult", i, testData);
                    enterDateOfBirth("adult", i, testData);
                    enterIdentityDoc("adult", i, testData);
                }
                navigateToBoardingPass();
                    if (noOfInfant>0 && infantCountAPIHandled<noOfInfant) {
                        if(apiDetailsAdded.length==0 || !apiDetailsAdded[0]) {
                            enterNationality("infant", i, testData);
                            enterDateOfBirth("infant", i, testData);
                            enterIdentityDoc("infant", i, testData);
                            infantCountAPIHandled++;
                        }
                        navigateToBoardingPass();
                    }
            }
            for (int i = 1; i <= noOfChild; i++) {
                if(apiDetailsAdded.length==0 || !apiDetailsAdded[0]) {
                    enterNationality("child", i, testData);
                    enterDateOfBirth("child", i, testData);
                    enterIdentityDoc("child", i, testData);
                }
                navigateToBoardingPass();
            }
            for (int i = 1; i <= noOfTeen; i++) {
                if(apiDetailsAdded.length==0 || !apiDetailsAdded[0]) {
                    enterNationality("teen", i, testData);
                    enterDateOfBirth("teen", i, testData);
                    enterIdentityDoc("teen", i, testData);
                }
                navigateToBoardingPass();
            }
            reportLogger.log(LogStatus.INFO,"successfully entered and saved passenger details");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to enter and save advance passenger details");
            throw e;
        }
    }

    private void enterNationality(String paxType,int paxCount,HashMap<String,String> testData){
        //populates and saves nationality
        try{
            actionUtility.waitForElementVisible(nationality,10);
            actionUtility.dropdownSelect(nationality,SELECTBYTEXT,testData.get(paxType+paxCount+"Nationality"));
            actionUtility.click(saveNationality);
            reportLogger.log(LogStatus.INFO,"successfully saved nationality");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to enter and save nationality");
            throw e;
        }
    }

    private void enterDateOfBirth(String paxType,int paxCount,HashMap<String,String> testData){
        //populates and saves DOB
        try{
            //todo month number
            if(paxType!="infant") {
                String[] dateOfBirth = testData.get(paxType + paxCount + "DOB").split(" ");
                System.out.println(dateOfBirth[1]);
                actionUtility.dropdownSelect(dayDob, SELECTBYTEXT, dateOfBirth[0]);
                actionUtility.dropdownSelect(monthDob, SELECTBYVALUE, String.valueOf( Month.valueOf(dateOfBirth[1].toUpperCase()).getValue()- 1));
                actionUtility.dropdownSelect(yearDob, SELECTBYTEXT, dateOfBirth[2]);

            }
            actionUtility.click(saveDob);
            reportLogger.log(LogStatus.INFO,"successfully saved DOB");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to enter and save DOB");
            throw e;
        }
    }

    private void enterIdentityDoc(String paxType,int paxCount,HashMap<String,String> testData){
        //enters and saves Identity doc
        //todo month no. from month name
        try{
            actionUtility.dropdownSelect(identityDoc,SELECTBYTEXT,testData.get(paxType+paxCount+"Document"));
            actionUtility.sendKeys(number,testData.get(paxType+paxCount+"DocumentNo"));
            if(testData.get(paxType+paxCount+"IdentityDoc").equalsIgnoreCase("Identity Document")){
                actionUtility.dropdownSelect(nationality,SELECTBYTEXT,testData.get("Nationality"));
            }
            String[] expiryDate = testData.get(paxType+paxCount+"APIExpiry").split(" ");
            actionUtility.dropdownSelect(dayExpiry,SELECTBYTEXT,expiryDate[0]);
            actionUtility.dropdownSelect(monthExpiry,SELECTBYVALUE,String.valueOf(Integer.valueOf(expiryDate[1])-1));
            actionUtility.dropdownSelect(yearExpiry,SELECTBYTEXT,expiryDate[2]);
            actionUtility.dropdownSelect(countryIssue,SELECTBYTEXT,testData.get("IssuingCountry"));
            actionUtility.click(saveID);
            reportLogger.log(LogStatus.INFO,"successfully entered and saved Identity Document details");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to enter and save Identity Document");
            throw e;
        }
    }

    public void enterGender(HashMap<String,String> testData,int noOfPassengers){
        //enters genders in case passengers gender information is unavailable
        try{
            actionUtility.waitForPageLoad(10);
            /*for(int i=1;i<=noOfPassengers;i++) {                                                                      //todo: for multiple passengers without gender
                if() {
            */        if (testData.get("Passenger" + 1 + "Gender").equalsIgnoreCase("female")) {
                        actionUtility.hardClick(gender);
                    }
                /*}
            }*/
            actionUtility.click(saveGender);
            reportLogger.log(LogStatus.INFO,"successfully entered and saved gender");
            navigateToBoardingPass();
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to enter and save gender information");
            throw e;
        }
    }
}
