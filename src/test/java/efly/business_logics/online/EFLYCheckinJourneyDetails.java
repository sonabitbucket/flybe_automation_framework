package efly.business_logics.online;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.TestManager;

public class EFLYCheckinJourneyDetails {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public EFLYCheckinJourneyDetails(){
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(xpath = "//span[text()='Confirm check-in']")
    private WebElement continueToPassengerDetailsButton;

    @FindBy(xpath = "//button[contains(@id,'flowForwardList_navigation')]")
    private WebElement skipEarlierFlightOption;

    public void navigateToPassengerDetailsPage(){
        try{
            actionUtility.click(continueToPassengerDetailsButton);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to Passenger Details page during checkin");
            throw e;
        }
    }

    public void navigateToDangerousGoodsSkippingEarlierFlightOptonForWandRClass(){
        //will select remain on the original flight and continue to dangerous goods
        try{
            if(actionUtility.verifyIfElementIsDisplayed(skipEarlierFlightOption,30)) {
                actionUtility.click(skipEarlierFlightOption);
            }
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to skip earlier flight option and navigate to dangerous goods section");
            throw e;
        }
    }


}
