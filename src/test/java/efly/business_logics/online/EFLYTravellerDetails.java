package efly.business_logics.online;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.CommonUtility;
import utilities.DataUtility;
import utilities.TestManager;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane;
import java.util.HashMap;
import java.util.List;

import static utilities.ActionUtility.SelectionType.SELECTBYTEXT;
import static utilities.ActionUtility.SelectionType.SELECTBYVALUE;

/**
 * Created by Sona on 3/27/2019.
 */
public class EFLYTravellerDetails {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public EFLYTravellerDetails(){
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    //personal details
    @FindBy(css = "input[id*='ADT-ADS_NUMBER']")
    private List<WebElement> adsADT;

    @FindBy(css = "input[id*='ADT_INF-ADS_NUMBER']")
    private List<WebElement> adsINF;

    @FindBy(css = "input[id*='CHD-ADS_NUMBER']")
    private List<WebElement> adsCHD;

    @FindBy(css = "input[id*='YTH-ADS_NUMBER']")
    private List<WebElement> adsYTH;

    @FindBy(css = "select[id*='ADT-IDEN_TitleCode']")
    private List<WebElement> titleADT;

    @FindBy(css = "input[id*='ADT-IDEN_FirstName']")
    private List<WebElement> firstNameADT;

    @FindBy(css = "input[id*='ADT-IDEN_LastName']")
    private List<WebElement> lastNameADT;

    @FindBy(css = "input[id*='ADT_INF-IDEN_FirstName']")
    private List<WebElement> firstNameINF;

    @FindBy(css = "input[id*='ADT_INF-IDEN_LastName']")
    private List<WebElement> lastNameINF;

    @FindBy(css = "input[id*='ADT_INF-IDEN_DateOfBirth-DateDay']")
    private List<WebElement> birthDayINF;

    @FindBy(css = "select[id*='ADT_INF-IDEN_DateOfBirth-DateMonth']")
    private List<WebElement> birthMonthINF;

    @FindBy(css = "input[id*='ADT_INF-IDEN_DateOfBirth-DateYear']")
    private List<WebElement> birthYearINF;

    @FindBy(css = "select[id*='YTH-IDEN_TitleCode']")
    private List<WebElement> titleTeen;

    @FindBy(css = "input[id*='YTH-IDEN_FirstName']")
    private List<WebElement> firstNameTeen;

    @FindBy(css = "input[id*='YTH-IDEN_LastName']")
    private List<WebElement> lastNameTeen;

    @FindBy(css = "select[id*='CHD-IDEN_TitleCode']")
    private List<WebElement> titleChild;

    @FindBy(css = "input[id*='CHD-IDEN_FirstName']")
    private List<WebElement> firstNameChild;

    @FindBy(css = "input[id*='CHD-IDEN_LastName']")
    private List<WebElement> lastNameChild;

    @FindBy(css = "input[id*='CHD-IDEN_DateOfBirth-DateDay']")
    private List<WebElement> birthDayChild;

    @FindBy(css = "select[id*='CHD-IDEN_DateOfBirth-DateMonth']")
    private List<WebElement> birthMonthChild;

    @FindBy(css = "input[id*='CHD-IDEN_DateOfBirth-DateYear']")
    private List<WebElement> birthYearChild;

    //Bags
    private String bagCost = "//section[div[contains(@id,'%s')]]//button[span[span[contains(text(),'%s')]]]//span[@class='price-value']";

    private String adultBags= "//section[div[contains(@id,'paneltravellerCardIdADT')]]//button[span[span[contains(text(),'%s')]]]";

    private String teenBags = "//section[div[contains(@id,'paneltravellerCardIdYTH')]]//button[span[span[contains(text(),'%s')]]]";

    private String childBags = "//section[div[contains(@id,'paneltravellerCardIdCHD')]]//button[span[span[contains(text(),'%s')]]]";

    String bagButton = "section[id*='%s'] div[class*='baggage-selector-option-list-container'] button";

    String bagButtonTitle = "section[id*='%s'] div[class*='baggage-selector-option-list-container'] button span[class*='title']";

    String bagButtonPriceStatus = "section[id*='ADT'] div[class*='baggage-selector-option-list-container'] button[id*='baggage-selector-option']>span[class*='price-']";

    String bagButtonDescription = "section[id*='%s'] div[class*='baggage-selector-option-list-container'] button span[class*='description']";

    //Booker contact information
    @FindBy(css = "div[class='booker-selector'] button")
    private List<WebElement> booker;

    @FindBy(css = "input[id*='apimTravellers-contactInformation-PhoneMobileCode']")
    private WebElement bookerPhoneCode;

    @FindBy(xpath = "//span[span[text()='Suggested values']]//li[1]")
    private WebElement selectPhoneCode;

    @FindBy(name = "PhoneMobile")
    private WebElement bookerPhoneNumber;

    @FindBy(name = "Email")
    private WebElement bookerEmail;

    @FindBy(name = "EmailConfirm")
    private WebElement bookerEmailConfirmation;

    @FindBy(css = "input[id*='apimTravellers-contactInformation-PhoneSmsNotifCode']")
    private WebElement paxPhoneCode;

    @FindBy(name = "PhoneSmsNotif")
    private WebElement paxPhoneNumber;

    @FindBy(name = "EMailNotif")
    private WebElement paxEmail;

    //marketing preference
    private String mktPref = "//div[contains(@class,'consent-button-container')]/button[span[contains(text(),'%s')]]";

    //Special Assistance
    @FindBy (xpath = "//section[contains(@id,'ADT')]//span[@class='assistance-toggle-text']")
    private List<WebElement> specialAssistanceCheckBoxADT;

    @FindBy (xpath = "//section[contains(@id,'ADT')]//div[contains(@class,'assistance-at-airport-option-bottom')]//span[@class='switch']")
    private List<WebElement> buttonWCHRADT;

    @FindBy(xpath = "//section[contains(@id,'ADT')]//div[contains(@class,'assistance-at-airport-option-top')]//span[@class='switch']")
    private List<WebElement> buttonWCHSADT;

    @FindBy(xpath = "//section[contains(@id,'ADT')]//div[contains(@class,'assistance-at-airport-option-onBoard')]//span[@class='switch']")
    private List<WebElement> buttonWCHCADT;

    @FindBy(xpath = "//section[contains(@id,'ADT')]//button[@class='btn confirm']")
    private List<WebElement> confirmRequestADT;

    @FindBy (xpath = "//section[contains(@id,'YTH')]//span[@class='assistance-toggle-text']")
    private List<WebElement> specialAssistanceCheckBoxYTH;

    @FindBy (xpath = "//section[contains(@id,'YTH')]//div[contains(@class,'assistance-at-airport-option-bottom')]//span[@class='switch']")
    private List<WebElement> buttonWCHRYTH;

    @FindBy(xpath = "//section[contains(@id,'YTH')]//div[contains(@class,'assistance-at-airport-option-top')]//span[@class='switch']")
    private List<WebElement> buttonWCHSYTH;

    @FindBy(xpath = "//section[contains(@id,'YTH')]//div[contains(@class,'assistance-at-airport-option-onBoard')]//span[@class='switch']")
    private List<WebElement> buttonWCHCYTH;

    @FindBy(xpath = "//section[contains(@id,'YTH')]//button[@class='btn confirm']")
    private List<WebElement> confirmRequestYTH;

    @FindBy (xpath = "//section[contains(@id,'CHD')]//span[@class='assistance-toggle-text']")
    private List<WebElement> specialAssistanceCheckBoxCHD;

    @FindBy (xpath = "//section[contains(@id,'CHD')]//div[contains(@class,'assistance-at-airport-option-bottom')]//span[@class='switch']")
    private List<WebElement> buttonWCHRCHD;

    @FindBy(xpath = "//section[contains(@id,'CHD')]//div[contains(@class,'assistance-at-airport-option-top')]//span[@class='switch']")
    private List<WebElement> buttonWCHSCHD;

    @FindBy(xpath = "//section[contains(@id,'CHD')]//div[contains(@class,'assistance-at-airport-option-onBoard')]//span[@class='switch']")
    private List<WebElement> buttonWCHCCHD;

    @FindBy(xpath = "//section[contains(@id,'CHD')]//button[@class='btn confirm']")
    private List<WebElement> confirmRequestCHD;

    //basket
    private String serviceCost = "//div[h3[contains(text(),'%s')]]//span[@class='tripsummary-price-amount-text']";

    @FindBy(className = "price-details-services-amount-value")
    private WebElement services;

    @FindBy(css = "div[class*='tripsummary-price-total'] span[class='tripsummary-price-amount-text']")
    private WebElement totalPrice;

    @FindBy(xpath = "//span[text()='continue']")
    private WebElement continueButton;
/*
    //logged In User
    @FindBy (xpath = "//span[@name='PhoneMobileCode']")
    private WebElement contactCountryCode;*/

    //frequent flyer

    @FindBy(linkText = "Frequent Flyer number")
    private WebElement frequentFlyerNumberLink;

    @FindBy(css = "input[placeholder='Enter your Number']")
    private WebElement getFrequentFlyerNumberField;

    public void addFrequentFlyerNumber(HashMap<String,String> testData){
        try{
            actionUtility.click(frequentFlyerNumberLink);
            actionUtility.sendKeys(getFrequentFlyerNumberField,testData.get("frequentFlyerNumber"));
            reportLogger.log(LogStatus.INFO,"successfully added frequent flyer number");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to add frequent flyer number");
            throw e;
        }
    }

    public void verifyBagIsNotAvailableforAirFrance(HashMap<String,String> testData){
        //verifies  bag section is not displyed for Air France
        try{
            int noOfChild = 0;
            int noOfTeen = 0;
            int noOfInfant = 0;
            int noOfAdult = Integer.parseInt(testData.get("noOfAdult"));
            if(testData.get("noOfTeen")!=null){
                noOfTeen = Integer.parseInt(testData.get("noOfTeen"));
            }
            if(testData.get("noOfChild")!=null){
                noOfChild = Integer.parseInt(testData.get("noOfChild"));
            }
            if(testData.get("noOfInfant")!=null){
                noOfChild = Integer.parseInt(testData.get("noOfInfant"));
            }
            /*if(testData.get("returnDateOffset")!=null){
                if(testData.get("departTicketType").equalsIgnoreCase("all in") && testData.get("returnTicketType").equalsIgnoreCase("all in")){*/
            for(int i=0;i<noOfAdult;i++){
                try{
                    WebElement bagButtonADT = TestManager.getDriver().findElement(By.cssSelector(String.format(bagButton,"ADT-"+i)));
                    Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(bagButtonADT,10));
                }catch(AssertionError e){
                    reportLogger.log(LogStatus.INFO,"bag buttons are not as expected for adult for departure - just fly and return - all in ");
                    throw e;
                }
            }
            int count = noOfAdult+noOfInfant;
            if(noOfTeen>0) {
                for (int i = 1; i <= noOfTeen; i++) {
                    WebElement bagButtonYTH = TestManager.getDriver().findElement(By.cssSelector(String.format(bagButton, "YTH-" + count)));
                    try {
                        Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(bagButtonYTH,10));
                    } catch (AssertionError e) {
                        reportLogger.log(LogStatus.INFO, "bag buttons are not as expected for teen for departure - just fly and return - all in ");
                        throw e;
                    }
                    count++;
                }
            }
            count = noOfAdult+noOfTeen+noOfInfant;
            if(noOfChild>0) {
                for (int i = 0; i < noOfChild; i++) {
                    WebElement bagButtonCHD = TestManager.getDriver().findElement(By.cssSelector(String.format(bagButton, "CHD-" + count)));
                    try {
                        Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(bagButtonCHD,10));
                    } catch (AssertionError e) {
                        reportLogger.log(LogStatus.INFO, "bag buttons are not as expected for teen for departure - just fly and return - all in ");
                        throw e;
                    }
                    count++;
                }
            }
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify if bag section is displayed for Air France booking");
        }
    }

    private void verifyLoggedInUserName(HashMap<String,String> testData,String pax){
        String item = null, expected = null, actual = null;
        try {
            if (pax.equals("adult")) {
                item = "title";
                actual = actionUtility.getValueSelectedInDropdown(titleADT.get(0));
                expected = testData.get("title");
                Assert.assertEquals(actual,expected);
                item = "first name";
                actual = firstNameADT.get(0).getAttribute("value");
                expected = testData.get("firstName");
                Assert.assertTrue(actual.equalsIgnoreCase(expected));
                item = "last name";
                actual = lastNameADT.get(0).getAttribute("value");
                expected = testData.get("lastName");
                Assert.assertTrue(actual.equalsIgnoreCase(expected));
            }
            else{
                item = "title";
                actual = actionUtility.getValueSelectedInDropdown(titleTeen.get(0));
                expected = testData.get("title");
                Assert.assertEquals(actual,expected);
                item = "first name";
                actual = firstNameTeen.get(0).getAttribute("value");;
                expected = testData.get("firstName");
                Assert.assertTrue(actual.equalsIgnoreCase(expected));
                item = "last name";
                actual = lastNameTeen.get(0).getAttribute("value");;
                expected = testData.get("lastName");
                Assert.assertTrue(actual.equalsIgnoreCase(expected));
            }
        }catch(AssertionError ae){
            System.out.println(item+" == "+expected+" ---- "+actual);
            reportLogger.log(LogStatus.INFO,"passenger "+item+" on traveller details doesn't match with "+item+" in My Account");
            throw ae;
        }
    }

    private void verifyADSNumber(HashMap<String,String> testData){
        try{
            Assert.assertTrue(adsADT.get(0).getAttribute("value").equals(testData.get("ads_number")));
            reportLogger.log(LogStatus.INFO,"ADS number displayed for adult 1 is as expected - "+testData.get("ads_number"));
        }catch(AssertionError ae){
            reportLogger.log(LogStatus.INFO,"expected ADS number displayed for adult 1 is "+testData.get("ads_number")+" but displayed is "+adsADT.get(0).getAttribute("value"));
            throw ae;
        }
    }

    public HashMap<String,String> enterPaxPersonalDetails(HashMap<String,String> testData, String... loggedInUser){
        // to enter passenger name,age
        try {
            actionUtility.waitForElementVisible(continueButton,90);
            if(testData.get("noOfAdult")!=null) {
                int noOfAdults = Integer.valueOf(testData.get("noOfAdult"));
                for (int i = 0; i < noOfAdults; i++) {
                    if(i==0 && loggedInUser.length>0 && loggedInUser[0].equals("loggedIn")){
                        verifyLoggedInUserName(testData,"adult");
                        testData.put("adult1Name",testData.get("title")+" "+testData.get("firstName")+" "+testData.get("lastName"));
                    }
                    else {
                        actionUtility.dropdownSelect(titleADT.get(i), SELECTBYTEXT, testData.get("adult" + (i + 1) + "Name").split(" ")[0]);
                        actionUtility.sendKeys(firstNameADT.get(i), testData.get("adult" + (i + 1) + "Name").split(" ")[1]);
                        actionUtility.sendKeys(lastNameADT.get(i), testData.get("adult" + (i + 1) + "Name").split(" ")[2]);
                    }
                    if(loggedInUser.equals("ads")){
                        if(i==0){
                            verifyADSNumber(testData);
                        }
                        else{
                            actionUtility.sendKeys(adsADT.get(i),testData.get("adult") + (i + 1) + "ADS");
                        }
                    }
                }
            }
            if(testData.get("noOfInfant")!=null) {
                int noOfInfant = Integer.valueOf(testData.get("noOfInfant"));
                for (int i = 0; i < noOfInfant; i++) {
                    actionUtility.sendKeys(firstNameINF.get(i), testData.get("infant" + (i + 1) + "Name").split(" ")[0]);
                    actionUtility.sendKeys(lastNameINF.get(i), testData.get("infant" + (i + 1) + "Name").split(" ")[1]);
                    actionUtility.sendKeys(birthDayINF.get(i), testData.get("infant" + (i + 1) + "DOB").split(" ")[0]);
                    actionUtility.dropdownSelect(birthMonthINF.get(i), SELECTBYTEXT, testData.get("infant" + (i + 1) + "DOB").split(" ")[1]);
                    actionUtility.sendKeys(birthYearINF.get(i), testData.get("infant" + (i + 1) + "DOB").split(" ")[2]);
                    if(loggedInUser.equals("ads")){
                        actionUtility.sendKeys(adsADT.get(i),testData.get("infant") + (i + 1) + "ADS");
                    }
                }
            }
            if(testData.get("noOfChild")!=null) {
                int noOfChild = Integer.valueOf(testData.get("noOfChild"));
                for (int i = 0; i < noOfChild; i++) {
                    actionUtility.dropdownSelect(titleChild.get(i), SELECTBYTEXT, testData.get("child" + (i + 1) + "Name").split(" ")[0]);
                    actionUtility.sendKeys(firstNameChild.get(i), testData.get("child" + (i + 1) + "Name").split(" ")[1]);
                    actionUtility.sendKeys(lastNameChild.get(i), testData.get("child" + (i + 1) + "Name").split(" ")[2]);
                    actionUtility.sendKeys(birthDayChild.get(i), testData.get("child" + (i + 1) + "DOB").split(" ")[0]);
                    actionUtility.dropdownSelect(birthMonthChild.get(i), SELECTBYTEXT, testData.get("child" + (i + 1) + "DOB").split(" ")[1]);
                    actionUtility.sendKeys(birthYearChild.get(i), testData.get("child" + (i + 1) + "DOB").split(" ")[2]);
                    if(loggedInUser.equals("ads")){
                        actionUtility.sendKeys(adsADT.get(i),testData.get("child") + (i + 1) + "ADS");
                    }
                }
            }
            if(testData.get("noOfTeen")!=null) {
                int noOfTeens = Integer.valueOf(testData.get("noOfTeen"));
                for (int i = 0; i < noOfTeens; i++) {
                    if(i==0 && Integer.parseInt(testData.get("noOfAdult"))==0 && loggedInUser.length>0 && loggedInUser[0].equals("loggedIn")){
                        verifyLoggedInUserName(testData,"teen");
                        testData.put("teen1Name",testData.get("title")+" "+testData.get("firstName")+" "+testData.get("lastName"));
                    }
                    actionUtility.dropdownSelect(titleTeen.get(i), SELECTBYTEXT, testData.get("teen" + (i + 1) + "Name").split(" ")[0]);
                    actionUtility.sendKeys(firstNameTeen.get(i), testData.get("teen" + (i + 1) + "Name").split(" ")[1]);
                    actionUtility.sendKeys(lastNameTeen.get(i), testData.get("teen" + (i + 1) + "Name").split(" ")[2]);
                    if(loggedInUser.equals("ads")){
                        actionUtility.sendKeys(adsADT.get(i),testData.get("teen") + (i + 1) + "ADS");
                    }
                }
            }
            reportLogger.log(LogStatus.INFO,"successfully entered traveller details");
            return testData;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to enter traveller details");
            throw e;
        }
    }

    public void verifyBagOptionsDisplayedForGetMore(HashMap<String,String> testData){
        //verifies bag options displayed for Get more
        try{
            int noOfBagButtonsForEachpax = 2;
            if(testData.get("returnDateOffset")!=null){
                noOfBagButtonsForEachpax = 4;
            }
            int noOfChild = 0;
            int noOfTeen = 0;
            int noOfInfant = 0;
            int noOfAdult = Integer.parseInt(testData.get("noOfAdult"));
            if(testData.get("noOfTeen")!=null){
                noOfTeen = Integer.parseInt(testData.get("noOfTeen"));
            }
            if(testData.get("noOfChild")!=null){
                noOfChild = Integer.parseInt(testData.get("noOfChild"));
            }
            if(testData.get("noOfInfant")!=null){
                noOfChild = Integer.parseInt(testData.get("noOfInfant"));
            }
            /*if(testData.get("returnDateOffset")!=null){
                if(testData.get("departTicketType").equalsIgnoreCase("all in") && testData.get("returnTicketType").equalsIgnoreCase("all in")){*/
            for(int i=0;i<noOfAdult;i++){
                List<WebElement> bagButtonTitleADT = TestManager.getDriver().findElements(By.cssSelector(String.format(bagButtonTitle,"ADT-"+i)));
                List<WebElement> bagButtonDescriptionADT = TestManager.getDriver().findElements(By.cssSelector(String.format(bagButtonDescription,"ADT-"+i)));
                List<WebElement> bagButtonPriceStatusADT = TestManager.getDriver().findElements(By.cssSelector(String.format(bagButtonPriceStatus,"ADT-"+i)));
                try{
                    Assert.assertTrue(bagButtonDescriptionADT.size()==noOfBagButtonsForEachpax);
                    for(int j=0;j<noOfBagButtonsForEachpax;j++) {
                        if(j%2==0) {
                            Assert.assertTrue(bagButtonTitleADT.get(j).getText().equals("1 case"));
                            Assert.assertTrue(bagButtonDescriptionADT.get(j).getText().equals("up to 23kg total"));
                            Assert.assertTrue(bagButtonPriceStatusADT.get(j).getText().equals("included"));
                        }
                        else{
                            Assert.assertTrue(bagButtonTitleADT.get(j).getText().equals("2 cases"));
                            Assert.assertTrue(bagButtonDescriptionADT.get(j).getText().equals("up to 46kg total"));
                        }
                    }
                }catch(AssertionError e){
                    reportLogger.log(LogStatus.INFO,"bag buttons are not as expected for adult for ticket type - get more ");
                    throw e;
                }
            }
            int count = noOfAdult+noOfInfant;
            if(noOfTeen>0) {
                for (int i = 1; i <= noOfTeen; i++) {
                    List<WebElement> bagButtonTitleYTH = TestManager.getDriver().findElements(By.cssSelector(String.format(bagButtonTitle, "YTH-" + count)));
                    List<WebElement> bagButtonDescriptionYTH = TestManager.getDriver().findElements(By.cssSelector(String.format(bagButtonDescription, "YTH-" + count)));
                    List<WebElement> bagButtonPriceStatusYTH = TestManager.getDriver().findElements(By.cssSelector(String.format(bagButtonPriceStatus,"YTH-"+count)));

                    try {
                        Assert.assertTrue(bagButtonDescriptionYTH.size()==noOfBagButtonsForEachpax);
                        for(int j=0;j<noOfBagButtonsForEachpax;j++) {
                            if(j%2==0) {
                                Assert.assertTrue(bagButtonTitleYTH.get(j).getText().equals("1 case"));
                                Assert.assertTrue(bagButtonDescriptionYTH.get(j).getText().equals("up to 23kg total"));
                                Assert.assertTrue(bagButtonPriceStatusYTH.get(j).getText().equals("included"));
                            }
                            else{
                                Assert.assertTrue(bagButtonTitleYTH.get(j).getText().equals("2 cases"));
                                Assert.assertTrue(bagButtonDescriptionYTH.get(j).getText().equals("up to 46kg total"));
                            }
                        }
                    } catch (AssertionError e) {
                        reportLogger.log(LogStatus.INFO, "bag buttons are not as expected for teen for ticket type - get more");
                        throw e;
                    }
                    count++;
                }
            }
            count = noOfAdult+noOfTeen+noOfInfant;
            if(noOfChild>0) {
                for (int i = 0; i < noOfChild; i++) {
                    List<WebElement> bagButtonTitleCHD = TestManager.getDriver().findElements(By.cssSelector(String.format(bagButtonTitle, "CHD-" + count)));
                    List<WebElement> bagButtonDescriptionCHD = TestManager.getDriver().findElements(By.cssSelector(String.format(bagButtonDescription, "CHD-" + count)));
                    List<WebElement> bagButtonPriceStatusCHD = TestManager.getDriver().findElements(By.cssSelector(String.format(bagButtonPriceStatus,"CHD-"+count)));
                    try {
                        Assert.assertTrue(bagButtonDescriptionCHD.size()==noOfBagButtonsForEachpax);
                        for(int j=0;j<noOfBagButtonsForEachpax;j++) {
                            if(j%2==0) {
                                Assert.assertTrue(bagButtonTitleCHD.get(j).getText().equals("1 case"));
                                Assert.assertTrue(bagButtonDescriptionCHD.get(j).getText().equals("up to 23kg total"));
                                Assert.assertTrue(bagButtonPriceStatusCHD.get(j).getText().equals("included"));
                            }
                            else{
                                Assert.assertTrue(bagButtonTitleCHD.get(j).getText().equals("2 cases"));
                                Assert.assertTrue(bagButtonDescriptionCHD.get(j).getText().equals("up to 46kg total"));
                            }
                        }
                    } catch (AssertionError e) {
                        reportLogger.log(LogStatus.INFO, "bag buttons are not as expected for child for ticket type - get more");
                        throw e;
                    }
                    count++;
                }
            }
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify bag section");
            throw e;
        }
    }

    public void verifyBagOptionsDisplayedForAllIn(HashMap<String,String> testData){
        //verifies bag options displayed for All in
        try{
            int noOfBagButtonsForEachpax = 1;
            if(testData.get("returnDateOffset")!=null){
                noOfBagButtonsForEachpax = 2;
            }
            int noOfChild = 0;
            int noOfTeen = 0;
            int noOfInfant = 0;
            int noOfAdult = Integer.parseInt(testData.get("noOfAdult"));
            if(testData.get("noOfTeen")!=null){
                noOfTeen = Integer.parseInt(testData.get("noOfTeen"));
            }
            if(testData.get("noOfChild")!=null){
                noOfChild = Integer.parseInt(testData.get("noOfChild"));
            }
            if(testData.get("noOfInfant")!=null){
                noOfInfant = Integer.parseInt(testData.get("noOfInfant"));
            }

            for(int i=0;i<noOfAdult;i++){
                List<WebElement> bagButtonTitleADT = TestManager.getDriver().findElements(By.cssSelector(String.format(bagButtonTitle,"ADT-"+i)));
                List<WebElement> bagButtonDescriptionADT = TestManager.getDriver().findElements(By.cssSelector(String.format(bagButtonDescription,"ADT-"+i)));
                List<WebElement> bagButtonPriceStatusADT = TestManager.getDriver().findElements(By.cssSelector(String.format(bagButtonPriceStatus,"ADT-"+i)));
                try{
                    Assert.assertTrue(bagButtonDescriptionADT.size()==noOfBagButtonsForEachpax);
                    for(int j=0;j<noOfBagButtonsForEachpax;j++) {
                        Assert.assertTrue(bagButtonTitleADT.get(j).getText().equals("2 cases"));
                        Assert.assertTrue(bagButtonDescriptionADT.get(j).getText().equals("up to 46kg total"));
                        Assert.assertTrue(bagButtonPriceStatusADT.get(j).getText().equals("included"));
                    }
                }catch(AssertionError e){
                    reportLogger.log(LogStatus.INFO,"bag buttons are not as expected for adult for  all in ticket type");
                    throw e;
                }
            }
            int count = noOfAdult+noOfInfant;
            if(noOfTeen>0) {
                for (int i = 1; i <= noOfTeen; i++) {
                    List<WebElement> bagButtonTitleYTH = TestManager.getDriver().findElements(By.cssSelector(String.format(bagButtonTitle, "YTH-" + count)));
                    List<WebElement> bagButtonDescriptionYTH = TestManager.getDriver().findElements(By.cssSelector(String.format(bagButtonDescription, "YTH-" + count)));
                    List<WebElement> bagButtonPriceStatusYTH = TestManager.getDriver().findElements(By.cssSelector(String.format(bagButtonPriceStatus,"YTH-"+count)));
                    try {
                        Assert.assertTrue(bagButtonDescriptionYTH.size()==noOfBagButtonsForEachpax);
                        for(int j=0;j<noOfBagButtonsForEachpax;j++) {
                            Assert.assertTrue(bagButtonTitleYTH.get(j).getText().equals("2 cases"));
                            Assert.assertTrue(bagButtonDescriptionYTH.get(j).getText().equals("up to 46kg total"));
                            Assert.assertTrue(bagButtonPriceStatusYTH.get(j).getText().equals("included"));
                        }
                    } catch (AssertionError e) {
                        reportLogger.log(LogStatus.INFO, "bag buttons are not as expected for teen for all in ticket type");
                        throw e;
                    }
                    count++;
                }
            }
            count = noOfAdult+noOfTeen+noOfInfant;
            if(noOfChild>0) {
                for (int i = 0; i < noOfChild; i++) {
                    List<WebElement> bagButtonTitleCHD = TestManager.getDriver().findElements(By.cssSelector(String.format(bagButtonTitle, "CHD-" + count)));
                    List<WebElement> bagButtonDescriptionCHD = TestManager.getDriver().findElements(By.cssSelector(String.format(bagButtonDescription, "CHD-" + count)));
                    List<WebElement> bagButtonPriceStatusCHD = TestManager.getDriver().findElements(By.cssSelector(String.format(bagButtonPriceStatus,"CHD-"+count)));
                    try {
                        Assert.assertTrue(bagButtonDescriptionCHD.size()==noOfBagButtonsForEachpax);
                        for(int j=0;j<noOfBagButtonsForEachpax;j++) {
                            Assert.assertTrue(bagButtonTitleCHD.get(j).getText().equals("2 cases"));
                            Assert.assertTrue(bagButtonDescriptionCHD.get(j).getText().equals("up to 46kg total"));
                            Assert.assertTrue(bagButtonPriceStatusCHD.get(j).getText().equals("included"));
                        }
                    } catch (AssertionError e) {
                        reportLogger.log(LogStatus.INFO, "bag buttons are not as expected for child for ticket type - all in ");
                        throw e;
                    }
                    count++;
                }
            }
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify bag section");
            throw e;
        }
    }

    public HashMap<String,Double> selectBags(HashMap<String,String> testData,HashMap<String,Double>... basket){
        //adds bags for each passenger and calculate cost incurred
        try{
            Double totalBagAmount = 0.00;
            Boolean returnJourney = false;
            //Boolean returnDifferentTickeType = true;
            if(testData.get("returnDateOffset")!=null)
            {
                returnJourney = true;
                /*if(testData.get("departTicketType").equalsIgnoreCase(testData.get("returnTicketType"))){
                    returnDifferentTickeType = false;
                }*/
            }
            int noOfAdults = Integer.valueOf(testData.get("noOfAdult"));
            int bagCountDepart = 0;
            int bagCountReturn = 1;
            Double cost = 0.00;
            WebElement bag = null;
            for(int i=0;i<noOfAdults;i++){
                if(testData.get("adult"+(i+1)+"BagDepart") != null) {
                    bag = TestManager.getDriver().findElements(By.xpath(String.format(adultBags, testData.get("adult" + (i + 1) + "BagDepart")))).get(bagCountDepart);
                    actionUtility.hardClick(bag);
                    actionUtility.hardSleep(2000);
                    cost = calculateBagCost("ADT",testData.get("adult" + (i + 1) + "BagDepart"),bagCountDepart);

                    if(basket.length>0){
                        basket[0].put("adult"+(i+1)+"DepartBagCost",cost);
                    }
                    /*if(returnJourney && !returnDifferentTickeType){
                        cost *= 2;
                    }*/
                    totalBagAmount += cost;
                    reportLogger.log(LogStatus.INFO,"selected bag for departure "+testData.get("adult" + (i + 1) + "BagDepart")+" for "+testData.get("adult" + (i + 1)+"Name"));
                    System.out.println("selected adult bag for departure "+testData.get("adult" + (i + 1) + "BagDepart")+"for "+testData.get("adult" + (i + 1)+"Name")+" for cost "+cost);
                }
                if(returnJourney /*&& returnDifferentTickeType*/){
                    bagCountDepart += 2;
                }
                else{
                    bagCountDepart += 1;
                }
                if(returnJourney /*&& returnDifferentTickeType*/){
                    if(testData.get("adult"+(i+1)+"BagReturn")!=null) {
                        bag = TestManager.getDriver().findElements(By.xpath(String.format(adultBags, testData.get("adult" + (i + 1) + "BagReturn")))).get(bagCountReturn);
                        actionUtility.hardClick(bag);
                        actionUtility.hardSleep(2000);
                        cost = calculateBagCost("ADT",testData.get("adult" + (i + 1) + "BagReturn"),bagCountReturn);
                        if(basket.length>0){
                            basket[0].put("adult"+(i+1)+"ReturnBagCost",cost);
                        }
                        /*if(returnJourney && !returnDifferentTickeType){
                            cost *= 2;
                        }
                        */totalBagAmount += cost;
                        reportLogger.log(LogStatus.INFO,"selected bag for return "+testData.get("adult" + (i + 1) + "BagReturn")+" for "+testData.get("adult" + (i + 1)+"Name"));
                        System.out.println("selected adult bag for return "+testData.get("adult" + (i + 1) + "BagReturn")+"for "+testData.get("adult" + (i + 1)+"Name")+" for a cost of "+cost);
                    }
                    bagCountReturn += 2;
                }
            }
            if(testData.get("noOfTeen")!=null) {
                int noOfTeens = Integer.valueOf(testData.get("noOfTeen"));
                bagCountDepart = 0;
                bagCountReturn = 1;
                for (int i = 0; i < noOfTeens; i++) {
                    if (testData.get("teen" + (i + 1) + "BagDepart") != null) {
                        bag = TestManager.getDriver().findElements(By.xpath(String.format(teenBags, testData.get("teen" + (i + 1) + "BagDepart")))).get(bagCountDepart);
                        actionUtility.hardClick(bag);
                        actionUtility.hardSleep(2000);
                        cost = calculateBagCost("YTH",testData.get("teen" + (i + 1) + "BagDepart"),bagCountDepart);
                        if(basket.length>0){
                            basket[0].put("teen"+(i+1)+"DepartBagCost",cost);
                        }
                        /*if(returnJourney && !returnDifferentTickeType){
                            cost *= 2;
                        }
                        */totalBagAmount += cost;
                        reportLogger.log(LogStatus.INFO,"selected bag for departure "+testData.get("teen" + (i + 1) + "BagDepart")+" for "+testData.get("teen" + (i + 1)+"Name"));
                        System.out.println("selected teen bag for departure " + testData.get("teen" + (i + 1) + "BagDepart")+testData.get("teen" + (i + 1)+"Name")+"for a cost "+cost);
                    }
                    if(returnJourney /*&& returnDifferentTickeType*/){
                        bagCountDepart += 2;
                    }
                    else{
                        bagCountDepart += 1;
                    }
                    if (returnJourney /*&& returnDifferentTickeType*/ ) {
                        if(testData.get("teen" + (i + 1) + "BagReturn") != null) {
                            bag = TestManager.getDriver().findElements(By.xpath(String.format(teenBags, testData.get("teen" + (i + 1) + "BagReturn")))).get(bagCountReturn);
                            actionUtility.hardClick(bag);
                            actionUtility.hardSleep(2000);
                            cost = calculateBagCost("YTH",testData.get("teen" + (i + 1) + "BagReturn"),bagCountReturn);

                            if(basket.length>0){
                                basket[0].put("teen"+(i+1)+"ReturnBagCost",cost);
                            }
                            /*if(returnJourney && !returnDifferentTickeType){
                                cost *= 2;
                            }
                            */totalBagAmount += cost;
                            reportLogger.log(LogStatus.INFO,"selected bag for return "+testData.get("teen" + (i + 1) + "BagReturn")+"for "+testData.get("teen" + (i + 1)+"Name"));
                            System.out.println("selected teen bag for return " + testData.get("teen" + (i + 1) + "BagReturn")+"for "+testData.get("teen" + (i + 1)+"Name")+"for a cost of "+cost);
                        }
                        bagCountReturn += 2;
                    }
                }

            }
            if(testData.get("noOfChild")!=null) {
                int noOfChild = Integer.valueOf(testData.get("noOfChild"));
                bagCountDepart = 0;
                bagCountReturn = 1;
                for (int i = 0; i < noOfChild; i++) {
                    if (testData.get("child" +(i+1)+ "BagDepart") != null) {
                        bag = TestManager.getDriver().findElements(By.xpath(String.format(childBags, testData.get("child" + (i + 1) + "BagDepart")))).get(bagCountDepart);
                        actionUtility.hardClick(bag);
                        actionUtility.hardSleep(2000);
                        cost = calculateBagCost("CHD",testData.get("child" + (i + 1) + "BagDepart"),bagCountDepart);

                        if(basket.length>0){
                            basket[0].put("child"+(i+1)+"DepartBagCost",cost);
                        }
                        /*if(returnJourney && !returnDifferentTickeType){
                            cost *= 2;
                        }
                        */totalBagAmount += cost;
                        reportLogger.log(LogStatus.INFO,"selected bag for departure "+testData.get("child" + (i + 1) + "BagDepart")+" for "+testData.get("child" + (i + 1)+"Name"));
                        System.out.println("selected child bag for departure " + testData.get("child" + (i + 1) + "BagDepart")+"for "+testData.get("child" + (i + 1)+"Name")+"for a cost of "+cost);
                    }
                    if(returnJourney /*&& returnDifferentTickeType*/){
                        bagCountDepart += 2;
                    }
                    else{
                        bagCountDepart += 1;
                    }
                    if (returnJourney /*&& returnDifferentTickeType*/) {
                        if(testData.get("child" + (i + 1) + "BagReturn") != null) {
                            bag = TestManager.getDriver().findElements(By.xpath(String.format(childBags, testData.get("child" + (i + 1) + "BagReturn")))).get(bagCountReturn);
                            actionUtility.hardClick(bag);
                            actionUtility.hardSleep(2000);
                            cost = calculateBagCost("CHD",testData.get("child" + (i + 1) + "BagReturn"),bagCountReturn);

                            if(basket.length>0){
                                basket[0].put("child"+(i+1)+"ReturnBagCost",cost);
                            }
                            /*if(returnJourney && !returnDifferentTickeType){
                                cost *= 2;
                            }*/
                            totalBagAmount += cost;
                            reportLogger.log(LogStatus.INFO,"selected bag for departure "+testData.get("child" + (i + 1) + "BagReturn")+" for "+testData.get("child" + (i + 1)+"Name"));
                            System.out.println("selected child bag for return " + testData.get("child" + (i + 1) + "BagReturn")+"for "+testData.get("child" + (i + 1)+"Name")+" for a cost of "+cost);
                        }
                        bagCountReturn += 2;
                    }
                    actionUtility.hardSleep(3000);
                }
            }
            reportLogger.log(LogStatus.INFO,"successfully added bags for passenger for a total cost of - "+totalBagAmount);
            if(basket.length>0) {
                basket[0].put("hold baggage",totalBagAmount);
                System.out.println("BASKET AFTER ADDING BAG - "+basket[0]);
                return basket[0];
            }
            return null;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to add bags");
            throw e;
        }
    }

    private Double calculateBagCost(String paxType,String bag,int counter) {
        Double cost = 0.00;
        cost = CommonUtility.convertStringToDouble(TestManager.getDriver().findElements(By.xpath(String.format(bagCost,paxType,bag))).get(counter).getText());

        return cost;
    }

    public void addBookerDetails(HashMap<String,String> testData,Boolean... loggedInUser){
        //selects booker,enter details. Also enters details if the booker is someone else
        //verifies booker contact details in case of logged in user
        HashMap<String,String> contactDetails = null;
        try{
            actionUtility.waitForElementVisible(booker,20);
            if(loggedInUser.length>0 && loggedInUser[0]){
                verifyContactInformationIsCorrect(testData);
            }
            else {
                if (testData.get("booker") != null && testData.get("booker").equals("Someone else")) {
                    actionUtility.click(booker.get(booker.size() - 1));
                } else {
                    actionUtility.hardClick(booker.get(0));
                }
                contactDetails = new DataUtility().testData("1");
                populateBookerContactDetails(contactDetails);
                if (testData.get("booker") != null && testData.get("booker").equals("Someone else")) {
                    populatePaxContactDetails(contactDetails);
                }
                reportLogger.log(LogStatus.INFO, "successfully selected booker and entered booker contact details");
            }
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select booker");
            throw e;
        }
    }

    private void populateBookerContactDetails(HashMap<String,String> contactDetails){
        actionUtility.sendKeys(bookerPhoneNumber, contactDetails.get("bookerPhoneNumber"));
        actionUtility.click(bookerPhoneCode);
        try {
            actionUtility.click(selectPhoneCode);
        }catch (TimeoutException te){
            actionUtility.scrollUP(selectPhoneCode);
            actionUtility.click(selectPhoneCode);
        }
        actionUtility.sendKeys(bookerEmail, contactDetails.get("bookerEmail"));
        actionUtility.sendKeys(bookerEmailConfirmation, contactDetails.get("bookerEmail"));
    }

    private void populatePaxContactDetails(HashMap<String,String> contactDetails){
        actionUtility.click(paxPhoneCode);
        try {
            actionUtility.click(selectPhoneCode);
        }catch (TimeoutException te){
            actionUtility.scrollUP(selectPhoneCode);
            actionUtility.click(selectPhoneCode);
        }
        actionUtility.sendKeys(paxPhoneNumber, contactDetails.get("paxPhoneNumber"));
        actionUtility.sendKeys(paxEmail, contactDetails.get("paxEmail"));
    }

    private void verifyContactInformationIsCorrect(HashMap<String,String> testData) {
        String item=null, expected = null,actual = null;
        try {
            actionUtility.click(booker.get(0));
            item = "phone number country code";
            expected = testData.get("countryCode");
            actual = bookerPhoneCode.getAttribute("value");
            Assert.assertTrue(expected.equals(actual));

            item = "phone number";
            expected = testData.get("phoneNumber");
            actual = bookerPhoneNumber.getAttribute("value");
            Assert.assertTrue(expected.equals(actual), "Phone Number in booker section is not matching");

            item = "booker email id";
            HashMap<String,String> contact = new DataUtility().testData("1");
            expected = contact.get("bookerEmail");
            actual = bookerEmail.getAttribute("value");
            Assert.assertTrue(expected.equals(actual));
            //incase of logged in teen is travelling alone
            if(Integer.parseInt(testData.get("noOfAdult"))==0){
                populatePaxContactDetails(contact);
            }
        } catch (AssertionError e) {
            System.out.println(item+" == "+expected+" ---- "+actual);
            reportLogger.log(LogStatus.INFO, item+" in booker contact details section not matching for logged in user, Expected- " + expected + " Displayed - " + actual);
            throw e;
        } catch (Exception e) {
            reportLogger.log(LogStatus.INFO, "unable to verify contact details for logged in user");
            throw e;
        }
    }

    public HashMap<String,Double> verifyBasket(HashMap<String,Double> basket){
        //verifies and retrieves baggage,total and services cost in basket
        String item = null;
        Double itemPrice = 0.00;
        Double expectedPrice = 0.00;
        Double expectedTotalServiceCost = 0.00;
        try{
            if(basket.get("hold baggage")!=null){
                expectedPrice = basket.get("hold baggage");
                item = "hold baggage";
                itemPrice = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(serviceCost,"Hold baggage"))).getText());
                Assert.assertEquals(itemPrice,expectedPrice);
                expectedTotalServiceCost = expectedPrice;
                reportLogger.log(LogStatus.INFO,"successfully verified hold baggage cost in basket - "+itemPrice);
            }

            if(basket.get("special assistance")!=null){
                item = "special assistance";
                expectedPrice = basket.get("special assistance");
                itemPrice = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(serviceCost,"Special Assistance"))).getText());
                Assert.assertEquals(expectedPrice,itemPrice);
                expectedTotalServiceCost += expectedPrice;
                reportLogger.log(LogStatus.INFO,"successfully verified special assistance cost in basket - "+itemPrice);
            }
            if(expectedTotalServiceCost>0) {
                item = "services";
                expectedPrice = expectedTotalServiceCost;
                itemPrice = CommonUtility.convertStringToDouble(services.getText());
                Assert.assertEquals(itemPrice,expectedPrice);
                basket.put("services",itemPrice);
                reportLogger.log(LogStatus.INFO,"successfully verified services cost in basket - "+itemPrice);
            }
            if(basket.get("fare")!=null) {
                item = "total";
                itemPrice = CommonUtility.convertStringToDouble(totalPrice.getText());
                if (expectedTotalServiceCost > 0) {
                    expectedPrice = CommonUtility.truncateToTWODecimalPlaces(basket.get("services") + basket.get("fare"));
                } else {
                    expectedPrice = basket.get("fare");
                }Assert.assertEquals(itemPrice, expectedPrice);
                basket.put(item, itemPrice);
                reportLogger.log(LogStatus.INFO, "successfully verified total cost in basket - " + itemPrice);
            }
        }catch(AssertionError e) {
            System.out.println(expectedPrice+"-------"+itemPrice);
            reportLogger.log(LogStatus.INFO, item+" cost displayed in basket - "+itemPrice+" doesn't match with expected "+item+" cost - "+expectedPrice);
            throw e;
        }
        catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify the basket in traveller details page");
            throw e;
        }
        return basket;
    }

    public void selectMarketingPrefAndContinue(HashMap<String,String> testData){
        //select marketing preference and continue to seat page
        try{
            actionUtility.click(TestManager.getDriver().findElement(By.xpath(String.format(mktPref,testData.get("marketingPreference")))));
            actionUtility.hardSleep(2000);
            reportLogger.log(LogStatus.INFO,"successfully selected marketing preference and navigated to seat page");
            actionUtility.hardClick(continueButton);
            actionUtility.hardSleep(2000);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to add marketing preference and continue to seat page");
            throw e;
        }
    }

    public HashMap<String,Double> selectSpecialAssistance(HashMap<String,String> testData,HashMap<String,Double>... basket){
        //Selects Special assistance and retruns cost of adding spl assistance
        try{
            if(testData.get("noOfAdult")!=null) {
                int noOfAdult = Integer.parseInt(testData.get("noOfAdult"));
                for (int i = 1; i<= noOfAdult; i++) {
                    if (testData.get("adult" + i + "SpecialAssistance") != null) {
                        actionUtility.hardClick(specialAssistanceCheckBoxADT.get(i-1));
                        if (testData.get("adult" + i + "SpecialAssistance").equals("WCHR")) {
                            actionUtility.hardClick(buttonWCHRADT.get(i-1));
                        } else if (testData.get("adult" + i + "SpecialAssistance").equals("WCHS")) {
                            actionUtility.hardClick(buttonWCHSADT.get(i-1));
                        } else {
                            actionUtility.hardClick(buttonWCHCADT.get(i-1));
                        }
                        actionUtility.click(confirmRequestADT.get(i-1));
                        reportLogger.log(LogStatus.INFO, "successfully requested for Special Assistance for passenger " + testData.get("adult" + i + "Name") + "-" + testData.get("adult" + i + "SpecialAssistance"));
                    }
                }
            }

            if(testData.get("noOfTeen")!=null) {
                int noOfTeen = new Integer(testData.get("noOfTeen"));
                for (int i = 1; i<= noOfTeen; i++) {
                    if(testData.get("teen"+i+"SpecialAssistance")!=null) {
                        actionUtility.hardClick(specialAssistanceCheckBoxYTH.get(i-1));
                        if (testData.get("teen" + i + "SpecialAssistance").equals("WCHR")) {
                            actionUtility.hardClick(buttonWCHRYTH.get(i-1));
                        } else if (testData.get("teen" + i + "SpecialAssistance").equals("WCHS")) {
                            actionUtility.hardClick(buttonWCHSYTH.get(i-1));
                        } else {
                            actionUtility.hardClick(buttonWCHCYTH.get(i-1));
                        }
                        actionUtility.click(confirmRequestYTH.get(i-1));
                    }
                    reportLogger.log(LogStatus.INFO,"successfully requested for Special Assistance for passenger "+testData.get("teen"+i+"Name")+ "-"  +testData.get("teen"+i+"SpecialAssistance"));
                }
            }
            if(testData.get("noOfChild")!=null) {
                int noOfChild = new Integer(testData.get("noOfChild"));
                for (int i = 1; i<= noOfChild; i++) {
                    if(testData.get("child"+i+"SpecialAssistance")!=null) {
                        actionUtility.hardClick(specialAssistanceCheckBoxCHD.get(i-1));
                        if (testData.get("child" + i + "SpecialAssistance").equals("WCHR")) {
                            actionUtility.hardClick(buttonWCHRCHD.get(i-1));
                        } else if (testData.get("child" + i + "SpecialAssistance").equals("WCHS")) {
                            actionUtility.hardClick(buttonWCHSCHD.get(i-1));
                        } else {
                            actionUtility.hardClick(buttonWCHCCHD.get(i-1));
                        }
                        actionUtility.click(confirmRequestCHD.get(i-1));
                    }
                    reportLogger.log(LogStatus.INFO,"successfully requested for Special Assistance for passenger "+testData.get("child"+i+"Name")+ "-" +testData.get("child" + i + "SpecialAssistance"));
                }
            }

            if(basket.length>0) {
                basket[0].put("special assistance",0.00);
                return basket[0];
            }
            return null;
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to request or confirm special assistance");
            throw e;

        }
    }


}
