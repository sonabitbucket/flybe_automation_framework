package efly.business_logics.online;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.relevantcodes.extentreports.model.Log;
import custom_listeners.TestExecutionListener;
import org.apache.commons.collections.functors.ExceptionPredicate;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import utilities.ActionUtility;
import utilities.DataUtility;
import utilities.TestManager;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.*;

public class CMSLeapCheapFlight {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public CMSLeapCheapFlight() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    //    -------------------recent search starts------------
    @FindBy(className = "open-recent-searches-btn")
    private WebElement openRecentSearchTab;

    @FindBy(className = "recent-search-panel")
    private WebElement recentSearchPanel;

    @FindBy(css = ".recent-search-entry .title > span")
    private List<WebElement> recentSearchRoutes;

    @FindBy(css = ".recent-search-entry .title+.dates")
    private List<WebElement> recentSearchDates;

    //    ---------------recent search ends------------------

    @FindBy(id = "search-widget")
    private WebElement searchWidget;

    @FindBy(xpath = "//div[contains(@class,'teenItaly')]")
    private WebElement unaccompaniedMinorMessage;

    @FindBy(xpath = "//div[@id='flight_from_chosen']/a/div/b")
    private WebElement flightFromDropDown;

    @FindBy(xpath = "//div[@id ='flight_from_chosen']//input")
    private WebElement flightFrom;

    @FindBy(xpath = "//div[@id='flight_to_chosen']/a")
    private WebElement flightToDropDown;

    @FindBy(xpath = "//div[@id='flight_to_chosen']//input")
    private WebElement flightTo;

    @FindBy(xpath = "//div[@id='flight_to_chosen']/a[@class='chosen-single']/span")
    private WebElement selectedDestination;

    //@FindBy(id = "ui-datepicker-div")
    private WebElement datePicker;

    @FindBy(className = "ui-datepicker-year")
    private WebElement datePickerYear;

    @FindBy(className = "ui-datepicker-month")
    private WebElement datePickerMonth;

    @FindBy(xpath = "//input[contains(@class,'inputAdults')]")
    private WebElement adultNos;

    @FindBy(xpath = "//input[contains(@class,'inputTeens')]")
    private WebElement teenNos;

    @FindBy(xpath = "//input[contains(@class,'inputChildren')]")
    private WebElement childrenNos;

    @FindBy(xpath = "//input[contains(@class,'inputInfants')]")
    private WebElement infantNos;

    @FindBy(xpath = "//button[contains(@class,'adult-up')]")
    private WebElement adultIncrement;

    @FindBy(xpath = "//button[contains(@class,'adult-down')]")
    private WebElement adultDecrement;

    @FindBy(xpath = "//button[contains(@class,'teen-up')]")
    private WebElement teenIncrement;

    @FindBy(xpath = "//button[contains(@class,'teen-down')]")
    private WebElement teenDecrement;

    @FindBy(xpath = "//button[contains(@class,'child-up')]")
    private WebElement childIncrement;

    @FindBy(xpath = "//button[contains(@class,'child-down')]")
    private WebElement childDecrement;

    @FindBy(xpath = "//button[contains(@class,'infant-up')]")
    private WebElement infantIncrement;

    @FindBy(xpath = "//button[contains(@class,'infant-down')]")
    private WebElement infantDecrement;

    @FindBy(xpath = "//h3[contains(text(),'Select passengers')]")
    private WebElement selectPassengerMenu;

    @FindBy(id = "pax-display")
    private WebElement openSelectPassengerMenu;

    @FindBy(xpath = "//div[contains(@class,'pax-close-button')]")
    private WebElement closeSelectPassengerMenu;

    @FindBy(css = "label.js-add-promo.promo-label")
    private WebElement openPromoCode;

    @FindBy(xpath = "//input[contains(@class,'inputPromoCode')]")
    private WebElement promoCode;

    @FindBy(id = "flight-search-button" )
    private WebElement findFlights;

    @FindBy(xpath = "//li[a[text()='My account']]")
    private WebElement myAccount;

    @FindBy(xpath = "//a[contains(text(),'Log in')]")
    private WebElement login;

    @FindBy (id = "sign-in-button")
    private WebElement loginOption;

    @FindBy(xpath = "//li/a[text()='Check-in']")
    private WebElement checkInTab;

    @FindBy(id = "label-booking-ref")
    private WebElement bookingRefForCheckIn;

    @FindBy(id = "label-forename")
    private WebElement foreNameForCheckIn;

    @FindBy(id = "label-surname")
    private WebElement surNameForCheckIn;

    @FindBy(xpath = "//div/button[contains(text(),'Check-in')]")
    private WebElement checkInButton;

    @FindBy(className ="loading-spinner")
    private WebElement checkInLoader;

    @FindBy(linkText = "Find flights")
    private WebElement findFlightTab;

    @FindBy(linkText = "View booking")
    private WebElement viewBookingTab;

    @FindBy(id = "ManageBookingRef")
    private WebElement bookingRefForViewBooking;

    @FindBy(id = "ManageBookingForename")
    private WebElement foreNameForViewBooking;

    @FindBy(id = "ManageBookingSurname")
    private WebElement surNameForViewBooking;

    @FindBy(className = "modal-dialog")
    private WebElement popUp;

    @FindBy(className = "modal-header")
    private WebElement popUpHeader;

    @FindBy(id = "accept-cookies")
    private WebElement acceptCookies;

    @FindBy(id = "amend-cookie-prefs")
    private WebElement amendCookies;

    @FindBy(id = "your-cookie-settings-button")
    private WebElement cookiePrefDone;

    @FindBy(css = ".slider.round")
    private List<WebElement> cookiePreference;

    @FindBy(xpath = "//div[contains(@class,'error-groupBooking')]")
    private WebElement moreThanEightPassengersMessage;

    @FindBy(id = "home-hero")
    private WebElement heroBanner;

    @FindBy(id = "featured-flights")
    private WebElement carouselSection;

    @FindBy(css = "div[id='featured-flights'] div[class*='flight-container']")
    private List<WebElement> carousalTiles;

    @FindBy(id = "featured-flights-disclaimer")
    private WebElement featuredFlightDisclaimer;

    @FindBy(id = "featured-flights-button")
    private WebElement exploreMoreDestinations;

    @FindBy(xpath = "//h1[text()='FLYBE DESTINATION AND CHEAP FLIGHT GUIDES']")
    private WebElement exploreMorePageContent;

    @FindBy(id = "route-map")
    private WebElement routeMap;

    @FindBy(id = "cta-container")
    //@FindBy(id = "home-cta-area")
    private WebElement offerTiles;
    //------------PLD----------
    @FindBy(linkText = "Retrieve Price Lock-Down Booking")
    private WebElement retrievePLDTab;

    @FindBy(xpath = "//div[@id='findBookingForm']//tr/td/input[@name='pnrLocator']")
    private WebElement pldReferenceNumberForSearch;

    @FindBy(xpath = "//div[@id='findBookingForm']//tr/td/input[@name='forename']")
    private WebElement pldForenameForSearch;

    @FindBy(xpath = "//div[@id='findBookingForm']//tr/td/input[@name='surname']")
    private WebElement pldSurnameForSearch;

    @FindBy(xpath = "//input[@class='button medium fbred rightButton']")
    private WebElement pldContinueButtonForSearch;

    //    @FindBy(xpath = "//span[@class='loading-spinner']")
//    private WebElement checkInLoader;
    String closePopUpLocator = "//*[@id='newsletter-close']";
    String checkInLoaderIndicator = "//span[text()='Loading...']";

    String datePickerDay = "//div[@id='ui-datepicker-div']/div[contains(@class,'first')]/descendant::a[text()='%s']";
    //----------Footer-in-Home-Page-------------
    @FindBy(css = "div[id='logo-and-media'] img[alt='Flybe']")
    private WebElement flybeLogo;

    @FindBy(xpath = "//div[@id='footer-tail']//p[1]")
    private WebElement flybeCopyRight;

    //    @FindBy(linkText = "Privacy Policy")
    @FindBy(css = "footer a[href='/flightInfo/privacy_policy.htm']")
    private WebElement privacyPolicyLink;

    //    @FindBy(linkText = "Cookie Policy")
    @FindBy(css = "a[href='/flightInfo/cookie-policy.htm']")
    private WebElement cookiePolicyLink;

    //    @FindBy(linkText = "Website terms and conditions")
    @FindBy(css = "a[href='/flightInfo/website-terms-and-conditions.htm']")
    private WebElement termsAndConditionsLink;

    //    @FindBy(linkText = "Website terms of use")
    @FindBy(css = "a[href='/flightInfo/website-terms-of-use.htm']")
    private WebElement termsOfUseLink;

    //    @FindBy(linkText = "Journey Comparison Times")
    @FindBy(css = "a[href='/proof/']")
    private WebElement JouneryComparisionTimesLink;

    @FindBy(css = "div.container a[href='https://flybe.custhelp.com/app/ask']")
    private WebElement ContactUsLink;

    @FindBy(css = "a[href='/terms/tariff']")
    private WebElement ancillaryCharges;

    @FindBy(css = "a[href='/price-guide']")
    private WebElement priceGuide;

    @FindBy(css = "div.container a[href='/']")
    private WebElement flybeInHeader;

    @FindBy(xpath = "//div[@id='footer-tail']//p[2]")
    private WebElement footerPara1;

    @FindBy(xpath = "//div[@id='footer-tail']//p[3]")
    private WebElement footerPara2;

    @FindBy(xpath = "//div[@id='footer-tail']//p[4]")
    private WebElement footerPara3;

    @FindBy(xpath = "//div[@id='footer-tail']//p[5]")
    private WebElement footerPara4;

    @FindBy(xpath = "//strong[contains(text(),'About Us')]")
    private WebElement aboutUsLink;

    /*@FindBy(css = "a[href='/investors/']")
    private WebElement investorsLink;

    @FindBy(xpath = "//a[contains(text(),'Corporate')]")
    private WebElement governanceLink;

    @FindBy(css = "a[href='/media']")
    private WebElement mediaLink;

    @FindBy(css = "a[href='/careers/']")
    private WebElement careersLink;

    @FindBy(css = "a[href='/trade/")
    private WebElement tradeLink;*/

    @FindBy(css = "a[href='http://www.flybeas.com']")
    private WebElement aviationServicesLink;

    @FindBy(css = "a[href='http://www.flybetraining.com']")
    private WebElement trainingAcademyLink;

    @FindBy(css = "a[href='https://www.travelfusion.com/register?dcsName=Flybe&dcsLogo=http://www.flybe.com/img/template/flybe-logo.png&dcsEmail=travelfusion@flybe.com']")
    private WebElement xmlApiLink;

    @FindBy(css = "a[href='//www.twitter.com/flybe")
    private WebElement twitterLink;

    @FindBy(css = "a[href='//www.facebook.com/flybe']")
    private WebElement fbLink;

    @FindBy(css = "a[href='//www.instagram.com/flybe/']")
    private WebElement instagramLink;

    @FindBy(css = "a[href='//www.youtube.com/user/FlybeOfficial']")
    private WebElement youtubeLink;

    @FindBy(css = "a[href='//www.linkedin.com/company/flybe']")
    private WebElement linkedinLink;

    @FindBy(css = "a[href='//www.flybe.com/subscribe/']")
    private WebElement subscribeLink;

    @FindBy(xpath = "//div/h2[text()='About Us']")
    private WebElement aboutUsHeader;

    @FindBy(xpath = "//div[h2[text()='Other Flybe Sites']]")
    private WebElement otherFlybeWebsitesHeader;

    @FindBy(xpath = "//div/p[text()='Join the Conversation']")
    private WebElement joinTheConversationHeader;

    @FindBy(xpath = "//li[a[span[contains(@class,'wheelchair')]]]")
    private WebElement passengerAssistanceLink;

    @FindBy(xpath = "//button[@data-direction='outbound'and text()='Check-In Now']")
    private List<WebElement> outboundCheckIns;


    @FindBy(xpath = "//ul[@class='nav navbar-nav navbar-left']/li[2]/a")
    private WebElement flightInfoAndHelp;

    @FindBy(xpath = "//a[text()='Live arrivals & departures']")
    private WebElement liveArrivalsAndDepartures;

    @FindBy(xpath = "//li[@class='dropdown-submenu']/a[text()='Flights']")
    private WebElement infoFlights;

    @FindBy(linkText = "Timetable")
    private WebElement timeTable;

    @FindBy(xpath = "//a[@href='/timetableClassic/timetable.jsp']")
    private WebElement standardTimetable;

    @FindBy(xpath = "//a[@href='/timetableService/bookingTimetable.do']")
    private WebElement connectionTimetable;

    @FindBy(name = "selDep")
    private WebElement departureAirport;

    @FindBy(name = "selDest")
    private WebElement destinationAirport;

    @FindBy(xpath = "//img[@src='/images/timetable/continueSm.gif']")
    private WebElement showResults;

    @FindBy(xpath="//table[@id='OBS']//tr[7]/td/table//tr[4]")
    private WebElement routeList;

    @FindBy(id = "departureSelect")
    private WebElement departingFrom;

    @FindBy(id = "destinationSelect")
    private WebElement goingTo;

    @FindBy(id = "outbound_date_trigger")
    private WebElement outboundDate;

    @FindBy(xpath = "//div[@class='calendar']//tr[@class='headrow']//td[3]")
    private WebElement outboundDatePicker;

    @FindBy(id="buttonLink")
    private WebElement viewTimetable;

    @FindBy(xpath = "//*[@id='wrapper']//table//tr[2]")
    private WebElement timetableFlightDetails;

    @FindBy(xpath = "//div[h2[text()='About Us']]//a")
    private List<WebElement> AboutUsLinks;

    //@FindBy(xpath = "//div[@id='footer-tail']//ul[@class='list-inline']/li")
    @FindBy(xpath = "//div[p[text()='Information']]/ul/li")
    private List<WebElement> informationLinks;

    @FindBy(xpath = "//iframe[contains(@src,'https://www.itflybe.com/web-app/rx/bookings.html#/manage-booking?')]")
    private WebElement manageBookingFrame;

    @FindBy(xpath = "//iframe[contains(@src,'https://www.itflybe.com/web-app/check-in.html?')]")
    private WebElement CheckinFrame;

    /*@FindBy(xpath = "//div[h4[text()='Offers']]")
    private WebElement offerTile;*/

    String fourTile = "//div[h4[text()='%s']]";

    //alert
    @FindBy(id = "disruption-standard")
    private WebElement disruptionAlert;

    @FindBy(css = "span[class='disruption-remove']")
    private WebElement hideAlert;

    //language
    @FindBy(css = "a[alt='Change language']")
    private WebElement changeLanguage;

    @FindBy(css = "ul[class='dropdown-menu']")
    private WebElement langDropdown;

    String selectLanguage = "a[href='/%s']";

    //header menu
    private String headerOption = "//a[contains(text(),'%s')]";

    private String headerSubOption = "//a[contains(text(),'%s')]";

    private String rhsHeaderSubOption = "li[class*='%s'] a";

    @FindBy (xpath = "//h1[contains(text(),'FLYBE DESTINATION AND CHEAP FLIGHT GUIDES')]")
    private WebElement DestinationPage;

    @FindBy (id = "bewhereyouwanttobe")
    private WebElement routeMapText;

    @FindBy (xpath = "//a[text()='Hold baggage']")
    private WebElement baggagePage;

    @FindBy (xpath = "//b[contains(text(),'Fast-track security')]")
    private WebElement fastTrackPage;

    @FindBy (xpath = "//h2[@class='section-title-desktop'][contains(text(),'Discover the Isle of Man')]")
    private WebElement IsleOfManPage;

    @FindBy(xpath ="//span[text()='Requesting Passenger Assistance & Guide Dogs']")
    private WebElement specialAssistanceText;

    @FindBy(id = "timetable-heading")
    private WebElement timetableText;

    @FindBy (xpath = "//a[text()='Cabin Baggage Allowance']")
    private WebElement cabinAllowanceText;

    @FindBy (xpath= "//div[text()='Delayed & Cancelled Flights Advice']")
    private WebElement flightDelayPage;

    @FindBy (className="manage-booking-heading")
    private WebElement mangeBookingPage;

    @FindBy (xpath = "//h1[text()='Ticket changes']")
    private WebElement ticketChangesPage;

    @FindBy (xpath = "//b[text()='Refunds']")
    private WebElement refundPage;

    @FindBy (xpath = "//span[contains(text(),'Last name')]")
    private WebElement amendCarHirePage;

    @FindBy  (xpath = "//li[a[text()='Hotels & Cars ']]//li[@class='heading1']")
    private List<WebElement> hotelsCarsSubMenu;

    @FindBy (xpath = "//span[text()='Find deals for any season']")
    private WebElement hotelSearch;

    @FindBy (className = "page-title")
    private WebElement carHireText ;

    @FindBy (xpath = "//a[contains(text(),'Park Closer Quicker Easier')]")
    private WebElement parkingText;

    String hotelsCarsSubMenuImages = "img[alt='%s']";

    //Hotels,Car and Avios Links
    String hotelCarAviosLinks = "//div[@id='cta-container']//h5[text()='%s']";

    //manage booking
    @FindBy(xpath = "//a[contains(text(),'My booking')]")
    private WebElement myBooking;

    @FindBy(css = "button[class*='manage-booking-button']")
    private WebElement manageBooking;

    @FindBy(xpath = "//li[@class='nav-arrivals-and-departures']//a[contains(text(),'Manage Booking')]")
    private WebElement manageBookingHeaderRHS;

    //checkin
    @FindBy(css = "button[class='secondary-button my-booking-button js-submit']")
    private WebElement checkinButton;

    public void clickOnTile(String tile){
        //will click on any tile passed as string value
        try{
            actionUtility.hardClick(TestManager.getDriver().findElement(By.xpath(String.format(fourTile,tile))));
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to click on tile "+tile);
            throw e;
        }
    }

    public void deleteCookie(String cookieName){
//        this method will delete the cookie
        try{
            Set<Cookie> cookiesPresent = TestManager.getDriver().manage().getCookies();
            if(cookiesPresent.contains(cookieName)) {
                TestManager.getDriver().manage().deleteCookieNamed(cookieName);
                reportLogger.log(LogStatus.INFO, "cookie is successfully deleted with the name-" + cookieName);
            }
        }catch (WebDriverException e){
            reportLogger.log(LogStatus.INFO,"unable to delete the cookie - "+cookieName);
            throw e;
        }
    }

    public void navigateToTimetable(HashMap<String,String> testData) {
        //Navigate to timetable page
        try {
            handleHomeScreenPopUp(testData);
            actionUtility.clickByAction(flightInfoAndHelp);
            actionUtility.clickByAction(infoFlights);
            actionUtility.clickByAction(timeTable);
            actionUtility.waitForPageLoad(10);
            reportLogger.log(LogStatus.INFO,"Successfully navigated to timetable page");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to navigate to timetable page");
            throw e;
        }
    }

    public void navigateToStandardTimetable(Hashtable<String,String> testData) {
        //Navigate to standard timetable page and verifies the routes displayed
        try {
            actionUtility.clickByAction(standardTimetable);
            actionUtility.waitForPageLoad(10);
            actionUtility.dropdownSelect(departureAirport,ActionUtility.SelectionType.SELECTBYVALUE,testData.get("departureAirport"));
            actionUtility.dropdownSelect(destinationAirport,ActionUtility.SelectionType.SELECTBYVALUE,testData.get("destinationAirport"));
            actionUtility.click(showResults);
            Assert.assertTrue(routeList.isDisplayed(),"The route list is not displayed in the Standard timetable page ");
            reportLogger.log(LogStatus.INFO,"The route list is not displayed in the Standard timetable page");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"The route list is not displayed in the Standard timetable page");
            throw e;
        }
    }

    public void navigateToConnectionTimetable(Hashtable<String,String> testData) {
        //Navigate to connection timetable page and verifies the routes displayed
        try {
            actionUtility.clickByAction(connectionTimetable);
            actionUtility.waitForPageLoad(10);
            actionUtility.dropdownSelect(departingFrom, ActionUtility.SelectionType.SELECTBYVALUE, testData.get("departingFrom"));
            actionUtility.dropdownSelect(goingTo, ActionUtility.SelectionType.SELECTBYVALUE, testData.get("goingTo"));
            actionUtility.click(outboundDate);
            actionUtility.click(outboundDatePicker);
            actionUtility.click(viewTimetable);
            Assert.assertTrue(timetableFlightDetails.isDisplayed(), "The route list is not displayed in the connection timetable page ");
            reportLogger.log(LogStatus.INFO, "The route list is not displayed in the connection timetable page");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "The route list is not displayed in the connection timetable page");
            throw e;
        }
    }

    public void navigateToMyAccount() {
        try {
            actionUtility.clickByAction(myAccount);
            //actionUtility.waitForPageLoad(10);
            reportLogger.log(LogStatus.INFO, "successfully navigated to My Account page");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to navigate to My Account page");
            throw e;
        }
    }

    public void verifyLoginIsSuccessful(HashMap<String,String> testData){
        try{

            reportLogger.log(LogStatus.WARNING, "login is successful");
        }catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "login is not successful");
            throw e;
        }
    }
    public Boolean enterSourceAndDestination(HashMap<String,String> testData,Boolean... cookieClosed){
        //enter source and destination
        Boolean popupHandled = false;
        try {
            if(cookieClosed.length<=0){
                popupHandled = handleHomeScreenPopUp(testData);
            }

            actionUtility.hardSleep(2000);
            //actionUtility.click(findFlights);
            actionUtility.waitForElementClickable(flightFromDropDown,3);
            actionUtility.click(flightFromDropDown);
            actionUtility.hardSleep(2000);
            actionUtility.sendKeysByAction(flightFrom, testData.get("flightFrom") + Keys.RETURN);
            /*actionUtility.waitForElementClickable(flightToDropDown,3);
            actionUtility.click(flightToDropDown);*/
            actionUtility.hardSleep(2000);
            actionUtility.sendKeysByAction(flightTo, testData.get("flightTo") + Keys.RETURN);
            reportLogger.log(LogStatus.INFO,"successfully entered the source - "+testData.get("flightFrom")+" and destination - "+testData.get("flightTo"));
            return popupHandled;
        }catch (Exception e){

            reportLogger.log(LogStatus.INFO,"unable to enter source and destination of the trip");
            throw e;
        }
    }

    public int selectNoOfPassengers(HashMap<String,String> testData){
        //select the no.of passengers
        try {
            int noOfPassengers=1;
            actionUtility.click(openSelectPassengerMenu);
            int noOfTeen = 0, noOfChild = 0, noOfInfant = 0, noOfAdult = 1;
            if (testData.get("noOfTeen") != null) {
                noOfTeen = Integer.parseInt(testData.get("noOfTeen"));

                noOfPassengers += noOfTeen;
                if(noOfTeen == 8)
                {
                    selectPassengers("teens",noOfTeen,Integer.parseInt(testData.get("noOfAdult")));
                }
                else{
                    selectPassengers("teens",noOfTeen);
                }
            }

            if (testData.get("noOfAdult") != null) {
                noOfAdult = Integer.parseInt(testData.get("noOfAdult"));
                selectPassengers("adults", noOfAdult);
                noOfPassengers += (noOfAdult-1);
            }

            if (testData.get("noOfChild") != null) {
                noOfChild = Integer.parseInt(testData.get("noOfChild"));
                selectPassengers("children", noOfChild);
                noOfPassengers += noOfChild;
            }
            if (testData.get("noOfInfant") != null) {
                noOfInfant = Integer.parseInt(testData.get("noOfInfant"));
                selectPassengers("infants", Integer.parseInt(testData.get("noOfInfant")));
            }
            try{
                actionUtility.click(closeSelectPassengerMenu);
            }catch (Exception e){}
            reportLogger.log(LogStatus.INFO,"successfully selected all types of passengers");
            reportLogger.log(LogStatus.INFO,"Adults - "+noOfAdult+" Teens - "+noOfTeen+" Children - "+noOfChild+" Infant - "+noOfInfant);

            return noOfPassengers;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to select the number of passengers");
            throw e;
        }
    }

    public void enterPromoCode(Hashtable<String,String> testData){
        // enter the promocode if present in the test data
        try {
            if (testData.get("promoCodeVal") != null) {
                enterPromoCode(testData.get("promoCodeVal"));
                reportLogger.log(LogStatus.INFO,"successfully entered PROMO code");
            }
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to enter PROMO code");
            throw e;
        }
    }

    public void findFlights(){
        // click on  the find flights
        try{
            actionUtility.hardClick(findFlights);
            actionUtility.hardSleep(3000);
            actionUtility.waitForPageLoad(20);
            actionUtility.hardSleep(3000);
        }catch (TimeoutException e){
            actionUtility.click(findFlights);
            actionUtility.hardSleep(3000);
            actionUtility.waitForPageLoad(20);
            actionUtility.hardSleep(3000);
        }catch (JavascriptException e){
            actionUtility.click(findFlights);
            actionUtility.hardSleep(3000);
            actionUtility.waitForPageLoad(20);
            actionUtility.hardSleep(3000);
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable to search flights");
            throw e;
        }
    }

    public String[] enterTravelDates(HashMap<String,String> testData){
        // enter the travel dates
        String[] travel_date = new String[2];
        try {
            String promoCodeVal = null;
            String date[] = new String[3];
            int departDateOffset = Integer.parseInt(testData.get("departDateOffset"));
            date = actionUtility.getDate(departDateOffset);
            WebElement departure = TestManager.getDriver().findElement(By.id("departureDate"));
            selectDateFromCalendar(date,departure);
            travel_date[0] = date[0]+"-"+date[1]+"-"+date[2];
            reportLogger.log(LogStatus.INFO, "Entered the depart date - "+travel_date[0]);

            if (testData.get("returnDateOffset") != null) {
                int returnDateOffset = Integer.parseInt(testData.get("returnDateOffset"));
                date = actionUtility.getDate(returnDateOffset);
                WebElement returnDate = TestManager.getDriver().findElement(By.id("returnDate"));
                selectDateFromCalendar(date,returnDate);
                travel_date[1] = date[0]+"-"+date[1]+"-"+date[2];
                reportLogger.log(LogStatus.INFO, "Entered the return date - "+travel_date[1]);
            }
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable to enter travel Dates");
            throw e;
        }
        return travel_date;
    }

    private void enterPromoCode(String promoCodeVal){
        // enter the promocode while searching the flight
        actionUtility.hardClick(openPromoCode);
        actionUtility.waitForElementVisible(promoCode,3);
        promoCode.sendKeys(promoCodeVal);

    }

    private void selectDateFromCalendar(String date[],WebElement datePicker){
        //selects the Date from calendar control
        actionUtility.waitForElementVisible(datePicker,3);
        actionUtility.hardClick(datePicker);
//        String date[] = actionUtility.getDate(offset);
        actionUtility.dropdownSelect(datePickerYear, ActionUtility.SelectionType.SELECTBYTEXT,date[2]);
        actionUtility.dropdownSelect(datePickerMonth, ActionUtility.SelectionType.SELECTBYTEXT,date[1]);
        actionUtility.click(TestManager.getDriver().findElement(By.xpath(String.format(datePickerDay,date[0]))));

    }

    private void selectPassengers(String passengerType, int noOfPassenger, int... adultPassenger){
        // selects the no. of passengers
        String passengerNos ;
        boolean flag = true;
        int attempt =0;
        boolean attemptFlag = false;
        while(attempt<8 && attemptFlag == false) {
            try {
                if (!actionUtility.verifyIfElementIsDisplayed(selectPassengerMenu)) {
                    actionUtility.click(openSelectPassengerMenu);

                }

                switch (passengerType) {
                    case ("adults"):
                        while (flag) {

                            actionUtility.waitForElementVisible(selectPassengerMenu,3);
                            passengerNos = adultNos.getAttribute("value");

                            if (Integer.parseInt(passengerNos) < noOfPassenger) {
                                actionUtility.click(adultIncrement);

                            } else if (Integer.parseInt(passengerNos) > noOfPassenger) {
                                actionUtility.click(adultDecrement);
                            } else {
                                flag = false;
                            }
                        }
                        attemptFlag = true;
                        break;

                    case ("teens"):
                        int counter = 0;
                        while (flag) {
                            actionUtility.waitForElementVisible(selectPassengerMenu,3);
                            passengerNos = teenNos.getAttribute("value");

                            if (Integer.parseInt(passengerNos) < noOfPassenger) {
                                actionUtility.click(teenIncrement);
                                counter++;
                                if (counter == 7)
                                    if (adultPassenger.length > 0)
                                        if (adultPassenger[0] == 0)
                                            actionUtility.click(adultDecrement);//Decrement adult when there are 8 teens
                            } else {
                                flag = false;
                            }

                        }
                        attemptFlag = true;
                        break;

                    case ("children"):
                        while (flag) {
                            actionUtility.waitForElementVisible(selectPassengerMenu,3);
                            passengerNos = childrenNos.getAttribute("value");
                            if (Integer.parseInt(passengerNos) < noOfPassenger) {
                                actionUtility.click(childIncrement);
                            } else {
                                flag = false;
                            }
                        }
                        attemptFlag = true;
                        break;

                    case ("infants"):
                        while (flag) {
                            actionUtility.waitForElementVisible(selectPassengerMenu,3);
                            passengerNos = infantNos.getAttribute("value");
                            if (Integer.parseInt(passengerNos) < noOfPassenger) {
                                actionUtility.click(infantIncrement);
                            } else {
                                flag = false;
                            }
                        }
                        attemptFlag = true;
                        break;
                }
            } catch (Exception e) {

                attempt++;
            }
        }
        if (!attemptFlag){
            throw  new RuntimeException("unable to select the passengers");
        }
    }

    public void openLoginPage(HashMap<String,String> testData){
        // opens the digitalProfile page from home page
        try {
            //handleHomeScreenPopUp(testData);

            actionUtility.hardClick(loginOption);
//            actionUtility.waitForPageLoad(10);

            reportLogger.log(LogStatus.INFO, "digital profile page opened");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to open digital profile page");
            throw e;
        }

    }

    public Boolean openLoginRegistrationPage(HashMap<String,String> testData,Boolean...cookieClosed){
        // open the digitalProfile/Registration page from flight search page
        try {
            Boolean popUpHandled = false;
            if(cookieClosed.length<=0){
                popUpHandled = handleHomeScreenPopUp(testData);
            }

            actionUtility.hardClick(login);
            actionUtility.waitForPageLoad(10);
            reportLogger.log(LogStatus.INFO, "digitalProfile page opened");
            return popUpHandled;
        }catch (Exception e){

            reportLogger.log(LogStatus.WARNING, "unable to open digitalProfile page");
            throw e;
        }

    }

    public void navigateToHomepage() {
        //navigate to home page
        try {
            String appUrl = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(TestManager.getTestEnvironment()).get("appUrl").getAsString();
            try {
                TestManager.getDriver().get(appUrl);
            } catch (TimeoutException e) {
            }
            actionUtility.waitForPageURL(appUrl,30);
            actionUtility.waitForPageLoad(25);
            reportLogger.log(LogStatus.INFO, "successfully navigated to home page");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to navigate to the home page");
            throw e;
        }
    }

    public void enterBookingDetailsForCheckIn(String bookingRef, Hashtable<String,String> passengerNames){
        //navigate to the home page and enter the booking ref. number and other details in check-in tab
        try {
            actionUtility.hardSleep(3000);
            boolean flag = true;
            int attempt = 0;
            while (flag && attempt<4) {
                navigateToHomepage();
                //handleHomeScreenPopUp();
                actionUtility.hardSleep(2000);
                actionUtility.hardClick(checkInTab);
                bookingRefForCheckIn.sendKeys(bookingRef);
                String[] nameForCheckIn = passengerNames.get("passengerName1").split(" ");
                foreNameForCheckIn.sendKeys(nameForCheckIn[1]);
                surNameForCheckIn.sendKeys(nameForCheckIn[2]);
                actionUtility.hardSleep(1000);
                actionUtility.clickByAction(checkInButton);
                actionUtility.waitForPageLoad(10);

                try{
                    actionUtility.waitAndSwitchToFrame(ActionUtility.FrameLocator.WEBELEMENT,CheckinFrame);
                    flag =false;
                    break;
                }catch (TimeoutException e){
                    actionUtility.hardSleep(2000);
                }

                attempt++;
            }
            if(!flag) {
                actionUtility.hardSleep(1000);

                try {
                    actionUtility.waitForElementVisible(checkInLoader, 15);
                }catch (TimeoutException e){}

                int loadChecker=0;
                while (actionUtility.verifyIfElementIsDisplayed(checkInLoader,3) && loadChecker<3) {
                    try {
                        actionUtility.waitForElementNotPresent("//span[@class='loading-spinner']", 30);
                    }catch (TimeoutException e){
                        loadChecker++;
                    }
                }
                reportLogger.log(LogStatus.INFO, "successfully entered the booking details and continued to check-in");
            }else {
                throw new RuntimeException("unable to enter the booking details and continue to check-in");
            }
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to enter the booking details and continue to check-in");
            throw e;
        }
    }


    public Boolean handleHomeScreenPopUp(HashMap<String,String> testData){
//     Handles the home screen popup/modal
        Boolean popUpHandled = false;
        String popUpMsg = "no cookie modal";
        try{
            try {
                actionUtility.waitForElementVisible(popUp, 2);
                popUpMsg = popUpHeader.getText();
                System.out.println(popUpHeader.getText() + "---"+popUpMsg);
                reportLogger.log(LogStatus.INFO,"home screen cookie modal is displayed");

            }catch (TimeoutException e){
                reportLogger.log(LogStatus.INFO,"home screen cookie modal is not displayed");

            }
            if(popUpMsg.equals("This website uses cookies")){
                int pref = 0;
                String[] preference = {"strictly necessary","analytical and performance","personalization","targeting and marketing"};
                if(testData.get("amendCookie")!=null && testData.get("amendCookie").equalsIgnoreCase("yes")){
                    actionUtility.hardClick(amendCookies);
                    if(testData.get("cookiePreferenceNotNeeded1")!=null) {
                        pref = Arrays.asList(preference).indexOf(testData.get("cookiePreferenceNotNeeded1").toLowerCase());
                        actionUtility.hardClick(cookiePreference.get(pref));
                    }
                    if(testData.get("cookiePreferenceNotNeeded2")!=null){
                        pref = Arrays.asList(preference).indexOf(testData.get("cookiePreferenceNotNeeded2").toLowerCase());
                        actionUtility.hardClick(cookiePreference.get(pref));
                    }
                    if(testData.get("cookiePreferenceNotNeeded3")!=null){
                        pref = Arrays.asList(preference).indexOf(testData.get("cookiePreferenceNotNeeded3").toLowerCase());
                        actionUtility.hardClick(cookiePreference.get(pref));
                    }
                    actionUtility.hardClick(cookiePrefDone);
                }
                else {
                    actionUtility.waitForElementClickable(acceptCookies, 3);
                    actionUtility.hardClick(acceptCookies);
                }
                popUpHandled = true;
                reportLogger.log(LogStatus.INFO,"successfully handled the cookie modal");
            }

            //reportLogger.log(LogStatus.INFO,"successfully handled the cookie modal");
            //popUpHandled = true;
            if(popUpMsg.toLowerCase().equals("no cookie modal")) {
                throw new RuntimeException("Cookie Modal is not displayed");
            }
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to close the home screen cookie modal or cookie modal is not displayed");
            //throw e;
        }
        return popUpHandled;
    }


    public void verifyMessageUnaccompaniedMinorForTeens() {
        //verifies the message displayed only teen is traveling from/to Italy

        String messageDisplayed =null;
        String messageTobeDisplayed = null;


        try{

            messageDisplayed = unaccompaniedMinorMessage.getText();
            messageTobeDisplayed = "Teens cannot fly unaccompanied on flights to or from Italy.";

            Assert.assertEquals(messageDisplayed.trim(),messageTobeDisplayed.trim(),"wrong message displayed for unaccompanied teen traveling from/to italy  ");
            reportLogger.log(LogStatus.INFO,"right message is displayed for unaccompanied teen traveling to/from italy");

        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"Wrong message is displayed for unaccompanied teen traveling to/from italy "+"Expected: "+messageTobeDisplayed+" Actual: "+messageDisplayed);
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"Unable to check the message displayed for teen traveling to/from italy");
            throw e;
        }



    }

    public void verifyMessageForMoreThanEightPassengers() {
        //verifies the message displayed only teen is traveling from/to Italy

        String messageDisplayed = null;
        String messageTobeDisplayed = null;
        try{

            messageDisplayed = moreThanEightPassengersMessage.getText();
            messageTobeDisplayed = "Booking for nine or more passengers? Visit our groups page.";

            Assert.assertEquals(messageDisplayed.trim(),messageTobeDisplayed.trim(),"wrong message displayed when more than eight passengers are selected");
            reportLogger.log(LogStatus.INFO,"right message displayed when more than eight passengers are selected");

        }catch (AssertionError e){
            reportLogger.log(LogStatus.WARNING,"Wrong message displayed when more than eight passengers are selected Expected: "+messageTobeDisplayed+" Actual: "+messageDisplayed);
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"Unable to check the message displayed when more than eight passengers are selected");
            throw e;
        }

    }

    public void verifyHomePageFooter(HashMap<String,String> testData){
        try {
            String fPara1 = "Flybe is committed to complete transparency regarding how we charge for our flights.";

            //String fPara2 = "All fares quoted on Flybe.com are subject to availability, and are inclusive of all taxes, fees and charges.";

            String fPara2 = "More information is available on our ancillary charges and our pricing guide.";

            String fPara3 = "Flybe Limited, registered company 02769768, New Walker Hangar, Exeter International Airport, Exeter, Devon EX5 2BA";
//            handleHomeScreenPopUp(testData);
//            actionUtility.hardSleep(2000);

            //verifySubLinks();
            verifyFooter();
            try {
                Assert.assertTrue(flybeLogo.isDisplayed(), "Flybe Logo is not displayed in Home Page");
                reportLogger.log(LogStatus.INFO, "Flybe Logo is displayed in Home Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Flybe Logo is not displayed in Home Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Flybe Logo is not displayed in Home Page");
                throw e;
            }

            try {
                Assert.assertTrue(flybeCopyRight.isDisplayed(), "Copy Right image is not displayed in Home Page");
                Calendar now = Calendar.getInstance();   // Gets the current date and time
                int year = now.get(Calendar.YEAR);       // The current year
                Assert.assertTrue(flybeCopyRight.getText().equals("Flybe © "+year+". All rights reserved"));
                reportLogger.log(LogStatus.INFO, "Copy Right is displayed in Home Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Copy Right is not displayed in Home Page or the year displayed is incorrect");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Copy Right is not displayed in Home Page");
                throw e;
            }


            /*try {
                Assert.assertTrue(privacyPolicyLink.isDisplayed(), "Privacy Policy Link is not displayed in Home Page");
                reportLogger.log(LogStatus.INFO, "Privacy Policy Link is displayed in Home Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Privacy Policy Link is not displayed in Home Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Privacy Policy Link is not displayed in Home Page");
                throw e;
            }

            try {
                Assert.assertTrue(cookiePolicyLink.isDisplayed(), "Cookie Policy Link is not displayed in Home Page");
                reportLogger.log(LogStatus.INFO, "Cookie Policy Link is displayed in Home Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Cookie Policy Link is not displayed in Home Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Cookie Policy Link is not displayed in Home Page");
                throw e;
            }

            try {
                Assert.assertTrue(termsOfUseLink.isDisplayed(), "Website Terms Of Use Link is not displayed in Home Page");
                reportLogger.log(LogStatus.INFO, "Website Terms Of Use Link is displayed in Home Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Website Terms Of Use Link is not displayed in Home Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Website Terms Of Use Link is not displayed in Home Page");
                throw e;
            }

            try {
                Assert.assertTrue(termsAndConditionsLink.isDisplayed(), "Terms And  Conditions Link is not displayed in Home Page");
                reportLogger.log(LogStatus.INFO, "Website Terms And Conditions Link is displayed in Home Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Website Terms And Conditions Link is not displayed in Home Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Website Terms And Conditions Link is not displayed in Home Page");
                throw e;
            }

            try {
                Assert.assertTrue(JouneryComparisionTimesLink.isDisplayed(), "Jounery Comparision Times Link is not displayed in Home Page");
                reportLogger.log(LogStatus.INFO, "Jounery Comparision Times Link is displayed in Home Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Jounery Comparision Times Link is not displayed in Home Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Jounery Comparision Times Link is not displayed in Home Page");
                throw e;
            }

            try {
                Assert.assertTrue(ContactUsLink.isDisplayed(), "Contact Us Link is not displayed in Home Page");
                reportLogger.log(LogStatus.INFO, "Contact Us Link is displayed in Home Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Contact Us Link is not displayed in Home Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Contact Us Link is not displayed in Home Page");
                throw e;
            }*/

            try {
                Assert.assertTrue(flybeInHeader.isDisplayed(), "Flybe.com Link is not displayed in Home Page");
                reportLogger.log(LogStatus.INFO, "Flybe.com Link is displayed in Home Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Flybe.com Link is not displayed in Home Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Flybe.com Link is not displayed in Home Page");
                throw e;
            }

            try {
                Assert.assertTrue(ancillaryCharges.isDisplayed(), "Ancillary Charges Link is not displayed in Home Page");
                reportLogger.log(LogStatus.INFO, "Ancillary Charges Link is displayed in Home Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Ancillary Charges Link is not displayed in Home Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Ancillary Charges Link is not displayed in Home Page");
                throw e;
            }

            try {
                Assert.assertTrue(priceGuide.isDisplayed(), "Pricing Guide Link is not displayed in Home Page");
                reportLogger.log(LogStatus.INFO, "Pricing Guide Link is displayed in Home Page");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Pricing Guide Link is not displayed in Home Page");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Pricing Guide Link is not displayed in Home Page");
                throw e;
            }

            try {
                Assert.assertEquals(footerPara1.getText().toLowerCase(), fPara1.toLowerCase(), "Footer para is not matching");
                reportLogger.log(LogStatus.INFO, "Footer para is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching");
                throw e;
            }

            /*try {//fpara2 removed from site in feb2018
                Assert.assertEquals(footerPara2.getText().toLowerCase(), fPara2.toLowerCase(), "Footer para is not matching");
                reportLogger.log(LogStatus.INFO, "Footer para is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching");
                throw e;
            }*/

            try {
                Assert.assertEquals(footerPara2.getText().toLowerCase(), fPara2.toLowerCase(), "Footer para is not matching");
                reportLogger.log(LogStatus.INFO, "Footer para is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching");
                throw e;
            }
            try {
                Assert.assertEquals(footerPara3.getText().toLowerCase(), fPara3.toLowerCase(), "Footer para is not matching");
                reportLogger.log(LogStatus.INFO, "Footer para is matching");
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching");
                throw e;
            } catch (NoSuchElementException e) {
                reportLogger.log(LogStatus.WARNING, "Footer para is not matching");
                throw e;
            }
        }catch (AssertionError e){
            throw e;
        }catch(NoSuchElementException e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to verify footer in cheap flight page");
            throw e;
        }

    }

    public void verifySubLinks() {
        try {
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(aboutUsLink));
            reportLogger.log(LogStatus.INFO, "About Flybe link is displayed");
            /*actionUtility.click(aboutUsLink);
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed());*/

        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify About Us link");
            throw e;
        }
    }

    public void verifyFooter(){

        try{
            String[] aboutUsExpectedLinks = {"About Us","Careers","Investors","Media","Corporate","Trade"};
            for(int i=0;i<AboutUsLinks.size();i++){
                Assert.assertEquals(AboutUsLinks.get(i).getText(),aboutUsExpectedLinks[i]);
            }
            reportLogger.log(LogStatus.INFO,"About Us links Careers,Investors,Media,Corporate,Trade displayed in footer is correct");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"About Us links displayed in footer is not correct");
            throw e;
        }
        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(aviationServicesLink));
            reportLogger.log(LogStatus.INFO,"Flybe Aviation Services link is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Flybe Aviation Services link in the footer");
            throw e;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(trainingAcademyLink));
            reportLogger.log(LogStatus.INFO,"Flybe Training Academy link is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Flybe Training Academy link in the footer");
            throw e;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(xmlApiLink));
            reportLogger.log(LogStatus.INFO,"Flybe XML/API link is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Flybe XML/API link in the footer");
            throw e;
        }
        String link = null;
        try{
            String[] expectedInformationLinks = {"Passenger Assistance","Privacy Policy","Cookie Policy","Cookie Settings","Website terms of use","Contact Us"};
            for(int i=0;i<informationLinks.size();i++){
                try {
                    Assert.assertTrue(informationLinks.get(i).getText().equals(expectedInformationLinks[i]));
                }catch (AssertionError e) {
                    reportLogger.log(LogStatus.WARNING, "Link for "+link+" is not displayed");
                    throw e;
                }
                link = expectedInformationLinks[i];
            }
            reportLogger.log(LogStatus.INFO,"\"Passenger Assistance\",\"Privacy Policy\",\"Cookie Policy\",\"Website terms of use\",\"Contact Us\" links are correctly displayed");
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to verify footer links");
            throw e;
        }
        /*try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(passengerAssistanceLink));
            reportLogger.log(LogStatus.INFO,"Passenger Assistance link is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Passenger Assistance link in the footer");
            throw e;
        }*/



        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(twitterLink));
            reportLogger.log(LogStatus.INFO,"Flybe Twitter link is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Flybe Twitter link in the footer");
            throw e;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(fbLink));
            reportLogger.log(LogStatus.INFO,"Flybe facebook link is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Flybe facebook link in the footer");
            throw e;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(instagramLink));
            reportLogger.log(LogStatus.INFO,"Flybe Instagram link is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Flybe Instagram link in the footer");
            throw e;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(youtubeLink));
            reportLogger.log(LogStatus.INFO,"Flybe YouTube link is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Flybe YouTube link in the footer");
            throw e;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(linkedinLink));
            reportLogger.log(LogStatus.INFO,"Flybe LinkedIn link is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Flybe LinkedIn link in the footer");
            throw e;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(subscribeLink));
            reportLogger.log(LogStatus.INFO,"Flybe Subscribe link is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Subscribe link in the footer");
            throw e;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(aboutUsHeader));
            reportLogger.log(LogStatus.INFO,"Corporate header is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Corporate header in the footer");
            throw e;
        }

        try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(otherFlybeWebsitesHeader));
            reportLogger.log(LogStatus.INFO,"Other Flybe Sites header is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Other Flybe Sites header in the footer");
            throw e;
        }

       /* try{
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(joinTheConversationHeader));
            reportLogger.log(LogStatus.INFO,"Join The Conversation header is displayed in the footer");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to verify Join The Conversation header in the footer");
            throw e;
        }*/
    }

    public void verifyTheStaticContentsInHomePage(){
        try{
            //handleHomeScreenPopUp();
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(heroBanner), "Hero banner is not displayed in Home Page");
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(carouselSection), "Carousel is not displayed in Home Page");
//            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(routeMap), "Route Map is not displayed in Home Page");
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(offerTiles), "Offer Tiles is not displayed in Home Page");
            actionUtility.click(checkInTab);
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(bookingRefForCheckIn),"Booking reference is not displayed in check-in tab");
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(foreNameForCheckIn),"Fore name is not displayed in check-in tab");
            //verifyHomePageFooter();//covered in R19
            actionUtility.click(findFlightTab);
        }catch(AssertionError e){
            throw e;
        }
        catch(Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable to verify contents in home page");
            throw e;

        }
    }

    public void navigateToArrivalAndDeparture(HashMap<String,String> testData){

        try{

            handleHomeScreenPopUp(testData);
            actionUtility.clickByAction(flightInfoAndHelp);
            actionUtility.clickByAction(liveArrivalsAndDepartures);
            actionUtility.waitForPageLoad(10);
            reportLogger.log(LogStatus.INFO,"Successfully navigated to flight in arrival and departure page");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to navigate to flight in arrival and departure page");
            throw e;
        }
    }

    public void enterPLDBookingDetailsForAmending(String pldbookingRef, Hashtable<String,String> passengerNames,HashMap<String,String> testData){
        //retrieve the PLDbooking to confirm booking
        try {

            navigateToHomepage();
            handleHomeScreenPopUp(testData);

            actionUtility.clickByAction(manageBooking);
            actionUtility.clickByAction(retrievePLDTab);
            actionUtility.waitForPageLoad(10);
            actionUtility.hardSleep(1000);
            actionUtility.waitForElementVisible(pldReferenceNumberForSearch,30);
            pldReferenceNumberForSearch.sendKeys(pldbookingRef);
            pldForenameForSearch.sendKeys(passengerNames.get("forename"));
            pldSurnameForSearch.sendKeys(passengerNames.get("surname"));
            // actionUtility.hardSleep(1000);
            actionUtility.click(pldContinueButtonForSearch);
            actionUtility.waitForPageLoad(10);

            reportLogger.log(LogStatus.INFO, "successfully retrieved the booking through manage-booking-Retrieve Price Lock-Down Booking");

        } catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to retrieve the booking through manage-booking-Retrieve Price Lock-Down Booking");
            throw e;
        }
    }

    public void selectRecentFlight(Hashtable<String,String> testData,int indexFlightToSelect){
//        verify the recent search route and clicks on the recent searched flight
        try {
            //handleHomeScreenPopUp(testData);
            int dateOffSet = Integer.parseInt(testData.get("departDateOffset"));
            String dateExpected[] = actionUtility.getDate(dateOffSet);
            String dayExpected = dateExpected[0];
            String monthExpected = actionUtility.getMonth(dateOffSet);
            System.out.println(monthExpected);
            actionUtility.waitForElementVisible(openRecentSearchTab, 3);
            actionUtility.click(openRecentSearchTab);
            // ********verifyRecentSearchedFlightsDisplayedIsCorrect(testData, dateExpected[0]+" "+monthExpected, indexFlightToSelect);
            actionUtility.click(recentSearchRoutes.get(indexFlightToSelect - 1 + 0));
            actionUtility.hardSleep(3000);
            actionUtility.waitForPageLoad(20);
            actionUtility.hardSleep(3000);
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify recent search button");
            throw e;
        }
    }

    public void verifyRecentSearchedFlightsDisplayedIsCorrect(HashMap<String,String> details,HashMap<String,String> testData,int routeNo/*, String recentSearchDateExpected **************, int indexFlightToSelect*/){
//        verify the recent search route
        try{
            actionUtility.waitForElementVisible(openRecentSearchTab,90);
            actionUtility.click(openRecentSearchTab);
            actionUtility.waitForElementVisible(recentSearchPanel,3);
            try{
                Assert.assertEquals(recentSearchRoutes.size(),routeNo*2);
            }catch (AssertionError e){
                reportLogger.log(LogStatus.INFO,"for "+routeNo+" routes searched, only "+recentSearchRoutes.size()+" recent search options are displayed");
                throw e;
            }
            //*************String depAirportExpected = testData.get("recentSearchDepartureAirport");
            String depAirportExpected = details.get("departure");
            //*******String depAirportActual = recentSearchRoutes.get(indexFlightToSelect - 1 + 0).getText().toString().trim();
            String depAirportActual = recentSearchRoutes.get(0).getText().toString().trim();
            try {
                Assert.assertTrue(depAirportExpected.contains(depAirportActual) ,"recent search departure airport name is not matching");
                reportLogger.log(LogStatus.INFO,"recent search departure airport is matching");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.INFO,"recent search departure airport is not matching, Expected- "+ depAirportExpected+" Actual- "+depAirportActual);
                throw e;
            }

            //***********String destAirportExpected = testData.get("recentSearchDestinationAirport");
            String destAirportExpected = details.get("destination");
            //************String destAirportActual = recentSearchRoutes.get(indexFlightToSelect - 1 + 1).getText().toString().trim();
            String destAirportActual = recentSearchRoutes.get(1).getText().toString().trim();
            try{
                Assert.assertTrue(destAirportExpected.contains(destAirportActual),"recent search destination airport name is not matching");
                reportLogger.log(LogStatus.INFO,"recent search destination airport is matching");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.INFO,"recent search destination airport is not matching Expected - "+destAirportExpected+" Actual - "+destAirportActual);
                throw e;
            }

            //String recentSearchDateExpected = dateExpected[0]+" "+dateExpected[1];
            //********String recentSearchDateActual = recentSearchDates.get(indexFlightToSelect-1).getText().toString().trim();
            String recentSearchDateActual = recentSearchDates.get(0).getText().toString().trim();
            int dateOffSet = Integer.parseInt(testData.get("departDateOffset"));
            String dateExpected[] = actionUtility.getDate(dateOffSet);
            String dayExpected = dateExpected[0];
            String monthExpected = actionUtility.getMonth(dateOffSet);
            String recentSearchDateExpected = dayExpected+" "+monthExpected;

            try{
                Assert.assertTrue(dayExpected.contains(recentSearchDateExpected.split(" ")[0]));
                Assert.assertTrue(monthExpected.equals(recentSearchDateExpected.split(" ")[1]));
                reportLogger.log(LogStatus.INFO,"recent search date is matching");
            }catch (AssertionError e){
                reportLogger.log(LogStatus.INFO,"recent search date is not matching Expected - " + recentSearchDateExpected + "Actual - "+recentSearchDateActual);
                throw e;
            }
        }catch (AssertionError e){
            reportLogger.log(LogStatus.INFO,"recent search flight details are not matching");
            //throw new RuntimeException("recent search flight details are not matching");
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"recent search panel is not displayed");
            //   throw new RuntimeException("recent search panel is not displayed");
            throw e;
        }
    }

    public void addCurrencyCookie(Hashtable<String,String> testData){
//        this method will add currency cookie which is read from testdata
        try{
            String currencyCookie = testData.get("currencyCookie");
            String currencyCookieValue = testData.get("currencyCookieValue");
            addCookie(currencyCookie,currencyCookieValue);
        }catch (WebDriverException e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to read country cookie");
            throw e;
        }
    }

    public void addCountryCookie(Hashtable<String,String> testData){
//        this method will add country cookie which is read from testdata
        try{
            String countryCookie = testData.get("countryCookie");
            String countryCookieValue = testData.get("countryCookieValue");
            addCookie(countryCookie,countryCookieValue);
        }catch (WebDriverException e){
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to read country cookie");
            throw e;
        }
    }

    public void addCookie(String cookieName, String cookieValue){
//        this method will the cookie
        try {
            Cookie newCookieToBeAdded = new Cookie(cookieName, cookieValue);
            TestManager.getDriver().manage().addCookie(newCookieToBeAdded);
            reportLogger.log(LogStatus.INFO,"successfully added the new cookie-"+cookieName+" with value-"+cookieValue);
        }catch (WebDriverException e){
            reportLogger.log(LogStatus.INFO,"unable to add the cookie-"+cookieName+" with value-"+cookieValue);
            throw e;
        }
    }

    public void selectPlaceWeFlyFrom(String place){
        //selects the place we fly from specified
        try{
            actionUtility.hardClick(TestManager.getDriver().findElements(By.linkText(place)).get(0));
            reportLogger.log(LogStatus.INFO,"successfully selected "+place+" from places we fly from list");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select "+place+" from places we fly from list");
            throw e;
        }
    }

    public void selectPopularDestination(String place){
        //selects the place we fly from specified
        try{
            actionUtility.hardClick(TestManager.getDriver().findElement(By.xpath(String.format("//div[h2[text()='Popular destinations']]//a[text()='%s']",place))));
            reportLogger.log(LogStatus.INFO,"successfully selected "+place+" from places we fly from list");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select "+place+" from places we fly from list");
            throw e;
        }
    }

    public void verifyFeaturedRoutesSection(String... lang) {
        //verifies that there are six tiles under featured routes
        //verifies the disclaimer displayed
        //verifies explore more destinations
        try{
            try {
                actionUtility.waitForElementVisible(featuredFlightDisclaimer, 30);
                Assert.assertEquals(carousalTiles.size(), 6);
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.INFO, "no. of featured routes is not six but " + carousalTiles.size());
                throw e;
            }
            if(lang.length>0){
                verifyDisclaimer(lang);
                verifyExploreMore(lang);
            }else{
                verifyDisclaimer();
                verifyExploreMore();
            }
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify the presence of featured routes");
            throw e;
        }
    }

    private void verifyDisclaimer(String... lang){
        //verifies the disclaimer displayed
        String expectedDisclaimer = null;
        String displayedDisclaimer = featuredFlightDisclaimer.getText();
        try{
            int plusTwoMonthsYear = LocalDate.now().plusMonths(2).getYear();
            int plusThreeMonthsYear = LocalDate.now().plusMonths(3).getYear();
            String monthsExpected = null;
            String date  =null;
            ZonedDateTime zonedDateTime = ZonedDateTime.now(ZoneId.of("Asia/Kolkata"));
            LocalDate presentIndiaDate = zonedDateTime.toLocalDate();
            LocalTime presentIndiaTime = zonedDateTime.toLocalTime();
            LocalTime IndiaTime = LocalTime.parse("12:30:00");
            if(lang.length==0) {
                if(plusThreeMonthsYear==plusTwoMonthsYear) {
                    monthsExpected = LocalDate.now().plusMonths(2).getMonth().toString() + " and " + LocalDate.now().plusMonths(3).getMonth() + " " +plusThreeMonthsYear;
                }
                else{
                    monthsExpected = LocalDate.now().plusMonths(2).getMonth().toString()+" "+ plusTwoMonthsYear + " and " + LocalDate.now().plusMonths(3).getMonth() + " " +plusThreeMonthsYear;
                }
                if (presentIndiaTime.isBefore(IndiaTime)) {
                    //date = LocalDate.now().minusDays(1).format(DateTimeFormatter.ofPattern("d MMMM yyyy")).toString();
                    date = presentIndiaDate.minusDays(1).format(DateTimeFormatter.ofPattern("d MMMM yyyy")).toString();
                } else {
                    //date = LocalDate.now().format(DateTimeFormatter.ofPattern("d MMMM yyyy")).toString();
                    date = presentIndiaDate.format(DateTimeFormatter.ofPattern("d MMMM yyyy")).toString();
                }
                expectedDisclaimer = "*Prices listed are for flights in the travel period of " + monthsExpected + ". All fares shown are one-way including all taxes & surcharges. Fares valid as at " + date /*+ ", 12:30"*/;
                Assert.assertTrue(displayedDisclaimer.toLowerCase().contains(expectedDisclaimer.toLowerCase()), expectedDisclaimer);
            }
            else if(lang[0].equals("FR")){
                if(plusThreeMonthsYear==plusTwoMonthsYear) {
                    monthsExpected = LocalDate.now().plusMonths(2).getMonth().getDisplayName(TextStyle.FULL,Locale.FRENCH).toString() + " et " + LocalDate.now().plusMonths(3).getMonth().getDisplayName(TextStyle.FULL,Locale.FRENCH) + " " +plusThreeMonthsYear;
                }
                else{
                    monthsExpected = LocalDate.now().plusMonths(2).getMonth().getDisplayName(TextStyle.FULL,Locale.FRENCH).toString()+" "+ plusTwoMonthsYear + " et " + LocalDate.now().plusMonths(3).getMonth().getDisplayName(TextStyle.FULL,Locale.FRENCH) + " " +plusThreeMonthsYear;
                }
                //monthsExpected = LocalDate.now().plusMonths(2).getMonth().getDisplayName(TextStyle.FULL,Locale.FRENCH).toString()+" et "+LocalDate.now().plusMonths(3).getMonth().getDisplayName(TextStyle.FULL,Locale.FRENCH)+" "+LocalDate.now().plusMonths(2).getYear();
                if (presentIndiaTime.isBefore(IndiaTime)) {
                    date = presentIndiaDate.minusDays(1).format(DateTimeFormatter.ofPattern("d MMMM yyyy").withLocale(Locale.FRENCH)).toString();
                } else {
                    date = presentIndiaDate.format(DateTimeFormatter.ofPattern("d MMMM yyyy").withLocale(Locale.FRENCH)).toString();
                }
                expectedDisclaimer = "*Les prix mentionnés sont ceux des vols de la période de voyage " + monthsExpected + ". Tous les tarifs indiqués sont pour des allers simples, taxes et suppléments inclus. Les tarifs sont en vigueur au " + date /*+ ", 12:30"*/;
                Assert.assertTrue(displayedDisclaimer.toLowerCase().contains(expectedDisclaimer.toLowerCase()), expectedDisclaimer);
            }
            else if(lang[0].equals("DE")){
                if(plusThreeMonthsYear==plusTwoMonthsYear) {
                    monthsExpected = LocalDate.now().plusMonths(2).getMonth().getDisplayName(TextStyle.FULL,Locale.GERMAN).toString() + " und " + LocalDate.now().plusMonths(3).getMonth().getDisplayName(TextStyle.FULL,Locale.GERMAN) + " " +plusThreeMonthsYear;
                }
                else{
                    monthsExpected = LocalDate.now().plusMonths(2).getMonth().getDisplayName(TextStyle.FULL,Locale.GERMAN).toString()+" "+ plusTwoMonthsYear + " und " + LocalDate.now().plusMonths(3).getMonth().getDisplayName(TextStyle.FULL,Locale.GERMAN) + " " +plusThreeMonthsYear;
                }
                //monthsExpected = LocalDate.now().plusMonths(2).getMonth().getDisplayName(TextStyle.FULL,Locale.GERMAN).toString()+" und "+LocalDate.now().plusMonths(3).getMonth().getDisplayName(TextStyle.FULL,Locale.GERMAN)+" "+LocalDate.now().plusMonths(2).getYear();
                if (presentIndiaTime.isBefore(IndiaTime)) {
                    date = presentIndiaDate.minusDays(1).format(DateTimeFormatter.ofPattern("d MMMM yyyy").withLocale(Locale.GERMAN)).toString();
                } else {
                    date = presentIndiaDate.format(DateTimeFormatter.ofPattern("d MMMM yyyy").withLocale(Locale.GERMAN)).toString();
                }
                expectedDisclaimer = "*Die aufgelisteten Preise gelten für Flüge im Reisezeitraum von " + monthsExpected + ". Alle angegebenen Flugpreise gelten für die einfache Strecke und enthalten alle Steuern & Gebühren. Flugpreise gültig ab " + date /*+ ", 12:30"*/;
                Assert.assertTrue(displayedDisclaimer.toLowerCase().contains(expectedDisclaimer.toLowerCase()), expectedDisclaimer);
            }
            else if(lang[0].equals("ES")){
                if(plusThreeMonthsYear==plusTwoMonthsYear) {
                    monthsExpected = LocalDate.now().plusMonths(2).getMonth().getDisplayName(TextStyle.FULL,new Locale("es","ES")).toString() + " y " + LocalDate.now().plusMonths(3).getMonth().getDisplayName(TextStyle.FULL,new Locale("es","ES")) + " " +plusThreeMonthsYear;
                }
                else{
                    monthsExpected = LocalDate.now().plusMonths(2).getMonth().getDisplayName(TextStyle.FULL,new Locale("es","ES")).toString()+" "+ plusTwoMonthsYear + " y " + LocalDate.now().plusMonths(3).getMonth().getDisplayName(TextStyle.FULL,new Locale("es","ES")) + " " +plusThreeMonthsYear;
                }

                //monthsExpected = LocalDate.now().plusMonths(2).getMonth().getDisplayName(TextStyle.FULL,new Locale("es","ES")).toString()+" y "+LocalDate.now().plusMonths(3).getMonth().getDisplayName(TextStyle.FULL,new Locale("es","ES"))+" "+LocalDate.now().plusMonths(2).getYear();
                System.out.println("ES "+monthsExpected);
                if (presentIndiaTime.isBefore(IndiaTime)) {
                    date = presentIndiaDate.minusDays(1).format(DateTimeFormatter.ofPattern("d MMMM yyyy").withLocale(new Locale("es","ES"))).toString();
                } else {
                    date = presentIndiaDate.format(DateTimeFormatter.ofPattern("d MMMM yyyy").withLocale(new Locale("es","ES"))).toString();
                }
                expectedDisclaimer = "*Los precios indicados son para vuelos dentro del periodo de viaje de " + monthsExpected + ". Todas las tarifas mostradas son para viaje de ida, incluidos impuestos y recargos. Tarifas válidas hasta el " + date /*+ ", 12:30"*/;
                Assert.assertTrue(displayedDisclaimer.toLowerCase().contains(expectedDisclaimer.toLowerCase()));
            }
        }catch(AssertionError e) {
            System.out.println("expectedDisclaimer  "+expectedDisclaimer);
            System.out.println("displayedDisclaimer  "+displayedDisclaimer);
            reportLogger.log(LogStatus.INFO, "featured flight disclaimer displayed - "+displayedDisclaimer+" does not match with expected disclaimer - "+expectedDisclaimer);
            throw e;
        }
    }

    private void  verifyExploreMore(String... lang){
        //verifies explore more button
        String currentURL = null;

        try{
            /*url = TestManager.getDriver().getCurrentUrl();
            if(lang.length>0){
                url = url.split(lang[0].toLowerCase())[0];
            }
            url = url.split(".")[1];*/
            actionUtility.click(exploreMoreDestinations);
            currentURL = TestManager.getDriver().getCurrentUrl();
            Assert.assertTrue(currentURL.contains("cheap-flights"),currentURL);
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(exploreMorePageContent));
            TestManager.getDriver().navigate().back();
            reportLogger.log(LogStatus.INFO,"successfully verified 'explore more destination' button");
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"clicking on 'explore more destinations' takes the  user to "+currentURL+" instead of url - https://www.itflybe.com/cheap-flights");
            throw e;
        }
    }

    public HashMap<String,String> selectFeaturedRouteAndGetDetails(int tile){
        //selects a featured route and captures price,source and destination
        HashMap<String,String> selectedDetails = new HashMap<>();
        try{
            String price = carousalTiles.get(tile).findElement(By.className("price")).getText();
            selectedDetails.put("price",price.substring(0,price.length()-1));
            selectedDetails.put("departure",carousalTiles.get(tile).findElement(By.className("dest-1")).getText());
            selectedDetails.put("destination",carousalTiles.get(tile).findElement(By.className("dest-2")).getText());
            System.out.println(selectedDetails);
            actionUtility.click(carousalTiles.get(tile));
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select a featured route and capture route details");
        }
        return selectedDetails;
    }

    public void verifyIfSearchedRouteIsFeatured(HashMap<String,String> details){
        try{
            actionUtility.waitForElementVisible(carousalTiles,90);
            System.out.println("featured route"+carousalTiles.size());
            int i = 0;
            for(;i<carousalTiles.size();i++){
                String departure = carousalTiles.get(i).findElement(By.className("dest-1")).getText();
                if(details.get("departure").contains(departure)){
                    String destination = carousalTiles.get(i).findElement(By.className("dest-2")).getText();
                    if(details.get("destination").contains(destination)){
                        break;
                        /*String price = carousalTiles.get(i).findElement(By.className("price")).getText();
                        try {
                            Assert.assertTrue(price.substring(0, price.length() - 1).equals(details.get("price")));
                            break;
                        }catch(AssertionError e){
                            reportLogger.log(LogStatus.INFO,"price displayed for featured route: "+details.get("departure")+" to "+details.get("destination")+" is"+price.substring(0, price.length() - 1)+"which is not same as that displayed on fare select page"+details.get("price"));
                            throw e;
                        }*/
                    }
                }
            }
            try{
                Assert.assertNotEquals(i,carousalTiles.size(),"route is not displayed in carousal");
            }catch(AssertionError e){
                reportLogger.log(LogStatus.INFO,"recent searched route "+details.get("departure")+" to "+details.get("destination")+" is not listed in featured routes");
                throw e;
            }
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify if recent searches are featured");
            throw e;
        }
    }

    public void verifyAlert(){
        try{
            try {
                Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(disruptionAlert));
            }catch (AssertionError e){
                reportLogger.log(LogStatus.INFO,"alert messages are not present");
                throw e;
            }
            do {
                actionUtility.hardClick(hideAlert);
            } while (actionUtility.verifyIfElementIsDisplayed(hideAlert));

            Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(disruptionAlert));
            reportLogger.log(LogStatus.INFO,"alert message is displayed and able to hide alert using 'hide alert' link");
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"alert message does not get hidden on clicking 'hide alert' link");
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify if alert message is present");
            throw e;
        }
    }

    public void navigateToLanguageVariant(String lang){
        try{
            String url = TestManager.getDriver().getCurrentUrl();
            actionUtility.hardClick(changeLanguage);
//            actionUtility.waitForElementVisible(langDropdown,30);
            actionUtility.hardClick(TestManager.getDriver().findElement(By.cssSelector(String.format(selectLanguage,lang.toLowerCase()))));
            actionUtility.waitForPageURL(url+lang.toLowerCase(),30);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to language site "+lang);
            throw e;
        }
    }

    public void verifyIfSearchWidgetIsDisplayed(){
        try{
            actionUtility.waitForPageLoad(90);
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(searchWidget));
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"search widget is not displayed");
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify if search widget is displayed");
            throw e;
        }
    }

    public void selectHeaderMenu(String menu){
        //selects menu specified from the header section
        try{
            actionUtility.hardClick(TestManager.getDriver().findElement(By.xpath(String.format(headerOption,menu))));
            reportLogger.log(LogStatus.INFO,"successfully clicked on "+menu+" in the header");
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to click on "+menu+" menu");
            throw e;
        }
    }

    public void selectHeaderSubMenu(String menu,String... rhs){
        //selects menu specified from the sub header section
        try{
            if(rhs.length>0){
                HashMap<String,String> links = new HashMap<>();
                links.put("ONLINE CHECK-IN","nav-check-in");
                links.put("ARRIVALS & DEPARTURES","nav-arrivals-and-departures");
                links.put("SPECIAL ASSISTANCE","nav-special-assistance");
                links.put("FAQS & SUPPORT","nav-faqs");
                //links.put("","");
                actionUtility.hardClick(TestManager.getDriver().findElement(By.cssSelector(String.format(rhsHeaderSubOption,links.get(menu.toUpperCase())))));
            }else {
                actionUtility.hardClick(TestManager.getDriver().findElement(By.xpath(String.format(headerSubOption, menu))));
            }
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to click on "+menu+" menu");
            throw e;
        }
    }

    public void verifyRedirectionOfSubHeaders(String menu){
        //Verifies the redirection of links in sub header section under flights,manage booking and Help Header
        String expectedLink = null;
        selectHeaderSubMenu(menu);
        try{
            actionUtility.waitForElementNotPresent(fourTile,30);
            switch (menu){
                case ("Flight Destinations"):
                    expectedLink = "https://www.itflybe.com/cheap-flights";
                    break;

                case ("Route Map"):
                    expectedLink = "https://www.itflybe.com/route-map";
                    break;

                case ("Baggage"):
                    expectedLink = "https://www.itflybe.com/baggage";
                    break;

                case ("Fast Track Security"):
                    expectedLink = "https://www.itflybe.com/executive-lounges#fast-track";
                    break;

                case ("Flights to Isle of Man"):
                    expectedLink = "https://www.itflybe.com/cheap-flights/isle-of-man";
                    break;

                case ("Special assistance"):
                    expectedLink = "https://flybe.custhelp.com/app/answers/detail/a_id/1347";
                    break;

                case ("Flight timetable"):
                    expectedLink = "https://www.itflybe.com/timetable";
                    break;

                case ("Baggage policy"):
                    expectedLink = "https://flybe.custhelp.com/app/answers/list/c/421";
                    break;

                case ("Flight delays and disruptions"):
                    expectedLink = "https://flybe.custhelp.com/app/answers/detail/a_id/38";
                    break;

                case ("Change your flight"):
                    expectedLink = "https://www.itflybe.com/manage-booking";
                    break;

                case ("Add API information"):
                    expectedLink = "https://www.itflybe.com/manage-booking";
                    break;

                case ("Ticket changes"):
                    expectedLink = "https://www.itflybe.com/ticket-rules/ticket-changes";
                    break;

                case ("Refunds"):
                    expectedLink = "https://www.itflybe.com/ticket-rules/refunds";
                    break;

                case ("Amend car hire booking"):
                    expectedLink = "https://flybe.carsbookingengine.com/en_GB/my-bookings";
                    break;

            }
            actionUtility.waitForPageLoad(300);
            Assert.assertTrue(TestManager.getDriver().getCurrentUrl().equals(expectedLink));
            verifyPageIsLoaded(menu);
            reportLogger.log(LogStatus.INFO,"expected URL - "+expectedLink+" is displayed while clicking on the sub header - "+menu);
        }catch(AssertionError e) {
            reportLogger.log(LogStatus.INFO, "clicking on " +menu + " button does not display site - "+expectedLink);
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO, " unable to verify if clicking on " +menu + " button does not display site - "+expectedLink);
            throw e;
        }
    }

    public void verifyHotels_CarsSubMenuSection(){
        //verifies that 3 sub menu - hotel car hire and car parking with images are displayed
        String[] menu = {"Hotels","Car Hire","Car Parking"};
        String[] menuImage = {"Booking.com hotels and flights offer","Avis car hire","Budget car hire","Save with flybe car parking"};
        int i = 0;
        try{
            for(;i<3;i++) {
                Assert.assertTrue(hotelsCarsSubMenu.get(i).getText().contains(menu[i]));
            }
            int j=0;
            try{
                for(;j<3;j++) {
                    WebElement subMenuImage = TestManager.getDriver().findElement(By.cssSelector(String.format(hotelsCarsSubMenuImages,menuImage[i])));
                    Assert.assertTrue(subMenuImage.isDisplayed());
                }
            }catch(AssertionError e){
                reportLogger.log(LogStatus.INFO,"sub-menu image/icon for "+menu[j]+" is not present");
                throw e;
            }
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"sub-menu "+menu[i]+" is not present");
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify sub-menu hotels, car hire and car parking");
            throw e;
        }
    }

    private void verifyPageIsLoaded(String menu){
        //Verifies if specified page loads properly
        try{
            switch (menu) {
                case ("Hotels"):
                    actionUtility.verifyIfElementIsDisplayed(hotelSearch,10);
                    break;

                case ("Car Hire"):
                    actionUtility.verifyIfElementIsDisplayed(carHireText,10);
                    break;

                case ("Parking"):
                    actionUtility.verifyIfElementIsDisplayed(parkingText,10);
                    break;

                case ("Flight Destinations"):
                    actionUtility.verifyIfElementIsDisplayed(DestinationPage,10);
                    break;

                case ("Route Map"):
                    actionUtility.verifyIfElementIsDisplayed(routeMapText,10);
                    break;

                case ("Baggage"):
                    actionUtility.verifyIfElementIsDisplayed(baggagePage,10);
                    break;

                case ("Fast Track Security"):
                    actionUtility.verifyIfElementIsDisplayed(fastTrackPage,10);
                    break;

                case ("Flights to Isle of Man"):
                    actionUtility.verifyIfElementIsDisplayed(IsleOfManPage,10);
                    break;

                case ("Special assistance"):
                    actionUtility.verifyIfElementIsDisplayed(specialAssistanceText,10);
                    break;

                case ("Flight timetable"):
                    actionUtility.verifyIfElementIsDisplayed(timetableText,10);
                    break;

                case ("Baggage policy"):
                    actionUtility.verifyIfElementIsDisplayed(cabinAllowanceText,10);
                    break;

                case ("Change your flight"):
                    actionUtility.verifyIfElementIsDisplayed(mangeBookingPage,10);
                    break;

                case ("Add API information"):
                    actionUtility.verifyIfElementIsDisplayed(mangeBookingPage,10);
                    break;

                case ("Ticket changes"):
                    actionUtility.verifyIfElementIsDisplayed(ticketChangesPage,10);
                    break;

                case ("Refunds"):
                    actionUtility.verifyIfElementIsDisplayed(refundPage,10);
                    break;

                case ("Amend car hire booking"):
                    actionUtility.verifyIfElementIsDisplayed(amendCarHirePage,10);
                    break;

                case ("Flight delays and disruptions"):
                    actionUtility.verifyIfElementIsDisplayed(flightDelayPage,10);
                    break;
            }
            reportLogger.log(LogStatus.INFO,"content on the site are displayed properly when "+menu+" is clicked");

        }catch (AssertionError e) {
            reportLogger.log(LogStatus.INFO,  "clicking on the" +menu+ "is not displaying the " +menu+ "site properly");
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to check whether" +menu+ "is loaded properly");
            throw e;
        }
    }

    public void verifyClickingOnHotelsCarsMenuDisplaysAppropriateSite(){
        //verifies whether appropriate link is displayed
        String[] menu= {"Hotels","Car Hire","Parking"};
        String[] expectedLink = {"https://hotels.flybe.com/?aid=387470&label=large-menu-banner-fp","https://flybe.carsbookingengine.com/en_GB/search","https://parking.flybe.com/en/"};
        String[] expectedTitle = {"hotel and property listings worldwide","Flybe Car Hire","Book your parking"};
        int i=0;
        try{
            for(;i<3;i++){
                selectHeaderMenu(menu[i]);
                actionUtility.switchWindow(expectedTitle[i]);
                try {
                    Assert.assertTrue(TestManager.getDriver().getCurrentUrl().equals(expectedLink[i]));
                    reportLogger.log(LogStatus.INFO,"appropriate url"+expectedLink[i]+" is displayed when " +menu[i]+ " is clicked");
                    verifyPageIsLoaded(menu[i]);
                }catch(AssertionError e){
                    reportLogger.log(LogStatus.INFO,"clicking on "+menu[i]+" does not display expected site: "+expectedLink[i]);
                    throw e;
                }
                actionUtility.switchBackToMainWindow("Cheap Flights & Low Cost Flight Tickets");
            }
        }catch(Exception e){
            if(i<3) {
                reportLogger.log(LogStatus.INFO, "unable to verify if appropriate link is displayed when " + menu[i] + " is clicked");
            }else{
                reportLogger.log(LogStatus.INFO, "unable to verify if appropriate link is displayed");
            }
            throw e;
        }

    }

    public void verifyHotelCarLinks(){
        //verifies redirection of hotel,car and avios links
        try{
            String[] button = {"BOOK NOW","HIRE NOW","RESERVE NOW"};
            String[] url = {"https://hotels.flybe.com/","https://flybe.carsbookingengine.com/en_GB/search","https://parking.flybe.com/en/"};
            int i=0;
            for(;i<3;i++){
                System.out.println("env url"+TestManager.getDriver().getCurrentUrl());
                actionUtility.hardClick(TestManager.getDriver().findElement(By.xpath(String.format(hotelCarAviosLinks,button[i]))));
                actionUtility.waitForElementNotPresent(fourTile,30);
                System.out.println("current url"+TestManager.getDriver().getCurrentUrl());
                try{
                    Assert.assertTrue(TestManager.getDriver().getCurrentUrl().contains(url[i]));
                    TestManager.getDriver().navigate().back();
                    reportLogger.log(LogStatus.INFO,"successfully clicked on "+button[i]+" button to open site "+url[i]);
                }catch(AssertionError e){
                    reportLogger.log(LogStatus.INFO,"clicking on "+button[i]+" button does not display site "+url[i]);
                    throw e;
                }
            }
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify hotels,cars and avios links");
            throw e;
        }
    }

    public void enterMyBookingDetailsFromSearchWidget(){
        //retrieve the booking to view the changes
        try {
            navigateToHomepage();
            actionUtility.waitForElementVisible(myBooking,30);
            actionUtility.hardClick(myBooking);
            actionUtility.hardClick(manageBooking);
            reportLogger.log(LogStatus.INFO, "successfully navigated to manage booking retrieve from search widget");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to retrieve the booking through manage booking from search widget");
            throw e;
        }
    }

    public void enterManageBookingDetailsFromHeader(){
        //retrieve the booking to view the changes from header
        try {
            navigateToHomepage();
            selectHeaderMenu("Manage Booking");
            actionUtility.hardClick(manageBookingHeaderRHS);
            reportLogger.log(LogStatus.INFO, "successfully navigated to  manage booking retrieve from header");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to navigate to  manage booking retrieve from header");
            throw e;
        }
    }

    public void enterBookingDetailsForCheckIn(){
        // enter the PNR and passenger name in check-in tab
        try {
            navigateToHomepage();
            actionUtility.waitForElementVisible(myBooking,30);
            actionUtility.hardClick(myBooking);
            actionUtility.click(checkinButton);
            reportLogger.log(LogStatus.INFO, "successfully entered the booking details and continued to check-in");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to enter the booking details and continue to check-in");
            throw e;
        }
    }



}
