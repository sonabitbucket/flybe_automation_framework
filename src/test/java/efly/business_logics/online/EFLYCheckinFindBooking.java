package efly.business_logics.online;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.DataUtility;
import utilities.TestManager;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

public class EFLYCheckinFindBooking {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public EFLYCheckinFindBooking(){
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }
    //identification page
    @FindBy(id = "tab_accordion-group-PNR")
    private WebElement pnrOption;

    @FindBy(id = "tab_accordion-group-ETKT")
    private WebElement etktOption;

    @FindBy(xpath = "//div[@class='form-group field-LastName']//input")
    private WebElement lastName;

    @FindBy(xpath = "//div[@class='form-group field-PNR']//input")
    private WebElement pnr;

    @FindBy(xpath = "//div[@class='form-group field-ETKT']//input")
    private WebElement eTKT;

    @FindBy(xpath = "//form[@ssci-form='identification_ETKT']//div[@class='form-group field-LastName']//input")
    private WebElement etktLastName;

    @FindBy(css = "button[id*='buttonId']")
    private List<WebElement> continueToDangerousGoodsFromIdPage;

    String loadingIdentification2 = "//div[@class='dw-loading dw-loading-overlay loading-overlay']";

    String loadingIdentification1 = "//*[@id='initial-loading']";

    @FindBy(className = "content-block")
    private WebElement findBookingsection;

    //dangerous goods page
    @FindBy(linkText = "Find booking")
    private WebElement page;

    @FindBy(linkText = "Dangerous goods")
    private WebElement dangerousGoods;

    @FindBy(linkText = "Hazardous materials")
    private WebElement hazardousMaterials;

    @FindBy(linkText = "Prohibited items")
    private WebElement prohibitedItems;

    @FindBy(linkText = "Secondary prohibited items")
    private WebElement secondaryProhibitedItems;

    @FindBy(css = "label[for='AcknowledgeDangerousGoodsCheckBox']")
    private WebElement readAndUnderstoodCheckBox;

    @FindBy(className = "icon-warning-mini")
    private WebElement warningIcon;

    @FindBy(className = "ng-scope")
    private WebElement readAndUnderstoodMsg;

    @FindBy(xpath = "//button[text()='Continue']")
    private WebElement continueToJourneyDetailsButton;

    //passenger selection for checkin

    @FindBy(className = "pax-table")
    private WebElement passengerList;

    @FindBy(xpath = "//table[@class='pax-table']/tbody/tr/td[2]/span/span[2]/label/span[1]")
    private List<WebElement> selectPassenger;

    @FindBy(css = "button[title='Click to confirm the selection of the passengers.']")
    private WebElement continueToDangerousGoodsFromPassengerSelection;

    String paxToBeCheckedIn = "//tr[th[passenger-name[div[span[contains(text(),'%s')]]]]]//span[@class='checkbox-skin']";

    //change or cancel checkin

    @FindBy(xpath = "//span[text()='Cancel check-in']")
    private WebElement cancelCheckin;

    @FindBy(xpath = "//span[text()='Change check-in']")
    private WebElement changeCheckin;

    //footer links

    @FindBy(linkText = "terms of use")
    private WebElement termsOfUse;

    @FindBy(linkText = "privacy")
    private WebElement privacy;

    @FindBy(linkText = "conditions of carriage")
    private WebElement conditionsOfcarraige;

    @FindBy(css = "#footer-links ul li")
    private List<WebElement> footerLinks;

    //journey selection

    @FindBy(xpath = "//button[span[text()='Check in']]")
    private List<WebElement> journeysAvailableForCheckin;

    public void navigateToIdentificationPage(){

        try {
            if(TestManager.getTestEnvironment().equals("ARDWEB")){
                try {
                    String printHandlerLocation = ".\\pre_requisites\\printDialogHandler";
                    if(DataUtility.readConfig("printDialogHandler.location")!= null){
                        printHandlerLocation = DataUtility.readConfig("printDialogHandler.location");
                    }

                    actionUtility.hardSleep(3000);
                    if( TestManager.getDriver() instanceof FirefoxDriver) {

                        printHandlerLocation = printHandlerLocation+"\\leavePage.exe";


                        Process process = Runtime.getRuntime().exec(printHandlerLocation);
                        InputStream is = process.getInputStream();
                        int retCode = 0;
                        int count =1;
                        while(retCode != -1)
                        {

                            retCode = is.read();

                            count++;

                        }
                    }
                    else if( TestManager.getDriver() instanceof InternetExplorerDriver) {
                        printHandlerLocation = printHandlerLocation+"\\leavePage.exe";
                        Process process =Runtime.getRuntime().exec(printHandlerLocation);
                        InputStream is = process.getInputStream();
                        int retCode = 0;
                        while(retCode != -1)
                        {
                            retCode = is.read();
                        }
                    }
                    else if( TestManager.getDriver() instanceof ChromeDriver) {
                        actionUtility.hardSleep(2000);
                        printHandlerLocation = printHandlerLocation+"\\leavePage.exe";
                        Process process =Runtime.getRuntime().exec(printHandlerLocation);
                        InputStream is = process.getInputStream();
                        int retCode = 0;

                        while(retCode != -1)
                        {
                            retCode = is.read();
                        }


                    }
                    reportLogger.log(LogStatus.INFO, "successfully navigated from ARDWEB application after closing the leave page dialog");
                } catch (IOException e) {
                    reportLogger.log(LogStatus.WARNING, "unable to close the leave application dialog");
                    throw e;
                }
            }
            actionUtility.hardSleep(5000);
            TestManager.getDriver().navigate().to("https://pdt.checkin.amadeus.net/static/PDT/BE/#/identification");
            reportLogger.log(LogStatus.INFO,"successfully navigated to identification page");
        }catch(IOException e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to identification page");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to identification page");
            throw e;
        }
    }

    public void checkinThroughIdentificationPage(HashMap<String,String> passengerName, String pnrToBeEntered,String... etkt){
        //selects booking reference option and enters PNR and LastName
        try{
            String name = passengerName.get("adult1") ;
//            actionUtility.waitForElementNotPresent(loadingIdentification1,20);
            String lastNametoBeEntered[] = name.split(" ");
            if(etkt.length>0){
                if (actionUtility.verifyIfElementIsDisplayed(etktLastName)) {
                    actionUtility.sendKeys(etktLastName, lastNametoBeEntered[2]);
                    actionUtility.sendKeys(eTKT, etkt[0]);
                } else {
                    actionUtility.click(etktOption);
                    actionUtility.sendKeys(etktLastName, lastNametoBeEntered[2]);
                    actionUtility.sendKeys(eTKT, etkt[0]);
                }
                actionUtility.hardClick(continueToDangerousGoodsFromIdPage.get(1));
                reportLogger.log(LogStatus.INFO,"successfully entered ETKT No and lastname and successfully retrieved");
            }else {
                if (actionUtility.verifyIfElementIsDisplayed(lastName)) {
                    actionUtility.sendKeys(lastName, lastNametoBeEntered[2]);
                    actionUtility.sendKeys(pnr, pnrToBeEntered);
                } else {
                    actionUtility.hardClick(pnrOption);
                    actionUtility.sendKeys(lastName, lastNametoBeEntered[2]);
                    actionUtility.sendKeys(pnr, pnrToBeEntered);
                }
                actionUtility.hardClick(continueToDangerousGoodsFromIdPage.get(0));
                reportLogger.log(LogStatus.INFO,"successfully entered PNR and lastname and successfully retrieved");
            }

        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to retrieve PNR entered");
            throw e;
        }
    }

    public void verifyDangerousGoodsPageIsDisplayed(){
        //verifies that dangerous goods page with all section is displayed
        try{
            actionUtility.verifyIfElementIsDisplayed(page);
            actionUtility.verifyIfElementIsDisplayed(dangerousGoods);
            actionUtility.verifyIfElementIsDisplayed(hazardousMaterials);
            actionUtility.verifyIfElementIsDisplayed(prohibitedItems);
            actionUtility.verifyIfElementIsDisplayed(secondaryProhibitedItems);
            actionUtility.verifyIfElementIsDisplayed(readAndUnderstoodCheckBox);
            actionUtility.verifyIfElementIsDisplayed(warningIcon);
            actionUtility.verifyIfElementIsDisplayed(readAndUnderstoodMsg);
            /*try {
                Assert.assertFalse(actionUtility.verfiyIfAnElementISEnabled(continueToJourneyDetailsButton), "button to continue to journey details is enabled when restriceted goods checkbox is not checked");
            }catch(AssertionError err){
                reportLogger.log(LogStatus.INFO,"button to continue to journey details is enabled when restricted goods checkbox is not checked");
                throw err;
            }*/
            /*actionUtility.click(readAndUnderstoodCheckBox);
            try{
                Assert.assertTrue(actionUtility.verfiyIfAnElementISEnabled(continueToJourneyDetailsButton),"button to continue to journey details is disabled when checkbox is ticked");
            }catch(AssertionError err){
                reportLogger.log(LogStatus.INFO,"button to continue to journey details is enabled when restricted goods checkbox is checked");
                throw err;
            }*/
            reportLogger.log(LogStatus.INFO,"successfully verified that dangerous goods page is displayed");
        }
        catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify if dangerous goods page is displayed");
            throw e;
        }

    }

    public void continueToJourneyDetailsPage(int noOfPassengers,HashMap<String,String> testData){
        //clicks continue button after selecting passengers in case of multiple passengers and then clicks on read and understood checkbox and clicks on continue button to navigate to Journey Details Page
        //if only a particular pax is to be selected, then give that paxType+paxCount as value for paxTobeSelected
        try{
            if(actionUtility.verifyIfElementIsDisplayed(passengerList)) {
                if (testData.get("paxToBeSelectedForCheckin") != null) {
                    actionUtility.click(TestManager.getDriver().findElement(By.xpath(String.format(paxToBeCheckedIn, testData.get("paxToBeSelectedForCheckin")))));
                } else {
                    selectPassengerForCheckin(noOfPassengers, testData);
                }
                navigateAfterSelectingPassengers();
            }
            actionUtility.hardClick(readAndUnderstoodCheckBox);
            actionUtility.click(continueToJourneyDetailsButton);
            reportLogger.log(LogStatus.INFO,"able to navigate to Journey Details Page");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"navigated to Journey Details Page");
            throw e;
        }

    }

    public void navigateAfterSelectingPassengers(){
        try{
            actionUtility.click(continueToDangerousGoodsFromPassengerSelection);
            reportLogger.log(LogStatus.INFO,"navigated to dangerous goods page");
        }catch(Exception e){

        }
    }

    public void selectPassengerForCheckin(int noOfPassengers,HashMap<String,String> testData){
        //in case of multiple passengers in a booking, this method selects all the passenger for checkin
        try{
            int totalPassengersToBeSelected = noOfPassengers;
            if(testData.get("noOfInfants")!=null && Integer.parseInt(testData.get("noOfInfants"))>0){
                totalPassengersToBeSelected = noOfPassengers-Integer.parseInt(testData.get("noOfInfants"));
            }

            for(int i=0;i<totalPassengersToBeSelected; i++){
                actionUtility.click(selectPassenger.get(i));
            }
            reportLogger.log(LogStatus.INFO,"selected all passengers for checkin");

        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select passengers for checkin");
            throw e;
        }
    }

    public void selectJourneyForCheckin(String outbound,String inbound){
        try{
            if(outbound.equalsIgnoreCase("yes")){
                actionUtility.click(journeysAvailableForCheckin.get(0));
                reportLogger.log(LogStatus.INFO,"selected outbound journey for selection");
            }

            if(inbound.equalsIgnoreCase("yes")){
                actionUtility.click(journeysAvailableForCheckin.get(1));
                reportLogger.log(LogStatus.INFO,"selected inbound journey for selection");
            }
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select journey for checkin");
            throw e;
        }
    }

    public void cancelCheckin(){
        //cancels checkin
        try{
            actionUtility.click(cancelCheckin);
            reportLogger.log(LogStatus.INFO,"successfully clicked on cancel checkin button");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to cancel checkin");
            throw e;
        }
    }

    public void changeCheckin(){
        //change checkin
        try{
            actionUtility.click(changeCheckin);
            reportLogger.log(LogStatus.INFO,"successfully clicked on change checkin button");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to change checkin");
            throw e;
        }
    }

    public void verifyFooterLinksOpensCorrectURL(){
        try{
            actionUtility.waitForElementNotPresent(loadingIdentification1,20);
            actionUtility.click(termsOfUse);
            actionUtility.switchWindow("Website terms of use | Flybe | Cheap flights &amp; budget flights");
            Assert.assertTrue(TestManager.getDriver().getCurrentUrl().equals("https://www.flybe.com/website-terms-of-use/"));
            actionUtility.switchBackToMainWindow("Flybe check-in, Find booking");

            actionUtility.click(privacy);
            actionUtility.switchWindow("Privacy policy | Flybe | Cheap flights &amp; budget flights");
            Assert.assertTrue(TestManager.getDriver().getCurrentUrl().equals("https://www.flybe.com/privacy-policy/"));
            actionUtility.switchBackToMainWindow("Flybe check-in, Find booking");

            actionUtility.click(termsOfUse);
            actionUtility.switchWindow("Website terms of use | Flybe | Cheap flights &amp; budget flights");
            Assert.assertTrue(TestManager.getDriver().getCurrentUrl().equals("https://www.flybe.com/website-terms-of-use/"));
            actionUtility.switchBackToMainWindow("Flybe check-in, Find booking");

            reportLogger.log(LogStatus.INFO,"successfully verified the footer links");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify footer links");
            throw e;
        }
    }

}
