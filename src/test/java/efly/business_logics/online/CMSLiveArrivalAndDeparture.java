package efly.business_logics.online;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.TestManager;

import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Sona on 4/26/2019.
 */
public class CMSLiveArrivalAndDeparture {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public CMSLiveArrivalAndDeparture() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(className = "by-flight")
    private WebElement searchByFlightNo;

    @FindBy(id = "flight-number")
    private WebElement flightNumber;

    @FindBy(id = "human-date")
    private WebElement travelDate;

    String calendar = "//table[@class='ui-datepicker-calendar']";

    @FindBy(xpath = "//table//tr/td[span]")
    private List<WebElement> disabledDates;

    String availableDates = "//table//tr/td[a]";

    @FindBy(xpath = "//table//tr/td[contains(@class,' ui-datepicker-other-month ui-datepicker-unselectable ui-state-disabled')]")
    private List<WebElement> otherMonthDates;

    @FindBy(xpath = "//span[text()='Prev']")
    private WebElement prevCalendar;

    @FindBy(xpath = "//span[text()='Next']")
    private WebElement nextCalendar;

    @FindBy(id = "flight-search-button" )
    private WebElement findFlights;

    @FindBy(tagName = "b")
    private List<WebElement> flightList;

    @FindBy(xpath = "//div[input[@placeholder='Enter a departure airport']]")
    private WebElement flightFromDropDown;

    @FindBy(xpath = "//div[input[@placeholder='Enter an arrival airport']]")
    private WebElement flightToDropDown;

    @FindBy(css = "div[class='chosen-search'] input")
    private List<WebElement> flight;

    @FindBy(xpath = "//button[text()='CHECK FLIGHT STATUS']")
    private WebElement checkFlightStatus;

    //LAD table
    private String ladHeader = "table[class='flight-table'] th[class='%s']";

    @FindBy(css = "table[class='flight-table']")
    private WebElement flightInfoTable;

    @FindBy(xpath = "//table[@class='flight-table']/tbody/tr")
    private List<WebElement> eachFlightDetail;

    public void searchByFlightNumber(){
        try{
            actionUtility.hardClick(searchByFlightNo);
            reportLogger.log(LogStatus.INFO,"successfully selected 'search by flight number' option");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select option 'search by flight number'");
            throw e;
        }
    }

    public void enterFlightNumberAndVerifyLAD(HashMap<String,String> details){
        //enters flight no. retrieved from fare select for date specified
        //verifies details displayed for flight no. entered
        int i = 1;
        try{
            int noOfFlights = Integer.parseInt(details.get("noOfDirectFlights"));
            for(;i<=noOfFlights;i++) {
                actionUtility.sendKeys(flightNumber, details.get("flight"+i+"Number").substring(2));
                actionUtility.click(checkFlightStatus);
                verifyFlightDetails(details,i,0);
            }
            verifyHeader();
            reportLogger.log(LogStatus.INFO,"successfully submitted flight number "+details.get("flight"+(i-1)+"Number").substring(2));
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to enter flight number "+details.get("flight"+(i-1)+"Number").substring(2));
            throw e;
        }
    }

    public void selectdate(HashMap<String,String> testData){
        //selects travel date
        try {
            actionUtility.click(travelDate);
            List<WebElement> enabledDays = verifyThatTodaysDatePlusMinusTwoAreEnabled();
            HashMap<String,WebElement> dates = getEnabledDates(enabledDays);
            actionUtility.click(dates.get(testData.get("departDateOffset")));
            reportLogger.log(LogStatus.INFO,"successfully selected travel date");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select travel date");
            throw e;
        }
    }

    private HashMap getEnabledDates(List<WebElement> enabledDates){
        HashMap<String,WebElement> dates = new HashMap<>();
        dates.put("-2",enabledDates.get(0));
        dates.put("-1",enabledDates.get(1));
        dates.put("0",enabledDates.get(2));
        dates.put("1",enabledDates.get(3));
        dates.put("2",enabledDates.get(4));
        return dates;
    }

    private List<WebElement> verifyThatTodaysDatePlusMinusTwoAreEnabled(){
        //verifies that today's date, today's date +2 days and today's date -2 days are enabled for selection
        List<WebElement> enabledDates = Collections.EMPTY_LIST;
        try{
            if(TestManager.getDriver().findElements(By.xpath(availableDates)).size()<5) {
                if(prevCalendar.isEnabled()) {
                    actionUtility.hardClick(prevCalendar);
                    enabledDates = TestManager.getDriver().findElements(By.xpath(availableDates));
                    actionUtility.click(nextCalendar);
                }
                else if(nextCalendar.isEnabled()) {
                    enabledDates = TestManager.getDriver().findElements(By.xpath(availableDates));
                    actionUtility.click(nextCalendar);
                }
                List<WebElement> remainingEnabledDates = TestManager.getDriver().findElements(By.xpath(availableDates));
                for (int i = 0; i < remainingEnabledDates.size(); i++) {
                    enabledDates.add(enabledDates.size(), remainingEnabledDates.get(i));
                }
            }
            else{
                enabledDates = TestManager.getDriver().findElements(By.xpath(availableDates));
            }
            Assert.assertEquals(5,enabledDates.size());
            reportLogger.log(LogStatus.INFO,"today's date, today's date +2 days and today's date -2 days are only enabled as expected on travel date calendar on live arrival and departure airport");
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"other dates (other than today's date, today's date +2 days and today's date -2 days) are also enabled");
            throw e;
        }
        return enabledDates;
    }

    public void enterSourceAndDestinationAndSubmit(HashMap<String,String> testData){
        //enter source and destination
        try{
            actionUtility.click(flightList.get(0));
            //actionUtility.waitForElementVisible(flightFromDropDown,30);
            //actionUtility.click(flightFromDropDown);
            //actionUtility.sendKeysByAction(flightFromDropDown, testData.get("flightFrom")+ Keys.RETURN);
            actionUtility.sendKeysByAction(flightList.get(0), testData.get("flightFrom")+ Keys.RETURN);
            actionUtility.click(flightList.get(1));
            //actionUtility.click(flightToDropDown);
            //actionUtility.hardSleep(2000);
            actionUtility.sendKeysByAction(flightList.get(1), testData.get("flightTo")+ Keys.RETURN);
            actionUtility.click(checkFlightStatus);
            reportLogger.log(LogStatus.INFO,"successfully entered the source - "+testData.get("flightFrom")+" and destination - "+testData.get("flightTo"));
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to enter source and destination of the trip");
            throw e;
        }
    }

    public void verifyLAD(HashMap<String,String> details){
        //verifies the LAD result displayed for search by airport option
        try{
            verifyHeader();
                int itr = Integer.parseInt(details.get("noOfDirectFlights"));
                for(int rowNo = 0,detailNo = 1;rowNo<itr;rowNo++,detailNo++) {
                    verifyFlightDetails(details,detailNo,rowNo);
                }
            reportLogger.log(LogStatus.INFO,"successfully verified live arrival and departure details displayed");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify live arrival and departure details displayed");
            throw e;
        }
    }

    private void verifyFlightDetails(HashMap<String,String> details,int detailsNo,int rowNo){
        //verifies each cell in the LAD table with flight details retrieved from fare select page
        try{
            int cellno = 0;
            if(eachFlightDetail.get(rowNo).findElements(By.tagName("td")).get(1).getText() == null) {
                cellno = 1;
            }
            System.out.println("cellno - "+cellno);
            System.out.println(eachFlightDetail.get(rowNo).findElements(By.tagName("td")).get(cellno).getText() + "*" + details.get("flight" + detailsNo + "Number"));
            Assert.assertTrue(eachFlightDetail.get(rowNo).findElements(By.tagName("td")).get(cellno).getText().equals(details.get("flight" + detailsNo + "Number")));
            System.out.println(eachFlightDetail.get(rowNo).findElements(By.tagName("td")).get(cellno+2).getText()+"**"+details.get("departure"));
            Assert.assertTrue(eachFlightDetail.get(rowNo).findElements(By.tagName("td")).get(cellno+2).getText().contains(details.get("departure")));
            System.out.println(eachFlightDetail.get(rowNo).findElements(By.tagName("td")).get(cellno+3).getText()+"***"+details.get("flight"+detailsNo+"DepartureTime"));
            Assert.assertTrue(eachFlightDetail.get(rowNo).findElements(By.tagName("td")).get(cellno+3).getText().equals(details.get("flight"+detailsNo+"DepartureTime")));
            System.out.println(eachFlightDetail.get(rowNo).findElements(By.tagName("td")).get(cellno+4).getText()+"****"+details.get("destination"));
            Assert.assertTrue(eachFlightDetail.get(rowNo).findElements(By.tagName("td")).get(cellno+4).getText().contains(details.get("destination")));
            System.out.println(eachFlightDetail.get(rowNo).findElements(By.tagName("td")).get(cellno+5).getText()+"*****"+details.get("flight"+detailsNo+"ArrivalTime"));
            Assert.assertTrue(eachFlightDetail.get(rowNo).findElements(By.tagName("td")).get(cellno+5).getText().equals(details.get("flight"+detailsNo+"ArrivalTime")));
            if(details.get("flight"+detailsNo+"Operator")==null) {
                Assert.assertTrue(eachFlightDetail.get(rowNo).findElements(By.tagName("td")).get(cellno+7).getText().equals("On time"));
            }
            else{
                System.out.println(eachFlightDetail.get(rowNo).findElements(By.tagName("td")).get(cellno+8).getText());
                System.out.println("Please visit the"+details.get("flight"+detailsNo+"Operator").split("operated by")[1]+"\n"+"website for details");
                Assert.assertTrue(eachFlightDetail.get(rowNo).findElements(By.tagName("td")).get(cellno+8).getText().equals("Please visit the"+details.get("flight"+detailsNo+"Operator").split("operated by")[1]+"\nwebsite for details"));
            }
            reportLogger.log(LogStatus.INFO,"flight details are correctly displayed in LAD table for flight no."+details.get("flight"+detailsNo+"Number"));
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"flight details are not correct for flight no. "+details.get("flight"+detailsNo+"Number")+" in LAD table");
            throw e;
        }
    }

    private void verifyHeader(){
        //verifies header in LAD table
        String[] headerClass = {"flight-number-table","dep-airport","sched-dep","arrv-airport","sched-arrv","status","notes"};
        String[] expectedHeading = {"Flight"+"\n"+"number","Departure airport","Scheduled"+"\n"+"departure","Arrival airport","Scheduled"+"\n"+"arrival","Status","Notes"};
        for(int i=0;i<7;i++){
            try{
                actionUtility.waitForElementVisible(flightInfoTable,60);
                Assert.assertTrue(TestManager.getDriver().findElement(By.cssSelector(String.format(ladHeader,headerClass[i]))).getText().contains(expectedHeading[i]));
            }catch(AssertionError e){
                reportLogger.log(LogStatus.INFO,"displayed header is not "+expectedHeading[i]);
                throw e;
            }
        }

    }
}
