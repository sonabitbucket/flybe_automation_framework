package efly.business_logics.online;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.CommonUtility;
import utilities.TestManager;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Indhu on 7/2/2019.
 */
public class CMSCheckInRetrievePNR {
    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public CMSCheckInRetrievePNR(){
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(id = "booking-reference")
    private WebElement bookingReference;

    @FindBy(id = "surname")
    private WebElement lastName;

    @FindBy(id = "flybe_checkin_btn")
    private WebElement checkin;

    @FindBy(css = "div[class='booking-search-errors alert start-hidden alert-warning']")
    private WebElement nonOnlineCheckinMessage;

    //trip modal
    @FindBy(xpath = "//div[@class='search-widget-trip-select-flight-info'][1]/div[contains(@class,'float-left')]/p")
    private WebElement outbound_departure_time;

    @FindBy(xpath = "//div[@class='search-widget-trip-select-flight-info'][2]/div[contains(@class,'float-left')]/p")
    private WebElement inbound_departure_time;

    @FindBy(xpath = "//div[@class='search-widget-trip-select-flight-info'][1]/div[contains(@class,'float-right')]/p")
    private WebElement outbound_flight_number;

    @FindBy(xpath = "//div[@class='search-widget-trip-select-flight-info'][2]/div[contains(@class,'float-right')]/p")
    private WebElement inbound_flight__number;

    @FindBy(xpath = "//div[@class='search-widget-trip-select-flight-info'][1]/div[contains(@class,'date')]/p")
    private WebElement outbound_departure_date;

    @FindBy(xpath = "//div[@class='search-widget-trip-select-flight-info'][2]/div[contains(@class,'date')]/p")
    private WebElement inbound_departure_date;

    @FindBy(css = "p[class*='airport-label']")
    private  List<WebElement> airport_name;

    @FindBy(css = "span[class*='airportCode']")
    private List<WebElement> airportCode;

    @FindBy(css = "div[class*='airports-icon']")
    private List<WebElement> selectFlight;

    public void retrievePNR(String PNR, String reservationName){
        try{
            bookingReference.sendKeys(PNR);
            lastName.sendKeys(reservationName);
            actionUtility.hardClick(checkin);
            actionUtility.waitForPageLoad(50);
            reportLogger.log(LogStatus.INFO,"successfully navigated to checkin page");
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to checkin page");
            throw e;

        }
    }

    public void verifyTripModal(HashMap<String,String> flightDetails,String flightToBeSelectedForCheckin){

        try{
            actionUtility.waitForElementVisible(selectFlight,60);
            verifyDepartureTiming(flightDetails);
            verifyDepartureDate(flightDetails);
            verifyFlightNumber(flightDetails);
            verifyAirport(flightDetails);
            if(flightToBeSelectedForCheckin.toUpperCase().equals("OUTBOUND")){
                selectFlight.get(0).click();
            }
            else{
                selectFlight.get(1).click();
            }
            reportLogger.log(LogStatus.INFO,"successfully verified checkin trip modal");
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify if trip modal");
            throw e;
        }
    }

    private void verifyAirport(HashMap<String,String> flightDetails) {
        verifyingAirport(flightDetails,"outbound");
        verifyingAirport(flightDetails,"inbound");
    }

    private void verifyingAirport(HashMap<String,String> flightDetails,String bound) {
        String item = null, expected = null, actual = null;
        int start = 0, noOfTimes = 2;
        try {
            if(bound.equals("inbound")) {
                start = 2; noOfTimes = 4;
            }
            do {
                item = bound+" source airport";
                actual = airport_name.get(start).getText();
                expected = flightDetails.get(bound + "Source").split(" ")[0];
                Assert.assertTrue(actual.contains(expected), expected + " -- " + actual);

                actual = airportCode.get(start++).getText();
                expected = flightDetails.get(bound + "Source").split("\\(")[1].split("\\)")[0];
                Assert.assertTrue(actual.equals(expected), expected + " -- " + actual);

                item = bound+" destination airport";
                actual = airport_name.get(start).getText();
                expected = flightDetails.get(bound + "Destination").split(" ")[0];
                Assert.assertTrue(actual.contains(expected), expected + " -- " + actual);

                actual = airportCode.get(start++).getText();
                expected = flightDetails.get(bound + "Destination").split("\\(")[1].split("\\)")[0];
                Assert.assertTrue(actual.equals(expected), expected + " -- " + actual);

            }while(start<noOfTimes);
        } catch (AssertionError e) {
            reportLogger.log(LogStatus.INFO, item + " on trip modal :- " + " expected is " + expected + " but displayed is " + actual);
            throw e;
        }
    }

    private void verifyFlightNumber(HashMap<String,String> flightDetails){
        String item =null,expected=null,actual=null;
        try{
            item = "outbound flight number";
            actual = outbound_flight_number.getText();
            Assert.assertTrue(actual.contains("FLIGHT"),actual);

            actual = outbound_flight_number.getAttribute("innerHTML").split("<br>")[1];
            expected = flightDetails.get("outboundFlightNo1").split("\\(")[1].split("\\)")[0].replace(" ","");
            Assert.assertTrue(actual.contains(expected),expected+" -- "+actual);

            item = "inbound flight number";
            actual = inbound_flight__number.getText();
            Assert.assertTrue(actual.contains("FLIGHT"),actual);

            actual = inbound_flight__number.getAttribute("innerHTML").split("<br>")[1];
            expected = flightDetails.get("inboundFlightNo1").split("\\(")[1].split("\\)")[0].replace(" ","");
            Assert.assertTrue(actual.contains(expected),expected+" -- "+actual);

        }catch (AssertionError e){
            reportLogger.log(LogStatus.INFO,item+" on trip modal :- "+" expected is "+expected+" but displayed is "+actual);
            throw e;
        }
    }

    private void verifyDepartureDate(HashMap<String,String> flightDetails){
        String item =null,expected=null,actual=null;
        try{
            item = "outbound departure date";
            actual = outbound_departure_date.getText();
            Assert.assertTrue(actual.contains("DATE"),actual);

            actual = outbound_departure_date.getAttribute("innerHTML").split("<br>")[1];
            expected = CommonUtility.convertDateFormat("EEEE dd MMM yyyy","yyyy-MM-dd",flightDetails.get("outboundDate"));
            Assert.assertTrue(actual.contains(expected),expected+" -- "+actual);

            item = "inbound departure date";
            actual = inbound_departure_date.getText();
            Assert.assertTrue(actual.contains("DATE"),actual);

            actual = inbound_departure_date.getAttribute("innerHTML").split("<br>")[1];
            expected = CommonUtility.convertDateFormat("EEEE dd MMM yyyy","yyyy-MM-dd",flightDetails.get("inboundDate"));
            Assert.assertTrue(actual.contains(expected),expected+" -- "+actual);

        }catch (AssertionError e){
            reportLogger.log(LogStatus.INFO,item+" on trip modal :- "+" expected is "+expected+" but displayed is "+actual);
            throw e;
        }
    }

    private void verifyDepartureTiming(HashMap<String,String> flightDetails){
        String item =null,expected=null,actual=null;
        try{
            item = "outbound departure time";
            actual = outbound_departure_time.getText();
            Assert.assertTrue(actual.contains("DEPARTURE"),actual);

            actual = outbound_departure_time.getAttribute("innerHTML").split("<br>")[1];
            expected = flightDetails.get("outboundSector1TakeOffTime");
            Assert.assertTrue(actual.contains(expected),expected+" -- "+actual);

            item = "inbound departure time";
            actual = inbound_departure_time.getText();
            Assert.assertTrue(actual.contains("DEPARTURE"),actual);

            actual = inbound_departure_time.getAttribute("innerHTML").split("<br>")[1];
            expected = flightDetails.get("inboundSector1TakeOffTime");
            Assert.assertTrue(actual.contains(expected),expected+" -- "+actual);

        }catch (AssertionError e){
            reportLogger.log(LogStatus.INFO,item+" on trip modal :- "+" expected is "+expected+" but displayed is "+actual);
            throw e;
        }
    }

    public void verifyNonOnlineCheckInMessage(){
        String expectedMessage= null;
        String actualMessage= null;
        try {
            expectedMessage = "Sorry, your departure airport does not support online check-in; please see full list of check-in options available by airport ";
            actualMessage = nonOnlineCheckinMessage.getText();
            actionUtility.waitForElementVisible(nonOnlineCheckinMessage, 30);
            Assert.assertEquals(actualMessage.trim(), expectedMessage.trim(), expectedMessage.trim()+"\n"+actualMessage.trim());
            reportLogger.log(LogStatus.INFO,"right message is displayed for an non online check-in airport");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.INFO,"Wrong message displayed for non online checkin airport"+"Expected-" +expectedMessage+ "Actual-" +actualMessage);
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"Unable to verify the message displayed");
            throw e;

        }
    }

    public void verifyUserIsRedirectedToAirFranceSite(){
        String expectedURL= null;
        String actualURL = null;
        try {
            expectedURL = "https://wwws.airfrance.co.uk/check-in";
            actionUtility.hardSleep(20000);
            actualURL = TestManager.getDriver().getCurrentUrl();
            Assert.assertTrue(actualURL.contains(expectedURL),expectedURL+"--"+actualURL);
            reportLogger.log(LogStatus.INFO, "user is redirected to Air France site successfully");
        }catch (AssertionError e){
            reportLogger.log(LogStatus.INFO, "user is not redirected to Air France site"+ "Expected-" +expectedURL+ "Actual-" +actualURL);
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO, "unable to check whether user is redirected to AF site");
            throw e;

        }
    }


}
