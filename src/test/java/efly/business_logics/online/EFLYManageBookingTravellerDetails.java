package efly.business_logics.online;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.TestManager;

import java.security.PrivateKey;
import java.util.HashMap;
import java.util.List;

import static utilities.ActionUtility.SelectionType.SELECTBYTEXT;
import static utilities.ActionUtility.SelectionType.SELECTBYVALUE;

/**
 * Created by Indhu on 6/13/2019.
 */
public class EFLYManageBookingTravellerDetails {

        ActionUtility actionUtility;
        ExtentTest reportLogger;

        public EFLYManageBookingTravellerDetails(){
            PageFactory.initElements(TestManager.getDriver(), this);
            this.actionUtility = new ActionUtility();
            this.reportLogger = TestManager.getReportLogger();
        }
    @FindBy(xpath = "//span[contains(text(),'Manage booking')]")
    private WebElement manageBookingButton;

    @FindBy(xpath = "//section[contains(@id,'travellerFormContent')]")
    private WebElement travellerSection;

    //API details

    @FindBy(css = "select[id*='ADT-PSPT'][name='IDEN_Nationality']")
    private List<WebElement> nationalityADT;

    @FindBy (css = "select[id*='ADT-PSPT'][name='IDEN_Gender']")
    private List<WebElement> genderADT;

    @FindBy(xpath = "//input[contains(@id,'ADT-PSPT') and not(contains(@id,'DocumentExpiryDate'))][@name='DateDay']")
    private List<WebElement> DOBDayADT;

    @FindBy(xpath="//select[contains(@id,'ADT-PSPT') and not(contains(@id,'DocumentExpiryDate'))][@name='DateMonth']")
    private List<WebElement> DOBMonthADT;

    @FindBy(xpath="//input[contains(@id,'ADT-PSPT') and not(contains(@id,'DocumentExpiryDate'))][@name='DateYear']")
    private List<WebElement> DOBYearADT;

    @FindBy(css = "select[id*='ADT-PSPT'][name='PSPT_DocumentType']")
    private List<WebElement> identityDocADT;

    @FindBy(css ="input[id*='ADT-PSPT'][name='PSPT_DocumentNumber']")
    private List<WebElement> documentNumberADT;

    @FindBy(xpath = "//input[contains(@id,'ADT-PSPT') and not(contains(@id,'DateOfBirth'))][@name='DateDay']")
    private List<WebElement> dayExpiryADT;

    @FindBy(xpath = "//select[contains(@id,'ADT-PSPT') and not(contains(@id,'DateOfBirth'))][@name='DateMonth']")
    private List<WebElement> monthExpiryADT;

    @FindBy(xpath = "//input[contains(@id,'ADT-PSPT') and not(contains(@id,'DateOfBirth'))][@name='DateYear']")
    private List<WebElement> yearExpiryADT;

    @FindBy(css = "select[id*='ADT-PSPT'][name='PSPT_DocumentIssuingCountry']")
    private List<WebElement> countryIssueADT;

    @FindBy(css = "select[id*='INF-PSPT'][name='IDEN_Nationality']")
    private List<WebElement> nationalityINF;

    @FindBy (css = "select[id*='INF-PSPT'][name='IDEN_Gender']")
    private List<WebElement> genderINF;

    @FindBy(xpath = "//input[contains(@id,'INF-PSPT') and not(contains(@id,'DocumentExpiryDate'))][@name='DateDay']")
    private List<WebElement> DOBDayINF;

    @FindBy(xpath="//select[contains(@id,'INF-PSPT') and not(contains(@id,'DocumentExpiryDate'))][@name='DateMonth']")
    private List<WebElement> DOBMonthINF;

    @FindBy(xpath="//input[contains(@id,'INF-PSPT') and not(contains(@id,'DocumentExpiryDate'))][@name='DateYear']")
    private List<WebElement> DOBYearINF;

    @FindBy(css = "select[id*='INF-PSPT'][name='PSPT_DocumentType']")
    private List<WebElement> identityDocINF;

    @FindBy(css ="input[id*='INF-PSPT'][name='PSPT_DocumentNumber']")
    private List<WebElement> documentNumberINF;

    @FindBy(xpath = "//input[contains(@id,'INF-PSPT') and not(contains(@id,'DateOfBirth'))][@name='DateDay']")
    private List<WebElement> dayExpiryINF;

    @FindBy(xpath = "//select[contains(@id,'INF-PSPT') and not(contains(@id,'DateOfBirth'))][@name='DateMonth']")
    private List<WebElement> monthExpiryINF;

    @FindBy(xpath = "//input[contains(@id,'INF-PSPT') and not(contains(@id,'DateOfBirth'))][@name='DateYear']")
    private List<WebElement> yearExpiryINF;

    @FindBy(css = "select[id*='INF-PSPT'][name='PSPT_DocumentIssuingCountry']")
    private List<WebElement> countryIssueINF;

    @FindBy(css = "select[id*='CHD-PSPT'][name='IDEN_Nationality']")
    private List<WebElement> nationalityCHD;

    @FindBy (css = "select[id*='CHD-PSPT'][name='IDEN_Gender']")
    private List<WebElement> genderCHD;

    @FindBy(xpath = "//input[contains(@id,'CHD-PSPT') and not(contains(@id,'DocumentExpiryDate'))][@name='DateDay']")
    private List<WebElement> DOBDayCHD;

    @FindBy(xpath="//select[contains(@id,'CHD-PSPT') and not(contains(@id,'DocumentExpiryDate'))][@name='DateMonth']")
    private List<WebElement> DOBMonthCHD;

    @FindBy(xpath="//input[contains(@id,'CHD-PSPT') and not(contains(@id,'DocumentExpiryDate'))][@name='DateYear']")
    private List<WebElement> DOBYearCHD;

    @FindBy(css = "select[id*='CHD-PSPT'][name='PSPT_DocumentType']")
    private List<WebElement> identityDocCHD;

    @FindBy(css ="input[id*='CHD-PSPT'][name='PSPT_DocumentNumber']")
    private List<WebElement> documentNumberCHD;

    @FindBy(xpath = "//input[contains(@id,'CHD-PSPT') and not(contains(@id,'DateOfBirth'))][@name='DateDay']")
    private List<WebElement> dayExpiryCHD;

    @FindBy(xpath = "//select[contains(@id,'CHD-PSPT') and not(contains(@id,'DateOfBirth'))][@name='DateMonth']")
    private List<WebElement> monthExpiryCHD;

    @FindBy(xpath = "//input[contains(@id,'CHD-PSPT') and not(contains(@id,'DateOfBirth'))][@name='DateYear']")
    private List<WebElement> yearExpiryCHD;

    @FindBy(css = "select[id*='CHD-PSPT'][name='PSPT_DocumentIssuingCountry']")
    private List<WebElement> countryIssueCHD;

    @FindBy(css = "select[id*='YTH-PSPT'][name='IDEN_Nationality']")
    private List<WebElement> nationalityYTH;

    @FindBy (css = "select[id*='YTH-PSPT'][name='IDEN_Gender']")
    private List<WebElement> genderYTH;

    @FindBy(xpath = "//input[contains(@id,'YTH-PSPT') and not(contains(@id,'DocumentExpiryDate'))][@name='DateDay']")
    private List<WebElement> DOBDayYTH;

    @FindBy(xpath="//select[contains(@id,'YTH-PSPT') and not(contains(@id,'DocumentExpiryDate'))][@name='DateMonth']")
    private List<WebElement> DOBMonthYTH;

    @FindBy(xpath="//input[contains(@id,'YTH-PSPT') and not(contains(@id,'DocumentExpiryDate'))][@name='DateYear']")
    private List<WebElement> DOBYearYTH;

    @FindBy(css = "select[id*='YTH-PSPT'][name='PSPT_DocumentType']")
    private List<WebElement> identityDocYTH;

    @FindBy(css ="input[id*='YTH-PSPT'][name='PSPT_DocumentNumber']")
    private List<WebElement> documentNumberYTH;

    @FindBy(xpath = "//input[contains(@id,'YTH-PSPT') and not(contains(@id,'DateOfBirth'))][@name='DateDay']")
    private List<WebElement> dayExpiryYTH;

    @FindBy(xpath = "//select[contains(@id,'YTH-PSPT') and not(contains(@id,'DateOfBirth'))][@name='DateMonth']")
    private List<WebElement> monthExpiryYTH;

    @FindBy(xpath = "//input[contains(@id,'YTH-PSPT') and not(contains(@id,'DateOfBirth'))][@name='DateYear']")
    private List<WebElement> yearExpiryYTH;

    @FindBy(css = "select[id*='YTH-PSPT'][name='PSPT_DocumentIssuingCountry']")
    private List<WebElement> countryIssueYTH;

    @FindBy (xpath = "//span[@class='plnext-widget-btn-text one-icon']")
    private WebElement continueButton;

    @FindBy (xpath = "//input[contains(@id='contactInformation-PhoneMobile')]")
    private WebElement phoneNumber;

    @FindBy(name = "Email")
    private WebElement bookerEmail;

    @FindBy(name = "EmailConfirm")
    private WebElement bookerEmailConfirmation;

    @FindBy(css = "input[id*='FREQ_Number']")
    private WebElement frequentFlyerNumber;

    //traveller details

    String traveller_name = "div[class*='%s'] div[class*='traveller-name']";

    String traveller_etkt = "div[class*='%s'] div[class='ticket-container'] span";


    public HashMap<String,String> verifyTravellerDetailsAndRetrieveETKT(HashMap<String,String> testData){
        HashMap<String,String> e_tkt = new HashMap<>();
        String expected=null,actual=null,item=null;
        try {
            if (Integer.parseInt(testData.get("noOfAdult")) > 0) {
                int noOfAdult = Integer.parseInt(testData.get("noOfAdult"));
                for (int i=0;i<noOfAdult; i++) {
                    item = "adult"+(i+1)+" name";
                    expected = testData.get("adult" + (i + 1) + "Name").split(" ")[1]+" "+ testData.get("adult" + (i + 1) + "Name").split(" ")[2];
                    actual = TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_name, "ADT"))).get(i).getText();;
                    Assert.assertEquals(actual,expected);
                    e_tkt.put("adult"+(i+1)+"etkt",TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_etkt, "ADT"))).get(i).getText().replace("-",""));
                    if(i==0 && testData.get("frequentFlyerNumber")!=null){
                        Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(frequentFlyerNumber));
                        Assert.assertTrue(frequentFlyerNumber.getText().contains(testData.get("frequentFlyerNumber")));
                    }
                }
            }
            if (testData.get("noOfTeen")!=null) {
                int noOfTeen = Integer.parseInt(testData.get("noOfTeen"));
                for (int i=0;i<noOfTeen; i++) {
                    item = "teen"+(i+1)+" name";
                    expected = testData.get("teen" + (i + 1) + "Name").split(" ")[1]+" "+ testData.get("teen" + (i + 1) + "Name").split(" ")[2];
                    actual = TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_name, "YTH"))).get(i).getText();;
                    Assert.assertEquals(actual,expected);
                    e_tkt.put("teen"+(i+1)+"etkt",TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_etkt, "YTH"))).get(i).getText().replace("-",""));
                }
            }
            if (testData.get("noOfChild")!=null) {
                int noOfChild = Integer.parseInt(testData.get("noOfChild"));
                for (int i=0;i<noOfChild; i++) {
                    item = "child"+(i+1)+" name";
                    expected = testData.get("child" + (i + 1) + "Name").split(" ")[1]+" "+testData.get("child" + (i + 1) + "Name").split(" ")[2];
                    actual = TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_name, "CHD"))).get(i).getText();;
                    Assert.assertEquals(actual,expected);
                    e_tkt.put("child"+(i+1)+"etkt",TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_etkt, "CHD"))).get(i).getText());
                }
            }
            if (testData.get("noOfInfant")!=null) {
                int noOfInfant = Integer.parseInt(testData.get("noOfInfant"));
                for (int i=0;i<noOfInfant; i++) {
                    item = "infant"+(i+1)+" name";
                    expected = testData.get("infant" + (i + 1) + "Name").split(" ")[0] +" "+ testData.get("infant" + (i + 1) + "Name").split(" ")[1];
                    actual = TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_name, "INF"))).get(i).getText();;
                    Assert.assertEquals(actual,expected);
                    e_tkt.put("infant"+(i+1)+"etkt",TestManager.getDriver().findElements(By.cssSelector(String.format(traveller_etkt, "INF"))).get(i).getText().replace("-",""));
                }
            }
            return e_tkt;
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,item+" - "+expected+" is displayed as "+actual);
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify traveller details section");
            throw e;
        }
    }


    public void enterAPIDetails(HashMap<String,String> testData){
        try {
            actionUtility.waitForElementVisible(travellerSection,40);
            int noOfAdults = Integer.valueOf(testData.get("noOfAdult"));
            if(testData.get("noOfAdult")!=null) {
                for (int i = 0; i < noOfAdults; i++) {
                    actionUtility.dropdownSelect(genderADT.get(i), SELECTBYTEXT, testData.get("adult" +(i+1)+ "Gender"));
                    actionUtility.sendKeys(DOBDayADT.get(i), testData.get("adult" + (i + 1) + "DOB").split(" ")[0]);
                    actionUtility.dropdownSelect(DOBMonthADT.get(i), SELECTBYTEXT, testData.get("adult" + (i + 1) + "DOB").split(" ")[1]);
                    actionUtility.sendKeys(DOBYearADT.get(i), testData.get("adult" + (i + 1) + "DOB").split(" ")[2]);
                    actionUtility.dropdownSelect(nationalityADT.get(i), SELECTBYTEXT, testData.get("adult" +(i+1)+ "Nationality"));
                    actionUtility.dropdownSelect(identityDocADT.get(i), SELECTBYTEXT, testData.get("adult" +(i+1)+ "Document"));
                    actionUtility.sendKeys(documentNumberADT.get(i),testData.get("adult"+(i+1)+"DocumentNo"));
                    actionUtility.dropdownSelect(countryIssueADT.get(i), SELECTBYTEXT, testData.get("adult" +(i+1)+ "IssuingCountry"));

                    actionUtility.sendKeys(dayExpiryADT.get(i), testData.get("adult" + (i + 1) + "APIExpiry").split(" ")[0]);
                    actionUtility.dropdownSelect(monthExpiryADT.get(i), SELECTBYTEXT, testData.get("adult" + (i + 1) + "APIExpiry").split(" ")[1]);
                    actionUtility.sendKeys(yearExpiryADT.get(i), testData.get("adult" + (i + 1) + "APIExpiry").split(" ")[2]);
                    reportLogger.log(LogStatus.INFO,"successfully entered and saved passenger details for-" +testData.get("adult" + (i+1) + "name"));
                    }
                }
                if(testData.get("noOfInfant")!=null) {
                    int noOfInfants = Integer.valueOf(testData.get("noOfInfant"));
                    for (int i = 0; i < noOfInfants; i++) {
                        actionUtility.dropdownSelect(genderINF.get(i), SELECTBYTEXT, testData.get("infant" +(i+1)+ "Gender"));
                        //actionUtility.sendKeys(DOBDayINF.get(i), testData.get("infant" + (i + 1) + "DOB").split(" ")[0]);
                        //actionUtility.dropdownSelect(DOBMonthINF.get(i), SELECTBYTEXT, testData.get("infant" + (i + 1) + "DOB").split(" ")[1]);
                        //actionUtility.sendKeys(DOBYearINF.get(i), testData.get("infant" + (i + 1) + "DOB").split(" ")[2]);
                        actionUtility.dropdownSelect(nationalityINF.get(i), SELECTBYTEXT, testData.get("infant" +(i+1)+ "Nationality"));
                        actionUtility.dropdownSelect(identityDocINF.get(i), SELECTBYTEXT, testData.get("infant" +(i+1)+ "Document"));
                        actionUtility.sendKeys(documentNumberINF.get(i),testData.get("infant"+(i+1)+"DocumentNo"));
                        actionUtility.dropdownSelect(countryIssueINF.get(i), SELECTBYTEXT, testData.get("infant" +(i+1)+ "IssuingCountry"));

                        actionUtility.sendKeys(dayExpiryINF.get(i), testData.get("infant" + (i + 1) + "APIExpiry").split(" ")[0]);
                        actionUtility.dropdownSelect(monthExpiryINF.get(i), SELECTBYTEXT, testData.get("infant" + (i + 1) + "APIExpiry").split(" ")[1]);
                        actionUtility.sendKeys(yearExpiryINF.get(i), testData.get("infant" + (i + 1) + "APIExpiry").split(" ")[2]);
                        reportLogger.log(LogStatus.INFO,"successfully entered and saved passenger details for-" +testData.get("infant" + (i+1) + "name"));
                    }

                }

                if(testData.get("noOfTeen")!=null) {
                    int noOfTeen = Integer.valueOf(testData.get("noOfTeen"));
                    for (int i = 0; i < noOfTeen; i++) {
                        actionUtility.dropdownSelect(genderYTH.get(i), SELECTBYTEXT, testData.get("teen" +(i+1)+ "Gender"));
                        actionUtility.sendKeys(DOBDayYTH.get(i), testData.get("teen" + (i + 1) + "DOB").split(" ")[0]);
                        actionUtility.dropdownSelect(DOBMonthYTH.get(i), SELECTBYTEXT, testData.get("teen" + (i + 1) + "DOB").split(" ")[1]);
                        actionUtility.sendKeys(DOBYearYTH.get(i), testData.get("teen" + (i + 1) + "DOB").split(" ")[2]);
                        actionUtility.dropdownSelect(nationalityYTH.get(i), SELECTBYTEXT, testData.get("teen" +(i+1)+ "Nationality"));
                        actionUtility.dropdownSelect(identityDocYTH.get(i), SELECTBYTEXT, testData.get("teen" +(i+1)+ "Document"));
                        actionUtility.sendKeys(documentNumberYTH.get(i),testData.get("teen"+(i+1)+"DocumentNo"));
                        actionUtility.dropdownSelect(countryIssueYTH.get(i), SELECTBYTEXT, testData.get("teen" +(i+1)+ "IssuingCountry"));

                        actionUtility.sendKeys(dayExpiryYTH.get(i), testData.get("teen" + (i + 1) + "APIExpiry").split(" ")[0]);
                        actionUtility.dropdownSelect(monthExpiryYTH.get(i), SELECTBYTEXT, testData.get("teen" + (i + 1) + "APIExpiry").split(" ")[1]);
                        actionUtility.sendKeys(yearExpiryYTH.get(i), testData.get("teen" + (i + 1) + "APIExpiry").split(" ")[2]);
                        reportLogger.log(LogStatus.INFO,"successfully entered and saved passenger details for-" +testData.get("teen" + (i+1) + "name"));
                    }

                }

                if(testData.get("noOfChild")!=null) {
                    int noOfChild = Integer.valueOf(testData.get("noOfChild"));
                    for (int i = 0; i < noOfChild; i++) {
                        actionUtility.dropdownSelect(genderCHD.get(i), SELECTBYTEXT, testData.get("child" +(i+1)+ "Gender"));
                        //actionUtility.sendKeys(DOBDayCHD.get(i), testData.get("child" + (i + 1) + "DOB").split(" ")[0]);
                        //actionUtility.dropdownSelect(DOBMonthCHD.get(i), SELECTBYTEXT, testData.get("child" + (i + 1) + "DOB").split(" ")[1]);
                        //actionUtility.sendKeys(DOBYearCHD.get(i), testData.get("child" + (i + 1) + "DOB").split(" ")[2]);
                        actionUtility.dropdownSelect(nationalityCHD.get(i), SELECTBYTEXT, testData.get("child" +(i+1)+ "Nationality"));
                        actionUtility.dropdownSelect(identityDocCHD.get(i), SELECTBYTEXT, testData.get("child" +(i+1)+ "Document"));
                        actionUtility.sendKeys(documentNumberCHD.get(i),testData.get("child"+(i+1)+"DocumentNo"));
                        actionUtility.dropdownSelect(countryIssueCHD.get(i), SELECTBYTEXT, testData.get("child" +(i+1)+ "IssuingCountry"));

                        actionUtility.sendKeys(dayExpiryCHD.get(i), testData.get("child" + (i + 1) + "APIExpiry").split(" ")[0]);
                        actionUtility.dropdownSelect(monthExpiryCHD.get(i), SELECTBYTEXT, testData.get("child" + (i + 1) + "APIExpiry").split(" ")[1]);
                        actionUtility.sendKeys(yearExpiryCHD.get(i), testData.get("child" + (i + 1) + "APIExpiry").split(" ")[2]);
                        reportLogger.log(LogStatus.INFO,"successfully entered and saved passenger details for-" +testData.get("child" + (i+1)+ "name"));
                    }
                }
                saveTravellerDetails();
                reportLogger.log(LogStatus.INFO,"successfully entered and saved passenger details");
            }catch (Exception e) {
                reportLogger.log(LogStatus.INFO, "unable to enter and save advance passenger details");
                throw e;
            }
        }

    private void saveTravellerDetails(){
            try{
                actionUtility.hardSleep(2000);
                actionUtility.waitForElementVisible(continueButton, 30);
                actionUtility.hardClick(continueButton);
                actionUtility.waitForPageLoad(30);
                actionUtility.waitForElementVisible(manageBookingButton, 40);
                reportLogger.log(LogStatus.INFO,"successfully saved passenger details");
            }catch (Exception e){
                reportLogger.log(LogStatus.INFO, "unable to save advance passenger details");
                throw e;
            }
        }

    public void modifyContactDetails(HashMap<String,String> testData){
        try{
                actionUtility.waitForElementVisible(phoneNumber,20);
                phoneNumber.clear();
                phoneNumber.sendKeys(testData.get("modifyPhoneNumber"));
                bookerEmail.clear();
                bookerEmail.sendKeys(testData.get("modifyBookerEmail"));
                bookerEmailConfirmation.clear();
                bookerEmailConfirmation.sendKeys(testData.get("modifyBookerEmail"));
                actionUtility.hardSleep(200);
                saveTravellerDetails();
                reportLogger.log(LogStatus.ERROR,"successfully modified contact details");
            }catch (Exception e){
                reportLogger.log(LogStatus.ERROR, "unable to modify contact details");
                throw e;

            }
    }

    public void enterFrequentFlyerNumber(HashMap<String,String> testData){
        try{
            actionUtility.sendKeys(frequentFlyerNumber,testData.get("frequentFlyerNumber"));
            saveTravellerDetails();
            reportLogger.log(LogStatus.INFO,"successfully entered frequent flyer number in MB-traveller details page");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to enter frequent flyer number in MB-traveller details page");
            throw e;
        }
    }

}
