package efly.business_logics.online;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.CommonUtility;
import utilities.TestManager;

import java.util.HashMap;
import java.util.List;


/**
 * Created by Sona on 3/20/2019.
 */
public class EFLYFareSelect {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public EFLYFareSelect(){
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    //Select Flight
    @FindBy(xpath = "//div[div[div[div[div[h3[div[span[@class='direct']]]]]]]]//div[div[span[text()='JUST FLY']]]")
    List<WebElement> directJustFly;

    @FindBy(xpath = "//div[div[div[div[div[h3[div[span[@class='direct']]]]]]]]//div[div[span[text()='GET MORE']]]")
    List<WebElement> directGetMore;

    @FindBy(xpath = "//div[div[div[div[div[h3[div[span[@class='direct']]]]]]]]//div[div[span[text()='ALL IN']]]")
    List<WebElement> directAllIn;

    @FindBy(xpath = "//div[div[div[div[div[span[text()='1 stop']]]]]]//div[div[span[text()='JUST FLY']]]")
    List<WebElement> oneStopJustFly;

    @FindBy(xpath = "//div[div[div[div[div[span[text()='1 stop']]]]]]//div[div[span[text()='GET MORE']]]")
    List<WebElement> oneStopGetMore;

    @FindBy(xpath = "//div[div[div[div[div[span[text()='1 stop']]]]]]//div[div[span[text()='ALL IN']]]")
    List<WebElement> oneStopAllIn;

    @FindBy(xpath = "//div[div[div[div[div[span[text()='2 stops']]]]]]//div[div[span[text()='JUST FLY']]]")
    List<WebElement> twoStopJustFly;

    @FindBy(xpath = "//div[div[div[div[div[span[text()='2 stops']]]]]]//div[div[span[text()='GET MORE']]]")
    List<WebElement> twoStopGetMore;

    @FindBy(xpath = "//div[div[div[div[div[span[text()='2 stops']]]]]]//div[div[span[text()='ALL IN']]]")
    List<WebElement> twoStopAllIn;

    private String directOtherOperatorJustFly = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='Direct']]]]]//div[div[span[text()='JUST FLY']]]";
    private String directOtherOperatorGetMore = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='Direct']]]]]//div[div[span[text()='GET MORE']]]";
    private String directOtherOperatorAllIn = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='Direct']]]]]//div[div[span[text()='ALL IN']]]";

    private String oneStopOtherOperatorJustFly = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='1 stop']]]]]//div[div[span[text()='JUST FLY']]]";
    private String oneStopOtherOperatorGetMore = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='1 stop']]]]]//div[div[span[text()='GET MORE']]]";
    private String oneStopOtherOperatorAllIn = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='1 stop']]]]]//div[div[span[text()='ALL IN']]]";

    private String twoStopOtherOperatorJustFly = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='2 stops']]]]]//div[div[span[text()='JUST FLY']]]";
    private String twoStopOtherOperatorGetMore = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='2 stops']]]]]//div[div[span[text()='GET MORE']]]";
    private String twoStopOtherOperatorAllIn = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='2 stops']]]]]//div[div[span[text()='ALL IN']]]";

    @FindBy(css = "button[aria-describedby='show-more-flights-description']")
    private WebElement showMoreFlights;

    //flight details
    String selectedFlightFare = "//span[contains(@class,'bestprice')]";

    @FindBy(xpath = "//div[div[div[div[div[h3[div[span[@class='direct']]]]]]]]")
    private List<WebElement> directFlights;

    @FindBy(xpath = "//div[div[div[div[div[h3[div[span[@class='direct']]]]]]]]//time[@class='time-from']")
    private List<WebElement> directFlightDepartureTime;

    @FindBy(xpath = "//div[div[div[div[div[h3[div[span[@class='direct']]]]]]]]//time[@class='time-to']")
    private List<WebElement> directFlightArrivalTime;

    @FindBy(xpath = "//div[div[div[div[div[h3[div[span[@class='direct']]]]]]]]//span[@class='aircraftnumber']")
    private List<WebElement> directFlightNumber;

    String opName = "span[class='opName']";

    @FindBy(css = "div[id*='table-flightline-details']")
    private List<WebElement> availableFlights;

    @FindBy(css = ".cell-reco-details.selected .cell-reco-bestprice-integer")
    private List<WebElement> selectedFlightPrice;

    @FindBy(xpath = "//div[span[text()='JUST FLY']]/div/span")
    private List<WebElement> justFlyFares;

    @FindBy(xpath = "//div[span[text()='GET MORE']]/div/span")
    private List<WebElement> getMoreFares;

    @FindBy(xpath = "//div[span[text()='ALL IN']]/div/span")
    private List<WebElement> allInFares;

    @FindBy(xpath = "//span[text()='continue']")
    private WebElement continueButton;

    //Flex popup
    @FindBy(css = "button[class='btn dialog-upgrade-btn flybe-btn']")
    private WebElement addFlex;

    @FindBy(css = "button[class='btn btn-link cancel-button']")
    private WebElement doNotAddFlex;

    @FindBy(className = "price-value")
    private WebElement flexPrice;

    //price on selected date
    @FindBy(xpath="//a[span[text()='Selected departure date']]//abbr")
    private WebElement dateSliderCurrencyOnSelectedDate;

    @FindBy(xpath = "//a[span[text()='Selected departure date']]//span[@class='price']")
    private WebElement dateSliderPriceonSelectedDate;

    @FindBy(xpath = "//div[span[text()='JUST FLY']]/div")
    private List<WebElement> justFlyPriceOnSelectedDate;

    //route

    @FindBy(css = "div[class='header-location header-locationfrom availability-header-locationfrom']")
    private List<WebElement> route;

    @FindBy(css = "div[class='tripsummary-title tripsummary-date tripsummary-details'] span:nth-child(2)")
    private WebElement dateSelected;

    //basket
    @FindBy(className = "tripsummary-price-amount-text")
    private WebElement totalAmount;

    @FindBy(id = "button-tripsummary-booking")
    private WebElement bookingDetails;

    @FindBy(className = "price-total-amount-value")
    private WebElement fare;

    @FindBy(css = "ul[class*='price-mini-breakdown'] li span[class*='price-details-traveller-value']")
    private List<WebElement> travellerTotal;

    @FindBy(css = "span[class*='price-details-taxes-amount-value']")
    private List<WebElement> taxAndFees;

    @FindBy(css = "i[id*='traveller-taxes']")
    private List<WebElement> travellerTaxBreakdown;

    String taxText = "//div[a[div[span[contains(text(),'%s')]]]]//div[contains(@id,'traveller-taxes')]//span[contains(@class,'text')]";
    String taxValue = "//div[a[div[span[contains(text(),'%s')]]]]//div[contains(@id,'traveller-taxes')]//span[contains(@class,'price-value')]";

    @FindBy(css = "span[class*='departure']")
    private List<WebElement> departure;

    @FindBy(css = "span[class*='return']")
    private List<WebElement> destination;

    @FindBy(css = "div[class='segment-details-departure '] div div:nth-child(2)")
    List<WebElement> journeySource;

    @FindBy(css = "div[class='segment-details-arrival '] div div:nth-child(2)")
    List<WebElement> journeyDestination;

    @FindBy(xpath = "//div[span[@class='segment-details-stop-location-name']]")
    List<WebElement> stop;

    @FindBy(xpath = "//div[span[@class='flight-info-airline-name']]")
    List<WebElement> flightNos;

    @FindBy(css = "div[class*='flight-info-aircraft'] div:nth-child(2)")
    List<WebElement> aircraft;

    @FindBy(className = "date")
    List<WebElement> journeyDates;

    @FindBy(css = "time[class*='segment-details-time']")
    List<WebElement> journeyTimings;

    @FindBy(className = "total-duration-value")
    List<WebElement> journeyDuration;

    //modifySearch
    @FindBy (xpath = "//span[text()='Modify search']")
    private WebElement modifySearch;

    @FindBy(css = "ol[role='listbox']")
    private WebElement airport_list;

    @FindBy(css = "span[class='input-group origin-input searchbar-location-origin-input '] input[placeholder='From']")
    private WebElement flightFromModifySearch;

    @FindBy (xpath = "//span[text()='Hide search']")
    private WebElement hideSearch;

    @FindBy(css = "span[class='input-group destination-input searchbar-location-destination-input '] input[placeholder='To']")
    private WebElement flightToModifySearch;

    @FindBy(xpath = "//span[text()='Search']")
    private WebElement searchFlights;

    String dateTobeSelected = "//div[@name='%s']//span[@class='std-dropdown-icon square icon-calendar']";

    String modifySearchMonthPicker = "//div[@class='xCalendar_std_monthTitle']";

    //String modifySearchDatePicker ="//td[contains(@id,'xCalendar_std')]";

    String modifySearchDatePicker ="//td[contains(@id,'xCalendar_std')][contains(@title,'%s')]";

    @FindBy (xpath = "//span[@class='dropdown-icon icon-adult']")
    private WebElement passengerSelection;

    @FindBy (xpath = "//select[contains(@id,'adtTravNumber')]")
    private WebElement modifyAdultSelection;

    @FindBy (xpath = "//select[contains(@id,'chdTravNumber')]")
    private WebElement modifyChildSelection;

    @FindBy (xpath = "//select[contains(@id,'infTravNumber')]")
    private WebElement modifyInfantSelection;

    @FindBy (xpath = "//select[contains(@id,'ythTravNumber')]")
    private WebElement modifyTeenSelection;

    @FindBy (xpath = "//div[@class='tripsummary-travellers tripsummary-small-text']/span")
    private List<WebElement> passengerOrder;

    public HashMap<String,String> retrieveFlightDetails(HashMap<String,String> testData){
        HashMap<String,String> flightDetails = new HashMap<>();
        String expected = null;
        String actual = null;
        String item = null;
        try{
            flightDetails.put("outboundStops",testData.get("departNoOfStops"));
            flightDetails.put("outboundTicketType",testData.get("departTicketType"));
            int outboundNoOfStops = Integer.parseInt(testData.get("departNoOfStops"));
            String[] departureDate = actionUtility.getDate(Integer.parseInt(testData.get("departDateOffset")));
            String[] returnDate = null;
            if(testData.get("returnDateOffset")!=null){
                flightDetails.put("inboundStops",testData.get("returnNoOfStops"));
                flightDetails.put("inboundTicketType",testData.get("returnTicketType"));
                returnDate = actionUtility.getDate(Integer.parseInt(testData.get("returnDateOffset")));
            }
            int stopCounter = 0;
            int flightCounter = 0;
            actionUtility.hardClick(bookingDetails);
            flightDetails.put("outboundSource",journeySource.get(0).getText());
            flightDetails.put("outboundDate",journeyDates.get(0).getText());
            flightDetails.put("outboundDuration",journeyDuration.get(0).getText());
            flightDetails.put("outboundFlightNo1",flightNos.get(flightCounter).getText());
            flightDetails.put("outboundAircraft1",aircraft.get(flightCounter++).getText());
            flightDetails.put("outboundSector1TakeOffTime",journeyTimings.get(0).getText());
            flightDetails.put("outboundSector1BoardingTime",CommonUtility.SubtractMinutesFromGivenTime(CommonUtility.convertStringToTime(flightDetails.get("outboundSector1TakeOffTime")),30)+"");
            flightDetails.put("outboundSector1LandingTime",journeyTimings.get(1).getText());
            int timeCounter = 2;
            if(outboundNoOfStops>0){
                flightDetails.put("outboundNoOfFlights",(outboundNoOfStops+1)+"");
                for(int i=1;i<=outboundNoOfStops;i++) {
                    flightDetails.put("outboundStop"+i,stop.get(stopCounter++).getText());
                    flightDetails.put("outboundFlightNo"+(i+1),flightNos.get(flightCounter).getText());
                    flightDetails.put("outboundAircraft"+(i+1),aircraft.get(flightCounter).getText());
                    flightDetails.put("outboundSector"+(i+1)+"TakeOffTime",journeyTimings.get(timeCounter++).getText());
                    flightDetails.put("outboundSector"+(i+1)+"BoardingTime",CommonUtility.SubtractMinutesFromGivenTime(CommonUtility.convertStringToTime(flightDetails.get("outboundSector"+(i+1)+"TakeOffTime")),30)+"");
                    flightDetails.put("outboundSector"+(i+1)+"LandingTime",journeyTimings.get(flightCounter++).getText());
                }
            }
            flightDetails.put("outboundDestination",journeyDestination.get(flightCounter-1).getText());
            if(testData.get("returnDateOffset")!=null){
                flightDetails.put("inboundSource",journeySource.get(flightCounter).getText());
                flightDetails.put("inboundDestination",journeyDestination.get(journeyDestination.size()-1).getText());
                flightDetails.put("inboundDate",journeyDates.get(1).getText());
                flightDetails.put("inboundDuration",journeyDuration.get(1).getText());
                flightDetails.put("inboundFlightNo1",flightNos.get(flightCounter).getText());
                flightDetails.put("inboundAircraft1",aircraft.get(flightCounter++).getText());
                flightDetails.put("inboundSector1TakeOffTime",journeyTimings.get(timeCounter++).getText());
                flightDetails.put("inboundSector1BoardingTime",CommonUtility.SubtractMinutesFromGivenTime(CommonUtility.convertStringToTime(flightDetails.get("inboundSector1TakeOffTime")),30)+"");
                flightDetails.put("inboundSector1LandingTime",journeyTimings.get(timeCounter++).getText());
                int inboundNoOfStops = Integer.parseInt(testData.get("returnNoOfStops"));
                if(inboundNoOfStops>0){
                    flightDetails.put("inboundNoOfFlights",(inboundNoOfStops+1)+"");
                    for(int i=1;i<=inboundNoOfStops;i++) {
                        flightDetails.put("inboundStop"+i,stop.get(stopCounter++).getText());
                        flightDetails.put("inboundFlightNo"+(i+1),flightNos.get(flightCounter).getText());
                        flightDetails.put("inboundAircraft"+(i+1),aircraft.get(flightCounter++).getText());
                        flightDetails.put("inboundSector"+(i+1)+"TakeOffTime",journeyTimings.get(timeCounter++).getText());
                        flightDetails.put("inboundSector"+(i+1)+"BoardingTime",CommonUtility.SubtractMinutesFromGivenTime(CommonUtility.convertStringToTime(flightDetails.get("inboundSector"+(i+1)+"TakeOffTime")),30)+"");
                        flightDetails.put("inboundSector"+(i+1)+"LandingTime",journeyTimings.get(timeCounter++).getText());
                    }
                }
            }
            try{
                item = "outbound source";
                expected = testData.get("flightFrom");
                actual = flightDetails.get("outboundSource").split("\\(")[1];
                actual = actual.substring(0,actual.length()-1);
                Assert.assertTrue(expected.contains(actual));

                item = "outbound destination";
                expected = testData.get("flightTo");
                actual = flightDetails.get("outboundDestination");
                Assert.assertTrue(actual.contains(expected));

                item = "outbound date";
                expected = departureDate[0]+" "+actionUtility.getMonth(Integer.parseInt(testData.get("departDateOffset")))+" "+departureDate[2];
                actual = flightDetails.get("outboundDate");
                Assert.assertTrue(actual.contains(expected));

                if(testData.get("returnDateOffset")!=null) {
                    item = "inbound source";
                    expected = testData.get("flightTo");
                    actual = flightDetails.get("inboundSource");
                    Assert.assertTrue(actual.contains(expected));

                    item = "inbound destination";
                    expected = testData.get("flightFrom");
                    actual = flightDetails.get("inboundDestination");
                    Assert.assertTrue(actual.contains(expected));

                    item = "inbound date";
                    expected = returnDate[0]+" "+actionUtility.getMonth(Integer.parseInt(testData.get("returnDateOffset")))+" "+returnDate[2];
                    actual = flightDetails.get("inboundDate");
                    Assert.assertTrue(actual.contains(expected));
                }
                reportLogger.log(LogStatus.INFO,"successfully retrieved itinerary details from booking details section - "+flightDetails);
            }catch(AssertionError ae){
                System.out.println(expected+" ---- "+actual);
                reportLogger.log(LogStatus.INFO,"there is mismatch in "+item+": displayed "+item+" - "+actual+" and expected "+item+" - "+expected);
                throw ae;
            }
            actionUtility.hardClick(bookingDetails);
        }catch(Exception e){
            throw e;
        }
        System.out.println(flightDetails);
        return flightDetails;
    }

    public HashMap<String,Double> getFareDetails(HashMap<String,String> testData,Boolean... taxVerificationPerPax){
        //retrieves tax and fees and also total per traveller
        HashMap<String,Double> basket = new HashMap<>();
        try{
            actionUtility.hardSleep(2000);
            actionUtility.hardClick(bookingDetails);
            try {
                basket.put("fare", CommonUtility.convertStringToDouble(fare.getText()));
            }catch(NoSuchElementException ne){
                actionUtility.hardClick(bookingDetails);
                actionUtility.hardSleep(2000);
                actionUtility.hardClick(bookingDetails);
            }
            basket.put("fare", CommonUtility.convertStringToDouble(fare.getText()));
            //basket.put("total",CommonUtility.convertStringToDouble(fare.getText()));
            basket.put("tax&feesTotal",CommonUtility.convertStringToDouble(taxAndFees.get(0).getText()));

            String[] pax = {"Adult","Teen","Child","Infant"};
            int noOfPax = 1;
            if(testData.get("noOfTeen")!=null && new Integer(testData.get("noOfTeen"))>0) {
                noOfPax++;
            }
            if(testData.get("noOfChild")!=null && new Integer(testData.get("noOfChild"))>0){
                noOfPax++;
            }
            if(testData.get("noOfInfant")!=null && new Integer(testData.get("noOfInfant"))>0){
                noOfPax++;
            }
            for(int i=0;i<noOfPax;i++){
                basket.put(pax[i]+"Total", CommonUtility.convertStringToDouble(travellerTotal.get(i).getText()));
                basket.put(pax[i]+"Tax&Fees", CommonUtility.convertStringToDouble(taxAndFees.get(i+1).getText()));

                if(pax[i]!="Adult" && taxVerificationPerPax.length>0 && taxVerificationPerPax[0]) {
                    actionUtility.hardClick(travellerTaxBreakdown.get(i));
                    taxVerificationPerPax(pax[i]);
                    actionUtility.hardClick(travellerTaxBreakdown.get(i));
                }
            }
            basket.put("paxType",new Double(noOfPax));
            actionUtility.hardClick(bookingDetails);
            reportLogger.log(LogStatus.INFO,"successfully retrieved fare details - "+basket);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to get fare details");
            throw e;
        }
        return basket;
    }

    private void taxVerificationPerPax(String pax){
        //verifies that APD is not present for teen,child and infant
        //verifies tax calculation //fixme - tax verification to be done
        List<WebElement> tax = TestManager.getDriver().findElements(By.xpath(String.format(taxText,pax)));
        List<WebElement> taxAmount = TestManager.getDriver().findElements(By.xpath(String.format(taxValue,pax)));
        for(int i=0;i<tax.size();i++){
            try {
                Assert.assertFalse(tax.get(i).getText().contains("Air Passenger Duty"));
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.INFO, "APD charges are applied for " + pax);
                throw e;
            }
        }
        reportLogger.log(LogStatus.INFO, "successfully verified that APD charges are not applied for "+ pax);

    }

    private Double getBasketPrice(){
        Double fare = 0.00;
        try{
            actionUtility.waitForElementVisible(totalAmount,90);
            fare = CommonUtility.convertStringToDouble(totalAmount.getText());
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to retrieve total fare displayed in basket");
            throw e;
        }
        return fare;
    }

    public HashMap getDateDisplayed(HashMap<String,String> details){
        try{
            String[] date = dateSelected.getText().split(" ");
            details.put("dateSelected",date[1]+" "+date[2]+" "+date[3]);
            System.out.println(details);
            reportLogger.log(LogStatus.INFO,"date for which flights are displayed on fare select page "+dateSelected.getText());
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to get date for which flights are displayed on fare select page");
            throw e;
        }
        return details;
    }

    public HashMap<String,String> selectFlight(HashMap<String,String> testData,String... getFare){
        //selects both departure and return flight - direct,1 stop and 2 stops
        /*Double inboundFare = 0.00;
        Double outboundFare = 0.00;
        */
        HashMap<String,String> fares = new HashMap<>();
        try{
            actionUtility.waitForElementVisible(continueButton,90);
            if(testData.get("departOperatorName")!=null){
                /*outboundFare = */selectOtherOperatorFlight(testData);
            }
            else {
                if (testData.get("departNoOfStops").equals("0")) {
                    /*outboundFare = */selectDirectFlight(testData);
                } else if (testData.get("departNoOfStops").equals("1")) {
                    /*outboundFare = */selectOneStopFlight(testData);
                } else if (testData.get("departNoOfStops").equals("2")) {
                    /*outboundFare = */selectTwoStopFlight(testData);
                }
            }
            if(getFare.length>0){
                fares.put("noOfFlights","0");
                fares = getPriceForEachFareTypeDisplayed("Outbound",fares);
            }
            if(testData.get("returnDateOffset")!=null){
                actionUtility.hardClick(continueButton);
                actionUtility.hardSleep(2000);
                if(testData.get("returnOperatorName")!=null){
                    /*inboundFare = */selectOtherOperatorFlight(testData,"inbound");
                }
                else {
                    if (testData.get("returnNoOfStops").equals("0")) {
                        /*inboundFare = */selectDirectFlight(testData,"inbound");
                    } else if (testData.get("returnNoOfStops").equals("1")) {
                        /*inboundFare = */selectOneStopFlight(testData,"inbound");
                    } else if (testData.get("returnNoOfStops").equals("2")) {
                        /*inboundFare = */selectTwoStopFlight(testData,"inbound");
                    }
                }
                if(getFare.length>0){
                    fares = getPriceForEachFareTypeDisplayed("Inbound",fares);
                }
            }
            return fares;
        }catch(Exception e){
            throw e;
        }
        /*return outboundFare+inboundFare;*/
    }

    private void selectDirectFlight(HashMap<String,String> testData,String... inbound){
        //selects direct flight for ticket type specified in testdata
        //Double fareSelected = 0.00;
        try{
            if(inbound.length>0){
                if (testData.get("returnTicketType").equalsIgnoreCase("just fly") || testData.get("returnTicketType").equalsIgnoreCase("just fly flex")) {
                    actionUtility.hardClick(directJustFly.get(directJustFly.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully selected inbound Just Fly Direct flight");
                } else if (testData.get("returnTicketType").equalsIgnoreCase("get more")) {
                    actionUtility.hardClick(directGetMore.get(directGetMore.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully selected inbound Get More Direct flight");
                } else {
                    actionUtility.hardClick(directAllIn.get(directAllIn.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully selected inbound All In Direct flight");
                }
                //fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(1).getText());
            }else {
                if (testData.get("departTicketType").equalsIgnoreCase("just fly") || testData.get("departTicketType").equalsIgnoreCase("just fly flex")) {
                    actionUtility.click(directJustFly.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully selected outbound Just Fly Direct flight");
                } else if (testData.get("departTicketType").equalsIgnoreCase("get more")) {
                    actionUtility.click(directGetMore.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully selected outbound Get More Direct flight");
                } else {
                    actionUtility.click(directAllIn.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully selected outbound All In Direct flight");
                }
                //fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(0).getText());
            }
            actionUtility.hardSleep(2000);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select direct flight");
            throw e;
        }
        //return fareSelected;
    }

    private void selectOneStopFlight(HashMap<String,String> testData, String... inbound){
        //selects 1 Stop flight for ticket type specified in testdata
        /*Double fareSelected = 0.0;*/
        try{
            if(inbound.length>0){
                if (testData.get("returnTicketType").equalsIgnoreCase("just fly") || testData.get("returnTicketType").equalsIgnoreCase("just fly flex")) {
                    actionUtility.click(oneStopJustFly.get(oneStopJustFly.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully selected inbound Just Fly 1 stop flight");
                } else if (testData.get("returnTicketType").equalsIgnoreCase("get more")) {
                    actionUtility.click(oneStopGetMore.get(oneStopGetMore.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully selected inbound Get More 1 stop flight");
                } else {
                    actionUtility.click(oneStopAllIn.get(oneStopAllIn.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully selected inbound All In 1 stop flight");
                }
                //fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(1).getText());
            }else {
                if (testData.get("departTicketType").equalsIgnoreCase("just fly") || testData.get("departTicketType").equalsIgnoreCase("just fly flex")) {
                    actionUtility.click(oneStopJustFly.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully selected outbound Just Fly 1 stop flight");
                } else if (testData.get("departTicketType").equalsIgnoreCase("get more")) {
                    actionUtility.click(oneStopGetMore.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully selected outbound Get More 1 stop flight");
                } else {
                    actionUtility.click(oneStopAllIn.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully selected outbound All In 1 stop flight");
                }
                //fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(0).getText());
            }
            actionUtility.hardSleep(2000);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select 1 stop flight");
            throw e;
        }
        /*return fareSelected;*/
    }

    private void selectTwoStopFlight(HashMap<String,String> testData, String... inbound){
        //selects 2 stops flight for ticket type specified in testdata
        /*Double fareSelected = 0.00;*/
        try{
            if(inbound.length>0){
                if (testData.get("returnTicketType").equalsIgnoreCase("just fly") || testData.get("returnTicketType").equalsIgnoreCase("just fly flex")) {
                    actionUtility.click(twoStopJustFly.get(twoStopJustFly.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully inbound selected Just Fly 2 stops flight");
                } else if (testData.get("returnTicketType").equalsIgnoreCase("getmore")) {
                    actionUtility.click(twoStopGetMore.get(twoStopGetMore.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully inbound selected Get More 2 Stops flight");
                } else {
                    actionUtility.click(twoStopAllIn.get(twoStopAllIn.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully inbound selected All In 2 stops flight");
                }
                //fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(1).getText());
            }
            else {
                if (testData.get("departTicketType").equalsIgnoreCase("just fly") || testData.get("departTicketType").equalsIgnoreCase("just fly flex")) {
                    actionUtility.click(twoStopJustFly.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully outbound selected Just Fly 2 stops flight");
                } else if (testData.get("departTicketType").equalsIgnoreCase("getmore")) {
                    actionUtility.click(twoStopGetMore.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully outbound selected Get More 2 Stops flight");
                } else {
                    actionUtility.click(twoStopAllIn.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully outbound selected All In 2 stops flight");
                }
                //fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(1).getText());
            }
            actionUtility.hardSleep(2000);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select 2 stops flight");
            throw e;
        }
        /*return fareSelected;*/
    }

    private void selectOtherOperatorFlight(HashMap<String,String> testData,String... inbound){
        //select other operators flights specified in testData and also calculates and returns total fare
        /*Double fareSelected = 0.00;*/
        try{
            if(inbound.length>0){
                if (testData.get("returnNoOfStops").equals("0")) {
                    if (testData.get("returnTicketType").equalsIgnoreCase("just fly") || testData.get("returnTicketType").equalsIgnoreCase("just fly flex")) {
                        List<WebElement> directJustFlyOperator = TestManager.getDriver().findElements(By.xpath(String.format(directOtherOperatorJustFly, testData.get("returnOperatorName"))));
                        actionUtility.click(directJustFlyOperator.get(directJustFlyOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected Just Fly direct " + testData.get("returnOperatorName") + " outbound flight");
                    } else if (testData.get("returnTicketType").equalsIgnoreCase("get more")) {
                        List<WebElement> directGetMoreOperator = TestManager.getDriver().findElements(By.xpath(String.format(directOtherOperatorGetMore, testData.get("returnOperatorName"))));
                        actionUtility.click(directGetMoreOperator.get(directGetMoreOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected Get More direct " + testData.get("returnOperatorName") + " outbound flight");
                    } else if (testData.get("returnTicketType").equalsIgnoreCase("all in")) {
                        List<WebElement> directAllInOperator = TestManager.getDriver().findElements(By.xpath(String.format(directOtherOperatorAllIn, testData.get("returnOperatorName"))));
                        actionUtility.click(directAllInOperator.get(directAllInOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected All In direct " + testData.get("returnOperatorName") + " outbound flight");
                    }
                }
                if (testData.get("returnNoOfStops").equals("1")) {
                    if (testData.get("returnTicketType").equalsIgnoreCase("just fly") || testData.get("returnTicketType").equalsIgnoreCase("just fly flex")) {
                        List<WebElement> oneStopJustFlyOperator = TestManager.getDriver().findElements(By.xpath(String.format(oneStopOtherOperatorJustFly, testData.get("returnOperatorName"))));
                        actionUtility.click(oneStopJustFlyOperator.get(oneStopJustFlyOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected Just Fly 1 stop " + testData.get("returnOperatorName") + " outbound flight");
                    } else if (testData.get("returnTicketType").equalsIgnoreCase("get more")) {
                        List<WebElement> oneStopGetMoreOperator = TestManager.getDriver().findElements(By.xpath(String.format(oneStopOtherOperatorGetMore, testData.get("returnOperatorName"))));
                        actionUtility.click(oneStopGetMoreOperator.get(oneStopGetMoreOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected Get More 1 stop " + testData.get("returnOperatorName") + " outbound flight");
                    } else if (testData.get("returnTicketType").equalsIgnoreCase("all in")) {
                        List<WebElement> oneStopAllInOperator = TestManager.getDriver().findElements(By.xpath(String.format(oneStopOtherOperatorAllIn, testData.get("returnOperatorName"))));
                        actionUtility.click(oneStopAllInOperator.get(oneStopAllInOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected All In 1 stop " + testData.get("returnOperatorName") + " outbound flight");
                    }

                }
                if (testData.get("returnNoOfStops").equals("2")) {
                    if (testData.get("returnTicketType").equalsIgnoreCase("just fly") || testData.get("returnTicketType").equalsIgnoreCase("just fly flex")) {
                        List<WebElement> twoStopJustFlyOperator = TestManager.getDriver().findElements(By.xpath(String.format(twoStopOtherOperatorJustFly, testData.get("returnOperatorName"))));
                        actionUtility.click(twoStopJustFlyOperator.get(twoStopJustFlyOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected Just Fly 2 stop " + testData.get("returnOperatorName") + " outbound flight");
                    } else if (testData.get("returnTicketType").equalsIgnoreCase("get more")) {
                        List<WebElement> twoStopGetMoreOperator = TestManager.getDriver().findElements(By.xpath(String.format(twoStopOtherOperatorGetMore, testData.get("returnOperatorName"))));
                        actionUtility.click(twoStopGetMoreOperator.get(twoStopGetMoreOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected Get More 2 stop " + testData.get("returnOperatorName") + " outbound flight");
                    } else if (testData.get("returnTicketType").equalsIgnoreCase("all in")) {
                        List<WebElement> twoStopAllInOperator = TestManager.getDriver().findElements(By.xpath(String.format(twoStopOtherOperatorAllIn, testData.get("returnOperatorName"))));
                        actionUtility.click(twoStopAllInOperator.get(twoStopAllInOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected All In 2 stop " + testData.get("returnOperatorName") + " outbound flight");
                    }

                }
                //fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(1).getText());
            }else {
                WebElement flightToBeSelected;
                if (testData.get("departNoOfStops").equals("0")) {
                    if (testData.get("departTicketType").equalsIgnoreCase("just fly") || testData.get("departTicketType").equalsIgnoreCase("just fly flex")) {
                        flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(directOtherOperatorJustFly, testData.get("departOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected just fly direct " + testData.get("departOperatorName") + " outbound flight");
                    } else if (testData.get("departTicketType").equalsIgnoreCase("get more")) {
                        flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(directOtherOperatorGetMore, testData.get("departOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected get more direct " + testData.get("departOperatorName") + " outbound flight");
                    } else if (testData.get("departTicketType").equalsIgnoreCase("all in")) {
                        flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(directOtherOperatorAllIn, testData.get("departOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected all in direct " + testData.get("departOperatorName") + " outbound flight");
                    }
                }
                if (testData.get("departNoOfStops").equals("1")) {
                    if (testData.get("departTicketType").equalsIgnoreCase("just fly") || testData.get("departTicketType").equalsIgnoreCase("just fly flex")) {
                        flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(oneStopOtherOperatorJustFly, testData.get("departOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected just fly 1 stop " + testData.get("departOperatorName") + " outbound flight");
                    } else if (testData.get("departTicketType").equalsIgnoreCase("get more")) {
                        flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(oneStopOtherOperatorGetMore, testData.get("departOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected get more 1 stop " + testData.get("departOperatorName") + " outbound flight");
                    } else if (testData.get("departTicketType").equalsIgnoreCase("all in")) {
                        flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(oneStopOtherOperatorAllIn, testData.get("departOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected all in 1 stop " + testData.get("departOperatorName") + " outbound flight");
                    }
                }
                if (testData.get("departNoOfStops").equals("2")) {
                    if (testData.get("departTicketType").equalsIgnoreCase("just fly") || testData.get("departTicketType").equalsIgnoreCase("just fly flex")) {
                        flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(twoStopOtherOperatorJustFly, testData.get("departOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected just fly 2 stop " + testData.get("departOperatorName") + " outbound flight");
                    } else if (testData.get("departTicketType").equalsIgnoreCase("get more")) {
                        flightToBeSelected =TestManager.getDriver().findElements(By.xpath(String.format(twoStopOtherOperatorGetMore, testData.get("departOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected get more 2 stop " + testData.get("departOperatorName") + " outbound flight");
                    } else if (testData.get("departTicketType").equalsIgnoreCase("all in")) {
                        flightToBeSelected = (TestManager.getDriver().findElements(By.xpath(String.format(twoStopOtherOperatorAllIn, testData.get("departOperatorName")))).get(0));
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected all in 2 stop " + testData.get("departOperatorName") + " outbound flight");
                    }
                }
                //fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(0).getText());
            }
            actionUtility.hardSleep(2000);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select other operator flight");
            throw e;
        }
        /*return fareSelected;*/
    }

    public HashMap<String,Double> navigateToTravellerDetailsAfterHandlingFlexPopup(HashMap<String,String> testData,HashMap<String,Double>... basket){
        //navigates to traveller details page and also select Just Fly Flex if specified in testdata
        //get fare with or without flex
        Double fare = 0.00;
        try{
            if(basket.length>0 && basket[0].get("fare")==null){
                basket[0].put("fare",getBasketPrice());
            }
            actionUtility.hardClick(continueButton);
            actionUtility.hardSleep(2000);
            boolean flexOption = true;
            if(testData.get("flexOptionAvailable")!=null && testData.get("flexOptionAvailable").equalsIgnoreCase("no")){
                flexOption = false;
            }
            if(flexOption) {
                try {
                    if (testData.get("departTicketType").equalsIgnoreCase("just fly") || testData.get("departTicketType").equalsIgnoreCase("just fly flex")) {
                        if (testData.get("returnTicketType") != null) {
                            if (testData.get("returnTicketType").equalsIgnoreCase("just fly") || testData.get("returnTicketType").equalsIgnoreCase("just fly flex")) {
                                fare = handleFlexAndCalculateFare(testData);
                            }
                        } else {
                            fare = handleFlexAndCalculateFare(testData);
                        }
                    }
                } catch (Exception e) {
                    reportLogger.log(LogStatus.INFO, "unable to select options on flex popup and proceed");
                    throw e;
                }
            }
            reportLogger.log(LogStatus.INFO,"successfully navigated to traveller details page");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to proceed to traveller details page");
            throw e;
        }
        if(basket.length>0) {

            fare += basket[0].get("fare");
            basket[0].put("fare", fare);
            basket[0].put("total",fare);
            return basket[0];
        }
        else{
            return null;
        }
    }

    private Double handleFlexAndCalculateFare(HashMap<String,String> testData){
        Double fare = 0.00;
        if (testData.get("flex") != null && testData.get("flex").equalsIgnoreCase("yes")) {
            fare = CommonUtility.convertStringToDouble(flexPrice.getText());
            actionUtility.click(addFlex);
            reportLogger.log(LogStatus.INFO, "successfully selected just fly flex");
        } else {
            actionUtility.click(doNotAddFlex);
            reportLogger.log(LogStatus.INFO, "successfully continued without flex");
        }
        return fare;
    }

    public void verifyBasket(HashMap<String,Double> basket){
        //verifies fare in booking details and in basket
        Double actual = getBasketPrice();
        Double expected = basket.get("fare");
        try{
            Assert.assertEquals(actual,expected);
            reportLogger.log(LogStatus.INFO,"fare displayed in basket - "+actual+"matches with fare displayed in booking details - "+expected);
        }catch(AssertionError ae){
            reportLogger.log(LogStatus.INFO,"fare displayed in basket - "+actual+"doesn't match with fare displayed in booking details - "+expected);
            throw ae;
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify fare displayed in basket");
            throw e;
        }
    }

    public void navigateBackToHome(){
        //navigates back to homepage
        try{
            TestManager.getDriver().navigate().back();
            reportLogger.log(LogStatus.INFO,"successfully navigated back to home page from fare select page");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate back to home page from fare select page");
            throw e;
        }
    }

    public HashMap getRouteAndPriceDetails(){
        //retrieves route details and price on date slider
        HashMap<String,String> details = new HashMap<>();
        try{
            actionUtility.waitForElementVisible(continueButton,90);
            details.put("departure",route.get(0).getText());
            details.put("destination",route.get(1).getText());
            details.put("price",dateSliderCurrencyOnSelectedDate.getText()+dateSliderPriceonSelectedDate.getText());
            System.out.println(details);
            reportLogger.log(LogStatus.INFO,"successfully retrieved route and price on fare select");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to retrieve route and price details on fare select");
            throw e;
        }
        return details;
    }

    public HashMap getDirectFlightDetails(HashMap<String,String> testData,String... operator){
        //return direct flight details
        HashMap<String,String> details = new HashMap<>();
        try{
            actionUtility.waitForElementVisible(continueButton,90);
            for(int i=1;i<=directFlights.size();i++){
                details.put("flight"+i+"DepartureTime",directFlightDepartureTime.get(i-1).getText());
                details.put("flight"+i+"ArrivalTime",directFlightArrivalTime.get(i-1).getText());
                details.put("flight"+i+"Number",directFlightNumber.get(i-1).getText().replace("(","").replace(")",""));
                try {
                    WebElement otherOperator = directFlights.get(i - 1).findElement(By.cssSelector(opName));
                    if (otherOperator.isDisplayed()) {
                        details.put("flight" + i + "Operator", otherOperator.getText());
                    }
                }catch(NoSuchElementException e){
                    //this will handle flybe operator flights
                }
            }
            details.put("departure",testData.get("flightFrom"));
            details.put("destination",testData.get("flightTo"));
            details.put("noOfDirectFlights",directFlights.size()+"");
            reportLogger.log(LogStatus.INFO,"successfully retrieved direct flights details on fare slect page");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to retrieve direct flight details");
            throw e;
        }
        System.out.println(details);
        return details;
    }

    public int getNoOfFlightsAvailable(){
        int noOfFlightsAvailable = 0;
        try{
            actionUtility.waitForElementVisible(continueButton,90);
            noOfFlightsAvailable = availableFlights.size();
            reportLogger.log(LogStatus.INFO,"no. of flights available for booking on fare select page is "+availableFlights);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to find the no. of flights available for booking on fare select page");
            throw e;
        }
        return noOfFlightsAvailable;
    }

    public HashMap<String, String> changeSourceInModifySearch(HashMap<String,String> testData,String clickModifySearch/*,String clickHideSearch*/) {
        // changes the source from modify search option
        try {
            actionUtility.hardSleep(2000);
            if(clickModifySearch.equalsIgnoreCase("yes")) {
                actionUtility.click(modifySearch);
            }
            actionUtility.click(flightFromModifySearch);
            flightFromModifySearch.clear();
            actionUtility.hardSleep(2000);
            actionUtility.sendKeys(flightFromModifySearch,testData.get("departure")/*+ Keys.RETURN*/);
            actionUtility.hardSleep(3000);
            actionUtility.click(airport_list);
            /*if(clickHideSearch.equalsIgnoreCase("yes")) {
                actionUtility.click(hideSearch);
            }
            */
            testData.put("flightFrom",testData.get("departure"));
            reportLogger.log(LogStatus.INFO, "successfully changed the source to " + testData.get("departure") );
            return testData;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to change source from Modify Search option");
            throw e;
        }
    }

    public HashMap<String,String> changeDestinationInModifySearch(HashMap<String,String> testData,String clickModifySearch/*,String clickHideSearch*/) {
        try {
            if(clickModifySearch.equalsIgnoreCase("yes")) {
                actionUtility.click(modifySearch);
            }
            actionUtility.hardSleep(2000);
            actionUtility.click(flightToModifySearch);
            flightToModifySearch.clear();
            actionUtility.hardSleep(2000);
            actionUtility.sendKeys(flightToModifySearch,testData.get("destination") /*+Keys.RETURN*/);
            actionUtility.click(airport_list);
            /*if(clickHideSearch.equalsIgnoreCase("yes")) {
                actionUtility.click(hideSearch);
            }
            */
            testData.put("flightTo",testData.get("destination"));
            reportLogger.log(LogStatus.INFO, "successfully changed the destination to " + testData.get("destination"));
            return testData;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to change destination from Modify Search option");
            throw e;
        }
    }

    public void searchFlights(){
        // clicks on  the search button in modify search
        try{
            //actionUtility.click(modifySearch);
            actionUtility.hardSleep(3000);
            actionUtility.hardClick(searchFlights);
            actionUtility.waitForPageLoad(20);
            actionUtility.hardSleep(3000);
            reportLogger.log(LogStatus.INFO, "Flight searched from modify search option");
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "Unable to search flights from modify search option");
            throw e;
        }
    }

    public void verifyRoute(HashMap<String,String> testData){
        try {
            actionUtility.waitForElementVisible(continueButton,90);
            if (testData.get("departure") != null) {
                Assert.assertTrue(route.get(0).getText().toLowerCase().contains(testData.get("departure").toLowerCase()));
            }
            reportLogger.log(LogStatus.INFO,"source ["+route.get(0).getText()+"] on fare select matches with["+testData.get("departure").toUpperCase()+"]");
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"source ["+route.get(0).getText()+"] on fare select do not match with ["+testData.get("departure").toUpperCase()+"]");
            throw e;
        }
        try{
            if (testData.get("destination")!= null) {
                Assert.assertTrue(route.get(1).getText().toLowerCase().contains(testData.get("destination").toLowerCase()));
            }
            reportLogger.log(LogStatus.INFO,"destination["+route.get(1).getText()+"] on fare select matches with ["+testData.get("destination").toUpperCase()+"]");
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"destination["+route.get(1).getText()+"] on fare select do not match with ["+testData.get("destination").toUpperCase()+"]");
            throw e;
        }
    }

    public HashMap changeTravelDatesInModifySearch(HashMap<String,String> testData,String clickModifySearch/*,String clickHideSearch*/){
        // Changes the the travel dates from modify search
        try {
            if(clickModifySearch.equalsIgnoreCase("yes")) {
                actionUtility.click(modifySearch);
            }
            int changeDepartDateOffset = Integer.parseInt(testData.get("changeDepartDateOffset"));
            String date[] = actionUtility.getDate(changeDepartDateOffset);
            WebElement departure= TestManager.getDriver().findElement(By.xpath(String.format(dateTobeSelected,"beginDate")));
            selectDateFromCalendar(date,departure);
            testData.put("departDateOffset",testData.get("changeDepartDateOffset"));
            reportLogger.log(LogStatus.INFO, "modified the depart date - "+date[0]+"-"+date[1]+"-"+date[2]);
            if (testData.get("changeReturnDateOffset") != null) {
                int changeReturnDateOffset = Integer.parseInt(testData.get("changeReturnDateOffset"));
                date = actionUtility.getDate(changeReturnDateOffset);
                WebElement returnDate = TestManager.getDriver().findElement(By.xpath(String.format(dateTobeSelected,"endDate")));
                selectDateFromCalendar(date,returnDate);
                testData.put("returnDateOffset",testData.get("changeReturnDateOffset"));
                reportLogger.log(LogStatus.INFO, "modified the return date - "+date[0]+"-"+date[1]+"-"+date[2]);
            }
            actionUtility.hardSleep(3000);
            /*if(clickHideSearch.equalsIgnoreCase("yes")) {
                actionUtility.click(hideSearch);
            }*/
            return testData;
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING, "unable to change travel dates from modify search");
            throw e;
        }
    }

    private void selectDateFromCalendar(String date[],WebElement datePicker){
        //selects the Date from calendar control
        actionUtility.waitForElementVisible(datePicker,60);
        actionUtility.hardClick(datePicker);
        actionUtility.hardClick(TestManager.getDriver().findElement(By.xpath(String.format(modifySearchDatePicker, date[0] + " " + date[1]))));
    }

    public HashMap<String,String>  changeNoOfPassenger(HashMap<String,String> testData,String clickModifySearch/*,String clickHideSearch*/) {
        //Changes the no.of passengers
        try {
            if(clickModifySearch.equalsIgnoreCase("yes")) {
                actionUtility.click(modifySearch);
            }
            actionUtility.click(passengerSelection);
            int noOfTeen = 0, noOfChild = 0, noOfInfant = 0, noOfAdult = 1;

            if (testData.get("changeNoOfTeen") != null) {
                noOfTeen = Integer.parseInt(testData.get("changeNoOfTeen"));
                actionUtility.waitForElementVisible(modifyTeenSelection, 200);
                modifyTeenSelection.sendKeys(testData.get("changeNoOfTeen") + Keys.RETURN);
                testData.put("noOfTeen",testData.get("changeNoOfTeen"));
            }
            if (testData.get("changeNoOfAdult") != null) {
                noOfAdult = Integer.parseInt(testData.get("changeNoOfAdult"));
                actionUtility.waitForElementVisible(modifyAdultSelection, 300);
                modifyAdultSelection.sendKeys(testData.get("changeNoOfAdult") + Keys.RETURN);
                testData.put("noOfAdult",testData.get("changeNoOfAdult"));
            }
            if (testData.get("changeNoOfChild") != null) {
                noOfChild = Integer.parseInt(testData.get("changeNoOfChild"));
                actionUtility.waitForElementVisible(modifyChildSelection, 300);
                modifyChildSelection.sendKeys(testData.get("changeNoOfChild") + Keys.RETURN);
                testData.put("noOfChild",testData.get("changeNoOfChild"));
            }
            if (testData.get("changeNoOfInfant") != null) {
                noOfInfant = Integer.parseInt(testData.get("changeNoOfInfant"));
                actionUtility.waitForElementVisible(modifyInfantSelection, 300);
                modifyInfantSelection.sendKeys(testData.get("changeNoOfInfant") + Keys.RETURN);
                testData.put("noOfInfant",testData.get("changeNoOfInfant"));
            }
            /*if(clickHideSearch.equalsIgnoreCase("yes")) {
                actionUtility.click(hideSearch);
            }*/
            reportLogger.log(LogStatus.INFO, "successfully modified the number of passengers");
            reportLogger.log(LogStatus.INFO, "Adults - " + noOfAdult + " Teens - " + noOfTeen + " Children - " + noOfChild + " Infant - " + noOfInfant);
            return testData;
        } catch (Exception e) {
            reportLogger.log(LogStatus.WARNING, "unable to modify the number of passengers");
            throw e;
        }
    }

    public void verifyThePassengerOrder(HashMap<String,String> testData){
        String expected = null,actual=null;
        try{
            int i = 0;
            if(testData.get("noOfAdult")!=null){
                expected = testData.get("noOfAdult")+" "+"adult";
                actual = passengerOrder.get(i++).getText();
                Assert.assertTrue(actual.contains(expected),expected+" -- "+actual);
            }
            if(testData.get("noOfTeen")!=null){
                expected = testData.get("noOfTeen")+" "+"teen";
                actual = passengerOrder.get(i++).getText();
                Assert.assertTrue(actual.contains(expected),expected+" -- "+actual);
            }
            if(testData.get("noOfChild")!=null){
                expected = testData.get("noOfChild")+" "+"child";
                actual = passengerOrder.get(i++).getText();
                Assert.assertTrue(actual.contains(expected),expected+" -- "+actual);
            }
            if(testData.get("noOfInfant")!=null){
                expected = testData.get("noOfInfant")+" "+"infant";
                actual = passengerOrder.get(i++).getText();
                Assert.assertTrue(actual.contains(expected),expected+" -- "+actual);
            }
        }catch (AssertionError e){
            reportLogger.log(LogStatus.INFO,"passenger order: expected - "+expected+" actual - "+actual);
            throw e;
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify passenger order");
            throw e;
        }

    }

    private HashMap<String,String> getPriceForEachFareTypeDisplayed(String way,HashMap<String,String> fares){
        try{
            int noOfFlights= 1;
            if(way.equals("Inbound")){
                noOfFlights = Integer.parseInt(fares.get("noOfOutboundFlights"))+1;
            }
            for(int flightCount=1, i=noOfFlights;i<=justFlyFares.size();i++,flightCount++){
                fares.put("justFly"+way+flightCount,justFlyFares.get(i-1).getText());
            }
            for(int flightCount=1,i=noOfFlights;i<=getMoreFares.size();i++,flightCount++){
                fares.put("getMore"+way+flightCount,getMoreFares.get(i-1).getText());
            }
            for(int flightCount=1,i=noOfFlights;i<=allInFares.size();i++,flightCount++){
                fares.put("allIn"+way+flightCount,allInFares.get(i-1).getText());
                noOfFlights = i;
            }
            fares.put("noOf"+way+"Flights",noOfFlights+"");
            if(way.equals("Inbound")) {
                fares.put("totalFare", getBasketPrice() + "");
            }
            System.out.println(fares);
            return fares;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to get fare for each flight");
            throw e;
        }
    }

}
