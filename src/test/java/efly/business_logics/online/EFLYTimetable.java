package efly.business_logics.online;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.relevantcodes.extentreports.model.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.TestManager;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Sona on 4/29/2019.
 */
public class EFLYTimetable {
    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public EFLYTimetable() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(css = "td[class*='timetable-operational-grid-req'] i[title='available']")
    private List<WebElement> availableFlightsOnSelectedDate;

    @FindBy(id = "human-date")
    private WebElement calendar;

    @FindBy(className = "ui-datepicker-month")
    private WebElement travelMonth;

    @FindBy(className = "ui-datepicker-year")
    private WebElement travelYear;

    @FindBy(xpath = "//span[text()='Next']")
    private WebElement nextCalendar;

    String travelDate = "//table[@class='ui-datepicker-calendar']//tbody//tr/td/a[text()='%s']";

    @FindBy(xpath = "//div[a[span[text()='Enter a departure airport']]]//b")
    private WebElement flightFromDropDown;

    @FindBy(xpath = "//div[a[span[text()='Enter an arrival airport']]]//b")
    private WebElement flightToDropDown;

    @FindBy(css = "div[class='chosen-search'] input")
    private List<WebElement> flight;

    @FindBy(xpath = "//button[text()='VIEW TIMETABLE']")
    private WebElement viewTimetable;

    public void verifyNoOfAvailableFlights(int noOfFlightsOnFareSelectPage){
        try {
            System.out.println(availableFlightsOnSelectedDate.size()+"----"+noOfFlightsOnFareSelectPage);
            Assert.assertTrue(availableFlightsOnSelectedDate.size()>=noOfFlightsOnFareSelectPage);
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"available no. of flights on timetable = "+availableFlightsOnSelectedDate.size()+" and available no. of flights on fare select page = "+noOfFlightsOnFareSelectPage+" does not match");
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify if the no. of available flights on timetable page matches with the no. of available flights on fare select page");
            throw e;
        }
    }

    public void selectdate(HashMap<String,String> testData){
        //selects travel date
        try {
            actionUtility.click(calendar);
            String[] date = actionUtility.getDate(Integer.parseInt(testData.get("departDateOffset")));
            System.out.println(date[0]+"-"+date[1]+"-"+date[2]);
            while(!travelMonth.getText().contains(date[1]) || !travelYear.getText().equals(date[2])){
                actionUtility.click(nextCalendar);
            }
            actionUtility.click(TestManager.getDriver().findElement(By.xpath(String.format(travelDate,date[0]))));
            reportLogger.log(LogStatus.INFO,"successfully selected today's date");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select travel date");
            throw e;
        }
    }

    public void enterSourceAndDestinationAndSubmit(HashMap<String,String> testData,Boolean... cookieClosed){
        //enter source and destination
        try{
            actionUtility.waitForElementClickable(flightFromDropDown,3);
            actionUtility.click(flightFromDropDown);
            actionUtility.hardSleep(2000);
            actionUtility.sendKeysByAction(flight.get(0), testData.get("flightFrom") + Keys.RETURN);
            actionUtility.click(flightToDropDown);
            actionUtility.hardSleep(2000);
            actionUtility.sendKeysByAction(flight.get(1), testData.get("flightTo") + Keys.RETURN);
            actionUtility.click(viewTimetable);
            reportLogger.log(LogStatus.INFO,"successfully entered the source - "+testData.get("flightFrom")+" and destination - "+testData.get("flightTo"));
        }catch (Exception e){
            reportLogger.log(LogStatus.WARNING,"unable to enter source and destination of the trip");
            throw e;
        }
    }

}
