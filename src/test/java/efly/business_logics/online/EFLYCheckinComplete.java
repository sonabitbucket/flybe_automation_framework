package efly.business_logics.online;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utilities.ActionUtility;
import utilities.CommonUtility;
import utilities.TestManager;

import java.util.HashMap;
import java.util.List;

public class EFLYCheckinComplete {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public EFLYCheckinComplete(){
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    @FindBy(css = "a[class='button continue ng-scope']")
    private WebElement checkinComplete;

    //cancel checkin
    @FindBy(xpath = "//table[@class='pax-table']/tbody/tr/td[2]/span/span[2]/label/span[1]")
    private List<WebElement> selectPassenger;

    @FindBy(xpath = "//span[text()='Cancel check-in']")
    private WebElement cancelCheckIn;

    @FindBy(id = "main-heading")
    private WebElement checkinCancelledMsg;

    @FindBy(className = "message-title")
    private WebElement passengerOffloadedMsg;

    //feedback
    @FindBy(xpath = "//button[text()='No thanks']")
    private WebElement noThanks;

    @FindBy(xpath = "//button[text()='Give feedback']")
    private WebElement giveFeedback;

    String feedback = "//h3[contains(text(),'feedback')]";

    //journey summary
    @FindBy(css = "span[ng-bind='flight.boardpoint.airportName']")
    private WebElement sourceAirport;

    @FindBy(css = "span[ng-bind='flight.offpoint.airportName']")
    private WebElement destinationAirport;

    @FindBy(css = "span[ng-bind*='flight.arrivalTime']")
    private WebElement arrivalTime;

    @FindBy(css = "span[ng-bind*='flight.departureTime']")
    private WebElement departureTime;

    public void completeCheckin(){
        //clicks on done button
        try{
            actionUtility.click(checkinComplete);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to complete checkin");
            throw e;
        }
    }

    public void verifyJourneySummery(HashMap<String,String> flightDetails,String journey){
        String item=null,expected=null,actual=null;
        try{
            handleFeedback();
            int outboundSectors = Integer.parseInt(flightDetails.get(journey+"Stops"))+1;
            for(int i=1;i<=outboundSectors;i++){
                String takeOffTime = CommonUtility.convert24HourTo12HourFormat(flightDetails.get(journey+"Sector"+i+"TakeOffTime"));
                String landingTime = CommonUtility.convert24HourTo12HourFormat(flightDetails.get(journey+"Sector"+i+"LandingTime"));

                item = journey+" sector "+i+" departure time";
                expected = takeOffTime.substring(1);
                actual = departureTime.getText();
                Assert.assertEquals(actual,expected);

                item = journey+" sector "+i+" arrival time";
                expected = landingTime.substring(1);
                actual = arrivalTime.getText();
                Assert.assertEquals(actual,expected);
            }
            item = journey+" departure airport";
            expected = flightDetails.get(journey+"Source").split(" ")[0];
            actual = sourceAirport.getText();
            Assert.assertTrue(actual.contains(expected.toUpperCase()));

            item = journey+" destination airport";
            expected = flightDetails.get(journey+"Destination").split(" ")[0];
            actual = destinationAirport.getText();
            Assert.assertTrue(actual.contains(expected.toUpperCase()));
            reportLogger.log(LogStatus.INFO,"successfully verified journey summary displayed on checkin complete page");
        }catch(AssertionError e){
            System.out.println(item+" == "+expected+"  -- "+actual);
            reportLogger.log(LogStatus.INFO,"journey summary - "+item+" displayed is incorrect on checkin complete page - expected - "+expected+" displayed - "+actual);
            throw e;
        }
    }

    private void handleFeedback(HashMap<String,String>... testData){
        actionUtility.waitForElementVisible(noThanks,30);
        if(testData.length>0 && testData[0].get("giveFeedback")!=null && testData[0].get("giveFeedback").equalsIgnoreCase("yes")){
            actionUtility.hardClick(giveFeedback);
        }
        else{
            actionUtility.hardClick(noThanks);
        }
        actionUtility.waitForElementNotPresent(feedback,60);
    }

    public void selectPassengerForCancelCheckin(int noOfPassengers){
        //in case of multiple passengers in a booking, this method selects all the passenger for cancel checkin
        try{
            for(int i=0;i<noOfPassengers; i++){
                actionUtility.click(selectPassenger.get(i));
            }
            reportLogger.log(LogStatus.INFO,"selected all passengers for cancelling checkin");

        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select passengers for cancelling checkin");
            throw e;
        }
    }

    public void completeCancelCheckin() {
        //clicks on cancel checkin button
        try {
            actionUtility.click(cancelCheckIn);
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(checkinCancelledMsg), "Cancelled Checkin Message is not displayed");
            Assert.assertTrue(actionUtility.verifyIfElementIsDisplayed(passengerOffloadedMsg), "Customer Offloaded Message is not displayed");
            reportLogger.log(LogStatus.INFO, "successfully cancelled checkin");
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"unable to  verify if checkin is cancelled");
            throw e;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to  cancel checkin");
            throw e;
        }
    }
}
