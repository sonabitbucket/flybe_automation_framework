package efly.business_logics.online;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import utilities.ActionUtility;
import utilities.CommonUtility;
import utilities.PDFReader;
import utilities.TestManager;

import java.util.HashMap;
import java.util.List;

public class EFLYBoardingPass {

    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public EFLYBoardingPass(){
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }
    //print boarding pass
    @FindBy(xpath = "//span[text()='Print boarding pass']")
    private WebElement printBoardingPassSection;

    @FindBy(xpath = "//button[contains(@id,'buttonId_Printer')]")
    private WebElement printBoardingPassButton;

    //
    @FindBy(xpath = "//button[span[text()='Finish check-in']]")
    private WebElement finishCheckIn;

    public void printBoardingPass(HashMap<String,String> testData,String PNR,HashMap<String,String> flightDetails,HashMap<String,String> paxDetails,String journeyForCheckin,Boolean... vouchers)
    {
        try{
            Boolean voucherTest = false;
            if(vouchers.length>0 && vouchers[0]){
                voucherTest = true;
            }
            actionUtility.click(printBoardingPassSection);
            actionUtility.click(printBoardingPassButton);
            switchToBoardingPass();
            verifyBP(voucherTest,PNR,testData,flightDetails,paxDetails,journeyForCheckin);
            actionUtility.switchBackToMainWindow("Amadeus 6X check-in,Boarding Pass");

        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to print boarding pass");
            throw e;
        }
    }

    private void verifyBP(boolean voucherTest,String PNR,HashMap<String,String> testData,HashMap<String,String>flightDetails,HashMap<String,String>paxDetails,String journey){
        int noOfTeen=0,noOfChild=0,noOfInfant=0;

        int noOfAdult = Integer.parseInt(testData.get("noOfAdult"));
        if(testData.get("noOfTeen")!=null){
            noOfTeen = Integer.parseInt(testData.get("noOfTeen"));
        }
        if(testData.get("noOfChild")!=null){
            noOfChild = Integer.parseInt(testData.get("noOfChild"));
        }
        if(testData.get("noOfInfant")!=null){
            noOfInfant = Integer.parseInt(testData.get("noOfInfant"));
        }

        String paxName = null;
        for (int i = 1; i <= noOfInfant; i++) {
            paxName = testData.get("infant"+i+"Name").split(" ")[1]+" "+testData.get("infant"+i+"Name").split(" ")[0];
            verifyBoardingPassContent(testData,i, PNR, voucherTest,i,flightDetails,paxDetails.get("infant"+i+"etkt"),paxName,journey);
        }

        if(voucherTest){
            for (int i = 1; i < (noOfAdult*2);i=i+2) {//fixme
                paxName = testData.get("adult"+i+"Name").split(" ")[0]+". "+testData.get("adult"+i+"Name").split(" ")[2]+" "+testData.get("adult"+i+"Name").split(" ")[1];
                verifyBoardingPassContent(testData,i, PNR, voucherTest,i+1,flightDetails,paxDetails.get("adult"+i+"etkt"),paxName,journey);
            }
            for (int i = 1; i < (noOfChild*2);i=i+2) {//fixme
                paxName = testData.get("child"+i+"Name").split(" ")[0];
                if(paxName.equalsIgnoreCase("Master")){
                    paxName = "Mstr";
                }
                paxName = paxName+". "+testData.get("child"+i+"Name").split(" ")[2]+" "+testData.get("child"+i+"Name").split(" ")[1];
                verifyBoardingPassContent(testData,i, PNR, voucherTest,i+1,flightDetails,paxDetails.get("child"+i+"etkt"),paxName,journey);
            }
            for (int i = 1; i < (noOfTeen*2);i=i+2) {//fixme
                paxName = testData.get("teen"+i+"Name").split(" ")[0];
                if(paxName.equalsIgnoreCase("Master")){
                    paxName = "Mstr";
                }
                paxName = paxName+". "+testData.get("teen"+i+"Name").split(" ")[2]+" "+testData.get("teen"+i+"Name").split(" ")[1];
                verifyBoardingPassContent(testData,i, PNR, voucherTest,i+1,flightDetails,paxDetails.get("teen"+i+"etkt"),paxName,journey);
            }
        }
        else {
            for (int i = 1,page=noOfInfant+1; i <= noOfAdult; i++,page++) {
                System.out.println("start page - "+page+" for adult - "+i);
                paxName = testData.get("adult"+i+"Name").split(" ")[0]+". "+testData.get("adult"+i+"Name").split(" ")[2]+" "+testData.get("adult"+i+"Name").split(" ")[1];
                verifyBoardingPassContent(testData,page, PNR, voucherTest,page,flightDetails,paxDetails.get("adult"+i+"etkt"),paxName,journey);
            }
            for (int i = 1,page=noOfInfant+noOfAdult+1; i <= noOfChild; i++,page++) {
                paxName = testData.get("child"+i+"Name").split(" ")[0];
                if(paxName.equalsIgnoreCase("Master")){
                    paxName = "Mstr";
                }
                paxName = paxName+". "+testData.get("child"+i+"Name").split(" ")[2]+" "+testData.get("child"+i+"Name").split(" ")[1];
                verifyBoardingPassContent(testData,page, PNR, voucherTest,page,flightDetails,paxDetails.get("child"+i+"etkt"),paxName,journey);
            }
            for (int i = 1,page=noOfInfant+noOfAdult+noOfChild+1; i <= noOfTeen; i++,page++) {
                paxName = testData.get("teen"+i+"Name").split(" ")[0];
                if(paxName.equalsIgnoreCase("Master")){
                    paxName = "Mstr";
                }
                paxName = paxName+". "+testData.get("teen"+i+"Name").split(" ")[2]+" "+testData.get("teen"+i+"Name").split(" ")[1];
                verifyBoardingPassContent(testData,page, PNR, voucherTest,page,flightDetails,paxDetails.get("teen"+i+"etkt"),paxName,journey);
            }
        }
    }

    private void switchToBoardingPass() {
        //verifies the contents of boarding pass
        boolean isSwitchSuccess = false;
        try {
            actionUtility.hardSleep(10);
            isSwitchSuccess = actionUtility.switchWindow("");
            System.out.println(isSwitchSuccess);
            System.out.println("title of BP - -- "+TestManager.getDriver().getTitle());
            if (!isSwitchSuccess) {
                throw new RuntimeException("unable to switch control to boarding pass");
            }
            reportLogger.log(LogStatus.INFO,"successfully switched to boarding pass window");
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to if verify boarding pass is displayed");
            throw e;
        }
    }

    private void verifyBoardingPassContent(HashMap<String,String> testData,int startPage,String PNR,Boolean voucherTest,int endPage,HashMap<String,String> flightDetails,String etkt,String paxName,String journey) {
        //BP verification
        String assertval = null;
        String boardingPassContent = null;
        try {
            boardingPassContent = PDFReader.extractPDFContent(TestManager.getDriver().getCurrentUrl(), startPage,endPage);
            System.out.println("*************boardingPassContent*****************"+boardingPassContent);
            assertval = "PNR "+PNR;
            Assert.assertTrue(boardingPassContent.contains(PNR), "PNR is not present in boarding pass");

            assertval = "E-Ticket "+etkt;
            Assert.assertTrue(boardingPassContent.contains(etkt), "etkt is not present in boarding pass");

            assertval = "Passenger name "+paxName;
            Assert.assertTrue(boardingPassContent.contains(paxName), "passenger name is not present in boarding pass");

            assertval = journey+" flight number - "+flightDetails.get(journey+"FlightNo1");
            String flightNo =  flightDetails.get(journey+"FlightNo1").split("\\(")[1];
            flightNo = flightNo.split("\\)")[0];
            flightNo = flightNo.split(" ")[0]+flightNo.split(" ")[1];
            Assert.assertTrue(boardingPassContent.contains(flightNo));

            assertval = journey+" source - "+flightDetails.get(journey+"Source");
            Assert.assertTrue((boardingPassContent.contains("Depart "+flightDetails.get(journey+"Source").split("\\(")[0].trim())));

            assertval = journey+" destination - "+flightDetails.get(journey+"Destination");
            Assert.assertTrue((boardingPassContent.contains("Arrive "+flightDetails.get(journey+"Destination").split("\\(")[0].trim())));

            System.out.println("boarding time   = "+(/*"Gate closure\n"+*//*"Date"+"\n"+ *//*CommonUtility.convertDateFormat("E d MMMM yyyy","d MMM yyyy",flightDetails.get(journey+"Date"))+"\n"+*/flightDetails.get(journey+"Sector1BoardingTime")));
            assertval = journey+" boarding time - "+flightDetails.get(journey+"Sector1BoardingTime");
            Assert.assertTrue((boardingPassContent.contains(/*"Gate closure\n"+*//*"Date"+"\n"+ CommonUtility.convertDateFormat("E d MMMM yyyy","d MMM yyyy",flightDetails.get(journey+"Date"))+"\n"+*/flightDetails.get(journey+"Sector1BoardingTime"))));

            assertval = journey+" departure time - "+flightDetails.get(journey+"Sector1TakeOffTime");
            Assert.assertTrue((boardingPassContent.contains("Departs\n"+flightDetails.get(journey+"Sector1TakeOffTime"))));

            assertval = journey+" date - "+flightDetails.get(journey+"Date");//13 Aug 2019
            Assert.assertTrue((boardingPassContent.contains("Date\n"+ CommonUtility.convertDateFormat("E d MMMM yyyy","d MMM yyyy",flightDetails.get(journey+"Date")))));

            if(voucherTest){
                assertval = "Drink voucher";
                Assert.assertTrue(boardingPassContent.contains("Drink"), "drink voucher is not present in boarding pass");
                Assert.assertTrue(boardingPassContent.contains("onboard the aircraft for your drink."), "drink voucher is not present in boarding pass");
                reportLogger.log(LogStatus.INFO,assertval + "is present in boarding pass");


                assertval = "Snack voucher";
                Assert.assertTrue(boardingPassContent.contains("Snack"), "snack voucher is not present in boarding pass");
                Assert.assertTrue(boardingPassContent.contains("onboard the aircraft for your snack."), "snack voucher is not present in boarding pass");
                reportLogger.log(LogStatus.INFO,assertval + "is present in boarding pass");

                if(testData.get("AirportHasLounge").equalsIgnoreCase("yes")) {
                    assertval = "Lounge voucher";
                    Assert.assertTrue(boardingPassContent.contains("Lounge"), "lounge voucher is not present in boarding pass");
                    Assert.assertTrue(boardingPassContent.contains("Visit flybe.com/lounges for more details"), "lounge voucher is not present in boarding pass");
                    reportLogger.log(LogStatus.INFO, assertval + "is present in boarding pass");
                }
                if(testData.get("AirportHasFastTrackSecurity").equalsIgnoreCase("yes")){
                    assertval = "Fast Track Security voucher";
                    Assert.assertTrue(boardingPassContent.contains("Fast track"), "lounge voucher is not present in boarding pass");
                    Assert.assertTrue(boardingPassContent.contains("Please visit fast track security."), "fast track security voucher is not present in boarding pass");
                    reportLogger.log(LogStatus.INFO,assertval + "is present in boarding pass");
                }
            }
            reportLogger.log(LogStatus.INFO,"successfully verified boarding pass for passenger - "+paxName);
        } catch (AssertionError ae) {
            System.out.println(assertval + " is not present in boarding pass");
            reportLogger.log(LogStatus.INFO, assertval + "is not present in boarding pass");
            throw ae;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify boarding pass");
            throw e;
        }
    }

    public void continueToCompleteCheckin(){
        //navigates to Complete Checkin Page
        try{
            actionUtility.click(finishCheckIn);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to continue to complete checkin page");
            throw e;
        }
    }

    //todo saving BP
}
