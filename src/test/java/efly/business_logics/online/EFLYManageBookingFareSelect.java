package efly.business_logics.online;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.ActionUtility;
import utilities.CommonUtility;
import utilities.TestManager;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Indhu on 6/18/2019.
 */
public class EFLYManageBookingFareSelect {
    ActionUtility actionUtility;
    ExtentTest reportLogger;

    public EFLYManageBookingFareSelect() {
        PageFactory.initElements(TestManager.getDriver(), this);
        this.actionUtility = new ActionUtility();
        this.reportLogger = TestManager.getReportLogger();
    }

    private String loader = "//div[@class='loader']";

    //Select Flight
    @FindBy(xpath = "//div[div[div[div[div[h3[div[span[@class='direct']]]]]]]]//div[div[span[text()='JUST FLY']]]")
    List<WebElement> directJustFly;

    @FindBy(xpath = "//div[div[div[div[div[h3[div[span[@class='direct']]]]]]]]//div[div[span[text()='JUST FLY FLEX']]]")
    List<WebElement> directJustFlyFlex;

    @FindBy(xpath = "//div[div[div[div[div[h3[div[span[@class='direct']]]]]]]]//div[div[span[text()='GET MORE']]]")
    List<WebElement> directGetMore;

    @FindBy(xpath = "//div[div[div[div[div[h3[div[span[@class='direct']]]]]]]]//div[div[span[text()='ALL IN']]]")
    List<WebElement> directAllIn;

    @FindBy(xpath = "//div[div[div[div[div[span[text()='1 stop']]]]]]//div[div[span[text()='JUST FLY FLEX']]]")
    List<WebElement> oneStopJustFlyFlex;

    @FindBy(xpath = "//div[div[div[div[div[span[text()='1 stop']]]]]]//div[div[span[text()='JUST FLY']]]")
    List<WebElement> oneStopJustFly;

    @FindBy(xpath = "//div[div[div[div[div[span[text()='1 stop']]]]]]//div[div[span[text()='GET MORE']]]")
    List<WebElement> oneStopGetMore;

    @FindBy(xpath = "//div[div[div[div[div[span[text()='1 stop']]]]]]//div[div[span[text()='ALL IN']]]")
    List<WebElement> oneStopAllIn;

    @FindBy(xpath = "//div[div[div[div[div[span[text()='2 stops']]]]]]//div[div[span[text()='JUST FLY FLEX']]]")
    List<WebElement> twoStopJustFlyFlex;

    @FindBy(xpath = "//div[div[div[div[div[span[text()='2 stops']]]]]]//div[div[span[text()='JUST FLY']]]")
    List<WebElement> twoStopJustFly;

    @FindBy(xpath = "//div[div[div[div[div[span[text()='2 stops']]]]]]//div[div[span[text()='GET MORE']]]")
    List<WebElement> twoStopGetMore;

    @FindBy(xpath = "//div[div[div[div[div[span[text()='2 stops']]]]]]//div[div[span[text()='ALL IN']]]")
    List<WebElement> twoStopAllIn;

    private String directOtherOperatorJustFly = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='Direct']]]]]//div[div[span[text()='JUST FLY']]]";
    private String directOtherOperatorGetMore = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='Direct']]]]]//div[div[span[text()='GET MORE']]]";
    private String directOtherOperatorAllIn = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='Direct']]]]]//div[div[span[text()='ALL IN']]]";

    private String oneStopOtherOperatorJustFly = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='1 stop']]]]]//div[div[span[text()='JUST FLY']]]";
    private String oneStopOtherOperatorGetMore = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='1 stop']]]]]//div[div[span[text()='GET MORE']]]";
    private String oneStopOtherOperatorAllIn = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='1 stop']]]]]//div[div[span[text()='ALL IN']]]";

    private String twoStopOtherOperatorJustFly = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='2 stops']]]]]//div[div[span[text()='JUST FLY']]]";
    private String twoStopOtherOperatorGetMore = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='2 stops']]]]]//div[div[span[text()='GET MORE']]]";
    private String twoStopOtherOperatorAllIn = "//div[div[div[div[div[div[div[span[@class='opName'][contains(text(),'%s')]]]]//span[text()='2 stops']]]]]//div[div[span[text()='ALL IN']]]";

    private String changeFlight = "//div[div[div[div[div[div[div[span[bdo[not(contains(text(),'%s'))]]]]]]]]]//div[div[span[text()='%s']]]";

    @FindBy(css = ".cell-reco-details.selected .cell-reco-bestprice-integer")
    private List<WebElement> selectedFlightPrice;

    @FindBy(xpath = "//span[text()='continue']")
    private WebElement continueButton;

    //route

    @FindBy(css = "div[class='header-location header-locationfrom availability-header-locationfrom']")
    private List<WebElement> route;

    @FindBy(css = "div[class='tripsummary-title tripsummary-date tripsummary-details'] span:nth-child(2)")
    private WebElement dateSelected;

    //basket
    @FindBy(className = "tripsummary-price-amount-text")
    private WebElement totalAmount;

    @FindBy(id = "button-tripsummary-booking")
    private WebElement bookingDetails;

    @FindBy(className = "price-total-amount-value")
    private WebElement fare;

    @FindBy(css = "ul[class*='price-mini-breakdown'] li span[class*='price-details-traveller-value']")
    private List<WebElement> travellerTotal;

    @FindBy(css = "span[class*='price-details-taxes-amount-value']")
    private List<WebElement> taxAndFees;

    @FindBy(css = "i[id*='traveller-taxes']")
    private List<WebElement> travellerTaxBreakdown;

    String taxText = "//div[a[div[span[contains(text(),'%s')]]]]//div[contains(@id,'traveller-taxes')]//span[contains(@class,'text')]";
    String taxValue = "//div[a[div[span[contains(text(),'%s')]]]]//div[contains(@id,'traveller-taxes')]//span[contains(@class,'price-value')]";

    //basket
    String basketPrice = "//div[span[contains(text(),'%s')]]//span[contains(@class,'price-value')]";

    public HashMap<String,Double> getFareDetails(HashMap<String,String> testData,Boolean... taxVerificationPerPax){
        //retrieves tax and fees and also total per traveller
        HashMap<String,Double> basket = new HashMap<>();
        try{
            actionUtility.hardClick(bookingDetails);

            basket.put("fare",CommonUtility.convertStringToDouble(fare.getText()));
            basket.put("tax&feesTotal",CommonUtility.convertStringToDouble(taxAndFees.get(0).getText()));

            String[] pax = {"Adult","Teen","Child","Infant"};
            int noOfPax = 1;
            if(testData.get("noOfTeen")!=null && new Integer(testData.get("noOfTeen"))>0) {
                noOfPax = 2;
            }
            if(testData.get("noOfChild")!=null && new Integer(testData.get("noOfChild"))>0){
                noOfPax = 3;
            }
            if(testData.get("noOfInfant")!=null && new Integer(testData.get("noOfInfant"))>0){
                noOfPax = 4;
            }
            for(int i=0;i<noOfPax;i++){
                basket.put(pax[i]+"Total", CommonUtility.convertStringToDouble(travellerTotal.get(i).getText()));
                basket.put(pax[i]+"Tax&Fees", CommonUtility.convertStringToDouble(taxAndFees.get(i+1).getText()));

                if(pax[i]!="Adult" && taxVerificationPerPax.length>0 && taxVerificationPerPax[0]) {
                    actionUtility.click(travellerTaxBreakdown.get(i));
                    taxVerificationPerPax(pax[i]);
                    actionUtility.click(travellerTaxBreakdown.get(i));
                }
            }
            basket.put("paxType",new Double(noOfPax));
            actionUtility.click(bookingDetails);
            reportLogger.log(LogStatus.INFO,"successfully retrieved fare details - "+basket);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to get fare details");
            throw e;
        }
        return basket;
    }

    private void taxVerificationPerPax(String pax){
        //verifies that APD is not present for teen,child and infant
        //verifies tax calculation //todo - tax verification per pax
        List<WebElement> tax = TestManager.getDriver().findElements(By.xpath(String.format(taxText,pax)));
        List<WebElement> taxAmount = TestManager.getDriver().findElements(By.xpath(String.format(taxValue,pax)));
        for(int i=0;i<tax.size();i++){
            try {
                Assert.assertFalse(tax.get(i).getText().contains("Air Passenger Duty"));
            } catch (AssertionError e) {
                reportLogger.log(LogStatus.INFO, "APD charges are applied for " + pax);
                throw e;
            }
        }
        reportLogger.log(LogStatus.INFO, "successfully verified that APD charges are not applied for "+ pax);

    }

    private Double getBasketPrice(){
        Double fare = 0.00;
        try{
            fare = CommonUtility.convertStringToDouble(totalAmount.getText());
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to retrieve total fare displayed in basket");
            throw e;
        }
        return fare;
    }

    public HashMap<String, Double> changeOutboundFlightNumber(HashMap<String,String> flightDetails, HashMap<String,String> testData, HashMap<String,Double> basket){
        //selects a outbound flight other than the flight number selected in booking flow
        try{
            actionUtility.waitForElementVisible(continueButton,90);
            WebElement flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(changeFlight,flightDetails.get("outboundFlightNo1").split(" ")[1]+flightDetails.get("outboundFlightNo1").split(" ")[2],testData.get("departTicketType").toUpperCase()))).get(0);
            actionUtility.hardClick(flightToBeSelected);

            Double changeFee = 0.0;
            int noOfPax = Integer.parseInt(testData.get("noOfAdult"));
            if(testData.get("noOfTeen")!=null){
                noOfPax += Integer.parseInt(testData.get("noOfTeen"));
            }
            if(testData.get("noOfChild")!=null){
                noOfPax += Integer.parseInt(testData.get("noOfChild"));
            }
            if(testData.get("outboundChangeFeeApplicable")!=null && testData.get("outboundChangeFeeApplicable").equalsIgnoreCase("yes")){
                int noOfSectors = Integer.parseInt(testData.get("departNoOfStops"))+1;
                changeFee = 45.00*noOfPax*noOfSectors;
            }
            if(testData.get("inboundChangeFeeApplicable")!=null && testData.get("inboundChangeFeeApplicable").equalsIgnoreCase("yes")) {
                int noOfSectors = Integer.parseInt(testData.get("returnNoOfStops"))+1;
                changeFee += (45.00*noOfPax*noOfSectors);
            }
            basket.put("change fee",changeFee);
            reportLogger.log(LogStatus.INFO,"successfully selected a flight other than "+flightDetails.get("outboundFlightNo1"));
            return basket;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to change flight on MB fare select page");
            throw e;
        }
    }

    public HashMap<String,Double> selectFlight(HashMap<String,String> testData,Boolean inboundSelection,HashMap<String,Double>... basket){
        //selects both departure and return flight - direct,1 stop and 2 stops
        /*Double inboundFare = 0.00;
        Double outboundFare = 0.00;
        */try{
            actionUtility.waitForElementVisible(continueButton,90);
            String noOfStops = getDepartNoOfStops(testData);
            String ticketType = getDepartTicketType(testData);
            if(testData.get("mb_DepartOperatorName")!=null || testData.get("departOperatorName")!=null){
                selectOtherOperatorFlight(testData,noOfStops,ticketType);
            }
            else {
                if (noOfStops.equals("0")) {
                    /*outboundFare = */selectDirectFlight(testData,ticketType);
                } else if (noOfStops.equals("1")) {
                    /*outboundFare = */selectOneStopFlight(testData,ticketType);
                } else if (noOfStops.equals("2")) {
                    /*outboundFare = */selectTwoStopFlight(testData,ticketType);
                }
            }
            if(inboundSelection){
                actionUtility.hardSleep(2000);
                noOfStops = getReturnNoOfStops(testData);
                ticketType = getReturnTicketType(testData);
                if(testData.get("mb_ReturnOperatorName")!=null || testData.get("returnOperatorName")!=null ){
                    /*inboundFare = */selectOtherOperatorFlight(testData,noOfStops,ticketType,"inbound");
                }
                else {
                    if (noOfStops.equals("0")) {
                        /*inboundFare = */selectDirectFlight(testData,ticketType,"inbound");
                    } else if (noOfStops.equals("1")) {
                        /*inboundFare = */selectOneStopFlight(testData,ticketType,"inbound");
                    } else if (noOfStops.equals("2")) {
                        /*inboundFare = */selectTwoStopFlight(testData,ticketType,"inbound");
                    }
                }
            }
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select flight on MB fare select page");
            throw e;
        }
        if(basket.length>0) {
            Double changeFee = 0.0;
            int noOfPax = Integer.parseInt(testData.get("noOfAdult"));
            if(testData.get("noOfTeen")!=null){
                noOfPax += Integer.parseInt(testData.get("noOfTeen"));
            }
            if(testData.get("noOfChild")!=null){
                noOfPax += Integer.parseInt(testData.get("noOfChild"));
            }
            if(testData.get("outboundChangeFeeApplicable")!=null && testData.get("outboundChangeFeeApplicable").equalsIgnoreCase("yes")){
                int noOfSectors = Integer.parseInt(testData.get("departNoOfStops"))+1;
                changeFee = 45.00*noOfPax*noOfSectors;
            }
            if(testData.get("inboundChangeFeeApplicable")!=null && testData.get("inboundChangeFeeApplicable").equalsIgnoreCase("yes")) {
                int noOfSectors = Integer.parseInt(testData.get("returnNoOfStops"))+1;
                changeFee += (45.00*noOfPax*noOfSectors);
            }
            basket[0].put("change fee",changeFee);
            return basket[0];
        }
        return null;
    }

    private void selectDirectFlight(HashMap<String,String> testData,String... ticketType_inbound){
        //selects direct flight for ticket type specified in testdata
        /*Double fareSelected = 0.00;*/
        try{
            if(ticketType_inbound.length==2 && ticketType_inbound[1].equals("inbound")){
                if (ticketType_inbound[0].equalsIgnoreCase("just fly")) {
                    if(testData.get("flex")!=null && testData.get("flex").equalsIgnoreCase("yes")){
                        actionUtility.hardClick(directJustFlyFlex.get(directJustFlyFlex.size()-1));
                    }
                    else {
                        actionUtility.hardClick(directJustFly.get(directJustFly.size() - 1));
                    }
                    reportLogger.log(LogStatus.INFO, "successfully selected inbound Just Fly Direct flight");
                } else if (ticketType_inbound[0].equalsIgnoreCase("get more")) {
                    actionUtility.hardClick(directGetMore.get(directGetMore.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully selected inbound Get More Direct flight");
                } else {
                    actionUtility.hardClick(directAllIn.get(directAllIn.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully selected inbound All In Direct flight");
                }
                /*fareSelected = */CommonUtility.convertStringToDouble(selectedFlightPrice.get(1).getText());
            }else {
                if (ticketType_inbound[0].equalsIgnoreCase("just fly")) {
                    if(testData.get("flex")!=null && testData.get("flex").equalsIgnoreCase("yes")){
                        actionUtility.hardClick(directJustFlyFlex.get(0));
                    }
                    else {
                        actionUtility.click(directJustFly.get(0));
                    }
                    reportLogger.log(LogStatus.INFO, "successfully selected outbound Just Fly Direct flight");
                } else if (ticketType_inbound[0].equalsIgnoreCase("get more")) {
                    actionUtility.click(directGetMore.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully selected outbound Get More Direct flight");
                } else {
                    actionUtility.click(directAllIn.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully selected outbound All In Direct flight");
                }
                /*fareSelected = */CommonUtility.convertStringToDouble(selectedFlightPrice.get(0).getText());
            }
            actionUtility.hardSleep(2000);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select direct flight");
            throw e;
        }
        /*return fareSelected;*/
    }

    private void selectOneStopFlight(HashMap<String,String> testData, String... ticketType_inbound){
        //selects 1 Stop flight for ticket type specified in testdata
        /*Double fareSelected = 0.0;*/
        try{
            if(ticketType_inbound.length==2 && ticketType_inbound[1].equals("inbound")){
                if (ticketType_inbound[0].equalsIgnoreCase("just fly")) {
                    if(testData.get("flex")!=null && testData.get("flex").equalsIgnoreCase("yes")){
                        actionUtility.hardClick(oneStopJustFly.get(oneStopJustFly.size()-1));
                    }
                    else {
                        actionUtility.click(oneStopJustFly.get(oneStopJustFly.size() - 1));
                    }
                    reportLogger.log(LogStatus.INFO, "successfully selected inbound Just Fly 1 stop flight");
                } else if (ticketType_inbound[0].equalsIgnoreCase("get more")) {
                    actionUtility.click(oneStopGetMore.get(oneStopGetMore.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully selected inbound Get More 1 stop flight");
                } else {
                    actionUtility.click(oneStopAllIn.get(oneStopAllIn.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully selected inbound All In 1 stop flight");
                }
                //fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(1).getText());
            }else {
                if (ticketType_inbound[0].equalsIgnoreCase("just fly")) {
                    if(testData.get("flex")!=null && testData.get("flex").equalsIgnoreCase("yes")){
                        actionUtility.hardClick(oneStopJustFly.get(0));
                    }
                    else {
                        actionUtility.click(oneStopJustFly.get(0));
                    }
                    reportLogger.log(LogStatus.INFO, "successfully selected outbound Just Fly 1 stop flight");
                } else if (ticketType_inbound[0].equalsIgnoreCase("get more")) {
                    actionUtility.click(oneStopGetMore.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully selected outbound Get More 1 stop flight");
                } else {
                    actionUtility.click(oneStopAllIn.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully selected outbound All In 1 stop flight");
                }
                //fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(0).getText());
            }
            actionUtility.hardSleep(2000);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select 1 stop flight");
            throw e;
        }
        /*return fareSelected;*/
    }

    private void selectTwoStopFlight(HashMap<String,String> testData, String... ticketType_inbound){
        //selects 2 stops flight for ticket type specified in testdata
        /*Double fareSelected = 0.00;*/
        try{
            if(ticketType_inbound.length==2 && ticketType_inbound[1].equals("inbound")){
                if (ticketType_inbound[0].equalsIgnoreCase("just fly")) {
                    if(testData.get("flex")!=null && testData.get("flex").equalsIgnoreCase("yes")){
                        actionUtility.hardClick(twoStopJustFly.get(twoStopJustFly.size()-1));
                    }
                    else {
                        actionUtility.click(twoStopJustFly.get(twoStopJustFly.size() - 1));
                    }
                    reportLogger.log(LogStatus.INFO, "successfully inbound selected Just Fly 2 stops flight");
                } else if (ticketType_inbound[0].equalsIgnoreCase("get more")) {
                    actionUtility.click(twoStopGetMore.get(twoStopGetMore.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully inbound selected Get More 2 Stops flight");
                } else {
                    actionUtility.click(twoStopAllIn.get(twoStopAllIn.size()-1));
                    reportLogger.log(LogStatus.INFO, "successfully inbound selected All In 2 stops flight");
                }
                //fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(1).getText());
            }
            else {
                if (ticketType_inbound[0].equalsIgnoreCase("just fly")) {
                    if(testData.get("flex")!=null && testData.get("flex").equalsIgnoreCase("yes")){
                        actionUtility.hardClick(twoStopJustFly.get(0));
                    }
                    else {
                        actionUtility.click(twoStopJustFly.get(0));
                    }
                    reportLogger.log(LogStatus.INFO, "successfully outbound selected Just Fly 2 stops flight");
                } else if (ticketType_inbound[0].equalsIgnoreCase("get more")) {
                    actionUtility.click(twoStopGetMore.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully outbound selected Get More 2 Stops flight");
                } else {
                    actionUtility.click(twoStopAllIn.get(0));
                    reportLogger.log(LogStatus.INFO, "successfully outbound selected All In 2 stops flight");
                }
                //fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(1).getText());
            }
            actionUtility.hardSleep(2000);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select 2 stops flight");
            throw e;
        }
        /*return fareSelected;*/
    }

    private String getReturnTicketType(HashMap<String,String> testData){
        String ticketType = "returnTicketType";
        if(testData.get("mb_ReturnTicketType")!=null){
            ticketType = "mb_ReturnTicketType";
        }
        return testData.get(ticketType);
    }

    private String getDepartTicketType(HashMap<String,String> testData){
        String ticketType = "departTicketType";
        if(testData.get("mb_DepartTicketType")!=null){
            ticketType = "mb_DepartTicketType";
        }
        return testData.get(ticketType);
    }

    private String getReturnNoOfStops(HashMap<String,String> testData){
        String no = "returnNoOfStops";
        if(testData.get("mb_ReturnNoOfStops")!=null){
            no = "mb_ReturnNoOfStops";
        }
        return testData.get(no);
    }

    private String getDepartNoOfStops(HashMap<String,String> testData){
        String no = "departNoOfStops";
        if(testData.get("mb_DepartNoOfStops")!=null){
            no = "mb_DepartNoOfStops";
        }
        return testData.get(no);
    }
    private void selectOtherOperatorFlight(HashMap<String,String> testData,String... noOfStops_ticketType_inbound){
        //select other operators flights specified in testData and also calculates and returns total fare
        /*Double fareSelected = 0.00;*/
        try{
            if(noOfStops_ticketType_inbound.length==3 && noOfStops_ticketType_inbound[2].equals("inbound")){
                if (noOfStops_ticketType_inbound[0].equals("0")) {
                    if (noOfStops_ticketType_inbound[1].equalsIgnoreCase("just fly")) {
                        List<WebElement> directJustFlyOperator = TestManager.getDriver().findElements(By.xpath(String.format(directOtherOperatorJustFly, testData.get("mb_ReturnOperatorName"))));
                        actionUtility.click(directJustFlyOperator.get(directJustFlyOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected Just Fly direct " + testData.get("mb_ReturnOperatorName") + " inbound flight");
                    } else if (noOfStops_ticketType_inbound[1].equalsIgnoreCase("get more")) {
                        List<WebElement> directGetMoreOperator = TestManager.getDriver().findElements(By.xpath(String.format(directOtherOperatorGetMore, testData.get("mb_ReturnOperatorName"))));
                        actionUtility.click(directGetMoreOperator.get(directGetMoreOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected Get More direct " + testData.get("mb_ReturnOperatorName") + " inbound flight");
                    } else if (noOfStops_ticketType_inbound[1].equalsIgnoreCase("all in")) {
                        List<WebElement> directAllInOperator = TestManager.getDriver().findElements(By.xpath(String.format(directOtherOperatorAllIn, testData.get("mb_ReturnOperatorName"))));
                        actionUtility.click(directAllInOperator.get(directAllInOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected All In direct " + testData.get("mb_ReturnOperatorName") + " inbound flight");
                    }
                }
                if (noOfStops_ticketType_inbound[0].equals("1")) {
                    if (noOfStops_ticketType_inbound[1].equalsIgnoreCase("just fly")) {
                        List<WebElement> oneStopJustFlyOperator = TestManager.getDriver().findElements(By.xpath(String.format(oneStopOtherOperatorJustFly, testData.get("mb_ReturnOperatorName"))));
                        actionUtility.click(oneStopJustFlyOperator.get(oneStopJustFlyOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected Just Fly 1 stop " + testData.get("mb_ReturnOperatorName") + " inbound flight");
                    } else if (noOfStops_ticketType_inbound[1].equalsIgnoreCase("get more")) {
                        List<WebElement> oneStopGetMoreOperator = TestManager.getDriver().findElements(By.xpath(String.format(oneStopOtherOperatorGetMore, testData.get("mb_ReturnOperatorName"))));
                        actionUtility.click(oneStopGetMoreOperator.get(oneStopGetMoreOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected Get More 1 stop " + testData.get("mb_ReturnOperatorName") + " inbound flight");
                    } else if (noOfStops_ticketType_inbound[1].equalsIgnoreCase("all in")) {
                        List<WebElement> oneStopAllInOperator = TestManager.getDriver().findElements(By.xpath(String.format(oneStopOtherOperatorAllIn, testData.get("mb_ReturnOperatorName"))));
                        actionUtility.click(oneStopAllInOperator.get(oneStopAllInOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected All In 1 stop " + testData.get("mb_ReturnOperatorName") + " inbound flight");
                    }

                }
                if (noOfStops_ticketType_inbound[0].equals("2")) {
                    if (noOfStops_ticketType_inbound[1].equalsIgnoreCase("just fly")) {
                        List<WebElement> twoStopJustFlyOperator = TestManager.getDriver().findElements(By.xpath(String.format(twoStopOtherOperatorJustFly, testData.get("mb_ReturnOperatorName"))));
                        actionUtility.click(twoStopJustFlyOperator.get(twoStopJustFlyOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected Just Fly 2 stop " + testData.get("mb_ReturnOperatorName") + " inbound flight");
                    } else if (noOfStops_ticketType_inbound[1].equalsIgnoreCase("get more")) {
                        List<WebElement> twoStopGetMoreOperator = TestManager.getDriver().findElements(By.xpath(String.format(twoStopOtherOperatorGetMore, testData.get("mb_ReturnOperatorName"))));
                        actionUtility.click(twoStopGetMoreOperator.get(twoStopGetMoreOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected Get More 2 stop " + testData.get("mb_ReturnOperatorName") + " inbound flight");
                    } else if (noOfStops_ticketType_inbound[1].equalsIgnoreCase("all in")) {
                        List<WebElement> twoStopAllInOperator = TestManager.getDriver().findElements(By.xpath(String.format(twoStopOtherOperatorAllIn, testData.get("mb_ReturnOperatorName"))));
                        actionUtility.click(twoStopAllInOperator.get(twoStopAllInOperator.size()-1));
                        reportLogger.log(LogStatus.INFO, "successfully selected All In 2 stop " + testData.get("mb_ReturnOperatorName") + " inbound flight");
                    }
                }
                /*fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(1).getText());*/
            }else {
                WebElement flightToBeSelected;
                if (noOfStops_ticketType_inbound[0].equals("0")) {
                    if (noOfStops_ticketType_inbound[1].equalsIgnoreCase("just fly")) {
                        flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(directOtherOperatorJustFly, testData.get("mb_DepartOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected just fly direct " + testData.get("mb_DepartOperatorName") + " outbound flight");
                    } else if (noOfStops_ticketType_inbound[1].equalsIgnoreCase("get more")) {
                        flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(directOtherOperatorGetMore, testData.get("mb_DepartOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected get more direct " + testData.get("mb_DepartOperatorName") + " outbound flight");
                    } else if (noOfStops_ticketType_inbound[1].equalsIgnoreCase("all in")) {
                        flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(directOtherOperatorAllIn, testData.get("mb_DepartOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected all in direct " + testData.get("mb_DepartOperatorName") + " outbound flight");
                    }
                }
                if (noOfStops_ticketType_inbound[0].equals("1")) {
                    if (noOfStops_ticketType_inbound[1].equalsIgnoreCase("just fly")) {
                        flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(oneStopOtherOperatorJustFly, testData.get("mb_DepartOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected just fly 1 stop " + testData.get("mb_DepartOperatorName") + " outbound flight");
                    } else if (noOfStops_ticketType_inbound[1].equalsIgnoreCase("get more")) {
                        flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(oneStopOtherOperatorGetMore, testData.get("mb_DepartOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected get more 1 stop " + testData.get("mb_DepartOperatorName") + " outbound flight");
                    } else if (noOfStops_ticketType_inbound[1].equalsIgnoreCase("all in")) {
                        flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(oneStopOtherOperatorAllIn, testData.get("mb_DepartOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected all in 1 stop " + testData.get("mb_DepartOperatorName") + " outbound flight");
                    }
                }
                if (noOfStops_ticketType_inbound[2].equals("2")) {
                    if (noOfStops_ticketType_inbound[1].equalsIgnoreCase("just fly")) {
                        flightToBeSelected = TestManager.getDriver().findElements(By.xpath(String.format(twoStopOtherOperatorJustFly, testData.get("mb_DepartOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected just fly 2 stop " + testData.get("mb_DepartOperatorName") + " outbound flight");
                    } else if (noOfStops_ticketType_inbound[1].equalsIgnoreCase("get more")) {
                        flightToBeSelected =TestManager.getDriver().findElements(By.xpath(String.format(twoStopOtherOperatorGetMore, testData.get("mb_DepartOperatorName")))).get(0);
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected get more 2 stop " + testData.get("mb_DepartOperatorName") + " outbound flight");
                    } else if (noOfStops_ticketType_inbound[1].equalsIgnoreCase("all in")) {
                        flightToBeSelected = (TestManager.getDriver().findElements(By.xpath(String.format(twoStopOtherOperatorAllIn, testData.get("mb_DepartOperatorName")))).get(0));
                        actionUtility.click(flightToBeSelected);
                        reportLogger.log(LogStatus.INFO, "successfully selected all in 2 stop " + testData.get("mb_DepartOperatorName") + " outbound flight");
                    }
                }
                /*fareSelected = CommonUtility.convertStringToDouble(selectedFlightPrice.get(0).getText());*/
            }
            actionUtility.hardSleep(2000);
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to select other operator flight");
            throw e;
        }
        /*return fareSelected;*/
    }

    public void verifyRoute(HashMap<String,String> testData){
        try {
            actionUtility.waitForElementVisible(continueButton,90);
            if (testData.get("changeDeparture") != null) {
                Assert.assertTrue(route.get(0).getText().toLowerCase().contains(testData.get("changeDeparture").toLowerCase()));
            }
            reportLogger.log(LogStatus.INFO,"source ["+route.get(0).getText()+"] on fare select matches with["+testData.get("changeDeparture").toUpperCase()+"]");
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"source ["+route.get(0).getText()+"] on fare select do not match with ["+testData.get("changeDeparture").toUpperCase()+"]");
            throw e;
        }
        try{
            if (testData.get("changeDestination")!= null) {
                Assert.assertTrue(route.get(1).getText().toLowerCase().contains(testData.get("changeDestination").toLowerCase()));
                reportLogger.log(LogStatus.INFO,"Destination["+route.get(1).getText()+"] on fare select matches with ["+testData.get("changeDestination").toUpperCase()+"]");
            }
        }catch(AssertionError e){
            reportLogger.log(LogStatus.INFO,"Destination["+route.get(1).getText()+"] on fare select do not match with ["+testData.get("changeDestination").toUpperCase()+"]");
            throw e;
        }
    }

    public void navigateToFlightDetailsPage(){
        try{
            actionUtility.waitForElementVisible(continueButton, 30);
            actionUtility.hardSleep(200);
            actionUtility.hardClick(continueButton);
            reportLogger.log(LogStatus.INFO,"successfully navigated to flight Details page");
        }catch (Exception e){
            reportLogger.log(LogStatus.INFO,"unable to navigate to flight details page");
            throw e;
        }
    }

    public HashMap<String,Double> verifyBasket(HashMap<String,Double> basket,HashMap<String,String> testData){
        //verifies price difference,total to be paid and change fee
        String item=null;
        Double expected=0.0,actual=0.0;
        try{
            actionUtility.waitForElementNotPresent(loader,90);
            basket.put("price difference",CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(basketPrice,"Price difference"))).getText()));
            item = "change fee";
            expected = basket.get("change fee");
            if(expected==0.0){
                try{
                Assert.assertFalse(actionUtility.verifyIfElementIsDisplayed(String.format(basketPrice,"Change fee")));
                    reportLogger.log(LogStatus.INFO,"change fee is not displayed as its not applicable");
                }catch(AssertionError ae){
                    actual = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(basketPrice,"Change fee"))).getText());
                    reportLogger.log(LogStatus.INFO,item+" = "+actual+" is displayed even though "+item+" is not applicable");
                    throw ae;
                }
            }
            else {
                actual = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(basketPrice, "Change fee"))).getText());
                Assert.assertEquals(actual,expected);
                reportLogger.log(LogStatus.INFO,"successfully verified that a change fee - "+expected+" is applied");
            }

            item = "to be paid";
            expected = basket.get("change fee")+basket.get("price difference");
            actual = CommonUtility.convertStringToDouble(TestManager.getDriver().findElement(By.xpath(String.format(basketPrice,"to be paid"))).getText());
            Assert.assertEquals(actual,expected);
            basket.put("to be paid",actual);

            reportLogger.log(LogStatus.INFO,"successfully verified basket on manage booking fare select page");
        }catch(AssertionError ae){
            System.out.println(item+" = = "+expected+" ----- "+actual);
            reportLogger.log(LogStatus.INFO,item+" displayed in basket - "+actual+"doesn't match with expected "+item+" - "+expected);
            throw ae;
        }catch(Exception e){
            reportLogger.log(LogStatus.INFO,"unable to verify basket on manage booking fare select page");
            throw e;
        }
        return basket;
    }
}

