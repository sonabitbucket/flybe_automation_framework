package ui_automator;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.testng.TestNG;
import utilities.DataUtility;
import utilities.FailExecution;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.util.*;


public class Automator extends JFrame implements WindowListener{
    // Variables declaration
    private ButtonGroup testSuiteButtonGroup;
    private ButtonGroup parallelButtonGroup;
    private ButtonGroup ScheduleButtonGroup;
    private ButtonGroup moduleSuiteButtonGroup;
    private static JButton start;
    private ArrayList<JCheckBox> priorityList;
    private ArrayList<JCheckBox> moduleList;
    private ArrayList<JRadioButton> testSuiteList;
    private static JComboBox environmentField;
    private static JComboBox browserField;
    private JLabel jLabel1;
    private JLabel jLabel10;
    private JLabel jLabel11;
    private JLabel jLabel12;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel environmentLabel;
    private JLabel browserLabel;
    private JLabel jLabel7;
    private JLabel jLabel8;
    private JLabel moduleLabel;
    private JLabel priorityLabel;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel3;
    private JPanel jPanel4;
    private JPanel jPanel5;
    private JPanel jPanel6;
    private JPanel jPanel7;
    private JPanel jPanel8;
    private JRadioButton smokeTest;
    private JRadioButton regression;
    private JRadioButton module;
    private JRadioButton testSuite;
    private JRadioButton parallelYes;
    private JRadioButton parallelNo;
    private JRadioButton runNow;
    private JRadioButton scheduleRun;
    private JSeparator jSeparator1;
    private JSeparator jSeparator2;
    private static JTextField email;
    private JsonObject config;
    private Iterator iter;
    private Thread t;
    private static JProgressBar progressBar;
    public static int progressBarCounter = 0;
    public static int totalCasesSelected = 0;
    public static String estimatedTime = "";
    private DefaultMutableTreeNode modulesRootNode;
    private DefaultMutableTreeNode subModulesRootNode;
    private DefaultMutableTreeNode prioritiesRootNode;
    private DefaultMutableTreeNode subPrioritesRootNode;
    private JTree moduleTree;
    private JTree priorityTree;
    TestNG testng = new TestNG();
    private String allModules = "Modules";
    private String allPriorities = "Priorities";
    public static int totalFailedCases = 0;
    public static JCheckBox rerun;
    private String project = "efly";
    private FailExecution reRunCases;
    private Thread reRunCasesThread;
    // End of variables declaration

    public Automator() {
        initComponents();
    }

    private void initComponents() {
        testSuiteButtonGroup = new ButtonGroup();
        moduleSuiteButtonGroup = new ButtonGroup();
        parallelButtonGroup = new ButtonGroup();
        ScheduleButtonGroup = new ButtonGroup();
        jPanel1 = new JPanel();
        jLabel1 = new JLabel();
        jLabel2 = new JLabel();
        jSeparator2 = new JSeparator();
        environmentField = new JComboBox();
        jLabel3 = new JLabel();
        jLabel4 = new JLabel();
        environmentLabel = new JLabel();
        browserField = new JComboBox();
        browserLabel = new JLabel();
        start = new JButton();
        jPanel2 = new JPanel();
        module = new JRadioButton();
        jSeparator1 = new JSeparator();
        jPanel4 = new JPanel();
        jLabel8 = new JLabel();
        moduleLabel = new JLabel();
        priorityLabel = new JLabel();
        jPanel7 = new JPanel();
        jPanel3 = new JPanel();
        smokeTest = new JRadioButton();
        testSuite = new JRadioButton();
        regression = new JRadioButton();
        jPanel5 = new JPanel();
        jLabel10 = new JLabel();
        parallelYes = new JRadioButton();
        parallelNo = new JRadioButton();
        jLabel11 = new JLabel();
        jPanel6 = new JPanel();
        jLabel7 = new JLabel();
        email = new JTextField();
        jLabel12 = new JLabel();
        runNow = new JRadioButton();
        scheduleRun = new JRadioButton();
        config = new DataUtility().getJsonData("UIConfig.json");
        priorityList = new ArrayList<JCheckBox>();
        moduleList = new ArrayList<JCheckBox>();
        testSuiteList = new ArrayList<JRadioButton>();
        jPanel8 = new JPanel();
        t = null;
        reRunCasesThread = null;
        progressBar = new javax.swing.JProgressBar();
        reRunCases = new FailExecution();
        if(DataUtility.readConfig("project")!=null){
            project = DataUtility.readConfig("project");
        }
        rerun = new JCheckBox("Re-Run failed cases");
        rerun.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                moduleTestSuiteCheckboxItemStateChanged(evt);
            }
        });




        GroupLayout jPanel1Layout = new GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel1, GroupLayout.PREFERRED_SIZE, 166, GroupLayout.PREFERRED_SIZE)
                                .addGap(26, 26, 26)
                                .addComponent(jLabel2)
                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addComponent(jSeparator2, GroupLayout.Alignment.TRAILING)
        );
        jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGap(19, 19, 19)
                                                .addComponent(jLabel2))
                                        .addComponent(jLabel1, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jSeparator2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(41, Short.MAX_VALUE))
        );


        GroupLayout jPanel2Layout = new GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
                jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addComponent(module)
                                        .addComponent(moduleLabel)
                                        .addComponent(jPanel7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jPanel4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(12, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
                jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(module)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jPanel7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addGap(79, 79, 79)
                                .addComponent(moduleLabel)
                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addComponent(jSeparator1)
                        .addComponent(jPanel4, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

//        jPanel2.add(moduleLabel);


        GroupLayout jPanel3Layout = new GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
                jPanel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addComponent(testSuite)
                                        .addGroup(jPanel3Layout.createSequentialGroup()
                                                .addGap(21, 21, 21)
                                                .addGroup(jPanel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
//                            .addComponent(smokeTest)
                                                        .addComponent(jPanel8))))
                                .addContainerGap(35, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
                jPanel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(testSuite)
                                .addGap(14, 14, 14)
//                .addComponent(smokeTest)
//                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel8)
                                .addContainerGap(154, Short.MAX_VALUE))
        );

//        jPanel5.setBorder(BorderFactory.createEtchedBorder());



        GroupLayout jPanel5Layout = new GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
                jPanel5Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel10)
                                .addGap(67, 67, 67))
                        .addGroup(jPanel5Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(parallelYes)
                                .addGap(18, 18, 18)
                                .addComponent(parallelNo)
                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
                jPanel5Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel5Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel10)
                                .addGap(18, 18, 18)
                                .addGroup(jPanel5Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(parallelYes)
                                        .addComponent(parallelNo))
                                .addContainerGap(46, Short.MAX_VALUE))
        );



        GroupLayout jPanel6Layout = new GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
                jPanel6Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel6Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel7)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                                .addComponent(email, GroupLayout.PREFERRED_SIZE, 370, GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
                jPanel6Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel6Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel6Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel7)
                                        .addComponent(email, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(27, Short.MAX_VALUE))
        );


        JPanel startPanel = new JPanel(new FlowLayout());
        rerun.setVisible(false);
        startPanel.add(rerun);
        startPanel.add(start);
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, 786, Short.MAX_VALUE)
                                .addContainerGap())
                        .addGroup(layout.createSequentialGroup()
                                .addGap(24, 24, 24)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addComponent(environmentLabel)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(10, 10, 10)
                                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addComponent(jLabel3)
                                                                .addGap(27, 27, 27)
                                                                .addComponent(environmentField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                        .addComponent(jPanel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(89, 89, 89)
                                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addComponent(jLabel4)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(browserField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                        .addComponent(browserLabel, GroupLayout.PREFERRED_SIZE, 202, GroupLayout.PREFERRED_SIZE)))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(97, 97, 97)
                                                .addComponent(jPanel3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 75, Short.MAX_VALUE)
                                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                                        .addComponent(jPanel5, GroupLayout.PREFERRED_SIZE, 154, GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(runNow)
                                                        .addComponent(scheduleRun)
                                                        .addComponent(startPanel, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE))))
                                .addGap(39, 39, 39))
                        .addGroup(layout.createSequentialGroup()
                                .addGap(43, 43, 43)
                                .addComponent(jPanel6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel11)
                                .addContainerGap())
                        .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel12)
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, 121, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(environmentLabel)
                                        .addComponent(browserLabel))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(environmentField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel3)
                                        .addComponent(jLabel4)
                                        .addComponent(browserField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                                .addComponent(jPanel3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jPanel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(jPanel5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(runNow)
                                                .addGap(3, 3, 3)
                                                .addComponent(scheduleRun)
                                                .addGap(18, 18, 18)
                                                .addComponent(startPanel)))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(jPanel6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                .addGap(19, 19, 19))
                                        .addComponent(jLabel11))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel12, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10))
        );


        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
                jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel6Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                                .addComponent(email, javax.swing.GroupLayout.PREFERRED_SIZE, 370, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(94, 94, 94))
        );
        jPanel6Layout.setVerticalGroup(
                jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel6Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel7)
                                        .addComponent(email, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(22, Short.MAX_VALUE))
        );

        setTitle("Flybe Automation Controller");
        setBackground(new Color(102, 255, 0));
//        this.setLocation(0,0);
        setResizable(false);
        this.addWindowListener(this);
        jPanel1.setPreferredSize(new Dimension(684, 800));

        jLabel1.setIcon(new ImageIcon(getClass().getResource("/flybe_logo.png"))); // NOI18N
        jLabel1.setMaximumSize(new Dimension(50, 69));
        jLabel1.setMinimumSize(new Dimension(50, 69));
        jLabel1.setPreferredSize(new Dimension(100, 19));

        jLabel2.setFont(new Font("Trebuchet MS", 1, 30)); // NOI18N
        jLabel2.setHorizontalAlignment(SwingConstants.CENTER);
        jLabel2.setText("Flybe Automation Controller");

        initComboBox(environmentField, "environment");
        initComboBox(browserField, "browser");

        jLabel3.setText("Environment");
        jLabel4.setText("Browser");

        environmentLabel.setForeground(new Color(255, 0, 0));
        environmentLabel.setText(" ");

        browserLabel.setForeground(new Color(255, 0, 0));
        browserLabel.setText(" ");

        start.setText("Start");
        start.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startButtonActionPerformed(evt);
            }
        });

        jPanel2.setBorder(BorderFactory.createEtchedBorder());

        moduleSuiteButtonGroup.add(module);
        module.setSelected(true);
        module.setText("Module");
        module.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                moduleRadioButtonStateChanged(evt);
            }
        });
        module.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                moduleTestSuiteCheckboxItemStateChanged(evt);
            }
        });

        jSeparator1.setOrientation(SwingConstants.VERTICAL);

        jLabel8.setText("Priority");

        jPanel4.setLayout(new FlowLayout(FlowLayout.LEFT, 20, 10));

        jPanel4.add(jLabel8);

        jPanel4.setBorder(new EmptyBorder(new Insets(10, 10, 50, 50)));

        initModule();

        initPriority2();

        moduleLabel.setForeground(new Color(255, 0, 0));
        moduleLabel.setText(" ");
        priorityLabel.setForeground(new Color(255, 0, 0));
        priorityLabel.setText(" ");

        jPanel3.setBorder(BorderFactory.createEtchedBorder());

        moduleSuiteButtonGroup.add(testSuite);
        testSuite.setText("Test suite");
        testSuite.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                moduleTestSuiteCheckboxItemStateChanged(evt);
            }
        });

        initTestSuite();

        jPanel8.setLayout(new BoxLayout(jPanel8, BoxLayout.Y_AXIS));
        jPanel8.setBorder(new EmptyBorder(new Insets(10, 10, 50, 50)));
        for(int j=0;j<testSuiteList.size();j++){
            if (j==0) testSuiteList.get(j).setSelected(true);
            testSuiteList.get(j).setEnabled(false);
            testSuiteButtonGroup.add(testSuiteList.get(j));
            jPanel8.add(testSuiteList.get(j));
            jPanel8.add(Box.createRigidArea(new Dimension(0, 5)));
        }

        testSuite.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                testSuiteRadioButtonStateChanged(evt);
            }
        });

        jLabel10.setText("Parallel Execution");
        jLabel10.setVisible(false);

        parallelButtonGroup.add(parallelYes);
        parallelYes.setText("Yes");
        parallelYes.setEnabled(false);
        parallelYes.setVisible(false);

        parallelButtonGroup.add(parallelNo);
        parallelNo.setSelected(true);
        parallelNo.setText("No");
        parallelNo.setEnabled(false);
        parallelNo.setVisible(false);

        jLabel11.setIcon(new ImageIcon(getClass().getResource("/harman_logo.png"))); // NOI18N
        jLabel11.setText(" ");

        jLabel7.setText("Send Results To:");

//        email.setEditable(false);
//        email.setText("Enter your mail ID");
//        email.setEnabled(false);

        Font font = new Font("Times New Roman", Font.PLAIN,13);
        jLabel12.setFont(font);
        jLabel12.setText("Contact us - flybeqa@gmail.com");

        ScheduleButtonGroup.add(runNow);
        runNow.setSelected(true);
        runNow.setText("Run Now");
        runNow.setVisible(false);

        ScheduleButtonGroup.add(scheduleRun);
        scheduleRun.setText("Schedule Run");
        scheduleRun.setEnabled(false);
        scheduleRun.setVisible(false);

        progressBar.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        progressBar.setStringPainted(true);
        progressBar.setString("Cases selected - 0; Estimated time - N/A");

        UIDefaults defaults = new UIDefaults();
        defaults.put("ProgressBar[Enabled].foregroundPainter", new MyPainter(Color.GREEN,progressBar));
        defaults.put("ProgressBar[Enabled+Finished].foregroundPainter", new MyPainter(Color.GREEN,progressBar));

        progressBar.putClientProperty("Nimbus.Overrides.InheritDefaults", Boolean.TRUE);
        progressBar.putClientProperty("Nimbus.Overrides", defaults);
//        progressBar.setVisible(false);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void startButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startButtonActionPerformed
        // TODO add your handling code here:
        if((start.getText().equals("Start"))&&(rerun.isSelected())){
            totalCasesSelected = totalFailedCases;
            totalFailedCases = 0;
            progressBarCounter = 0;

            progressBar.setMinimum(progressBarCounter);
            progressBar.setMaximum(totalCasesSelected);
            progressBar.setStringPainted(true);
            progressBar.setString(progressBarCounter+" of "+totalCasesSelected);
            progressBar.setVisible(true);

            reRunCases.reRunFailedCases();
            reRunCasesThread = new Thread(reRunCases);
            reRunCasesThread.start();
            start.setText("Stop");

        }else if(start.getText().equals("Stop")){
            removeDriverServers();
        }else if(start.getText().equals("Pause")){
            try{
                JavaSuiteGenerator.testNG.wait();
                start.setText("Resume");
            }catch (Exception e){
                throw new RuntimeException("unable to suspend");
            }

        }else if(start.getText().equals("Resume")){
            JavaSuiteGenerator.testNG.notify();
            start.setText("Pause");
        }else{
            environmentLabel.setText(" ");
            browserLabel.setText(" ");
            moduleLabel.setText(" ");
            priorityLabel.setText(" ");
            if(environmentField.getSelectedIndex() == 0){
                environmentLabel.setText("Please select an environment");
            }
            if(browserField.getSelectedIndex() == 0){
                browserLabel.setText("Please select a browser");
            }
            if(module.isSelected()){
                CheckBoxStatusModules test = new CheckBoxStatusModules();
                boolean flag = test.ModuleCheckedOrNot(modulesRootNode);
                if(!flag){
                    moduleLabel.setText("Please select a module");
                }
                flag = test.PriorityCheckedOrNot(prioritiesRootNode);
                if(!flag){
                    priorityLabel.setText(" \n \n Please select a priority");
                }
            }
            if(!((environmentField.getSelectedIndex()==0)||(browserField.getSelectedIndex()==0))){
                if(runNow.isSelected()) {
                    if(module.isSelected() && start.getText().equals("Start")){
                        readSelectedModulesAndPriorities();
                    }
                    else if( testSuite.isSelected() && start.getText().equals("Start") ){
                        readSelectedTestSuite();
                    }
                }
            }
        }

    }//GEN-LAST:event_startButtonActionPerformed

    private void testSuiteRadioButtonStateChanged(javax.swing.event.ChangeEvent evt) {
        //GEN-FIRST:event_testSuiteRadioButtonStateChanged
//        If test suite radio button is selected then enable all the test suites and disable all the modules and priorities
        int totalCases = 0;
        if(testSuite.isSelected()){
            for(int i=0;i<testSuiteList.size();i++){
                testSuiteList.get(i).setEnabled(true);
            }
            priorityTree.setEditable(false);
            priorityTree.setEnabled(false);

            moduleTree.setEditable(false);
            moduleTree.setEnabled(false);
            int arr[] = {120,300};
            for(int i=0; i<testSuiteList.size();i++){
                if(testSuiteList.get(i).isSelected()){
                    totalCases += arr[i];
                }
            }

        }
    }//GEN-LAST:event_testSuiteRadioButtonStateChanged

    private void moduleRadioButtonStateChanged(javax.swing.event.ChangeEvent evt) {
        //GEN-FIRST:event_moduleRadioButtonStateChanged
//        If module is selected then enable all the modules, priorities and disable all the suites
        if(module.isSelected()){
            for(int i=0;i<testSuiteList.size();i++){
                testSuiteList.get(i).setEnabled(false);
            }

            priorityTree.setEditable(true);
            priorityTree.setEnabled(true);

            moduleTree.setEditable(true);
            moduleTree.setEnabled(true);
        }
    }//GEN-LAST:event_moduleRadioButtonStateChanged

    void moduleTestSuiteCheckboxItemStateChanged(java.awt.event.ItemEvent evt) {
//     this method is called when ever there is a change in the selection of modules or test suite

        if(start.getText().equals("Start")) {
            totalCasesSelected = 0;
            estimatedTime = "";
            if(rerun.isSelected()){
                totalCasesSelected = totalFailedCases;
                for(int i=0;i<totalCasesSelected;i++) {
                    estimatedTime = TimeTest.addTime(estimatedTime, "0:03");
                }
                progressBar.setStringPainted(true);
                progressBar.setString("Cases for re-run - "+totalCasesSelected+"; Estimated time - "+estimatedTime + " h:mm");
                progressBar.setVisible(true);
            }else if (module.isSelected()) {
                JsonObject countSelected = DataUtility.getJsonData("count.json");
                CheckBoxStatusModules modulesSelectedFromUI = new CheckBoxStatusModules();
//                ArrayList<String> modules = modulesSelectedFromUI.getModulesSelected(modulesRootNode);

                Hashtable<String,ArrayList<String>> modules = modulesSelectedFromUI.getModulesSelected(modulesRootNode);

                Enumeration<String> selectedModule = modules.keys();

                while(selectedModule.hasMoreElements()){
                    String str = selectedModule.nextElement();
                    System.out.println("sona1 "+str);
                    ArrayList<String> selMod = modules.get(str);
                    System.out.println("sona2 "+selMod);

                    for (int i = 0; i < selMod.size(); i++) {

//                    ArrayList<String> selectedModule = modules.keys();
                        System.out.println(selMod.get(i));
                        CheckBoxStatusModules prioritiesSelectedFromUI = new CheckBoxStatusModules();
//                        ArrayList<String> priority = prioritiesSelectedFromUI.getPrioritiesSelected(prioritiesRootNode);

                        Hashtable<String,ArrayList<String>> selPriorities = modulesSelectedFromUI.getPrioritiesSelected2(prioritiesRootNode);



                        if(selPriorities.contains(allPriorities)){
                            selPriorities = readAllPriorities2();
                        }
                        ArrayList<String> priority = readPriorityFromJson2(selPriorities);

                        JsonObject priorityCount = null;


                        for (int j = 0; j < priority.size(); j++) {
                            if(DataUtility.readConfig("flow")!= null && DataUtility.readConfig("flow").equals("online")){
                                System.out.println("sona 3"+selMod.get(i));
                                System.out.println("sona4 "+priority.get(j));
                                priorityCount = countSelected.getAsJsonObject("online").getAsJsonObject("modules").getAsJsonObject(str).getAsJsonObject(selMod.get(i)).getAsJsonObject(priority.get(j));
                            }else if(DataUtility.readConfig("flow")!= null && DataUtility.readConfig("flow").equals("offline")){
                                priorityCount = countSelected.getAsJsonObject("offline").getAsJsonObject("modules").getAsJsonObject(str).getAsJsonObject(priority.get(j));
                            }

                            totalCasesSelected += Integer.parseInt(priorityCount.get("count").getAsString());
                            estimatedTime = TimeTest.addTime(estimatedTime,priorityCount.get("time").getAsString());
                        }
                    }
                }
                progressBar.setStringPainted(true);
                if(estimatedTime .equals("")){
                    progressBar.setString("Cases selected - "+totalCasesSelected +"; Estimated time - N/A");
                }else{
                    progressBar.setString("Cases selected - "+totalCasesSelected +"; Estimated time - "+estimatedTime + " h:mm");
                }
                progressBar.setVisible(true);
//                progressBar.setVisible(false);
            } else if (testSuite.isSelected()) {
                for (int i = 0; i < testSuiteList.size(); i++) {
                    if (testSuiteList.get(i).isSelected()) {
                        String suiteSelected = readTestSuite(testSuiteList.get(i));
                        JsonObject countSelected = DataUtility.getJsonData("count.json");
                        if(DataUtility.readConfig("flow")!= null && DataUtility.readConfig("flow").equals("lf")){
                            totalCasesSelected = Integer.parseInt(countSelected.getAsJsonObject("lf3").getAsJsonObject("suite").getAsJsonObject(suiteSelected).get("count").getAsString());
                            estimatedTime = TimeTest.addTime(estimatedTime,countSelected.getAsJsonObject("lf3").getAsJsonObject("suite").getAsJsonObject(suiteSelected).get("time").getAsString());
                        }else if(DataUtility.readConfig("flow")!= null && DataUtility.readConfig("flow").equals("cam")){
                            totalCasesSelected = Integer.parseInt(countSelected.getAsJsonObject("lf2").getAsJsonObject("suite").getAsJsonObject(suiteSelected).get("count").getAsString());
                            estimatedTime = TimeTest.addTime(estimatedTime,countSelected.getAsJsonObject("lf2").getAsJsonObject("suite").getAsJsonObject(suiteSelected).get("time").getAsString());
                        }
                        break;
                    }
                }
                if (totalCasesSelected != 0) {
                    progressBar.setStringPainted(true);
                    progressBar.setString("Cases selected - "+totalCasesSelected+"; Estimated time - "+estimatedTime+" h:mm");
                    progressBar.setVisible(true);
//                    progressBar.setVisible(false);
                }
            }
        }
    }

    public static void main(String args[]) {
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Automator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Automator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Automator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Automator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        Automator auto = new Automator();
        auto.setVisible(true);
    }

    private void initComboBox(JComboBox box, String readObj){
//        this methods adds the values to the checkbox(initializing UI) which are read from json

        box.addItem("Select");
        if(readObj.toLowerCase().equals("environment")|readObj.toLowerCase().equals("browser")){
            Set<Map.Entry<String,JsonElement>> jsonSet = config.getAsJsonObject("UIConfig").getAsJsonObject(readObj.toLowerCase()).entrySet();
            iter = jsonSet.iterator();
            while(iter.hasNext()){
                Map.Entry jsonMap = (Map.Entry) iter.next();
                box.addItem(jsonMap.getKey().toString());
            }

        }
    }

    private String  readComboBoxData(JComboBox box, String readObj){
//        this method will return the value from json for what is being selected in UI
        JsonObject comboBoxObj = config.getAsJsonObject("UIConfig").getAsJsonObject(readObj.toLowerCase());
        return comboBoxObj.get(box.getSelectedItem().toString()).getAsString();
    }

    private void initPriority() {
//        this method will initialize the UI priorities in Tree structure from json
        Set<Map.Entry<String, JsonElement>> jsonSet = null;
        if (DataUtility.readConfig("flow") != null && DataUtility.readConfig("flow").equals("lf")) {
            jsonSet = config.getAsJsonObject("UIConfig").getAsJsonObject("priority").getAsJsonObject("lf").entrySet();
        } else if (DataUtility.readConfig("flow") != null && DataUtility.readConfig("flow").equals("cam")) {
            jsonSet = config.getAsJsonObject("UIConfig").getAsJsonObject("priority").getAsJsonObject("cam").entrySet();
        }
        iter = jsonSet.iterator();
        int i = 0;
        prioritiesRootNode = new DefaultMutableTreeNode(allPriorities);
        while (iter.hasNext()) {
            Map.Entry jsonMap = (Map.Entry) iter.next();
            prioritiesRootNode.add(new DefaultMutableTreeNode(jsonMap.getKey().toString()));
            i++;
        }

        Icon leafIcon = new ImageIcon(getClass().getResource("/blank_image.png"));
        Icon openIcon = new ImageIcon(getClass().getResource("/blank_image.png"));
        Icon closedIcon = new ImageIcon(getClass().getResource("/blank_image.png"));

        UIManager.put("Tree.leafIcon", leafIcon);
        UIManager.put("Tree.openIcon", openIcon);
        UIManager.put("Tree.closedIcon", closedIcon);

        priorityTree = new JTree(prioritiesRootNode) {
            @Override
            public void updateUI() {
                setCellRenderer(null);
                setCellEditor(null);
                super.updateUI();
                setCellRenderer(new CheckBoxNodeRenderer());
                setCellEditor(new CheckBoxNodeEditor());
            }
        };
        TreeModel priorirtyTreeModel = priorityTree.getModel();

        Enumeration e = prioritiesRootNode.breadthFirstEnumeration();
        while (e.hasMoreElements()) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.nextElement();
            node.setUserObject(new CheckBoxNode(Objects.toString(node.getUserObject(), ""), Status.DESELECTED));
        }
        priorirtyTreeModel.addTreeModelListener(new TreeModelListener() {
            private boolean adjusting;

            @Override
            public void treeNodesChanged(TreeModelEvent e) {
                if (adjusting) {
                    return;
                }
                adjusting = true;
                Object[] children = e.getChildren();
                DefaultTreeModel model = (DefaultTreeModel) e.getSource();

                DefaultMutableTreeNode node;
                CheckBoxNode c; // = (CheckBoxNode) node.getUserObject();
                if (Objects.nonNull(children) && children.length == 1) {
                    node = (DefaultMutableTreeNode) children[0];
                    c = (CheckBoxNode) node.getUserObject();
                    TreePath parent = e.getTreePath();
                    DefaultMutableTreeNode n = (DefaultMutableTreeNode) parent.getLastPathComponent();
                    while (Objects.nonNull(n)) {
                        updateParentUserObject(n);
                        DefaultMutableTreeNode tmp = (DefaultMutableTreeNode) n.getParent();
                        if (Objects.nonNull(tmp)) {
                            n = tmp;
                        } else {
                            break;
                        }
                    }
                    model.nodeChanged(n);
                } else {
                    node = (DefaultMutableTreeNode) model.getRoot();
                    c = (CheckBoxNode) node.getUserObject();
                }
                updateAllChildrenUserObject(node, c.status);
                model.nodeChanged(node);
                adjusting = false;
                java.awt.event.ItemEvent event = null;
                moduleTestSuiteCheckboxItemStateChanged(event);
            }

            private void updateParentUserObject(DefaultMutableTreeNode parent) {
                int selectedCount = 0;
                Enumeration children = parent.children();
                while (children.hasMoreElements()) {
                    DefaultMutableTreeNode node = (DefaultMutableTreeNode) children.nextElement();
                    CheckBoxNode check = (CheckBoxNode) node.getUserObject();
                    if (check.status == Status.INDETERMINATE) {
                        selectedCount = -1;
                        break;
                    }
                    if (check.status == Status.SELECTED) {
                        selectedCount++;
                    }
                }
                String label = ((CheckBoxNode) parent.getUserObject()).label;
                if (selectedCount == 0) {
                    parent.setUserObject(new CheckBoxNode(label, Status.DESELECTED));
                } else if (selectedCount == parent.getChildCount()) {
                    parent.setUserObject(new CheckBoxNode(label, Status.SELECTED));
                } else {
                    parent.setUserObject(new CheckBoxNode(label));
                }

            }

            private void updateAllChildrenUserObject(DefaultMutableTreeNode root, Status status) {
                Enumeration breadth = root.breadthFirstEnumeration();
                while (breadth.hasMoreElements()) {
                    DefaultMutableTreeNode node = (DefaultMutableTreeNode) breadth.nextElement();
                    if (Objects.equals(root, node)) {
                        continue;
                    }
                    CheckBoxNode check = (CheckBoxNode) node.getUserObject();
                    node.setUserObject(new CheckBoxNode(check.label, status));
                }
            }

            @Override
            public void treeNodesInserted(TreeModelEvent e) { /* not needed */ }

            @Override
            public void treeNodesRemoved(TreeModelEvent e) { /* not needed */ }

            @Override
            public void treeStructureChanged(TreeModelEvent e) { /* not needed */ }
        });
        priorityTree.setEditable(true);
        priorityTree.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
//        priorityTree.expandRow(0);
        JScrollPane sp = new JScrollPane(priorityTree);
        sp.setSize(10, 10);
        jPanel4.setLayout(new BorderLayout());
        JPanel subPane = new JPanel(new FlowLayout(FlowLayout.LEFT));
        subPane.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
        subPane.add(jLabel8);

        JPanel subPanel = new JPanel();
        subPanel.setLayout(new BorderLayout());
        subPanel.add(subPane, BorderLayout.NORTH);
        subPanel.add(sp, BorderLayout.CENTER);
        jPanel4.add(subPanel, BorderLayout.NORTH);

        JPanel pane = new JPanel(new BorderLayout(5, 80));
        pane.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
        pane.add(priorityLabel, BorderLayout.SOUTH);
        jPanel4.add(pane, BorderLayout.SOUTH);
    }

    private void initModule(){
//        this method will initialize the UI modules in tree structure from json
        Set<Map.Entry<String,JsonElement>> jsonSet = null;
        Set<Map.Entry<String,JsonElement>> jsonSetForSubModules = null;
        if(DataUtility.readConfig("flow")!= null && DataUtility.readConfig("flow").equals("online")){
            jsonSet = config.getAsJsonObject("UIConfig").getAsJsonObject("module").getAsJsonObject(project).getAsJsonObject("online").entrySet();
        }else if(DataUtility.readConfig("flow")!= null && DataUtility.readConfig("flow").equals("offline")){
            jsonSet = config.getAsJsonObject("UIConfig").getAsJsonObject("module").getAsJsonObject(project).getAsJsonObject("offline").entrySet();
        }
        iter = jsonSet.iterator();

        modulesRootNode = new DefaultMutableTreeNode(allModules);

        while(iter.hasNext()){
            Map.Entry jsonMap = (Map.Entry) iter.next();
            subModulesRootNode = new DefaultMutableTreeNode(jsonMap.getKey().toString());
//            modulesRootNode.add(new DefaultMutableTreeNode(jsonMap.getKey().toString()));
            if(DataUtility.readConfig("flow")!= null && DataUtility.readConfig("flow").equals("online")){
                jsonSetForSubModules = config.getAsJsonObject("UIConfig").getAsJsonObject("module").getAsJsonObject(project).getAsJsonObject("online").getAsJsonObject(jsonMap.getKey().toString()).entrySet();
            }else if(DataUtility.readConfig("flow")!= null && DataUtility.readConfig("flow").equals("offline")){
                jsonSetForSubModules = config.getAsJsonObject("UIConfig").getAsJsonObject("module").getAsJsonObject(project).getAsJsonObject("offline").getAsJsonObject(jsonMap.getKey().toString()).entrySet();
            }
            Iterator subModulesIterator = jsonSetForSubModules.iterator();

            while(subModulesIterator.hasNext()){
                Map.Entry jsonMapForSubModules = (Map.Entry) subModulesIterator.next();
                subModulesRootNode.add(new DefaultMutableTreeNode(jsonMapForSubModules.getKey().toString()));

            }
            modulesRootNode.add(subModulesRootNode);
        }



        Icon leafIcon = new ImageIcon(getClass().getResource("/blank_image.png"));
        Icon openIcon = new ImageIcon(getClass().getResource("/blank_image.png"));
        Icon closedIcon = new ImageIcon(getClass().getResource("/blank_image.png"));

        UIManager.put("Tree.leafIcon", leafIcon);
        UIManager.put("Tree.openIcon", openIcon);
        UIManager.put("Tree.closedIcon", closedIcon);

        moduleTree = new JTree(modulesRootNode){
            @Override public void updateUI() {
                setCellRenderer(null);
                setCellEditor(null);
                super.updateUI();
                setCellRenderer(new CheckBoxNodeRenderer());
                setCellEditor(new CheckBoxNodeEditor());
            }
        };
        TreeModel moduleTreeModel = moduleTree.getModel();

        Enumeration e = modulesRootNode.breadthFirstEnumeration();
        while (e.hasMoreElements()) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.nextElement();
            node.setUserObject(new CheckBoxNode(Objects.toString(node.getUserObject(), ""), Status.DESELECTED));
        }
//        model.addTreeModelListener(new CheckBoxStatusModules());
        moduleTreeModel.addTreeModelListener(new TreeModelListener(){
            private boolean adjusting;
            @Override public void treeNodesChanged(TreeModelEvent e) {
                if (adjusting) {
                    return;
                }
                adjusting = true;
                Object[] children = e.getChildren();
                DefaultTreeModel model = (DefaultTreeModel) e.getSource();

                DefaultMutableTreeNode node;
                CheckBoxNode c; // = (CheckBoxNode) node.getUserObject();
                if (Objects.nonNull(children) && children.length == 1) {
                    node = (DefaultMutableTreeNode) children[0];
                    c = (CheckBoxNode) node.getUserObject();
                    TreePath parent = e.getTreePath();
                    DefaultMutableTreeNode n = (DefaultMutableTreeNode) parent.getLastPathComponent();
                    while (Objects.nonNull(n)) {
                        updateParentUserObject(n);
                        DefaultMutableTreeNode tmp = (DefaultMutableTreeNode) n.getParent();
                        if (Objects.nonNull(tmp)) {
                            n = tmp;
                        } else {
                            break;
                        }
                    }
                    model.nodeChanged(n);
                } else {
                    node = (DefaultMutableTreeNode) model.getRoot();
                    c = (CheckBoxNode) node.getUserObject();
                }
                updateAllChildrenUserObject(node, c.status);
                model.nodeChanged(node);
                adjusting = false;
                java.awt.event.ItemEvent event=null;
                moduleTestSuiteCheckboxItemStateChanged(event);
            }
            private void updateParentUserObject(DefaultMutableTreeNode parent) {
                int selectedCount = 0;
                Enumeration children = parent.children();
                while (children.hasMoreElements()) {
                    DefaultMutableTreeNode node = (DefaultMutableTreeNode) children.nextElement();
                    CheckBoxNode check = (CheckBoxNode) node.getUserObject();
                    if (check.status == Status.INDETERMINATE) {
                        selectedCount = -1;
                        break;
                    }
                    if (check.status == Status.SELECTED) {
                        selectedCount++;
                    }
                }
                String label = ((CheckBoxNode) parent.getUserObject()).label;
                if (selectedCount == 0) {
                    parent.setUserObject(new CheckBoxNode(label, Status.DESELECTED));
                } else if (selectedCount == parent.getChildCount()) {
                    parent.setUserObject(new CheckBoxNode(label, Status.SELECTED));
                } else {
                    parent.setUserObject(new CheckBoxNode(label));
                }
            }
            private void updateAllChildrenUserObject(DefaultMutableTreeNode root, Status status) {
                Enumeration breadth = root.breadthFirstEnumeration();
                while (breadth.hasMoreElements()) {
                    DefaultMutableTreeNode node = (DefaultMutableTreeNode) breadth.nextElement();
                    if (Objects.equals(root, node)) {
                        continue;
                    }
                    CheckBoxNode check = (CheckBoxNode) node.getUserObject();
                    node.setUserObject(new CheckBoxNode(check.label, status));
                }
            }
            @Override public void treeNodesInserted(TreeModelEvent e)    { /* not needed */ }
            @Override public void treeNodesRemoved(TreeModelEvent e)     { /* not needed */ }
            @Override public void treeStructureChanged(TreeModelEvent e) { /* not needed */ }
        });
        moduleTree.setEditable(true);
        moduleTree.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
        JScrollPane sp = new JScrollPane(moduleTree);
        jPanel7.add(sp);
    }

    private void initPriority2(){
//        this method will initialize the UI modules in tree structure from json
        Set<Map.Entry<String,JsonElement>> jsonSet = null;
        Set<Map.Entry<String,JsonElement>> jsonSetForSubPriorities = null;
        if(DataUtility.readConfig("flow")!= null && DataUtility.readConfig("flow").equals("online")){
            jsonSet = config.getAsJsonObject("UIConfig").getAsJsonObject("priority").getAsJsonObject("online").entrySet();
        }else if(DataUtility.readConfig("flow")!= null && DataUtility.readConfig("flow").equals("offline")){
            jsonSet = config.getAsJsonObject("UIConfig").getAsJsonObject("priority").getAsJsonObject("offline").entrySet();
        }
        iter = jsonSet.iterator();

        prioritiesRootNode = new DefaultMutableTreeNode(allPriorities);

        while(iter.hasNext()){
            Map.Entry jsonMap = (Map.Entry) iter.next();
            subPrioritesRootNode = new DefaultMutableTreeNode(jsonMap.getKey().toString());
//            prioritiesRootNode.add(new DefaultMutableTreeNode(jsonMap.getKey().toString()));
            if(DataUtility.readConfig("flow")!= null && DataUtility.readConfig("flow").equals("online")){
                jsonSetForSubPriorities = config.getAsJsonObject("UIConfig").getAsJsonObject("priority").getAsJsonObject("online").getAsJsonObject(jsonMap.getKey().toString()).entrySet();
            }else if(DataUtility.readConfig("flow")!= null && DataUtility.readConfig("flow").equals("offline")){
                jsonSetForSubPriorities = config.getAsJsonObject("UIConfig").getAsJsonObject("priority").getAsJsonObject("offline").getAsJsonObject(jsonMap.getKey().toString()).entrySet();
            }
            Iterator subPrioritiesIterator = jsonSetForSubPriorities.iterator();

            while(subPrioritiesIterator.hasNext()){
                Map.Entry jsonMapForSubModules = (Map.Entry) subPrioritiesIterator.next();
                subPrioritesRootNode.add(new DefaultMutableTreeNode(jsonMapForSubModules.getKey().toString()));

            }
            prioritiesRootNode.add(subPrioritesRootNode);
        }



        Icon leafIcon = new ImageIcon(getClass().getResource("/blank_image.png"));
        Icon openIcon = new ImageIcon(getClass().getResource("/blank_image.png"));
        Icon closedIcon = new ImageIcon(getClass().getResource("/blank_image.png"));

        UIManager.put("Tree.leafIcon", leafIcon);
        UIManager.put("Tree.openIcon", openIcon);
        UIManager.put("Tree.closedIcon", closedIcon);

        priorityTree = new JTree(prioritiesRootNode){
            @Override public void updateUI() {
                setCellRenderer(null);
                setCellEditor(null);
                super.updateUI();
                setCellRenderer(new CheckBoxNodeRenderer());
                setCellEditor(new CheckBoxNodeEditor());
            }
        };
        TreeModel priorirtyTreeModel = priorityTree.getModel();

        Enumeration e = prioritiesRootNode.breadthFirstEnumeration();
        while (e.hasMoreElements()) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.nextElement();
            node.setUserObject(new CheckBoxNode(Objects.toString(node.getUserObject(), ""), Status.DESELECTED));
        }
//        model.addTreeModelListener(new CheckBoxStatusModules());
        priorirtyTreeModel.addTreeModelListener(new TreeModelListener(){
            private boolean adjusting;
            @Override public void treeNodesChanged(TreeModelEvent e) {
                if (adjusting) {
                    return;
                }
                adjusting = true;
                Object[] children = e.getChildren();
                DefaultTreeModel model = (DefaultTreeModel) e.getSource();

                DefaultMutableTreeNode node;
                CheckBoxNode c; // = (CheckBoxNode) node.getUserObject();
                if (Objects.nonNull(children) && children.length == 1) {
                    node = (DefaultMutableTreeNode) children[0];
                    c = (CheckBoxNode) node.getUserObject();
                    TreePath parent = e.getTreePath();
                    DefaultMutableTreeNode n = (DefaultMutableTreeNode) parent.getLastPathComponent();
                    while (Objects.nonNull(n)) {
                        updateParentUserObject(n);
                        DefaultMutableTreeNode tmp = (DefaultMutableTreeNode) n.getParent();
                        if (Objects.nonNull(tmp)) {
                            n = tmp;
                        } else {
                            break;
                        }
                    }
                    model.nodeChanged(n);
                } else {
                    node = (DefaultMutableTreeNode) model.getRoot();
                    c = (CheckBoxNode) node.getUserObject();
                }
                updateAllChildrenUserObject(node, c.status);
                model.nodeChanged(node);
                adjusting = false;
                java.awt.event.ItemEvent event=null;
                moduleTestSuiteCheckboxItemStateChanged(event);
            }
            private void updateParentUserObject(DefaultMutableTreeNode parent) {
                int selectedCount = 0;
                Enumeration children = parent.children();
                while (children.hasMoreElements()) {
                    DefaultMutableTreeNode node = (DefaultMutableTreeNode) children.nextElement();
                    CheckBoxNode check = (CheckBoxNode) node.getUserObject();
                    if (check.status == Status.INDETERMINATE) {
                        selectedCount = -1;
                        break;
                    }
                    if (check.status == Status.SELECTED) {
                        selectedCount++;
                    }
                }
                String label = ((CheckBoxNode) parent.getUserObject()).label;
                if (selectedCount == 0) {
                    parent.setUserObject(new CheckBoxNode(label, Status.DESELECTED));
                } else if (selectedCount == parent.getChildCount()) {
                    parent.setUserObject(new CheckBoxNode(label, Status.SELECTED));
                } else {
                    parent.setUserObject(new CheckBoxNode(label));
                }
            }
            private void updateAllChildrenUserObject(DefaultMutableTreeNode root, Status status) {
                Enumeration breadth = root.breadthFirstEnumeration();
                while (breadth.hasMoreElements()) {
                    DefaultMutableTreeNode node = (DefaultMutableTreeNode) breadth.nextElement();
                    if (Objects.equals(root, node)) {
                        continue;
                    }
                    CheckBoxNode check = (CheckBoxNode) node.getUserObject();
                    node.setUserObject(new CheckBoxNode(check.label, status));
                }
            }
            @Override public void treeNodesInserted(TreeModelEvent e)    { /* not needed */ }
            @Override public void treeNodesRemoved(TreeModelEvent e)     { /* not needed */ }
            @Override public void treeStructureChanged(TreeModelEvent e) { /* not needed */ }
        });
        priorityTree.setEditable(true);
        priorityTree.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
//        priorityTree.expandRow(0);
        JScrollPane sp = new JScrollPane(priorityTree);
        sp.setSize(10, 10);
        jPanel4.setLayout(new BorderLayout());
        JPanel subPane = new JPanel(new FlowLayout(FlowLayout.LEFT));
        subPane.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
        subPane.add(jLabel8);

        JPanel subPanel = new JPanel();
        subPanel.setLayout(new BorderLayout());
        subPanel.add(subPane, BorderLayout.NORTH);
        subPanel.add(sp, BorderLayout.CENTER);
        jPanel4.add(subPanel, BorderLayout.NORTH);

        JPanel pane = new JPanel(new BorderLayout(5, 80));
        pane.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
        pane.add(priorityLabel, BorderLayout.SOUTH);
        jPanel4.add(pane, BorderLayout.SOUTH);
    }

    private ArrayList<String> readModuleFromJson(Hashtable<String,ArrayList<String>> selModules){
//        this method will read the packages names for the modules which are passed as parameter from UIConfig.JSON file
        ArrayList<String> tmp = new ArrayList<>();
        JsonObject moduleObj = null;
        Enumeration<String> selectedModule = selModules.keys();

        while(selectedModule.hasMoreElements()) {
            String str = selectedModule.nextElement();
            ArrayList<String> selMod = selModules.get(str);
            for(String st:selMod) {
                String modulePath = "";
                if (DataUtility.readConfig("flow") != null && DataUtility.readConfig("flow").equals("lf")) {
                    moduleObj = config.getAsJsonObject("UIConfig").getAsJsonObject("module").getAsJsonObject(project).getAsJsonObject("lf").getAsJsonObject(str);
                    modulePath = "test_definition."+project+"."+"lf_three." + moduleObj.get(st).getAsString();
                } else if (DataUtility.readConfig("flow") != null && DataUtility.readConfig("flow").equals("cam")) {
                    moduleObj = config.getAsJsonObject("UIConfig").getAsJsonObject("module").getAsJsonObject(project).getAsJsonObject("cam").getAsJsonObject(str);
                    modulePath = "test_definition."+project+"."+"lf_two." + moduleObj.get(st).getAsString();
                }
                tmp.add(modulePath);
            }
        }
        return tmp;

    }

    private ArrayList<String> readPriorityFromJson(ArrayList<String> priority){
//        this method will read the values of priorities from UIConfig.JSON file
        ArrayList<String> tmp = new ArrayList<>();

        JsonObject priorityObj = null;
        if(DataUtility.readConfig("flow")!= null && DataUtility.readConfig("flow").equals("lf")){
            priorityObj = config.getAsJsonObject("UIConfig").getAsJsonObject("priority").getAsJsonObject("lf");
        }else if(DataUtility.readConfig("flow")!= null && DataUtility.readConfig("flow").equals("cam")){
            priorityObj = config.getAsJsonObject("UIConfig").getAsJsonObject("priority").getAsJsonObject("cam");
        }
        for(String str:priority){
            String modulePath = priorityObj.get(str).getAsString();
            tmp.add(modulePath);
        }
        return tmp;

    }

    private ArrayList<String> readPriorityFromJson2(Hashtable<String,ArrayList<String>> selPriorities){
//        this method will read the packages names for the modules which are passed as parameter from UIConfig.JSON file
        ArrayList<String> tmp = new ArrayList<>();
        JsonObject moduleObj = null;
        Enumeration<String> selectedPriority = selPriorities.keys();

        while(selectedPriority.hasMoreElements()) {
            String str = selectedPriority.nextElement();
            ArrayList<String> selPri = selPriorities.get(str);
            for(String st:selPri) {
                String modulePath = "";
                if (DataUtility.readConfig("flow") != null && DataUtility.readConfig("flow").equals("online")) {
                    moduleObj = config.getAsJsonObject("UIConfig").getAsJsonObject("priority").getAsJsonObject("online").getAsJsonObject(str);
                    modulePath = moduleObj.get(st).getAsString();
                } else if (DataUtility.readConfig("flow") != null && DataUtility.readConfig("flow").equals("offline")) {
                    moduleObj = config.getAsJsonObject("UIConfig").getAsJsonObject("priority").getAsJsonObject("offline").getAsJsonObject(str);
                    modulePath = moduleObj.get(st).getAsString();
                }
                tmp.add(modulePath);
            }
        }
        System.out.println("sona5 priority set "+tmp);
        return tmp;


    }

    private Hashtable<String,ArrayList<String>> readAllModules(){
//        this module will read all the modules when test suite is been selected
        Hashtable<String,ArrayList<String>> allModulesHashTable = new Hashtable<>();

        Set<Map.Entry<String,JsonElement>> rootJsonSet = null;
        JsonObject rootModules = null;
        if(DataUtility.readConfig("flow")!= null && DataUtility.readConfig("flow").equals("lf")){
            rootModules = config.getAsJsonObject("UIConfig").getAsJsonObject("module").getAsJsonObject(project).getAsJsonObject("lf");
            rootJsonSet = rootModules.entrySet();
        }else if(DataUtility.readConfig("flow")!= null && DataUtility.readConfig("flow").equals("cam")){
            rootModules = config.getAsJsonObject("UIConfig").getAsJsonObject("module").getAsJsonObject(project).getAsJsonObject("cam");
            rootJsonSet = rootModules.entrySet();
        }
        iter = rootJsonSet.iterator();
        while(iter.hasNext()) {
            Map.Entry jsonMap = (Map.Entry) iter.next();
            Set<Map.Entry<String,JsonElement>> jsonSet = null;

//            allModules.add(jsonMap.getKey().toString());
            if(allModulesHashTable.get(jsonMap.getKey().toString())==null){
                jsonSet = rootModules.getAsJsonObject(jsonMap.getKey().toString()).entrySet();
                Iterator iterator = jsonSet.iterator();
                ArrayList<String> allModules = new ArrayList<>();
                while (iterator.hasNext()){
                    allModules.add(((Map.Entry)iterator.next()).getKey().toString());
                }
                allModulesHashTable.put(jsonMap.getKey().toString(),allModules);
            }else{
                ArrayList<String> allModules = allModulesHashTable.get(jsonMap.getKey().toString());
                Iterator iterator = jsonSet.iterator();
                while (iterator.hasNext()){
                    allModules.add(((Map.Entry)iterator.next()).getKey().toString());
                }
                allModulesHashTable.put(jsonMap.getKey().toString(),allModules);
            }
        }
        return allModulesHashTable;
    }

    private ArrayList<String> readAllPriorities(){
//        this method will read all the priorities when test suite is been selected
        ArrayList<String> allPriorities = new ArrayList<>();
        Set<Map.Entry<String,JsonElement>> jsonSet = null;
        if(DataUtility.readConfig("flow")!= null && DataUtility.readConfig("flow").equals("lf")){
            jsonSet = config.getAsJsonObject("UIConfig").getAsJsonObject("priority").getAsJsonObject("lf").entrySet();
        }else if(DataUtility.readConfig("flow")!= null && DataUtility.readConfig("flow").equals("cam")){
            jsonSet = config.getAsJsonObject("UIConfig").getAsJsonObject("priority").getAsJsonObject("cam").entrySet();
        }
        iter = jsonSet.iterator();
        while(iter.hasNext()) {
            Map.Entry jsonMap = (Map.Entry) iter.next();
            allPriorities.add(jsonMap.getKey().toString());
        }
        return allPriorities;
    }

    private Hashtable<String,ArrayList<String>> readAllPriorities2(){
//        this module will read all the modules when test suite is been selected
        Hashtable<String,ArrayList<String>> allPrioritiesHashTable = new Hashtable<>();

        Set<Map.Entry<String,JsonElement>> rootJsonSet = null;
        JsonObject rootPriorities = null;
        if(DataUtility.readConfig("flow")!= null && DataUtility.readConfig("flow").equals("lf")){
            rootPriorities = config.getAsJsonObject("UIConfig").getAsJsonObject("priority").getAsJsonObject("lf");
            rootJsonSet = rootPriorities.entrySet();
        }else if(DataUtility.readConfig("flow")!= null && DataUtility.readConfig("flow").equals("cam")){
            rootPriorities = config.getAsJsonObject("UIConfig").getAsJsonObject("priority").getAsJsonObject("cam");
            rootJsonSet = rootPriorities.entrySet();
        }
        iter = rootJsonSet.iterator();
        while(iter.hasNext()) {
            Map.Entry jsonMap = (Map.Entry) iter.next();
            Set<Map.Entry<String,JsonElement>> jsonSet = null;

//            allModules.add(jsonMap.getKey().toString());
            if(allPrioritiesHashTable.get(jsonMap.getKey().toString())==null){
                jsonSet = rootPriorities.getAsJsonObject(jsonMap.getKey().toString()).entrySet();
                Iterator iterator = jsonSet.iterator();
                ArrayList<String> allPriorities = new ArrayList<>();
                while (iterator.hasNext()){
                    allPriorities.add(((Map.Entry)iterator.next()).getKey().toString());
                }
                allPrioritiesHashTable.put(jsonMap.getKey().toString(),allPriorities);
            }else{
                ArrayList<String> allPriorities = allPrioritiesHashTable.get(jsonMap.getKey().toString());
                Iterator iterator = jsonSet.iterator();
                while (iterator.hasNext()){
                    allPriorities.add(((Map.Entry)iterator.next()).getKey().toString());
                }
                allPrioritiesHashTable.put(jsonMap.getKey().toString(),allPriorities);
            }
        }
        return allPrioritiesHashTable;
    }

    private void initTestSuite(){
//        this method will initialize the UI suite from the values read from json
        Set<Map.Entry<String,JsonElement>> jsonSet = null;
        if(DataUtility.readConfig("flow")!= null && DataUtility.readConfig("flow").equals("online")){
            jsonSet = config.getAsJsonObject("UIConfig").getAsJsonObject("testsuite").getAsJsonObject("online").entrySet();
        }else if(DataUtility.readConfig("flow")!= null && DataUtility.readConfig("flow").equals("offline")){
            jsonSet = config.getAsJsonObject("UIConfig").getAsJsonObject("testsuite").getAsJsonObject("offline").entrySet();
        }
        iter = jsonSet.iterator();
        int i=0;
        while(iter.hasNext()){
            Map.Entry jsonMap = (Map.Entry) iter.next();
            testSuiteList.add(new JRadioButton());
            testSuiteList.get(i).setText(jsonMap.getKey().toString());
//            add the listeners for each suite
            testSuiteList.get(i).addItemListener(new java.awt.event.ItemListener() {
                public void itemStateChanged(java.awt.event.ItemEvent evt) {
                    moduleTestSuiteCheckboxItemStateChanged(evt);
                }
            });
            i++;
        }
    }

    private String readTestSuite(JRadioButton suite){
//        this method will return the suite which is being selected in UI and give the value from json
        JsonObject moduleObj = null;
        if(DataUtility.readConfig("flow")!= null && DataUtility.readConfig("flow").equals("lf")){
            moduleObj = config.getAsJsonObject("UIConfig").getAsJsonObject("testsuite").getAsJsonObject("lf");
        }else if(DataUtility.readConfig("flow")!= null && DataUtility.readConfig("flow").equals("cam")){
            moduleObj = config.getAsJsonObject("UIConfig").getAsJsonObject("testsuite").getAsJsonObject("cam");
        }
        return moduleObj.get(suite.getText().toString()).getAsString();
    }

    private void removeDriverServers(){
//        kills the drivers for which the browsers is selected before closing or killing
        if(start.getText().toLowerCase().equals("stop")){
            String browserSelected = browserField.getSelectedItem().toString();
            String strCmd = null;
            String strPrintCmd = null;
            if(browserSelected.toLowerCase().equals("ie")){
                strCmd = String.format("TaskKill /F /IM " + "IEDriverServer.exe");
                //strPrintCmd = String.format("TaskKill /F /IM " + "PrintDialog_ie.exe");
            } else if(browserSelected.toLowerCase().equals("firefox")){
                //strPrintCmd = String.format("TaskKill /F /IM " + "PrintDialog2.exe");
                strCmd = String.format("TaskKill /F /IM " + "geckodriver.exe");
            } else if(browserSelected.toLowerCase().equals("chrome")){
                strCmd = String.format("TaskKill /F /IM " + "chromedriver.exe");
                //strPrintCmd = String.format("TaskKill /F /IM " + "PrintDialog_ie.exe");
            }

            /*try {
                Runtime.getRuntime().exec(strPrintCmd);
                Thread.sleep(2000);
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
*/
            try {
                Runtime.getRuntime().exec(strCmd);
                Thread.sleep(2000);
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }

        System.exit(-1);
    }

    private void readSelectedModulesAndPriorities(){
//      initializes the modules and priorities in the UI
        boolean moduleFlag = false;
        boolean priorityFlag = false;
        CheckBoxStatusModules modulesSelectedFromUI = new CheckBoxStatusModules();
        moduleFlag = modulesSelectedFromUI.ModuleCheckedOrNot(modulesRootNode);
        priorityFlag = modulesSelectedFromUI.PriorityCheckedOrNot(prioritiesRootNode);

        if((moduleFlag) && (priorityFlag)) {
            start.setText("Stop");
//            start.setText("Pause");

            Hashtable<String,ArrayList<String>> selModules = modulesSelectedFromUI.getModulesSelected(modulesRootNode);

            ArrayList<String> modules = readModuleFromJson(selModules);

//            ArrayList<String> priority = modulesSelectedFromUI.getPrioritiesSelected(prioritiesRootNode);
//
//            priority = readPriorityFromJson(priority);

            Hashtable<String,ArrayList<String>> selPriorities = modulesSelectedFromUI.getPrioritiesSelected2(prioritiesRootNode);

            ArrayList<String> priority = readPriorityFromJson2(selPriorities);

//          Before starting the thread and running
//          Just disable the email and get the text and then run it
            email.setEnabled(false);
            email.setEditable(false);
            browserField.setEnabled(false);
            environmentField.setEnabled(false);
            String emailsList = email.getText();

            progressBar.setMinimum(progressBarCounter);
            progressBar.setMaximum(totalCasesSelected);
            progressBar.setStringPainted(true);
            progressBar.setString(progressBarCounter+" of "+totalCasesSelected);
            progressBar.setVisible(true);
//            progressBar.setVisible(false);


            JavaSuiteGenerator javaSuiteGenerator = new JavaSuiteGenerator();
            javaSuiteGenerator.initParametersForModule(readComboBoxData(environmentField,"environment"), readComboBoxData(browserField,"browser"), false, emailsList, modules, priority);
            t = new Thread(javaSuiteGenerator);
            t.start();
        }
    }

    private void readSelectedTestSuite(){
//      moduleLabel.setText("This is how it works!");
        start.setText("Stop");
        int i=0;
        while(i<testSuiteList.size()){
            if(testSuiteList.get(i).isSelected()){
                Hashtable<String,ArrayList<String>> module = readAllModules();
//              Before starting the thread and running
//              Just disable the browser, environment & email and get the text and then run it
                ArrayList<String> modules = readModuleFromJson(module);
                email.setEnabled(false);
                email.setEditable(false);
                browserField.setEnabled(false);
                environmentField.setEnabled(false);
                String emailsList = email.getText();

                String suiteSelected = readTestSuite(testSuiteList.get(i));
                System.out.println(suiteSelected);
                progressBar.setMinimum(progressBarCounter);
                progressBar.setMaximum(totalCasesSelected);
                progressBar.setStringPainted(true);
                progressBar.setString(progressBarCounter+" of "+totalCasesSelected);
                progressBar.setVisible(true);
//                progressBar.setVisible(false);
                JavaSuiteGenerator javaSuiteGenerator = new JavaSuiteGenerator();
                javaSuiteGenerator.initParametersForSuite(readComboBoxData(environmentField,"environment"), readComboBoxData(browserField,"browser"), false, emailsList, suiteSelected, modules);
                t = new Thread(javaSuiteGenerator);
                t.start();
                break;
            }
            i++;
        }
    }

    public static void afterExcecution(){
//        This methods just reverts back the start button and enables the email option
        start.setText("Start");
        email.setEnabled(true);
        email.setEditable(true);
        browserField.setEnabled(true);
        environmentField.setEnabled(true);
        progressBar.setStringPainted(true);
        progressBar.setString(" ");
        progressBar.setValue(0);
        progressBarCounter = 0;
        if(totalFailedCases>0){
            rerun.setVisible(true);
        }
//        progressBar.setVisible(false);
//        totalCasesSelected = 0;
    }

    public static void incrementProgressBar(){
//        increments the progressbar by 1 and dispalys in the UI
        progressBarCounter++;
        progressBar.setValue(progressBarCounter);
        progressBar.setString(progressBarCounter+" of "+totalCasesSelected);
    }

    public static void incrementFailedCasesCount(){
//        increments the failed cases count whenever a case if failed which is used for counting the failed cases in the rerun
        totalFailedCases++;
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        removeDriverServers();
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}

class MyPainter implements Painter<JProgressBar> {
    //      this method will put green color to the progress bar
    private final Color color;
    private final JProgressBar progressBar;

    public MyPainter(Color c1,JProgressBar jProgressBar1) {
        this.color = c1;
        this.progressBar = jProgressBar1;
    }
    @Override
    public void paint(Graphics2D gd, JProgressBar t, int width, int height) {
        gd.setColor(color);
        int ht = progressBar.getHeight();
        gd.fillRect(0, 0, width, ht-(ht*20)/100);
    }
}