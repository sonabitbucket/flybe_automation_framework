package ui_automator;

import org.testng.TestNG;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlPackage;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;
import utilities.MailSsl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JavaSuiteGenerator implements Runnable{
    static TestNG testNG;
//    JavaSuiteGenerator(TestNG testNG){
//        this.testNG = testNG;
//    }

    public void initParametersForModule(String environment, String browser, boolean parallel, String email, ArrayList<String> module, ArrayList<String> priority){

        MailSsl.recipients = email;
        testNG = null;
        testNG = new TestNG();


//Create an instance of XML Suite and assign a name for it.


        XmlSuite suite = new XmlSuite();
        suite.setName("Flybe_Suite");
        suite.addListener("custom_listeners.TestInitiationListener");
        suite.addListener("custom_listeners.TestExecutionListener");
        suite.addListener("custom_listeners.RetryListner");
        suite.setParallel(XmlSuite.ParallelMode.getValidParallel("false"));

//Create an instance of XmlTest and assign a name for it.
        XmlTest test = new XmlTest(suite);
        test.setName("Dynamic_Test");

//Add any parameters that you want to set to the Test.
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("browserName", browser);
        parameters.put("environment", environment);
        test.setParameters(parameters);

        List<String> testGroup = new ArrayList<String>();
        if(priority.size()>0){
            for(int i=0;i<priority.size();i++){
                if(priority.get(i)!=null){
                    testGroup.add(priority.get(i));

                }
            }
        }

        test.setIncludedGroups(testGroup);

//Create a list which can contain the classes that you want to run.
        List<XmlPackage> modules = new ArrayList<XmlPackage> ();
/*
        for(int i=0;i<module.length;i++){
            if(module[i]!=null){
                modules.add(new XmlPackage(module[i]));

            }

        }
*/
        for(int i=0;i<module.size();i++){
            modules.add(new XmlPackage(module.get(i)));
        }
//Assign that to the XmlTest Object created earlier.
        test.setXmlPackages(modules);

//Create a list of XmlTests and add the Xmltest you created earlier to it.
        List<XmlTest> tests = new ArrayList<XmlTest>();
        tests.add(test);

//add the list of tests to your Suite.
        suite.setTests(tests);

//Add the suite to the list of suites.
        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        suites.add(suite);

//Set the list of Suites to the testNG object you created earlier.
        testNG.setXmlSuites(suites);

    }

    public void initParametersForSuite(String environment, String browser, boolean parallel, String email, String testSuite, ArrayList<String> modules1){
        //Create an instance on TestNG
//        TestNG testNG = new TestNG();

//Create an instance of XML Suite and assign a name for it.

        MailSsl.recipients = email;

        testNG = null;
        testNG = new TestNG();
        XmlSuite suite = new XmlSuite();
        suite.setName("Flybe_Suite");
        suite.addListener("custom_listeners.TestInitiationListener");
        suite.addListener("custom_listeners.TestExecutionListener");
        suite.addListener("custom_listeners.RetryListner");
        suite.setParallel("false");

//Create an instance of XmlTest and assign a name for it.
        XmlTest test = new XmlTest(suite);
        test.setName("Dynamic_Test");

//Add any parameters that you want to set to the Test.
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("browserName", browser);
        parameters.put("environment", environment);
        test.setParameters(parameters);

        List<String> testGroup = new ArrayList<String>();
        testGroup.add(testSuite);
        test.setIncludedGroups(testGroup);

//Create a list which can contain the classes that you want to run.
        List<XmlPackage> modules = new ArrayList<XmlPackage> ();
        for(int i=0;i<modules1.size();i++){
            modules.add(new XmlPackage(modules1.get(i)));
        }

//Assign that to the XmlTest Object created earlier.
        test.setXmlPackages(modules);

//Create a list of XmlTests and add the Xmltest you created earlier to it.
        List<XmlTest> tests = new ArrayList<XmlTest>();
        tests.add(test);

//add the list of tests to your Suite.
        suite.setTests(tests);

//Add the suite to the list of suites.
        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        suites.add(suite);

//Set the list of Suites to the testNG object you created earlier.
        testNG.setXmlSuites(suites);
    }

    @Override
    public void run() {
        testNG.run();
    }
}
