package ui_automator;


import org.apache.commons.lang3.time.DateUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimeTest {
    public static String addTime(String time1, String time2){
//        String time1="2:59";
//        String time2="1:59";

        if(time1.equals("")){
            return time2;
        }
        DateFormat sdf = new SimpleDateFormat("hh:mm");

        Date date=null;
        Integer i = Integer.parseInt(time1.split(":")[0]);
        Integer j = Integer.parseInt(time2.split(":")[0]);
        try {
            if(i>=j){
                date = sdf.parse(time2);
                String splitTime[] = time1.split(":");
                date = DateUtils.addHours(date, Integer.parseInt(splitTime[0]));
                date = DateUtils.addMinutes(date, Integer.parseInt(splitTime[1]));
            }else{
                date = sdf.parse(time1);
                String splitTime[] = time2.split(":");
                date = DateUtils.addHours(date, Integer.parseInt(splitTime[0]));
                date = DateUtils.addMinutes(date, Integer.parseInt(splitTime[1]));
            }
        } catch (ParseException e1) {
            e1.printStackTrace();
        }

        System.out.println(date.toString().split(" ")[3].substring(0,5));
        String str = date.toString().split(" ")[3].substring(0,5);
        return str;
    }

}
