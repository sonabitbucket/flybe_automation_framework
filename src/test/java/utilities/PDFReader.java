package utilities;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class PDFReader {

    public static String extractPDFContent(String strURL,int fromPageNo,int toPageNo) {

        PDFTextStripper pdfStripper = null;
        PDDocument pdDoc = null;
        COSDocument cosDoc = null;
        String parsedText = null;
/*
        try {
            URL url = new URL(strURL);
            //BufferedInputStream file = new BufferedInputStream(url.openStream());
            RandomAccessFile file = new RandomAccessFile(new File(InputStream()),"r");
            PDFParser parser = new PDFParser(file);

            parser.parse();
            cosDoc = parser.getDocument();
            pdfStripper = new PDFTextStripper();
            pdfStripper.setStartPage(fromPageNo);
            pdfStripper.setEndPage(toPageNo);

            pdDoc = new PDDocument(cosDoc);
            parsedText = pdfStripper.getText(pdDoc);
        } catch (MalformedURLException e2) {
            System.err.println("URL string could not be parsed "+e2.getMessage());
        } catch (IOException e) {
            System.err.println("Unable to open PDF Parser. " + e.getMessage());
            try {
                if (cosDoc != null)
                    cosDoc.close();
                if (pdDoc != null)
                    pdDoc.close();
            } catch (Exception e1) {
                e.printStackTrace();
            }
        }finally
        {
         try{   if( pdDoc != null )
            {
                pdDoc.close();
            }}catch(Exception e){
             System.out.println("unable to close PDF doc");
         }
        }

        *//*System.out.println("+++++++++++++++++");
        System.out.println(parsedText);
        System.out.println("+++++++++++++++++");*/
        return parsedText;
    }

}
