package utilities;

import org.testng.TestNG;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlPackage;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SuiteGenerator {
    public static void main(String[]args){
        //Create an instance on TestNG
        TestNG testNG = new TestNG();

//Create an instance of XML Suite and assign a name for it.
        XmlSuite suite = new XmlSuite();
        suite.setName("Flybe-Test-Suite");
        suite.addListener("custom_listeners.TestInitiationListener");
        suite.addListener("custom_listeners.TestExecutionListener");
        suite.addListener("custom_listeners.RetryListner");
        suite.setParallel("classes");

//Create an instance of XmlTest and assign a name for it.
        XmlTest test = new XmlTest(suite);
        test.setName("Smoke-Test");

//Add any parameters that you want to set to the Test.
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("browserName", "firefox");
        test.setParameters(parameters);

//        List<String> testGroup = new ArrayList<String>();
////        testGroup.add("smoke");
//////
////        testGroup.add("p1");
//        test.setIncludedGroups(testGroup);

//Create a list which can contain the classes that you want to run.
        List<XmlPackage> modules = new ArrayList<XmlPackage> ();
        modules.add(new XmlPackage("test_definition.smoke.booking"));

//Assign that to the XmlTest Object created earlier.
        test.setXmlPackages(modules);

//Create a list of XmlTests and add the Xmltest you created earlier to it.
        List<XmlTest> tests = new ArrayList<XmlTest>();
        tests.add(test);

//add the list of tests to your Suite.
        suite.setTests(tests);

//Add the suite to the list of suites.
        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        suites.add(suite);

//Set the list of Suites to the testNG object you created earlier.
        testNG.setXmlSuites(suites);

//invoke run() - this will run your class.
        testNG.run();
    }

}




