package utilities;

import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.base.Predicate;
import com.relevantcodes.extentreports.LogStatus;
import com.relevantcodes.extentreports.model.Test;
//import org.apache.commons.FileUtils;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.*;
import org.testng.Reporter;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.SECONDS;
import static utilities.ReportUtility.reportLocation;


public class ActionUtility {
    // Defining of Basic selenium actions
    Actions action = null;



    public ActionUtility() {
        this.action = new Actions(TestManager.getDriver());
    }

    public enum SelectionType {
        //Selection types used to select the value in the dropdown
        SELECTBYTEXT,
        SELECTBYVALUE,
        SELECTBYINDEX
    }


    public enum FrameLocator {
        //Locator types used to find the webelements
        NAMEORID,
        INDEX,
        WEBELEMENT
    }

//    private void setReportLocation(String timeStamp){
//        if (reportLocation!=null){
//            reportLocation = DataUtility.readConfig("report.location")+"\\screenShort" + timeStamp;
//        }
//    }

    public static void takeScreenShot(String methodName, String timeStamp) throws Exception {

        // captures the screen shot
        try {
            EventFiringWebDriver eventFiringWebDriver = new EventFiringWebDriver(TestManager.getDriver());
            File scrFile = eventFiringWebDriver.getScreenshotAs(OutputType.FILE);
            /*WebDriver augmentedDriver = new Augmenter().augment(TestManager.getDriver());
            File scrFile = ((TakesScreenshot)augmentedDriver).getScreenshotAs(OutputType.FILE);*/
            //below steps will save the screen shot in specified path with test method name
            FileUtils.copyFile(scrFile, new File(reportLocation+"\\"+"screenshots\\"+methodName+"_"+timeStamp+".png"));
            System.out.println("***Placed screen shot in "+reportLocation+"\\ screenshots\\ ***");
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }

    public void sendKeysByAction(WebElement element, String keysToEnter){
        // send key by actions
        try {
            waitForElementVisible(element, 10);
            action.sendKeys(keysToEnter).build().perform();
        }catch (TimeoutException e){
            Reporter.log("Element not visible",3);
            throw e;
        }catch (StaleElementReferenceException ex){
            Reporter.log("Element not present on page");
            throw ex;
        }
    }

    public void hardSendKeys(WebElement element,String keysToEnter){
        try{
            ((JavascriptExecutor) TestManager.getDriver()).executeScript("arguments[0].setAttribute('value',arguments[1])", element,keysToEnter);


        }catch (TimeoutException e){
            e.printStackTrace();
            throw e;
        }
    }

    public void sendKeys(WebElement element,String keysToEnter){

        try{
            waitForElementVisible(element,10);
            element.clear();
            element.sendKeys(keysToEnter);
        }catch (TimeoutException e){
            e.printStackTrace();
            throw e;
        }
    }

    public void waitForPageLoad(int time){
        //will verify the DOM with java script

        new WebDriverWait(TestManager.getDriver(), time).until((ExpectedCondition<Boolean>) wd ->
                ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete"));
//        try{
////            TestManager.getDriver().manage().timeouts().pageLoadTimeout(time, TimeUnit.SECONDS);
//            WebDriverWait wait = new WebDriverWait(TestManager.getDriver(),time);
//
//            wait.until( new Predicate<WebDriver>() {
//                            public boolean apply(WebDriver driver) {
//                                return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
//                            }
//                        }
//            );}catch (Exception e){}

    }
    public void waitForPageTitle(String pageTitle,int timeFactor){
        // Wait for the page to have the respective title
        try{
            WebDriverWait wait = new WebDriverWait(TestManager.getDriver(),timeFactor,300);
            wait.ignoring(Exception.class);
            wait.until(ExpectedConditions.titleContains(pageTitle));
        }catch (TimeoutException e){
            Reporter.log("page title does not match");
            throw e;
        }
    }

    public void waitForPageURL(String pageURL,int timeFactor){
        // Wait for the page to have the respective title
        try{
            WebDriverWait wait = new WebDriverWait(TestManager.getDriver(),timeFactor);
            wait.until(ExpectedConditions.urlContains(pageURL));
        }catch (TimeoutException e){
            Reporter.log("page URL does not match");
            throw e;
        }
    }

    public void waitForElementVisible(WebElement element, int time){
        // wait till the element is visible

        try {
            WebDriverWait wait = new WebDriverWait(TestManager.getDriver(), time,250);
            wait.ignoring(StaleElementReferenceException.class);
            wait.ignoring(Exception.class);
            wait.until(ExpectedConditions.visibilityOf(element));
        }catch (TimeoutException e){
            Reporter.log("Element not visible",3);
            throw e;
        }

    }
    public void waitForTextChange(String element, int time,String text){
        // wait till the text changes

        try {
            WebDriverWait wait = new WebDriverWait(TestManager.getDriver(), time,250);
            wait.until(ExpectedConditions.invisibilityOfElementWithText(By.xpath(element),text));
        }catch (TimeoutException e){
            Reporter.log("Element not visible",3);
            throw e;
        }

    }
    public Boolean flexibleWaitForElementVisible(String element,int time){
        //fluent wait

          /*  FluentWait<WebDriver> wait = new FluentWait<WebDriver>(TestManager.getDriver());
            wait.pollingEvery(100, TimeUnit.MILLISECONDS);
            wait.withTimeout((long) time, TimeUnit.SECONDS);
            wait.ignoring(NoSuchElementException.class);
//            wait.until((Function<? super WebDriver, ExpectedCondition<WebElement>>) input -> ExpectedConditions.visibilityOf(input.findElement(By.cssSelector(".passengers form"))));

            wait.until(new Function<WebDriver, Object>() {
                @Override
                public ExpectedCondition apply(WebDriver input){

                    return ExpectedConditions.visibilityOf(input.findElement(By.cssSelector(".passengers form")));
                }
            });*/

        Wait<WebDriver> wait = new FluentWait<WebDriver>(TestManager.getDriver())
                .withTimeout(20, java.util.concurrent.TimeUnit.SECONDS)
                .pollingEvery(100, java.util.concurrent.TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class);

        boolean result = false;
        try {
            result = wait.until(new Function<WebDriver, Boolean>() {
                @Override
                public Boolean apply(WebDriver driver) {
                    return driver.findElements(By.xpath(element)).size() == 0;
                }
            });
        } catch (TimeoutException e) {
            ;
        } catch (Exception e) {
            Reporter.log("ElementNotPresent");

        }

        return result;

    }

    public void waitForElementClickable( WebElement element,int time){
        // wait till the element is clickable
        try{

            WebDriverWait wait = new WebDriverWait(TestManager.getDriver(),time,300);
            wait.ignoring(Exception.class);
            wait.until(ExpectedConditions.elementToBeClickable(element));
        }catch (TimeoutException e){
            Reporter.log("Element not clickable",3);
            throw e;
        }

    }
    public Boolean verfiyIfAnElementISEnabled(WebElement e){
        //verifies if an element is enabled and returns true if it is clickable else returns false
        Boolean flag = false;
        try{
            if(e.isEnabled()){
                flag = true;
            }
        }catch(Exception ex){}
        return flag;
    }
    public void waitForElementNotPresent(String locator,int time){
        // wait till the element is invisible
        try {
            WebDriverWait wait = new WebDriverWait(TestManager.getDriver(),time,300);
            wait.ignoring(Exception.class);
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(locator)));
        }catch (TimeoutException e){
            Reporter.log("Element is still visible",3);
            throw e;
        }
    }

    public void waitAndSwitchToFrame(FrameLocator frameLocatorType, Object...element) {
        // wait till the frame is available and switch to it
        try{
            WebDriverWait wait = new WebDriverWait(TestManager.getDriver(),35,500);
            wait.ignoring(Exception.class);
            switch (frameLocatorType){
                case WEBELEMENT:
                    wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt((WebElement) element[0]));
                    break;
                case NAMEORID:
                    wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt((String)element[0]));
                    break;
                case INDEX:
                    wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(((int) element[0])));
                    break;
            }

        }catch(TimeoutException e){
            TestManager.getReportLogger().log(LogStatus.WARNING,"No Frame Found with Locator: "+frameLocatorType+": "+element[0]);
            throw e;
        }
    }

    public boolean isAlertPresent(){
        boolean flag = false;
        try {
            WebDriverWait wait = new WebDriverWait(TestManager.getDriver(), 5);
            wait.until(ExpectedConditions.alertIsPresent());
            flag = true;
        }catch (TimeoutException e){}
        return flag;

    }

    public boolean checkAlertAndPerform(String alertAction) {
        // wait till the alert is available and switches the to it
        if (isAlertPresent()){
            Alert alert = TestManager.getDriver().switchTo().alert();
            if (alertAction.toLowerCase().equals("accept")){
                alert.accept();
            }else if (alertAction.toLowerCase().equals("dismiss")){
                alert.dismiss();
            }
            return true;
        }else {
            return false;
        }
    }

    public void switchBackToWindowFromFrame(){
        //switch the control to main window from frame
        try{
            TestManager.getDriver().switchTo().defaultContent();
            TestManager.getReportLogger().log(LogStatus.INFO,"successfully switched to main window");
        }catch(Exception e){
            TestManager.getReportLogger().log(LogStatus.INFO,"unable to switch to main window");
            throw e;
        }
    }

    public void hardSleep(int timeInMillsec){
        // will suspend the thread for specified amount of time
        try {
            Thread.sleep(timeInMillsec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void dropdownSelect(WebElement element, SelectionType selectedMethod, String valueToSelect) {
        // select the value in the dropdown
        waitForElementVisible(element,10);
        Select select = new Select(element);
        switch (selectedMethod) {
            case SELECTBYTEXT:
                select.selectByVisibleText(valueToSelect);
                hardSleep(1000);
                break;
            case SELECTBYVALUE:
                select.selectByValue(valueToSelect);
                break;
            case SELECTBYINDEX:
                int index = Integer.parseInt(valueToSelect);
                select.selectByIndex(index);
                break;
        }
    }

    public String getValueSelectedInDropdown(WebElement element) {
        // select the value in the dropdown
        Select select = new Select(element);
        return select.getFirstSelectedOption().getText();

    }


    public void selectOption(WebElement element) {
        //select the checkbox and radio button with 5 attempts
        try {
            waitForElementClickable(element,30);
            int attempt = 0;
            while ((!element.isSelected()) && attempt < 5) {
                click(element);
                attempt++;
            }
            if((!element.isSelected() )&& (attempt >=5)){
                throw new RuntimeException("Unable to select the element");
            }
        }catch (TimeoutException e){
//            Reporter.log("Element not visible to select",3);
            throw e;
        }catch (RuntimeException e){
            Reporter.log("Unable to select the element",3);
            throw  e;
        }
    }

    public void hardSelectOption(WebElement element) {
        //select the checkbox and radio button with 5 attempts using java script
        try {
            waitForElementClickable(element,20);
            int attempt = 0;
            boolean flag = false;
            while ((!element.isSelected()) && attempt < 5) {
                hardClick(element);
                attempt++;
            }
            if(!element.isSelected() && (attempt >=5)){
                throw new RuntimeException("Unable to select the element");
            }

        }catch (TimeoutException e){
            Reporter.log("Element not visible to select",3);
            throw e;
        }catch (RuntimeException e){
            Reporter.log("Unable to select the element",3);
            throw  e;
        }
    }

    public List<String> getDropDownValues(WebElement e){
        //returnsvalues in a dropdown field
        List<String> dropDownValues = new ArrayList<>();
        String valueFound;
        int i=0;
        try{
            Select dropdown = new Select(e);
            List<WebElement> dropdownValue = dropdown.getOptions();
            for(WebElement value:dropdownValue){
                valueFound = value.getText();
                dropDownValues.add(i++,valueFound);
            }

            return dropDownValues;
        }
        catch(Exception ex){
            Reporter.log("Unable to find the dropdown valuesin the element");
            throw ex;
        }
    }

    public void click (WebElement element){
        // wait for element and click
        try{
            waitForElementClickable(element,30);
            element.click();
        }catch (TimeoutException e){
            Reporter.log("Element not visible to click",3);
            throw e;
        }
    }

    public void clickByAction(WebElement element){
        // wait for element and click it by actions
        try {
            waitForElementClickable(element, 20);
            action.moveToElement(element).click(element).build().perform();
        }catch (TimeoutException e){
            Reporter.log("Element not visible",1);
            throw e;
        }
    }

    public void  mouseHoverAndClick(WebElement elementTobeClicked){
        //mouse hover by actions and click
//        waitForElementClickable(element,10);
//        action.moveToElement(element).build1().perform();
//        click(elementTobeClicked,10);
        hardClick(elementTobeClicked);

    }

    public void hardClick (WebElement element){
        // javascript to click on the element
        try {
//            waitForElementClickable(element,20);

            ((JavascriptExecutor) TestManager.getDriver()).executeScript("arguments[0].click();", element);
        }catch (TimeoutException e){
            Reporter.log("Element not visisble to click",3);
            throw e;
        }
    }

    public boolean verifyIfCheckboxIsSelected(WebElement element){
        try{
            return element.isSelected();
        }catch (Exception e){
                Reporter.log("Unable to verify if the element is selected");
                throw  e;
         }
    }

    public void unSelectOption(WebElement element) {
        //unselect the checkbox and radio button with 5 attempts
        try {
            waitForElementClickable(element,20);
            int attempt = 0;
            while ((element.isSelected()) && attempt < 5) {
                element.click();
                attempt++;
            }
            if (element.isSelected() && (attempt >= 5)) {
                throw new RuntimeException("Unable to Unselect the element");
            }
        }catch (TimeoutException e){
            Reporter.log("Element not visible to unselect",3);
            throw e;
        }catch (Exception e){
            Reporter.log("Unable to Unselect the element");
            throw  e;
        }

    }

    private String  calculateDate(int dateOffset, String format) {
        // will calculate the date as per the offset
        SimpleDateFormat dateFormatter = new SimpleDateFormat(format);

        Calendar now = Calendar.getInstance();
        now.setTime(new Date());
        now.add(Calendar.DATE, dateOffset);

        Date calculatedDate = now.getTime();

        String frmtdDate = dateFormatter.format(calculatedDate);

        return frmtdDate;
    }


    public String[] getDate(int dateOffset,String... format){
        // will return the calculated date as string array in format[0] or ddmmmyyyy
        String frmtdDate;
        if(format.length>0){
            frmtdDate = calculateDate(dateOffset, format[0]);
        }
        else {
            frmtdDate = calculateDate(dateOffset, "d/MMM/yyyy");
        }
        return frmtdDate.split("/");

    }

    public String[] getDateWithFullMonthName (int dateOffset){
        // will return the calculated date as string array in ddmmmmyyyy
        String frmtdDate = calculateDate(dateOffset, "d/MMMM/yyyy");
        return frmtdDate.split("/");

    }

    public String getMonth(int dateOffset){
        // will return the calculated date as month
        String frmtdDate = calculateDate(dateOffset, "MMMM");
        return frmtdDate;
    }

    public String getYear(int dateOffset){
        // will return the calculated date as month
        String frmtdDate = calculateDate(dateOffset, "YYYY");
        return frmtdDate;
    }

    public String getDateAsString(int dateOffset,String format){
        // will return the calculated date as string
        String frmtdDate = calculateDate(dateOffset,format);
        return frmtdDate;
    }

    public YearMonth convertStringinMMMMYYYYformatToDate(String dateText){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MMMM yyyy");
        return YearMonth.parse(dateText, dtf);
    }
    public int getRandomNumber(){
        // return the random number
        Random rand = new Random(System.currentTimeMillis());
        int random=rand.nextInt();
        return random;
    }

    public void waitForPopupAsNewWindow(int... time){
        int timeFactor =20;
        if(time.length>0){
            timeFactor = time[0];
        }
        try{
            WebDriverWait wait = new WebDriverWait(TestManager.getDriver(),timeFactor);
            wait.until(ExpectedConditions.numberOfWindowsToBe(2));

        }catch (TimeoutException e){
            throw e;
        }
    }

    public boolean switchWindow(String title,String...attribute){
        //switches to window specified
        try{
            boolean flag =false;
            waitForPopupAsNewWindow(35);

            for(String winHandle:TestManager.getDriver().getWindowHandles()){
                TestManager.getDriver().switchTo().window(winHandle);
                waitForPageLoad(30);
                if(TestManager.getDriver().getTitle().toLowerCase().equals(title.toLowerCase())){
                    flag = true;
                    break;

                }
                if (!flag) {
                    if(attribute.length>0) {
                        if(TestManager.getDriver().getPageSource().contains(attribute[0])){
                            flag = true;
                            break;
                        }
                    }
                }
            }
            return  flag;
        }catch (Exception e){

            throw e;
        }

    }

    public void switchBackToMainWindow(String title){
        //switches to window specified
        try{
            TestManager.getDriver().close();
            WebDriverWait wait = new WebDriverWait(TestManager.getDriver(),20);
            wait.until(ExpectedConditions.numberOfWindowsToBe(1));
            for(String winHandle:TestManager.getDriver().getWindowHandles()){
                TestManager.getDriver().switchTo().window(winHandle);
                if(TestManager.getDriver().getTitle().toLowerCase().contains(title.toLowerCase())){
                    break;
                }
            }
        }catch (Exception e){

            throw e;
        }
    }

    public void switchToWindowHandle(String handle){
        TestManager.getDriver().switchTo().window(handle);
    }

    public String getPresentWindowHandle(){
        Iterator<String> it = TestManager.getDriver().getWindowHandles().iterator();
        return it.next();
    }

    public boolean verifyIfElementIsDisplayed(WebElement element, int... time) {
        //checks whether element is present in the web page
        boolean flag = false;
        int timeFactor = 5;
        if (time.length > 0){
            timeFactor = time[0];
        }
        try{
            waitForElementVisible(element,timeFactor);
            int attempt =0;

            while(attempt<3 && !flag){
                try {
                    flag = element.isDisplayed();
                }catch (StaleElementReferenceException e){}
                attempt++;
            }


        }catch(NoSuchElementException e){
            //If the element is not present in the DOM then it will come here
            System.out.println("element not present");
        }catch (TimeoutException e){}
        return flag;
    }

    public boolean verifyIfDependentElementIsDisplayed(WebElement parent_element,String child_locator,String elementLocator, int... time) {
        //checks whether element is present in the web page
        boolean flag = false;
        int timeFactor = 5;
        if (time.length > 0){
            timeFactor = time[0];
        }
        try{
            if(elementLocator.equalsIgnoreCase("class"))
            waitForElementVisible(parent_element.findElement(By.className(child_locator)),timeFactor);
            int attempt =0;

            while(attempt<3 && !flag){
                try {
                    flag = parent_element.findElement(By.xpath(child_locator)).isDisplayed();
                }catch (StaleElementReferenceException e){}
                attempt++;
            }


        }catch(NoSuchElementException e){
            //If the element is not present in the DOM then it will come here
            System.out.println("element not present");
        }catch (TimeoutException e){}
        return flag;
    }

    public boolean verifyIfElementIsDisplayed(String locator, int... time) {
        //checks whether element is present in the web page
        boolean flag = false;
        int timeFactor = 5;
        if (time.length > 0){
            timeFactor = time[0];
        }
        try{
            WebElement element = TestManager.getDriver().findElement(By.xpath(locator));
            waitForElementVisible(element,timeFactor);
            int attempt =0;
            while(attempt<3 && !flag){
                try {
                    flag = element.isDisplayed();
                }catch (StaleElementReferenceException e){}
                attempt++;
            }
        }catch(NoSuchElementException e){
            //If the element is not present in the DOM then it will come here
        }catch (TimeoutException e){}
        return flag;
    }


    public boolean verifyIfElementIsDisplayed(List<WebElement> element, int... time) {
        //checks whether element is present in the web page
        boolean flag = false;
        int timeFactor = 5;
        if (time.length > 0){
            timeFactor = time[0];
        }
        try{
            waitForElementVisible(element,timeFactor);
            int attempt =0;

            while(attempt<3 && !flag){
                try {
                    if(element.size()>0){
                        flag=true;
                    }
                }catch (StaleElementReferenceException e){}
                attempt++;
            }


        }catch(NoSuchElementException e){
            //If the element is not present in the DOM then it will come here
        }catch (TimeoutException e){}
        return flag;
    }

    public void waitForElementVisible(List<WebElement> element, int time){
        // wait till the element is visible

        try {
            WebDriverWait wait = new WebDriverWait(TestManager.getDriver(), time,250);
            wait.ignoring(StaleElementReferenceException.class);
            wait.ignoring(Exception.class);
            wait.until(ExpectedConditions.visibilityOfAllElements(element));
        }catch (TimeoutException e){
            Reporter.log("Element not visible",3);
            throw e;
        }

    }

    public WebElement findParentNode(WebElement childElement){

        /*WebElement parent = (WebElement) ((JavascriptExecutor) TestManager.getDriver()).executeScript(
                "return arguments[0].parentNode;", element);*/

        WebElement parentElement = childElement.findElement(By.xpath("./.."));

        return parentElement;
    }

    public List<WebElement> findChildNode(WebElement parentElement){
        List<WebElement> children = parentElement.findElements(By.xpath(".//*"));
        return children;
    }

    public void scrollToViewElement(WebElement element){
        ((JavascriptExecutor)TestManager.getDriver()).executeScript("arguments[0].scrollIntoView();", element);
    }

    public void scrollUP(WebElement element){
        ((JavascriptExecutor)TestManager.getDriver()).executeScript("window.scrollBy(0, -250)", "");
    }
}
