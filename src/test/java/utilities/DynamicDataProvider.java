package utilities;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;
import org.testng.annotations.ObjectFactory;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.io.FileInputStream;

import static utilities.DataUtility.readConfig;

/**
 * Created by Sona on 5/8/2019.
 */
public class DynamicDataProvider {

        static InputStream fis=null;
        static Workbook wb=null;
        static Sheet s=null;

        static final int RUN_STATUS_COL=2;
        static final int TCNAME_COL=1;
        static final int TCDATA_START_COL=3;

        @DataProvider(name="testData"/*,parallel=true*/)
        public static Object[][] getTestData(Method m)
    {
            fis = DataUtility.class.getClassLoader().getResourceAsStream(readConfig("dataSource.name"));

            try {
                wb = WorkbookFactory.create(fis);
            } catch (InvalidFormatException e) {
                e.printStackTrace();
            }catch (IOException e) {
                System.out.println("Incorrect file");
                e.printStackTrace();
            }
            XSSFFormulaEvaluator evaluator = new XSSFFormulaEvaluator((XSSFWorkbook) wb);
            s = wb.getSheet("dataProvider");
            String tcname=m.getName();
            System.out.println("***"+tcname);
            Object[][] testData=new String[getTCs(tcname)][getTCData(tcname)];
            int noOfRows=s.getPhysicalNumberOfRows();
            int rowIndex=0;
            for(int eachRow=0;eachRow<noOfRows;eachRow++)
            {
                Row r=s.getRow(eachRow);
                Cell c1=r.getCell(RUN_STATUS_COL);
                Cell c2=r.getCell(TCNAME_COL);

                if(c1.getStringCellValue().equalsIgnoreCase("Y")&&c2.getStringCellValue().equalsIgnoreCase(tcname))
                {
                    int noOfCells=r.getLastCellNum();
                    int cellIndex=0;
                    for(int eachCell=TCDATA_START_COL;eachCell<noOfCells;eachCell++)
                    {
                        //testData[rowIndex][cellIndex++]=r.getCell(eachCell);
                        testData[rowIndex][cellIndex++]=getCellValue(r.getCell(eachCell),evaluator);
                    }
                    rowIndex++;
                }

            }
            System.out.println(testData);
            return testData;
        }

        public static int getTCs(String tcname)
        {
            int count=0;
            int noOfRows=s.getPhysicalNumberOfRows();
            for(int eachRow=1;eachRow<noOfRows;eachRow++)
            {
                Row r=s.getRow(eachRow);
                Cell c1=r.getCell(RUN_STATUS_COL);
                Cell c2=r.getCell(TCNAME_COL);

                if(c1.getStringCellValue().equalsIgnoreCase("Y")&&c2.getStringCellValue().equalsIgnoreCase(tcname))
                {
                    count++;
                }
            }
            return count;
        }

        public static int getTCData(String tcname)
        {
            int count=0;
            int noOfRows=s.getPhysicalNumberOfRows();
            for(int eachRow=0;eachRow<noOfRows;eachRow++)
            {
                Row r=s.getRow(eachRow);
                Cell c1=r.getCell(RUN_STATUS_COL);
                Cell c2=r.getCell(TCNAME_COL);
                if(c1.getStringCellValue().equalsIgnoreCase("Y")&&c2.getStringCellValue().equalsIgnoreCase(tcname))
                {
                    return r.getLastCellNum()-TCDATA_START_COL;
                }

            }
            return 0;
        }

    private static String getCellValue(Cell cell, XSSFFormulaEvaluator evaluator){
        // returns the cell value in the form of String
        String cellValue = null;
        evaluator.evaluate(cell);
        DataFormatter formatter = new DataFormatter();
        cellValue = formatter.formatCellValue(cell, evaluator);
        return cellValue;
    }

}



