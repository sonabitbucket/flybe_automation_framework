package utilities;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;



public class DBUtility {

    public static void getAllDetails() {
        Connection connection = null;
        Statement statement = null;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://10.98.12.33:3306/automation_schema", "username", "password");
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select * from testresult");
            while (rs.next())
                System.out.println(rs.getString(1) + "  " + rs.getBoolean(2) + "  " + rs.getDate(3) + " " + rs.getString(4));

        } catch (Exception e) {
            System.out.println(e);
        } finally {
        }
    }

    public static void executeInsertToTestCases(String id, String environment, String result, String endTime, String timeTakenForTC, String machineID,String startTime, String... failureReason) {

        System.out.println("startTime in dbUtility"+startTime);
        Connection connection = null;
        PreparedStatement statement = null;
        String sql = "";
        String testSuite = "";
        int sec = Integer.parseInt(timeTakenForTC);
        int hours = (sec / 3600);
        int minutes = (sec % 3600)/60;
        int seconds = (sec % 3600)%60;
        String timeTaken = hours+":"+minutes+":"+seconds;

        if (id.substring(0, 1).equals("S")) {
            testSuite = "Smoke";
        } else if (id.substring(0, 1).equals("R")) {
            testSuite = "Regression";
        }

        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://10.98.12.33:3306/automation_schema", "username", "password");
            String suite_id = ReportUtility.formattedTime;
            if (failureReason.length > 0) {
                sql = "insert into testresult (ID, Result, Failure_Reason, Environment, Test_Suite, Suite_ID, Execution_End_Time, TC_Execution_Time, Machine_ID,Date_of_Execution) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                statement = connection.prepareStatement(sql);
                statement.setString(1, id);
                statement.setString(2, result);
                statement.setString(3, failureReason[0]);
                statement.setString(4, environment);
                statement.setString(5, testSuite);
                statement.setString(6, suite_id);
                statement.setString(7, endTime);
                statement.setString(8, timeTaken);
                statement.setString(9, machineID);
                statement.setString(10, startTime);
            } else {
                sql = "insert into testresult (ID, Result, Environment, Test_Suite, Suite_ID, Execution_End_Time, TC_Execution_Time, Machine_ID,Date_of_Execution) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
                statement = connection.prepareStatement(sql);
                statement.setString(1, id);
                statement.setString(2, result);
                statement.setString(3, environment);
                statement.setString(4, testSuite);
                statement.setString(5, suite_id);
                statement.setString(6, endTime);
                statement.setString(7, timeTaken);
                statement.setString(8, machineID);
                statement.setString(9, startTime);
            }
            statement.execute();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            try {
                if (statement != null)
                    connection.close();
            } catch (SQLException se) {
            }// do nothing
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }

        updateReliability(id, result);
        updatePassPercent(id);
    }

    private static void updateReliability(String id, String result){
        Connection connection = null;
        String sql;
        PreparedStatement statement = null;
        try{
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://10.98.12.33:3306/automation_schema", "username", "password");
            if(result.equals("Pass")){
                sql = "update jira set No_Of_Pass = (No_Of_Pass + 1) where ID = ?";
                statement = connection.prepareStatement(sql);
                statement.setString(1,id);
                statement.execute();
            }
            sql = "update jira set Total_Count = (Total_Count + 1) where ID = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1,id);
            statement.execute();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            try {
                if (statement != null)
                    connection.close();
            } catch (SQLException se) {
            }// do nothing
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }
    }

    private static void updatePassPercent(String id){
        Connection connection = null;
        String sql;
        PreparedStatement statement = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://10.98.12.33:3306/automation_schema", "username", "password");
            sql = "update jira set Pass_Percent = ROUND(No_Of_Pass * 100.0 / Total_Count, 0) where ID = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, id);
            statement.execute();
        }catch (Exception e) {
            System.out.println(e);
        } finally {
            try {
                if (statement != null)
                    connection.close();
            } catch (SQLException se) {
            }// do nothing
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }
    }

    public static void writeFailedConnectionToDBFromExcel() {
//        this method writes to DB if the DB connection fails during the time
        FileInputStream inputStream = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            inputStream = new FileInputStream(new File("D:\\failedPush.xlsx"));
            Workbook workbook = new XSSFWorkbook(inputStream);
            Sheet dataSheet = workbook.getSheetAt(0);
            int endRow = dataSheet.getLastRowNum();
            endRow++;
            Class.forName("com.mysql.jdbc.Driver");

            connection = DriverManager.getConnection("jdbc:mysql://10.98.12.33:3306/automation_schema", "username", "password");
            String sql = "insert into testresult (ID, Result, Failure_Reason, Environment, Test_Suite, Suite_ID) values (?, ?, ?, ?, ?, ?)";
            for (int i = 1; i < endRow; i++) {

                Row row = dataSheet.getRow(i);
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, row.getCell(0).toString());
                preparedStatement.setString(2, row.getCell(1).toString());
                if (row.getCell(2) != null) {
                    preparedStatement.setString(3, row.getCell(2).toString());
                } else {
                    preparedStatement.setString(3, null);
                }
                preparedStatement.setString(4, row.getCell(3).toString());
                preparedStatement.setString(5, row.getCell(4).toString());
                preparedStatement.setString(6, row.getCell(5).toString());
                preparedStatement.execute();
            }
        } catch (IOException e) {
            System.out.println("file not found");
        } catch (SQLException e) {
            System.out.println("SQL exception");
        } catch (ClassNotFoundException e) {
            System.out.println("Class Not Found");
        } finally {
            try {
                if (preparedStatement != null)
                    connection.close();
            } catch (SQLException se) {
            }// do nothing
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }
    }

    public static void updateJiraTable() {
//        this will update the jira table
        FileInputStream inputStream = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            inputStream = new FileInputStream(new File("D:\\JIRA_Mapping_Regression.xlsx"));
            Workbook workbook = new XSSFWorkbook(inputStream);
            Sheet dataSheet = workbook.getSheetAt(0);
            int endRow = dataSheet.getLastRowNum();
            endRow++;
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://10.98.12.33:3306/automation_schema", "username", "password");
            String sql = "insert into jira (ID, Jira_id) values (?, ?)";
            for (int i = 0; i < endRow; i++) {
                Row row = dataSheet.getRow(i);
                preparedStatement = connection.prepareStatement(sql);
//                String str = row.getCell(1).toString();
//                Integer k = (int)Double.parseDouble(str);
                System.out.println(row.getCell(0).toString());
                System.out.println(row.getCell(1).toString());
                preparedStatement.setString(1, row.getCell(0).toString());
                preparedStatement.setString(2, row.getCell(1).toString());
                preparedStatement.execute();
            }
        } catch (IOException e) {
            System.out.println("file not found");
        } catch (SQLException e) {
            System.out.println("SQL exception");
        } catch (ClassNotFoundException e) {
            System.out.println("Class Not Found");
        } finally {
            try {
                if (preparedStatement != null)
                    connection.close();
            } catch (SQLException se) {
            }// do nothing
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }
    }

   /* private void updatePassPercent(){
        //this will update Pass_Percent in jira table

    }
*/

    /*public static void main(String args[]){
        updatePassPercent("R1");
    }*/
}
