package utilities;

import custom_listeners.TestInitiationListener;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import com.relevantcodes.extentreports.ExtentTest;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import ui_automator.Automator;

import java.io.IOException;
import java.util.Set;

public class TestManager {

    // define the thread local for webDriver and report instances
    private static final ThreadLocal<WebDriver> webDriver = new ThreadLocal<WebDriver>();
    private static final ThreadLocal<ExtentTest> reportLogger = new ThreadLocal<ExtentTest>();
    private static final ThreadLocal<String> testEnvironment = new ThreadLocal<String>();

    public static WebDriver getDriver() {
        return webDriver.get();
    }

    public static void setWebDriver(WebDriver driver) {
        if(TestManager.getDriver()== null) {
            webDriver.set(driver);
        }else {
            System.out.println(TestManager.getDriver().hashCode()+"from TestMANAGER SET");
        };
    }

    public static ExtentTest getReportLogger() {
        return reportLogger.get();
    }

    public static void setReportLogger(ExtentTest extentTest) {
        reportLogger.set(extentTest);
    }

    public static String getTestEnvironment() {
        return testEnvironment.get();
    }

    public static void setTestEnvironment(String environment) {
        testEnvironment.set(environment);
    }


    public static void tearDown(){
        // remove driver instances from ThreadLocal
        ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Manager:tear down - "+" entry");
        if(TestManager.getDriver()!=null) {
            String strCmd = null;
            String strPrintCmd = null;

            if (TestManager.getDriver()instanceof FirefoxDriver){
                strPrintCmd = String.format("TaskKill /F /IM " + "PrintDialog2.exe");
            }else if (TestManager.getDriver()instanceof InternetExplorerDriver){
                strCmd = String.format("TaskKill /F /IM " + "IEDriverServer.exe");
                strPrintCmd = String.format("TaskKill /F /IM " + "PrintDialog_ie.exe");
            }else if (TestManager.getDriver()instanceof ChromeDriver){
                strCmd = String.format("TaskKill /F /IM " + "chromedriver.exe");
                strPrintCmd = String.format("TaskKill /F /IM " + "PrintDialog_ie.exe");

            }

            ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Manager: tearDown - "+" driver:"+TestManager.getDriver().hashCode()+" attempting to quit the driver");
            TestManager.getDriver().quit();
            ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Manager: tearDown - "+" successfully quit the driver");
            try {
                if(strPrintCmd!=null)
                    Runtime.getRuntime().exec(strPrintCmd);
                ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Manager: tearDown - "+strPrintCmd+" closed the print dialog");
                Thread.sleep(2000);
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            try {
                if (strCmd!=null)
                    Runtime.getRuntime().exec(strCmd);
                Thread.sleep(2000);
                ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Manager: tearDown - "+strCmd+" stopped the server");
            } catch (IOException e1) {
                e1.printStackTrace();
                ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Manager: tearDown - "+strCmd+" unable to stop the server");
            } catch (InterruptedException e) {
                e.printStackTrace();
                ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Manager: tearDown - "+strCmd+" unable to stop the server");
            }




        }else{
            ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Manager: tearDown - "+" unable to quit the driver is it is NULL");
        }
        TestManager.webDriver.remove();
        TestManager.testEnvironment.remove();
        ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Test Manager: tearDown - "+" reset test manager");
    }


    public static void tearDownReport(){
        // remove report instances from ThreadLocal
        TestManager.reportLogger.remove();
    }

}
