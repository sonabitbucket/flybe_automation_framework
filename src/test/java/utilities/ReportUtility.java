package utilities;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.DisplayOrder;
import com.relevantcodes.extentreports.NetworkMode;
import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//import org.apache.xpath.operations.Bool;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class ReportUtility {
    public static ExtentReports reportLog = null;
    static String formattedTime;
    public static String reportLocation;
    static SXSSFWorkbook  wb ;
    static Sheet sheet1 ;
    static Sheet sheet2 ;
    static int rowCount_sheet2=1;
    static int rowCount_sheet1=1;

    public static synchronized void initiateReportLog() {
        // creates the report instance
        ZonedDateTime currentTime = ZonedDateTime.now();
        formattedTime = currentTime.toLocalDateTime().format(DateTimeFormatter.ofPattern("dd-MM-yyy_hh_mm_ss"));


        String reportLocation = DataUtility.readConfig("report.location");

//        System.out.println(reportLocation);

        String reportPath ="..execution_reports\\reports";
        if (reportLocation != null){
            reportPath = reportLocation+"\\reports";
        }

        ReportUtility.reportLocation = reportPath+"\\report_for_"+formattedTime;

        String filepath = ReportUtility.reportLocation+"\\flybe_automation_report"+formattedTime+".html";
        Boolean replaceExisting = Boolean.TRUE;
        if (reportLog == null) {
            ReportUtility.reportLog = new ExtentReports(filepath, replaceExisting, DisplayOrder.OLDEST_FIRST, NetworkMode.ONLINE);

            URI configUri = null;
            try {
                configUri = ReportUtility.class.getClassLoader().getResource(DataUtility.readConfig("report.config.name")).toURI();
                ReportUtility.reportLog.loadConfig(configUri.toURL());
            } catch (URISyntaxException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            ReportUtility.reportLog.setTestRunnerOutput("Report Utility:initiate report log - report initiated");

        }
        return;
    }

    public static synchronized void flushReprotLog(){
        // writes the test case result to HTML file
        if (ReportUtility.reportLog != null) {
            ReportUtility.reportLog.close();
        }
        return;
    }

    public static synchronized void deleteOldReports(){
        // delete the old reports
        String reportLocation = "src\\reports";
        try {
            FileUtils.forceDelete((new File(reportLocation)));
        } catch (Exception e) {
            System.out.println("--- No Reports folder found,that needs to be deleted ---");
        }
    }

    public static void clearExcel(){
        try {
            File file = new File("");
            FileInputStream inputStream = new FileInputStream(file);
            Workbook workbook = new XSSFWorkbook(inputStream);
            Sheet sheet = workbook.getSheet("Sheet1");
            for(int i=1;i<sheet.getPhysicalNumberOfRows();i++){
                sheet.removeRow(sheet.getRow(i));
            }
            inputStream.close();
            FileOutputStream outputStream = new FileOutputStream(file);
            workbook.write(outputStream);
            outputStream.close();
        }catch(FileNotFoundException e){
            System.out.println("file is not present");
        }catch(IOException e){
            System.out.println("cannot find file");
        }
    }

    //Write test report to excel
    public static void writeExcel(int noOfcolumns,String... dataToWrite){
        try{
            String filepath = ReportUtility.reportLocation+dataToWrite[0]+formattedTime+".xlsx";
            File file =    new File(filepath);
            FileInputStream inputStream = new FileInputStream(file);
            XSSFWorkbook wb_template = new XSSFWorkbook(inputStream);
            Workbook wkbk = null;
            wkbk = new SXSSFWorkbook(wb_template,100);
            Sheet sheet = (SXSSFSheet) wkbk.getSheet(dataToWrite[1]);
            Row row = sheet.createRow(rowCount_sheet1++);;
            if(dataToWrite[1].equalsIgnoreCase("Sheet2")){
                row = sheet.createRow(ReportUtility.rowCount_sheet2++);
            }
            for(int cellnum = 0; cellnum <= noOfcolumns; cellnum++){
                Cell cell = row.createCell(cellnum);
                cell.setCellValue(dataToWrite[cellnum+2]);
            }
            inputStream.close();
            FileOutputStream outputStream = new FileOutputStream(file);
            wkbk.write(outputStream);
            outputStream.close();
        }catch(FileNotFoundException e){
            System.out.println("file is not present");
        }catch(IOException e){
            System.out.println("cannot find file");
        }
    }

    public static synchronized void setXLFilepathAndCreateXL(int[] noOfcolumns,String... dataToWrite){
        //create excel file with header
        //specify noOfcolumns[0] for sheet1
        //specify nofcolumns[1] for sheet2 and if there is no sheet2 then noOfcolumns[1] = 0
        try {
            String filepath = ReportUtility.reportLocation+dataToWrite[0]+formattedTime+".xlsx";
            wb =  new SXSSFWorkbook(50); // keep 50 rows in memory, exceeding rows will be flushed to disk
            sheet1 = wb.createSheet("Sheet1");
            Row row1 = sheet1.createRow(0);
            int sheet1_cellnum = 0;
            for (int cellnum=0; cellnum <= noOfcolumns[0]; cellnum++) {
                Cell cell = row1.createCell(cellnum);
                cell.setCellValue(dataToWrite[cellnum+1]);
            }
            if(noOfcolumns[1]>0){
                sheet2 = wb.createSheet("Sheet2");
                Row row2 = sheet2.createRow(0);
                int dataCount = noOfcolumns[0]+2;
                for (int cellnum = 0; cellnum <= noOfcolumns[1]; cellnum++,dataCount++) {
                    Cell cell = row2.createCell(cellnum);
                    cell.setCellValue(dataToWrite[dataCount]);
                }
            }
            File f = new File(filepath);
            if(!f.exists()) {
                try{
                    f.createNewFile();
                }catch(IOException e){
                    System.out.println("cannot find filepath");
                }
                FileOutputStream out = new FileOutputStream(f,true);
                wb.write(out);
                out.close();
                // dispose of temporary files backing this workbook on disk
                wb.dispose();
                System.out.println("File is created");
            }
        }catch(FileNotFoundException e){
            System.out.println("cannot find file");
        }
        catch(IOException e){
            System.out.println("cannot find filepath");
        }
    }



}
