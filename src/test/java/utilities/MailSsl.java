package utilities;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class MailSsl {

    static String attachmentName;
    static String attachmentLocation;
    public static String recipients;


    public static void sendMail() {
        // sends mail through gmail ssl smtp

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "SMTP.flybe.local");
        props.put("mail.smtp.port", "25");


        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("api.testing@flybe.com", "Flybe1232");
                    }
                });

        try {



            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("api.testing@flybe.com"));
//            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("api.testing@flybe.com"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipients));

            message.setSubject("Test Execution Report");

            BodyPart messagePart = new MimeBodyPart();
            messagePart.setText("Requesting to rename the file extension to .zip!!!");

            Multipart multipart = new MimeMultipart();

            multipart.addBodyPart(messagePart);
            MimeBodyPart messageBodyPart = new MimeBodyPart();

//            String fileName = "attachment.txt";
//            DataSource source = new FileDataSource(new File("D:\\Fly\\automation_5\\reports\\report_for_30-06-2016_11_10_03.zip"));

            String fileName = attachmentName;
            DataSource source = new FileDataSource(new File(attachmentLocation));


            messageBodyPart.setDataHandler(new DataHandler(source));

            messageBodyPart.setFileName(fileName);

            multipart.addBodyPart(messageBodyPart);

            message.setContent(multipart);

            Transport.send(message);

        } catch (MessagingException e) {
            e.printStackTrace();
        }




    }
}