package utilities;

import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.testng.Reporter;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class DriverFactory {

    public static WebDriver getDriver(String browserName, String environment) {
        // creates the driver instance depending on the browserName passed

        WebDriver driver = null;
        String appUrl = DataUtility.getJsonData(DataUtility.readConfig("env.config.name")).getAsJsonObject("env").getAsJsonObject(environment).get("appUrl").getAsString();
        String driverPath= ".\\pre_requisites\\drivers";
        String driverLocation = DataUtility.readConfig("driver.path");
        String PROXY = DataUtility.readConfig("network.proxy");

        org.openqa.selenium.Proxy proxy = new org.openqa.selenium.Proxy();

        proxy.setHttpProxy(PROXY)
                .setFtpProxy(PROXY)
                .setSslProxy(PROXY);

        if (driverLocation !=null);{
            driverPath = driverLocation;
        }

        String composedDriverPath = null;

        if (browserName.toLowerCase().equals("chrome")) {
            try {
                composedDriverPath = new File(driverPath+"\\Chrome_new\\chromedriver.exe").getCanonicalPath();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.setProperty("webdriver.chrome.driver",composedDriverPath);
            ChromeOptions options = new ChromeOptions();
            options.addArguments("incognito");
            options.addArguments("--start-maximized");
            options.addArguments("--dns-prefetch-disable");
            options.addArguments("--disable-print-preview");
            options.addArguments("--disable-extensions");
            options.setCapability(CapabilityType.SUPPORTS_ALERTS, true);
            options.setCapability(CapabilityType.SUPPORTS_ALERTS, true);
            options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);
            options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
            options.setCapability("nativeEvents", false);
            options.setCapability("ignoreZoomSetting", true);
            options.addArguments("disable-infobars");
            options.addArguments("test-type");
            options.addArguments("--disable-notifications");
            options.addArguments("chrome.switches", "--disable-extensions");
            Map<String, Object> prefs = new HashMap<String, Object>();
            prefs.put("profile.default_content_settings.popups", 0);
            prefs.put("credentials_enable_service", false);
            prefs.put("password_manager_enabled", false);
            //prefs.put("download.default_directory", Constants.DOWNLOAD_PATH);
            options.setExperimentalOption("prefs", prefs);
            options.addArguments("chrome.switches", "--disable-extensions");
            driver = new ChromeDriver(options);
        }
        else if (browserName.toLowerCase().equals("ie")) {
            try {
                composedDriverPath = new File(driverPath + "\\IE\\IEDriverServer.exe").getCanonicalPath();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.setProperty("webdriver.ie.driver", composedDriverPath);
            InternetExplorerOptions options = new InternetExplorerOptions();
            options.setCapability("EnableNativeEvents", false);
            options.setCapability("ignoreZoomSetting", true);
            options.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
            options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
            options.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
            driver = new InternetExplorerDriver(options);
        }
        else if(browserName.toLowerCase().equals("firefox")){
            try {
                composedDriverPath = new File(driverPath + "\\Firefox\\geckodriver.exe").getCanonicalPath();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.setProperty("webdriver.gecko.driver", composedDriverPath);
            FirefoxProfile profile = new FirefoxProfile();
            profile.setPreference("browser.privatebrowsing.autostart", true);
            FirefoxOptions options = new FirefoxOptions();
            options.setProfile(profile);
            driver = new FirefoxDriver(options);

        }
        else if (browserName.toLowerCase().equals("edge")) {
            try {
                composedDriverPath = new File(driverPath + "\\Edge\\MicrosoftWebDriver.exe").getCanonicalPath();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.setProperty("webdriver.edge.driver", composedDriverPath);
            /*EdgeOptions options = new EdgeOptions();
            options.setCapability("EnableNativeEvents", false);
            options.setCapability("ignoreZoomSetting", true);
            options.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
            options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
            driver = new EdgeDriver(options);*/
            driver = new EdgeDriver();
        }
        try {
            driver.manage().window().maximize();
            driver.manage().deleteAllCookies();
            driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
            driver.manage().timeouts().pageLoadTimeout(110, TimeUnit.SECONDS);
            driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
            driver.get(appUrl);
            if(!environment.equalsIgnoreCase("ARDWEB")) {
                driver.manage().deleteAllCookies();
            }
            if (driver != null) {
                if (driver.getTitle().toLowerCase().contains("certificate error")) {
                    driver.navigate().to("javascript:document.getElementById('overridelink').click()");
                }
            }
            ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" DataUtility:get driver - "+" driver:"+driver.hashCode()+" properties to the driver is set");
        }catch (Exception e){
            ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" DataUtility:get driver - "+" driver:"+driver.hashCode()+" exception raised while setting the properties to the driver");
            driver.close();
            driver.quit();
            driver=null;
            ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" DataUtility:get driver - "+" driver cleared");
        }
        Reporter.log("Successfully launched the browser <br><p>");
        ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" DataUtility:get driver - "+" driver:"+driver.hashCode()+" generated");
        return driver;
    }

}

