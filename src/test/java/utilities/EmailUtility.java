package utilities;

import javax.mail.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailUtility {

    private static String reportLocation = "..\\execution_report\\reports";
    ;

    public static void getLatestReport() {
//  fetches the latest report

        String reportPath = DataUtility.readConfig("report.location");
        String compressedReportPath = "..\\execution_report\\compressedReports";

        if (reportPath != null) {
            EmailUtility.reportLocation = reportPath + "\\reports";
            compressedReportPath = reportPath + "\\compressedReports";

        }

        File dir = new File(reportLocation);
        File[] files = dir.listFiles();

        File lastModifiedFile = files[0];
        String lastModifiedFileName = lastModifiedFile.getName();

        for (int i = 0; i < files.length; i++) {
            if (lastModifiedFile.lastModified() < files[i].lastModified()) {
                lastModifiedFile = files[i];
                lastModifiedFileName = lastModifiedFile.getName();
            }
        }
        File compressedReportFile = new File(compressedReportPath);
        if (!compressedReportFile.exists()) {
            compressedReportFile.mkdirs();
        }

        try {
            String targetPath = new File(compressedReportPath).getCanonicalPath() + "\\" + lastModifiedFileName + ".zip";
            String destinationPath = new File(EmailUtility.reportLocation).getCanonicalPath() + "\\" + lastModifiedFileName;
            zaco.OUTPUT_ZIP_FILE = targetPath;
            zaco.SOURCE_FOLDER = destinationPath;
            MailSsl.attachmentLocation = targetPath;
            MailSsl.attachmentName = lastModifiedFileName + ".txt";
        } catch (IOException e) {
            e.printStackTrace();
        }

        return;

    }


    public static String readMail(String userName, String password, String mail_subject, String... linkText) {
        //logins to gmail with user name and password provided
        //reads mail body based on the mail subject provided
        //if link text is given, url is extracted from the link text

        Folder folder = null;
        Store store = null;
        Object content = null;
        String url = null;
        System.out.println("***READING MAILBOX...");
        try {
            Properties props = new Properties();
            props.put("mail.store.protocol", "imaps");
            Session session = Session.getInstance(props);
            store = session.getStore("imaps");
            store.connect("imap.gmail.com", userName, password);
            folder = store.getFolder("INBOX");
            folder.open(Folder.READ_ONLY);
            Message[] messages = folder.getMessages();
            Arrays.sort( messages, (m1, m2 ) -> {
                try {
                    return m2.getSentDate().compareTo( m1.getSentDate() );
                } catch ( MessagingException e ) {
                    throw new RuntimeException( e );
                }
            } );
            for (int i = 0; i < messages.length; i++) {
                System.out.println("Reading MESSAGE # " + (i + 1) + "...");
                Message msg = messages[i];
                String strMailSubject = "", strMailBody = "";
                // Getting mail subject
                Object subject = msg.getSubject();
                // Getting mail body
                content = msg.getContent();
                // Casting objects of mail subject and body into String
                strMailSubject = (String) subject;
                //---- This is what you want to do------
                if (strMailSubject.contains(mail_subject)) {
                    System.out.println(strMailSubject);
                    //code to retrieve URL
                    if (linkText.length > 0) {
                        url = retrieveURL(linkText[0], content);
                    }
                    break;
                }
            }
        } catch (MessagingException messagingException) {
            messagingException.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (folder != null) {
                try {
                    folder.close(true);
                } catch (MessagingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            if (store != null) {
                try {
                    store.close();
                } catch (MessagingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return url;
    }


    private static String retrieveURL(String linkText, Object content) throws MessagingException {
        String url = null;
        if (content instanceof Multipart) {
            Multipart mp = (Multipart) content;
            for (int i = 0; i < mp.getCount(); i++) {
                BodyPart bp = mp.getBodyPart(i);
                try {
                    String html = getMessageContent(bp);
                    if(html.contains(linkText)){
                        url = extractUrls(html);
                    }
                }catch(Exception e){
                    System.out.println("unable to convert mail bodypart "+i+" to html format" );
                }
            }
        }
        return url;
    }



    /**
     * Returns HTML of the email's content
     */
    public static String getMessageContent(BodyPart message) throws Exception {
        StringBuilder builder = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(message.getInputStream()));
        String line;
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }
        return builder.toString();
    }

    /**
     * Returns all urls from an email message with the linkText specified
     */
    /*private static List<String> getURL(String html) throws Exception{
        List<String> allMatches = new ArrayList<String>();
        Pattern linkPattern = Pattern.compile("href=\"([^\"]*)",  Pattern.CASE_INSENSITIVE|Pattern.DOTALL);
        Matcher pageMatcher = linkPattern.matcher(html);
        while(pageMatcher.find()){
            allMatches.add(pageMatcher.group(1));
        }
        System.out.println("URL found "+allMatches);
        return allMatches;
    }*/

    public static String extractUrls(String text)
    {
        List<String> containedUrls = new ArrayList<String>();
        String urlRegex = "((https?|ftp|gopher|telnet|file):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
        Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);
        Matcher urlMatcher = pattern.matcher(text);

        while (urlMatcher.find())
        {
            containedUrls.add(text.substring(urlMatcher.start(0),
                    urlMatcher.end(0)));
        }
        System.out.println("url - "+containedUrls);
        return containedUrls.get(0);
    }

}





