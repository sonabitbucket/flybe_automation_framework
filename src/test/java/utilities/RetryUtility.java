package utilities;

import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebDriver;
import org.testng.IInvokedMethod;
import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static utilities.ReportUtility.reportLog;


public class RetryUtility implements IRetryAnalyzer {

    private int retryCount = 0;
    private int maxRetryCount = Integer.parseInt(DataUtility.readConfig("retry.count"));
//    private String testName=null;


 // Below method returns 'true' if the test method has to be retried else 'false'
//and it takes the 'Result' as parameter of the test method that just ran
    public boolean retry(ITestResult result) {
        ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Retry Utility:retry for test - "+" driver:"+TestManager.getDriver().hashCode()+" entry");

        // checks if the retry is to be done based on no. of allready done retries
        if (this.retryCount < this.maxRetryCount) {
            System.out.println("_*retry count*__");
            this.retryCount++;
            return true;
        }

//        if(this.testName== null){
//            this.testName = result.getMethod().getMethodName();
////            logReport(result);
////            reportTearDown();
//        }
        ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Retry Utility:retry for test - "+" driver:"+TestManager.getDriver().hashCode()+" exit");
        return false;
    }


//    private void logReport(ITestResult testResult) {
    //kept this methods for future use
//        //log the status of each test and takes the screenShot in case of failure,
//        // call the tearDown process after each test
//
//        if (testResult.getMethod().isTest()) {
//            ZonedDateTime currentTime = ZonedDateTime.now();
//            String formattedTime = currentTime.toLocalDateTime().format(DateTimeFormatter.ofPattern("dd-MM-yyy_hh_mm_ss"));
//
//            if (testResult.getStatus() == 2) {
////                ActionUtility.takeScreenShot(testResult.getMethod().getMethodName(),formattedTime);
//                TestManager.getReportLogger().log(LogStatus.FAIL, testResult.getName() + "**FAILED***");
//                TestManager.getReportLogger().log(LogStatus.INFO, testResult.getThrowable());
//                TestManager.getReportLogger().log(LogStatus.INFO, "Snapshot below: ", TestManager.getReportLogger().addScreenCapture("screenshots\\" + testResult.getMethod().getMethodName()+"_"+formattedTime+".png"));
//            } else if (testResult.getStatus() == 1) {
//                TestManager.getReportLogger().log(LogStatus.PASS, testResult.getName() + "**PASSED***");
//            } else if (testResult.getStatus() == 3) {
//                TestManager.getReportLogger().log(LogStatus.SKIP, testResult.getName() + "**SKIPPED***");
//            }
//        }
//
//    }


//    private void reportTearDown(){
    //kept this methods for future use
//        if ((TestManager.getReportLogger() != null))  {
//            reportLog.endTest(TestManager.getReportLogger());
//            reportLog.flush();
//            TestManager.tearDownReport();
//
//        }
//    }

}
