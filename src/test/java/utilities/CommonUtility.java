package utilities;

import com.github.javafaker.Faker;

import java.io.IOException;
import java.net.InetAddress;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.File;

/*
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.Country;
*/

import static utilities.DataUtility.readConfig;


public class CommonUtility {

    public static String convertNonEnglishDateToEnglishDate(String inputdate,String presentFormat,String requiredFormat,String presentLanguage){
        // Convert to US date format
        Locale localeUS = new Locale("en", "US");
        DateFormat dateFormatUS = new SimpleDateFormat(requiredFormat, localeUS);
        Date date = null;
        try {
            date = new SimpleDateFormat(requiredFormat).parse(convertDateFormat(presentFormat, requiredFormat, inputdate, presentLanguage));
        }catch (ParseException e) {
            e.printStackTrace();
        }
        return dateFormatUS.format(date);
    }

    public static String convert24HourTo12HourFormat(String time)
    {
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            final Date dateObj = sdf.parse(time);
            time = new SimpleDateFormat("hh:mm aa").format(dateObj);
            System.out.println("12 hour time - "+time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return time;
    }

    public static LocalTime convertStringToTime(String str){
        return LocalTime.parse(str);
    }

    public static LocalTime SubtractMinutesFromGivenTime(LocalTime time,int minutes){
        return time.minusMinutes(minutes);
    }

    public static String removeCommaFromCurrency(String value){
        String repl = value.replaceAll("(?<=\\d),(?=\\d)|\\$", "");
        return repl;
    }

    public static double convertStringToDouble(String value){
        // will convertStringToDouble
        try {
            return Double.parseDouble(getDoubleValueFromString(removeCommaFromCurrency(value)));
        }catch (NumberFormatException e){
            return 0.0;
        }catch (NullPointerException e) {
            return 0.0;
        }
    }

    private static String getDoubleValueFromString(String value){
        // reads the decimal pattren from string and converts it double and return
        Pattern regex = Pattern.compile("(\\d+(?:\\.\\d+)?)");
        Matcher matcher = regex.matcher(value);
        String doubleVal = null;
        while(matcher.find()) {
            doubleVal = matcher.group(1);
        }
            return doubleVal;
    }

    public static String convertDateFormat(String presentFormat, String requiredFormat, String dateToConvert,String... language){
//        this converts the present format to required format
        DateFormat originalFormat = new SimpleDateFormat(presentFormat, Locale.ENGLISH);
        if(language.length>0){
            if(language[0].equalsIgnoreCase("FR")) {
                originalFormat = new SimpleDateFormat(presentFormat, Locale.FRENCH);
            }
            if(language[0].equalsIgnoreCase("DE")){
                originalFormat = new SimpleDateFormat(presentFormat, Locale.GERMAN);
            }
            if(language[0].equalsIgnoreCase("ES")){
                originalFormat = new SimpleDateFormat(presentFormat,new Locale("es","ES"));
            }
        }
        DateFormat targetFormat = new SimpleDateFormat(requiredFormat);
        Date date = null;
        try {
            date = originalFormat.parse(dateToConvert);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formattedDate = targetFormat.format(date);
        return formattedDate;
    }

    public static HashMap<String,String> getRandomName(HashMap<String,String> testData){
        //creates random name in Title FirstName Lastname format
        Faker faker = new Faker();
        testData.put("adult1Name","Miss "+faker.firstName()+" "+faker.lastName());
        return testData;
    }

    public static Double truncateToTWODecimalPlaces(Double value){
        return Math.rint(value * 100) / 100;
    }

    /*public static String getCountryFromIP() {
        String IsoCode = null;
        try {
            File dbFile = new File(DataUtility.readConfig("geolocation.path"));
            DatabaseReader reader = new DatabaseReader.Builder(dbFile).build();
            //InetAddress ipAddress = InetAddress.getByName(InetAddress.getLocalHost().getHostAddress());
            InetAddress ipAddress = InetAddress.getByName("103.227.97.127");
            CityResponse response = reader.city(ipAddress);
            Country country = response.getCountry();
            IsoCode = country.getIsoCode();
        }catch(IOException io){

        }catch(Exception e){
            System.out.println("unable to get geolocation");
        }
        System.out.println("country code "+IsoCode);
        return IsoCode;
    }*/
}

