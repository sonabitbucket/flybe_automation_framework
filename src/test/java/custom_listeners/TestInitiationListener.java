package custom_listeners;


import org.testng.IExecutionListener;
import ui_automator.Automator;
import utilities.DataUtility;
import utilities.ReportUtility;
import utilities.TestManager;
import utilities.zaco;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class TestInitiationListener implements IExecutionListener {

    static String startTimeofExecution;
    static String endTimeofExecution;
    static int noOfTestcasesExecuted;
    static int noOfTestcasesPassed;
    static int noOfTestcasesFailed;
    static String testEnvironment;

    @Override
    public void onExecutionStart() {
        //Calling the methods that deletes old and initiate the new reports at the start of execution
//        ReportUtility.deleteOldReports();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        String startTimeStamp = dtf.format(now);
        startTimeofExecution = LocalDateTime.parse(startTimeStamp, dtf) + "";

        ReportUtility.initiateReportLog();
        ReportUtility.reportLog.setTestRunnerOutput("Release: " + DataUtility.readConfig("project.release"));
        ReportUtility.reportLog.setTestRunnerOutput("Test Initiation Listener:on execution start - exit");
        ReportUtility.setXLFilepathAndCreateXL(new int[]{6, 2}, "\\flybe_automation_report_", "Build or Release", "Environment", "Execution Start Time", "Execution End Time", "No. Of Testcases Executed", "No. Of Testcases Passed", "No. Of Testcases Failed", "Testcase", "Result", "Failure_Reason"); //required if results needs to be saved in excel file
        if (DataUtility.readConfig("pnr.generate").equals("yes")) {
            ReportUtility.setXLFilepathAndCreateXL(new int[]{6,0},"\\flybe_automation_report_PNRGeneration_","TEST DESCRIPTION","PNR","LASTNAME","FREQUENT FLYER NUMBER","OUTBOUND FLIGHT DATE","INBOUND FLIGHT DATE","E-TICKET");
        }
    }
    @Override
    public void onExecutionFinish() {
        //Calling the method that writes the result to the HTML file at the end of the execution
        ReportUtility.reportLog.setTestRunnerOutput("Test Initiation Listener:on execution finish - entry");
        ReportUtility.reportLog.setTestRunnerOutput("Test Initiation Listener:on execution finish - flushing the report");
        ReportUtility.flushReprotLog();
        if(ReportUtility.reportLog!=null){
            ReportUtility.reportLog=null;
        }
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        String endTimeStamp = dtf.format(now);
        endTimeofExecution = LocalDateTime.parse(endTimeStamp, dtf)+"";
        //timeTakenForTCExecution = ChronoUnit.SECONDS.between(startTimeofTCExecution, endTimeofTCExecution)+"";
        ReportUtility.writeExcel(6,"\\flybe_automation_report","Sheet1",DataUtility.readConfig("project.release"),testEnvironment,startTimeofExecution,endTimeofExecution,noOfTestcasesExecuted+"",noOfTestcasesPassed+"",noOfTestcasesFailed+"");
//       zaco.zipAndMail();
//       Automator.afterExcecution();
    }
}
