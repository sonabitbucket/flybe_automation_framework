package custom_listeners;

import org.testng.IAnnotationTransformer;
import org.testng.IRetryAnalyzer;
import org.testng.annotations.ITestAnnotation;
import utilities.ReportUtility;
import utilities.RetryUtility;
import utilities.TestManager;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class RetryListner implements IAnnotationTransformer {

    @Override
    public void transform(ITestAnnotation testannotation, Class testClass,
                          Constructor testConstructor, Method testMethod){
//
//        if (ReportUtility.reportLog!=null) {
//            ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId() + " Retry Listner:transform - "  + " entry");
//        }
        // This will re-trigger the failed test case
        IRetryAnalyzer retry = testannotation.getRetryAnalyzer();

        if (retry == null)	{

            testannotation.setRetryAnalyzer(RetryUtility.class);
//            ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId()+" Retry Listner:transform - "+" driver:"+ TestManager.getDriver().hashCode()+" called retry utility");
        }
//        if (ReportUtility.reportLog!=null) {
//            ReportUtility.reportLog.setTestRunnerOutput(Thread.currentThread().getId() + " Retry Listner:transform - "+" exit");
//        }

    }

}
